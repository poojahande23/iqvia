﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class UserLog_VM
    {
        public Int32 LogID { get; set; }
        public Int32 UserID { get; set; }
        public Int32 MenuID { get; set; }
        public string PeriodType { get; set; }
        public Int32 StartPeriod { get; set; }
        public Int32 EndPeriod { get; set; }
        public Int32  CityID { get; set; }
        public DateTime LogDate { get; set; }
        public string CategoryType { get; set; }
        public string CategoryValue { get; set; }
        public string PatchID { get; set; }
        public string DataType { get; set; }
        public string TCLevel { get; set; }
        public string TCCode { get; set; }
        public string BrandID { get; set; }
        public string ProductID { get; set; }
        public string SessionID { get; set; }

    }
    public class AccessLogReport_IVM
    {
        public Int32 CompanyID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class AccessLogReport_OVM
    {
       
        public Int32 UserID { get; set; }
        public Int32 MenuID { get; set; }
        public string UserName { get; set; }
        public string MenuName { get; set; }
        public string SubscriptionPeriodTypeName { get; set; }
        public Int32 StartPeriod { get; set; }
        public Int32 EndPeriod { get; set; }
        public string CityName { get; set; }
        public string LogDate { get; set; }
        public string CategoryType { get; set; }
        public string CategoryValue { get; set; }
        public string PatchID { get; set; }
        public string DataType { get; set; }
        public string TCLevel { get; set; }
        public string TCCode { get; set; }
        public string BrandID { get; set; }
        public string ProductID { get; set; }
        public string LoginTime { get; set; }
        public string LogoutTime { get; set; }
        
    }


}
