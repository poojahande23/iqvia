﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class CompanyTeamDetails
    {
        public int TeamID { get; set; }
        public string TeamName { get; set; }
        public int UserCount { get; set; }
    }

    public class CompanyUserCount_OVM
    {
        public int CompanyUserRoleID { get; set; }
        public string RoleName { get; set; }
        public int UserCount { get; set; }
    }

    public class CompanyUser
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public double MobileNo { get; set; }
        public int CompanyID { get; set; }
        public int CompanyRoleID { get; set; }
        public string RoleName { get; set; }
        public Int32 Pincode { get; set; }
       public int Active { get; set; }
        public int DivisionID { get; set; }
        public int RoleID { get; set; }
        public int ReportingManagerID { get; set; }
        public string ReportingManagerName { get; set; }
        public string DivisionName { get; set; }
        public string ProfileImage { get; set; }
        public UserAccessRights UserAccessRights { get; set; }
    }
    public class Permission
    {
        public int PermissionID { get; set; }
        public string PermissionName { get; set; }
    }

    public class UserRole
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class CompanyUserRole
    {
        public int CompanyUserRoleID { get; set; }
        public string RoleName { get; set; }
        public int ParentRoleID { get; set; }
        public string ParentRoleName { get; set; }
        public int CompanyID { get; set; }
        public int DivisionID { get; set; }
        public IList<Permission_IVM> permissions { get; set; }
    }

    public class Team_IVM
    {
        public int TeamID { get; set; }
        public int CompanyID { get;set;}
        public string TeamName { get; set; }
        public int TeamMemberCount { get; set; }
        public int TeamManagerID { get; set; }
        public int TeamSalesTarget { get; set; }
        public int Active { get; set; }
    }

    public class ChangePassword_IVM
    {
        public Int32 UserID { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public Int32 isAdmin { get; set; }
    }

    public class User_IVM
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public double MobileNo { get; set; }
        public int DivisionID { get; set; }
        public int RoleID { get; set; }
        public Int32 Pincode { get; set; }
        public int Active { get; set; }
        public int CompanyID { get; set; }
        public int CompanyRoleID { get; set; }
        public int DataAccessLevelID { get; set; }
        public string DataAccessIDs { get; set; }
        public int ReportingManagerID { get; set; }
        public string ProfileImage { get; set; }
        public UserAccessRights UserAccessRights { get; set; }
       
    }
    public class Permission_IVM
    {
        public int ApplicationID { get; set; }
        public bool Permission { get; set; }
    }

    public class CompanyUserRole_IVM
    {
        public int CompanyUserRoleID { get; set; }
        public int CompanyID { get; set; }
        public int DivisionID { get; set; }
        public string RoleName { get; set; }
        public int ParentRoleID { get; set; }
        public IList<Permission_IVM> permissions { get; set; }
    }

    public class UserMapping_IVM
    {
        public int TeamID { get; set; }
        public int UserID { get; set; }
        public int Active { get; set; }
    }
    public class TeamPermissionMapping_IVM
    {
        public int TeamID { get; set; }
        public int PermissionID { get; set; }
        public int Active { get; set; }
    }

    public class UserInfo
    {
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public int DivisionID { get; set; }
        public int CompanyID { get; set; }
        public string UserName { get; set; }
        public int CompanyRoleID { get; set; }
        public string CompanyRoleName { get; set; }
        public string ProfileImage { get; set; }
        public string LogoFileName { get; set; }
        public string CompanyName { get; set; }
        public string DivisionName { get; set; }
        public DateTime SubscriptionStartDate { get; set; }
        public DateTime SubscriptionEndDate { get; set; }
        public bool CompanyStatus { get; set; }
        public string SessionID { get; set; }
        public bool Active { get; set; }
      
        public MenuAccessRights objUserMenuAccess { get; set; }




    }
    public class MenuAccessRights
    {
        public bool isSummary { get; set; } = false;
        public bool IsAnalysis { get; set; } = false;
        public bool isMapView { get; set; } = false;
        public bool isCompetativeAnalysis { get; set; } = false;
        public bool isReport { get; set; } = false;

    }

    public class SessionInfo
    {
        public string SessionID { get; set; }
        public Int32 UserID { get; set; }
        public DateTime SessionDate { get; set; }
    }

    public class AdminUserInfo
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }



}
