﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
  public class SavedSearches
    {
        public int SearchKeyID { get; set; }
        public string SearchKey { get; set; }
        public string SearchData { get; set; }
        public string UserID { get; set; }
        public string TabType { get; set; }
    }
}
