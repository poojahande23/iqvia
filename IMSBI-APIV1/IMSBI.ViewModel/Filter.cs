﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class cityMaster
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int StateID { get; set; }
    }

    public class PincodeCoordinateIVM
    {
       public string PatchIDs { get; set; }
       public Int32 CityID { get; set; }
       public Int32 CompanyID { get; set; }
       public Int32  DivisionID { get; set; }
    }

    public class PincodeCoordinate_OVM
    {
        public Int32 PatchID { get; set; }
        public string PatchName { get; set; }
        public string ColorCode { get; set; }
        public double Lat { get; set; }
        public double lon { get; set; }
        public IList<pincodedata> pincodedatadetails { get; set; }
    }

    public class pincoderawdata
    {
        public Int32 BrickID { get; set; }
        public string BrickName { get; set; }
        public Int32 Pincode { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
    }
    public class pincodedata
    {
        public Int32 PincodeID { get; set; }
        public ArrayList latlong { get; set; }
    }

    public class StateMaster
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int ZoneID { get; set; }
    }

    public class DataSubscriptionType
    {
        public Int32 DataSubscriptionTypeID { get; set; }
        public string DataSubscriptionTypeName { get; set; }
        public string DataSubscriptionTypeShortName { get; set; }
    }

    public class TherapyMaster
    {
        public string TherapyCode { get; set; }
        public string TherapyName { get; set; }
       
    }

    public class ApplicationPermissionOVM
    {
        public int PermissionID { get; set; }
        public string PermissionName { get; set; }
    }

    public class ApplicationDataAccessLevelOVM
    {
        public int DataAccessLevelID { get; set; }
        public string DataAccessLevel { get; set; }
    }

    public class ZoneMaster
    {
        public int ZoneID { get; set; }
        public string ZoneName { get; set; }
    }

    public class BrandMaster
    {
        public Int64 BrandID { get; set; }
        public string BrandName { get; set; }
    }
    public class TimePeriod
    {
        public int TimePeriodID { get; set; }
       
        public string MonthYear { get; set; }
    }

    

    public class CategoryValues
    {
        public Int64 CategoryID { get; set; }
        public string CategoryValue { get; set; }
    }

    public class GeographyValues
    {
        public int GeographyID { get; set; }
        public string GeographyValue { get; set; }
    }

    public class ComparisonValues
    {
        public Int64 ComparisonID { get; set; }
        public string ComparisonValue { get; set; }
    }
    public class PDF_IVM
    {
        public string PDFName { get; set; }
        public string PDFData { get; set; }
    }

    public class BrickMaster
    {
        public Int32 BrickID { get; set; }
        public string BrickName { get; set; }
        public Int32 CityID { get; set; }
        public bool Active { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public bool IsCustom { get; set; }
        public Int32  CompanyID { get; set; }
        public Int32 DivisionID { get; set; }
        public   bool IsCluster { get; set; }
        public string PinCodeIDs { get; set; }
    }


    public class BrickMaster_OVM
    {
        public Int32 BrickID { get; set; }
        public string BrickName { get; set; }
        public string CityName { get; set; }
        public bool Active { get; set; }
        public string CompanyName { get; set; }
        public string DivisionName { get; set; }
        public string PinCodeIDs { get; set; }
    }

}
