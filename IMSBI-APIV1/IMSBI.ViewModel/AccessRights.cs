﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
   public class CompanyAccessRights
    {
        public Int32 CompanyID { get;set;}
        public int AccessRightType { get; set; }
        public string TCValues { get; set; }
        public string BrandIDs { get; set; }
       

    }

    public class UserAccessRights
    {
        public Int32 UserID { get; set; }
        public int AccessRightType { get; set; }
        public string TCValues { get; set; }
        public string CityIDs { get; set; }
     

    }
}
