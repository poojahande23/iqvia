﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
   public  class SubscriptionPeriodType
    {
        public string SubscriptionPeriodTypeName { get; set; }
        public Int32 SubscriptionPeriodTypeID { get; set; }
        public string PeriodShortName { get; set; }
        public bool Active { get; set; }
        
    }
    public class SubscriptionPeriodDetails
    {
        public string SubscriptionPeriodTypeName { get; set; }
        public Int32 SubscriptionPeriodTypeID { get; set; }
        public string PeriodShortName { get; set; }
        public bool isDefault { get; set; }

    }
}
