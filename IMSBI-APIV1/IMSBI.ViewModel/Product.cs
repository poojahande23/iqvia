﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class Product
    {
        public Int64 PFCID { get; set; }
        public string ProductName { get; set; }
    }
}
