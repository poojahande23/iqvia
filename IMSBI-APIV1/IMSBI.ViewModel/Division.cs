﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class Division_IVM
    {
        public int DivisionID { get; set; }
        public int CompanyID { get; set; }
        public string DivisionName { get; set; }
        public Int32 DataSubscriptionTypeID { get; set; }
        public string SubscribedPeriodTypeIDs { get; set; }
        public Int32 DefaultPeriodTypeID { get; set; }
    }

    public class Division_OVM
    {
        public Int64 PFCID { get; set; } 
        public string ProductName { get; set; }
        public int IsNewIntroduction { get; set; }
        public int IsOnFocus { get; set; }
        public string Manuf_Name { get; set; }
        public string TC4Name { get; set; }
    }
    public class DivisionInfo
    {
        public int DivisionID { get; set; }
        public int CompanyID { get; set; }
        public string DivisionName { get; set; }
        public int DataSubscriptionTypeID { get; set; }
        public string SubscribedPeriodTypeIDs { get; set; }
        public Int32 DefaultPeriodTypeID { get; set; }
    }

    public class DivisionBrand_OVM
    {
        public Int32 BrandID { get; set; }
        public string BrandName { get; set; }
    }

    public class BrandGroup
    {
        public Int32 BrandGroupID { get; set; }
        public string BrandGroupName { get; set; }
        public int CompanyID { get; set; }
        public int DivisionID { get; set; }
        public string BrandIDs { get; set; }

    }

    public class DivisionProductMapping
    {
        public Int64 PFCID { get; set; }
     
        public int CompanyID { get; set; }
        public int DivisionID { get; set; }
        public int IsOnFocus { get; set; }
        public int IsNewIntroduction { get; set; }



    }
}
