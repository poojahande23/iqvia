﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class Manufacturer
    {
        public int ManufacturerCode { get; set; }
        public string ManufacturerName { get; set; }

    }
    public class AddProductCVM
    {
        public Int32 ProductID { get; set; }
        public Int32 CompanyID { get; set; }
        public string MappedProductIds { get; set; }
    }
    public class ProductData
    {
        public Int32 ProductID { get; set; }
        public string ProductName { get; set; }
        public string TCCode { get; set; }
    }
    public class ProductTherapy
    {
        public Int32 ProductID { get; set; }
        public string TC4Code { get; set; }
    }

    public class DefinedCVM
    {
        public Int32 CompanyPFCID { get; set; }
        public string OtherPFC { get; set; }
    }
    public class CVMProductTCDetails
    {
        public IList<Int32> ProductList { get; set; }
        public IList<string> TC4CodeList { get; set; }
    }
    public class CompanyDivision
    {
        public int DivisionID { get; set; }
        public string DivisionName { get; set; }
        public string DataSubscriptionPeriodName { get; set; }
    }

    public class CompanyMaster
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Indian_MNC { get; set; }
    }

    public class SubscriptionTypeCounter
    {
        public string SubscriptionName { get;set;}
        public int MemberCount { get; set; }
        public string ColorCode { get; set; }
    }

    public class SubscriptionType
    {
        public int SubscriptionTypeID { get; set; }
        public string SubscriptionName { get; set; }
        public string ColorCode { get; set; }
    }

    public class ReportingMangerDetails
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public int DataAccessLevelID { get; set; }
    }
    public class BrandPerformance
    {
        public Int32 BrandID { get; set; }
        public string BrandName { get; set; }
        public double TotalSales { get; set; }
        public int Rank { get; set; }
        public Int32 BrickID { get; set; }
        public double TotalUnits { get; set; }
        public Int32 ValueGrowth { get; set; }
        public Int32 UnitGrowth { get; set; }
        public string CompanyName { get; set; }
        public Int32 CityID { get; set; }
    }

    public class MarketGrowth
    {
        public double CityGrowthValue { get; set; }
        public double CityGrowthUnit { get; set; }
        public IList<MarketPatchGrowth> PatchGrowth { get; set; }

    }
    public class MarketPatchGrowth
    {
        public Int32 BrickID { get; set; }
        public double PatchGrowthValue { get; set; }
        public double PatchGrowthUnit { get; set; }
    }

        public class CompanyPerformance
    {
        public int CompanyID { get; set; }
       
        public string CompanyName { get; set; }

        public double TotalSales { get; set; }
        public int Rank { get; set; }
        public double MarketShare { get; set; }
        public double MarketUnitShare { get; set; }
        public int BrandID { get; set; }
        public Int32 TimePeriod { get; set; }
        public string PeriodName { get; set; }
        public Int32 BrickID { get; set; }
        public double TotalUnits { get; set; }
        public string ColorCode { get; set; }
       
    }
    public class CompanyCompanyPerformancePatch
    {
        public string Key { get; set; }
        public Int32 ID { get; set; }
        public IList<CompanyPerformance> CompanyPerformance { get; set; }
    }


    public class BrandComprsionPerformanceDetails
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public BrandComparsionPerformanceDetails CityComparsionData { get; set; }
    }

    public class BrandComparsionPerformanceDetails
    {
        public IList<CompanyPerformance> CompanyPerformance { get; set; }
        public IList<CompanyPerformance> CompanyPerformanceUnit { get; set; }
        public double TotalSalesValue { get; set; }
        public double TotalSalesUnit { get; set; }
       // public IList<Patch> PatchList { get; set; }        
        public IList<SalesByPatch> CompanyComparisonSales { get; set; }
        public IList<SalesByPatch> CompanyComparisonUnit { get; set; }
     //   public IList<PatchData> CompanyComparisonByPatchSales { get; set; }
       // public IList<PatchData> CompanyComparisonByPatchUnit { get; set; }
        public IList<TableList> CompanyPerformanceSalesTable { get; set; }
        public IList<TableList> CompanyPerformanceUnitTable { get; set; }
        public IList<PatchDetailsTableList> CompanyPerformanceSalesPatchTable { get; set; }
        public IList<PatchDetailsTableList> CompanyPerformanceUnitPatchTable { get; set; }
        public IList<BarChartData> BarChartSales { get; set; }
        public IList<BarChartData> BarChartUnit { get; set; }
    }

    public class ComparsionPerformanceDetails
    {
        public IList<CompanyPerformance> CompanyPerformance { get; set; }
        public IList<CompanyPerformance> CompanyPerformanceUnit { get; set; }
        public double TotalSalesValue { get; set; }
        public double TotalSalesUnit { get; set; }
        public IList<Patch> PatchList { get; set; }        
        public IList<SalesByPatch> CompanyComparisonSales { get; set; }
        public IList<SalesByPatch> CompanyComparisonUnit { get; set; }
         public IList<PatchData> CompanyComparisonByPatchSales { get; set; }
         public IList<PatchData> CompanyComparisonByPatchUnit { get; set; }
        public IList<TableList> CompanyPerformanceSalesTable { get; set; }
        public IList<TableList> CompanyPerformanceUnitTable { get; set; }
        public IList<PatchDetailsTableList> CompanyPerformanceSalesPatchTable { get; set; }
        public IList<PatchDetailsTableList> CompanyPerformanceUnitPatchTable { get; set; }
        public IList<BarChartData> BarChartSales { get; set; }
        public IList<BarChartData> BarChartUnit { get; set; }
    }

    public class CompanyBrandPerformance_OVM
    {
       
        public string TCCategoryName { get; set; }
        public IList<CompanyBrandPerformanceData> CompanyBrandPerformanceData { get; set; }
    }

    public class CompanyBrandPerformanceData
    {
        public Int32 CityID { get; set; }
        public string CityName { get; set; }
        public ReportGraphData ReportData { get; set; }

    }
    public class ReportGraphData
    {
        public IList<Patch> PatchList { get; set; }
        public IList<SalesByPatch> CompanyComparisonSales { get; set; }
        public IList<SalesByPatch> CompanyComparisonUnit { get; set; }
        public IList<SalesByPatch> CompanyPatchComparisonSales { get; set; }
        public IList<SalesByPatch> CompanyPatchComparisonUnit { get; set; }
        public IList<TableList> CompanyPerformanceSalesTable { get; set; }
        public IList<TableList> CompanyPerformanceUnitTable { get; set; }

        public IList<TableList> CompanyPerformanceGrowthSalesTable { get; set; }
        public IList<TableList> CompanyPerformanceGrowthUnitTable { get; set; }

        public ProductRank ProductRankDetailsValue { get; set; }
        public ProductRank ProductRankDetailsUnit { get; set; }
    }
    public class ProductRank
    {
       public string ProductName { get; set; }
        public Int32 ProductID { get; set; }
       public IList<ProductBrickRank> Rank { get; set; }
    }
    public class ProductBrickRank
    {
        public Int32 BrickID { get; set; }
        public string BrickName { get; set; }
        public Int32 Rank { get; set; }
    }


    public class PatchDetailsTableList
    {
        public Int32 PatchID { get; set; }
        public string PatchName { get; set; }
        public IList<TableList> TableData { get; set; }
    }

    public class BrandPatchDetailsTableList
    {
        public Int32 PatchID { get; set; }
        public string PatchName { get; set; }
        public IList<TableListBrand> TableData { get; set; }
    }

    public class CompanyPerformanceTimePeriod
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public Int32 TotalSales { get; set; } 
        public double MarketShare { get; set; }
        public int BrandID { get; set; }

    }

    public class CompanyPerformanceData
    {
        public IList<Mapview_OVM> CompanyPerformanceValueList { get; set; }
        public IList<Mapview_OVM> CompanyPerformanceUnitList { get; set; }
    }

    public class Mapview_OVM
    {
        public MapData map_chart { get; set; }
        public string name { get; set; }
        public Int32 PatchID { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }
    public class MapData
    {
        public ArrayList data { get; set; }
    }





    public class SubscribedCompany_OVM
    {
        public int CompanyID { get; set; }
        public int SubscribedCompanyID { get; set; }
        public string CompanyName { get; set; }
        public int Division { get; set; }
        public int TeamStrength { get; set; }
        public string SubscriptionName { get; set; }
        public string SubscriptionLocationID { get; set; }
        public int Action { get; set; }
        public string ColorCode { get; set;}
        public string SubscriptionLocation { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int DaysRemaning { get; set; }
        public Int32 AdminUserID { get; set; }
        public Int32 ActiveUsers { get; set; }
        public string LogoFileName { get; set; }
        public string ContactPersonName { get; set; }
        public Double ContactPersonPhone { get; set; }
        public string ContactPersonEmail { get; set; }
       
    }
    public class SubscribedCompanyInfo
    {
        public int CompanyID { get; set; }     
        public int NoOfDivision { get; set; }
        public int TeamStrength { get; set; }
        public Int32 SubscriptionTypeID { get; set; }
        public string SubscriptionLocationID { get; set; }
        public string SubscriptionDate { get; set; }
      
        public Int32 CityID { get; set; }
        public Int32 StateID { get; set; }
        public Int32 Pincode { get; set; }
        public string Address { get; set; }
        public string LandMark { get; set; }
        public string Country { get; set; }
        public Int32 Action { get; set; }
        public DateTime SubscriptionStartDate { get; set; }
        public DateTime SubscriptionEndDate { get; set; }
        public string ProfileImage { get; set; }
        public IVM_ContactInfo ContactInfo { get; set; }
        public string CityName { get; set; }
        public string ColorCode { get; set; }
        public string SubscriptionPeriodTypeIDs { get; set; }
        public Int32 DefaultPeriodID { get; set; }
        public CompanyAccessRights CompanyAccessRights { get; set; }
        public Int32 DataSubscriptionTypeID { get; set; }


    }

    public class CompanySubscripedPeriodType
    {
        public string SubscribedPeriodType { get; set; }
        public Int32 DefaultSubscriptionType { get; set; }
    }

    public class CompanyProductList_OVM
    {
        public Int32 PFCID { get; set; }
        public string ProductName { get; set; }
        public string TCCode4 { get; set; }
    }

    public class SubscribedCompany_IVM
    {
        public int CompanyID { get; set; } 
        public int NoOfDivision { get; set; }
        public int TeamStrength { get; set; }
        public int SubscriptionTypeID { get; set; }
        public string SubscriptionLocationID { get; set; }
        public int Action { get; set; }
        public int CityID{ get; set; }
        public int StateID { get; set; }
        public string CityName { get; set; }
        public Int32 Pincode { get; set; }
        public string Address { get; set; }
        public string LandMark { get; set; }
        public string Country { get; set; }
        public string Divisions { get; set; }
        public AdminUserInfo UserInfo { get; set; }
        public string SubscriptionStartDate { get; set; } 
        public string SubscriptionEndDate { get; set; }
        public IVM_ContactInfo ContactInfo { get; set; }
        public bool isUpdate { get; set; }
        public string ProfileImage { get; set; }
        public string ColorCode { get; set; }
        public string SubscriptionPeriodTypeIDs { get; set; }
        public Int32 DefaultPeriodTypeID { get; set; }
        public CompanyAccessRights  CompanyAccessRights { get; set; }
        public Int32 DataSubscriptionTypeID { get; set; }
     

    }

    public class MenuMaster
    {
        public Int32 MenuID { get; set; }
        public string MenuName { get; set; }
        public string MenuShortName { get; set; }
        public bool Active { get; set; }
    }

    public class IVM_ContactInfo
    {
        public Double Phone { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string Address { get; set; }
      
    }

    public class SubscriptionType_IVM
    {
        public int SubscriptionTypeID { get; set; }
        public string SubscriptionName { get; set; }
        public int active { get; set; }
        public int SubscriptionPeriod { get; set; }
        public string Gelocation { get; set; }
        public bool ViewDoctorData { get; set; }
        public int NoOfDoctor { get; set; }
        public bool ViewChemistData { get; set; }
        public int NoofChemist { get; set; }
        public bool ViewHospitalData { get; set; }
        public int NoOfHospital { get; set; }
        public string ColorCode { get; set; }
    }

    public class SubscriptionType_OVM
    {
        public int SubscriptionTypeID { get; set; }
        public string SubscriptionName { get; set; }
        public int active { get; set; }
        public int SubscriptionPeriod { get; set; }
        public string Gelocation { get; set; }
        public bool ViewDoctorData { get; set; }
        public int NoOfDoctor { get; set; }
        public bool ViewChemistData { get; set; }
        public int NoofChemist { get; set; }
        public bool ViewHospitalData { get; set; }
        public int NoOfHospital { get; set; }
        public string ColorCode { get; set; }
    }

    public class BulkProductMapping_IVM
    {
        public Int32 CompanyID { get; set; }
        public Int32 DivisionID { get; set; }
        public IList<ProductMapping_IVM> Productmapping { get; set; }
    }

    public class BulkProductMapping_OVM
    {
        public Int32 PFCID { get; set; }
        public string Status { get; set; }
    }

    public class ProductMapping_IVM
    {
        public Int32 PFCID { get; set; }
        public int IsFocus { get; set; }
        public int IsNew { get; set; }
        public string BIDivisionName { get; set; }


    }

    public class PatchData
    {
        public Int32 PatchID { get; set; }
        public string PatchName { get; set; }
        public IList<SalesByPatch> PatchDataDetails { get; set; }
    }
   
    public class SalesByPatch
    {
        public string key { get; set; }
        public string ColorCode { get; set; }
        public IList<ValueDataSalesPatch> values { get; set; }
    }

    public class UnitByPatch
    {
        public string key { get; set; }
        public string ColorCode { get; set; }
        public IList<ValueDataSalesPatch> values { get; set; }
    }

    public class ValueDataSalesPatch
    {
        public string x { get; set; }
        public Double y { get; set; }
        public Int32 z { get; set; }
    }

    public class Summary_IVM
    {
        public string SessionID { get; set; }
        public int CompanyID { get; set; }
        public int DivisonID { get; set; }
        public string PeriodType { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public string DataType { get; set; } = "npm"; // for CVM it should be CVM
    }

    public class Summary_OVM
    {
        public IList<CityData> CityMasterData { get; set; }
        public IList<SubscribedCityData> SubscribedCityData { get; set; }
       public IList<CityGraphData> CityGraphDataDetails { get; set; }
     

         
    }
    public class CityData
    {
        public Int32 CityID { get; set; }
        public string CityName { get; set; }
        public decimal Lat { get; set; }
        public decimal longu { get; set; }
    }
    public class CityGraphData
    {
        public Int32 CityID { get; set; }
        public string CityName { get; set; }
        public IList<CompanySalesData> TopCompanySalesDatavalue { get; set; }
        public IList<BrandSalesData> TopBrandSalesDataValue { get; set; }
        public IList<SupergroupSalesData> TopSuperGroupSalesDataValue { get; set; }
        public IList<CompanySalesData> TopCompanySalesDataUnit { get; set; }
        public IList<BrandSalesData> TopBrandSalesDataUnit { get; set; }
        public IList<SupergroupSalesData> TopSuperGroupSalesDataUnit { get; set; }

    }
    public class CompanySalesData
    {
        public Int32 CompanyID { get; set; }
        public string CompanyName { get; set; }
        public Int32 CityID { get; set; }
        public double TotalSales { get; set; }
        public double TotalUnits { get; set; }

    }
    public class BrandSalesData
    {
        public Int32 BrandID { get; set; }
        public string BrandName { get; set; }
        public Int32 CityID { get; set; }
        public double TotalSales { get; set; }
        public double TotalUnits { get; set; }
    }
    public class SupergroupSalesData
    {
        public Int32 SupergroupID { get; set; }
        public string SupergroupName { get; set; }
        public Int32 CityID { get; set; }
        public double TotalSales { get; set; }
        public double TotalUnits { get; set; }
    }
    public class SubscribedCityData
    {
        public Int32 CityID { get; set; }
        public string CityName { get; set; }
        public double TotalSales { get; set; }
        public double TotalUnits { get; set; }
    }



    public class Comparison_IVM
    {
        public int CompanyID { get; set; }
        public int DivisonID { get; set; }
        public int GeographyID { get; set; }
        public string GeographyFilter { get; set; }
        public string PatchIds { get; set; }
        public string PeriodType { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public string ComparisonType { get; set; }
        public string ComparisionValue { get; set; }
        public bool IsViewTen { get; set; }
        public string TCLevel { get; set; }
        public string TCCode { get; set; }
        public bool includeTopSearches { get; set; }
        public string SessionID { get; set; }
        public string DataType { get; set; } = "npm"; // for CVM it should be CVM
        public string SKUIDs { get; set; }

    }

    public class BrandComparison_IVM
    {
        public int CompanyID { get; set; }
        public int DivisonID { get; set; }        
        public string PeriodType { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }       
        public string SessionID { get; set; }
        public string DataType { get; set; } = "npm"; // for CVM it should be CVM
        public int BrandID { get; set; }

    }

    public class CompanyBrandPerformance_IVM
    {
        public int CompanyID { get; set; }
        public int DivisonID { get; set; }
        public int GeographyID { get; set; }
        public string GeographyFilter { get; set; }
        public string PatchIds { get; set; }
        public string PeriodType { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public string SessionID { get; set; }
        public Int32 BrandID { get; set; }
    }

    public class ComparisonTrend_OVM
    {
        public string key { get; set; }
        public ArrayList values { get; set; }

    }

    public class CompanyCluster
    {
        public Int32 ClusterID { get; set; }
        public string ClusterName { get; set; }
        public int CompanyID { get; set; }       
        public string PatchIDs { get; set; }
        public Int32 CityID { get; set; }

    }

    public class SessionData
    {
        public string CompanyColorCodes { get; set; }
        public string BrandColorCodes { get; set; }
        public string SKUColorCodes { get; set; }
        public string CompanyUsedColorCode { get; set; }
        public string BrandUsedColorCode { get; set; }
        public string SKUUsedColorCode { get; set; }
    }

    public class CompanyDetails
    {
        public Int32 CompanyID { get; set; }
        public string CompanyName { get; set; }
    }
    public class BrandDetails
    {
        public Int32 BrandID { get; set; }
        public string Brand { get; set; }
        public string CompanyName { get; set; }
    }

    public class CompanySubscriptionDetails
    {
        public IList<cityMaster> CityList { get; set; }
        public IList<TherapyMaster> TherapyList { get; set; }
        public bool IsAllAccess { get; set; }
    }

    public class TableList
    {
        public Int32 Rank { get; set; }
        public Int32 ID { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public IList<MonthData> MonthData { get; set; }
        public double TotalValue { get; set; }

    }
    public class TableListBrand
    {
        public Int32 Rank { get; set; }
        public Int32 ID { get; set; }
        public string Brand { get; set; }
        public IList<MonthData> MonthData { get; set; }
        public double TotalValue { get; set; }

    }
    public class MonthData
    {
        public string MonthName { get; set; }
        public double Data { get; set; }

    }



}
