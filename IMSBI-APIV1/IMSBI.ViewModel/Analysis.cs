﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class Analysis_IVM
    {
        public int CompanyID { get; set; }
        public int DivisonID { get; set; }
        public int GeographyID { get; set; }
        public string GeographyFilter { get; set; }
        public string PatchIds { get; set; }
        public string PeriodType { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public string CategoryType { get; set; }
        public string CategoryValue { get; set; }
        public bool includeTopSearches { get; set; }
        public string SessionID { get; set; }

    }

    public class AnalysisMarketShare_IVM
    {
        public int CompanyID { get; set; }
        public int DivisonID { get; set; }
        public int GeographyID { get; set; }
        public string GeographyFilter { get; set; }
        public string PatchIds { get; set; }
        public string PeriodType { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public string CategoryType { get; set; }
        public string CategoryValue { get; set; }
        public int TCCode { get; set; }
        public string TCValue { get; set; }
        public bool includeTopSearches { get; set; }
        public string SessionID { get; set; }

    }


    public class AnalysisMarketShare_OVM
    {
        public string key { get; set; }
        public string ColorCode { get; set; }
        public ArrayList values { get; set; }

    }
    public class AnalysisMarketShareByCode_OVM
    {
        public string Therpaycode { get; set; }    
        public IList<AnalysisMarketShare_OVM> GraphData { get; set; }

    }

    public class MarketShareGraphData
    {
        public IList<AnalysisMarketShare_OVM> AnalysisMarketShareGraphData { get; set; }

        public IList<AnalysisMarketShare_OVM> AnalysisMarketShareGraphDataUnit { get; set; }

        public IList<MarketShareProductList> AnalysisMarketShareProductList { get; set; }
        public IList<TherapyList> TherapyList { get; set; }
        public double TotalSalesAVGMonth { get; set; }
        public string PopularTCCategory { get; set; }
        public decimal TotalTCAVGMonth { get; set; }
    }

    public class TherapyList
    {
        public string TherapyID { get; set; }
        public string TherapyName { get; set; }
    }

    public class MarketShare_OVM
    {
        public IList<AnalysisMarketShare_OVM> GraphMarketShare { get; set; }
        public IList<AnlaysisMarketProductInfo> ProductList { get; set; }
    }

    public class CompanyProductBrandreport_OVM
    {
        public IList<BrandTimeperiodData> BrandSalesValue { get; set; }
        public IList<BrandTimeperiodData> BrandSalesUnit { get; set; }
        public IList<BrandTimeperiodData> BrandCVMSalesValue { get; set; }
        public IList<BrandTimeperiodData> BrandCVMSalesUnit { get; set; }
        public IList<BrandTableDetails> BrandTableValue { get; set; }
        public IList<BrandTableDetails> BrandTableUnit { get; set; }
    }
    public class BrandTableDetails
    {
        public Int64 BrandID { get; set; }
        public string BrandName { get; set; }
        public IList<BrandToBrandList> TableData { get; set; }
    }
    public class BrandToBrandList
    {
        public Int64 BrandID { get; set; }
        public Int32 Rank { get; set; }
        public string BrandName { get; set; }
        public string CompanyName { get; set; }
        public IList<MonthData> MonthData { get; set; }
        public double TotalValue { get; set; }

    }
    public class BrandTimeperiodData
    {
        public string BrandName { get; set; }
        public Int64 BrandID { get; set; }
        public MultiBarChart graphData { get; set; }

    }

    public class MultiBarChart
    {
        public dataClass data { get; set; }
        public bardata bar { get; set; }
        public legnddata legend { get; set; }
        public colorData color { get; set; }
        public axisdata axis { get; set; }
    }
    public class legnddata
    {
        public bool show = true;
    }
    public class bardata
    {
        public widthdata width { get; set; }
    }
    public class widthdata
    {
        public double ratio { get; set; } = 0.7;
    }

    public class dataClass
    {
        public string x { get; set; } = "x";
        public ArrayList columns { get; set; }
        public string type { get; set; } = "bar";
        public bool labels { get; set; } = true;

    }
    public class colorData
    {
        public ArrayList pattern { get; set; }
    }
    public class axisdata
    {
        public bool rotated { get; set; } = false;
        public xAxisdata x { get; set; }
        public yaxisdata y { get; set; }
    }
    public class xAxisdata
    {
        public string type { get; set; } = "category";
    }
    public class yaxisdata
    {
        public string label { get; set; }
    }

    public class Analysis_OVM
    {
        public IList<BarChartData> BarChartSales { get; set; }
        public IList<BarChartData> BarChartUnit { get; set; }
        public IList<AnlaysisProductList> GetAnalysisProductList { get; set; }
        public IList<AnlaysisProductList> GetAnalysisProductListUnit { get; set; }
        public IList<Patch> PatchList { get; set; }

        public IList<PatchData> AnalysisComparisonByPatchSales { get; set; }
        public IList<PatchData> AnalysisComparisonByPatchUnit { get; set; }

        public IList<BrandPatchDetailsTableList> AnalysisComparisonSalesPatchTable { get; set; }
        public IList<BrandPatchDetailsTableList> AnalysisComparisonUnitPatchTable { get; set; }

        public IList<SalesByPatch> AnalysisComparisonSales { get; set; }
        public IList<SalesByPatch> AnalysisComparisonUnit { get; set; }

    }
    public class AnalysisProductListPatch
    {
        public Int32 PatchID { get; set; }
        public IList<AnlaysisProductList> ProductList { get; set; }
    }

    public class Patch
    {
        public Int32 PatchID { get; set; }
        public string PatchName { get; set; }
    }

    public class BarChartSales
    {
        public Int64 Sales { get; set; }
        public string TimePeriod { get; set; }
        public Int64 UnitCount { get; set; }
        public Int32 TimePeriodID { get; set; }
    }

    public class BarChartData
    {
        public string key { get; set; }
        public string ColorCode { get; set; }
        public ArrayList values { get; set; }
    }

    public class GroupChartSalesPatch
    {
        public Int32 PatchID { get; set; }
        public GraphDataSales GroupChartSales {get;set;}
        public GraphDataUnit GroupChartUnit { get; set; }
    }

    public class GroupChartSales
    {
        public double Sales { get; set; }
        public double Unit { get; set; }
        public string TimePeriod { get; set; }
        public string ProductName { get; set; }
        public int BrickID { get; set; }
        public Int64 BrandID { get; set; }
        public string BrandName { get; set; }
        public int TimePeriodID { get; set; }
        public string BrickName { get; set; }

    }
    public class ProductDetails
    {
        public string ProductName { get; set; }
        public Int64 ProductID { get; set; }
    }

    public class TcLevelMarket
    {
        public  string TCLevel { get; set; }
        public double MarketValue { get; set; }
        public double MarketUnit { get; set; }
        
    }

    public class GroupChartUnit
    {
        public double Unit { get; set; }
        public string TimePeriod { get; set; }
        public string ProductName { get; set; }
    }

    public class AnlaysisProductInfo
    {
        public Int64 Prod_Code { get; set; }
        public int Rank { get; set; }
        public string Brand { get; set; }
        public Int64 TotalUnit { get; set; }
        public double TotalSale { get; set; }
        public Int32 TimePeriodID { get; set; }
        public double MarketShare { get; set; }
        public double TotalMarketUnit { get; set; }
        public Int32 BrickID { get; set; }
        public string PeriodName { get; set; }
        public string BrickName { get; set; }
    }

    public class AnlaysisProductList
    {
        public Int64 Prod_Code { get; set; }
        public int Rank { get; set; }
        public string Brand { get; set; }
        public IList<MonthwiseSale> MonthData { get; set; }
        public double TotalSale { get; set; }
    }

    public class BrandBarChartData
    {
        public string TimePeriod { get; set; }
        public double SalesValue { get; set; }
        public double Unit { get; set; }
        public Int32 TimePeriodID { get; set; }

    }


    public class MonthwiseSale
    {
        public string MonthName { get; set; }
        public double Value { get; set; }
        public double Unit { get; set; }
    }

    public class AnlaysisMarketProductInfo
    {
       
        public int Rank { get; set; }
        public string ProductName { get; set; }
        public Int64 TotalUnit { get; set; }
        public decimal TotalSale { get; set; }
        public decimal MarketShare { get; set; }

    }

    public class GraphDataSales
    {
        public string key { get; set; }
        public string ColorCode { get; set; }
        public IList<ValueDataSales> values { get; set; }
    }

    public class GraphDataUnit
    {
        public string key { get; set; }
        public string ColorCode { get; set; }
        public IList<ValueDataUnit> values { get; set; }
    }

    public class ValueDataSales
    {
        public string x { get; set; }
        public Double y { get; set; }
    }

    public class ValueDataUnit
    {
        public string x { get; set; }
        public Double y { get; set; }
    }

    public class MarketShareData
    {
        public string TimePeriod { get; set; }
        public int TimePeriodID { get; set; }
        public string MonthName { get; set; }
        public int Year { get; set; }
        public Int64 NoOfUnit { get; set; }
        public Double TotalSale { get; set; }
        public string TherapyCode { get; set; }
        public string TherapyName { get; set; }
        public string ProductName { get; set; }
        public string TCCode { get; set; }

    }

    public class MarketShareProductList
    {
        public Int64 BrandID { get; set; }   
        public string Brand { get; set; }
        public int Rank { get; set; }
        public Int64 NoOfUnit { get; set; }
        public Double TotalSale { get; set; }
        public decimal MarketShare { get; set; } 
        public double MarketValue { get; set; }
        public double MarketUnit { get; set; }
        public decimal MarketUnitShare { get; set; }
        public string TCCode { get; set; }
      

    }

    public class MonthlySale
    {
        public Int32 TimePeriodID { get; set; }
        public Double Totalsale { get; set; }
        public Double TotalUnit { get; set; }
    }

    public class TCLevelSales
    {
        public string TCCode { get; set; }
        public decimal TotalSale { get; set; }
    }

    

    public class TimePeriodData
    {
        public int TimePeriodID { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
         
    }

  
}

