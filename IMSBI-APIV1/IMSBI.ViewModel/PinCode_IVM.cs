﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.ViewModel
{
    public class PinCityState_OVM
    {
        public Int32 PinCode { get; set; }
        public string CityName { get; set; }
        public Int32 CityID { get; set; }
        public string StateName { get; set; }
        public Int32 StateID { get; set; }
        public string CountryName { get; set; }
        public Int32 CountryID { get; set; }

    }
}
