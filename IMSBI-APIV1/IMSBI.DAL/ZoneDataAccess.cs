﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class ZoneDataAccess
    {
        public IList<ZoneMaster> GetZoneListByCompany(int CompanyID)
        {
            IList<ZoneMaster> ZoneList = new List<ZoneMaster>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetZoneByCompany + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new ZoneMaster
                              {
                                  ZoneID = Convert.ToInt32(row["ZoneID"]),
                                  ZoneName = Convert.ToString(row["ZoneName"]),

                              }
                             ).ToList();

                ZoneList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ZoneList;
        }
    }
}
