﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class DALUtility
    {
        private const int SQL_LOCK_TIMEOUT_DEFAULT = 0;
        private const string SQL_LOCK_DEFAULT_STMT = "SET LOCK_TIMEOUT {0}";

        public void Fill(SqlCommand cmd, DataSet ds)
        {
            SqlConnection con = null;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                if (da.SelectCommand.Connection == null)
                {
                    con = GetConnection();

                    da.SelectCommand.Connection = con;
                }

                da.Fill(ds);
            }
            catch (SqlException odbe)
            {
                throw odbe;
            }
            finally
            {
                cmd = null;
                if (con != null) { con.Close(); con = null; }
            }
        }
        public void Fill(SqlCommand cmd, DataSet ds, SqlConnection con)
        {

            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                if (da.SelectCommand.Connection == null)
                {


                    da.SelectCommand.Connection = con;
                }

                da.Fill(ds);
            }
            catch (SqlException odbe)
            {
                throw odbe;
            }
            finally
            {
                cmd = null;

            }
        }

        public void Fill(SqlCommand cmd, DataTable dt)
        {
            SqlConnection con = null;
            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                if (da.SelectCommand.Connection == null)
                {
                    con = GetConnection();

                    da.SelectCommand.Connection = con;
                }

                da.Fill(dt);
            }
            catch (SqlException odbe)
            {
                throw odbe;
            }
            finally
            {
                cmd = null;
                if (con != null) { con.Close(); con = null; }
            }
        }

        public void Fill(SqlCommand cmd, DataTable dt, SqlConnection con)
        {

            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                if (da.SelectCommand.Connection == null)
                {


                    da.SelectCommand.Connection = con;
                }

                da.Fill(dt);
            }
            catch (SqlException odbe)
            {
                throw odbe;
            }
            finally
            {
                cmd = null;

            }
        }

        
        public int ExecuteNonQuery(SqlCommand cmd)
        {
            int iCntAffectedRows = 0;
            SqlConnection con = null;

            try
            {
                if (cmd.Connection == null)
                {
                    con = GetConnection();
                    cmd.Connection = con;
                }

                iCntAffectedRows = cmd.ExecuteNonQuery();
            }
            catch (SqlException odbe)
            {
                throw odbe;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }

            return iCntAffectedRows;
        }

        public int ExecuteNonQuery(SqlCommand cmd, SqlConnection con)
        {
            int iCntAffectedRows = 0;


            try
            {
                if (cmd.Connection == null)
                {

                    cmd.Connection = con;
                }

                iCntAffectedRows = cmd.ExecuteNonQuery();
            }
            catch (SqlException odbe)
            {
                throw odbe;
            }


            return iCntAffectedRows;
        }

        public bool Execute(params String[] query)
        {
            SqlCommand cmd = new SqlCommand();
            SqlTransaction trn;
            SqlConnection con = GetConnection();
            Int32 sqlNO = 0;
            Int32 sqlTrn = 0;
            trn = con.BeginTransaction();
            cmd.Connection = con;
            cmd.Transaction = trn;
            sqlNO = query.Length;
            try
            {
                while (sqlTrn < sqlNO)
                {
                    cmd.CommandText = query.GetValue(sqlTrn).ToString();
                    cmd.ExecuteNonQuery();
                    sqlTrn += 1;
                }
                trn.Commit();
                cmd = null;
                con.Close();
                return true;
            }
            catch (Exception ex)
            {

                trn.Rollback();

                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }

        }

        public bool Execute(ref SqlConnection con, ref SqlTransaction trn, params String[] query)
        {
            SqlCommand cmd = new SqlCommand();


            Int32 sqlNO = 0;
            Int32 sqlTrn = 0;

            cmd.Connection = con;
            cmd.Transaction = trn;
            sqlNO = query.Length;
            try
            {
                while (sqlTrn < sqlNO)
                {
                    cmd.CommandText = query.GetValue(sqlTrn).ToString();
                    cmd.ExecuteNonQuery();
                    sqlTrn += 1;
                }

                cmd = null;

                return true;
            }
            catch (Exception ex)
            {


                throw ex;
            }

        }

        public bool ExecuteDS(DataSet ds)
        {
            SqlConnection con = GetConnection();
            SqlTransaction trn;
            trn = con.BeginTransaction();

            try
            {
                foreach (DataTable dt in ds.Tables)
                {
                    string strSelectedColumns = "";
                    string strTablename = dt.TableName;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        strSelectedColumns = strSelectedColumns + "," + dc.ColumnName;
                    }
                    strSelectedColumns = strSelectedColumns.Substring(1);
                    string strSelect = "SELECT " + strSelectedColumns + " FROM " + strTablename + " WHERE 1=0";

                    SqlDataAdapter da = new SqlDataAdapter(strSelect, con);
                    da.SelectCommand.Transaction = trn;
                    SqlCommandBuilder scb = new SqlCommandBuilder(da);
                    da = scb.DataAdapter;
                    da.Update(ds, strTablename);
                }
                trn.Commit();
                return true;
            }
            catch (Exception ex)
            {
                trn.Rollback();

                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }
        }

        public bool ExecuteDS(DataSet ds, ref SqlConnection con, ref SqlTransaction trn)
        {


            try
            {
                foreach (DataTable dt in ds.Tables)
                {
                    string strSelectedColumns = "";
                    string strTablename = dt.TableName;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        strSelectedColumns = strSelectedColumns + "," + dc.ColumnName;
                    }
                    strSelectedColumns = strSelectedColumns.Substring(1);
                    string strSelect = "SELECT " + strSelectedColumns + " FROM " + strTablename + " WHERE 1=0";

                    SqlDataAdapter da = new SqlDataAdapter(strSelect, con);
                    da.SelectCommand.Transaction = trn;
                    SqlCommandBuilder scb = new SqlCommandBuilder(da);
                    da = scb.DataAdapter;
                    da.Update(ds, strTablename);
                }

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataSet ReturnDataset(string strQry)
        {
            SqlConnection con = GetConnection();
            DataSet ds = new DataSet();
            try
            {


                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0; 
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }
        }

        public DataSet ReturnDataset(string strQry, SqlConnection con)
        {

            DataSet ds = new DataSet();
            try
            {


                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataTable ReturnDataTable(string strQry)
        {
            SqlConnection con = GetConnection();
            DataTable dt = new DataTable();
            try
            {


                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }
        }
        //added by soujanya
        public DataTable ReturnDataTable_Params(string strQry, string condition)
        {
            SqlConnection con = GetConnection();
            DataTable dt = new DataTable();
            strQry = strQry + " " + condition;
            try
            {


                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }
        }

        public DataTable ReturnDataTable(string strQry, SqlConnection con)
        {

            DataTable dt = new DataTable();
            try
            {


                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public object ReturnValue(string strQry)
        {
            SqlConnection con = GetConnection();
            object objResult;
            try
            {



                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandType = CommandType.Text;
                objResult = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }
            return objResult;
        }

        public object ReturnValue(string strQry, SqlConnection con)
        {

            object objResult;
            try
            {



                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.CommandType = CommandType.Text;
                objResult = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objResult;
        }
        public object ReturnValue(string strQry, ref SqlConnection con, ref SqlTransaction trn)
        {

            object objResult;
            try
            {



                SqlCommand cmd = new SqlCommand(strQry, con);
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.Text;
                objResult = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objResult;
        }

        public DataTable GetPrimaryKey(string strTablename)
        {
            return ReturnDataTable("exec ctpl_sp_getprimarykey '" + strTablename + "'");
        }

        public DataSet ReturnListQryResult(string strLstQry, string condition)
        {
            SqlConnection con = GetConnection();
            DataSet ds = new DataSet();
            try
            {


                SqlDataAdapter da;
                SqlCommand cmd = new SqlCommand("Exec SP_GetLstQry '" + strLstQry + "'", con);
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                StringBuilder qry = new StringBuilder();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["Query"].ToString().Trim().Substring(0, 6).ToUpper() == "SELECT")
                    {
                        qry.Append(dr["Query"].ToString() + " where 1=1 ");
                        if (dr["Where_Clause"].ToString().Trim() != "")
                        {
                            qry.Append(" and " + dr["Where_Clause"].ToString().Trim());
                        }
                        if (condition.Trim() != "")
                        {
                            qry.Append(" and " + condition.Trim());
                        }

                        if (dr["GroupBy_Clause"].ToString().Trim() != "")
                        {
                            qry.Append(" Group By " + dr["GroupBy_Clause"].ToString().Trim());
                        }
                        if (dr["OrderBy_Clause"].ToString().Trim() != "")
                        {
                            qry.Append(" Order By " + dr["OrderBy_Clause"].ToString().Trim());
                        }
                    }
                    else
                    {
                        qry.Append("Exec " + dr["Query"].ToString() + " " + condition.Trim());

                    }

                }
                DataSet dsReturn = new DataSet();
                cmd = new SqlCommand(qry.ToString(), con);
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(dsReturn);
                return dsReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null) { con.Close(); con = null; }
            }
        }


        public SqlConnection GetConnection()
        {
            ConnectionStringSettings mySetting = ConfigurationManager.ConnectionStrings["SNABIConnection"];
            if (mySetting == null || string.IsNullOrEmpty(mySetting.ConnectionString))
                throw new Exception("Fatal error: missing connecting string in web.config file");

            return GetConnection(mySetting.ConnectionString);
        }

        public SqlConnection GetConnection(String dbConn)
        {

            SqlConnection con = null;

            try
            {
                con = new SqlConnection(dbConn);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                return con;
            }
            catch (SqlException odbe)
            {
                con = null;
                throw odbe;
            }

        }

        public string GetConnectionString(string servername, string userid, string pwd, string dbname, bool trustedconnection)
        {
            string connectionstring = "";
            try
            {
                if (trustedconnection)
                {
                    connectionstring = "Data Source=" + servername + ";Initial Catalog=" + dbname + ";Trusted_connection=true;";
                }
                else
                {
                    connectionstring = "Data Source=" + servername + ";Initial Catalog=" + dbname + ";uid=" + userid + ";pwd=" + pwd + ";Trusted_connection=false;";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return connectionstring;
        }
        private string GetDefaultConString()
        {
            string connectionstring = "";
            try
            {

                String sServer, sDataBase, sUid, sPwd, sTrustedConnection;
                DataSet ds = new DataSet();

                ds.ReadXml(AppDomain.CurrentDomain.BaseDirectory + "\\connection");

                sServer = ds.Tables["SQLConnection"].Rows[0]["SERVER"].ToString();
                sDataBase = ds.Tables["SQLConnection"].Rows[0]["DATABASE"].ToString();
                sUid = ds.Tables["SQLConnection"].Rows[0]["USER_NAME"].ToString();
                sPwd = ds.Tables["SQLConnection"].Rows[0]["PASSWORD"].ToString();
                sTrustedConnection = ds.Tables["SQLConnection"].Rows[0]["TrustedConnection"].ToString();


                connectionstring = GetConnectionString(sServer, sUid, sPwd, sDataBase, Convert.ToBoolean(sTrustedConnection));

            }
            catch (Exception ex)
            {
                throw ex;

            }
            return connectionstring;
        }



        public string GetAppPath()
        {

            int i;
            string strAppPath;
            strAppPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            i = strAppPath.Length - 1;

            while (strAppPath.Substring(i, 1) == "\\")
            {
                i = i - 1;
            }

            strAppPath = strAppPath.Substring(0, i);
            return strAppPath;

        }


    }
}
