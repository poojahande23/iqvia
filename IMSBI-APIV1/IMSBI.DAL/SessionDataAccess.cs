﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class SessionDataAccess
    {
        public int SetSession(string sessionID, int UserID)
        {
            int Status = 0;
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateSession + " '" + sessionID +"',"+ UserID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToInt32(obj);
                IMSBIDB = null;
            }
            catch
            {
                throw;
            }
            return Status;
        }
       
        

        public int ValidateSession(string sessionID)
        {
            int UserID = 0;
            try
            {
                SessionInfo objSessionInfo = new SessionInfo();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSession + " '" + sessionID + "'";
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SessionInfo
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  SessionID = Convert.ToString(row["SessionID"]),
                                  SessionDate = Convert.ToDateTime(row["CreatedDate"])
                                


                              }
                             ).FirstOrDefault();

                objSessionInfo = result;

                if (objSessionInfo != null)
                {
                    if (objSessionInfo.SessionDate < DateTime.Now.AddHours(-12))
                    {
                        this.DeleteSession(sessionID);
                    }
                    else
                    {
                        UserID = objSessionInfo.UserID;
                    }
                }
                
                IMSBIDB = null;
            }
            catch
            {
                throw;

            }
            return UserID;
        }

        public int DeleteSession(string sessionID)
        {
            int Status = 0;
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteSession + " '" + sessionID + "'";
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToInt32(obj);
                IMSBIDB = null;
            }
            catch
            {
                throw;
            }
            return Status;
        }

    }
}
