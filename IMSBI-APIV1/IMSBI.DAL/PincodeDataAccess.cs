﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class PincodeDataAccess
    {
        public List<Int32> GetPinCodeList(int PinCode)
        {
            List<Int32> PincodeList = new List<int>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetPincodeList + " " + PinCode;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select Convert.ToInt32(row["Pincode"])

                              ).ToList();

                PincodeList = result;
            }
            catch
            {
                throw;
            }
            return PincodeList;
        }


        public PinCityState_OVM GetCityStateByPinCode(int PinCode)
        {
            PinCityState_OVM PinCodeCity_OVM = new PinCityState_OVM();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCityStateByPinCode + " " + PinCode;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new PinCityState_OVM
                              {
                                  PinCode = Convert.ToInt32(row["Pincode"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  StateName = Convert.ToString(row["StateName"]),
                                  CountryID = Convert.ToInt32(row["CountryID"]),
                                  StateID = Convert.ToInt32(row["StateID"]),
                                  CountryName = Convert.ToString(row["CountryName"])


                              }
                             ).FirstOrDefault();

                PinCodeCity_OVM = result;
            }
            catch
            {

            }
            return PinCodeCity_OVM;
        }

        public IList<Int32> GetPinCodeListToCreatePatch(Int32 CompanyID, Int32 CityID, Int32 DivisionID)
        {
            List<Int32> PincodeList = new List<int>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetPinCodeListToCreatePath + " " + CompanyID + "," + DivisionID + "," + CityID;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select Convert.ToInt32(row["Pincode"])

                              ).ToList();

                PincodeList = result;
            }
            catch
            {
                throw;
            }
            return PincodeList;
        }
    }
    }
