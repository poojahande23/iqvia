﻿using IMSBI.Common;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class UserDataAccess
    {
        public UserInfo UserLogin(string UserName, string Password)
        {
            UserInfo objUserInfo = null;
            try
            {
                IList<string> CityNameList = new List<string>();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_Userlogin + " '" + UserName + "','"+ CryptorEngine.Encrypt(Password,true) + "'" ;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new UserInfo
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  RoleID = Convert.ToInt32(row["RoleID"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  CompanyRoleName = Convert.ToString(row["RoleName"]),
                                  CompanyRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  LogoFileName = Convert.ToString(row["LogoImageName"]),
                                  ProfileImage = Convert.ToString(row["ProfileImage"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  DivisionName = Convert.ToString(row["DivisionName"]),
                                  SubscriptionEndDate = !string.IsNullOrEmpty(Convert.ToString(row["SubscriptionEndDate"])) ? Convert.ToDateTime(row["SubscriptionEndDate"]) : DateTime.Now,
                                  SubscriptionStartDate = !string.IsNullOrEmpty(Convert.ToString(row["SubscriptionStartDate"])) ? Convert.ToDateTime(row["SubscriptionStartDate"]) : DateTime.Now,
                                 Active = Convert.ToBoolean(row["Active"]),
                                  CompanyStatus = !string.IsNullOrEmpty(Convert.ToString(row["CompanyStatus"]))? Convert.ToBoolean(row["CompanyStatus"]):false

                              }                            
                              ).FirstOrDefault();

                objUserInfo = result;
              


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objUserInfo;
        }


        public UserInfo UserInfoDetails(int UserID)
        {
            UserInfo objUserInfo = null;
            try
            {
                IList<string> CityNameList = new List<string>();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetUserInfoDetails + " "+ UserID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new UserInfo
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  RoleID = Convert.ToInt32(row["RoleID"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  CompanyRoleName = Convert.ToString(row["RoleName"]),
                                  CompanyRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  LogoFileName = Convert.ToString(row["LogoImageName"]),
                                  ProfileImage = Convert.ToString(row["ProfileImage"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  DivisionName = Convert.ToString(row["DivisionName"]),
                                  SubscriptionEndDate = !string.IsNullOrEmpty(Convert.ToString(row["SubscriptionEndDate"])) ? Convert.ToDateTime(row["SubscriptionEndDate"]) : DateTime.Now,
                                  SubscriptionStartDate = !string.IsNullOrEmpty(Convert.ToString(row["SubscriptionStartDate"])) ? Convert.ToDateTime(row["SubscriptionStartDate"]) : DateTime.Now,

                                  CompanyStatus = !string.IsNullOrEmpty(Convert.ToString(row["CompanyStatus"])) ? Convert.ToBoolean(row["CompanyStatus"]) : false

                              }
                              ).FirstOrDefault();

                objUserInfo = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objUserInfo;
        }

        public int AddUpdateUser(User_IVM objUser)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {

                if (objUser.UserID == null)
                {
                    objUser.UserID = 0;
                }
                if (objUser != null)
                {
                    param = objUser.UserID + ",";
                    param += "'" + objUser.UserName + "',";
                    param += "'" + CryptorEngine.Encrypt(objUser.Password, true) + "',";
                    param += "'" + objUser.Email + "',";
                    param += objUser.MobileNo + ",";
                    param += objUser.DivisionID + ",";
                    param += objUser.RoleID + ",";
                    param += objUser.Pincode + ",";
                    param += objUser.Active + ",";
                    param += objUser.CompanyID + ",";
                    param += objUser.CompanyRoleID + ",";
                    param += objUser.ReportingManagerID + ",";
                    param += "'" + objUser.ProfileImage + "'";


                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateUser + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);

               // Update the User Access Rights
               if(result > 0 && objUser.UserAccessRights != null)
                {
                    AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                    objAccessRightsDataAccess.CreateUpdateUserAccessRights(result, objUser.UserAccessRights.AccessRightType, objUser.UserAccessRights.TCValues, objUser.UserAccessRights.CityIDs);
                    objAccessRightsDataAccess = null;
                }

               
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627) // <-- but this will
                {
                    result = -1;
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

        public int AddUpdateCompanyUserRole(CompanyUserRole_IVM objCompanyUserRole)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {

               
                if (objCompanyUserRole != null)
                {
                    param += objCompanyUserRole.CompanyUserRoleID + ",";
                    param += objCompanyUserRole.CompanyID + ",";
                    param += objCompanyUserRole.DivisionID + ",";
                    param += "'" + objCompanyUserRole.RoleName + "',";
                    param += objCompanyUserRole.ParentRoleID + "";
                  

                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateCompanyRole + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);

                if (result > 0)
                {
                    // Process User Role Permission.
                    if(objCompanyUserRole.permissions != null)
                    {
                        for(int i =0; i< objCompanyUserRole.permissions.Count; i++)
                        {
                            // Process Indivual Permission.
                            string strpermission = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateCompanyRolePermission + " " + result + ","+ objCompanyUserRole.permissions[i].ApplicationID +","+ objCompanyUserRole.permissions[i].Permission;
                             IMSBIDB.ReturnValue(strpermission);

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627) // <-- but this will
                {
                    result = -1;
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }



        public string ChangePassword(ChangePassword_IVM ObjUserInfo)
        {
            string ChangedStatus = string.Empty;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (ObjUserInfo != null)
                {
                    param += ObjUserInfo.UserID + ",";
                    param += "'" + CryptorEngine.Encrypt(ObjUserInfo.OldPassword, true) + "',";
                    param += "'" + CryptorEngine.Encrypt(ObjUserInfo.NewPassword, true) + "',";
                    param += ObjUserInfo.isAdmin;
                  


                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ChangeUserPassword + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                ChangedStatus = Convert.ToString(obj);
               
            }
            catch
            {
                throw;
            }
            return ChangedStatus;
        }

        public SessionData GetSessionData (string SessionID)
        {
            SessionData objSessionData = new SessionData();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSessionData + " '" + SessionID + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SessionData
                              {
                                  CompanyColorCodes = Convert.ToString(row["CompanyColorCodes"]),
                                  BrandColorCodes = Convert.ToString(row["BrandColorCodes"]),
                                  SKUColorCodes = Convert.ToString(row["SkuColorCodes"]),
                                  CompanyUsedColorCode = Convert.ToString(row["CompanyUsedColorCode"]),
                                  BrandUsedColorCode = Convert.ToString(row["BrandUsedColorCode"]),
                                  SKUUsedColorCode = Convert.ToString(row["SkuUsedColorCode"]),

                              }
                              ).FirstOrDefault();

                objSessionData = result;

            }
            catch
                {
                throw;
            }

            return objSessionData;
        }


        public bool UpdateSessionData(string SessionID, SessionData ObjSessionData)
        {
            bool status = false;
            try
            {
                int result = 0;
                DALUtility IMSBIDB = new DALUtility();
                var param = string.Empty;
                

                    param += "'"+ SessionID + "',";
                    param += "'" + ObjSessionData.CompanyColorCodes + "',";
                    param += "'" + ObjSessionData.BrandColorCodes + "',";
                    param += "'" + ObjSessionData.SKUColorCodes + "',";
                    param += "'" + ObjSessionData.CompanyUsedColorCode + "',";
                    param += "'" + ObjSessionData.BrandUsedColorCode + "',";
                    param += "'" + ObjSessionData.SKUUsedColorCode + "'";

                    



                    string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_UpdateSessionData + " " + param;
                    var obj = IMSBIDB.ReturnValue(str);
                    result = Convert.ToInt32(obj);


                }
                catch
            {
                throw;
            }

            return status;
        }
    }
}
