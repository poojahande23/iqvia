﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class BrickDataAccess
    {
        public IList<BrickMaster> GetBrickListByCityID(int GeographyID, int CompanyID, int DivisionID, string PatchIDs)
        {
            IList<BrickMaster> BrickList = new List<BrickMaster>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetBricksByPatch + " " + GeographyID + "," + CompanyID + "," + DivisionID + ",'"+ PatchIDs + "'";
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrickMaster
                              {
                                  BrickID = Convert.ToInt32(row["BrickID"]),
                                  BrickName = Convert.ToString(row["BrickName"]),
                                  lat = (!string.IsNullOrEmpty(Convert.ToString(row["Lat"])))? Convert.ToDecimal(row["Lat"]): 0,
                                  lng = (!string.IsNullOrEmpty(Convert.ToString(row["Lat"]))) ? Convert.ToDecimal(row["Long"]) : 0

                              }
                              ).ToList();

                BrickList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

           return  BrickList;

        }

        public Int32 CreateUpdateCustomPatch(BrickMaster objBrickMaster)
        {
            int BrickID = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {

                if (objBrickMaster.BrickID == null)
                {
                    objBrickMaster.BrickID = 0;
                }
                if (objBrickMaster != null)
                {
                    param = objBrickMaster.BrickID + ",";
                    param += "'" + objBrickMaster.BrickName + "',";
                  
                    param += objBrickMaster.CityID + ",";
                    param += objBrickMaster.Active + ",";
                    param += objBrickMaster.lat + ",";
                    param += objBrickMaster.lng + ",";
                    param += objBrickMaster.IsCustom + ",";
                    param += objBrickMaster.CompanyID + ",";
                    param += objBrickMaster.DivisionID + ";";
                   


                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateCustomBrick + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                BrickID = Convert.ToInt32(obj);
                this.DeleteBrickPinCodeMapping(BrickID);
                if(!string.IsNullOrEmpty(objBrickMaster.PinCodeIDs))
                {
                    string[] PincodeList = objBrickMaster.PinCodeIDs.Split(',');
                    for(int i=0; i< PincodeList.Length; i++)
                    {
                        this.CreateUpdatePincodeMapping(BrickID, Int32.Parse(PincodeList[i]));
                    }
                }


            }
            catch
            {
                throw;
            }
            return BrickID;
        }

        public bool CreateUpdatePincodeMapping(Int32 BrickID, Int32 Pincode)
        {
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
                param = BrickID + ",";               
                param += Pincode + ";";           

            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateBrickPincodeMapping + " " + param;
            var obj = IMSBIDB.ReturnValue(str);
            return true;
        }


        public bool DeleteBrickPinCodeMapping(Int32 BrickID)
        {
            bool Status = false;
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteBrickPincodemappingbyBrickID + " " + BrickID;
                var obj = IMSBIDB.ReturnValue(str);
                var result = Convert.ToInt32(obj);
                if (result == 1)
                {
                    Status = true;
                }
            }
            catch
            {
                throw;
            }

            return Status;
        }
      
        public IList<Int32> GetBrickPincodeList(int BrickID)
        {
            IList<Int32> Pincodes = new List<Int32>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetBrickPincodeList + " " + BrickID;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select Convert.ToInt32(row["Pincode"])                              
                              ).ToList();

                Pincodes = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Pincodes;

        }


    }
}
