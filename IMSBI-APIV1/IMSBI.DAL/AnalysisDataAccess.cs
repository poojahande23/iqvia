﻿using IMSBI.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class AnalysisDataAccess
    {

        public Analysis_OVM GetAnalysisDashboardData(Analysis_IVM objAnalysis)
        {
            Analysis_OVM AnalysisData = new Analysis_OVM();

            SessionDataAccess objSessionDataAccess = new SessionDataAccess();
            int UserID = objSessionDataAccess.ValidateSession(objAnalysis.SessionID);
            objSessionDataAccess = null;
            if (UserID == 0)
            {
                AnalysisData = null;
                return AnalysisData;
            }

            // Access log
            UserLog_VM objLogDetails = new UserLog_VM()
            {
                UserID = UserID,
                MenuID =2,
                CityID = objAnalysis.GeographyID,
                CategoryType = objAnalysis.CategoryType,
                CategoryValue = objAnalysis.CategoryValue,
                DataType ="npm",
                EndPeriod = objAnalysis.PeriodEnd,
                StartPeriod = objAnalysis.PeriodStart,
                PatchID = objAnalysis.PatchIds,
                PeriodType = objAnalysis.PeriodType      ,
                SessionID = objAnalysis.SessionID          


            };
            UserLogAccessDetails objUserLogAccessDetails = new UserLogAccessDetails();
            objUserLogAccessDetails.AddUpdateUserLog(objLogDetails);
            objUserLogAccessDetails = null;

            // Get UserAccessRights 

            AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
            UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
            
            int AccessType = 1;
            string TCCodes = string.Empty;
            if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
            {
               
                    AccessType = 2;
                    TCCodes = objUserAccessrights.TCValues;
                
            }
            else
            {
                CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objAnalysis.CompanyID);
                if (objCompanyAccessRight != null)
                {
                    switch (objCompanyAccessRight.AccessRightType)
                    {
                        case 2:
                            AccessType = 2;
                            FilterDataAccess objFilterdataAccess = new FilterDataAccess();
                            IList<string> TCCodesList = objFilterdataAccess.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                            objFilterdataAccess = null;
                            TCCodes = string.Join(",", TCCodesList);
                            break;
                        case 3:
                            AccessType = 2;
                            TCCodes = objCompanyAccessRight.TCValues;
                            break;
                    }
                }
            }
            objAccessRightsDataAccess = null;




            try
            {
                var param = string.Empty;
                if (objAnalysis != null)
                {


                    DataSet resultds = new DataSet();
                    DALUtility IMSBIDB = new DALUtility();

                    // Get the list of Patch List for Filter

                    FilterDataAccess objFilterDataAccess = new FilterDataAccess();
                    IList<BrickMaster> objBrickList = new List<BrickMaster>();
                    BrickDataAccess objBrickDataaccess = new BrickDataAccess();

                    objBrickList = objBrickDataaccess.GetBrickListByCityID(objAnalysis.GeographyID, objAnalysis.CompanyID, objAnalysis.DivisonID, objAnalysis.PatchIds);
                    objBrickDataaccess = null;
                    IList<Patch> objtPathcList = new List<Patch>();
                    for (int j = 0; j < objBrickList.Count; j++)
                    {
                        objtPathcList.Add(new Patch
                        {
                            PatchID = objBrickList[j].BrickID,
                            PatchName = objBrickList[j].BrickName
                        });
                    }
                    AnalysisData.PatchList = objtPathcList;
                    switch (objAnalysis.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSalesInitialloadmat + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd;
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_MarketSalesInitialloadmatWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSalesInitialloadmth + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd;
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_MarketSalesInitialloadmthWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSalesInitialloadqtr + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd;
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_MarketSalesInitialloadqtrWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSalesInitialloadhlf + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd;
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_MarketSalesInitialloadhlfWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSalesInitialloadytdf + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd;
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_MarketSalesInitialloadytdfWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                          
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSalesInitialloadytdc + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd;
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_MarketSalesInitialloadytdcWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;

                    }

                    if (resultds != null && resultds.Tables.Count > 0)
                    {
                        var resultBarChartSales = (from row in resultds.Tables[0].AsEnumerable()
                                                   select new BarChartSales
                                                   {
                                                       TimePeriod = Convert.ToString(row["MonthName"]),
                                                       Sales = Convert.ToInt64(row["TotalSale"]),
                                                       UnitCount = Convert.ToInt64(row["NoOfUnit"]),
                                                       TimePeriodID = Convert.ToInt32(row["TimePeriodID"])

                                                   }
                                   ).ToList();




                        IList<BarChartData> BarChartSales = new List<BarChartData>();
                        IList<BarChartData> BarChartUnit = new List<BarChartData>();

                        IList<BarChartData> BarChartSalesFinal = new List<BarChartData>();
                        IList<BarChartData> BarChartUnitFinal = new List<BarChartData>();

                        ArrayList BarChartArraySale = new ArrayList();
                        ArrayList BarChartArrayUnit = new ArrayList();


                        CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                        IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objAnalysis.PeriodType.ToLower(), objAnalysis.PeriodStart, objAnalysis.PeriodEnd);
                        objCompanyDataaccessForTimeperiod = null;
                        for(int i=0; i< TimePeriodlist.Count; i++)
                        {
                            BarChartSales objBarchartData = resultBarChartSales.Where(a => a.TimePeriodID == TimePeriodlist[i].TimePeriodID).FirstOrDefault();
                            if(objBarchartData != null)
                            {
                                ArrayList CurrentObjectSale = new ArrayList();
                                CurrentObjectSale.Add(objBarchartData.TimePeriod);
                                CurrentObjectSale.Add(objBarchartData.Sales);
                                BarChartArraySale.Add(CurrentObjectSale);


                                ArrayList CurrentObjectUnit = new ArrayList();
                                CurrentObjectUnit.Add(objBarchartData.TimePeriod);
                                CurrentObjectUnit.Add(objBarchartData.UnitCount);
                                BarChartArrayUnit.Add(CurrentObjectUnit);
                            }
                            else
                            {
                                ArrayList CurrentObjectSale = new ArrayList();
                                CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                CurrentObjectSale.Add(0);
                                BarChartArraySale.Add(CurrentObjectSale);


                                ArrayList CurrentObjectUnit = new ArrayList();
                                CurrentObjectUnit.Add(TimePeriodlist[i].MonthName);
                                CurrentObjectUnit.Add(0);
                                BarChartArrayUnit.Add(CurrentObjectUnit);
                            }

                        }

                        BarChartData objChartDataSale = new BarChartData();
                        objChartDataSale.key = "All Brands*";
                        objChartDataSale.values = BarChartArraySale;
                        BarChartSales.Add(objChartDataSale);

                        BarChartData objChartDataUnit = new BarChartData();
                        objChartDataUnit.key = "All Brands*";
                        objChartDataUnit.values = BarChartArrayUnit;
                        BarChartUnit.Add(objChartDataUnit);



                        var resultProductBrickList = (from row in resultds.Tables[1].AsEnumerable()
                                                      select new AnlaysisProductInfo
                                                      {

                                                          Prod_Code = Convert.ToInt64(row["Prod_Code"]),
                                                          Brand = Convert.ToString(row["Brand"]),
                                                          TotalUnit = Convert.ToInt32(row["NoOfUnit"]),
                                                          TotalSale = Convert.ToInt32(row["TotalSale"]),
                                                          BrickID = Convert.ToInt32(row["BrickID"]),
                                                          TimePeriodID = Convert.ToInt32(row["TimePeriodID"]),
                                                          PeriodName = Convert.ToString(row["TimePeriod"])

                                                      }
                         ).ToList();


                        var UniqueTimeperiodListData = resultProductBrickList.Select(a => new { a.PeriodName, a.TimePeriodID }).Distinct().ToList();

                        List<TimePeriod> UniqueTimeperiodList = UniqueTimeperiodListData.Select(a => new TimePeriod { MonthYear = a.PeriodName, TimePeriodID = a.TimePeriodID }).OrderBy(a => a.TimePeriodID).ToList();

                        var resultProductList = from r in resultProductBrickList
                                                group r by new
                                                {
                                                    r.Prod_Code,
                                                    r.Brand
                                                } into g
                                                select new AnlaysisProductInfo
                                                {
                                                    Prod_Code = g.Key.Prod_Code,
                                                    Brand = g.Key.Brand,
                                                    TotalUnit = g.Sum(a => a.TotalUnit),
                                                    TotalSale = g.Sum(a => a.TotalSale)
                                                };

                        IList<Int64> ProductListToadd = new List<Int64>();

                        if (!string.IsNullOrEmpty(objAnalysis.CategoryValue))
                        {
                            if (objAnalysis.CategoryValue != "all")
                            {
                                string[] SelectedProductList = objAnalysis.CategoryValue.Split(',');
                                if (SelectedProductList.Length > 0)
                                {
                                    for (int j = 0; j < SelectedProductList.Length; j++)
                                    {
                                        ProductListToadd.Add(Convert.ToInt64(SelectedProductList[j]));
                                    }
                                }
                            }
                        }

                        /// Need to call function to get the BarChart Sales Data
                        /// 
                        IList<AnlaysisProductList> objAnalysisProductListSales = new List<AnlaysisProductList>();
                        IList<AnlaysisProductInfo> objCurrentAllProductList = resultProductList.OrderByDescending(a => a.TotalSale).ToList();
                        IList<BarChartData> BarChartSalesData = this.GetBarChartData(objCurrentAllProductList, resultProductBrickList, ProductListToadd, objAnalysis, 0, out objAnalysisProductListSales);
                        if (BarChartSalesData != null && BarChartSalesData.Count > 0)
                        {
                            foreach (BarChartData obj in BarChartSalesData)
                            {
                                BarChartSales.Add(obj);
                            }
                        }

                        IList<AnlaysisProductList> objAnalysisProductListUnit = new List<AnlaysisProductList>();
                        IList<AnlaysisProductInfo> objCurrentAllProductListUnit = resultProductList.OrderByDescending(a => a.TotalUnit).ToList();
                        IList<BarChartData> BarChartUnitData = this.GetBarChartData(objCurrentAllProductListUnit, resultProductBrickList, ProductListToadd, objAnalysis, 1, out objAnalysisProductListUnit);
                        if (BarChartUnitData != null && BarChartUnitData.Count > 0)
                        {
                            if (BarChartUnitData != null && BarChartUnitData.Count > 0)
                            {
                                foreach (BarChartData obj in BarChartUnitData)
                                {
                                    BarChartUnit.Add(obj);
                                }
                            }
                        }

                        AnalysisData.BarChartSales = BarChartSales;
                        AnalysisData.BarChartUnit = BarChartUnit;
                        AnalysisData.GetAnalysisProductList = objAnalysisProductListSales;
                        AnalysisData.GetAnalysisProductListUnit = objAnalysisProductListUnit;

                        // Comparison by Patch

                        var BrandResultTable = from r in resultProductBrickList
                                               group r by new
                                               {
                                                   r.Prod_Code,
                                                   r.Brand,
                                                   r.PeriodName,
                                                   r.TimePeriodID
                                               } into g
                                               select new AnlaysisProductInfo
                                               {
                                                   Prod_Code = g.Key.Prod_Code,
                                                   Brand = g.Key.Brand,
                                                   PeriodName = g.Key.PeriodName,
                                                   TimePeriodID = g.Key.TimePeriodID,
                                                   TotalSale = g.Sum(a => a.TotalSale),
                                                   TotalUnit = g.Sum(a => a.TotalUnit)
                                               };


                        AnalysisData.AnalysisComparisonSales = new List<SalesByPatch>();
                        AnalysisData.AnalysisComparisonUnit = new List<SalesByPatch>();

                        AnalysisData.AnalysisComparisonSalesPatchTable = new List<BrandPatchDetailsTableList>();
                        AnalysisData.AnalysisComparisonUnitPatchTable = new List<BrandPatchDetailsTableList>();

                        IList<Int64> Top5BrandSales = resultProductList.OrderByDescending(a => a.TotalSale).Select(a => a.Prod_Code).Distinct().Take(5).ToList();
                        if (!objAnalysis.includeTopSearches)
                        {
                            Top5BrandSales = new List<Int64>();
                        }

                        for (int i = 0; i < ProductListToadd.Count; i++)
                        {
                            if (!Top5BrandSales.Contains(ProductListToadd[i]))
                            {
                                Top5BrandSales.Add(ProductListToadd[i]);
                            }
                        }
                        int DataProductType = 1;
                        if (objAnalysis.CategoryType.ToLower() == "sku")
                        {
                            DataProductType = 2;
                        }
                        if(objAnalysis.CategoryType.ToLower()== "newintro")
                        {
                            DataProductType = 2;
                        }
                        Dictionary<Int64, string> ObjBrandColorCodeSales = this.GetBrandColorCode(Top5BrandSales, objAnalysis.SessionID, DataProductType);
                        // Dictionary<int, string> ObjCompanyColorCodeSales = objCP.ge(Top5BrandSales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                        AnalysisData.AnalysisComparisonSales = this.GetSalesPatchDataListBrand(objBrickList, resultProductBrickList, Top5BrandSales, 0, ObjBrandColorCodeSales, DataProductType);

                        IList<Int64> Top5BrandUnit = resultProductList.OrderByDescending(a => a.TotalUnit).Select(a => a.Prod_Code).Distinct().Take(5).ToList();
                        if (!objAnalysis.includeTopSearches)
                        {
                            Top5BrandUnit = new List<Int64>();
                        }

                        for (int i = 0; i < ProductListToadd.Count; i++)
                        {
                            if (!Top5BrandUnit.Contains(ProductListToadd[i]))
                            {
                                Top5BrandUnit.Add(ProductListToadd[i]);
                            }
                        }
                        Dictionary<Int64, string> ObjBrandColorCodeUnit = this.GetBrandColorCode(Top5BrandUnit, objAnalysis.SessionID, DataProductType);


                        AnalysisData.AnalysisComparisonUnit = this.GetSalesPatchDataListBrand(objBrickList, resultProductBrickList, Top5BrandUnit, 1, ObjBrandColorCodeUnit, DataProductType);

                        AnalysisData.AnalysisComparisonByPatchSales = new List<PatchData>();
                        AnalysisData.AnalysisComparisonByPatchUnit = new List<PatchData>();
                        for (int i = 0; i < objBrickList.Count; i++)
                        {
                            IList<AnlaysisProductInfo> ObjBrickResult = resultProductBrickList.Where(a => a.BrickID == objBrickList[i].BrickID).ToList();

                            var BrandResultPatch = from r in ObjBrickResult
                                                   group r by new
                                                   {
                                                       r.Prod_Code,
                                                       r.Brand
                                                   } into g
                                                   select new AnlaysisProductInfo
                                                   {
                                                       Prod_Code = g.Key.Prod_Code,
                                                       Brand = g.Key.Brand,
                                                       TotalSale = g.Sum(a => a.TotalSale),
                                                       TotalUnit = g.Sum(a => a.TotalUnit)
                                                   };

                            IList<Int64> Top5BrandPatchSales = BrandResultPatch.OrderByDescending(a => a.TotalSale).Select(a => a.Prod_Code).Distinct().Take(5).ToList();
                            if (!objAnalysis.includeTopSearches)
                            {
                                Top5BrandPatchSales = new List<Int64>();
                            }

                            for (int k = 0; k < ProductListToadd.Count; k++)
                            {
                                if (!Top5BrandPatchSales.Contains(ProductListToadd[k]))
                                {
                                    Top5BrandPatchSales.Add(ProductListToadd[k]);
                                }
                            }
                            IList<BrickMaster> objBrickdata = new List<BrickMaster>();
                            objBrickdata.Add(objBrickList[i]);

                            Dictionary<Int64, string> ObjCompanyColorCodePatchSale = this.GetBrandColorCode(Top5BrandPatchSales, objAnalysis.SessionID, DataProductType);

                            IList<SalesByPatch> objPatchSalesPatchListSales = this.GetSalesPatchDataListBrand(objBrickdata, resultProductBrickList, Top5BrandPatchSales, 0, ObjCompanyColorCodePatchSale, DataProductType);
                            PatchData objPatchData = new PatchData()
                            {
                                PatchDataDetails = objPatchSalesPatchListSales,
                                PatchID = objBrickList[i].BrickID,
                                PatchName = objBrickList[i].BrickName
                            };
                            AnalysisData.AnalysisComparisonByPatchSales.Add(objPatchData);

                            // Unit
                            IList<Int64> Top5BrandPatchUnit = BrandResultPatch.OrderByDescending(a => a.TotalUnit).Select(a => a.Prod_Code).Distinct().Take(5).ToList();
                            if (!objAnalysis.includeTopSearches)
                            {
                                Top5BrandPatchUnit = new List<Int64>();
                            }

                            for (int k = 0; k < ProductListToadd.Count; k++)
                            {
                                if (!Top5BrandPatchUnit.Contains(ProductListToadd[k]))
                                {
                                    Top5BrandPatchUnit.Add(ProductListToadd[k]);
                                }
                            }

                            Dictionary<Int64, string> ObjBrandolorCodePatchUnit = this.GetBrandColorCode(Top5BrandPatchUnit, objAnalysis.SessionID, DataProductType);


                            IList<SalesByPatch> objPatchSalesPatchListUnit = this.GetSalesPatchDataListBrand(objBrickdata, resultProductBrickList, Top5BrandPatchUnit, 1, ObjBrandolorCodePatchUnit, DataProductType);
                            PatchData objPatchDataUnit = new PatchData()
                            {
                                PatchDataDetails = objPatchSalesPatchListUnit,
                                PatchID = objBrickList[i].BrickID,
                                PatchName = objBrickList[i].BrickName
                            };
                            AnalysisData.AnalysisComparisonByPatchUnit.Add(objPatchDataUnit);



                            // Patch Table 
                            var BrandResultPatchTable = from r in ObjBrickResult
                                                        group r by new
                                                        {
                                                            r.Prod_Code,
                                                            r.Brand,
                                                            r.TimePeriodID,
                                                            r.PeriodName
                                                        } into g
                                                        select new AnlaysisProductInfo
                                                        {
                                                            Prod_Code = g.Key.Prod_Code,
                                                            Brand = g.Key.Brand,
                                                            PeriodName = g.Key.PeriodName,
                                                            TimePeriodID = g.Key.TimePeriodID,
                                                            TotalSale = g.Sum(a => a.TotalSale),
                                                            TotalUnit = g.Sum(a => a.TotalUnit)
                                                        };

                            // Company Tablel data ends
                            var BrandResultSalesPatchTable = BrandResultPatchTable.OrderByDescending(a => a.TotalUnit).ToList();
                            IList<TableListBrand> TableDataSales = this.GetBrandPerformanceListTable(BrandResultSalesPatchTable, ProductListToadd, objAnalysis, 0, 0, UniqueTimeperiodList, DataProductType);
                            var BrandResultUnitPatchTable = BrandResultPatchTable.OrderByDescending(b => b.TotalUnit).ToList();
                            IList<TableListBrand> TableDataUnit = this.GetBrandPerformanceListTable(BrandResultSalesPatchTable, ProductListToadd, objAnalysis, 0, 1, UniqueTimeperiodList, DataProductType);

                            BrandPatchDetailsTableList objCCPSalePatch = new BrandPatchDetailsTableList()
                            {
                                PatchID = objBrickList[i].BrickID,
                                PatchName = objBrickList[i].BrickName,
                                TableData = TableDataSales
                            };

                            BrandPatchDetailsTableList objCCPUnitPatch = new BrandPatchDetailsTableList()
                            {
                                PatchID = objBrickList[i].BrickID,
                                PatchName = objBrickList[i].BrickName,
                                TableData = TableDataUnit
                            };
                            AnalysisData.AnalysisComparisonSalesPatchTable.Add(objCCPSalePatch);
                            AnalysisData.AnalysisComparisonUnitPatchTable.Add(objCCPUnitPatch);

                        }

                    }
                }

             }
            catch (Exception ex)
            {
                throw ex;
            }

            return AnalysisData;
        }

       


        public MarketShareGraphData GetAnalysisMarketShareData(AnalysisMarketShare_IVM objAnalysis)
        {
            MarketShareGraphData AnalysisData = new MarketShareGraphData();

            SessionDataAccess objSessionDataAccess = new SessionDataAccess();
            int UserID = objSessionDataAccess.ValidateSession(objAnalysis.SessionID);
            objSessionDataAccess = null;
            if (UserID == 0)
            {
                AnalysisData = null;
                return AnalysisData;
            }

            // Get UserAccessRights 

            AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
            UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
           
            int AccessType = 1;
            string TCCodes = string.Empty;
            if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
            {
                
                    AccessType = 2;
                    TCCodes = objUserAccessrights.TCValues;
                
            }

            else
            {
                CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objAnalysis.CompanyID);
                if (objCompanyAccessRight != null)
                {
                    switch (objCompanyAccessRight.AccessRightType)
                    {
                        case 2:
                            AccessType = 2;
                            FilterDataAccess objFilterdataAccess = new FilterDataAccess();
                            IList<string> TCCodesList = objFilterdataAccess.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                            objFilterdataAccess = null;
                            TCCodes = string.Join(",", TCCodesList);
                            break;
                        case 3:
                            AccessType = 2;
                            TCCodes = objCompanyAccessRight.TCValues;
                            break;
                    }
                }
            }
            objAccessRightsDataAccess = null;
            try
            {
                var param = string.Empty;
                if (objAnalysis != null)
                {

                    DataSet resultds = new DataSet();
                    DALUtility IMSBIDB = new DALUtility();

                    string TherapylistSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTherapyList + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "'," + AccessType + ",'" + TCCodes + "'";//"+ objAnalysis.PatchIds + "','" + objAnalysis.CategoryValue + "'";
                    DataSet TherapyList = IMSBIDB.ReturnDataset(TherapylistSQL);


                    var resultTherapyList = (from row in TherapyList.Tables[0].AsEnumerable()
                                             select new TherapyList
                                             {

                                                 TherapyID = Convert.ToString(row["TherapyCode"]),
                                                 TherapyName = Convert.ToString(row["TherapyName"]),

                                             }
                                 ).ToList();

                    AnalysisData.TherapyList = resultTherapyList;


                    switch (objAnalysis.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharemat + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharematWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "','" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharemth + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharemthWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "','" + TCCodes + "'" ;
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareQtr + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareQtrWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "','" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharehlf + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharehlfWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "','" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdf + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdfWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "','" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (AccessType == 1)
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdc + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdcWithRights + " " + objAnalysis.GeographyID + ",'" + objAnalysis.GeographyFilter + "'," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + "," + objAnalysis.TCCode + ",'" + objAnalysis.CategoryType + "','" + objAnalysis.PatchIds + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + ",'" + objAnalysis.TCValue + "','" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;

                    }



                    var TotalSalesofAllMonth = (from row in resultds.Tables[0].AsEnumerable()
                                           select new MonthlySale
                                           {
                                               Totalsale =  Convert.ToDouble(row["TotalSale"]),
                                               TotalUnit = Convert.ToDouble(row["TotalUnit"]),
                                               TimePeriodID = Convert.ToInt32(row["TimePeriodID"])
                                           }
                                            ).ToList();
                    Double TotalSalesofAll = 0;
                    Double TotalUnitOfAll = 0;
                    Dictionary<int, Double> MonthWiseSales = new Dictionary<int, Double>();
                    Dictionary<int, Double> MonthWiseUnits = new Dictionary<int, Double>();

                    Int32 TotalMonth = TotalSalesofAllMonth.Count;
                    for (int k=0; k< TotalSalesofAllMonth.Count; k++)
                    {
                        TotalSalesofAll += TotalSalesofAllMonth[k].Totalsale;
                        TotalUnitOfAll += TotalSalesofAllMonth[k].TotalUnit;
                        MonthWiseSales.Add(TotalSalesofAllMonth[k].TimePeriodID, TotalSalesofAllMonth[k].Totalsale);
                        MonthWiseUnits.Add(TotalSalesofAllMonth[k].TimePeriodID, TotalSalesofAllMonth[k].TotalUnit);
                    }

                    AnalysisData.TotalSalesAVGMonth = TotalSalesofAll / TotalMonth;


                    var TotalSalesByTCLevel = (from row in resultds.Tables[1].AsEnumerable()
                                               select new TCLevelSales
                                               {
                                                   TCCode = Convert.ToString(row["TCCode"]),
                                                   TotalSale = Convert.ToDecimal(row["Totalsale"])
                                               }
                                               ).ToList();

                    if(TotalSalesByTCLevel.Count >0)
                    {
                        AnalysisData.TotalTCAVGMonth = TotalSalesByTCLevel[0].TotalSale / TotalMonth ;
                        AnalysisData.PopularTCCategory = resultTherapyList.Where(x => x.TherapyID == TotalSalesByTCLevel[0].TCCode).Select(x => x.TherapyName).FirstOrDefault();
                    }


                    var resultMarketShareProductList = (from row in resultds.Tables[2].AsEnumerable()
                                                        select new MarketShareProductList
                                                        {
                                                            BrandID = Convert.ToInt64(row["BrandID"]),
                                                            TotalSale = Convert.ToInt32(row["TotalSale"]),
                                                            NoOfUnit = Convert.ToInt32(row["NoOfUnit"]),
                                                            Brand = Convert.ToString(row["Brand"])


                                                        }
                              ).ToList();


                    IList<Int64> ProductListToadd = new List<Int64>();

                    if (!string.IsNullOrEmpty(objAnalysis.CategoryValue))
                    {
                        if (objAnalysis.CategoryValue != "all")
                        {
                            string[] SelectedProductList = objAnalysis.CategoryValue.Split(',');
                            if (SelectedProductList.Length > 0)
                            {
                                for (int j = 0; j < SelectedProductList.Length; j++)
                                {
                                    ProductListToadd.Add(Convert.ToInt64(SelectedProductList[j]));
                                }
                            }
                        }
                    }

                    IList<Int64> Top10ProductAdded = new List<Int64>();

                    AnalysisData.AnalysisMarketShareProductList = new List<MarketShareProductList>();
                    IList<Int64> TopProducts = new List<Int64>();
                    for (int z = 0; z < resultMarketShareProductList.Count; z++)
                    {

                        if (objAnalysis.includeTopSearches)
                        {
                            if (Top10ProductAdded.Count < 10)
                            {

                                MarketShareProductList objNew = new MarketShareProductList();
                                objNew.Rank = z + 1;
                                objNew.Brand = resultMarketShareProductList[z].Brand;
                                objNew.NoOfUnit = resultMarketShareProductList[z].NoOfUnit;
                                objNew.TotalSale = resultMarketShareProductList[z].TotalSale;
                                objNew.MarketShare =  Convert.ToDecimal( Math.Round(((resultMarketShareProductList[z].TotalSale / TotalSalesofAll) * 100), 2));
                                objNew.TCCode = objAnalysis.TCValue;
                                objNew.MarketValue = Math.Round(TotalSalesofAll);
                                objNew.MarketUnit = TotalUnitOfAll;
                                objNew.MarketUnitShare = Convert.ToDecimal(Math.Round(((resultMarketShareProductList[z].NoOfUnit / TotalUnitOfAll) * 100), 2));

                                AnalysisData.AnalysisMarketShareProductList.Add(objNew);

                                Top10ProductAdded.Add(resultMarketShareProductList[z].BrandID);
                            }


                            if (TopProducts.Count < 5)
                            {
                                TopProducts.Add(resultMarketShareProductList[z].BrandID);
                            }
                        }

                        if (ProductListToadd.Contains(resultMarketShareProductList[z].BrandID))
                        {
                            if (!TopProducts.Contains(resultMarketShareProductList[z].BrandID))
                            {
                                TopProducts.Add(resultMarketShareProductList[z].BrandID);
                            }
                            if (!Top10ProductAdded.Contains(resultMarketShareProductList[z].BrandID))
                            {
                                MarketShareProductList objNew = new MarketShareProductList();
                                objNew.Rank = z + 1;
                                objNew.Brand = resultMarketShareProductList[z].Brand;
                                objNew.NoOfUnit = resultMarketShareProductList[z].NoOfUnit;
                                objNew.TotalSale = resultMarketShareProductList[z].TotalSale;
                                objNew.MarketShare = Convert.ToDecimal(Math.Round(((resultMarketShareProductList[z].TotalSale  / TotalSalesofAll) * 100), 2));
                                objNew.TCCode = objAnalysis.TCValue;
                                objNew.MarketValue = Math.Round(TotalSalesofAll);
                                objNew.MarketUnit = TotalUnitOfAll;
                                objNew.MarketUnitShare = Convert.ToDecimal(Math.Round(((resultMarketShareProductList[z].NoOfUnit  / TotalUnitOfAll) * 100), 2));

                                AnalysisData.AnalysisMarketShareProductList.Add(objNew);

                                Top10ProductAdded.Add(resultMarketShareProductList[z].BrandID);
                                
                            }
                        }
                    }



                    // ZeroValueProducts 
                    IList<Int64> ZeroValueProductList = new List<Int64>();
                    for (int j = 0; j < ProductListToadd.Count; j++)
                    {
                        if (!TopProducts.Contains(ProductListToadd[j]))
                        {
                            ZeroValueProductList.Add(ProductListToadd[j]);
                        }

                    }

                    // Get Zero Value Company  Details
                    if (ZeroValueProductList.Count > 0)
                    {
                        ProductDataAccess objProductDataAccess = new ProductDataAccess();
                        IList<BrandDetails> ZeroValueBrandDetails = objProductDataAccess.GetZeroValueBrandDetails(string.Join(",", ZeroValueProductList), objAnalysis.CategoryType.ToLower());
                        objProductDataAccess = null;
                        if (ZeroValueBrandDetails.Count > 0)
                        {
                            for (int zeroValueProduct = 0; zeroValueProduct < ZeroValueBrandDetails.Count; zeroValueProduct++)
                            {
                                MarketShareProductList objNew = new MarketShareProductList();
                                objNew.Rank = 0;
                                objNew.Brand = ZeroValueBrandDetails[zeroValueProduct].Brand;
                                objNew.NoOfUnit = 0;
                                objNew.TotalSale = 0;
                                objNew.MarketShare = 0;
                                objNew.MarketUnit = 0;
                                objNew.MarketUnitShare = 0;
                                objNew.MarketValue = 0;
                                objNew.TCCode = objAnalysis.TCValue;
                                AnalysisData.AnalysisMarketShareProductList.Add(objNew);
                            }

                        }
                    }


                    string TopProductstring = String.Join(",", TopProducts);
                    int FilterTypeOfColor = 1;
                    if(objAnalysis.CategoryType =="sku")
                    {
                        FilterTypeOfColor = 2;
                    }

                    Dictionary<Int64, string> BrandColorCode = this.GetBrandColorCode(TopProducts, objAnalysis.SessionID, FilterTypeOfColor);

                    string TopProductDataSQL = string.Empty;
                    if (objAnalysis.CategoryType == "sku")
                    {
                        switch (objAnalysis.PeriodType.ToLower())
                        {
                            case "mat":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharematProductData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "mth":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharemthProductData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "qtr":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareQtrProductData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "hlf":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharehlfProductData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "ytdf":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdfProductData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "ytdc":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdcProductData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;


                        }
                    }
                    else
                    {
                        switch (objAnalysis.PeriodType.ToLower())
                        {
                            case "mat":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharematBrandData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "mth":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharemthBrandData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "qtr":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareQtrBrandData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "hlf":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketSharehlfBrandData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "ytdf":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdfBrandData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;
                            case "ytdc":
                                TopProductDataSQL = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMarketShareytdcBrandData + " " + objAnalysis.GeographyID + "," + objAnalysis.CompanyID + "," + objAnalysis.DivisonID + ",'" + TopProductstring + "','" + objAnalysis.GeographyFilter + "','" + objAnalysis.PatchIds + "','" + objAnalysis.PeriodType + "'," + objAnalysis.PeriodStart + "," + objAnalysis.PeriodEnd + "," + objAnalysis.TCCode;
                                break;


                        }
                    }

                    resultds = IMSBIDB.ReturnDataset(TopProductDataSQL);
                   
                    var resultMarketSales = (from row in resultds.Tables[0].AsEnumerable()
                                             select new MarketShareData
                                             {
                                                 TimePeriodID = Convert.ToInt32(row["TimePeriodID"]),
                                                 TimePeriod = Convert.ToString(row["TimePeriod"]),
                                                 TotalSale = Convert.ToInt32(row["TotalSale"]),
                                                 NoOfUnit = Convert.ToInt32(row["NoOfUnit"]),
                                                 TherapyCode = Convert.ToString(row["TherapyCode"]),
                                                 TherapyName = Convert.ToString(row["TherapyName"]),
                                                 TCCode = Convert.ToString(row["TCCode"])


                                             }
                                 ).ToList();
                   

                    string CurrentTherapyCode = string.Empty;
                   
                    IList<AnalysisMarketShare_OVM> objGraphDatalist = new List<AnalysisMarketShare_OVM>();
                   

                    AnalysisMarketShare_OVM objNewoutput = new AnalysisMarketShare_OVM();

                    IList<AnalysisMarketShare_OVM> objGraphDatalistUnit = new List<AnalysisMarketShare_OVM>();


                    AnalysisMarketShare_OVM objNewoutputUnit = new AnalysisMarketShare_OVM();

                    CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                    IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objAnalysis.PeriodType.ToLower(), objAnalysis.PeriodStart, objAnalysis.PeriodEnd);
                    objCompanyDataaccessForTimeperiod = null;

                    string ColorCode = string.Empty;
                    string CurrentTherapyName = string.Empty;
                    ArrayList objArrayList = new ArrayList();
                    ArrayList objArrayListUnit = new ArrayList();
                    IList<Int32> AlreadyaddedTimePeriod = new List<Int32>();
                    for (int j = 0; j < resultMarketSales.Count; j++)
                    {
                        if (objGraphDatalist.Count < 6)
                        {

                            if (CurrentTherapyCode == resultMarketSales[j].TherapyCode)
                            {
                                
                                ArrayList singleobj = new ArrayList();
                                singleobj.Add(resultMarketSales[j].TimePeriod);
                                Double MonthlySale;
                                if(MonthWiseSales.TryGetValue(resultMarketSales[j].TimePeriodID,out MonthlySale))
                                {
                                    singleobj.Add(Math.Round(((resultMarketSales[j].TotalSale  / MonthlySale) * 100), 2));
                                }
                                   
                                objArrayList.Add(singleobj);

                                ArrayList singleobjUnit = new ArrayList();
                                singleobjUnit.Add(resultMarketSales[j].TimePeriod);
                                Double MonthlyUnit;
                                if (MonthWiseUnits.TryGetValue(resultMarketSales[j].TimePeriodID, out MonthlyUnit))
                                {
                                    singleobjUnit.Add(Math.Round(((resultMarketSales[j].NoOfUnit  / MonthlyUnit) * 100), 2));
                                }

                                objArrayListUnit.Add(singleobjUnit);

                                AlreadyaddedTimePeriod.Add(resultMarketSales[j].TimePeriodID);

                            }
                            else
                            {
                                if (j != 0)
                                {
                                    //following loop will add the not added Timeperiod data as zero
                                    for (int TimeperiodCounter = 0; TimeperiodCounter < TimePeriodlist.Count; TimeperiodCounter++)
                                    {

                                        if (!AlreadyaddedTimePeriod.Contains(TimePeriodlist[TimeperiodCounter].TimePeriodID))
                                        {
                                            ArrayList singleobjZero = new ArrayList();
                                            singleobjZero.Add(TimePeriodlist[TimeperiodCounter].TimePeriodID);                                          
                                            singleobjZero.Add(0);                                        

                                            objArrayList.Add(singleobjZero);

                                            ArrayList singleobjUnitzero = new ArrayList();
                                            singleobjUnitzero.Add(TimePeriodlist[TimeperiodCounter].TimePeriodID);
                                            singleobjUnitzero.Add(0);                                          

                                            objArrayListUnit.Add(singleobjUnitzero);

                                        }
                                    }


                                    objNewoutput.key = CurrentTherapyName;
                                    ColorCode = string.Empty;
                                    if (BrandColorCode.TryGetValue(Convert.ToInt64(CurrentTherapyCode), out ColorCode))
                                    {
                                        objNewoutput.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objNewoutput.ColorCode = ColorCode;
                                    }
                                    objNewoutput.values = objArrayList;
                                    objGraphDatalist.Add(objNewoutput);
                                
                                 
                                    objNewoutput = new AnalysisMarketShare_OVM();
                                    objArrayList = new ArrayList();

                                    objNewoutputUnit.key = CurrentTherapyName;
                                    ColorCode = string.Empty;
                                    if (BrandColorCode.TryGetValue(Convert.ToInt64(CurrentTherapyCode), out ColorCode))
                                    {
                                        objNewoutputUnit.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objNewoutputUnit.ColorCode = ColorCode;
                                    }
                                    objNewoutputUnit.values = objArrayListUnit;
                                    objGraphDatalistUnit.Add(objNewoutputUnit);

                                    objNewoutputUnit = new AnalysisMarketShare_OVM();
                                    objArrayListUnit = new ArrayList();
                                    AlreadyaddedTimePeriod = new List<Int32>();





                                }
                                CurrentTherapyName = resultMarketSales[j].TherapyName;
                                


                                ArrayList singleobj = new ArrayList();
                                singleobj.Add(resultMarketSales[j].TimePeriod);
                                double MonthlySale;
                                if (MonthWiseSales.TryGetValue(resultMarketSales[j].TimePeriodID, out MonthlySale))
                                {
                                    singleobj.Add(Math.Round(((resultMarketSales[j].TotalSale  / MonthlySale) * 100), 2));
                                }
                                objArrayList.Add(singleobj);

                                ArrayList singleobjUnit = new ArrayList();
                                singleobjUnit.Add(resultMarketSales[j].TimePeriod);
                                Double MonthlyUnit;
                                if (MonthWiseUnits.TryGetValue(resultMarketSales[j].TimePeriodID, out MonthlyUnit))
                                {
                                    singleobjUnit.Add(Math.Round(((resultMarketSales[j].NoOfUnit  / MonthlyUnit) * 100), 2));
                                }

                                objArrayListUnit.Add(singleobjUnit);

                                AlreadyaddedTimePeriod.Add(resultMarketSales[j].TimePeriodID);

                                CurrentTherapyCode = resultMarketSales[j].TherapyCode;
                               
                            }
                        }
                        
                       
                    }

                

                    //Process last item
                    if (objGraphDatalist.Count < 6)
                    {

                        if (objArrayList.Count > 0)
                        {

                            for (int TimeperiodCounter = 0; TimeperiodCounter < TimePeriodlist.Count; TimeperiodCounter++)
                                    {

                                    if (!AlreadyaddedTimePeriod.Contains(TimePeriodlist[TimeperiodCounter].TimePeriodID))
                                    {
                                        ArrayList singleobjZero = new ArrayList();
                                        singleobjZero.Add(TimePeriodlist[TimeperiodCounter].TimePeriodID);
                                        singleobjZero.Add(0);

                                        objArrayList.Add(singleobjZero);

                                        ArrayList singleobjUnitzero = new ArrayList();
                                        singleobjUnitzero.Add(TimePeriodlist[TimeperiodCounter].TimePeriodID);
                                        singleobjUnitzero.Add(0);

                                        objArrayListUnit.Add(singleobjUnitzero);
                                      }   
                                }
                            objNewoutput.key = CurrentTherapyName;
                            ColorCode = string.Empty;
                            if (BrandColorCode.TryGetValue(Convert.ToInt64(CurrentTherapyCode), out ColorCode))
                            {
                                objNewoutput.ColorCode = ColorCode;

                            }
                            else
                            {
                                objNewoutput.ColorCode = ColorCode;
                            }
                            objNewoutput.values = objArrayList;
                            objGraphDatalist.Add(objNewoutput);


                            objNewoutputUnit.key = CurrentTherapyName;
                            ColorCode = string.Empty;
                            if (BrandColorCode.TryGetValue(Convert.ToInt64(CurrentTherapyCode), out ColorCode))
                            {
                                objNewoutputUnit.ColorCode = ColorCode;

                            }
                            else
                            {
                                objNewoutputUnit.ColorCode = ColorCode;
                            }
                            objNewoutputUnit.values = objArrayListUnit;
                            objGraphDatalistUnit.Add(objNewoutputUnit);
                        }
                    }

                    AnalysisData.AnalysisMarketShareGraphData = objGraphDatalist;
                    AnalysisData.AnalysisMarketShareGraphDataUnit = objGraphDatalistUnit;
                    // Product List




                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return AnalysisData;
        }

        private IList<BarChartData> GetBarChartData(IList<AnlaysisProductInfo>  objCurrentAllProductList, IList<AnlaysisProductInfo> resultProductBrickList, IList<Int64> ProductListToadd, Analysis_IVM objAnalysis, int Types, out IList<AnlaysisProductList> objAnalysisProductList)
        {
            IList<BarChartData> objBarChartData = new List<BarChartData>();
            objAnalysisProductList = new List<AnlaysisProductList>();
            IList<Int64> Top10Products = new List<Int64>();
            Dictionary<Int64, int> ProductRankNumber = new Dictionary<Int64, int>();
            if (objCurrentAllProductList != null)
            {
                for (int i = 0; i < objCurrentAllProductList.Count; i++)
                {
                    if (objAnalysis.includeTopSearches)
                    {

                        if (Top10Products.Count < 10)
                        {
                            Top10Products.Add(objCurrentAllProductList[i].Prod_Code);

                            ProductRankNumber.Add(objCurrentAllProductList[i].Prod_Code, i + 1);

                        }
                    }

                    if (ProductListToadd.Contains(objCurrentAllProductList[i].Prod_Code))
                    {

                        if (!Top10Products.Contains(objCurrentAllProductList[i].Prod_Code))
                        {
                            Top10Products.Add(objCurrentAllProductList[i].Prod_Code);
                            ProductRankNumber.Add(objCurrentAllProductList[i].Prod_Code, i + 1);


                        }
                    }


                }


            }


            // ZeroValueProducts 
            IList<Int64> ProductListForColorCode = Top10Products;
            IList<Int64> ZeroValueProductList = new List<Int64>();
            for (int j = 0; j < ProductListToadd.Count; j++)
            {
                if (!Top10Products.Contains(ProductListToadd[j]))
                {
                    ZeroValueProductList.Add(ProductListToadd[j]);
                    ProductListForColorCode.Add(ProductListToadd[j]);
                }

            }

            // Get Zero Value Company  Details
            string DataProductTypeString = "brand";
            if (objAnalysis.CategoryType.ToLower() == "sku")
            {
                DataProductTypeString = "sku";
            }
            if (objAnalysis.CategoryType.ToLower() == "newintro")
            {
                DataProductTypeString = "sku";
            }
            string ColorCode = string.Empty;
            IList<BrandDetails> ZeroValueBrandDetails = new List<BrandDetails>();
            if (ZeroValueProductList.Count > 0)
            {
                ProductDataAccess objProductDataAccess = new ProductDataAccess();
                ZeroValueBrandDetails = objProductDataAccess.GetZeroValueBrandDetails(string.Join(",", ZeroValueProductList), DataProductTypeString.ToLower());
                objProductDataAccess = null;

            }

            int DataProductType = 1;
            if (objAnalysis.CategoryType.ToLower() == "sku")
            {
                DataProductType = 2;
            }

            string TopProductIds = String.Join(",", Top10Products);
            // Get Selected Product Info              

            Dictionary<Int64, string> ObjBrandColorCode = this.GetBrandColorCode(ProductListForColorCode, objAnalysis.SessionID, DataProductType);
            // Filter required data 

            var resultProductTimeperiodList = from r in resultProductBrickList
                                              group r by new
                                              {
                                                  r.Prod_Code,
                                                  r.Brand,
                                                  r.TimePeriodID,
                                                  r.PeriodName
                                              } into g
                                              select new GroupChartSales
                                              {
                                                  BrandID = g.Key.Prod_Code,
                                                  BrandName = g.Key.Brand,
                                                  TimePeriod = g.Key.PeriodName,
                                                  TimePeriodID = g.Key.TimePeriodID,
                                                  Unit = g.Sum(a => a.TotalUnit),
                                                  Sales = g.Sum(a => a.TotalSale)
                                              };

            CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
            IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objAnalysis.PeriodType.ToLower(), objAnalysis.PeriodStart, objAnalysis.PeriodEnd);
            objCompanyDataaccessForTimeperiod = null;
            Dictionary<Int32, Double> TotalPeriodDataValue = new Dictionary<int, double>();
            for (int i=0; i< Top10Products.Count; i++)
            {
                AnlaysisProductList objProductData = new AnlaysisProductList();
                IList<GroupChartSales> ProductData = resultProductTimeperiodList.Where(a => a.BrandID == Top10Products[i]).ToList();
                if (ProductData != null && ProductData.Count > 0)
                {
                    BarChartData objChartData= new BarChartData();
                    objChartData.key = ProductData[0].BrandName;
                    ColorCode = string.Empty;
                    if (ObjBrandColorCode.TryGetValue(Top10Products[i], out ColorCode))
                    {
                        objChartData.ColorCode = ColorCode;                  
                        
                    }
                    else
                    {
                        objChartData.ColorCode = ColorCode;
                        
                    }
                    ArrayList SalesArray = new ArrayList();
                    IList<MonthwiseSale> ProductWiseMonthSales = new List<MonthwiseSale>();
                    Double TotalSale = 0;
                    for (int j = 0; j < TimePeriodlist.Count; j++)
                    {
                        GroupChartSales TimeperiodProductData = ProductData.Where(a => a.TimePeriodID == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                        if (TimeperiodProductData != null)
                        {
                                    ArrayList SalesData = new ArrayList();
                                    SalesData.Add(TimeperiodProductData.TimePeriod);
                                    if(Types==0)
                                    {
                                TotalSale += TimeperiodProductData.Sales;
                                        SalesData.Add(TimeperiodProductData.Sales);
                                        Double PeriodTotal = 0;
                                        if (TotalPeriodDataValue.TryGetValue(TimePeriodlist[j].TimePeriodID, out PeriodTotal))
                                        {
                                            PeriodTotal += TimeperiodProductData.Sales;
                                            TotalPeriodDataValue[TimePeriodlist[j].TimePeriodID] = PeriodTotal;
                                        }
                                        else
                                        {
                                            if (!TotalPeriodDataValue.ContainsKey(TimePeriodlist[j].TimePeriodID))
                                            {
                                                TotalPeriodDataValue.Add(TimePeriodlist[j].TimePeriodID, TimeperiodProductData.Sales);
                                            }
                                        }
                                    }
                                    else
                                    {
                                          TotalSale += TimeperiodProductData.Unit;
                                          SalesData.Add(TimeperiodProductData.Unit);
                                        Double PeriodTotal = 0;
                                        if (TotalPeriodDataValue.TryGetValue(TimePeriodlist[j].TimePeriodID, out PeriodTotal))
                                        {
                                            PeriodTotal += TimeperiodProductData.Unit;
                                            TotalPeriodDataValue[TimePeriodlist[j].TimePeriodID] = PeriodTotal;
                                        }
                                        else
                                        {
                                            if (!TotalPeriodDataValue.ContainsKey(TimePeriodlist[j].TimePeriodID))
                                            {
                                                TotalPeriodDataValue.Add(TimePeriodlist[j].TimePeriodID, TimeperiodProductData.Unit);
                                            }
                                        }
                                 }
                                   
                                    SalesData.Add(TimeperiodProductData.TimePeriodID);
                                    SalesArray.Add(SalesData);

                                    MonthwiseSale objMonthwiseSale = new MonthwiseSale
                                    {
                                        MonthName = TimeperiodProductData.TimePeriod,
                                        Value = TimeperiodProductData.Sales,
                                        Unit = TimeperiodProductData.Unit
                                    };
                                    ProductWiseMonthSales.Add(objMonthwiseSale);

                          
                        }
                        else
                        {
                            ArrayList SalesData = new ArrayList();
                            SalesData.Add(TimePeriodlist[j].MonthName);
                            if (Types == 0)
                            {
                                SalesData.Add(0);
                            }
                            else
                            {
                                SalesData.Add(0);
                            }
                        
                            SalesData.Add(TimePeriodlist[j].TimePeriodID);
                            SalesArray.Add(SalesData);

                            MonthwiseSale objMonthwiseSale = new MonthwiseSale
                            {
                                MonthName = TimePeriodlist[j].MonthName,
                                Value = 0,
                                Unit = 0
                            };
                            ProductWiseMonthSales.Add(objMonthwiseSale);
                        }

                    }
                    objChartData.values = SalesArray;
                    objBarChartData.Add(objChartData);
                    int RankNumber = 0;
                    if (ProductRankNumber.TryGetValue(Top10Products[i], out RankNumber))
                    {
                        objProductData.Rank = RankNumber;

                    }
                    else
                    {
                        objProductData.Rank = 0;
                    }
                    objProductData.TotalSale = TotalSale;
                    objProductData.MonthData = ProductWiseMonthSales;
                    objProductData.Prod_Code = Top10Products[i];
                    objProductData.Brand = ProductData[0].BrandName;
                    objAnalysisProductList.Add(objProductData);
                }
            }



            if(ZeroValueProductList.Count > 0)
            {
                if (ZeroValueBrandDetails != null && ZeroValueBrandDetails.Count > 0)
                {
                    for (int i = 0; i < ZeroValueBrandDetails.Count; i++)
                    {
                        AnlaysisProductList objProductData = new AnlaysisProductList();
                        BarChartData objChartData = new BarChartData();
                        objChartData.key = ZeroValueBrandDetails[i].Brand;
                        ColorCode = string.Empty;
                        if (ObjBrandColorCode.TryGetValue(ZeroValueBrandDetails[i].BrandID, out ColorCode))
                        {
                            objChartData.ColorCode = ColorCode;

                        }
                        else
                        {
                            objChartData.ColorCode = ColorCode;

                        }
                        ArrayList SalesArray = new ArrayList();
                        IList<MonthwiseSale> ProductWiseMonthSales = new List<MonthwiseSale>();
                        for (int j = 0; j < TimePeriodlist.Count; j++)
                        {
                            ArrayList SalesData = new ArrayList();
                            SalesData.Add(TimePeriodlist[j].MonthName);
                            if (Types == 0)
                            {
                                SalesData.Add(0);
                            }
                            else
                            {
                                SalesData.Add(0);
                            }

                            SalesData.Add(TimePeriodlist[j].TimePeriodID);
                            SalesArray.Add(SalesData);


                            MonthwiseSale objMonthwiseSale = new MonthwiseSale
                            {
                                MonthName = TimePeriodlist[j].MonthName,
                                Value = 0,
                                Unit = 0
                            };
                            ProductWiseMonthSales.Add(objMonthwiseSale);

                        }
                        objChartData.values = SalesArray;
                        objBarChartData.Add(objChartData);

                        objProductData.MonthData = ProductWiseMonthSales;
                        objProductData.Prod_Code = ZeroValueBrandDetails[i].BrandID ;
                        objProductData.Brand = ZeroValueBrandDetails[i].Brand;
                        objProductData.TotalSale = 0;
                        objAnalysisProductList.Add(objProductData);
                    }
                }
                
            }

            IList<MonthwiseSale> objMonthDataTotal = new List<MonthwiseSale>();
            IList<MonthwiseSale> objMonthDataOther = new List<MonthwiseSale>();
            Double TotalCompanyGrandUnit = 0;
            Double TotalCompanyOtherUnit = 0;
            for (int j = 0; j < TimePeriodlist.Count; j++)
            {

                Double TotalTimePeriodSale = 0;
                if (Types == 0)
                {
                    TotalTimePeriodSale = resultProductTimeperiodList.Where(a => a.TimePeriodID == TimePeriodlist[j].TimePeriodID).Select(a => a.Sales).Sum();
                }
                else
                {
                    TotalTimePeriodSale = resultProductTimeperiodList.Where(a => a.TimePeriodID == TimePeriodlist[j].TimePeriodID).Select(a => a.Unit).Sum();
                }
                Double Value = 0;

                MonthwiseSale objMonth = new MonthwiseSale()
                {
                    Unit = TotalTimePeriodSale,
                    Value = TotalTimePeriodSale,
                    MonthName = TimePeriodlist[j].MonthName
                };
                objMonthDataTotal.Add(objMonth);
                TotalCompanyGrandUnit += TotalTimePeriodSale;

                if (TotalPeriodDataValue.TryGetValue(TimePeriodlist[j].TimePeriodID, out Value))
                {

                    MonthwiseSale objMonthO = new MonthwiseSale()
                    {
                         Unit = TotalTimePeriodSale - Value,
                         Value = TotalTimePeriodSale - Value,
                        MonthName = TimePeriodlist[j].MonthName
                    };
                    objMonthDataOther.Add(objMonthO);
                    TotalCompanyOtherUnit += TotalTimePeriodSale - Value;
                }
                else
                {
                    MonthwiseSale objMonth0 = new MonthwiseSale()
                    {
                        Unit = TotalTimePeriodSale,
                        Value = TotalTimePeriodSale,
                        MonthName = TimePeriodlist[j].MonthName
                    };
                    objMonthDataOther.Add(objMonth0);
                    TotalCompanyOtherUnit += TotalTimePeriodSale;
                }

            }
            AnlaysisProductList objProductData2 = new AnlaysisProductList()
            {
                MonthData = objMonthDataOther,
                Prod_Code = 0,
                Brand = "Others",
                Rank = 0,
                TotalSale = TotalCompanyOtherUnit
            };
            objAnalysisProductList.Add(objProductData2);

            AnlaysisProductList objProductData1 = new AnlaysisProductList()
            {
                MonthData = objMonthDataTotal,
                Prod_Code = 0,
                Brand = "Grand Total",
                Rank = 0,
                TotalSale = TotalCompanyGrandUnit
            };
            objAnalysisProductList.Add(objProductData1);

            return objBarChartData;

        }

        public IList<SalesByPatch> GetSalesPatchDataListBrand(IList<BrickMaster> objBrickList, IList<AnlaysisProductInfo> result, IList<Int64> Top5BrandSales, int Type, Dictionary<Int64, string> ObjBrandColorCode, Int32 DataType)
        {
            IList<SalesByPatch> ObjSalesPatchDataList = new List<SalesByPatch>();
            ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
            string ProductType = "brand";
            if(DataType==2)
            {
                 ProductType = "sku";
            }
            IList<BrandDetails> SelectedbrandDetails = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", Top5BrandSales), ProductType);
            objCompanyDataAccess = null;

            string ColorCode = string.Empty;
            for (int j = 0; j < SelectedbrandDetails.Count; j++)
            {
                SalesByPatch objSalesPatchSales = new SalesByPatch();
                objSalesPatchSales.key = SelectedbrandDetails[j].Brand;
                ColorCode = string.Empty;
                if (ObjBrandColorCode.TryGetValue(SelectedbrandDetails[j].BrandID, out ColorCode))
                {
                    objSalesPatchSales.ColorCode = ColorCode;

                }
                else
                {
                    objSalesPatchSales.ColorCode = ColorCode;
                }
                IList<ValueDataSalesPatch> CompanyPatchDetailList = new List<ValueDataSalesPatch>();
                for (int i = 0; i < objBrickList.Count; i++)
                {
                    IList<AnlaysisProductInfo> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID && a.Prod_Code == SelectedbrandDetails[j].BrandID).ToList();

                    if (ObjBrickResult != null && ObjBrickResult.Count > 0)
                    {
                        var BrandResultPatch = from r in ObjBrickResult
                                                 group r by new
                                                 {
                                                     r.Prod_Code,
                                                     r.Brand
                                                 } into g
                                                 select new AnlaysisProductInfo
                                                 {
                                                     Prod_Code = g.Key.Prod_Code,
                                                     Brand = g.Key.Brand,
                                                     TotalSale = g.Sum(a => a.TotalSale),
                                                     TotalUnit = g.Sum(a => a.TotalUnit)
                                                 };


                        var BrandResultSalesPatch = BrandResultPatch.OrderByDescending(a => a.TotalSale).FirstOrDefault();
                        if (Type == 0)
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = BrandResultSalesPatch.TotalSale,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                        else
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = BrandResultSalesPatch.TotalUnit,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                    }
                    else
                    {
                        ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                        {
                            x = objBrickList[i].BrickName,
                            y = 0,
                            z = objBrickList[i].BrickID
                        };

                        CompanyPatchDetailList.Add(objValueDataSalesPatch);
                    }


                }
                objSalesPatchSales.values = CompanyPatchDetailList;
                ObjSalesPatchDataList.Add(objSalesPatchSales);
            }

            return ObjSalesPatchDataList;
        }

        public IList<TableListBrand> GetBrandPerformanceListTable(IList<AnlaysisProductInfo> result, IList<Int64> ProductListToadd, Analysis_IVM objAnlysis, Int32 Type, Int32 DataType, List<TimePeriod> UniqueTimeperiodList,Int32 DataProdctType)
        {
            IList<TableListBrand> CompanyPerformanceTableList = new List<TableListBrand>();
            try
            {
                string ProductType = "brand";
                if (DataProdctType == 2)
                {
                    ProductType = "sku";
                }
               // IList<Int32> AlreadyAddedCompany = new List<Int32>();

                // Code For color coding
                var BrandSales = from r in result
                                    group r by new
                                    {
                                        r.Prod_Code,
                                        r.Brand
                                    } into g
                                    select new AnlaysisProductInfo
                                    {
                                        Prod_Code = g.Key.Prod_Code,
                                        Brand = g.Key.Brand,
                                        TotalSale = g.Sum(a => a.TotalSale),
                                        TotalUnit = g.Sum(a => a.TotalUnit)
                                    };


                Double TotalMarketSale = result.Select(a => a.TotalSale).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnit).Sum();
                IList<Int64> UniqueBrandList = new List<Int64>();
                IList<Int64> UniqueBrandListData = new List<Int64>();
                if (DataType == 0)
                {
                    UniqueBrandListData = BrandSales.OrderByDescending(a => a.TotalSale).Select(a => a.Prod_Code).ToList();

                }
                else
                {
                    UniqueBrandListData = BrandSales.OrderByDescending(a => a.TotalUnit).Select(a => a.Prod_Code).ToList();

                }


                // UniqueBrandList.Add(0);
                Dictionary<Int64, Int32> BrandRankDetails = new Dictionary<Int64, int>();
                for (int i = 0; i < UniqueBrandListData.Count; i++)
                {
                    if (objAnlysis.includeTopSearches)
                    {

                        if (i < 10)
                        {
                            if (!UniqueBrandList.Contains(UniqueBrandListData[i]))
                            {
                                UniqueBrandList.Add(UniqueBrandListData[i]);

                            }
                            if (!BrandRankDetails.ContainsKey(UniqueBrandListData[i]))
                            {
                                BrandRankDetails.Add(UniqueBrandListData[i], i + 1);
                            }

                        }
                    }
                    if (ProductListToadd.Count > 0)
                    {
                        if (ProductListToadd.Contains(UniqueBrandListData[i]) && !UniqueBrandList.Contains(UniqueBrandListData[i]))
                        {
                            UniqueBrandList.Add(UniqueBrandListData[i]);
                            if (!BrandRankDetails.ContainsKey(UniqueBrandListData[i]))
                            {
                                BrandRankDetails.Add(UniqueBrandListData[i], i + 1);
                            }
                        }
                    }
                }
                

               // var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriodID }).Distinct().ToList();
                ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                IList<BrandDetails> BrandDetailsList = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", UniqueBrandList), ProductType);
                objCompanyDataAccess = null;
                Dictionary<Int32, Double> TotalPeriodDataValue = new Dictionary<int, double>();
                for (int j = 0; j < UniqueBrandList.Count; j++)
                {
                    TableListBrand objTableDataList = new TableListBrand();
                    objTableDataList.ID = UniqueBrandList[j];
                    BrandDetails objBrandDetails = BrandDetailsList.Where(a => a.BrandID == UniqueBrandList[j]).FirstOrDefault();
                    if (objBrandDetails != null)
                    {
                        objTableDataList.Brand = objBrandDetails.Brand;
                    }
                    Int32 RankCounter = 0;
                    if (BrandRankDetails.TryGetValue(UniqueBrandList[j], out RankCounter))
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    else
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    IList<AnlaysisProductInfo> BrandTimeperiodData = result.Where(a => a.Prod_Code == UniqueBrandList[j]).ToList();
                    IList<MonthData> objMonthDataList = new List<MonthData>();
                    Double TotalSales = 0;
                    for (int i = 0; i < UniqueTimeperiodList.Count; i++)
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = UniqueTimeperiodList[i].MonthYear;
                        var TimePeriodData = BrandTimeperiodData.Where(a => a.TimePeriodID == UniqueTimeperiodList[i].TimePeriodID).FirstOrDefault();
                        if (TimePeriodData != null)
                        {
                            if (DataType == 0)
                            {
                                objMonthData.Data = TimePeriodData.TotalSale;
                                TotalSales += TimePeriodData.TotalSale;
                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalSale;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalSale);
                                    }
                                }
                            }
                            else
                            {
                                objMonthData.Data = TimePeriodData.TotalUnit;
                                TotalSales += TimePeriodData.TotalUnit;
                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalUnit;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalUnit);
                                    }
                                }
                            }

                        }
                        else
                        {
                            objMonthData.Data = 0;

                        }
                        objMonthDataList.Add(objMonthData);
                    }
                    objTableDataList.MonthData = objMonthDataList;
                    objTableDataList.TotalValue = TotalSales;
                    CompanyPerformanceTableList.Add(objTableDataList);
                }



                IList<MonthData> objMonthDataTotalValue = new List<MonthData>();
                IList<MonthData> objMonthDataOtherValue = new List<MonthData>();
                Double TotalCompanyGrandValue = 0;
                Double TotalCompanyOtherGrandValue = 0;
                for (int j = 0; j < UniqueTimeperiodList.Count; j++)
                {
                    Double TotalTimePeriodSale = 0;
                    if (DataType == 0)
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriodID == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalSale).Sum();
                    }
                    else
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriodID == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalUnit).Sum();
                    }
                    MonthData objMonth = new MonthData()
                    {
                        Data = TotalTimePeriodSale,
                        MonthName = UniqueTimeperiodList[j].MonthYear
                    };
                    objMonthDataTotalValue.Add(objMonth);
                    TotalCompanyGrandValue += TotalTimePeriodSale;

                    Double Value = 0;
                    if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[j].TimePeriodID, out Value))
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale - Value,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale - Value;

                    }
                    else
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale;
                    }

                }


                TableListBrand objTableDataListotherTotal = new TableListBrand()
                {
                    ID = 0,
                    MonthData = objMonthDataOtherValue,
                    Brand = "Others",
                    Rank = 0,
                    TotalValue = TotalCompanyOtherGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListotherTotal);

                TableListBrand objTableDataListGrandTotal = new TableListBrand()
                {
                    ID = 0,
                    MonthData = objMonthDataTotalValue,
                    Brand = "Grand Total",
                    Rank = 0,
                    TotalValue = TotalCompanyGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListGrandTotal);





            }
            catch
            {
                throw;
            }

            return CompanyPerformanceTableList;
        }



        private Dictionary<Int64, string> GetBrandColorCode(IList<Int64> BrandList,  string SessionID, int FilterType)
        {
            // Dictionary<int, string> objCompanyColorCode = new Dictionary<int, string>();
                    

            SessionData objSessionData = new SessionData();
            if (!string.IsNullOrEmpty(SessionID))
            {
                UserDataAccess objUserDataAccess = new UserDataAccess();
                objSessionData = objUserDataAccess.GetSessionData(SessionID);
                objUserDataAccess = null;
            }
            string SessionColorCode = string.Empty;
            string UsedColorCode = string.Empty;

            switch (FilterType)
            {
                case 0:
                    SessionColorCode = objSessionData.CompanyColorCodes;
                    UsedColorCode = objSessionData.CompanyUsedColorCode;
                    break;
                case 1:
                    SessionColorCode = objSessionData.BrandColorCodes;
                    UsedColorCode = objSessionData.BrandUsedColorCode;
                    break;
                case 2:
                    SessionColorCode = objSessionData.SKUColorCodes;
                    UsedColorCode = objSessionData.SKUUsedColorCode;
                    break;
            }

            Dictionary<Int64, string> ObjSessionColorCode = new Dictionary<Int64, string>();
            if (!string.IsNullOrEmpty(SessionColorCode))
            {
                ObjSessionColorCode = JsonConvert.DeserializeObject<Dictionary<Int64, string>>(SessionColorCode);
            }


            IList<string> UsedColorCodeArray = new List<string>();
            if (!string.IsNullOrEmpty(UsedColorCode))
            {
                UsedColorCodeArray = UsedColorCode.Split(',').ToList();
            }
            // int ColorCodeLengthCounter = ColorCode.Length;
            bool IsJsonUpdate = false;
            for (int i = 0; i < BrandList.Count; i++)
            {
                if (!ObjSessionColorCode.ContainsKey(BrandList[i]))
                {                   

                        string CurrentColorCode = this.GetColorCode(UsedColorCodeArray);
                        ObjSessionColorCode.Add(BrandList[i], CurrentColorCode);
                        IsJsonUpdate = true;
                        UsedColorCodeArray.Add(CurrentColorCode);
                    

                }
            }

            if (IsJsonUpdate)
            {

                // Covert List to string 
                SessionColorCode = JsonConvert.SerializeObject(ObjSessionColorCode);

                UsedColorCode = string.Join(",", UsedColorCodeArray);
                switch (FilterType)
                {
                    case 0:
                        objSessionData.CompanyColorCodes = SessionColorCode;
                        objSessionData.CompanyUsedColorCode = UsedColorCode;
                        break;
                    case 1:

                        objSessionData.BrandColorCodes = SessionColorCode;
                        objSessionData.BrandUsedColorCode = UsedColorCode;
                        break;
                    case 2:

                        objSessionData.SKUColorCodes = SessionColorCode;
                        objSessionData.SKUUsedColorCode = UsedColorCode;
                        break;
                }

                // Update the data with Used ColorCode
                UserDataAccess objUserDataAccess = new UserDataAccess();
                bool status = objUserDataAccess.UpdateSessionData(SessionID, objSessionData);
                objUserDataAccess = null;
            }

            return ObjSessionColorCode;

        }

        private string GetColorCode(IList<string> UsedColorCode )
        {
            string CurrentColorCode = string.Empty;
            string[] ColorCode = new string[] { "#8D6EB1","#53C2B4","#A3CF5A","#093A1F","#D6D627","#BDB268","#ECB32B","#523973","#F58020","#D82729","#184376","#41F447","#2784A3","#47D1FF","#FC4E77","#9F247F","#F4F441","#F441D6","#8B41F4","#41F4F4","#721B25","#6E7272","#CC0052","#BA6E05","#600080","#00FFFF","#00E68A","#ffff00","#4B5320","#FF9966","#98777B","#00FF6F","#BDB76B","#E9967A","#008000","#FFD1DC","#D1E231","#826644","#C0C0C0","#800080","#000080","#8b4513","#ff0000","#ff6347","#E8ADAA",
"#9400D3","#DFFF00","#FF8C00","#B38481" };

            bool IsColorCodeFound = false;
            for (int i = 0; i < ColorCode.Length; i++)
            {
                if (!UsedColorCode.Contains(ColorCode[i]))
                {
                    IsColorCodeFound = true;
                    return ColorCode[i];
                }
            }
            if (!IsColorCodeFound)
            {
                CurrentColorCode = "#" + RandomString(6);
            }
            return CurrentColorCode;
        }

        private static Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "ABCDEF0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


    }
}
