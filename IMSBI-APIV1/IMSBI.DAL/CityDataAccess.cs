﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMSBI.Common;
using IMSBI.ViewModel;

namespace IMSBI.DAL
{
    public class CityDataAccess
    {
        public string GetCityNameByCityIds(string CityIDs)
        {
            string LocationList = string.Empty;
            try
            {
                IList<string> CityNameList = new List<string>();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCityNameBasedOnCityIDs + " '" + CityIDs + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select Convert.ToString(row["CityName"])
                              ).ToList();

                CityNameList = result;
                for (int i = 0; i < CityNameList.Count; i++)
                {
                    LocationList += "<span class=\"label label-primary sub-locations\">"+ CityNameList[i] + "</span>";
                }
    
            
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return LocationList;
        }


        public IList<cityMaster> GetCityListByCompany(int CompanyID, int UserAccessType, string CityIDs)
        {
            IList<cityMaster> CityList = new List<cityMaster>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCityByCompany + " " + CompanyID + "," + UserAccessType + ",'"+ CityIDs +"'" ;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new cityMaster
                              {
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  CityName = Convert.ToString(row["CityName"]),

                              }
                             ).ToList();

                CityList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CityList;
        }


        public IList<CityData> GetCityData()
        {
            IList<CityData> objCityList = new List<CityData>();
            try
            {
                IList<string> CityNameList = new List<string>();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCityDetails;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CityData
                              {
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  Lat = Convert.ToDecimal(row["Lat"]),
                                  longu = Convert.ToDecimal(row["Long"])

                              }
                            ).ToList();

                objCityList = result;
              
    
            
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objCityList;
        }

        public IList<SubscribedCityData> GetSubscribedCityData(string CityIDs, string PeriodType, Int32 StartPeriod, Int32 EndPeriod)
        {
            

                 IList<SubscribedCityData> objCityList = new List<SubscribedCityData>();
            try
            {
                IList<string> CityNameList = new List<string>();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCityTotalSales + " '"+CityIDs + "','" +  PeriodType + "'," + StartPeriod + "," + EndPeriod;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SubscribedCityData
                              {
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  TotalSales = Convert.ToDouble(row["TotalSales"]),
                                  TotalUnits = Convert.ToDouble(row["TotalUnits"])

                              }
                            ).ToList();

                objCityList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objCityList;
        }

        public IList<CompanySalesData> GetCompanySalesData(string CityIDs, string PeriodType, Int32 StartPeriod, Int32 EndPeriod)
        {


            IList<CompanySalesData> objCompanyList = new List<CompanySalesData>();
            try
            {
               
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanySalesData + " '" + CityIDs + "'," + StartPeriod + "," + EndPeriod + ",'" + PeriodType + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanySalesData
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  TotalSales = Convert.ToDouble(row["TotalSales"]),
                                  TotalUnits = Convert.ToDouble(row["TotalUnit"])

                              }
                            ).ToList();

                objCompanyList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objCompanyList;
        }

        public IList<BrandSalesData> GetBrandSalesData(string CityIDs, string PeriodType, Int32 StartPeriod, Int32 EndPeriod)
        {


            IList<BrandSalesData> objBrandList = new List<BrandSalesData>();
            try
            {
                
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetBrandSalesData + " '" + CityIDs + "'," + StartPeriod + "," + EndPeriod + ",'" + PeriodType + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrandSalesData
                              {
                                  BrandID = Convert.ToInt32(row["BrandID"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  BrandName = Convert.ToString(row["Brand"]),
                                  TotalSales = Convert.ToDouble(row["TotalSales"]),
                                  TotalUnits = Convert.ToDouble(row["TotalUnit"])

                              }
                            ).ToList();

                objBrandList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objBrandList;
        }
        public IList<SupergroupSalesData> GetSupergroupSalesData(string CityIDs, string PeriodType, Int32 StartPeriod, Int32 EndPeriod)
        {


            IList<SupergroupSalesData> objSupergroupList = new List<SupergroupSalesData>();
            try
            {
                IList<string> CityNameList = new List<string>();
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSupergroupSalesData + " '" + CityIDs + "'," + StartPeriod + "," + EndPeriod + ",'" + PeriodType + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SupergroupSalesData
                              {
                                  SupergroupID = Convert.ToInt32(row["SupergroupID"]),
                                  SupergroupName = Convert.ToString(row["SupergroupName"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  TotalSales = Convert.ToDouble(row["TotalSales"]),
                                  TotalUnits = Convert.ToDouble(row["TotalUnit"])

                              }
                            ).ToList();

                objSupergroupList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objSupergroupList;
        }

        


    }



}
