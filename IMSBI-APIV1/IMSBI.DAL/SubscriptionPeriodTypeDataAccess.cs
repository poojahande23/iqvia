﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class SubscriptionPeriodTypeDataAccess
    {
        public IList<SubscriptionPeriodType> GetSubscriptionPeriodType()
        {
            IList<SubscriptionPeriodType> SubscriptionTypeList = new List<SubscriptionPeriodType>();
            try
            {
                DataSet resultIDs;
                DALUtility IMSDB = new DALUtility();
                string sqlString = "Exec " + IMSBI.Common.Constants.DB_SP_GetSubscriptionPeriodTypeList;
                resultIDs = IMSDB.ReturnDataset(sqlString);
                var result = (from row in resultIDs.Tables[0].AsEnumerable()
                              select new SubscriptionPeriodType
                              {
                                  SubscriptionPeriodTypeName = Convert.ToString(row["SubscriptionPeriodTypeName"]),
                                  SubscriptionPeriodTypeID = Convert.ToInt32(row["SubscriptionPeriodTypeID"]),
                                  Active = Convert.ToBoolean(row["Active"])
                              }).ToList();

                SubscriptionTypeList = result;
            }
            catch
            {
                throw;
            }

            return SubscriptionTypeList;
        }

        public IList<SubscriptionPeriodType> GetCompanySubscriptionPeriodType(Int32 CompanyID)
        {
            IList<SubscriptionPeriodType> SubscriptionTypeList = new List<SubscriptionPeriodType>();
            try
            {

                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                CompanySubscripedPeriodType objCompanySubscriped = objCompanyDataAccess.GetCompanySubscribedPeridoType(CompanyID);
                objCompanyDataAccess = null;
                IList<string> SubscribedPeriod = new List<string>();
               
                if(!string.IsNullOrEmpty(objCompanySubscriped.SubscribedPeriodType) )
                {
                    SubscribedPeriod = objCompanySubscriped.SubscribedPeriodType.Split(',');
                }

                DataSet resultIDs;
                DALUtility IMSDB = new DALUtility();
                string sqlString = "Exec " + IMSBI.Common.Constants.DB_SP_GetSubscriptionPeriodTypeList;
                resultIDs = IMSDB.ReturnDataset(sqlString);
                var result = (from row in resultIDs.Tables[0].AsEnumerable()
                              select new SubscriptionPeriodType
                              {
                                  SubscriptionPeriodTypeName = Convert.ToString(row["SubscriptionPeriodTypeName"]),
                                  SubscriptionPeriodTypeID = Convert.ToInt32(row["SubscriptionPeriodTypeID"]),
                                  Active = Convert.ToBoolean(row["Active"])
                              }).ToList();

                SubscriptionTypeList = result;
                foreach(SubscriptionPeriodType obj in SubscriptionTypeList)
                {
                    if(!SubscribedPeriod.Contains(obj.SubscriptionPeriodTypeID.ToString()))
                    {
                        obj.Active = false;
                    }
                    
                }
            }
            catch
            {
                throw;
            }

            return SubscriptionTypeList;
        }


        public IList<SubscriptionPeriodType> GetSubscribedCompanyPeriods(string SubscripedPeriodIDs)
        {
            IList<SubscriptionPeriodType> SubscriptionTypeList = new List<SubscriptionPeriodType>();
            try
            {
                DataSet resultIDs;
                DALUtility IMSDB = new DALUtility();
                string sqlString = "Exec " + IMSBI.Common.Constants.DB_SP_GetSubscribedCompanyPeriods + " '" + SubscripedPeriodIDs + "'";
                resultIDs = IMSDB.ReturnDataset(sqlString);
                var result = (from row in resultIDs.Tables[0].AsEnumerable()
                              select new SubscriptionPeriodType
                              {
                                  SubscriptionPeriodTypeName = Convert.ToString(row["SubscriptionPeriodTypeName"]),
                                  SubscriptionPeriodTypeID = Convert.ToInt32(row["SubscriptionPeriodTypeID"]),
                                  PeriodShortName = Convert.ToString(row["PeriodShortName"]),
                                  Active = Convert.ToBoolean(row["Active"])
                              }).ToList();

                SubscriptionTypeList = result;
            }
            catch
            {
                throw;
            }

            return SubscriptionTypeList;
        }


    }
}
