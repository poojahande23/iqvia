﻿using IMSBI.ViewModel;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class UserLogAccessDetails
    {
        public IList<AccessLogReport_OVM> GetUserUserLog(AccessLogReport_IVM objUserLog)
        {
            IList<AccessLogReport_OVM> AccessLogList = new List<AccessLogReport_OVM>();
              DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (objUserLog != null)
                {
                    param = objUserLog.CompanyID + ",";
                  
                    param += "'" + objUserLog.StartDate.ToString("yyyy-MM-dd HH:mm:ss") + "',";
                  
                    param += "'" + objUserLog.EndDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                  


                }
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetUserAccesslog + " " + param;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new AccessLogReport_OVM
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  MenuID = Convert.ToInt32(row["MenuID"]),
                                  MenuName = Convert.ToString(row["MenuName"]),
                                  DataType = Convert.ToString(row["DataType"]),
                                  CategoryType = Convert.ToString(row["CategoryType"]),
                                  CategoryValue = Convert.ToString(row["CategoryValue"]),
                                  TCLevel = Convert.ToString(row["TCLevel"]),
                                  TCCode = Convert.ToString(row["TCCode"]),
                                  BrandID = Convert.ToString(row["BrandID"]),
                                  ProductID = Convert.ToString(row["ProductID"]),
                                  LogDate = Convert.ToString(row["LogDate"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  StartPeriod = Convert.ToInt32(row["StartPeriod"]),
                                  EndPeriod = Convert.ToInt32(row["EndPeriod"]),
                                  PatchID = Convert.ToString(row["PatchID"]),
                                  SubscriptionPeriodTypeName = Convert.ToString(row["SubscriptionPeriodTypeName"]),
                                  LoginTime = Convert.ToString(row["LoginTime"]),
                                  LogoutTime = Convert.ToString(row["LogoutTime"]),

                              }
                              ).ToList();

                AccessLogList = result;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return AccessLogList;
        }

        public int AddUpdateUserLog(UserLog_VM objUserLog)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (objUserLog != null)
                {
                    param = objUserLog.UserID + ",";
                    param += objUserLog.MenuID + ",";
                    param += "'" + objUserLog.PeriodType + "',";
                    param += objUserLog.StartPeriod + ",";
                    param += objUserLog.EndPeriod + ",";
                    param += objUserLog.CityID + ",";
                    param += "'" + objUserLog.CategoryType + "',";
                    param += "'" + objUserLog.CategoryValue + "',";
                    param += "'" + objUserLog.PatchID + "',";
                    param += "'" + objUserLog.DataType + "',";
                    param += "'" + objUserLog.TCLevel + "',";
                    param += "'" + objUserLog.TCCode + "',";
                    param += "'" + objUserLog.BrandID + "',";
                    param += "'" + objUserLog.ProductID + "',";
                    param += "'" + objUserLog.SessionID + "'";



                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUserAccessLog + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public void InsertUpdateUserlogin(Int32 UserID, string SessionID, bool isLogin)
        {
            DALUtility IMSBIDB = new DALUtility();
            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_InsertUpdateUserLogin + " " + UserID + ",'" +SessionID+ "'," + isLogin;
            var obj = IMSBIDB.ReturnValue(str);
        }
    }
}
