﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class AccessRightsDataAccess
    {
        public CompanyAccessRights GetCompanyAccessRights(Int32 CompanyID)
        {
            CompanyAccessRights objCompanyAccessRights = new CompanyAccessRights();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyAccessRights + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyAccessRights
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  AccessRightType = Convert.ToInt32(row["AccessType"]),
                                  TCValues = Convert.ToString(row["TCValues"]),
                                  BrandIDs = Convert.ToString(row["BrandIDs"])
                                  
                              }
                             ).FirstOrDefault();

                objCompanyAccessRights = result;
                if(objCompanyAccessRights == null)
                {
                    objCompanyAccessRights = new CompanyAccessRights();
                    objCompanyAccessRights.CompanyID = CompanyID;
                    objCompanyAccessRights.AccessRightType = 1;
                    objCompanyAccessRights.TCValues = "";
                }
            }
            catch
            {
                throw;
            }
           

            return objCompanyAccessRights;

        }

        public UserAccessRights GetUserAccessRights(Int32 UserID)
        {
            UserAccessRights objUserAccessRights = new UserAccessRights();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetUserAccessRights + " " + UserID;
                resultds = IMSBIDB.ReturnDataset(str);

                if (resultds.Tables.Count > 0)
                {
                    if (resultds.Tables[0].Rows.Count > 0)
                    {

                        var result = (from row in resultds.Tables[0].AsEnumerable()
                                      select new UserAccessRights
                                      {
                                          UserID = Convert.ToInt32(row["UserID"]),
                                          AccessRightType = Convert.ToInt32(row["AccessRightType"]),
                                          TCValues = Convert.ToString(row["TCValues"]),
                                          CityIDs = Convert.ToString(row["CityIDs"])

                                      }
                             ).FirstOrDefault();

                        objUserAccessRights = result;
                    }
                }
                if (objUserAccessRights != null && objUserAccessRights.AccessRightType == 2 )
                {
                    UserDataAccess objUserDataAccess = new UserDataAccess();
                    UserInfo objUserInfo = objUserDataAccess.UserInfoDetails(UserID);
                    objUserDataAccess = null;
                    if (objUserInfo != null)
                    {
                        CompanyAccessRights objCompanyAccessRight = this.GetCompanyAccessRights(objUserInfo.CompanyID);
                        if (objCompanyAccessRight != null && string.IsNullOrEmpty(objUserAccessRights.TCValues))
                        {
                            objUserAccessRights.TCValues = objCompanyAccessRight.TCValues;
                        }
                       

                        if (string.IsNullOrEmpty(objUserAccessRights.CityIDs))
                        {
                            CityDataAccess objCityDataAccess = new CityDataAccess();
                            IList<cityMaster> CityList = objCityDataAccess.GetCityListByCompany(objUserInfo.CompanyID, 1, "");
                            IList<Int32> CityIDS = new List<Int32>();
                            for (int j = 0; j < CityList.Count; j++)
                            {
                                CityIDS.Add(CityList[j].CityID);
                            }
                            objUserAccessRights.CityIDs = string.Join(",", CityIDS);
                            objCityDataAccess = null;
                        }
                    }

                }

            }
            catch
            {
                throw;
            }


            return objUserAccessRights;

        }


        public int CreateUpdateUserAccessRights(Int32 UserID, int AccessType, string TCValue, string CityIds)
        {
            int status = 0;
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                var param = string.Empty;

                param = UserID + ",";
                param += AccessType + ",";
                param += "'" + TCValue + "',";
                param += "'" + CityIds + "';";
               



                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateUserAccessRights + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                status = Convert.ToInt32(obj);

            }
            catch
            {
                throw;
            }
            
            return status;
        }

        public int UpdateUserAccessRights(Int32 UserID, int AccessType, string TCValue, string CityIds)
        {
            int status = 0;
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                var param = string.Empty;

                param = UserID + ",";
                param += AccessType + ",";
                param += "'" + TCValue + "',";
                param += "'" + CityIds + "';";
          



                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_UpdateUserAccessRights + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                status = Convert.ToInt32(obj);

            }
            catch
            {
                throw;
            }

            return status;
        }
        public int CreateUpdateCompanyAccessRights(CompanyAccessRights objCompanyAccessRights)
        {
            int status = 0;
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                var param = string.Empty;
                
                    param = objCompanyAccessRights.CompanyID + ",";
                    param += objCompanyAccessRights.AccessRightType +  ",";
                    param += "'" + objCompanyAccessRights.TCValues + "',";
                    param += "'" + objCompanyAccessRights.BrandIDs + "'";
                   


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateCompanyAccessRights + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                status = Convert.ToInt32(obj);

            }
            catch
            {
                throw;
            }

            return status;
        }

        public bool DeleteUserAccessRights(Int32 UserID)
        {
            bool Status = false;
            DataSet resultds;
            DALUtility IMSBIDB = new DALUtility();


            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteUserAccessRights + " " + UserID;
            var obj = IMSBIDB.ReturnValue(str);
            Status = Convert.ToBoolean(obj);

            return Status;
        }

        public bool DeleteCompanyAccessRights(Int32 CompanyID)
        {
            bool Status = false;
            DataSet resultds;
            DALUtility IMSBIDB = new DALUtility();


            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteCompanyAccessRights + " " + CompanyID;
            var obj = IMSBIDB.ReturnValue(str);
            Status = Convert.ToBoolean(obj);

            return Status;
        }
    }
}
