﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class ProductDataAccess
    {
        public IList<Product> GetProductsByCompanyDivison(int CompanyID, int DivisionID, Int32 UserAccessType, string TCCodes)
        {
            IList<Product> ProductList = new List<Product>();
            try
            {
               
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetProductsByCompanyDivison + " " + CompanyID + ","+ DivisionID + "," + UserAccessType + ",'" + TCCodes + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Product
                              {
                                  PFCID = Convert.ToInt64(row["PFCID"]),
                                  ProductName = Convert.ToString(row["Pack_Desc"]),

                              }
                             ).ToList();

                ProductList = result;
               


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ProductList;
        }

        public IList<Product> DB_SP_GetFocusedProductsByCompanyDivison(int CompanyID, int DivisionID, int UserAccessType,string TCCodes)
        {
            IList<Product> ProductList = new List<Product>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetFocusedProductsByCompanyDivison + " " + CompanyID + "," + DivisionID + "," + UserAccessType + ",'" + TCCodes + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Product
                              {
                                  PFCID = Convert.ToInt32(row["BrandID"]),
                                  ProductName = Convert.ToString(row["BrandName"]),

                              }
                             ).ToList();

                ProductList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ProductList;
        }

        public IList<Product> GetNewIntroductionProductsByCompanyDivison(int CompanyID, int DivisionID, int UserAccessType, string TCValues)
        {
            IList<Product> ProductList = new List<Product>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetNewIntroductionProductsByCompanyDivison + " " + CompanyID + "," + DivisionID + "," + UserAccessType +",'"+ TCValues + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Product
                              {

                                  PFCID = Convert.ToInt64(row["PFCID"]),
                                  ProductName = Convert.ToString(row["Pack_Desc"]),
                                 

                              }
                             ).ToList();

                ProductList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ProductList;
        }

        public IList<BrandDetails> GetZeroValueBrandDetails(string BrandIDs, string Type)
        {


            IList<BrandDetails> BrandList = new List<BrandDetails>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetZeroValueBrandList + " '" + BrandIDs + "','" + Type+"'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrandDetails
                              {
                                  BrandID = Convert.ToInt64(row["BrandID"]),
                                  Brand = Convert.ToString(row["Brand"]),
                                  CompanyName = Convert.ToString(row["CompanyName"])


                              }
                              ).ToList();

                BrandList = result;
                IMSBIDB = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return BrandList;
        }

        public IList<Division_OVM> GetDivisionProducts(int CompanyID, int DivisionID)
        {
            IList<Division_OVM> ProductList = new List<Division_OVM>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDivisionProducts + " " + CompanyID + "," + DivisionID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Division_OVM
                              {
                                  PFCID = Convert.ToInt64(row["PFCID"]),
                                  ProductName = Convert.ToString(row["Pack_Desc"]),
                                  IsNewIntroduction = Convert.ToInt32(row["IsNewIntroduction"]),
                                  IsOnFocus = Convert.ToInt32(row["IsOnFocus"]),
                                  Manuf_Name = Convert.ToString(row["Manufact_desc"]),
                                  TC4Name = Convert.ToString(row["TCCode"])

                              }
                             ).ToList();

                ProductList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ProductList;
        }

        public IList<ProductData> GetProductsByBrand(string BrandIds)
        {
            IList<ProductData> ProductList = new List<ProductData>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetProductsByBrand + " '" + BrandIds + "'" ;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new ProductData
                              {
                                  ProductID = Convert.ToInt64(row["PFCID"]),
                                  ProductName = Convert.ToString(row["Pack_Desc"]),
                                  TCCode = Convert.ToString(row["TCCode"])

                              }
                             ).ToList();

                ProductList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ProductList;
        }

        public IList<BrandMaster> GetProductListByTherapy(string TCCode, string TherapyID, string TCValues, Int32 AccessType)
        {


            IList<BrandMaster> BrandList = new List<BrandMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetProductByTherapy + " '" + TherapyID + "','" + TCCode + "'," + AccessType + ",'" + TCValues + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrandMaster
                              {
                                  BrandID = Convert.ToInt64(row["BrandID"]),
                                  BrandName = Convert.ToString(row["Brand"])

                              }
                              ).ToList();

                BrandList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return BrandList;
        }
    }

}
