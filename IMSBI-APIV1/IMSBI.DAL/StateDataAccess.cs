﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class StateDataAccess
    {
        public IList<StateMaster> GetStateListByCompany(int CompanyID)
        {
            IList<StateMaster> StateList = new List<StateMaster>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetStateByCompany + " " + CompanyID ;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new StateMaster
                              {
                                  StateID = Convert.ToInt32(row["StateID"]),
                                  StateName = Convert.ToString(row["StateName"]),

                              }
                             ).ToList();

                StateList = result;



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return StateList;
        }
    }
}
