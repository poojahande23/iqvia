﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class  TherapyDataAccess
    {
        public IList<ProductData> GetProductsByTherapyCode(string TCCode)
        {


            IList<ProductData> objProductList = new List<ProductData>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetProductsByTherapy + " '" + TCCode + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new ProductData
                              {
                                  ProductID = Convert.ToInt64(row["PFC_ID"]),
                                  ProductName = Convert.ToString(row["ProductName"])

                              }
                              ).ToList();

                objProductList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objProductList;
        }


        public DataSet GetTherapyPerformance(Comparison_IVM objComparision, int AccessType, string TCCodes)
        {
            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            try
            {
                if (objComparision.DataType == "npm")
                {
                    string stringSQLString = string.Empty;
                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":                           
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancemat + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancematTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }                           
                            break;
                        case "mth":                            
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancemth + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancemthTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            break;
                        case "ytdf":
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }                           
                            break;
                        case "ytdc":                          
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdc + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdcTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }                          
                            break;
                        case "qtr":                          
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceQtr + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceqtrTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                           
                            break;
                        case "hlf":                           
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancehlf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancehlfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }                            
                            break;

                    }

                    if (!string.IsNullOrEmpty(stringSQLString))
                    {
                        resultds = IMSBIDB.ReturnDataset(stringSQLString);
                    }
                }
                else
                {
                    string stringSQLString = string.Empty;
                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":                           
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancematCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancematTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            
                            break;
                        case "mth":                            
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancemthCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancemthTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            
                            break;
                        case "ytdf":                            
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            break;
                        case "ytdc":
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdcCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceytdcTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLString);
                            break;
                        case "qtr":
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceQtrCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformanceqtrTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            break;
                        case "hlf":                           
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancehlfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLString = "Exec" + " " + IMSBI.Common.Constants.DB_SP_TherapyPerformancehlfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                          
                            break;

                    }
                    if (!string.IsNullOrEmpty(stringSQLString))
                    {
                        resultds = IMSBIDB.ReturnDataset(stringSQLString);
                    }

                }

            }
            catch
            {

                throw;
            }

            return resultds;
        }

    }
}
