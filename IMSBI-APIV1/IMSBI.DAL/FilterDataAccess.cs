﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class FilterDataAccess
    {
        public IList<BrickMaster> GetPatchList(Int32 GeographyID, string PatchIDs)
        {
            IList<BrickMaster> objBrickMasterList = new List<BrickMaster>();
            try
            {
                DataSet resultds = new DataSet();
                DALUtility IMSBIDB = new DALUtility();

                // Get the list of Patch List for Filter
                string stringPathList = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetPatchList + " " + GeographyID + ",'"+ PatchIDs + "'";
                DataSet resultPatchList = IMSBIDB.ReturnDataset(stringPathList);

                var objtPathcList = (from row in resultPatchList.Tables[0].AsEnumerable()
                                     select new BrickMaster
                                     {
                                         BrickID = Convert.ToInt32(row["BrickID"]),
                                         BrickName = Convert.ToString(row["BrickName"])

                                     }
                              ).ToList();

                objBrickMasterList = objtPathcList;
            }
            catch
            {
                throw;
            }
            return objBrickMasterList;
        }

        public IList<TimePeriodData> GetTimePeriodList(string PeriodType, Int32 PeriodStart, Int32 PeriodEnd)
        {
            IList<TimePeriodData> objTimePeriodList = new List<TimePeriodData>();
            try
            {
                DataSet resultdTimePeriod;
                DALUtility IMSBIDB = new DALUtility();
                string strPeriod = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTimePeriodListBasedOnPeriodType + " '" + PeriodType + "'," + PeriodStart + "," + PeriodEnd;
                resultdTimePeriod = IMSBIDB.ReturnDataset(strPeriod);


                var resultTimePeriod = (from row in resultdTimePeriod.Tables[0].AsEnumerable()
                                        select new TimePeriodData
                                        {
                                            TimePeriodID = Convert.ToInt32(row["TimePeriodID"]),
                                            Year = Convert.ToInt32(row["Year"]),
                                            Month = Convert.ToInt32(row["Month"]),
                                            MonthName = Convert.ToString(row["MonthName"]),

                                        }
                       ).ToList();

            }
            catch
            {
                throw;
            }

            return objTimePeriodList;
        }
        public IList<string> GetTCCodebySuperGroup(string Supergroups)
        {


            IList<string> objTcCode = new List<string>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTCCodebySuperGroup + " '" + Supergroups + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select Convert.ToString(row["TherapyCode"])
                              ).ToList();

                objTcCode = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objTcCode;
        }

        public IList<DataSubscriptionType> GetDataSubscriptionPeriodType()
        {
            IList<DataSubscriptionType> objDataSubscriptionTypeList = new List<DataSubscriptionType>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDataSubscriptionPeriodType;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                                           select new DataSubscriptionType
                                           {
                                               DataSubscriptionTypeID = Convert.ToInt32(row["DataSubscriptionPeriodTypeID"]),
                                               DataSubscriptionTypeName = Convert.ToString(row["SubscriptionPeriodName"]),
                                               DataSubscriptionTypeShortName = Convert.ToString(row["ShotName"])
                                               

                                           }).ToList();

                objDataSubscriptionTypeList = result;
            }
            catch
            {

            }
            return objDataSubscriptionTypeList;
        }

        public IList<DataSubscriptionType> GetDataSubscriptionPeriodTypeByCompany(int CompanyID)
        {
            IList<DataSubscriptionType> objDataSubscriptionTypeList = new List<DataSubscriptionType>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDataSubscriptionPeriodTypeForCompany +" " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new DataSubscriptionType
                              {
                                  DataSubscriptionTypeID = Convert.ToInt32(row["DataSubscriptionPeriodTypeID"]),
                                  DataSubscriptionTypeName = Convert.ToString(row["SubscriptionPeriodName"]),
                                  DataSubscriptionTypeShortName = Convert.ToString(row["ShotName"])


                              }).ToList();

                objDataSubscriptionTypeList = result;
            }
            catch
            {

            }
            return objDataSubscriptionTypeList;
        }

        public string GetCityNameByCityID(Int32 CityID)
        {
            string CityName = string.Empty;

            DALUtility IMSBIDB = new DALUtility();         
            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCityNameByCityID + " " + CityID ;
             CityName =  IMSBIDB.ReturnValue(str).ToString();


            return CityName;

        }


        public IList<pincoderawdata> GetPincodeCoordinate(Int32 CityID, Int32 CompanyID, Int32 DivisionID, string PatchIDs)
        {
            IList<pincoderawdata> objPincodeRawData = new List<pincoderawdata>();
            try
            {
                DataSet resultds = new DataSet();
                DALUtility IMSBIDB = new DALUtility();

                // Get the list of Patch List for Filter
                string stringPathList = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetPincodeCoordinate + " " + CityID + "," + CompanyID +", "+ DivisionID + ", '" + PatchIDs + "'";
                DataSet resultPatchList = IMSBIDB.ReturnDataset(stringPathList);

                var objtPathcList = (from row in resultPatchList.Tables[0].AsEnumerable()
                                     select new pincoderawdata
                                     {
                                         BrickID = Convert.ToInt32(row["BrickID"]),
                                         BrickName = Convert.ToString(row["BrickName"]),
                                         Pincode  = Convert.ToInt32(row["Pincode"]),
                                         Lat = Convert.ToDouble(row["Lat"]),
                                         Long = Convert.ToDouble(row["long"]),

                                     }
                              ).ToList();

                objPincodeRawData = objtPathcList;
            }
            catch
            {
                throw;
            }

            return objPincodeRawData;
        }
    }
}
