﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
   public class DivisionDataAccess
    {
        public DivisionInfo GetDivisionInfo(int DivisionID)
        {
            DivisionInfo objDivisionInfo = new DivisionInfo();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDivisionInfo + " " + DivisionID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new DivisionInfo
                              {
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  DivisionName = Convert.ToString(row["DivisionName"]),
                                  DataSubscriptionTypeID = !string.IsNullOrEmpty(Convert.ToString(row["DataSubscriptionPeriodTypeID"]))? Convert.ToInt32(row["DataSubscriptionPeriodTypeID"]) : 1,
                                  SubscribedPeriodTypeIDs = Convert.ToString(row["SubscriptionPeriodTypeIDs"]),
                                  DefaultPeriodTypeID = !string.IsNullOrEmpty(Convert.ToString(row["DefaultPeriodTypeID"]) )? Convert.ToInt32(row["DefaultPeriodTypeID"]):1,



                              }
                              ).FirstOrDefault();

                objDivisionInfo = result;

            }
            catch
            {
                throw;
            }

            return objDivisionInfo;
        }

        public IList<DivisionBrand_OVM> GetDivisionBrandList(int DivisionID, int CompanyID)
        {
            IList<DivisionBrand_OVM> objDivisionBrandList = new List<DivisionBrand_OVM>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDivisionBrandList + " " + DivisionID +","+ CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new DivisionBrand_OVM
                              {
                                  BrandID = Convert.ToInt32(row["BrandID"]),
                                  BrandName = Convert.ToString(row["BrandName"])
                              }
                              ).ToList();

                objDivisionBrandList = result;

            }
            catch
            {
                throw;
            }

            return objDivisionBrandList;
        }


        public int AddUpdateCompanyDivision(Division_IVM objDivision)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (objDivision != null)
                {
                    param = objDivision.DivisionID + ",";
                    param += objDivision.CompanyID + ",";
                    param += "'" + objDivision.DivisionName + "',";
                    param += objDivision.DataSubscriptionTypeID + ",";
                    param += "'" + objDivision.SubscribedPeriodTypeIDs + "',";
                    param += objDivision.DefaultPeriodTypeID;



                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateCompanyDivision + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627) // <-- but this will
                {
                    result = -1;
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

        public int AddDivisionProductMapping(DivisionProductMapping objDivisionProductMapping)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (objDivisionProductMapping != null)
                {
                    param = objDivisionProductMapping.DivisionID + ",";
                    param += objDivisionProductMapping.CompanyID + ",";
                    param += objDivisionProductMapping.PFCID + ",";
                    param += objDivisionProductMapping.IsOnFocus + ",";
                    param += objDivisionProductMapping.IsNewIntroduction ;

                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_AddDivisionProductMapping + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

      

        public BrandGroup GetBrandGroupInfo(int BrandGroupID)
        {
            BrandGroup objBrandGroup = new BrandGroup();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetBrandGroupInfo + " " + BrandGroupID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrandGroup
                              {
                                  BrandGroupID = Convert.ToInt32(row["BrandGroupID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  BrandGroupName = Convert.ToString(row["BrandGroupName"]),
                                  BrandIDs = Convert.ToString(row["BrandIDs"])

                              }
                              ).FirstOrDefault();

                objBrandGroup = result;

            }
            catch
            {
                throw;
            }

            return objBrandGroup;
        }

        public IList<BrandGroup> GetBrandGroupList(int DivisionID, int CompanyID)
        {
            IList<BrandGroup> objBrandGroupList = new List<BrandGroup>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetBrandGroupListByDivision + " " + DivisionID +","+ CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrandGroup
                              {
                                  BrandGroupID = Convert.ToInt32(row["BrandGroupID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  BrandGroupName = Convert.ToString(row["BrandGroupName"]),
                                  BrandIDs = Convert.ToString(row["BrandIDs"])

                              }
                              ).ToList();

                objBrandGroupList = result;

            }
            catch
            {
                throw;
            }

            return objBrandGroupList;
        }

        public int AddUpdateBrandGroup(BrandGroup objBrandData)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (objBrandData != null)
                {
                    param = objBrandData.BrandGroupID + ",";
                    param += objBrandData.CompanyID + ",";
                    param += objBrandData.DivisionID + ",";
                    param += "'" + objBrandData.BrandGroupName+ "',";
                    param += "'" + objBrandData.BrandIDs + "'";


                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateBrandGroup + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch(SqlException ex)
            {
                if (ex.Number == 2627) // <-- but this will
                {
                    result = -1;
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

    }
}
