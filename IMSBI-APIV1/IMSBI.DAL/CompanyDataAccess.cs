﻿using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.DAL
{
    public class CompanyDataAccess
    {


        public DataSet GetCompanyPerformance(Comparison_IVM objComparision, int AccessType, string TCCodes)
        {
            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            try
            {
                if (objComparision.DataType == "npm")
                {
                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty; 
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancemat + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancematTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancemth + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancemthTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }

                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdc + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdcTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceQtr + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceqtrTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancehlf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancehlfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }
                }
                else
                {
                  

                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancematCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancematTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }

                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancemthCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancemthTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "ytdf":

                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdcCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceytdcTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceQtrCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceqtrTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancehlfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformancehlfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }

                }

            }
            catch
            {

                throw;
            }

            return resultds;
       }

        public DataSet GetManufPerformance(Comparison_IVM objComparision, int AccessType, string TCCodes)
        {
            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            try
            {
                if (objComparision.DataType == "npm")
                {
                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancemat + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancematTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                                
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancemth + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancemthTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdc + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdcTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceQtr + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceqtrTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancehlf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancehlfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;


                    }
                }
                else
                {


                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancematCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancematTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancemthCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancemthTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }

                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdcCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceytdcTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceQtrCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceqtrTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceHlfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformancehlfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }

                }

            }
            catch
            {

                throw;
            }

            return resultds;
        }

        public DataSet GetCompanyPerformanceMap(Comparison_IVM objComparision, int AccessType, string TCCodes)
        {

            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            try
            {
                if (objComparision.DataType == "npm")
                {
                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmat + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmatTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmth + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmthTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapqtr + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapqtrTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdc + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdcTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMaphlf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMaphlfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }

                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }
                }
                else
                {

              

                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmatCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmatTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmthCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapmthTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapqtrCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapqtrTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdcCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapytdcTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMapHlfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyPerformanceMaphlfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }
                }
            }
            catch
            {
                throw;
            }

            return resultds;
        }

        public DataSet GetManufPerformanceMap(Comparison_IVM objComparision, int AccessType, string TCCodes)
        {

            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            try
            {
                if (objComparision.DataType == "npm")
                {
                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmat + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmatTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmth + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmthTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapqtr + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapqtrTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdc + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdcTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMaphlf + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMaphlfTCCode + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }
                }
                else
                {



                    switch (objComparision.PeriodType.ToLower())
                    {
                        case "mat":
                            string stringSQLTopProductmat = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmatCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmatTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                            break;
                        case "mth":
                            string stringSQLTopProductmth = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmthCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapmthTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                            break;
                        case "qtr":
                            string stringSQLTopProductqtr = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapqtrCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapqtrTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }

                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                            break;
                        case "ytdf":
                            string stringSQLTopProductytdf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                            break;
                        case "ytdc":
                            string stringSQLTopProductytdc = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdcCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapytdcTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }

                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                            break;
                        case "hlf":
                            string stringSQLTopProducthlf = string.Empty;
                            if (string.IsNullOrEmpty(objComparision.TCCode) || objComparision.TCCode == "all")
                            {
                                 stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMapHlfCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + "," + AccessType + ",'" + TCCodes + "'";
                            }
                            else
                            {
                                stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ManufPerformanceMaphlfTCCodeCVM + " " + objComparision.GeographyID + ",'" + objComparision.GeographyFilter + "'," + objComparision.CompanyID + "," + objComparision.DivisonID + ",'" + objComparision.PatchIds + "'," + objComparision.PeriodStart + "," + objComparision.PeriodEnd + ",'" + objComparision.TCLevel + "','" + objComparision.TCCode + "'," + AccessType + ",'" + TCCodes + "'";
                            }
                            resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                            break;

                    }
                }
            }
            catch
            {
                throw;
            }

            return resultds;
        }

        

        public DataSet GetCompanyBrandPerformance(CompanyBrandPerformance_IVM objComparision, Int32 PeriodID, string TCCodes)
        {
            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            try
            {
                switch (objComparision.PeriodType.ToLower())
                {

                    case "mat":
                        string stringSQLTopProductmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyBrandPerformancemat + " "  + objComparision.CompanyID + "," + objComparision.DivisonID + "," + PeriodID + ",'" + TCCodes + "'";
                        resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmat);
                        break;
                    case "mth":
                        string stringSQLTopProductmth = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyBrandPerformancemth + " " + objComparision.CompanyID + "," + objComparision.DivisonID + "," + PeriodID + ",'" + TCCodes + "'";
                        resultds = IMSBIDB.ReturnDataset(stringSQLTopProductmth);
                        break;
                   
                    case "qtr":
                        string stringSQLTopProductqtr = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyBrandPerformanceqtr + " " + objComparision.CompanyID + "," + objComparision.DivisonID + "," + PeriodID + ",'" + TCCodes + "'";
                        resultds = IMSBIDB.ReturnDataset(stringSQLTopProductqtr);
                        break;
                    case "ytdf":
                        string stringSQLTopProductytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyBrandPerformanceytdf + " " + objComparision.CompanyID + "," + objComparision.DivisonID + "," + PeriodID + ",'" + TCCodes + "'";
                        resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdf);
                        break;
                    case "ytdc":
                        string stringSQLTopProductytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyBrandPerformanceytdc + " "  + objComparision.CompanyID + "," + objComparision.DivisonID + "," + PeriodID + ",'" + TCCodes + "'";
                        resultds = IMSBIDB.ReturnDataset(stringSQLTopProductytdc);
                        break;
                    case "hlf":
                        string stringSQLTopProducthlf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyBrandPerformancehlf + " "  + objComparision.CompanyID + "," + objComparision.DivisonID + "," + PeriodID + ",'" + TCCodes + "'";
                        resultds = IMSBIDB.ReturnDataset(stringSQLTopProducthlf);
                        break;


                }

            }
            catch
            {

                throw;
            }

            return resultds;
        }


        public IList<CompanyMaster> GetCompanyList(int UserAccessType, string TCValues)
        {


            IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
            try
            {
                DataSet resutset;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyList+ " "+ UserAccessType +",'" + TCValues + "'";
                resutset = IMSBIDB.ReturnDataset(str);
                var result = (from row in resutset.Tables[0].AsEnumerable()
                              select new CompanyMaster
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  Indian_MNC = Convert.ToString(row["Indian_MNC"]),

                              }
                              ).ToList();

                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }

        public IList<CompanyMaster> GetCompanyListByTCCode(string TCCode, string TherapyID, int UserAccessType, string TCValues)
        {


            IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
            try
            {
                DataSet resutset;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyListbyTherapy + " '" + TherapyID + "','" + TCCode + "'," + UserAccessType + ",'" + TCValues + "'";
                resutset = IMSBIDB.ReturnDataset(str);
                var result = (from row in resutset.Tables[0].AsEnumerable()
                              select new CompanyMaster
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  Indian_MNC = Convert.ToString(row["Indian_MNC"]),

                              }
                              ).ToList();

                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }

        public IList<CompanyMaster> GetManufListByTCCode(string TCCode, string TherapyIDs, int UserAccessType, string TCValues)
        {


            IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
            try
            {
                DataSet resutset;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetManufListbyTherapy + " '" + TherapyIDs + "','" + TCCode + "'," + UserAccessType + ",'" + TCValues + "'";
                resutset = IMSBIDB.ReturnDataset(str);
                var result = (from row in resutset.Tables[0].AsEnumerable()
                              select new CompanyMaster
                              {
                                  CompanyID = Convert.ToInt32(row["Manufac_Code"]),
                                  CompanyName = Convert.ToString(row["Manufact_desc"]),


                              }
                              ).ToList();

                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }

        public IList<CompanyMaster> GetManufList(int UserAccessType, string TCValues)
        {


            IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
            try
            {
                DataSet resutset;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetManufList + " " + UserAccessType + ",'" + TCValues + "'";
                resutset = IMSBIDB.ReturnDataset(str);
                var result = (from row in resutset.Tables[0].AsEnumerable()
                              select new CompanyMaster
                              {
                                  CompanyID = Convert.ToInt32(row["Manufac_Code"]),
                                  CompanyName = Convert.ToString(row["Manufact_desc"]),
                                

                              }
                              ).ToList();

                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }


        public IList<CompanyDetails> GetZeroValueCompanyDetails(string CompanyIDs, string type)
        {


            IList<CompanyDetails> CompanyList = new List<CompanyDetails>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
               
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetZeroValueCompanyList + " '" + CompanyIDs + "'";
                if (type == "manuf")
                {
                     str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetZeroValueManufList + " '" + CompanyIDs + "'";
                }
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyDetails
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  

                              }
                              ).ToList();

                CompanyList = result;
                IMSBIDB = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }

        public IList<TimePeriodData> GetTimePeriodDetails(string TimePeriodType, Int32 PeriodStart, Int32 PeriodEnd)
        {


            IList<TimePeriodData> CompanyList = new List<TimePeriodData>();
            try
            {
                DataSet resultdTimePeriod;
                DALUtility IMSBIDB = new DALUtility();
                string strPeriod = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTimePeriodListBasedOnPeriodType + " '" + TimePeriodType + "'," + PeriodStart + "," + PeriodEnd;
                resultdTimePeriod = IMSBIDB.ReturnDataset(strPeriod);


                var resultTimePeriod = (from row in resultdTimePeriod.Tables[0].AsEnumerable()
                                        select new TimePeriodData
                                        {
                                            TimePeriodID = Convert.ToInt32(row["TimePeriodID"]),
                                            Year = Convert.ToInt32(row["Year"]),
                                            Month = Convert.ToInt32(row["Month"]),
                                            MonthName = Convert.ToString(row["MonthName"]),

                                        }
                       ).ToList();
                IMSBIDB = null;
                CompanyList = resultTimePeriod;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }


       

        // CompanyCluster

        public CompanyCluster GetCompanyClusterInfo(int ClusterID)
        {
            CompanyCluster objCompanyCluster = new CompanyCluster();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyClusterInfo + " " + ClusterID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyCluster
                              {
                                  ClusterID = Convert.ToInt32(row["ClusterID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  ClusterName = Convert.ToString(row["ClusterName"]),
                                  PatchIDs = Convert.ToString(row["PatchIds"])

                              }
                              ).FirstOrDefault();

                objCompanyCluster = result;

            }
            catch
            {
                throw;
            }

            return objCompanyCluster;
        }

        public IList<CompanyCluster> GetCompanyClusterList(int CompanyID, int CityID)
        {
            IList<CompanyCluster> objCompanyClusterList = new List<CompanyCluster>();
            try
            {

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyClusterList + " " + CompanyID + "," + CityID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyCluster
                              {
                                  ClusterID = Convert.ToInt32(row["ClusterID"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CityID = Convert.ToInt32(row["CityiD"]),
                                  ClusterName = Convert.ToString(row["ClusterName"]),
                                  PatchIDs = Convert.ToString(row["PatchIDs"])

                              }
                              ).ToList();

                objCompanyClusterList = result;

            }
            catch
            {
                throw;
            }

            return objCompanyClusterList;
        }

        public bool GenerateCompanyCVMData(int CompanyID)
        {
            bool status = false; ;
            try
            {


               // CVMProductTCDetails objCMProductTCDetails = this.GetCompanyCVMProductList(CompanyID);             

                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GenerateCompanyCVMData + " " + CompanyID ;
                var obj = IMSBIDB.ReturnValue(str);
                if(Convert.ToInt32(obj) >0 )
                {
                    status = true;
                }



            }
            catch
            {
                throw;
            }

            return status;
        }
        
        public int AddUpdateCluster(CompanyCluster objClusterData)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {


                if (objClusterData != null)
                {
                    param = objClusterData.ClusterID + ",";
                    param += objClusterData.CompanyID + ",";
                    param += objClusterData.CityID + ",";
                    param += "'" + objClusterData.ClusterName + "',";
                    param += "'" + objClusterData.PatchIDs + "'";


                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateCompanyCluster + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public string GetDivisionName(Int32 DivisionID)
        {
            string DivisionName = string.Empty;
            try
            {

                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDivisionName + " " + DivisionID;
                var obj = IMSBIDB.ReturnValue(str);
                DivisionName = Convert.ToString(obj);
            }
            catch
            {
                throw;
            }

            return DivisionName;
        }

        public string GetSubscribedCompanyColor(Int32 CompanyID)
        {
            string ColorCode = string.Empty;
            try
            {

                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSubscribedCompanyColorCode + " " + CompanyID;
                var obj = IMSBIDB.ReturnValue(str);
                ColorCode = Convert.ToString(obj);
            }
            catch
            {
                throw;
            }

            return ColorCode;
        }

        public string GetCompanySubscribedlocation(Int32 CompanyID)
        {
            string SubscriptionLocation = string.Empty;

            DALUtility IMSBIDB = new DALUtility();
            DataSet resultds;
            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSubscribedCompanyInfo + " " + CompanyID;
            resultds = IMSBIDB.ReturnDataset(str);
            IMSBIDB = null;
            var result = (from row in resultds.Tables[0].AsEnumerable()
                          select new SubscribedCompanyInfo
                          {                            
                              SubscriptionLocationID = Convert.ToString(row["SubscriptionLocationID"]),
                           }
                          ).FirstOrDefault();
            SubscriptionLocation = result.SubscriptionLocationID;
            return SubscriptionLocation;
        }

        public CompanySubscripedPeriodType GetCompanySubscribedPeridoType(Int32 CompanyID)
        {
            CompanySubscripedPeriodType SubscriptionPeriodType = new CompanySubscripedPeriodType();

            DALUtility IMSBIDB = new DALUtility();
            DataSet resultds;
            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSubscriptionPeriodDetails + " " + CompanyID;
            resultds = IMSBIDB.ReturnDataset(str);
            IMSBIDB = null;
            var result = (from row in resultds.Tables[0].AsEnumerable()
                          select new CompanySubscripedPeriodType
                          {
                              SubscribedPeriodType = Convert.ToString(row["SubscriptionPeriodTypeIDs"]),
                              DefaultSubscriptionType = !string.IsNullOrEmpty(Convert.ToString(row["DefaultPeriodTypeID"])) ?  Convert.ToInt32(row["DefaultPeriodTypeID"]):0
                          }
                          ).FirstOrDefault();
            SubscriptionPeriodType = result;
            return SubscriptionPeriodType;
        }

        public CompanySubscripedPeriodType GetDivisonSubscribedPeridoType(Int32 DivisionID)
        {
            CompanySubscripedPeriodType SubscriptionPeriodType = new CompanySubscripedPeriodType();

            DALUtility IMSBIDB = new DALUtility();
            DataSet resultds;
            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDivisionSubscriptionPeriodDetails + " " + DivisionID;
            resultds = IMSBIDB.ReturnDataset(str);
            IMSBIDB = null;
            var result = (from row in resultds.Tables[0].AsEnumerable()
                          select new CompanySubscripedPeriodType
                          {
                              SubscribedPeriodType = Convert.ToString(row["SubscriptionPeriodTypeIDs"]),
                              DefaultSubscriptionType = !string.IsNullOrEmpty(Convert.ToString(row["DefaultPeriodTypeID"])) ? Convert.ToInt32(row["DefaultPeriodTypeID"]) : 0
                          }
                          ).FirstOrDefault();
            SubscriptionPeriodType = result;
            return SubscriptionPeriodType;
        }

        public IList<ProductData> GetProductCVM(Int64 ProductID)
        {
            IList<ProductData> ProductList = new List<ProductData>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetProductCVM + " " + ProductID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new ProductData
                              {
                                  ProductID = Convert.ToInt64(row["PFCID"]),
                                  ProductName = Convert.ToString(row["ProductName"])


                              }
                              ).ToList();


                ProductList = result;
            }
            catch
            {
                throw;
            }

            return ProductList;

        }

        public IList<CompanyProductList_OVM> GetCompanyProductList(Int32 CompanyID)
        {


            IList<CompanyProductList_OVM> CompanyProductList = new List<CompanyProductList_OVM>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyProduct + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyProductList_OVM
                              {
                                  PFCID = Convert.ToInt64(row["PFC_ID"]),
                                  ProductName = Convert.ToString(row["ProductName"]),
                                  TCCode4 = Convert.ToString(row["TCCode4"]),


                              }
                              ).ToList();


                CompanyProductList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyProductList;
        }

        public IList<DefinedCVM> GetCompanyCVM(int CompanyID)
        {
            IList<DefinedCVM> DefinedCVM = new List<DefinedCVM>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyDefinedCVM + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new DefinedCVM
                              {
                                  CompanyPFCID = Convert.ToInt64(row["PFCID"]),


                                  OtherPFC = Convert.ToString(row["MappedPFC"]),

                              }
                              ).ToList();

                DefinedCVM = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return DefinedCVM;
        }

        public CVMProductTCDetails GetCompanyCVMProductList(Int32 CompanyID)
        {
            CVMProductTCDetails objProductList = new CVMProductTCDetails();
            try
            {

                IList<DefinedCVM> objDefinedCVM = this.GetCompanyCVM(CompanyID);
                Dictionary<Int64, string> objDefinedCVMD = new Dictionary<Int64, string>();
                for (int j = 0; j < objDefinedCVM.Count; j++)
                {
                    if (!objDefinedCVMD.ContainsKey(objDefinedCVM[j].CompanyPFCID))
                    {
                        objDefinedCVMD.Add(objDefinedCVM[j].CompanyPFCID, objDefinedCVM[j].OtherPFC);

                    }
                }
                IList<Int64> PFCIds = new List<Int64>();
                IList<string> TC4Code = new List<string>();
                IList<CompanyProductList_OVM> ObjCompanyProductInfo = this.GetCompanyProductList(CompanyID);
                for (int i = 0; i < ObjCompanyProductInfo.Count; i++)
                {
                    string OtherPFC = string.Empty;
                    if (objDefinedCVMD.TryGetValue(ObjCompanyProductInfo[i].PFCID, out OtherPFC))
                    {
                        string[] PFCIDS = OtherPFC.Split(',');
                        for (int k = 0; k < PFCIDS.Length; k++)
                        {
                            if (!PFCIds.Contains(Int32.Parse(PFCIDS[k])))
                            {
                                PFCIds.Add(Int32.Parse(PFCIDS[k]));
                            }
                        }

                    }
                    else
                    {
                        TC4Code.Add(ObjCompanyProductInfo[i].TCCode4);
                    }
                }
                // Get Products by  TC 4 
                if (TC4Code != null && TC4Code.Count > 0)
                {
                    IList<CompanyProductList_OVM> objProductListTC4 = this.GetPFCIDByTC4Code(string.Join(",", TC4Code));
                    if (objProductListTC4 != null)
                    {
                        for (int i = 0; i < objProductListTC4.Count; i++)
                        {
                            if (!PFCIds.Contains(objProductListTC4[i].PFCID))
                            {
                                PFCIds.Add(objProductListTC4[i].PFCID);
                            }
                        }
                    }
                }
                objProductList.ProductList = PFCIds;





            }
            catch
            {
                throw;
            }

            return objProductList;
        }

        public IList<CompanyProductList_OVM> GetPFCIDByTC4Code(string TC4Code)
        {


            IList<CompanyProductList_OVM> CompanyProductList = new List<CompanyProductList_OVM>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetPFCIDByTC4Code + " '" + TC4Code + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyProductList_OVM
                              {
                                  PFCID = Convert.ToInt64(row["PFC_ID"]),
                                  ProductName = Convert.ToString(row["Pack_Desc"]),


                              }
                              ).ToList();


                CompanyProductList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyProductList;
        }

        public int AddProductCVM(Int64 ProductId, Int32 CompanyID, Int32 OtherProductId)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {

                param = ProductId + ",";
                param += CompanyID + ",";
                param += OtherProductId + "";


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_AddProductCVM + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public int DeleteProductCVM(Int64 ProductID)
        {
            int Status = 0;
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteProductCVM + " " + ProductID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToInt32(obj);
                IMSBIDB = null;
            }
            catch
            {
                throw;
            }
            return Status;
        }

        public DataSubscriptionType GetCompanyDataSubscriptionDetails(Int32 CompanyID, Int32 DivisionID)
        {
            DataSubscriptionType objDatasubcriptionType = new DataSubscriptionType();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyDataSubscriptionDetails + " " + CompanyID + "," + DivisionID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new DataSubscriptionType
                              {
                                  DataSubscriptionTypeID = Convert.ToInt32(row["DataSubscriptionPeriodTypeID"]),
                                  DataSubscriptionTypeName = Convert.ToString(row["SubscriptionPeriodName"]),
                                  DataSubscriptionTypeShortName = Convert.ToString(row["ShotName"])


                              }
                              ).FirstOrDefault();


                objDatasubcriptionType = result;
            }
            catch
            {
                throw;
            }

            return objDatasubcriptionType;
        }
        public IList<ProductTherapy> GetProductTherapy(string ProductIDs, string ProductType)
        {
            IList<ProductTherapy> ProductTherapyList = new List<ProductTherapy>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetProductTherapy + " '" + ProductIDs + "','" + ProductType + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new ProductTherapy
                              {
                                  ProductID = Convert.ToInt32(row["ProductID"]),
                                  TC4Code = Convert.ToString(row["TCCode4"])


                              }
                              ).ToList();


                ProductTherapyList = result;
            }
            catch
            {
                throw;
            }

            return ProductTherapyList;

        }

        public IList<MenuMaster> GetApplicationMenuMaster()
        {
            IList<MenuMaster> objMenuList = new List<MenuMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetApplicationMenuMaster;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new MenuMaster
                              {
                                  MenuID = Convert.ToInt32(row["MenuID"]),
                                  MenuName = Convert.ToString(row["MenuName"]),
                                  MenuShortName = Convert.ToString(row["MenuShortName"]),
                                  Active = Convert.ToBoolean(row["Active"])


                              }
                              ).ToList();


                objMenuList = result;
            }
            catch
            {
                throw;
            }

            return objMenuList;

        }

        public IList<MenuMaster> GetCompanyApplicationMenuMaster(int CompanyID)
        {
            IList<MenuMaster> objMenuList = new List<MenuMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyApplicationMenuMaster + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new MenuMaster
                              {
                                  MenuID = Convert.ToInt32(row["MenuID"]),
                                  MenuName = Convert.ToString(row["MenuName"]),
                                  MenuShortName = Convert.ToString(row["MenuShortName"]),
                                  Active = Convert.ToBoolean(row["Active"])


                              }
                              ).ToList();


                objMenuList = result;
            }
            catch
            {
                throw;
            }

            return objMenuList;

        }
    }
}
