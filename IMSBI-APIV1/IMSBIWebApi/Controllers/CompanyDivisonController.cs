﻿using IMSBI.BL.BusinessLogicBase;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IMSBIWebApi.Controllers
{
    public class CompanyDivisonController : ApiController
    {
        [HttpPost]
        public IHttpActionResult AddUpdateCompanyDivision(Division_IVM objDivision)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var status = obDivisionBusinessManager.AddUpdateCompanyDivision(objDivision);
            obDivisionBusinessManager = null;
            if (status == -1)
            {
                return Content(HttpStatusCode.NoContent, "Any Data");
            }

            return Ok(status);
        }

        public IHttpActionResult GetDivisionProducts(int CompanyID,int DivisionID)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var DivisionProductList = obDivisionBusinessManager.GetDivisionProductIDs(CompanyID, DivisionID);
            obDivisionBusinessManager = null;
            if (DivisionProductList == null)
            {
                return NotFound();
            }
            return Ok(DivisionProductList);
        }

        public IHttpActionResult GetDivisionInfo(int DivisionID)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var DivisionInfo = obDivisionBusinessManager.GetDivisionInfo( DivisionID);
            obDivisionBusinessManager = null;
            if (DivisionInfo == null)
            {
                return NotFound();
            }
            return Ok(DivisionInfo);
        }

        public IHttpActionResult GetDivisionBrand(int DivisionID, int CompanyID)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var DivisionInfo = obDivisionBusinessManager.GetDivisionBrands(DivisionID, CompanyID);
            obDivisionBusinessManager = null;
            if (DivisionInfo == null)
            {
                return NotFound();
            }
            return Ok(DivisionInfo);
        }

        [HttpPost]
        public IHttpActionResult AddUpdateBrandGroup(BrandGroup objBrandGroup)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var status = obDivisionBusinessManager.AddUpdateBrandGroup(objBrandGroup);
            obDivisionBusinessManager = null;
            if(status == -1)
            {
                return Content(HttpStatusCode.NoContent, "Any Data");
            }
            return Ok(status);
        }

        [HttpPost]
        public IHttpActionResult AddDivisionProductMapping(DivisionProductMapping objDivisionProductMapping)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var status = obDivisionBusinessManager.AddDivisionProductMapping(objDivisionProductMapping);
            obDivisionBusinessManager = null;

            return Ok(status);
        }

        public IHttpActionResult GetBrandGroupList(int DivisionID, int CompanyID)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var DivisionBrandGroupList = obDivisionBusinessManager.GetBrandGroupList(DivisionID,  CompanyID);
            obDivisionBusinessManager = null;
            if (DivisionBrandGroupList == null)
            {
                return NotFound();
            }
            return Ok(DivisionBrandGroupList);
        }

        public IHttpActionResult GetBrandGroupInfo(int BrandgroupID)
        {
            DivisionBusinessManger obDivisionBusinessManager = new DivisionBusinessManger();
            var DivisionInfo = obDivisionBusinessManager.GetBrandGroupInfo(BrandgroupID);
            obDivisionBusinessManager = null;
            if (DivisionInfo == null)
            {
                return NotFound();
            }
            return Ok(DivisionInfo);
        }


    }
}
