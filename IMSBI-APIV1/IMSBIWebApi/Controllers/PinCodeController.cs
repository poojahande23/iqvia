﻿using IMSBI.BL.BusinessLogicBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IMSBIWebApi.Controllers
{
    public class PinCodeController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetPinCodeList(int PinCode)
        {
            PinCodeBusinessManger objpincodeBusinessManager = new PinCodeBusinessManger();
            var status = objpincodeBusinessManager.GetPinCodeList(PinCode);
            objpincodeBusinessManager = null;

            return Ok(status);
        }

        [HttpGet]
        public IHttpActionResult GetPinCodeListToCreatePatch(int CompanyID, int CityID, int DivisionID =0)
        {
            PinCodeBusinessManger objpincodeBusinessManager = new PinCodeBusinessManger();
            var status = objpincodeBusinessManager.GetPinCodeListToCreatePatch(CompanyID, CityID ,DivisionID);
            objpincodeBusinessManager = null;

            return Ok(status);
        }

        [HttpGet]
        public IHttpActionResult GetCityStateByPinCode(int PinCode)
        {
            PinCodeBusinessManger objpincodeBusinessManager = new PinCodeBusinessManger();
            var status = objpincodeBusinessManager.GetCityStateByPinCode(PinCode);
            objpincodeBusinessManager = null;

            return Ok(status);
        }
    }
}
