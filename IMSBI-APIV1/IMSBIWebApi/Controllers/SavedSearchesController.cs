﻿using IMSBI.BL.BusinessLogicBase;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IMSBIWebApi.Controllers
{
    public class SavedSearchesController : ApiController
    {
        public IHttpActionResult GetSavedSearchesbyUser(int UserID, int TabType)
        {
            SavedSerachBusinessManager objSavedSearchesBusinessManager = new SavedSerachBusinessManager();
            var SavedSearchList = objSavedSearchesBusinessManager.GetSavedSearchesbyUser(UserID, TabType);
            objSavedSearchesBusinessManager = null;
            if (SavedSearchList == null)
            {
                return NotFound();
            }
            return Ok(SavedSearchList);
        }

        [HttpPost]
        public IHttpActionResult AddUpdateSavedSearches(SavedSearches objSavedSearches)
        {
            SavedSerachBusinessManager objSavedSearchesBusinessManager = new SavedSerachBusinessManager();
            var status = objSavedSearchesBusinessManager.AddUpdateSavedSearches(objSavedSearches);
            objSavedSearchesBusinessManager = null;
            if (status == -1)
            {
                return Content(HttpStatusCode.NoContent, "Any Data");
            }

            return Ok(status);
        }

        [HttpDelete]
        public IHttpActionResult DeleteSavedSearches(Int32 SearchKeyID)
        {
            SavedSerachBusinessManager objSavedSearchesBusinessManager = new SavedSerachBusinessManager();
            var status = objSavedSearchesBusinessManager.DeleteSavedSearches(SearchKeyID);
            objSavedSearchesBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

    }
}
