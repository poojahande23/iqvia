﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IMSBI.BL.BusinessLogicBase;
using IMSBI.ViewModel;

namespace IMSBIWebApi.Controllers
{

    [RoutePrefix("api/Filter")]
    public class FilterController : ApiController
    {



        public IHttpActionResult GetAllCities()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var CityList = objFilterBusinessManager.GetAllCities();
            objFilterBusinessManager = null;
            if (CityList == null)
            {
                return NotFound();
            }
            return Ok(CityList);
        }

        public IHttpActionResult GetCustomPatchCities()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var CityList = objFilterBusinessManager.GetCustomPatchCities();
            objFilterBusinessManager = null;
            if (CityList == null)
            {
                return NotFound();
            }
            return Ok(CityList);
        }

        public IHttpActionResult GetAllState()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var StateList = objFilterBusinessManager.GetAllState();
            objFilterBusinessManager = null;
            if (StateList == null)
            {
                return NotFound();
            }
            return Ok(StateList);
        }

        public IHttpActionResult GetAllZone()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var ZoneList = objFilterBusinessManager.GetAllZone();
            objFilterBusinessManager = null;
            if (ZoneList == null)
            {
                return NotFound();
            }
            return Ok(ZoneList);
        }

        public IHttpActionResult GetAllBricks()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var BrickList = objFilterBusinessManager.GetAllBricks();
            objFilterBusinessManager = null;
            if (BrickList == null)
            {
                return NotFound();
            }
            return Ok(BrickList);
        }

        public IHttpActionResult GetAllTimePeriods(Int32 PeriodType, Int32 CompanyID, Int32 DivisionID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var TimePeriodList = objFilterBusinessManager.GetTimePeriodList(PeriodType, CompanyID, DivisionID);
            objFilterBusinessManager = null;
            if (TimePeriodList == null)
            {
                return NotFound();
            }
            return Ok(TimePeriodList);
        }


        public IHttpActionResult GetCategoryValues(string CategoryType, int CompanyID, int DivisionID, string SessionID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var CategoryList = objFilterBusinessManager.GetCategoryValues(CategoryType, CompanyID, DivisionID, SessionID);
            objFilterBusinessManager = null;
            if (CategoryList == null)
            {
                return NotFound();
            }
            return Ok(CategoryList);
        }

        public IHttpActionResult GetGeographyValues(string FilterType, int CompanyID, string SessionID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var GeographyList = objFilterBusinessManager.GetGeographyValues(FilterType, CompanyID, SessionID);
            objFilterBusinessManager = null;
            if (GeographyList == null)
            {
                return NotFound();
            }
            return Ok(GeographyList);
        }

        public IHttpActionResult GetBricksByGeography(string FilterType, int GeographyID, int CompanyID, int DivisionID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var Brickslist = objFilterBusinessManager.GetBricksByGeography(FilterType, GeographyID, CompanyID, DivisionID);
            objFilterBusinessManager = null;
            if (Brickslist == null)
            {
                return NotFound();
            }
            return Ok(Brickslist);
        }

        public IHttpActionResult GetComparisionTypeValue(string FilterType, string SessionID, string TCcode = "", string TherapyID = "")
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var ComparisonType = objFilterBusinessManager.GetComparisionTypeValue(FilterType, SessionID, TCcode, TherapyID);
            objFilterBusinessManager = null;
            if (ComparisonType == null)
            {
                return NotFound();
            }
            return Ok(ComparisonType);
        }

        public IHttpActionResult GetSubscriptionPeriodType(string SessionID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var ComparisonType = objFilterBusinessManager.GetSubscriptionPeriodType(SessionID);
            objFilterBusinessManager = null;
            if (ComparisonType == null)
            {
                return NotFound();
            }
            return Ok(ComparisonType);
        }

        public IHttpActionResult GetTherapyListByTherapyCode(string TCcode, string SessionID = "")
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var TherapyList = objFilterBusinessManager.GetTherapyListByTherapyCode(TCcode, SessionID);
            objFilterBusinessManager = null;
            if (TherapyList == null)
            {
                return NotFound();
            }
            return Ok(TherapyList);
        }

        public IHttpActionResult GetSupergroupList()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var SuperGroupList = objFilterBusinessManager.GetSupergroupList();
            objFilterBusinessManager = null;
            if (SuperGroupList == null)
            {
                return NotFound();
            }
            return Ok(SuperGroupList);
        }

        public IHttpActionResult GetMasterTherapyList()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var MasterTherapyList = objFilterBusinessManager.GetMasterTherapyList();
            objFilterBusinessManager = null;
            if (MasterTherapyList == null)
            {
                return NotFound();
            }
            return Ok(MasterTherapyList);
        }

        public IHttpActionResult GetApplicationPermissionList()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var PermissionList = objFilterBusinessManager.GetApplicationPermissionList();
            objFilterBusinessManager = null;
            if (PermissionList == null)
            {
                return NotFound();
            }
            return Ok(PermissionList);
        }

        public IHttpActionResult GetApplicationDataAccessLevel()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var DataAccessLevelList = objFilterBusinessManager.GetApplicationDataAccessLevelList();
            objFilterBusinessManager = null;
            if (DataAccessLevelList == null)
            {
                return NotFound();
            }
            return Ok(DataAccessLevelList);
        }


        public IHttpActionResult GetPatchListBycityID(Int32 CityID)
        {
            FilterBusinessManger objfilterBusinessManager = new FilterBusinessManger();
            var Patchlist = objfilterBusinessManager.GetPatchListByCityID(CityID);
            objfilterBusinessManager = null;
            if (Patchlist == null)
            {
                return NotFound();

            }
            return Ok(Patchlist);
        }

        public IHttpActionResult GetPatchIDsByClusterIDs(string ClusterIDs)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var PatchID = objFilterBusinessManager.GetPatchIDsByClusterIDs(ClusterIDs);
            objFilterBusinessManager = null;
            if (PatchID == null)
            {
                return NotFound();
            }
            return Ok(PatchID);

        }

        // Following are the code related to subscription Period Type

        public IHttpActionResult GetSubscriptionPeriodTypes()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var PatchID = objFilterBusinessManager.GetSubscriptionPeriodType();
            objFilterBusinessManager = null;
            if (PatchID == null)
            {
                return NotFound();
            }
            return Ok(PatchID);

        }


        public IHttpActionResult GetCompanySubscriptionPeriodTypes(int CompanyID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var PatchID = objFilterBusinessManager.GetCompanySubscriptionPeriodType(CompanyID);
            objFilterBusinessManager = null;
            if (PatchID == null)
            {
                return NotFound();
            }
            return Ok(PatchID);

        }

        public IHttpActionResult GetProductsByTherapyCode(string TherapyCode)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var ProductList = objFilterBusinessManager.GetProductsByTherapyCode(TherapyCode);
            objFilterBusinessManager = null;
            if (ProductList == null)
            {
                return NotFound();
            }
            return Ok(ProductList);

        }

        public IHttpActionResult GetProductsByBrand(string BrandIds)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var ProductList = objFilterBusinessManager.GetProductsByBrand(BrandIds);
            objFilterBusinessManager = null;
            if (ProductList == null)
            {
                return NotFound();
            }
            return Ok(ProductList);

        }

        public IHttpActionResult GetDataSubscriptionPeriodType()
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var Subscriptionlist = objFilterBusinessManager.GetDataSubscriptionPeriodType();
            objFilterBusinessManager = null;
            if (Subscriptionlist == null)
            {
                return NotFound();
            }
            return Ok(Subscriptionlist);

        }

        public IHttpActionResult GetDataSubscriptionPeriodTypeByCompany(int CompanyID)
        {
            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var Subscriptionlist = objFilterBusinessManager.GetDataSubscriptionPeriodTypeByCompany(CompanyID);
            objFilterBusinessManager = null;
            if (Subscriptionlist == null)
            {
                return NotFound();
            }
            return Ok(Subscriptionlist);

        }

        [HttpPost]
        public IHttpActionResult GetPinCodeCoordinate(PincodeCoordinateIVM objIVM)
        {

            FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
            var Subscriptionlist = objFilterBusinessManager.GetPinCodeCoordinate(objIVM.PatchIDs, objIVM.CityID, objIVM.CompanyID, objIVM.DivisionID);
            objFilterBusinessManager = null;
            if (Subscriptionlist == null)
            {
                return NotFound();
            }
            return Ok(Subscriptionlist);
        }
    }
}

