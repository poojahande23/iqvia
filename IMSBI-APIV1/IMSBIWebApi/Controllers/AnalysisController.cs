﻿using IMSBI.BL.BusinessLogicBase;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IMSBIWebApi.Controllers
{
    [RoutePrefix("api/analysis")]
    public class AnalysisController : ApiController
    {
        [HttpPost]
        public IHttpActionResult GetAnalysisDashboardData(Analysis_IVM objAnalysis)
        {
            AnalysisBusinessManager objAnalysisBusinessManager = new AnalysisBusinessManager();
            var KPIMatrixData = objAnalysisBusinessManager.GetAnalysisDashboardData(objAnalysis);
            objAnalysisBusinessManager = null;
            if (KPIMatrixData == null)
            {
                return NotFound();
            }
            return Ok(KPIMatrixData);
        }

        [HttpPost]
        public IHttpActionResult GetAnalysisMarketShareData(AnalysisMarketShare_IVM objAnalysis)
        {
            AnalysisBusinessManager objAnalysisBusinessManager = new AnalysisBusinessManager();
            var KPIMatrixData = objAnalysisBusinessManager.GetAnalysisMarketShareData(objAnalysis);
            objAnalysisBusinessManager = null;
            if (KPIMatrixData == null)
            {
                return NotFound();
            }
            return Ok(KPIMatrixData);
        }

        [HttpPost]
        public HttpResponseMessage GeneratePDF(PDF_IVM objPDF)
        {
          
            try
            {

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(objPDF.PDFData);
                //a text file is actually an octet-stream (pdf, etc)
                result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                //we used attachment to force download
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = objPDF.PDFName;

               

                return result;
            }
            catch (Exception ex)
            {
               
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Data, "text/html");
            }
        }

        [HttpPost]
        public IHttpActionResult CompanyProductBrandReport(Analysis_IVM objCompanyReportInput)
        {
            AnalysisBusinessManager objAnalysisBusinessManager = new AnalysisBusinessManager();
            var status = "";// objAnalysisBusinessManager.CompanyProductBrandReport(objCompanyReportInput);
            objAnalysisBusinessManager = null;

            return Ok(status);
        }
    }
}
