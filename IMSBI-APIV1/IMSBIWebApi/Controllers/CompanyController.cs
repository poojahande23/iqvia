﻿using IMSBI.BL.BusinessLogicBase;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IMSBIWebApi.Controllers
{
    [RoutePrefix("api/company")]
    public class CompanyController : ApiController
    {
        public IHttpActionResult GetCompanyManfList(int CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var ManuFList = objCompanyBusinessManager.GetCompanyManfList(CompanyID);
            objCompanyBusinessManager = null;
            if (ManuFList == null)
            {
                return NotFound();
            }
            return Ok(ManuFList);
        }


        public IHttpActionResult GetCompanyDivisions(int CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyDivisioList = objCompanyBusinessManager.GetCompanyDivisions(CompanyID);
            objCompanyBusinessManager = null;
            if (CompanyDivisioList == null)
            {
                return NotFound();
            }
            return Ok(CompanyDivisioList);
        }

        public IHttpActionResult GetCompanyList()
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetCompanyList();
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        public IHttpActionResult GetCompanyCountBasedOnSubscription()
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var SubscriptionCompanyList = objCompanyBusinessManager.GetCompanyCountBasedOnSubscription();
            objCompanyBusinessManager = null;
            if (SubscriptionCompanyList == null)
            {
                return NotFound();
            }
            return Ok(SubscriptionCompanyList);
        }

        public IHttpActionResult GetCompanySubscriptionType()
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetCompanySubscriptionType();
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        public IHttpActionResult GetApplicationMenuList()
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var MenuList = objCompanyBusinessManager.GetApplicationMenuList();
            objCompanyBusinessManager = null;
            if (MenuList == null)
            {
                return NotFound();
            }
            return Ok(MenuList);
        }
        public IHttpActionResult GetCompanyApplicationMenuList(Int32 CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var MenuList = objCompanyBusinessManager.GetCompanyApplicationMenuList(CompanyID);
            objCompanyBusinessManager = null;
            if (MenuList == null)
            {
                return NotFound();
            }
            return Ok(MenuList);
        }
        public IHttpActionResult GetCompanyReportingManagerBasedOnRoleType(int CompanyRoleID, int CompanyID, int DivisionID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var ReportingMangerList = objCompanyBusinessManager.GetCompanyReportingManagerBasedOnRoleType(CompanyRoleID, CompanyID, DivisionID);
            objCompanyBusinessManager = null;
            if (ReportingMangerList == null)
            {
                return NotFound();
            }
            return Ok(ReportingMangerList);
        }


     





        public IHttpActionResult GetUnsubscribedCompanyList()
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetUnsubscribedCompanyList();
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        public IHttpActionResult GetSubscribedCompanyList()
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetSubscribedCompanyList();
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        public IHttpActionResult GetSubscribedCompanyInfo(Int32 CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetSubscribedCompanyInfo(CompanyID);
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        public IHttpActionResult GetCompanyProductList(int CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetCompanyProductList(CompanyID);
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }


        [HttpPost]
        public IHttpActionResult GetCompanyPerformance(Comparison_IVM objComparision)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetCompanyPerformance(objComparision);
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }



        [HttpPost]
        public IHttpActionResult GetCompanyBrandPerformanceReport(BrandComparison_IVM objComparision)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetCompanyBrandPerformanceReportNew(objComparision);
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        [HttpPost]
        public IHttpActionResult GetSummaryData(Summary_IVM objSummary)
        {
            SummaryBusinessManager objSummaryBusinessManager = new SummaryBusinessManager();
            var CompanyList = objSummaryBusinessManager.GetSummaryData(objSummary);
            objSummaryBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }


        [HttpPost]
        public IHttpActionResult GetCompanyBrandPerformance(CompanyBrandPerformance_IVM objBrandPerformanceIVM)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var DataList = objCompanyBusinessManager.GetCompanyBrandPerformance(objBrandPerformanceIVM);
            objCompanyBusinessManager = null;
            if (DataList == null)
            {
                return NotFound();
            }
            return Ok(DataList);
        }





        [HttpPost]
        public IHttpActionResult BulkProductMapping(BulkProductMapping_IVM objBulkProductMapping)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var MappingList = objCompanyBusinessManager.BulkProductMapping(objBulkProductMapping);
            objCompanyBusinessManager = null;
            if (MappingList == null)
            {
                return NotFound();
            }
            return Ok(MappingList);
        }



        [HttpPost]
        public IHttpActionResult GetCompanyPerformanceMap(Comparison_IVM objComparision)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyList = objCompanyBusinessManager.GetCompanyPerformanceMap(objComparision);
            objCompanyBusinessManager = null;
            if (CompanyList == null)
            {
                return NotFound();
            }
            return Ok(CompanyList);
        }

        public IHttpActionResult GetSubscriptionTypeDetails(int SubscriptionTypeID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var SubscriptionDetails = objCompanyBusinessManager.GetSubscriptionTypeDetails(SubscriptionTypeID);
            objCompanyBusinessManager = null;
            if (SubscriptionDetails == null)
            {
                return NotFound();
            }
            return Ok(SubscriptionDetails);
        }

        [HttpPost]
        public IHttpActionResult AddUpdateSubscribedCompany(SubscribedCompany_IVM objSubscribedCompany)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var status = objCompanyBusinessManager.AddUpdateSubscribedCompany(objSubscribedCompany);
            objCompanyBusinessManager = null;
            if (status == -1)
            {
                return Content(HttpStatusCode.NoContent, "Any Data");
            }

            return Ok(status);
        }


        [HttpPost]
        public IHttpActionResult AddUpdateSubscribedType(SubscriptionType_IVM objSubscriptionType)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var status = objCompanyBusinessManager.AddUpdateSubscriptionType(objSubscriptionType);
            objCompanyBusinessManager = null;

            return Ok(status);
        }


        // Cluster information

        [HttpPost]
        public IHttpActionResult AddUpdateCluster(CompanyCluster objCluster)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var status = objCompanyBusinessManager.AddUpdateCluster(objCluster);
            objCompanyBusinessManager = null;

            return Ok(status);
        }

       
        public IHttpActionResult GetCompanyClusterList(int CompanyID, int CityID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var companyClusterList = objCompanyBusinessManager.GetCompanyClusterList(CompanyID, CityID );
            objCompanyBusinessManager = null;
            if (companyClusterList == null)
            {
                return NotFound();
            }
            return Ok(companyClusterList);
        }

        [HttpGet]
        public IHttpActionResult GenerateCompanyCVMData(int CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var companyClusterList = objCompanyBusinessManager.GenerateCompanyCVMData(CompanyID);
            objCompanyBusinessManager = null;
            if (companyClusterList == null)
            {
                return NotFound();
            }
            return Ok(companyClusterList);
        }

        public IHttpActionResult GetCompanyClusterInfo(int ClusterID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var ClusterInfo = objCompanyBusinessManager.GetCompanyClusterInfo(ClusterID);
            objCompanyBusinessManager = null;
            if (ClusterInfo == null)
            {
                return NotFound();
            }
            return Ok(ClusterInfo);
        }

        public IHttpActionResult GetCompanyAccessRights(int CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var CompanyAccessDetails = objCompanyBusinessManager.GetCompanyAccessRights(CompanyID);
            objCompanyBusinessManager = null;
            if (CompanyAccessDetails == null)
            {
                return NotFound();
            }
            return Ok(CompanyAccessDetails);
        }

        public IHttpActionResult GetCompanySubscribedCityTCClass(int CompanyID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var SubscribedDetails = objCompanyBusinessManager.GetCompanySubscribedCityTCClass(CompanyID);
            objCompanyBusinessManager = null;
            if (SubscribedDetails == null)
            {
                return NotFound();
            }
            return Ok(SubscribedDetails);
        }

        [HttpDelete]
        public IHttpActionResult DeleteCompany(Int32 CompanyID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteCompany(CompanyID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

        [HttpDelete]
        public IHttpActionResult DeleteDivision(Int32 DivisionID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteDivision(DivisionID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

        [HttpDelete]
        public IHttpActionResult DeleteDivisionProductMapping (Int32 DivisionID, Int32 CompanyID, Int32 PFCID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteDivisionProduct (DivisionID, CompanyID,PFCID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

        [HttpDelete]
        public IHttpActionResult DeleteBrandGroup(Int32 BrandGroupID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteBrandGroup(BrandGroupID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

        [HttpDelete]
        public IHttpActionResult DeleteCompanyCluster(Int32 ClusterID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteCluster(ClusterID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

        public IHttpActionResult GetProductCM(Int64 ProductID)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var SubscribedDetails = objCompanyBusinessManager.GetProductCM(ProductID);
            objCompanyBusinessManager = null;
            if (SubscribedDetails == null)
            {
                return NotFound();
            }
            return Ok(SubscribedDetails);
        }

        [HttpPost]
        public IHttpActionResult AddProductCVM(AddProductCVM ProductCVMInput)
        {
            CompanyBusinessManager objCompanyBusinessManager = new CompanyBusinessManager();
            var AddStatus = objCompanyBusinessManager.AddProductCVM(ProductCVMInput);
            objCompanyBusinessManager = null;

            return Ok(AddStatus);
        }





    }
}
