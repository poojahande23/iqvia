﻿using IMSBI.BL.BusinessLogicBase;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IMSBIWebApi.Controllers
{

    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        public IHttpActionResult GetCompanyTeamWiseUserCount(int CompanyID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var CompanyTeamUseCountList = objUserBusinessManager.GetCompanyTeamWiseUserCount(CompanyID);
            objUserBusinessManager = null;
            if (CompanyTeamUseCountList == null)
            {
                return NotFound();
            }
            return Ok(CompanyTeamUseCountList);
        }

        public IHttpActionResult GetCompanyUserCount(int CompanyID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var UserCount = objUserBusinessManager.GetCompanyUserCount(CompanyID);
            objUserBusinessManager = null;
            
            return Ok(UserCount);
        }

        public IHttpActionResult GetCompanyUsers(int CompanyID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var UserList = objUserBusinessManager.GetCompanyUsers(CompanyID);
            objUserBusinessManager = null;
            if (UserList == null)
            {
                return NotFound();
            }
            return Ok(UserList);
        }

        public IHttpActionResult GetAdminUsers()
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var AdminUserList = objUserBusinessManager.GetAdminUsers();
            objUserBusinessManager = null;
            if (AdminUserList == null)
            {
                return NotFound();
            }
            return Ok(AdminUserList);
        }


        public IHttpActionResult GetUserInfo(Int32 UserID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var Userinfo = objUserBusinessManager.GetUserInfo(UserID);
            objUserBusinessManager = null;
            if (Userinfo == null)
            {
                return NotFound();
            }
            return Ok(Userinfo);
        }

        [HttpDelete]
        public IHttpActionResult DeleteUser(Int32 UserID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteUser(UserID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }

        [HttpDelete]
        public IHttpActionResult DeleteCompanyUserRole(Int32 CompanyRoleID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.DeleteCompanyUserRole(CompanyRoleID);
            objUserBusinessManager = null;
            if (!status)
            {
                return NotFound();
            }
            return Ok(status);
        }



        public IHttpActionResult GetDivisionUsers(int CompanyID, int DivisionID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var UserList = objUserBusinessManager.GetDivisionUsers(CompanyID, DivisionID);
            objUserBusinessManager = null;
            if (UserList == null)
            {
                return NotFound();
            }
            return Ok(UserList);
        }

        public IHttpActionResult GetTeamPermission(int TeamID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var PermissionList = objUserBusinessManager.GetTeamPermission(TeamID);
            objUserBusinessManager = null;
            if (PermissionList == null)
            {
                return NotFound();
            }
            return Ok(PermissionList);
        }

        public IHttpActionResult GetUserPermission(int UserID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var Permissionlist = objUserBusinessManager.GetUserPermission(UserID);
            objUserBusinessManager = null;
            if (Permissionlist == null)
            {
                return NotFound();
            }
            return Ok(Permissionlist);
        }

        public IHttpActionResult GetUserRoles()
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var UserRoleList = objUserBusinessManager.GetUserRoles();
            objUserBusinessManager = null;
            if (UserRoleList == null)
            {
                return NotFound();
            }
            return Ok(UserRoleList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <param name="DivisionID"></param>
        /// <returns></returns>
        public IHttpActionResult GetUserRolesByCompany(int CompanyID, int DivisionID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var UserRoleList = objUserBusinessManager.GetUserRolesbyCompany(CompanyID, DivisionID);
            objUserBusinessManager = null;
            if (UserRoleList == null)
            {
                return NotFound();
            }
            return Ok(UserRoleList);
        }


        public IHttpActionResult GetCompanyUserRoleByRoleID(int CompanyUserRoleID)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var UserRole = objUserBusinessManager.GetCompanyUserRoleByRoleID(CompanyUserRoleID);
            objUserBusinessManager = null;
            if (UserRole == null)
            {
                return NotFound();
            }
            return Ok(UserRole);
        }

        [HttpPost]
        public IHttpActionResult AddUpdateUser(User_IVM objUser)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.AddUpdateUser(objUser);
            objUserBusinessManager = null;
            if (status == -1)
            {
                return Content(HttpStatusCode.NoContent, "Any Data");
            }

            return Ok(status);
        }

        [HttpPost]
        public IHttpActionResult AddUpdateCompanyUserRole(CompanyUserRole_IVM ObjCompanyUserRole)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.AddUpdateCompanyUserRole(ObjCompanyUserRole);
            objUserBusinessManager = null;
            if (status == -1)
            {
                return Content(HttpStatusCode.NoContent, "Any Data");
            }

            return Ok(status);
        }

        [HttpPost]
        public IHttpActionResult AddUpdateUserTeam(Team_IVM objTeam)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.AddUpdateUserTeam(objTeam);
            objUserBusinessManager = null;

            return Ok(status);
        }

        [HttpPost]
        public IHttpActionResult GetUserAccesslog(AccessLogReport_IVM objUserAccessLog)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var AccessLogList = objUserBusinessManager.GetUserAccessLog(objUserAccessLog);
            objUserBusinessManager = null;

            return Ok(AccessLogList);
        }

        [HttpPost]
        public IHttpActionResult ChangePassword(ChangePassword_IVM objUserInfo)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.ChangePassword(objUserInfo);
            objUserBusinessManager = null;

            return Ok(status);
        }

        [HttpPost]
        public IHttpActionResult InsertUpdateTeamUserAssocation(UserMapping_IVM objUserMapping)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.InsertUpdateTeamUserAssocation(objUserMapping.UserID, objUserMapping.TeamID, objUserMapping.Active);
            objUserBusinessManager = null;

            return Ok(status);
        }

        [HttpPost]
        public IHttpActionResult InsertUpdateTeamPermissionMapping(TeamPermissionMapping_IVM objTeamPermissionMapping)
        {
            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
            var status = objUserBusinessManager.InsertUpdateTeamPermissionMapping(objTeamPermissionMapping.TeamID, objTeamPermissionMapping.PermissionID, objTeamPermissionMapping.Active);
            objUserBusinessManager = null;

            return Ok(status);
        }



    }
}
