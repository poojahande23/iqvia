﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMSBIWebApi.Models
{
    public class cityMaster
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int StateID { get; set; }
    }

    public class StateMaster
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int ZoneID { get; set; }
    }

    public class ZoneMaster
    {
        public int ZoneID { get; set; }
        public int ZoneName { get; set; }
    }
    public class TimePeriod
    {
        public int TimePeriodID { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
    }

    public class BrickMaster
    {
        public int BrickID { get; set; }
        public string BrinkName { get; set; }
    }
}