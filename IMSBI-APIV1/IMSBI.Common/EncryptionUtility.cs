﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.Common
{

    public interface IEncryptionUtility
    {
        string Decrypt(string ciphertext, string key);
        string Encrypt(string plainText, string key);
    }

    #region For User Display Name and user Meta Data

    public class AESEncryptionUtility : IEncryptionUtility
    {
        //Refer link: http://stephenhaunts.com/2013/03/04/cryptography-in-net-advanced-encryption-standard-aes/
        private readonly int _saltSize = 32;

        public string Encrypt(string plainText, string key)
        {
            if (string.IsNullOrEmpty(plainText))
            {
                throw new ArgumentNullException("plainText");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            using (var keyDerivationFunction = new Rfc2898DeriveBytes(key, _saltSize))
            {
                byte[] saltBytes = keyDerivationFunction.Salt;
                byte[] keyBytes = keyDerivationFunction.GetBytes(32);
                byte[] ivBytes = keyDerivationFunction.GetBytes(16);

                using (var aesManaged = new AesManaged())
                {
                    aesManaged.KeySize = 256;

                    using (var encryptor = aesManaged.CreateEncryptor(keyBytes, ivBytes))
                    {
                        MemoryStream memoryStream = null;
                        CryptoStream cryptoStream = null;

                        return WriteMemoryStream(plainText, ref saltBytes, encryptor, ref memoryStream, ref cryptoStream);
                    }
                }
            }
        }

        public string Decrypt(string ciphertext, string key)
        {
            if (string.IsNullOrEmpty(ciphertext))
            {
                throw new ArgumentNullException("ciphertext");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            var allTheBytes = Convert.FromBase64String(ciphertext);
            var saltBytes = allTheBytes.Take(_saltSize).ToArray();
            var ciphertextBytes = allTheBytes.Skip(_saltSize).Take(allTheBytes.Length - _saltSize).ToArray();

            using (var keyDerivationFunction = new Rfc2898DeriveBytes(key, saltBytes))
            {
                var keyBytes = keyDerivationFunction.GetBytes(32);
                var ivBytes = keyDerivationFunction.GetBytes(16);

                return DecryptWithAES(ciphertextBytes, keyBytes, ivBytes);
            }
        }

        private string WriteMemoryStream(string plainText, ref byte[] saltBytes, ICryptoTransform encryptor, ref MemoryStream memoryStream, ref CryptoStream cryptoStream)
        {
            try
            {
                memoryStream = new MemoryStream();

                try
                {
                    cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);

                    using (var streamWriter = new StreamWriter(cryptoStream))
                    {
                        streamWriter.Write(plainText);
                    }
                }
                finally
                {
                    if (cryptoStream != null)
                    {
                        cryptoStream.Dispose();
                    }
                }

                var cipherTextBytes = memoryStream.ToArray();
                Array.Resize(ref saltBytes, saltBytes.Length + cipherTextBytes.Length);
                Array.Copy(cipherTextBytes, 0, saltBytes, _saltSize, cipherTextBytes.Length);

                return Convert.ToBase64String(saltBytes);
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
        }

        private static string DecryptWithAES(byte[] ciphertextBytes, byte[] keyBytes, byte[] ivBytes)
        {
            using (var aesManaged = new AesManaged())
            {
                using (var decryptor = aesManaged.CreateDecryptor(keyBytes, ivBytes))
                {
                    MemoryStream memoryStream = null;
                    CryptoStream cryptoStream = null;
                    StreamReader streamReader = null;

                    try
                    {
                        memoryStream = new MemoryStream(ciphertextBytes);
                        cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                        streamReader = new StreamReader(cryptoStream);

                        return streamReader.ReadToEnd();
                    }
                    finally
                    {
                        if (memoryStream != null)
                        {
                            memoryStream.Dispose();
                            memoryStream = null;
                        }
                    }
                }
            }
        }
    }

    #endregion For User Display Name and user Meta Data

    #region For UserName and Password

    public class CryptorEngine
    {
        /// <summary>
        /// Encrypt a string using dual encryption method. Return a encrypted cipher Text
        /// </summary>
        /// <param name="toEncrypt">string to be encrypted</param>
        /// <param name="useHashing">use hashing? send to for extra secirity</param>
        /// <returns></returns>
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            if (!string.IsNullOrEmpty(toEncrypt))
            {
                toEncrypt = toEncrypt.ToLower();
                byte[] keyArray;
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

                //System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
                //// Get the key from config file
                //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
                string key = "PI@Encrypt123";
                //System.Windows.Forms.MessageBox.Show(key);
                if (useHashing)
                {
                    MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();
                }
                else
                    keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            return string.Empty;
        }
        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="cipherString">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            ////Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "PI@Encrypt123"; ;
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }

    #endregion For UserName and Password




    #region Hashing

    /// <summary>
    /// An interface which defines implementation of Hashing Utility for generating Hash using some specific hashing algorithm
    /// </summary>
    public interface IHashingUtility
    {
        string Key { get; }

        string GenerateHash(string plainTextMessage);
    }

    #region AES-128 CMAC Hash (Message Authentication) for SubPub Subscription

    public class AesCmacHash : IHashingUtility
    {
        private string hashKey;

        /// <summary>
        /// private key for encryption using AES-128
        /// </summary>
        public string Key
        {
            get { return hashKey; }
        }

        /// <summary>
        /// Constructor for the class. Need to pass the key.
        /// </summary>
        /// <param name="hashingKey">private key for encryption using AES-128</param>
        public AesCmacHash(string hashingKey)
        {
            hashKey = hashingKey;
        }

        /// <summary>
        /// Generates a CMAC Message Authentication hash of the plain text Message provided, using key and AES-128 for encryption.
        /// </summary>
        /// <param name="plainTextMessage">the message whose hash is to be generated</param>
        /// <returns>the CMAC Message Authentication hash of the plain text Message</returns>
        public string GenerateHash(string plainTextMessage)
        {
            //Check if Hash key has been defined?
            if (!string.IsNullOrEmpty(hashKey))
            {
                //Check if valid plain text message is sent?
                if (!string.IsNullOrEmpty(plainTextMessage))
                {
                    byte[] hash = AESCMAC(StringByteArrayConversions.StringToBytes(hashKey), StringByteArrayConversions.StringToBytes(plainTextMessage));
                    return StringByteArrayConversions.ByteArrayToString(hash);
                }
                else
                {
                    //null or empty message. Return it as it is.
                    return plainTextMessage;
                }
            }
            else
            {
                //Hash key has not been defined
                return plainTextMessage;
            }
        }

        /// <summary>
        /// Below method is the implementation of the AES-CMAC algorithm as described in RFC4493 (https://tools.ietf.org/html/rfc4493).
        /// Referenced from http://stackoverflow.com/questions/29163493/aes-cmac-calculation-c-sharp
        /// </summary>
        /// <param name="key">encryption key as byte array</param>
        /// <param name="data">data to be hashed as byte array</param>
        /// <returns>encrypted hash as bytes array</returns>
        private byte[] AESCMAC(byte[] key, byte[] data)
        {
            // SubKey generation
            // step 1, AES-128 with key K is applied to an all-zero input block.
            byte[] L = AESEncrypt(key, new byte[16], new byte[16]);

            // step 2, K1 is derived through the following operation:
            byte[] FirstSubkey = ShiftLeftByOneBit(L); //If the most significant bit of L is equal to 0, K1 is the left-shift of L by 1 bit.
            if ((L[0] & 0x80) == 0x80)
                FirstSubkey[15] ^= 0x87; // Otherwise, K1 is the exclusive-OR of const_Rb and the left-shift of L by 1 bit.

            // step 3, K2 is derived through the following operation:
            byte[] SecondSubkey = ShiftLeftByOneBit(FirstSubkey); // If the most significant bit of K1 is equal to 0, K2 is the left-shift of K1 by 1 bit.
            if ((FirstSubkey[0] & 0x80) == 0x80)
                SecondSubkey[15] ^= 0x87; // Otherwise, K2 is the exclusive-OR of const_Rb and the left-shift of K1 by 1 bit.

            // MAC computing
            if (((data.Length != 0) && (data.Length % 16 == 0)) == true)
            {
                // If the size of the input message block is equal to a positive multiple of the block size (namely, 128 bits),
                // the last block shall be exclusive-OR'ed with K1 before processing
                for (int j = 0; j < FirstSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= FirstSubkey[j];
            }
            else
            {
                // Otherwise, the last block shall be padded with 10^i
                byte[] padding = new byte[16 - data.Length % 16];
                padding[0] = 0x80;

                data = data.Concat<byte>(padding.AsEnumerable()).ToArray();

                // and exclusive-OR'ed with K2
                for (int j = 0; j < SecondSubkey.Length; j++)
                    data[data.Length - 16 + j] ^= SecondSubkey[j];
            }

            // The result of the previous process will be the input of the last encryption.
            byte[] encResult = AESEncrypt(key, new byte[16], data);

            byte[] HashValue = new byte[16];
            Array.Copy(encResult, encResult.Length - HashValue.Length, HashValue, 0, HashValue.Length);

            return HashValue;
        }

        /// <summary>
        /// Below method is used by the CMAC algorithm to shift arrays left by one bit.
        /// taken from http://stackoverflow.com/questions/29163493/aes-cmac-calculation-c-sharp
        /// </summary>
        /// <param name="bytesArray">bytes array to be shifted left by one bit</param>
        /// <returns>processed bytes</returns>
        private byte[] ShiftLeftByOneBit(byte[] bytesArray)
        {
            byte[] r = new byte[bytesArray.Length];
            byte carry = 0;

            for (int i = bytesArray.Length - 1; i >= 0; i--)
            {
                ushort u = (ushort)(bytesArray[i] << 1);
                r[i] = (byte)((u & 0xff) + carry);
                carry = (byte)((u & 0xff00) >> 8);
            }

            return r;
        }

        /// <summary>
        /// For CMAC, first we need to derive two subkeys from the AES key. For this, we will need the AESEncrypt function. Below method implementes the AES encryption using dotNet AesCryptoServiceProvider
        /// taken from http://stackoverflow.com/questions/29163493/aes-cmac-calculation-c-sharp
        /// </summary>
        /// <param name="key">key as byte array</param>
        /// <param name="iv">IV as byte array</param>
        /// <param name="data">data to be encrypted as byte array</param>
        /// <returns>encrypted bytes</returns>
        private byte[] AESEncrypt(byte[] key, byte[] iv, byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.None;

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    cs.Write(data, 0, data.Length);
                    cs.FlushFinalBlock();

                    return ms.ToArray();
                }
            }
        }
    }

    #endregion AES-128 CMAC Hash (Message Authentication) for SubPub Subscription

    #endregion Hashing

    public class ASCIIEncoding
    {
        #region For Passcode using Base64

        static public string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string DecodeFrom64(string passCode)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(passCode);
            string decryptedPasscode = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return decryptedPasscode;
        }

        #endregion
    }

    /// <summary>
    /// This static class holds static methods for converting String to byte array before passing it on for Encryption, and converting Byte arry to string after encryption has taken place
    /// </summary>
    public static class StringByteArrayConversions
    {
        /// <summary>
        /// Converts Encrypted Byte Array to String
        /// Referenced from: http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa
        /// Refer answer: http://stackoverflow.com/a/311179
        /// </summary>
        /// <param name="encryptedBytes">byte array after encryption</param>
        /// <returns>encrypted string value</returns>
        public static string ByteArrayToString(byte[] encryptedBytes)
        {
            StringBuilder hexadecimalString = new StringBuilder(encryptedBytes.Length * 2);
            foreach (byte byteValue in encryptedBytes)
                hexadecimalString.AppendFormat("{0:x2}", byteValue);
            return hexadecimalString.ToString();
        }

        ////Referenced from: http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa
        ////Refer answer: http://stackoverflow.com/a/311179
        //public static string ByteArrayToString2(byte[] ba)
        //{
        //    string hex = BitConverter.ToString(ba);
        //    return hex.Replace("-", "");
        //}

        /// <summary>
        /// Converts a string input for encryption, into encryption-friendly byte array
        /// Referenced from Link: http://stackoverflow.com/questions/18131357/expressing-byte-values-127-in-net-strings
        /// </summary>
        /// <param name="stringForEncryption">string (data or key) which is to be passed for encryption purposes</param>
        /// <returns>a byte array that can be passed into the encryption algorithm</returns>
        public static byte[] StringToBytes(string stringForEncryption)
        {
            //Taken from Answer: http://stackoverflow.com/a/18131708
            return stringForEncryption.Select(Convert.ToByte).ToArray();

            ////This works too! - Taken from Answer: http://stackoverflow.com/a/18131886
            //var bytes = new byte[str.Length];

            //for (int i = 0; i < str.Length; i++)
            //{
            //    bytes[i] = checked((byte)str[i]); // Slower but throws OverflowException if there is an invalid character
            //    //bytes[i] = unchecked((byte)str[i]); // Faster
            //}

            //return bytes;
        }
    }
}
