﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.Common
{
    public class Constants
    {

        public static string DB_SP_InsertUpdateSubscribedCompany = "InsertUpdateSubscribedCompany";
        public static string DB_SP_ValidateUserName = "ValidateUserName";        
        public static string DB_SP_GetApplicationMenuMaster = "GetApplicationMenuMaster";
        public static string DB_SP_GetCompanyApplicationMenuMaster = "GetCompanyApplicationMenuMaster";

        public static string DB_SP_GetAllCity ="GetAllCities";
        public static string DB_SP_GetCustomPatchEnabledCity = "GetCustomPatchEnabledCity";
        public static string DB_SP_GetAllBricks ="GetAllBrickList";
        public static string DB_SP_GetAllState ="GetAllStates";
        public static string DB_SP_GetTimePeriods ="GetTimePeriods";
        public static string DB_SP_GetQuarterPeriod = "GetQuarterPeriod";
        public static string DB_SP_GetHalfYearPeriod = "GetHalfYearPeriod";
        public static string DB_SP_GetMatPeriod = "GetMatPeriod";
        
        public static string DB_SP_GetFyearPeriod = "GetFyearPeriod";
        public static string DB_SP_GetCyearPeriod = "GetCyearPeriod";
        public static string DB_SP_GetZoneList ="GetZoneList";
        public static string DB_SP_GetBricksByCityID = "GetBricksByCityID";
        public static string DB_SP_GetBricksByPatch = "GetBricksByPatch";
        public static string DB_SP_GetBrickPincodeList = "GetBrickPincodeList";

        public static string DB_SP_GetCityByCompany = "GetCityByCompany";
        public static string DB_SP_GetZoneByCompany = "GetZoneByCompany";
        public static string DB_SP_GetStateByCompany = "GetStateByCompany";
        public static string DB_SP_GetPatchList = "GetPatchList";
        public static string DB_SP_GetPincodeCoordinate = "GetPincodeCoordinate";
        public static string DB_SP_GetPincodeList = "GetPinCodeList";
        public static string DB_SP_GetPinCodeListToCreatePath = "GetPinCodeListToCreatePath";
        public static string DB_SP_GetCityStateByPinCode = "GetCityStateByPinCode";
        public static string DB_SP_GetCompanyReportingManagerBasedOnRoleType = "GetCompanyReportingManagerBasedOnRoleType";
        public static string DB_sp_GetPatchIDsbyClusterIDs = "GetPatchIDsByClusterIDs";
        public static string DB_SP_ChangeUserPassword = "ChangeUserPassword";
        public static string DB_SP_GetCompanyDataSubscriptionDetails = "GetCompanyDataSubscriptionDetails";

        //Session SP
        public static string DB_SP_CreateSession = "CreateSession";
        public static string DB_SP_GetSession = "GetSessionInfo";
        public static string DB_SP_DeleteSession = "DeleteSession";
        public static string DB_SP_UpdateUserSession = "UpdateUserSession";



        public static string DB_SP_CompanyManfList = "CompanyManfList";
        public static string DB_SP_GetCompanyDivisions = "GetCompanyDivisions";
        public static string DB_SP_GetCompanyList = "GetCompanyList";
        public static string DB_SP_GetCompanyListbyTherapy = "GetCompanyListbyTherapy";
        public static string DB_SP_GetManufList = "GetManufList";
        public static string DB_SP_GetManufListbyTherapy = "GetManufListbyTherapy";


        public static string DB_SP_GetZeroValueCompanyList = "GetZeroValueCompanyList";
        public static string DB_SP_GetZeroValueBrandList = "GetZeroValueBrandList";
        public static string DB_SP_GetZeroValueManufList = "GetZeroValueManufList";



        // Company Performance SPs
        public static string DB_SP_CompanyPerformancemth = "CompanyPerformancemth";
        public static string DB_SP_CompanyPerformanceQtr = "CompanyPerformanceQtr";        
        public static string DB_SP_CompanyPerformancemat = "CompanyPerformancemat";
        public static string DB_SP_CompanyPerformanceytdf = "CompanyPerformanceytdf";
        public static string DB_SP_CompanyPerformanceytdc = "CompanyPerformanceytdc";
        public static string DB_SP_CompanyPerformancehlf = "CompanyPerformancehlf";
        //TCCode
        public static string DB_SP_CompanyPerformancemthTCCode = "CompanyPerformancemthTCCode";
        public static string DB_SP_CompanyPerformanceqtrTCCode = "CompanyPerformanceqtrTCCode";
        public static string DB_SP_CompanyPerformancematTCCode = "CompanyPerformancematTCCode";
        public static string DB_SP_CompanyPerformanceytdfTCCode = "CompanyPerformanceytdfTCCode";
        public static string DB_SP_CompanyPerformanceytdcTCCode = "CompanyPerformanceytdcTCCode";
        public static string DB_SP_CompanyPerformancehlfTCCode = "CompanyPerformancehlfTCCode";


        // Therapy Performance SPs
        public static string DB_SP_TherapyPerformancemth = "TherapyPerformancemth";
        public static string DB_SP_TherapyPerformanceQtr = "TherapyPerformanceQtr";
        public static string DB_SP_TherapyPerformancemat = "TherapyPerformancemat";
        public static string DB_SP_TherapyPerformanceytdf = "TherapyPerformanceytdf";
        public static string DB_SP_TherapyPerformanceytdc = "TherapyPerformanceytdc";
        public static string DB_SP_TherapyPerformancehlf = "TherapyPerformancehlf";
        //Therapy
        public static string DB_SP_TherapyPerformancemthTCCode = "TherapyPerformancemthTCCode";
        public static string DB_SP_TherapyPerformanceqtrTCCode = "TherapyPerformanceqtrTCCode";
        public static string DB_SP_TherapyPerformancematTCCode = "TherapyPerformancematTCCode";
        public static string DB_SP_TherapyPerformanceytdfTCCode = "TherapyPerformanceytdfTCCode";
        public static string DB_SP_TherapyPerformanceytdcTCCode = "TherapyPerformanceytdcTCCode";
        public static string DB_SP_TherapyPerformancehlfTCCode = "TherapyPerformancehlfTCCode";

        // Manuf Performance SPs
        public static string DB_SP_ManufPerformancemth = "ManufPerformancemth";
        public static string DB_SP_ManufPerformanceQtr = "ManufPerformanceQtr";
        public static string DB_SP_ManufPerformancemat = "ManufPerformancemat";
        public static string DB_SP_ManufPerformanceytdf = "ManufPerformanceytdf";
        public static string DB_SP_ManufPerformanceytdc = "ManufPerformanceytdc";
        public static string DB_SP_ManufPerformancehlf = "ManufPerformancehlf";

        //TCcode
        public static string DB_SP_ManufPerformancemthTCCode = "ManufPerformancemthTCCode";
        public static string DB_SP_ManufPerformanceqtrTCCode = "ManufPerformanceqtrTCCode";
        public static string DB_SP_ManufPerformancematTCCode = "ManufPerformancematTCCode";
        public static string DB_SP_ManufPerformanceytdfTCCode = "ManufPerformanceytdfTCCode";
        public static string DB_SP_ManufPerformanceytdcTCCode = "ManufPerformanceytdcTCCode";
        public static string DB_SP_ManufPerformancehlfTCCode = "ManufPerformancehlfTCCode";


        // CompanyPerformance Sps
        public static string DB_SP_GetCompanyBrandPerformancemat = "GetCompanyBrandPerformancemat";
        public static string DB_SP_GetCompanyBrandPerformancemth = "GetCompanyBrandPerformancemth";
        public static string DB_SP_GetCompanyBrandPerformanceqtr = "GetCompanyBrandPerformanceqtr";
        public static string DB_SP_GetCompanyBrandPerformanceytdc = "GetCompanyBrandPerformanceytdc";
        public static string DB_SP_GetCompanyBrandPerformanceytdf = "GetCompanyBrandPerformanceytdf";
        public static string DB_SP_GetCompanyBrandPerformancehlf = "GetCompanyBrandPerformancehlf";


        // Company Performance CVM Sps
        public static string DB_SP_CompanyPerformancemthCVM = "CompanyPerformancemthCVM";
        public static string DB_SP_CompanyPerformanceQtrCVM = "CompanyPerformanceQtrCVM";
        public static string DB_SP_CompanyPerformancematCVM = "CompanyPerformancematCVM";
        public static string DB_SP_CompanyPerformanceytdfCVM = "CompanyPerformanceytdfCVM";
        public static string DB_SP_CompanyPerformanceytdcCVM = "CompanyPerformanceytdcCVM";
        public static string DB_SP_CompanyPerformancehlfCVM = "CompanyPerformancehlfCVM";
        //TCcode
        public static string DB_SP_CompanyPerformancemthTCCodeCVM = "CompanyPerformancemthTCCodeCVM";
        public static string DB_SP_CompanyPerformanceqtrTCCodeCVM = "CompanyPerformanceqtrTCCodeCVM";
        public static string DB_SP_CompanyPerformancematTCCodeCVM = "CompanyPerformancematTCCodeCVM";
        public static string DB_SP_CompanyPerformanceytdfTCCodeCVM = "CompanyPerformanceytdfTCCodeCVM";
        public static string DB_SP_CompanyPerformanceytdcTCCodeCVM = "CompanyPerformanceytdcTCCodeCVM";
        public static string DB_SP_CompanyPerformancehlfTCCodeCVM = "CompanyPerformancehlfTCCodeCVM";


        // Therapy Performance CVM Sps
        public static string DB_SP_TherapyPerformancemthCVM = "TherapyPerformancemthCVM";
        public static string DB_SP_TherapyPerformanceQtrCVM = "TherapyPerformanceQtrCVM";
        public static string DB_SP_TherapyPerformancematCVM = "TherapyPerformancematCVM";
        public static string DB_SP_TherapyPerformanceytdfCVM = "TherapyPerformanceytdfCVM";
        public static string DB_SP_TherapyPerformanceytdcCVM = "TherapyPerformanceytdcCVM";
        public static string DB_SP_TherapyPerformancehlfCVM = "TherapyPerformancehlfCVM";
        //Therapy
        public static string DB_SP_TherapyPerformancemthTCCodeCVM = "TherapyPerformancemthTCCodeCVM";
        public static string DB_SP_TherapyPerformanceqtrTCCodeCVM = "TherapyPerformanceqtrTCCodeCVM";
        public static string DB_SP_TherapyPerformancematTCCodeCVM = "TherapyPerformancematTCCodeCVM";
        public static string DB_SP_TherapyPerformanceytdfTCCodeCVM = "TherapyPerformanceytdfTCCodeCVM";
        public static string DB_SP_TherapyPerformanceytdcTCCodeCVM = "TherapyPerformanceytdcTCCodeCVM";
        public static string DB_SP_TherapyPerformancehlfTCCodeCVM = "TherapyPerformancehlfTCCodeCVM";

        // Manuf Performance CVM Sps
        public static string DB_SP_ManufPerformancemthCVM = "ManufPerformancemthCVM";
        public static string DB_SP_ManufPerformanceQtrCVM = "ManufPerformanceQtrCVM";
        public static string DB_SP_ManufPerformancematCVM = "ManufPerformancematCVM";
        public static string DB_SP_ManufPerformanceytdfCVM = "ManufPerformanceytdfCVM";
        public static string DB_SP_ManufPerformanceytdcCVM = "ManufPerformanceytdcCVM";
        public static string DB_SP_ManufPerformanceHlfCVM = "ManufPerformancehlfCVM";
        //TCCode
        public static string DB_SP_ManufPerformancemthTCCodeCVM = "ManufPerformancemthTCCodeCVM";
        public static string DB_SP_ManufPerformanceqtrTCCodeCVM = "ManufPerformanceqtrTCCodeCVM";
        public static string DB_SP_ManufPerformancematTCCodeCVM = "ManufPerformancematTCCodeCVM";
        public static string DB_SP_ManufPerformanceytdfTCCodeCVM = "ManufPerformanceytdfTCCodeCVM";
        public static string DB_SP_ManufPerformanceytdcTCCodeCVM = "ManufPerformanceytdcTCCodeCVM";
        public static string DB_SP_ManufPerformancehlfTCCodeCVM = "ManufPerformancehlfTCCodeCVM";

        //Company performance MAP 
        public static string DB_SP_CompanyPerformanceMapmat = "CompanyPerformanceMapmat";
        public static string DB_SP_CompanyPerformanceMapmth = "CompanyPerformanceMapmth";
        public static string DB_SP_CompanyPerformanceMapqtr = "CompanyPerformanceMapqtr";
        public static string DB_SP_CompanyPerformanceMapytdf = "CompanyPerformanceMapytdf";
        public static string DB_SP_CompanyPerformanceMapytdc = "CompanyPerformanceMapytdc";
        public static string DB_SP_CompanyPerformanceMaphlf = "CompanyPerformanceMaphlf";
        //TCCode
        public static string DB_SP_CompanyPerformanceMapmatTCCode = "CompanyPerformanceMapmatTCCode";
        public static string DB_SP_CompanyPerformanceMapmthTCCode = "CompanyPerformanceMapmthTCCode";
        public static string DB_SP_CompanyPerformanceMapqtrTCCode = "CompanyPerformanceMapqtrTCCode";
        public static string DB_SP_CompanyPerformanceMapytdfTCCode = "CompanyPerformanceMapytdfTCCode";
        public static string DB_SP_CompanyPerformanceMapytdcTCCode = "CompanyPerformanceMapytdcTCCode";
        public static string DB_SP_CompanyPerformanceMaphlfTCCode = "CompanyPerformanceMaphlfTCCode";

        //Company performance MAP CVM
        public static string DB_SP_CompanyPerformanceMapmatCVM = "CompanyPerformanceMapmatCVM";
        public static string DB_SP_CompanyPerformanceMapmthCVM = "CompanyPerformanceMapmthCVM";
        public static string DB_SP_CompanyPerformanceMapqtrCVM = "CompanyPerformanceMapqtrCVM";
        public static string DB_SP_CompanyPerformanceMapytdfCVM = "CompanyPerformanceMapytdfCVM";
        public static string DB_SP_CompanyPerformanceMapytdcCVM = "CompanyPerformanceMapytdcCVM";
        public static string DB_SP_CompanyPerformanceMapHlfCVM = "CompanyPerformanceMaphlfCVM";
        //TCCode
        public static string DB_SP_CompanyPerformanceMapmatTCCodeCVM = "CompanyPerformanceMapmatTCCodeCVM";
        public static string DB_SP_CompanyPerformanceMapmthTCCodeCVM = "CompanyPerformanceMapmthTCCodeCVM";
        public static string DB_SP_CompanyPerformanceMapqtrTCCodeCVM = "CompanyPerformanceMapqtrTCCodeCVM";
        public static string DB_SP_CompanyPerformanceMapytdfTCCodeCVM = "CompanyPerformanceMapytdfTCCodeCVM";
        public static string DB_SP_CompanyPerformanceMapytdcTCCodeCVM = "CompanyPerformanceMapytdcTCCodeCVM";
        public static string DB_SP_CompanyPerformanceMaphlfTCCodeCVM = "CompanyPerformanceMaphlfTCCodeCVM";


        //Manuf performance MAP 
        public static string DB_SP_ManufPerformanceMapmat = "ManufPerformanceMapmat";
        public static string DB_SP_ManufPerformanceMapmth = "ManufPerformanceMapmth";
        public static string DB_SP_ManufPerformanceMapqtr = "ManufPerformanceMapqtr";
        public static string DB_SP_ManufPerformanceMapytdf = "ManufPerformanceMapytdf";
        public static string DB_SP_ManufPerformanceMapytdc = "ManufPerformanceMapytdc";
        public static string DB_SP_ManufPerformanceMaphlf = "ManufPerformanceMaphlf";
        //TCCode
        public static string DB_SP_ManufPerformanceMapmatTCCode = "ManufPerformanceMapmatTCCode";
        public static string DB_SP_ManufPerformanceMapmthTCCode = "ManufPerformanceMapmthTCCode";
        public static string DB_SP_ManufPerformanceMapqtrTCCode = "ManufPerformanceMapqtrTCCode";
        public static string DB_SP_ManufPerformanceMapytdfTCCode = "ManufPerformanceMapytdfTCCode";
        public static string DB_SP_ManufPerformanceMapytdcTCCode = "ManufPerformanceMapytdcTCCode";
        public static string DB_SP_ManufPerformanceMaphlfTCCode = "ManufPerformanceMaphlfTCCode";

        //Manuf performance MAP CVM
        public static string DB_SP_ManufPerformanceMapmatCVM = "ManufPerformanceMapmatCVM";
        public static string DB_SP_ManufPerformanceMapmthCVM = "ManufPerformanceMapmthCVM";
        public static string DB_SP_ManufPerformanceMapqtrCVM = "ManufPerformanceMapqtrCVM";
        public static string DB_SP_ManufPerformanceMapytdfCVM = "ManufPerformanceMapytdfCVM";
        public static string DB_SP_ManufPerformanceMapytdcCVM = "ManufPerformanceMapytdcCVM";
        public static string DB_SP_ManufPerformanceMapHlfCVM = "ManufPerformanceMaphlfCVM";
        //TC Code
        public static string DB_SP_ManufPerformanceMapmatTCCodeCVM = "ManufPerformanceMapmatTCCodeCVM";
        public static string DB_SP_ManufPerformanceMapmthTCCodeCVM = "ManufPerformanceMapmthTCCodeCVM";
        public static string DB_SP_ManufPerformanceMapqtrTCCodeCVM = "ManufPerformanceMapqtrTCCodeCVM";
        public static string DB_SP_ManufPerformanceMapytdfTCCodeCVM = "ManufPerformanceMapytdfTCCodeCVM";
        public static string DB_SP_ManufPerformanceMapytdcTCCodeCVM = "ManufPerformanceMapytdcTCCodeCVM";
        public static string DB_SP_ManufPerformanceMaphlfTCCodeCVM = "ManufPerformanceMaphlfTCCodeCVM";



        public static string DB_SP_GetCompanyPerformanceMap = "GetCompanyPerformanceMap";

        public static string DB_SP_GetBrandPerformance = "GetBrandPerformance";
       

        //BrandPerformance Sps
        public static string DB_SP_BrandPerformancemat = "BrandPerformancemat";
        public static string DB_SP_BrandPerformancemth = "BrandPerformancemth";
        public static string DB_SP_BrandPerformanceqtr = "BrandPerformanceqtr";
        public static string DB_SP_BrandPerformancehlf = "BrandPerformancehlf";
        public static string DB_SP_BrandPerformanceytdc = "BrandPerformanceytdc";
        public static string DB_SP_BrandPerformanceytdf = "BrandPerformanceytdf";

        //BrandPerformance Sps

        public static string DB_SP_BrandPerformancematCR = "BrandPerformancematCR";
        public static string DB_SP_BrandPerformancematCR1 = "BrandPerformancematCR1";
        public static string DB_SP_BrandPerformancemthCR = "BrandPerformancemthCR";
        public static string DB_SP_BrandPerformancemthCR1 = "BrandPerformancemthCR1";
        public static string DB_SP_BrandPerformanceqtrCR = "BrandPerformanceqtrCR";
        public static string DB_SP_BrandPerformanceqtrCR1 = "BrandPerformanceqtrCR1";
        public static string DB_SP_BrandPerformancehlfCR = "BrandPerformancehlfCR";
        public static string DB_SP_BrandPerformanceytdcCR = "BrandPerformanceytdcCR";
        public static string DB_SP_BrandPerformanceytdfCR = "BrandPerformanceytdfCR";
        public static string DB_SP_BrandPerformancehlfCR1 = "BrandPerformancehlfCR1";
        public static string DB_SP_BrandPerformanceytdcCR1 = "BrandPerformanceytdcCR1";
        public static string DB_SP_BrandPerformanceytdfCR1 = "BrandPerformanceytdfCR1";

        // BrandPerformance CVM Sps
        public static string DB_SP_BrandPerformancemthCVM = "BrandPerformancemthCVM";
        public static string DB_SP_BrandPerformanceqtrCVM = "BrandPerformanceqtrCVM";
        public static string DB_SP_BrandPerformancehlfCVM = "BrandPerformancehlfCVM";
        public static string DB_SP_BrandPerformancematCVM = "BrandPerformancematCVM";
        public static string DB_SP_BrandPerformanceytdcCVM = "BrandPerformanceytdcCVM";
        public static string DB_SP_BrandPerformanceytdfCVM = "BrandPerformanceytdfCVM";

        //SKUPerformance Sps
        public static string DB_SP_SKUPerformancemat = "SKUPerformancemat";
        public static string DB_SP_SKUPerformancemth = "SKUPerformancemth";
        public static string DB_SP_SKUPerformanceqtr = "SKUPerformanceqtr";
        public static string DB_SP_SKUPerformancehlf = "SKUPerformancehlf";
        public static string DB_SP_SKUPerformanceytdc = "SKUPerformanceytdc";
        public static string DB_SP_SKUPerformanceytdf = "SKUPerformanceytdf";

        // SKUPerformance CVM Sps
        public static string DB_SP_SKUPerformancemthCVM = "SKUPerformancemthCVM";
        public static string DB_SP_SKUPerformanceqtrCVM = "SKUPerformanceqtrCVM";
        public static string DB_SP_SKUPerformancehlfCVM = "SKUPerformancehlfCVM";
        public static string DB_SP_SKUPerformancematCVM = "SKUPerformancematCVM";
        public static string DB_SP_SKUPerformanceytdcCVM = "SKUPerformanceytdcCVM";
        public static string DB_SP_SKUPerformanceytdfCVM = "SKUPerformanceytdfCVM";


        public static string DB_SP_SKUPerformancematCVMProduct = "SKUPerformancematCVMProduct";
        public static string DB_SP_SKUPerformancemthCVMProduct = "SKUPerformancemthCVMProduct";
        public static string DB_SP_SKUPerformanceqtrCVMProduct = "SKUPerformanceqtrCVMProduct";
        public static string DB_SP_SKUPerformancehlfCVMProduct = "SKUPerformancehlfCVMProduct";
        public static string DB_SP_SKUPerformanceytdcCVMProduct = "SKUPerformanceytdcCVMProduct";
        public static string DB_SP_SKUPerformanceytdfCVMProduct = "SKUPerformanceytdfCVMProduct";


        // Brand Data
        //
        public static string DB_SP_BrandPerformancematCVMBrand = "BrandPerformancematCVMBrand";
        public static string DB_SP_BrandPerformancemthCVMBrand = "BrandPerformancemthCVMBrand";
        public static string DB_SP_BrandPerformanceqtrCVMBrand = "BrandPerformanceqtrCVMBrand";
        public static string DB_SP_BrandPerformancehlfCVMBrand = "BrandPerformancehlfCVMBrand";
        public static string DB_SP_BrandPerformanceytdcCVMBrand = "BrandPerformanceytdcCVMBrand";
        public static string DB_SP_BrandPerformanceytdfCVMBrand = "BrandPerformanceytdfCVMBrand";
        //Product CVM

        public static string DB_SP_BrandPerformancematCVMProduct = "BrandPerformancematCVMProduct";
        public static string DB_SP_BrandPerformancemthCVMProduct = "BrandPerformancemthCVMProduct";
        public static string DB_SP_BrandPerformanceqtrCVMProduct = "BrandPerformanceqtrCVMProduct";
        public static string DB_SP_BrandPerformancehlfCVMProduct = "BrandPerformancehlfCVMProduct";
        public static string DB_SP_BrandPerformanceytdcCVMProduct = "BrandPerformanceytdcCVMProduct";
        public static string DB_SP_BrandPerformanceytdfCVMProduct = "BrandPerformanceytdfCVMProduct";

        public static string DB_SP_BrandPerformancematCVMProductCR = "BrandPerformancematCVMProductCR";
        public static string DB_SP_BrandPerformancematCVMProductCR1 = "BrandPerformancematCVMProductCR1";
        public static string DB_SP_BrandPerformancemthCVMProductCR = "BrandPerformancemthCVMProductCR";
        public static string DB_SP_BrandPerformancemthCVMProductCR1 = "BrandPerformancemthCVMProductCR1";
        
        public static string DB_SP_BrandPerformanceqtrCVMProductCR = "BrandPerformanceqtrCVMProductCR";
        public static string DB_SP_BrandPerformanceqtrCVMProductCR1 = "BrandPerformanceqtrCVMProductCR1";
        public static string DB_SP_BrandPerformancehlfCVMProductCR = "BrandPerformancehlfCVMProductCR";
        public static string DB_SP_BrandPerformanceytdcCVMProductCR = "BrandPerformanceytdcCVMProductCR";
        public static string DB_SP_BrandPerformanceytdfCVMProductCR = "BrandPerformanceytdfCVMProductCR";
        public static string DB_SP_BrandPerformancehlfCVMProductCR1 = "BrandPerformancehlfCVMProductCR1";
        public static string DB_SP_BrandPerformanceytdcCVMProductCR1 = "BrandPerformanceytdcCVMProductCR1";
        public static string DB_SP_BrandPerformanceytdfCVMProductCR1 = "BrandPerformanceytdfCVMProductCR1";


        //BrandPerformance Map SPs
        public static string DB_SP_BrandPerformanceMapmat = "BrandPerformanceMapmat";
        public static string DB_SP_BrandPerformanceMapmth = "BrandPerformanceMapmth";
        public static string DB_SP_BrandPerformanceMapqtr = "BrandPerformanceMapqtr";
        public static string DB_SP_BrandPerformanceMaphlf = "BrandPerformanceMaphlf";
        public static string DB_SP_BrandPerformanceMapytdc = "BrandPerformanceMapytdc";
        public static string DB_SP_BrandPerformanceMapytdf = "BrandPerformanceMapytdf";

        // BrandPerformanceMap CVM Sps

        public static string DB_SP_BrandPerformanceMapmatCVM = "BrandPerformanceMapmatCVM";
        public static string DB_SP_BrandPerformanceMapmthCVM = "BrandPerformanceMapmthCVM";
        public static string DB_SP_BrandPerformanceMapqtrCVM = "BrandPerformanceMapqtrCVM";
        public static string DB_SP_BrandPerformanceMaphlfCVM = "BrandPerformanceMaphlfCVM";
        public static string DB_SP_BrandPerformanceMapytdcCVM = "BrandPerformanceMapytdcCVM";
        public static string DB_SP_BrandPerformanceMapytdfCVM = "BrandPerformanceMapytdfCVM";

        //BrandPerformance Map SPs
        public static string DB_SP_SKUPerformanceMapmat = "SKUPerformanceMapmat";
        public static string DB_SP_SKUPerformanceMapmth = "SKUPerformanceMapmth";
        public static string DB_SP_SKUPerformanceMapqtr = "SKUPerformanceMapqtr";
        public static string DB_SP_SKUPerformanceMaphlf = "SKUPerformanceMaphlf";
        public static string DB_SP_SKUPerformanceMapytdc = "SKUPerformanceMapytdc";
        public static string DB_SP_SKUPerformanceMapytdf = "SKUPerformanceMapytdf";

        // BrandPerformanceMap CVM Sps

        public static string DB_SP_SKUPerformanceMapmatCVM = "SKUPerformanceMapmatCVM";
        public static string DB_SP_SKUPerformanceMapmthCVM = "SKUPerformanceMapmthCVM";
        public static string DB_SP_SKUPerformanceMapqtrCVM = "SKUPerformanceMapqtrCVM";
        public static string DB_SP_SKUPerformanceMaphlfCVM = "SKUPerformanceMaphlfCVM";
        public static string DB_SP_SKUPerformanceMapytdcCVM = "SKUPerformanceMapytdcCVM";
        public static string DB_SP_SKUPerformanceMapytdfCVM = "SKUPerformanceMapytdfCVM";

        public static string DB_SP_SKUPerformanceMapmatCVMProduct = "SKUPerformanceMapmatCVMProduct";
        public static string DB_SP_SKUPerformanceMapmthCVMProduct = "SKUPerformanceMapmthCVMProduct";
        public static string DB_SP_SKUPerformanceMapqtrCVMProduct = "SKUPerformanceMapqtrCVMProduct";
        public static string DB_SP_SKUPerformanceMaphlfCVMProduct = "SKUPerformanceMaphlfCVMProduct";
        public static string DB_SP_SKUPerformanceMapytdcCVMProduct = "SKUPerformanceMapytdcCVMProduct";
        public static string DB_SP_SKUPerformanceMapytdfCVMProduct = "SKUPerformanceMapytdfCVMProduct";

        // Brand CVM
        public static string DB_SP_BrandPerformanceMapmatCVMBrand = "BrandPerformanceMapmatCVMBrand";
        public static string DB_SP_BrandPerformanceMapmthCVMBrand = "BrandPerformanceMapmthCVMBrand";
        public static string DB_SP_BrandPerformanceMapqtrCVMBrand = "BrandPerformanceMapqtrCVMBrand";
        public static string DB_SP_BrandPerformanceMaphlfCVMBrand = "BrandPerformanceMaphlfCVMBrand";
        public static string DB_SP_BrandPerformanceMapytdcCVMBrand = "BrandPerformanceMapytdcCVMBrand";
        public static string DB_SP_BrandPerformanceMapytdfCVMBrand = "BrandPerformanceMapytdfCVMBrand";
        // Product CVM
        public static string DB_SP_BrandPerformanceMapmatCVMProduct = "BrandPerformanceMapmatCVMProduct";
        public static string DB_SP_BrandPerformanceMapmthCVMProduct = "BrandPerformanceMapmthCVMProduct";
        public static string DB_SP_BrandPerformanceMapqtrCVMProduct = "BrandPerformanceMapqtrCVMProduct";
        public static string DB_SP_BrandPerformanceMaphlfCVMProduct = "BrandPerformanceMaphlfCVMProduct";
        public static string DB_SP_BrandPerformanceMapytdcCVMProduct = "BrandPerformanceMapCVMytdcProduct";
        public static string DB_SP_BrandPerformanceMapytdfCVMProduct = "BrandPerformanceMapytdfCVMProduct";


        public static string DB_SP_GetCompanyTeamWiseUserCount = "GetCompanyTeamWiseUserCount";
        public static string DB_SP_GetCompanyUserCount = "GetCompanyUserCount";
        public static string DB_SP_GetCompanyUsers = "GetCompanyUsers";
        public static string DB_SP_GetDivisionUsers = "GetDivisionUsers";
        public static string DB_SP_GetSubscribedCompanyList = "GetSubscribedCompanyList";
        public static string DB_SP_GetUnsubscribedCompanyList = "GetUnsubscribedCompanyList";

        public static string DB_SP_GetTeamPermissions = "GetTeamPermissions";
        public static string DB_SP_GetUserInfo = "GetUserInfo";
        public static string DB_SP_DeleteUser = "DeleteUser";
        public static string DB_SP_DeleteCompanyUserRole = "DeleteCompanyUserRole";
        public static string DB_SP_DeleteCompany = "DeleteCompany";
        public static string DB_SP_DeleteDivision = "DeleteDivision";
        public static string DB_SP_DeleteBrandGroup = "DeleteBrandGroup";
        public static string DB_SP_DeleteCluster = "DeleteCluster";
        public static string DB_SP_DeleteDivisionProductMapping = "DeleteDivisionProductMapping";
        
        public static string DB_SP_GetUserPermissions = "GetUserPermissions";
        public static string DB_SP_GetUserRoles = "GetUserRoles";
        public static string DB_SP_GetUserRolesByCompany = "GetUserRolesByCompany";
        public static string DB_SP_GetCompanyUserRoleByRoleID = "GetCompanyUserRoleByRoleID";
        public static string DB_SP_CreateUpdateCompanyRole = "CreateUpdateCompanyRole";

        public static string DB_SP_CreateUpdateCompanyRolePermission = "CreateUpdateCompanyRolePermission";



        public static string DB_SP_GetSavedSearchesbyUser = "GetSavedSearchesbyUser";
        public static string DB_SP_CreateSavedSearches = "CreateSavedSearches";
        public static string DB_SP_DeleteSavedSearches = "DeleteSavedSearches";

        public static string DB_SP_CreateUpdateTeam = "CreateUpdateTeam";   
        public static string DB_SP_CreateUpdateUser = "CreateUpdateUser";
        public static string DB_SP_CreateUpdateCompanyDivision = "CreateUpdateDivision";
        public static string DB_SP_AddDivisionProductMapping = "AddDivisionProductMapping";
        public static string DB_SP_GetCompanyUnMappedProduct = "GetCompanyUnMappedProduct";
        public static string DB_SP_CreateUpdateCustomBrick = "CreateUpdateCustomBrick";
        public static string DB_SP_GetCustomPatchbyCompany = "GetCustomPatchbyCompany";
        public static string DB_SP_GetCustomPatchDetails = "GetCustomPatchDetails";

        public static string DB_SP_DeleteBrickPincodemappingbyBrickID = "DeleteBrickPincodemappingbyBrickID";
        public static string DB_SP_CreateBrickPincodeMapping = "CreateBrickPincodeMapping";

        public static string DB_SP_CreateUserAccessLog = "CreateUserAccessLog";
        public static string DB_SP_GetUserAccesslog = "GetUserAccesslog";
        public static string DB_SP_InsertUpdateUserLogin = "InsertUpdateUserLogin";





        public static string DB_SP_InsertUpdateTeamPermissionMapping = "InsertUpdateTeamPermissionMapping";

        public static string DB_SP_InsertUpdateTeamUserAssocation = "InsertUpdateTeamUserAssocation";

        public static string DB_SP_UpdateSessionData = "UpdateSessionData";

        public static string DB_SP_GetCompanySubscriptionType = "GetCompanySubscriptionType";
        public static string DB_SP_CreateUpdateSubscriptionType = "CreateUpdateSubscriptionType";
        public static string DB_SP_GetDivisionProducts = "GetDivisionProducts";

        public static string DB_SP_GetProductsByBrand = "GetProductsByBrand";

        public static string DB_SP_GetDivisionName = "GetDivisionName";



        public static string DB_SP_GetSubscriptionTypeInfo = "GetSubscriptionTypeInfo";

        public static string DB_SP_GetCityDetails = "GetCityDetails";
        public static string DB_SP_GetCityTotalSales = "GetCityTotalSales";
        public static string DB_SP_GetSupergroupSalesData = "GetSupergroupSalesData";
        public static string DB_SP_GetBrandSalesData = "GetBrandSalesData";
        public static string DB_SP_GetCompanySalesData = "GetCompanySalesData";
        public static string DB_SP_GetCityNameBasedOnCityIDs = "GetCityNameBasedOnCityIDs";
        public static string DB_SP_GetDivisionInfo = "GetDivisionInfo";
        public static string DB_SP_GetDivisionBrandList = "GetDivisionBrands";
        public static string DB_SP_CreateUpdateBrandGroup = "CreateUpdateBrandGroup";
        public static string DB_SP_GetBrandGroupListByDivision = "GetBrandGroupListByDivision";
        public static string DB_SP_GetBrandGroupInfo = "GetBrandGroupInfo";

        // Cluster SPS
        public static string DB_SP_CreateUpdateCompanyCluster = "CreateUpdateCompanyCluster";
        public static string DB_SP_GetCompanyClusterList = "GetCompanyClusterList";
        public static string DB_SP_GetCompanyClusterInfo = "GetCompanyClusterInfo";

        public static string DB_SP_GenerateCompanyCVMData = "GenerateCompanyCVMData";

        public static string DB_SP_GetSubscribedCompanyInfo = "GetSubscribedCompanyInfo";

        public static string DB_SP_Userlogin = "UserLogin";
        public static string DB_SP_GetUserInfoDetails = "GetUserInfoDetails";
        public static string DB_SP_GetSessionData = "GetSessionData";
        public static string DB_SP_GetSubscriptionCompanyCount = "GetSubscriptionCompanyCount";

        //product SPS
        public static string DB_SP_GetFocusedProductsByCompanyDivison = "GetFocusedProductsByCompanyDivison";
        public static string DB_SP_GetNewIntroductionProductsByCompanyDivison = "GetNewIntroductionProductsByCompanyDivison";
        public static string DB_SP_GetProductsByCompanyDivison = "GetProductsByCompanyDivison";

       
        
        public static string DB_SP_GetMarketSalesInitialloadmat = "MarketSalesInitialloadmat";
        public static string DB_SP_MarketSalesInitialloadmatWithRights = "MarketSalesInitialloadmatWithRights";
        public static string DB_SP_GetMarketSalesInitialloadmth = "MarketSalesInitialloadmth";
        public static string DB_SP_MarketSalesInitialloadmthWithRights = "MarketSalesInitialloadmthWithRights";
        public static string DB_SP_GetMarketSalesInitialloadqtr = "MarketSalesInitialloadqtr";
        public static string DB_SP_MarketSalesInitialloadqtrWithRights = "MarketSalesInitialloadqtrWithRights";
        public static string DB_SP_GetMarketSalesInitialloadhlf = "MarketSalesInitialloadhlf";
        public static string DB_SP_MarketSalesInitialloadhlfWithRights = "MarketSalesInitialloadhlfWithRights";

        public static string DB_SP_GetMarketSalesInitialloadytdf = "MarketSalesInitialloadytdf";
        public static string DB_SP_MarketSalesInitialloadytdfWithRights = "MarketSalesInitialloadytdfWithRights";
        public static string DB_SP_GetMarketSalesInitialloadytdc = "MarketSalesInitialloadytdc";
        public static string DB_SP_MarketSalesInitialloadytdcWithRights = "MarketSalesInitialloadytdcWithRights";




        public static string DB_SP_GetMarketSharemat = "GetMarketSharemat";
        public static string DB_SP_GetMarketSharematWithRights = "GetMarketSharematWithRights";
        public static string DB_SP_GetMarketSharemth = "GetMarketSharemth";
        public static string DB_SP_GetMarketSharemthWithRights = "GetMarketSharemthWithRights";
        public static string DB_SP_GetMarketShareQtr = "GetMarketShareQtr";
        public static string DB_SP_GetMarketShareQtrWithRights = "GetMarketShareQtrWithRights";
        public static string DB_SP_GetMarketSharehlf = "GetMarketSharehlf";
        public static string DB_SP_GetMarketSharehlfWithRights = "GetMarketSharehlfWithRights";
        public static string DB_SP_GetMarketShareytdf = "GetMarketShareytdf";
        public static string DB_SP_GetMarketShareytdfWithRights = "GetMarketShareytdfWithRights";
        public static string DB_SP_GetMarketShareytdc = "GetMarketShareytdc";
        public static string DB_SP_GetMarketShareytdcWithRights = "GetMarketShareytdcWithRights";
        public static string DB_SP_GetMarketSharemthProductData = "GetMarketSharemthProductData";
        public static string DB_SP_GetMarketShareQtrProductData = "GetMarketShareQtrProductData";
        public static string DB_SP_GetMarketSharematProductData = "GetMarketSharematProductData";
        public static string DB_SP_GetMarketSharehlfProductData = "GetMarketSharehlfProductData";
        public static string DB_SP_GetMarketShareytdcProductData = "GetMarketShareytdcProductData";
        public static string DB_SP_GetMarketShareytdfProductData = "GetMarketShareytdfProductData";

        public static string DB_SP_GetMarketSharemthBrandData = "GetMarketSharemthBrandData";
        public static string DB_SP_GetMarketShareQtrBrandData = "GetMarketShareQtrBrandData";
        public static string DB_SP_GetMarketSharematBrandData = "GetMarketSharematBrandData";
        public static string DB_SP_GetMarketSharehlfBrandData = "GetMarketSharehlfBrandData";
        public static string DB_SP_GetMarketShareytdcBrandData = "GetMarketShareytdcBrandData";
        public static string DB_SP_GetMarketShareytdfBrandData = "GetMarketShareytdfBrandData";

        public static string DB_SP_GetTherapyList = "GetTherapyList";
     


        public static string DB_SP_GetTimePeriodListBasedOnPeriodType = "GetTimePeriodListBasedOnPeriodType";

        public static string DB_SP_GetbrandList = "GetbrandList";
        public static string DB_SP_GetbrandByCompanyID = "GetbrandByCompanyID";
        public static string DB_SP_GetBrandsByTherapy = "GetBrandsByTherapy";
        public static string DB_SP_GetTherapyListByCode = "GetTherapyListByCode";
        public static string DB_SP_GetProductByTherapy = "GetProductByTherapy";

        public static string DB_SP_GetTherapyListByTherapyCodes = "GetTherapyListByTherapyCodes";

        public static string DB_SP_GetTCCodebySuperGroup = "GetTCCodebySuperGroup";

        public static string DB_SP_GetDataSubscriptionPeriodType = "GetDataSubscriptionPeriodType";
        public static string DB_SP_GetDataSubscriptionPeriodTypeForCompany = "GetDataSubscriptionPeriodTypeForCompany";


        public static string DB_SP_GetTherapyMasterList = "GetTherapyMasterList";

        public static string DB_SP_GetApplicationPermissionList = "GetApplicationPermissionList";
        public static string DB_SP_GetApplicationDataAccessLevelList = "GetApplicationDataAccessLevelList";




        public static string DB_SP_GetSubscribedCompanyColorCode = "GetSubscribedCompanyColorCode";


        // Access right SP Details

        public static string DB_SP_DeleteCompanyAccessRights = "DeleteCompanyAccessRights";
        public static string DB_SP_DeleteUserAccessRights = "DeleteUserAccessRights";
        public static string DB_SP_GetUserAccessRights = "GetUserAccessRights";
        public static string DB_SP_GetCompanyAccessRights = "GetCompanyAccessRights";
        public static string DB_SP_CreateUpdateUserAccessRights = "CreateUpdateUserAccessRights";
        public static string DB_SP_UpdateUserAccessRights = "UpdateUserAccessRights";
        public static string DB_SP_CreateUpdateCompanyAccessRights = "CreateUpdateCompayAccessRights";

        public static string DB_SP_GetSuperGroupList = "GetSuperGroupList";


        // SP List related to subsctiption Type List

        public static string DB_SP_GetSubscriptionPeriodTypeList = "GetSubscriptionPeriodTypeList";
        public static string DB_SP_GetSubscriptionPeriodDetails = "GetSubscriptionPeriodDetails";
        public static string DB_SP_GetDivisionSubscriptionPeriodDetails = "GetDivisionSubscriptionPeriodDetails";
        public static string DB_SP_GetSubscribedCompanyPeriods = "GetSubscribedCompanyPeriods";



        // CVM
        public static string DB_SP_GetProductCVM = "GetProductCVM";
        public static string DB_SP_AddProductCVM = "AddProductCVM";
        public static string DB_SP_DeleteProductCVM = "DeleteProductCVM";
        public static string DB_SP_GetCompanyDefinedCVM = "GetCompanyDefinedCVM";
        public static string DB_SP_GetCompanyProduct = "GetCompanyProduct";
        public static string DB_SP_GetProductsByTherapy = "GetProductsByTherapy";
        public static string DB_SP_GetPFCIDByTC4Code = "GetPFCIDByTC4Code";

        public static string DB_SP_GetProductTherapy = "GetProductTherapy";
        public static string DB_SP_GetCityNameByCityID = "GetCityNameByCityID";




        public static string DB_SP_GetAdminUsers = "GetAdminUsers";


    }
}

