﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.Common
{
    public enum UserRoleEnum : int
    {
        /// <summary>
        /// Super Admin
        /// </summary>
        SA = 1,
        /// <summary>
        /// Admin
        /// </summary>
        A = 2,
        /// <summary>
        /// Teacher
        /// </summary>
        U = 3
        /// <summary>
        /// Student
        /// </summary>
       
    }
  }
