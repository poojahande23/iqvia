﻿using IMSBI.DAL;

using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
   public class LoginBusinessManager
    {
        public UserInfo GetLogin(string UserName, string Password)
        {
            UserInfo objUserInfo =null;
            try
            {
                UserDataAccess objLoginDataAccess = new UserDataAccess();
                UserInfo objResult = objLoginDataAccess.UserLogin(UserName, Password);
                if(objResult != null)
                {
                    string SessionID = Guid.NewGuid().ToString();
                    SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                    int Status = objSessionDataAccess.SetSession(SessionID, objResult.UserID);
                    objSessionDataAccess = null;
                    objResult.SessionID = SessionID;
                    objResult.objUserMenuAccess = this.UserMenuAccessRights(objResult.UserID);

                    objUserInfo = objResult;
                    UserLogAccessDetails objUserLogin = new UserLogAccessDetails();
                    objUserLogin.InsertUpdateUserlogin(objResult.UserID, SessionID, true);
                    objUserLogin = null;


                }
                objLoginDataAccess = null;
            }
            catch
            {
                throw;
            }
            return objUserInfo;
        }

        public MenuAccessRights UserMenuAccessRights(Int32 UserID)
        {
            MenuAccessRights objMenuAccessRights = new MenuAccessRights();
            try
            {
                AccessRightsDataAccess objAccessRightDataAccess = new AccessRightsDataAccess();
                UserAccessRights objUserAccessRights = objAccessRightDataAccess.GetUserAccessRights(UserID);
                objAccessRightDataAccess = null;
              
            }
            catch
            {
                throw;
            }

            return objMenuAccessRights;

        }

        public UserInfo GetUserSession(string SessionID)
        {
            UserInfo objUserInfo = null;
            try
            {

                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                int UserID = objSessionDataAccess.ValidateSession(SessionID);
                

                if(UserID >0 )
                {
                    

                    UserDataAccess objLoginDataAccess = new UserDataAccess();
                    UserInfo objResult = objLoginDataAccess.UserInfoDetails(UserID);
                    if (objResult != null)
                    {                      
                        objResult.SessionID = SessionID;

                        objUserInfo = objResult;


                    }

                    objLoginDataAccess = null;
                }

                objSessionDataAccess = null;
            }
            catch
            {
                throw;
            }
            return objUserInfo;
        }

        public Int32 LogOut(string SessionID)
        {
            int Status = 0;
            try
            {
                UserLogAccessDetails objUserLogin = new UserLogAccessDetails();
                objUserLogin.InsertUpdateUserlogin(0, SessionID, false);
                objUserLogin = null;

                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                 Status = objSessionDataAccess.DeleteSession(SessionID);
                objSessionDataAccess = null;


            }
            catch
            {
                throw;
            }
            return Status;
        }


    }
}
