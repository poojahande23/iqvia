﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
   public class AnalysisBusinessManager
    {
        public Analysis_OVM GetAnalysisDashboardData(Analysis_IVM objAnalysis)
        {
            Analysis_OVM AnalysisData = new Analysis_OVM();
           // try
            //{
                AnalysisDataAccess objAnalysisDataAccess = new AnalysisDataAccess();
               
                AnalysisData = objAnalysisDataAccess.GetAnalysisDashboardData(objAnalysis);
                objAnalysisDataAccess = null;
            //}
           // catch (Exception ex)
           // {
             //   throw ex;
            //}

            return AnalysisData;
        }

      /*
        public CompanyProductBrandreport_OVM CompanyProductBrandReport(Analysis_IVM objCompanyProductBrandReportInput)
        {
            CompanyProductBrandreport_OVM objCompanyProductBrandReportOVM = new CompanyProductBrandreport_OVM();
            try
            {
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                int UserID = objSessionDataAccess.ValidateSession(objCompanyProductBrandReportInput.SessionID);
                objSessionDataAccess = null;
                if (UserID == 0)
                {
                    objCompanyProductBrandReportOVM = null;
                    return objCompanyProductBrandReportOVM;
                }

                // Get UserAccessRights 

                AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

                int AccessType = 1;
                string TCCodes = string.Empty;
                if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
                {

                    AccessType = 2;
                    TCCodes = objUserAccessrights.TCValues;

                }
                else
                {
                    CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objCompanyProductBrandReportInput.CompanyID);
                    if (objCompanyAccessRight != null)
                    {
                        switch (objCompanyAccessRight.AccessRightType)
                        {
                            case 2:
                                AccessType = 2;
                                FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                                IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                                TCCodes = string.Join(",", TCCodesList);
                                break;
                            case 3:
                                AccessType = 2;
                                TCCodes = objCompanyAccessRight.TCValues;
                                break;
                        }
                    }
                }

                objAccessRightsDataAccess = null;

                if (!string.IsNullOrEmpty(objCompanyProductBrandReportInput.CategoryValue))
                {
                    DataSet resultds = new DataSet();


                    string[] ProductIDS = objCompanyProductBrandReportInput.CategoryValue.Split(',');
                    IList<Int32> SelectedProducts = new List<Int32>();
                    for (int i = 0; i < ProductIDS.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(ProductIDS[i]))
                        {
                            if (!SelectedProducts.Contains(Int32.Parse(ProductIDS[i])))
                            {
                                SelectedProducts.Add(Int32.Parse(ProductIDS[i]));
                            }

                        }
                    }
                    objCompanyProductBrandReportOVM.BrandSalesUnit = new List<BrandTimeperiodData>();
                    objCompanyProductBrandReportOVM.BrandSalesValue = new List<BrandTimeperiodData>();
                    objCompanyProductBrandReportOVM.BrandCVMSalesUnit = new List<BrandTimeperiodData>();
                    objCompanyProductBrandReportOVM.BrandCVMSalesValue = new List<BrandTimeperiodData>();
                    objCompanyProductBrandReportOVM.BrandTableValue = new List<BrandTableDetails>();
                    objCompanyProductBrandReportOVM.BrandTableUnit = new List<BrandTableDetails>();

                    CompanyDataAccess objProductDetailsCompany = new CompanyDataAccess();
                    Dictionary<Int32, string> SelectedProductDetails = objProductDetailsCompany.GetSelectedProductList(SelectedProducts, objCompanyProductBrandReportInput.ProductType);
                    objProductDetailsCompany = null;
                    for (int i = 0; i < SelectedProducts.Count; i++)
                    {
                        IList<ProductTherapy> ProductTherapy = this.GetCVMTherapy(SelectedProducts[i], objCompanyProductBrandReportInput.ProductType);
                        Int32 CVMType = 1; // 1= Therapy, 2= Product and 3= Both
                        IList<string> TherapyCode = new List<string>();
                        IList<Int32> ProductList = new List<Int32>();

                        if (ProductTherapy != null && ProductTherapy.Count > 0)
                        {
                            for (int j = 0; j < ProductTherapy.Count; j++)
                            {
                                if (!ProductList.Contains(ProductTherapy[j].ProductID))
                                {
                                    ProductList.Add(ProductTherapy[j].ProductID);
                                }
                                bool IsCVMDefined = false;
                                IList<Int32> CVMProductList = this.GetCVMProducts(ProductTherapy[j].ProductID);
                                if (CVMProductList != null && CVMProductList.Count > 0)
                                {

                                    IsCVMDefined = true;
                                    for (int k = 0; k < CVMProductList.Count; k++)
                                    {
                                        if (!ProductList.Contains(CVMProductList[k]))
                                        {
                                            ProductList.Add(CVMProductList[k]);
                                        }
                                    }
                                }

                                if (!IsCVMDefined)
                                {
                                    if (!TherapyCode.Contains(ProductTherapy[j].TC4Code))
                                    {
                                        TherapyCode.Add(ProductTherapy[j].TC4Code);
                                    }
                                }


                            }

                        }
                        string TherapyDetails = string.Empty;

                        if (TherapyCode != null && TherapyCode.Count > 0)
                        {
                            string Products = string.Empty; ;
                            if (ProductList.Count > 0)
                            {
                                Products = "CVM Products + ";
                            }
                            TherapyDetails = Products + this.GetTherapyDetails(TherapyCode);
                            CompanyDataAccess DataAccessTherapy = new CompanyDataAccess();
                            IList<CompanyProductList_OVM> TherapyProductList = DataAccessTherapy.GetPFCIDByTC4Code(string.Join(",", TherapyCode));
                            DataAccessTherapy = null;
                            for (int z = 0; z < TherapyProductList.Count; z++)
                            {
                                if (!ProductList.Contains(TherapyProductList[z].PFCID))
                                {
                                    ProductList.Add(TherapyProductList[z].PFCID);
                                }
                            }
                        }
                        else
                        {
                            TherapyDetails = this.GetProductDetails(ProductList);
                        }


                        BrandPerformanceDataAccess objBrandPerformanceData = new BrandPerformanceDataAccess();
                        resultds = objBrandPerformanceData.GetProductBrandSalesCVM(objCompanyProductBrandReportInput, ProductList);
                        objBrandPerformanceData = null;

                        var resultBrandCVM = (from row in resultds.Tables[0].AsEnumerable()
                                              select new RawBrandToBrandData
                                              {
                                                  BrandID = Convert.ToInt32(row["BrandID"]),
                                                  BrandName = Convert.ToString(row["BrandName"]),
                                                  PeriodID = Convert.ToInt32(row["TimePeriod"]),
                                                  PeriodName = Convert.ToString(row["MonthName"]),
                                                  Value = Convert.ToDouble(row["Value"]),
                                                  Units = Convert.ToDouble(row["Units"]),
                                                  CompanyName = Convert.ToString(row["CompanyName"])

                                              }
                              ).ToList();

                        // Get the Brand Sale;
                        var ResultBrandData = from r in resultBrandCVM
                                              group r by new
                                              {
                                                  r.BrandID,
                                                  r.BrandName,
                                                  r.CompanyName
                                              } into g
                                              select new RawBrandToBrandData
                                              {
                                                  BrandID = g.Key.BrandID,
                                                  BrandName = g.Key.BrandName,
                                                  CompanyName = g.Key.CompanyName,
                                                  Value = g.Sum(a => a.Value),
                                                  Units = g.Sum(a => a.Units)
                                              };
                        ResultBrandData = ResultBrandData.OrderByDescending(a => a.Value).ToList();

                        bool CurrentProductAdded = false;
                        Dictionary<int, BrandDataObject> Top5BrandSalesDetails = new Dictionary<int, BrandDataObject>();

                        Int32 Counter = 0;
                        // BrandDataObject CurrentBrandObject = new BrandDataObject();
                        foreach (RawBrandToBrandData item in ResultBrandData)
                        {
                            if (Counter < 5)
                            {
                                if (!Top5BrandSalesDetails.ContainsKey(item.BrandID))
                                {
                                    BrandDataObject obj = new BrandDataObject()
                                    {
                                        BrandID = item.BrandID,
                                        BrandName = item.BrandName,
                                        Rank = Counter + 1,
                                        CompanyName = item.CompanyName
                                    };
                                    Top5BrandSalesDetails.Add(item.BrandID, obj);
                                    if (item.BrandID == SelectedProducts[i])
                                    {
                                        CurrentProductAdded = true;
                                        // CurrentBrandObject = obj;
                                    }
                                }


                            }
                            if (!CurrentProductAdded && item.BrandID == SelectedProducts[i])
                            {
                                if (!Top5BrandSalesDetails.ContainsKey(item.BrandID))
                                {
                                    BrandDataObject obj = new BrandDataObject()
                                    {
                                        BrandID = item.BrandID,
                                        BrandName = item.BrandName,
                                        Rank = Counter + 1,
                                        CompanyName = item.CompanyName
                                    };
                                    Top5BrandSalesDetails.Add(item.BrandID, obj);
                                    // CurrentBrandObject = obj;
                                }
                            }

                            Counter++;
                        }

                        ResultBrandData = ResultBrandData.OrderByDescending(a => a.Units).ToList();

                        CurrentProductAdded = false;
                        Dictionary<int, BrandDataObject> Top5BrandUnitDetails = new Dictionary<int, BrandDataObject>();

                        Counter = 0;
                        foreach (RawBrandToBrandData item in ResultBrandData)
                        {
                            if (Counter < 5)
                            {
                                if (!Top5BrandUnitDetails.ContainsKey(item.BrandID))
                                {
                                    BrandDataObject obj = new BrandDataObject()
                                    {
                                        BrandID = item.BrandID,
                                        BrandName = item.BrandName,
                                        Rank = Counter + 1,
                                        CompanyName = item.CompanyName
                                    };
                                    Top5BrandUnitDetails.Add(item.BrandID, obj);
                                }

                                if (item.BrandID == SelectedProducts[i])
                                {
                                    CurrentProductAdded = true;
                                }
                            }
                            if (!CurrentProductAdded && item.BrandID == SelectedProducts[i])
                            {
                                if (!Top5BrandUnitDetails.ContainsKey(item.BrandID))
                                {
                                    BrandDataObject obj = new BrandDataObject()
                                    {
                                        BrandID = item.BrandID,
                                        BrandName = item.BrandName,
                                        Rank = Counter + 1,
                                        CompanyName = item.CompanyName
                                    };
                                    Top5BrandUnitDetails.Add(item.BrandID, obj);
                                }
                            }

                            Counter++;
                        }

                        BrandDataObject CurrentBrandObject = new BrandDataObject();
                        CurrentBrandObject.BrandID = SelectedProducts[i];
                        string BrandName = string.Empty;
                        if (SelectedProductDetails.TryGetValue(SelectedProducts[i], out BrandName))
                        {
                            CurrentBrandObject.BrandName = BrandName;
                        }

                        BrandTimeperiodData objBrandTimePeriodData = this.GetMultiPartChartData(resultBrandCVM, objCompanyProductBrandReportInput.SessionID, Top5BrandSalesDetails, 0, CurrentBrandObject);
                        BrandTimeperiodData objBrandTimePeriodDataUnit = this.GetMultiPartChartData(resultBrandCVM, objCompanyProductBrandReportInput.SessionID, Top5BrandUnitDetails, 1, CurrentBrandObject);
                        objCompanyProductBrandReportOVM.BrandSalesUnit.Add(objBrandTimePeriodDataUnit);
                        objCompanyProductBrandReportOVM.BrandSalesValue.Add(objBrandTimePeriodData);
                        BrandTableDetails objBrandTableValue = this.GetBrandTableData(resultBrandCVM, objCompanyProductBrandReportInput.SessionID, Top5BrandSalesDetails, 0, TherapyDetails, CurrentBrandObject);
                        BrandTableDetails objBrandTableUnit = this.GetBrandTableData(resultBrandCVM, objCompanyProductBrandReportInput.SessionID, Top5BrandUnitDetails, 1, TherapyDetails, CurrentBrandObject);
                        objCompanyProductBrandReportOVM.BrandTableUnit.Add(objBrandTableUnit);
                        objCompanyProductBrandReportOVM.BrandTableValue.Add(objBrandTableValue);
                        BrandTimeperiodData objBrandTimePeriodDataCVM = this.GetMultiPartChartDataCVM(resultBrandCVM, objCompanyProductBrandReportInput.SessionID, Top5BrandSalesDetails, 0, TherapyDetails, CurrentBrandObject);
                        BrandTimeperiodData objBrandTimePeriodDataUnitCVM = this.GetMultiPartChartDataCVM(resultBrandCVM, objCompanyProductBrandReportInput.SessionID, Top5BrandUnitDetails, 1, TherapyDetails, CurrentBrandObject);
                        objCompanyProductBrandReportOVM.BrandCVMSalesUnit.Add(objBrandTimePeriodDataUnitCVM);
                        objCompanyProductBrandReportOVM.BrandCVMSalesValue.Add(objBrandTimePeriodDataCVM);



                    }



                }

            }
            catch
            {
                throw;
            }
            return objCompanyProductBrandReportOVM;
        }

        */

        public MarketShareGraphData GetAnalysisMarketShareData(AnalysisMarketShare_IVM objAnalysis)
        {
            MarketShareGraphData AnalysisMarketShareData = new MarketShareGraphData();
            try
            {
                AnalysisDataAccess objAnalysisDataAccess = new AnalysisDataAccess();
                AnalysisMarketShareData = objAnalysisDataAccess.GetAnalysisMarketShareData(objAnalysis);
                objAnalysisDataAccess = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return AnalysisMarketShareData;
        }



    }
}
