﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
    public class UserBusinessManager
    {
        public IList<CompanyTeamDetails> GetCompanyTeamWiseUserCount(int CompanyID)
        {
            IList<CompanyTeamDetails> CompanyTeamList = new List<CompanyTeamDetails>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyTeamWiseUserCount + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyTeamDetails
                              {
                                  TeamID = Convert.ToInt32(row["TeamID"]),
                                  TeamName = Convert.ToString(row["TeamName"]),
                                  UserCount = Convert.ToInt32(row["UserCount"])

                              }
                              ).ToList();

                CompanyTeamList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyTeamList;
        }

        public IList<CompanyUserCount_OVM> GetCompanyUserCount(int CompanyID)
        {
            IList<CompanyUserCount_OVM> objCompanyUserCount = new List<CompanyUserCount_OVM>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyUserCount + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new  CompanyUserCount_OVM
                              {
                                     CompanyUserRoleID =  Convert.ToInt32(row["CompanyRoleID"]),
                                  UserCount = Convert.ToInt32(row["UserCount"]),
                                  RoleName = Convert.ToString(row["RoleName"]) + " (" + Convert.ToInt32(row["UserCount"]) + ")",
                              } 
                               ).ToList();
                objCompanyUserCount = result;


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objCompanyUserCount;
        }

        public IList<CompanyUser> GetCompanyUsers(int CompanyID)
        {

            IList<CompanyUser> CompanyDivisionUserList = new List<CompanyUser>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyUsers + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyUser
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  Email = Convert.ToString(row["Email"]),
                                  MobileNo = Convert.ToDouble(row["MobileNo"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  Pincode = Convert.ToInt32(row["Pincode"]),
                                  CompanyRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  RoleName = Convert.ToString(row["RoleName"]),
                                  ReportingManagerID = Convert.ToInt32(row["ReportingManagerID"]),
                                  ReportingManagerName = Convert.ToString(row["ReportingManagerName"]),
                                  DivisionName = Convert.ToString(row["DivisionName"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  ProfileImage =  Convert.ToString(row["ProfileImage"]),
                                  Active = Convert.ToInt32(row["Active"])
                              }
                              ).ToList();

                CompanyDivisionUserList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyDivisionUserList;
        }

        public IList<CompanyUser> GetAdminUsers()
        {

            IList<CompanyUser> objAdminUserList = new List<CompanyUser>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetAdminUsers + " "  ;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyUser
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  Email = Convert.ToString(row["Email"]),
                                  MobileNo = Convert.ToDouble(row["MobileNo"]),
                                  RoleID = Convert.ToInt32(row["RoleID"]),
                                  ProfileImage = Convert.ToString(row["ProfileImage"]),
                                  Active = Convert.ToInt32(row["Active"])
                              }
                              ).ToList();

                objAdminUserList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objAdminUserList;
        }



        public CompanyUser GetUserInfo(int UserID)
        {

            CompanyUser UserInfo = new CompanyUser();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetUserInfo + " " + UserID;
                resultds = IMSBIDB.ReturnDataset(str);
              
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyUser
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  Email = Convert.ToString(row["Email"]),
                                  MobileNo = Convert.ToDouble(row["MobileNo"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  Pincode = Convert.ToInt32(row["Pincode"]),
                                  RoleID = Convert.ToInt32(row["RoleID"]),
                                  Active = Convert.ToInt32(row["Active"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  CompanyRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  RoleName = Convert.ToString(row["RoleName"]),
                                  ReportingManagerID = Convert.ToInt32(row["ReportingManagerID"]),
                                  ProfileImage =  Convert.ToString(row["ProfileImage"])


                              }
                              ).FirstOrDefault();

                UserInfo = result;
                // Get the user access rigths
                AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                UserInfo.UserAccessRights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
                objAccessRightsDataAccess = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserInfo;
        }

        public bool DeleteUser(int UserID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteUser + " " + UserID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);

                if(Status)
                {
                    AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                    objAccessRightsDataAccess.DeleteUserAccessRights(UserID);
                    objAccessRightsDataAccess = null;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }

        public bool DeleteCompanyUserRole(int CompanyUserRoleID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteCompanyUserRole + " " + CompanyUserRoleID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }
        

        public bool DeleteCompany(int CompanyID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteCompany + " " + CompanyID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }

        public bool DeleteDivision(int DivisionID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteDivision + " " + DivisionID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }

        public bool DeleteDivisionProduct(int DivisionID, int CompanyID, Int64 PFCID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteDivisionProductMapping + " " + DivisionID +"," + CompanyID +","+ PFCID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }

        public bool DeleteBrandGroup(int BrandGroupID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteBrandGroup + " " + BrandGroupID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }

        public bool DeleteCluster(int ClusterID)
        {

            bool Status = false;
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteCluster + " " + ClusterID;
                var obj = IMSBIDB.ReturnValue(str);
                Status = Convert.ToBoolean(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Status;
        }





        public IList<CompanyUser> GetDivisionUsers(int CompanyID, int DivisionID)
        {

            IList<CompanyUser> CompanyDivisionUserList = new List<CompanyUser>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetDivisionUsers + " " + DivisionID + "," + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyUser
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                  Email = Convert.ToString(row["Email"]),
                                  MobileNo = Convert.ToInt32(row["MobileNo"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  Pincode = Convert.ToInt32(row["Pincode"]),
                                  CompanyRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  RoleName = Convert.ToString(row["RoleName"]),
                                  ReportingManagerID = Convert.ToInt32(row["ReportingManagerID"]),
                                  DivisionName = Convert.ToString(row["DivisionName"])


                              }
                              ).ToList();

                CompanyDivisionUserList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyDivisionUserList;
        }

        public IList<Permission> GetTeamPermission(int TeamID)
        {

            IList<Permission> TeamPermissionList = new List<Permission>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTeamPermissions + " " + TeamID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Permission
                              {
                                  PermissionID = Convert.ToInt32(row["PermissionID"]),
                                  PermissionName = Convert.ToString(row["PermissionName"]),
                                

                              }
                              ).ToList();

                TeamPermissionList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TeamPermissionList;
        }

        public IList<Permission> GetUserPermission(int UserID)
        {

            IList<Permission> UserPermissionList = new List<Permission>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetUserPermissions + " " + UserID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Permission
                              {
                                  PermissionID = Convert.ToInt32(row["PermissionID"]),
                                  PermissionName = Convert.ToString(row["PermissionName"]),


                              }
                              ).ToList();

                UserPermissionList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserPermissionList;
        }


        public IList<UserRole> GetUserRoles()
        {


            IList<UserRole> UserRoleList = new List<UserRole>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetUserRoles);
                var result = (from row in dt.AsEnumerable()
                              select new UserRole
                              {
                                  RoleID = Convert.ToInt32(row["UserRoleID"]),
                                  RoleName = Convert.ToString(row["RoleName"])
                              }
                              ).ToList();

                UserRoleList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserRoleList;
        }

        public IList<CompanyUserRole> GetUserRolesbyCompany(int CompanyID, int DivisionID)
        {


            IList<CompanyUserRole> UserRoleList = new List<CompanyUserRole>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetUserRolesByCompany + " " + CompanyID + "," + DivisionID;
                resultds = IMSBIDB.ReturnDataset(str);

              
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyUserRole
                              {
                                  CompanyUserRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  RoleName = Convert.ToString(row["RoleName"]),
                                  ParentRoleID = Convert.ToInt32(row["ParentRoleID"]),
                                  ParentRoleName = Convert.ToString(row["ParentRoleName"])
                              }
                              ).ToList();

                UserRoleList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserRoleList;
        }

        public CompanyUserRole GetCompanyUserRoleByRoleID(int CompanyUserRoleID)
        {


            CompanyUserRole UserRoleList = new CompanyUserRole();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyUserRoleByRoleID + " " + CompanyUserRoleID;
                resultds = IMSBIDB.ReturnDataset(str);


                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyUserRole
                              {
                                  CompanyUserRoleID = Convert.ToInt32(row["CompanyRoleID"]),
                                  RoleName = Convert.ToString(row["RoleName"]),
                                  ParentRoleID = Convert.ToInt32(row["ParentRoleID"]),
                                  ParentRoleName = Convert.ToString(row["ParentRoleName"]),
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  DivisionID = Convert.ToInt32(row["DivisionID"])
                              }
                              ).FirstOrDefault();

                var PermissionList = (from row in resultds.Tables[1].AsEnumerable()
                              select new Permission_IVM
                              {
                                  ApplicationID = Convert.ToInt32(row["PermissionID"]),
                                  Permission = Convert.ToBoolean(row["Status"])
                              }
                             ).ToList();

                result.permissions = new List<Permission_IVM>();
                result.permissions = PermissionList;

                UserRoleList = result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserRoleList;
        }

        public int AddUpdateUserTeam(Team_IVM objTeam)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {
                if (objTeam != null)
                {
                    param = objTeam.TeamID + ",";
                    param += objTeam.CompanyID + ",";
                    param += "'" + objTeam.TeamName + "'";
                    param += objTeam.TeamMemberCount + ",";
                    param += objTeam.TeamManagerID + ",";
                    param += objTeam.TeamSalesTarget + ",";
                    param += objTeam.Active;
                   
                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateTeam + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public IList<AccessLogReport_OVM> GetUserAccessLog(AccessLogReport_IVM objUserAccesslog)
        {
            IList<AccessLogReport_OVM> UserAccesslog = new List<AccessLogReport_OVM>();
            try
            {
                UserLogAccessDetails objUserAccesslogDetails = new UserLogAccessDetails();
                UserAccesslog = objUserAccesslogDetails.GetUserUserLog(objUserAccesslog);
                objUserAccesslogDetails = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserAccesslog;

        }


        public int AddUpdateUser(User_IVM objUser)
        {
            int result = 0;
          
            try
            {

                UserDataAccess objUserDataAccess = new UserDataAccess();
                result = objUserDataAccess.AddUpdateUser(objUser);
                objUserDataAccess = null;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public int AddUpdateCompanyUserRole(CompanyUserRole_IVM objCompanyUserRole)
        {
            int result = 0;

            try
            {

                UserDataAccess objUserDataAccess = new UserDataAccess();
                result = objUserDataAccess.AddUpdateCompanyUserRole(objCompanyUserRole);
                objUserDataAccess = null;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        

        public int InsertUpdateTeamUserAssocation(int UserID, int TeamID, int Active)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {
              
                    param = TeamID + ",";
                
                    param += UserID + ",";
                    param += Active;

                

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_InsertUpdateTeamUserAssocation + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public int InsertUpdateTeamPermissionMapping(int TeamID, int PermissionID, int Active)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {
                
                param = TeamID + ",";
                param = PermissionID + ",";
                param += Active;              

              

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_InsertUpdateTeamPermissionMapping + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public string ChangePassword(ChangePassword_IVM objUserInfo)
        {
            string ChangedPasswordStatus = string.Empty;
            try
            {
                UserDataAccess objUserDataAccess = new UserDataAccess();
                ChangedPasswordStatus = objUserDataAccess.ChangePassword(objUserInfo);
                objUserDataAccess = null;

            }
            catch
            {
                throw;
            }

            return ChangedPasswordStatus;
        }
    }
}
