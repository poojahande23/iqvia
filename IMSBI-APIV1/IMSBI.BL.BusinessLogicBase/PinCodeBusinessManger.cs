﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
   public class PinCodeBusinessManger
    {
        public List<Int32> GetPinCodeList(int PinCode)
        {
            List<Int32> PincodeList = new List<int>();
            try
            {
                PincodeDataAccess objPincodeDataAccess = new PincodeDataAccess();
                PincodeList = objPincodeDataAccess.GetPinCodeList(PinCode);
                objPincodeDataAccess = null;
            }
            catch
            {
                throw;
            }

            return PincodeList;

        }
        public IList<Int32> GetPinCodeListToCreatePatch(int CompanyID, Int32 CityID, int DivisionID)
        {
            IList<Int32> PincodeList = new List<int>();
            try
            {
                PincodeDataAccess objPincodeDataAccess = new PincodeDataAccess();
                PincodeList = objPincodeDataAccess.GetPinCodeListToCreatePatch(CompanyID, CityID, DivisionID);
                objPincodeDataAccess = null;
            }
            catch
            {
                throw;
            }

            return PincodeList;

        }

        
        public PinCityState_OVM GetCityStateByPinCode(int PinCode)
        {
            PinCityState_OVM  objPinCityState = new PinCityState_OVM();
            try
            {
                PincodeDataAccess objPincodeDataAccess = new PincodeDataAccess();
                objPinCityState = objPincodeDataAccess.GetCityStateByPinCode(PinCode);
                objPincodeDataAccess = null;
            }
            catch
            {
                throw;
            }

            return objPinCityState;

        }
    }
}
