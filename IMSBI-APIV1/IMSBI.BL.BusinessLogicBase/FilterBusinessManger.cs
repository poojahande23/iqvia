﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IMSBI.BL.BusinessLogicBase
{
    public class FilterBusinessManger
    {
        public IList<cityMaster> GetAllCities()        {

            
             IList<cityMaster> CityList = new List<cityMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetAllCity);
                var result = (from row in dt.AsEnumerable()
                              select new cityMaster
                              {
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  StateID = Convert.ToInt32(row["StateID"])

                              }
                              ).ToList();

                CityList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CityList;
        }

        public IList<cityMaster> GetCustomPatchCities()
        {


            IList<cityMaster> CityList = new List<cityMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetCustomPatchEnabledCity);
                var result = (from row in dt.AsEnumerable()
                              select new cityMaster
                              {
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  CityName = Convert.ToString(row["CityName"]),
                              

                              }
                              ).ToList();

                CityList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CityList;
        }

        
        public IList<cityMaster> GetAllCityList()
        {


            IList<cityMaster> CityList = new List<cityMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetAllCity);
                var result = (from row in dt.AsEnumerable()
                              select new cityMaster
                              {
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  StateID = Convert.ToInt32(row["StateID"])

                              }
                              ).ToList();

                CityList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CityList;
        }



        public IList<StateMaster> GetAllState()
        {


            IList<StateMaster> StateList = new List<StateMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetAllState);
                var result = (from row in dt.AsEnumerable()
                              select new StateMaster
                              {
                                  StateID = Convert.ToInt32(row["StateID"]),
                                  StateName = Convert.ToString(row["StateName"]),

                              }
                              ).ToList();

                StateList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return StateList;
        }


        public IList<TherapyMaster> GetMasterTherapyList()
        {


            IList<TherapyMaster> objtherapyList = new List<TherapyMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetTherapyMasterList);
                var result = (from row in dt.AsEnumerable()
                              select new TherapyMaster
                              {
                                  TherapyCode = Convert.ToString(row["TherapyCode"]),
                                  TherapyName = Convert.ToString(row["TherapyName"]),

                              }
                              ).ToList();

                objtherapyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objtherapyList;
        }

        public IList<ApplicationPermissionOVM> GetApplicationPermissionList()
        {


            IList<ApplicationPermissionOVM> objApplicationPermissionList = new List<ApplicationPermissionOVM>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetApplicationPermissionList);
                var result = (from row in dt.AsEnumerable()
                              select new ApplicationPermissionOVM
                              {
                                  PermissionID = Convert.ToInt16(row["PermissionID"]),
                                  PermissionName = Convert.ToString(row["PermissionName"])

                              }
                              ).ToList();

                objApplicationPermissionList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objApplicationPermissionList;
        }
        public IList<ApplicationDataAccessLevelOVM> GetApplicationDataAccessLevelList()
        {


            IList<ApplicationDataAccessLevelOVM> objApplicationDataAccessLevelList = new List<ApplicationDataAccessLevelOVM>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetApplicationDataAccessLevelList);
                var result = (from row in dt.AsEnumerable()
                              select new ApplicationDataAccessLevelOVM
                              {
                                  DataAccessLevelID = Convert.ToInt16(row["DataAccessLevelID"]),
                                  DataAccessLevel = Convert.ToString(row["DataAccessLevel"])

                              }
                              ).ToList();

                objApplicationDataAccessLevelList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objApplicationDataAccessLevelList;
        }



        public IList<TherapyMaster> GetTherapyListByTherapyCode( string TCCode, string SessionID)
        {
            IList<TherapyMaster> objtherapyList = new List<TherapyMaster>();
            Int32 UserID = 0;
            Int32 UserAccessType = 1;
            string TCValues = string.Empty;
            if (!string.IsNullOrEmpty(SessionID))
            {
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                UserID = objSessionDataAccess.ValidateSession(SessionID);
                objSessionDataAccess = null;
                if (UserID == 0)
                {
                    objtherapyList = null;
                    return objtherapyList;
                }

                //GetUserAccessRights 
                AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                UserAccessRights objUserAccessRights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
                if (objUserAccessRights != null && objUserAccessRights.AccessRightType != 1 && !string.IsNullOrEmpty(objUserAccessRights.TCValues))
                {
                  
                        UserAccessType = 2;
                        TCValues = objUserAccessRights.TCValues;
                    
                }
                else
                {
                    UserDataAccess objUserDataAccess = new UserDataAccess();
                    UserInfo objUserInfo = objUserDataAccess.UserInfoDetails(UserID);
                    objUserDataAccess = null;
                    if (objUserInfo != null)
                    {
                        CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objUserInfo.CompanyID);
                        if (objCompanyAccessRight != null)
                        {
                            switch (objCompanyAccessRight.AccessRightType)
                            {
                                case 2:
                                    UserAccessType = 2;
                                  
                                    IList<string> TCCodesList = this.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                                    TCValues = string.Join(",", TCCodesList);
                                    break;
                                case 3:
                                    UserAccessType = 2;
                                    TCValues = objCompanyAccessRight.TCValues;
                                    break;
                            }
                        }
                    }
                }

                objAccessRightsDataAccess = null;

                objSessionDataAccess = null;
            }
          


            
            try
            {
               
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTherapyListByCode + " '" + TCCode + "'," + UserAccessType + ",'" + TCValues + "'" ;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new TherapyMaster
                              {
                                  TherapyCode = Convert.ToString(row["TherapyCode"]),
                                  TherapyName = Convert.ToString(row["TherapyCode"]) + " "+ Convert.ToString(row["TherapyName"])

                              }
                              ).ToList();

                objtherapyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objtherapyList;
        }

        public IList<TherapyMaster> GetTherapyListByTherapyCodes(string TCCodes)
        {


            IList<TherapyMaster> objtherapyList = new List<TherapyMaster>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTherapyListByTherapyCodes + " '" + TCCodes + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new TherapyMaster
                              {
                                  TherapyCode = Convert.ToString(row["TherapyCode"]),
                                  TherapyName = Convert.ToString(row["TherapyCode"]) + " " + Convert.ToString(row["TherapyName"])

                              }
                              ).ToList();

                objtherapyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objtherapyList;
        }

        public IList<string> GetTCCodebySuperGroup(string Supergroups)
        {


            IList<string> objTcCode = new List<string>();
            try
            {

                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTCCodebySuperGroup + " '" + Supergroups + "'";
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select Convert.ToString(row["TherapyCode"])                            
                              ).ToList();

                objTcCode = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objTcCode;
        }

        



        public IList<BrickMaster> GetAllBricks()
        {


            IList<BrickMaster> BrickList = new List<BrickMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetAllBricks);
                var result = (from row in dt.AsEnumerable()
                              select new BrickMaster
                              {
                                  BrickID = Convert.ToInt32(row["BrickID"]),
                                  BrickName = Convert.ToString(row["BrinkName"]),

                              }
                              ).ToList();

                BrickList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return BrickList;
        }

        public IList<BrickMaster> GetPatchListByCityID(Int32 CityID)
        {
            IList<BrickMaster> objBrickMasterList = new List<BrickMaster>();
            try
            {
                FilterDataAccess objFilterDataAccess = new FilterDataAccess();
                objBrickMasterList = objFilterDataAccess.GetPatchList(CityID, "all");
                objFilterDataAccess = null;
            }
            catch
            {
                throw;
            }
            return objBrickMasterList;
        }


        public IList<BrickMaster> GetBricksByGeography(string FilterType, int GeographyID, int CompanyID, int DivisionID)
        {


            IList<BrickMaster> BrickList = new List<BrickMaster>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetBricksByCityID + " " + GeographyID +","+ CompanyID+","+ DivisionID;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrickMaster
                              {
                                  BrickID = Convert.ToInt32(row["BrickID"]),
                                  BrickName = Convert.ToString(row["BrickName"]),
                                  IsCluster = Convert.ToString(row["FilterType"]) == "cluster"? true : false

                              }
                              ).ToList();

                BrickList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return BrickList;
        }



        public IList<ZoneMaster> GetAllZone()
        {


            IList<ZoneMaster> ZoneList = new List<ZoneMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetZoneList);
                var result = (from row in dt.AsEnumerable()
                              select new ZoneMaster
                              {
                                  ZoneID = Convert.ToInt32(row["ZoneID"]),
                                  ZoneName = Convert.ToString(row["ZoneName"])

                              }
                              ).ToList();

                ZoneList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ZoneList;
        }



        public IList<TimePeriod> GetTimePeriodList( Int32 PeriodID, Int32 CompanyID, Int32 DivisionID)
        {


            IList<TimePeriod> TimePeriodList = new List<TimePeriod>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();

                SubscriptionPeriodTypeDataAccess objSubscriptionPeriodTypeDataAccess = new SubscriptionPeriodTypeDataAccess();
               IList<SubscriptionPeriodType> objSubcriptionList = objSubscriptionPeriodTypeDataAccess.GetSubscribedCompanyPeriods(PeriodID.ToString());
                objSubscriptionPeriodTypeDataAccess = null;

                // Get Company Data Subscription Type
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                DataSubscriptionType objdatasubscription = objCompanyDataAccess.GetCompanyDataSubscriptionDetails(CompanyID, DivisionID);
                objCompanyDataAccess = null;

                string DatasubscriptionType = "mth";
                if(objdatasubscription != null)
                {
                    DatasubscriptionType = objdatasubscription.DataSubscriptionTypeShortName;
                }
                if (objSubcriptionList != null)
                {
                    switch (objSubcriptionList[0].PeriodShortName.ToString().ToLower())
                    {
                        case "mth":
                            string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetTimePeriods + " '" + DatasubscriptionType + "'";
                            DataSet resultds = IMSBIDB.ReturnDataset(str);

                            var result = (from row in resultds.Tables[0].AsEnumerable()
                                          select new TimePeriod
                                          {
                                              TimePeriodID = Convert.ToInt32(row["TimePeriodID"]),
                                              MonthYear = Convert.ToString(row["Month"]) + " " + Convert.ToInt32(row["Year"])

                                          }
                                          ).ToList();

                            TimePeriodList = result;
                            break;
                        case "qtr":
                            string strquarter = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetQuarterPeriod +" '" + DatasubscriptionType + "'";
                            DataSet resultdsquarter = IMSBIDB.ReturnDataset(strquarter);

                            var resultquarter = (from row in resultdsquarter.Tables[0].AsEnumerable()
                                          select new TimePeriod
                                          {
                                              TimePeriodID = Convert.ToInt32(row["quarterPeriodID"]),
                                              MonthYear = Convert.ToString(row["QuarterPeriod"])
                                          }
                                          ).ToList();

                            TimePeriodList = resultquarter;
                            break;
                        case "hlf":
                            string strhalf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetHalfYearPeriod + " '" + DatasubscriptionType + "'";
                            DataSet resultdshalf = IMSBIDB.ReturnDataset(strhalf);

                            var resulthalf = (from row in resultdshalf.Tables[0].AsEnumerable()
                                                 select new TimePeriod
                                                 {
                                                     TimePeriodID = Convert.ToInt32(row["HalfYearPeriodID"]),
                                                     MonthYear = Convert.ToString(row["HalfYearPeriod"])
                                                 }
                                          ).ToList();

                            TimePeriodList = resulthalf;
                            break;
                        case "ytdc":
                            string strytdc = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCyearPeriod + " '" + DatasubscriptionType + "'";
                            DataSet resultdsytdc = IMSBIDB.ReturnDataset(strytdc);

                            var resultytdc = (from row in resultdsytdc.Tables[0].AsEnumerable()
                                              select new TimePeriod
                                              {
                                                  TimePeriodID = Convert.ToInt32(row["YearPeriodID"]),
                                                  MonthYear = Convert.ToString(row["YearPeriodName"])
                                              }
                                          ).ToList();

                            TimePeriodList = resultytdc;
                            break;
                        case "mat":
                            string strmat = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetMatPeriod + " '" + DatasubscriptionType + "'";
                            DataSet resultsdmat = IMSBIDB.ReturnDataset(strmat);

                            var resultmat = (from row in resultsdmat.Tables[0].AsEnumerable()
                                              select new TimePeriod
                                              {
                                                  TimePeriodID = Convert.ToInt32(row["MatPeriodID"]),
                                                  MonthYear = Convert.ToString(row["MatPeriodName"])
                                              }
                                          ).ToList();

                            TimePeriodList = resultmat;
                            break;
                        case "ytdf":
                            string strytdf = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetFyearPeriod + " '" + DatasubscriptionType + "'";
                            DataSet resultdsytdf = IMSBIDB.ReturnDataset(strytdf);

                            var resultytdf = (from row in resultdsytdf.Tables[0].AsEnumerable()
                                              select new TimePeriod
                                              {
                                                  TimePeriodID = Convert.ToInt32(row["YearPeriodID"]),
                                                  MonthYear = Convert.ToString(row["YearPeriodName"])
                                              }
                                          ).ToList();

                            TimePeriodList = resultytdf;
                            break;

                    }

              
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return TimePeriodList;
        }

        public IList<CategoryValues> GetCategoryValues(string categoryType, int CompanyID, int DivisonID, string SessionID)
        {


            IList<CategoryValues> CategoryList = new List<CategoryValues>();
            try
            {
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                Int32 UserID = 0;
                Int32 UserAccessType = 1;
                string TCValues = string.Empty;
                if (!string.IsNullOrEmpty(SessionID))
                {
                    UserID = objSessionDataAccess.ValidateSession(SessionID);
                    objSessionDataAccess = null;
                    if (UserID == 0)
                    {
                        CategoryList = null;
                        return CategoryList;
                    }

                    //GetUserAccessRights 
                    AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                    UserAccessRights objUserAccessRights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
                    if (objUserAccessRights != null && objUserAccessRights.AccessRightType != 1 && !string.IsNullOrEmpty(objUserAccessRights.TCValues))
                    {
                     
                            UserAccessType = 2;
                            TCValues = objUserAccessRights.TCValues;
                        
                    }
                    else
                    {

                        UserDataAccess objUserDataAccess = new UserDataAccess();
                        UserInfo objUserInfo = objUserDataAccess.UserInfoDetails(UserID);
                        objUserDataAccess = null;
                        if (objUserInfo != null)
                        {
                            CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objUserInfo.CompanyID);
                            if (objCompanyAccessRight != null)
                            {
                                switch (objCompanyAccessRight.AccessRightType)
                                {
                                    case 2:
                                        UserAccessType = 2;                                       
                                        IList<string> TCCodesList = this.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                                        TCValues = string.Join(",", TCCodesList);
                                        break;
                                    case 3:
                                        UserAccessType = 2;
                                        TCValues = objCompanyAccessRight.TCValues;
                                        break;
                                }
                            }
                        }
                    }

                    objAccessRightsDataAccess = null;



                }
                objSessionDataAccess = null;
                switch (categoryType)
                {
                    case "sku":
                        ProductDataAccess objProductDataAccess = new ProductDataAccess();
                        IList<Product> ProductList = objProductDataAccess.GetProductsByCompanyDivison(CompanyID, DivisonID, UserAccessType,TCValues);
                        if(ProductList != null)
                            {
                                for (int i = 0; i < ProductList.Count;i++)
                                {
                                    CategoryValues objNewCat = new CategoryValues()
                                    {
                                        CategoryID = ProductList[i].PFCID,
                                        CategoryValue = ProductList[i].ProductName
                                    };

                                    CategoryList.Add(objNewCat);
                                }
                          }
                        objProductDataAccess = null;
                        break;
                    case "brand":
                        BrandDataAccess objBrandDataAccess = new BrandDataAccess();
                        IList<BrandMaster> BrandList = objBrandDataAccess.GetBrandListByCompany(CompanyID, DivisonID, UserAccessType, TCValues);
                        if (BrandList != null)
                        {
                            for (int i = 0; i < BrandList.Count; i++)
                            {
                                CategoryValues objNewCat = new CategoryValues()
                                {
                                    CategoryID = BrandList[i].BrandID,
                                    CategoryValue = BrandList[i].BrandName
                                };

                                CategoryList.Add(objNewCat);
                            }
                        }
                        objBrandDataAccess = null;
                        break;
                        
                    case "brandgp":
                        DivisionDataAccess objBrandGroupAccess = new DivisionDataAccess();
                        IList<BrandGroup> BrandGroupList = objBrandGroupAccess.GetBrandGroupList(DivisonID,CompanyID);
                        if (BrandGroupList != null)
                        {
                            for (int i = 0; i < BrandGroupList.Count; i++)
                            {
                                CategoryValues objNewCat = new CategoryValues()
                                {
                                    CategoryID = BrandGroupList[i].BrandGroupID,
                                    CategoryValue = BrandGroupList[i].BrandGroupName
                                };

                                CategoryList.Add(objNewCat);
                            }
                        }
                        objBrandGroupAccess = null;
                        break;

                       
                    case "focusbr":
                        ProductDataAccess objProductDataAccessFocus = new ProductDataAccess();
                        IList<Product> ProductListFocus = objProductDataAccessFocus.DB_SP_GetFocusedProductsByCompanyDivison(CompanyID, DivisonID, UserAccessType, TCValues);
                        if (ProductListFocus != null)
                        {
                            for (int i = 0; i < ProductListFocus.Count; i++)
                            {
                                CategoryValues objNewCat = new CategoryValues()
                                {
                                    CategoryID = ProductListFocus[i].PFCID,
                                    CategoryValue = ProductListFocus[i].ProductName
                                };

                                CategoryList.Add(objNewCat);
                            }
                        }
                        objProductDataAccessFocus = null;
                        break;
                    case "newintro":
                        ProductDataAccess objProductDataAccessNew = new ProductDataAccess();
                        IList<Product> ProductListNew = objProductDataAccessNew.GetNewIntroductionProductsByCompanyDivison(CompanyID, DivisonID, UserAccessType, TCValues);
                        if (ProductListNew != null)
                        {
                            for (int i = 0; i < ProductListNew.Count; i++)
                            {
                                CategoryValues objNewCat = new CategoryValues()
                                {
                                    CategoryID = ProductListNew[i].PFCID,
                                    CategoryValue = ProductListNew[i].ProductName
                                };

                                CategoryList.Add(objNewCat);
                            }
                        }
                        objProductDataAccessNew = null;
                        break;
                  

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CategoryList;
        }

        public IList<GeographyValues> GetGeographyValues(string filtertype, int CompanyID, string SessionID)
        {


            IList<GeographyValues> GeographyList = new List<GeographyValues>();
            try
            {
                // Get user details
                  Int32 UserID = 0;
                Int32 UserAccessType = 1;
                string CityIDs = string.Empty;
                if (!string.IsNullOrEmpty(SessionID))
                {
                    SessionDataAccess objSessionDataAccess = new SessionDataAccess();

                    UserID = objSessionDataAccess.ValidateSession(SessionID);
                    objSessionDataAccess = null;
                    if (UserID == 0)
                    {
                        GeographyList = null;
                        return GeographyList;
                    }

                    //GetUserAccessRights 
                    AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                    UserAccessRights objUserAccessRights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
                    if (objUserAccessRights != null  && !string.IsNullOrEmpty(objUserAccessRights.CityIDs))
                    {
                        CityIDs = objUserAccessRights.CityIDs;
                    }
                     if (objUserAccessRights != null && objUserAccessRights.AccessRightType != 1 && !string.IsNullOrEmpty(objUserAccessRights.CityIDs))
                    {
                       
                            UserAccessType = 2;
                            CityIDs = objUserAccessRights.CityIDs;
                        
                    }
                    else
                    {
                        UserDataAccess objUserDataAccess = new UserDataAccess();
                        UserInfo objUserInfo = objUserDataAccess.UserInfoDetails(UserID);
                        objUserDataAccess = null;
                        if (objUserInfo != null)
                        {
                            CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objUserInfo.CompanyID);
                            if (objCompanyAccessRight != null)
                            {
                                if(objCompanyAccessRight.AccessRightType !=1)
                                {
                                    UserAccessType = 2;
                                    CityDataAccess objCityDataAccess = new CityDataAccess();
                                    IList<cityMaster> CityList = objCityDataAccess.GetCityListByCompany(CompanyID, 1, "");
                                    IList<Int32> CityIDS = new List<Int32>();
                                    for (int j=0; j < CityList.Count; j++)
                                    {
                                        CityIDS.Add(CityList[j].CityID);
                                    }
                                    CityIDs = string.Join(",", CityIDS);
                                    objCityDataAccess = null;
                                }
                               
                            }
                        }
                    }

                    objAccessRightsDataAccess = null;



                    objSessionDataAccess = null;
                }
               
                switch (filtertype)
                {
                    case "zone":
                        ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
                        IList<ZoneMaster> ZoneList = objZoneDataAccess.GetZoneListByCompany(CompanyID);
                        if (ZoneList != null)
                        {
                            for (int j = 0; j < ZoneList.Count; j++)
                            {
                                GeographyValues NewEntry = new GeographyValues()
                                {
                                    GeographyID = ZoneList[j].ZoneID,
                                    GeographyValue = ZoneList[j].ZoneName
                                };
                                GeographyList.Add(NewEntry);
                            }

                        }
                        objZoneDataAccess = null;
                        break;
                    case "state":
                        StateDataAccess objStateDataAccess = new StateDataAccess();
                        IList<StateMaster> StateList = objStateDataAccess.GetStateListByCompany(CompanyID);
                        if (StateList != null)
                        {
                            for (int k = 0; k < StateList.Count; k++)
                            {
                                GeographyValues NewEntry = new GeographyValues()
                                {
                                    GeographyID = StateList[k].StateID,
                                    GeographyValue = StateList[k].StateName
                                };
                                GeographyList.Add(NewEntry);
                            }

                        }
                        objStateDataAccess = null;
                        break;
                    case "city":


                        CityDataAccess objCityDataAccess = new CityDataAccess();
                        IList<cityMaster> CityList = objCityDataAccess.GetCityListByCompany(CompanyID, UserAccessType, CityIDs);
                        if(CityList !=null)
                        {
                            for(int i=0; i< CityList.Count; i++ )
                            {
                                GeographyValues NewEntry = new GeographyValues()
                                {
                                    GeographyID = CityList[i].CityID,
                                    GeographyValue = CityList[i].CityName
                                };
                                GeographyList.Add(NewEntry);
                            }

                        }
                        objCityDataAccess = null;
                        break;
                 
                  


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return GeographyList;
        }


      
        public IList<ComparisonValues> GetComparisionTypeValue(string filtertype,  string SessionID, string TCcode ="" ,string TherapyID ="")
        {


            IList<ComparisonValues> ComparisonList = new List<ComparisonValues>();
            try
            {
                // Get user details
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                Int32 UserID = 0;
                Int32 UserAccessType = 1;
                string TCValues = string.Empty;
                if (!string.IsNullOrEmpty(SessionID))
                {
                    UserID = objSessionDataAccess.ValidateSession(SessionID);
                    objSessionDataAccess = null;
                    if (UserID == 0)
                    {
                        ComparisonList = null;
                        return ComparisonList;
                    }

                    //GetUserAccessRights 
                    AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                    UserAccessRights objUserAccessRights = objAccessRightsDataAccess.GetUserAccessRights(UserID);
                    if (objUserAccessRights != null && objUserAccessRights.AccessRightType != 1 && !string.IsNullOrEmpty(objUserAccessRights.TCValues))
                    {
                       
                            UserAccessType = 2;
                            TCValues = objUserAccessRights.TCValues;
                        
                    }
                    else
                    {
                        UserDataAccess objUserDataAccess = new UserDataAccess();
                        UserInfo objUserInfo = objUserDataAccess.UserInfoDetails(UserID);
                        objUserDataAccess = null;
                        if (objUserInfo != null)
                        {
                            CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objUserInfo.CompanyID);
                            if (objCompanyAccessRight != null)
                            {
                                switch (objCompanyAccessRight.AccessRightType)
                                {
                                    case 2:
                                        UserAccessType = 2;

                                        IList<string> TCCodesList = this.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                                        TCValues = string.Join(",", TCCodesList);
                                        break;
                                    case 3:
                                        UserAccessType = 2;
                                        TCValues = objCompanyAccessRight.TCValues;
                                        break;
                                }
                            }
                        }
                    }

                    objAccessRightsDataAccess = null;


                }
                objSessionDataAccess = null;

                switch (filtertype)
                {
                    case "brand":
                        BrandDataAccess objBrandDataAccess = new BrandDataAccess();
                        IList<BrandMaster> BrandList = new List<BrandMaster>();
                        if (!string.IsNullOrEmpty(TCcode) && !string.IsNullOrEmpty(TherapyID))
                        {
                            BrandList = objBrandDataAccess.GetBrandListByTherapy(TCcode, TherapyID, TCValues, UserAccessType);
                        }
                        else
                        {
                           
                            BrandList = objBrandDataAccess.GetBrandList();
                        }
                        if (BrandList != null)
                        {
                            for (int j = 0; j < BrandList.Count; j++)
                            {
                                ComparisonValues NewEntry = new ComparisonValues()
                                {
                                    ComparisonID = BrandList[j].BrandID,
                                    ComparisonValue = BrandList[j].BrandName
                                };
                                ComparisonList.Add(NewEntry);
                            }

                        }
                        objBrandDataAccess = null;
                        break;
                    case "sku":
                        ProductDataAccess objProductDataAccess = new ProductDataAccess();
                        IList<BrandMaster> ProductList = new List<BrandMaster>();
                        if (!string.IsNullOrEmpty(TCcode) && !string.IsNullOrEmpty(TherapyID))
                        {
                            ProductList = objProductDataAccess.GetProductListByTherapy(TCcode, TherapyID, TCValues, UserAccessType);
                        }
                        else
                        {

                           
                        }
                        if (ProductList != null)
                        {
                            for (int j = 0; j < ProductList.Count; j++)
                            {
                                ComparisonValues NewEntry = new ComparisonValues()
                                {
                                    ComparisonID = ProductList[j].BrandID,
                                    ComparisonValue = ProductList[j].BrandName
                                };
                                ComparisonList.Add(NewEntry);
                            }

                        }
                        objBrandDataAccess = null;
                        break;
                    case "company":
                        CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                        IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
                        if (!string.IsNullOrEmpty(TCcode) && !string.IsNullOrEmpty(TherapyID))
                        {
                            CompanyList = objCompanyDataAccess.GetCompanyListByTCCode(TCcode, TherapyID, UserAccessType , TCValues);
                        }
                        else
                        {

                            CompanyList = objCompanyDataAccess.GetCompanyList(UserAccessType, TCValues);
                        }
                       
                        if (CompanyList != null)
                        {
                            for (int k = 0; k < CompanyList.Count; k++)
                            {
                                ComparisonValues NewEntry = new ComparisonValues()
                                {
                                    ComparisonID = CompanyList[k].CompanyID,
                                    ComparisonValue = CompanyList[k].CompanyName
                                };
                                ComparisonList.Add(NewEntry);
                            }

                        }
                        objCompanyDataAccess = null;
                        break;
                    case "manuf":
                        CompanyDataAccess objManufDataAccess = new CompanyDataAccess();
                        IList<CompanyMaster> ManufList = new List<CompanyMaster>();
                     
                        if (!string.IsNullOrEmpty(TCcode) && !string.IsNullOrEmpty(TherapyID))
                        {
                            ManufList = objManufDataAccess.GetManufListByTCCode(TCcode, TherapyID, UserAccessType, TCValues);
                        }
                        else
                        {

                            ManufList = objManufDataAccess.GetManufList(UserAccessType, TCValues);
                        }
                        if (ManufList != null)
                        {
                            for (int k = 0; k < ManufList.Count; k++)
                            {
                                ComparisonValues NewEntry = new ComparisonValues()
                                {
                                    ComparisonID = ManufList[k].CompanyID,
                                    ComparisonValue = ManufList[k].CompanyName
                                };
                                ComparisonList.Add(NewEntry);
                            }

                        }
                        objCompanyDataAccess = null;
                        break;





                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ComparisonList;
        }


        public IList<SubscriptionPeriodDetails> GetSubscriptionPeriodType(string SessionID)
        {


            IList<SubscriptionPeriodDetails> SubscriptionPeriodType = new List<SubscriptionPeriodDetails>();
            try
            {
                // Get user details
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                Int32 UserID = 0;
                Int32 UserAccessType = 1;
                string TCValues = string.Empty;
                if (!string.IsNullOrEmpty(SessionID))
                {
                    UserID = objSessionDataAccess.ValidateSession(SessionID);
                    objSessionDataAccess = null;
                    Int32 SubscriobedCompanyID = 0;
                    Int32 SubscribedDivisionID = 0;
                    UserDataAccess objUserDataAccess = new UserDataAccess();
                    UserInfo objUserInfo = objUserDataAccess.UserInfoDetails(UserID);
                    objUserDataAccess = null;
                    if (objUserInfo != null)
                    {
                        SubscriobedCompanyID = objUserInfo.CompanyID;
                        SubscribedDivisionID = objUserInfo.DivisionID;
                    }
                    CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                    CompanySubscripedPeriodType objCompanySubscriped = new CompanySubscripedPeriodType();
                    CompanySubscripedPeriodType objDivisionSubscriped = objCompanyDataAccess.GetDivisonSubscribedPeridoType(SubscribedDivisionID);
                    if (objDivisionSubscriped != null && !string.IsNullOrEmpty(objDivisionSubscriped.SubscribedPeriodType))
                    {
                        objCompanySubscriped.SubscribedPeriodType = objDivisionSubscriped.SubscribedPeriodType;
                        objCompanySubscriped.DefaultSubscriptionType = objDivisionSubscriped.DefaultSubscriptionType;
                    }
                    else
                    {
                        objCompanySubscriped = objCompanyDataAccess.GetCompanySubscribedPeridoType(SubscriobedCompanyID);
                    }
                    objCompanyDataAccess = null;
                    if (objCompanySubscriped != null && !string.IsNullOrEmpty(objCompanySubscriped.SubscribedPeriodType))
                    {
                        SubscriptionPeriodTypeDataAccess objSubscribedPeriodDataAccess = new SubscriptionPeriodTypeDataAccess();
                        IList<SubscriptionPeriodType> objSubscribedPeriodType = objSubscribedPeriodDataAccess.GetSubscribedCompanyPeriods(objCompanySubscriped.SubscribedPeriodType);
                        objSubscribedPeriodDataAccess = null;
                        if(objSubscribedPeriodType != null && objSubscribedPeriodType.Count > 0)
                        {
                            for(Int16 i =0; i< objSubscribedPeriodType.Count; i++)
                            {
                                SubscriptionPeriodDetails objSubscriptionType = new ViewModel.SubscriptionPeriodDetails();
                                objSubscriptionType.SubscriptionPeriodTypeID = objSubscribedPeriodType[i].SubscriptionPeriodTypeID;
                                objSubscriptionType.SubscriptionPeriodTypeName = objSubscribedPeriodType[i].SubscriptionPeriodTypeName;
                                if(objSubscribedPeriodType[i].SubscriptionPeriodTypeID == objCompanySubscriped.DefaultSubscriptionType)
                                {
                                    objSubscriptionType.isDefault = true;
                                }
                                objSubscriptionType.PeriodShortName = objSubscribedPeriodType[i].PeriodShortName;
                                SubscriptionPeriodType.Add(objSubscriptionType);
                            }
                        }
                    }

                }
                objSessionDataAccess = null;

               



                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return SubscriptionPeriodType;
        }

        public string GetPatchIDsByClusterIDs(string ClusterIDs)
        {
            string PatchIDs = string.Empty;
            try
            {
                DataSet resultIDs;
                DALUtility IMSDB = new DALUtility();
                string sqlString = "Exec " + IMSBI.Common.Constants.DB_sp_GetPatchIDsbyClusterIDs + " '" + ClusterIDs + "'";
                resultIDs = IMSDB.ReturnDataset(sqlString);
                var result = (from row in resultIDs.Tables[0].AsEnumerable()
                              select Convert.ToString(row["PatchIDs"])
                              ).ToList();

                IList<string> CurrentPatchIDs = new List<string>();
                for(int i=0; i< result.Count; i++)
                {
                    CurrentPatchIDs.Add(result[i]);
                }
                PatchIDs = String.Join(",", CurrentPatchIDs);
            }
            catch
            {
                throw;
            }

            return PatchIDs;
        }

        public IList<TherapyMaster> GetSupergroupList()
        {
            IList<TherapyMaster> objSuperGroupList = new List<TherapyMaster>();
            try
            {
                DataSet resultIDs;
                DALUtility IMSDB = new DALUtility();
                string sqlString = "Exec " + IMSBI.Common.Constants.DB_SP_GetSuperGroupList;
                resultIDs = IMSDB.ReturnDataset(sqlString);
                var result = (from row in resultIDs.Tables[0].AsEnumerable()
                              select new TherapyMaster
                              {
                                  TherapyCode = Convert.ToString(row["Supergroupname"]),
                                  TherapyName = Convert.ToString(row["Supergroupname"])

                              }


                             
                              ).ToList();


                objSuperGroupList = result;
            }
            catch
            {
                throw;
            }

            return objSuperGroupList;



        }


        public IList<SubscriptionPeriodType> GetSubscriptionPeriodType()
        {
            IList<SubscriptionPeriodType> objSubscriptionPeriodTypeList = new List<SubscriptionPeriodType>();
            try
            {
                SubscriptionPeriodTypeDataAccess objSubscriptionPeriodTypeDataAccess = new SubscriptionPeriodTypeDataAccess();
                objSubscriptionPeriodTypeList = objSubscriptionPeriodTypeDataAccess.GetSubscriptionPeriodType();
                objSubscriptionPeriodTypeDataAccess = null;
            }
            catch
            {
                throw;
            }

            return objSubscriptionPeriodTypeList;
        }

        public IList<SubscriptionPeriodType> GetCompanySubscriptionPeriodType(int CompanyID)
        {
            IList<SubscriptionPeriodType> objSubscriptionPeriodTypeList = new List<SubscriptionPeriodType>();
            try
            {
                SubscriptionPeriodTypeDataAccess objSubscriptionPeriodTypeDataAccess = new SubscriptionPeriodTypeDataAccess();
                objSubscriptionPeriodTypeList = objSubscriptionPeriodTypeDataAccess.GetCompanySubscriptionPeriodType(CompanyID);
                objSubscriptionPeriodTypeDataAccess = null;
            }
            catch
            {
                throw;
            }

            return objSubscriptionPeriodTypeList;
        }


        public IList<ProductData> GetProductsByTherapyCode(string TherapyCode)
        {
            IList<ProductData> ProductList = new List<ProductData>();
            try
            {
                TherapyDataAccess objThearpyDataAccess = new TherapyDataAccess();
                ProductList = objThearpyDataAccess.GetProductsByTherapyCode(TherapyCode);
                objThearpyDataAccess = null;
            }
            catch
            {
                throw;
            }
            return ProductList;
        }

        public IList<ProductData> GetProductsByBrand(string BrandIDs)
        {
            IList<ProductData> ProductList = new List<ProductData>();
            try
            {
                ProductDataAccess objproductdataaccess = new ProductDataAccess();
                ProductList = objproductdataaccess.GetProductsByBrand(BrandIDs);
                objproductdataaccess = null;
            }
            catch
            {
                throw;
            }
            return ProductList;
        }

        public IList<DataSubscriptionType> GetDataSubscriptionPeriodType()
        {
            IList<DataSubscriptionType> SubscriptionPeriodList = new List<DataSubscriptionType>();
            try
            {
                FilterDataAccess objFilterdataaccess = new FilterDataAccess();
                SubscriptionPeriodList = objFilterdataaccess.GetDataSubscriptionPeriodType();
                objFilterdataaccess = null;
            }
            catch
            {
                throw;
            }
            return SubscriptionPeriodList;
        }

        public IList<DataSubscriptionType> GetDataSubscriptionPeriodTypeByCompany(int CompanyID)
        {
            IList<DataSubscriptionType> SubscriptionPeriodList = new List<DataSubscriptionType>();
            try
            {
                FilterDataAccess objFilterdataaccess = new FilterDataAccess();
                SubscriptionPeriodList = objFilterdataaccess.GetDataSubscriptionPeriodTypeByCompany(CompanyID);
                objFilterdataaccess = null;
            }
            catch
            {
                throw;
            }
            return SubscriptionPeriodList;
        }

        public Int32 CreateUpdateCustomPatch(BrickMaster objBrickMaster)
        {
            Int32 BrickID = 0;
            try
            {
                BrickDataAccess objBrickDataAccess = new BrickDataAccess();
                BrickID = objBrickDataAccess.CreateUpdateCustomPatch(objBrickMaster);
                objBrickDataAccess = null;
            }
            catch
            {
                throw;
            }

            return BrickID;
        }

    
        public IList<PincodeCoordinate_OVM> GetPinCodeCoordinate(string PatchIDs, Int32 CityID, Int32 CompanyID, Int32 DivisionID)
        {
            IList<PincodeCoordinate_OVM> objPincodeCoordinateOVM = new List<PincodeCoordinate_OVM>();
            try
            {
                FilterDataAccess objFilterDataAccess = new FilterDataAccess();
                IList<pincoderawdata> objPincoderawData = objFilterDataAccess.GetPincodeCoordinate(CityID, CompanyID, DivisionID, PatchIDs);
                objFilterDataAccess = null;
                if(objPincoderawData !=null)
                {
                  //  IList<BrickMaster> objBrickMaster = objPincoderawData.Select(a => new BrickMaster { BrickID = a.BrickID, BrickName = a.BrickName }).Distinct().ToList();

                    var objBrickMasterresult = from r in objPincoderawData
                                         group r by new
                                             {
                                                 r.BrickID,
                                                 r.BrickName
                                             } into g
                                             select new BrickMaster
                                             {
                                                 BrickID = g.Key.BrickID,
                                                 BrickName = g.Key.BrickName,
                                                
                                             };
                    IList<BrickMaster> objBrickMaster = objBrickMasterresult.OrderByDescending(a => a.BrickID).ToList();
                    if (objBrickMaster != null && objBrickMaster.Count > 0)
                    {
                        for(Int32 i=0; i< objBrickMaster.Count; i++)
                        {
                            IList<pincoderawdata> objIndBrickData = objPincoderawData.Where(a => a.BrickID == objBrickMaster[i].BrickID).ToList();
                            PincodeCoordinate_OVM objPincodeCordinate = new PincodeCoordinate_OVM();
                            double lowestx = objIndBrickData.OrderBy(x => x.Lat).Select(x => x.Lat).Take(1).FirstOrDefault();
                            double lowesty = objIndBrickData.OrderBy(x => x.Long).Select(x => x.Long).Take(1).FirstOrDefault();
                            double highestx = objIndBrickData.OrderByDescending(x => x.Lat).Select(x => x.Lat).Take(1).FirstOrDefault();
                            double highesty = objIndBrickData.OrderByDescending(x => x.Long).Select(x => x.Long).Take(1).FirstOrDefault();
                            objPincodeCordinate.Lat = lowestx + ((highestx - lowestx) / 2);
                            objPincodeCordinate.lon = lowesty + ((highesty - lowesty) / 2);
                            objPincodeCordinate.pincodedatadetails = new List<pincodedata>();
                            objPincodeCordinate.PatchID = objBrickMaster[i].BrickID;
                            objPincodeCordinate.PatchName = objBrickMaster[i].BrickName;
                            IList<Int32> PincodeList = objIndBrickData.Select(a => a.Pincode).Distinct().ToList();
                            for(int j=0; j< PincodeList.Count; j++)
                            {
                                IList<pincoderawdata> objInPinCodeData = objIndBrickData.Where(a => a.Pincode == PincodeList[j]).ToList();
                               
                                 ArrayList objLatLong = new ArrayList();
                                foreach (pincoderawdata objdata in objInPinCodeData)
                                {
                                    ArrayList obj = new ArrayList()
                                    {
                                        objdata.Lat,
                                        objdata.Long
                                    };
                                    objLatLong.Add(obj);

                                }
                                pincodedata objpincodedata = new pincodedata()
                                {
                                    PincodeID = PincodeList[j],
                                    latlong = objLatLong
                                };

                                objPincodeCordinate.pincodedatadetails.Add(objpincodedata);
                            }
                            objPincodeCordinate.ColorCode = this.GetColorCode();
                            objPincodeCoordinateOVM.Add(objPincodeCordinate);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return objPincodeCoordinateOVM;
        }

        private string GetColorCode()
        {
            string CurrentColorCode = string.Empty;

                CurrentColorCode = "#" + RandomString(6);
            
            return CurrentColorCode;
        }

        private static Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "ABCDEF0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
    