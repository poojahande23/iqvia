﻿using IMSBI.Common;
using IMSBI.DAL;
using IMSBI.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
    public class CompanyBusinessManager
    {
        public IList<Manufacturer> GetCompanyManfList(int CompanyID)
        {
            IList<Manufacturer> ManuFList = new List<Manufacturer>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CompanyManfList + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new Manufacturer
                              {
                                  ManufacturerCode = Convert.ToInt32(row["Manufac_Code"]),
                                  ManufacturerName = Convert.ToString(row["Manufact_desc"]),

                              }
                              ).ToList();

                ManuFList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ManuFList;
        }

        public IList<CompanyDivision> GetCompanyDivisions(int CompanyID)
        {
            IList<CompanyDivision> CompanyDivisionList = new List<CompanyDivision>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyDivisions + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyDivision
                              {
                                  DivisionID = Convert.ToInt32(row["DivisionID"]),
                                  DivisionName = Convert.ToString(row["DivisionName"]),
                                  DataSubscriptionPeriodName = Convert.ToString(row["SubscriptionPeriodName"])


                              }
                              ).ToList();

                CompanyDivisionList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyDivisionList;
        }

        public IList<MenuMaster> GetApplicationMenuList()
        {
            IList<MenuMaster> objMenuMaster = new List<MenuMaster>();

            CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
            objMenuMaster = objCompanyDataAccess.GetApplicationMenuMaster();
            objCompanyDataAccess = null;

            return objMenuMaster;
        }
        public IList<MenuMaster> GetCompanyApplicationMenuList(Int32 CompanyID)
        {
            IList<MenuMaster> objMenuMaster = new List<MenuMaster>();

            CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
            objMenuMaster = objCompanyDataAccess.GetCompanyApplicationMenuMaster(CompanyID);
            objCompanyDataAccess = null;

            return objMenuMaster;
        }
        
        public IList<CompanyMaster> GetCompanyList()
        {


            IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetCompanyList);
                var result = (from row in dt.AsEnumerable()
                              select new CompanyMaster
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  Indian_MNC = Convert.ToString(row["Indian_MNC"]),

                              }
                              ).ToList();

                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }

        public IList<SubscriptionTypeCounter> GetCompanyCountBasedOnSubscription()
        {


            IList<SubscriptionTypeCounter> SubscriptionTypeCounterList = new List<SubscriptionTypeCounter>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetSubscriptionCompanyCount);
                var result = (from row in dt.AsEnumerable()
                              select new SubscriptionTypeCounter
                              {
                                  SubscriptionName = Convert.ToString(row["SubscriptionName"]),
                                  MemberCount = Convert.ToInt32(row["MemberCount"]),
                                  ColorCode = Convert.ToString(row["ColorCode"]),


                              }
                              ).ToList();

                SubscriptionTypeCounterList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return SubscriptionTypeCounterList;
        }

        public IList<SubscriptionType> GetCompanySubscriptionType()
        {


            IList<SubscriptionType> SubscriptionTypeList = new List<SubscriptionType>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetCompanySubscriptionType);
                var result = (from row in dt.AsEnumerable()
                              select new SubscriptionType
                              {
                                  SubscriptionTypeID = Convert.ToInt32(row["SubscriptionTypeID"]),
                                  SubscriptionName = Convert.ToString(row["SubscriptionName"]),
                                  ColorCode = Convert.ToString(row["ColorCode"])


                              }
                              ).ToList();

                SubscriptionTypeList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return SubscriptionTypeList;
        }


        public IList<ReportingMangerDetails> GetCompanyReportingManagerBasedOnRoleType(int CompanyRoleID, int CompanyID, int DivisionID)
        {


            IList<ReportingMangerDetails> ReportingManagerList = new List<ReportingMangerDetails>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyReportingManagerBasedOnRoleType + " " + CompanyRoleID + "," + CompanyID + "," + DivisionID;
                resultds = IMSBIDB.ReturnDataset(str);
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new ReportingMangerDetails
                              {
                                  UserID = Convert.ToInt32(row["UserID"]),
                                  UserName = Convert.ToString(row["UserName"]),
                                 // DataAccessLevelID = Convert.ToInt16(row["DataAccessLevelID"])


                              }
                              ).ToList();

                ReportingManagerList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ReportingManagerList;
        }


        public IList<CompanyMaster> GetUnsubscribedCompanyList()
        {


            IList<CompanyMaster> CompanyList = new List<CompanyMaster>();
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetUnsubscribedCompanyList);
                var result = (from row in dt.AsEnumerable()
                              select new CompanyMaster
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  Indian_MNC = Convert.ToString(row["Indian_MNC"]),

                              }
                              ).ToList();

                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }


        public SubscribedCompanyInfo GetSubscribedCompanyInfo(Int32 CompanyID)
        {


            SubscribedCompanyInfo CompanyInfo = new SubscribedCompanyInfo();
            try
            {
                IMSBI.DAL.CityDataAccess objCityDataAccess = new CityDataAccess();
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSubscribedCompanyInfo + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);
                IMSBIDB = null;
                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SubscribedCompanyInfo
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  NoOfDivision = Convert.ToInt32(row["Division"]),
                                  TeamStrength = Convert.ToInt32(row["TeamStrength"]),
                                  SubscriptionLocationID = Convert.ToString(row["SubscriptionLocationID"]),
                                  Action = Convert.ToInt32(row["Action"]),
                                  Address = Convert.ToString(row["Address"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  StateID = Convert.ToInt32(row["StateID"]),
                                  Pincode = Convert.ToInt32(row["Pincode"]),
                                  Country = Convert.ToString(row["Country"]),
                                  SubscriptionPeriodTypeIDs = Convert.ToString(row["SubscriptionPeriodTypeIDs"]),
                                  DefaultPeriodID = !string.IsNullOrEmpty(Convert.ToString(row["DefaultPeriodTypeID"])) ? Convert.ToInt32(row["DefaultPeriodTypeID"]) : 0,
                                  SubscriptionTypeID = Convert.ToInt32(row["SubscriptionTypeID"]),
                                  LandMark = Convert.ToString(row["LandMark"]),
                                  CityName = Convert.ToString(row["CityName"]),
                                  ColorCode = Convert.ToString(row["ColorCode"]),
                                  ContactInfo = new IVM_ContactInfo
                                  {
                                      name = Convert.ToString(row["ContactPersonName"]),
                                      Address = Convert.ToString(row["ContactPersonAddress"]),
                                      email = Convert.ToString(row["ContactPersonEmail"]),
                                      Phone = row["ContactPersonPhone"] == null || Convert.ToString(row["ContactPersonPhone"]).Trim() == string.Empty ? 0 : Convert.ToDouble(row["ContactPersonPhone"]),
                                  },
                                  ProfileImage = Convert.ToString(row["LogoImageName"]),
                                  SubscriptionDate = Convert.ToString(row["SubscriptionDate"]),
                                  SubscriptionEndDate = Convert.ToDateTime(row["SubscriptionEndDate"]),
                                  SubscriptionStartDate = Convert.ToDateTime(row["SubscriptionStartDate"]),
                                  DataSubscriptionTypeID = Convert.ToInt32(row["DataSubscriptionPeriodTypeID"]),
                                  





                              }
                              ).FirstOrDefault();

                objCityDataAccess = null;
                CompanyInfo = result;

                // Get The company Access right
                AccessRightsDataAccess objAccessRights = new AccessRightsDataAccess();
                CompanyInfo.CompanyAccessRights = objAccessRights.GetCompanyAccessRights(CompanyID);
                objAccessRights = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyInfo;
        }

        public IList<SubscribedCompany_OVM> GetSubscribedCompanyList()
        {


            IList<SubscribedCompany_OVM> CompanyList = new List<SubscribedCompany_OVM>();
            try
            {
                IMSBI.DAL.CityDataAccess objCityDataAccess = new CityDataAccess();
                DALUtility IMSBIDB = new DALUtility();
                DataTable dt = IMSBIDB.ReturnDataTable(IMSBI.Common.Constants.DB_SP_GetSubscribedCompanyList);
                var result = (from row in dt.AsEnumerable()
                              select new SubscribedCompany_OVM
                              {
                                  CompanyID = Convert.ToInt32(row["CompanyID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  SubscribedCompanyID = Convert.ToInt32(row["SubscribedCompanyID"]),
                                  Division = Convert.ToInt32(row["Division"]),
                                  TeamStrength = Convert.ToInt32(row["TeamStrength"]),
                                  SubscriptionName = Convert.ToString(row["SubscriptionName"]),
                                  SubscriptionLocationID = Convert.ToString(row["SubscriptionLocationID"]),
                                  Action = Convert.ToInt32(row["Action"]),
                                  ColorCode = Convert.ToString(row["ColorCode"]),
                                  LocationID = Convert.ToInt32(row["Location"]),
                                  LocationName = Convert.ToString(row["CityName"]),
                                  DaysRemaning = Convert.ToInt32(row["DaysRemaning"]),
                                  AdminUserID = Convert.ToInt32(row["AdminUserID"]),
                                  ActiveUsers = Convert.ToInt32(row["ActiveUsers"]),
                                  LogoFileName = Convert.ToString(row["LogoFileName"]),
                                  
                                  ContactPersonEmail = Convert.ToString(row["ContactPersonEmail"]),
                                  ContactPersonPhone = row["ContactPersonPhone"] == null || Convert.ToString(row["ContactPersonPhone"]).Trim() == string.Empty ? 0 : Convert.ToDouble(row["ContactPersonPhone"]),
                                  ContactPersonName = Convert.ToString(row["ContactPersonName"]),



                                  SubscriptionLocation = objCityDataAccess.GetCityNameByCityIds(Convert.ToString(row["SubscriptionLocationID"]))


                              }
                              ).ToList();

                objCityDataAccess = null;
                CompanyList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyList;
        }

        public IList<CompanyProductList_OVM> GetCompanyProductList(Int32 CompanyID)
        {


            IList<CompanyProductList_OVM> CompanyProductList = new List<CompanyProductList_OVM>();
            try
            {
                IMSBI.DAL.CityDataAccess objCityDataAccess = new CityDataAccess();
                DALUtility IMSBIDB = new DALUtility();
                DataSet resultds;
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetCompanyUnMappedProduct + " " + CompanyID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyProductList_OVM
                              {
                                  PFCID = Convert.ToInt64(row["PFC_ID"]),
                                  ProductName = Convert.ToString(row["ProductName"])


                              }
                              ).ToList();

                objCityDataAccess = null;
                CompanyProductList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CompanyProductList;
        }



        public ComparsionPerformanceDetails GetCompanyPerformance(Comparison_IVM objComparision)
        {


            ComparsionPerformanceDetails objComparsionPerformance = new ComparsionPerformanceDetails();
            // Validate the session 
            SessionDataAccess objSessionDataAccess = new SessionDataAccess();
            int UserID = objSessionDataAccess.ValidateSession(objComparision.SessionID);
            objSessionDataAccess = null;
            if (UserID == 0)
            {
                objComparsionPerformance = null;
                return objComparsionPerformance;
            }

            // Access log
            UserLog_VM objLogDetails = new UserLog_VM()
            {
                UserID = UserID,
                MenuID = 4,
               
                CityID = objComparision.GeographyID,
                CategoryType = objComparision.ComparisonType,
                CategoryValue = objComparision.ComparisionValue,
                DataType = objComparision.DataType,
                EndPeriod = objComparision.PeriodEnd,
                StartPeriod = objComparision.PeriodStart,
                PatchID = objComparision.PatchIds,
                PeriodType = objComparision.PeriodType,
                TCCode = objComparision.TCCode,
                TCLevel = objComparision.TCLevel,
                ProductID = objComparision.SKUIDs,
                SessionID = objComparision.SessionID
               


            };
            UserLogAccessDetails objUserLogAccessDetails = new UserLogAccessDetails();
            objUserLogAccessDetails.AddUpdateUserLog(objLogDetails);
            objUserLogAccessDetails = null;
            // Get UserAccessRights 

            AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
            UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

            int AccessType = 1;
            string TCCodes = string.Empty;
            if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
            {

                AccessType = 2;
                TCCodes = objUserAccessrights.TCValues;

            }
            else
            {
                CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objComparision.CompanyID);
                if (objCompanyAccessRight != null)
                {
                    switch (objCompanyAccessRight.AccessRightType)
                    {
                        case 2:
                            AccessType = 2;
                            FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                            IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                            TCCodes = string.Join(",", TCCodesList);
                            break;
                        case 3:
                            AccessType = 2;
                            TCCodes = objCompanyAccessRight.TCValues;
                            break;
                    }
                }
            }

            objAccessRightsDataAccess = null;

            BrickDataAccess objBrickDataaccess = new BrickDataAccess();
            IList<BrickMaster> objBrickList = new List<BrickMaster>();
           

            objBrickList = objBrickDataaccess.GetBrickListByCityID(objComparision.GeographyID, objComparision.CompanyID, objComparision.DivisonID, objComparision.PatchIds);
            objBrickDataaccess = null;
            IList<Patch> objtPathcList = new List<Patch>();


            for (int j = 0; j < objBrickList.Count; j++)
            {
                objtPathcList.Add(new Patch
                {
                    PatchID = objBrickList[j].BrickID,
                    PatchName = objBrickList[j].BrickName
                });
            }
            objComparsionPerformance.PatchList = objtPathcList;

            IList<CompanyPerformance> CompanyPerformanceList = new List<CompanyPerformance>();
            IList<CompanyPerformance> CompanyPerformanceUnitList = new List<CompanyPerformance>();
            // Following object is for the Patch level data

            IList<SalesByPatch> objCompanyPerformancebyPatch = new List<SalesByPatch>();
            IList<UnitByPatch> objCompanyPerformancebyPatchUnit = new List<UnitByPatch>();


            try
            {
                DataSet resultds = new DataSet();
                DALUtility IMSBIDB = new DALUtility();
                string param = string.Empty;
                if (objComparision != null)
                {

                    if (objComparision.ComparisonType == "company" || objComparision.ComparisonType == "manuf")
                    {
                        #region Company Manuf Data


                        CompanyDataAccess objCompanyDataAccessCp = new CompanyDataAccess();
                        if (objComparision.ComparisonType == "company")
                        {
                            resultds = objCompanyDataAccessCp.GetCompanyPerformance(objComparision, AccessType, TCCodes);
                        }
                        else
                        {
                            resultds = objCompanyDataAccessCp.GetManufPerformance(objComparision, AccessType, TCCodes);
                        }
                        objCompanyDataAccessCp = null;

                        IList<Int64> CompanyListToadd = new List<Int64>();
                        CompanyListToadd.Add(objComparision.CompanyID);
                        if (!string.IsNullOrEmpty(objComparision.ComparisionValue))
                        {
                            if (objComparision.ComparisionValue != "all")
                            {
                                string[] SelectedCompanyList = objComparision.ComparisionValue.Split(',');
                                if (SelectedCompanyList.Length > 0)
                                {
                                    for (int j = 0; j < SelectedCompanyList.Length; j++)
                                    {
                                        CompanyListToadd.Add(Convert.ToInt32(SelectedCompanyList[j]));
                                    }
                                }
                            }
                        }
                        if(resultds.Tables.Count >0)
                        { 
                        var result = (from row in resultds.Tables[0].AsEnumerable()
                                      select new CompanyPerformance
                                      {
                                          CompanyID = Convert.ToInt32(row["CompanyID"]),
                                          CompanyName = Convert.ToString(row["CompanyName"]),
                                          TotalSales = Convert.ToDouble(row["TotalSales"]),
                                          TotalUnits = Convert.ToDouble(row["TotalUnit"]),
                                          BrickID = Convert.ToInt32(row["BrickID"]),
                                          TimePeriod = Convert.ToInt32(row["TimePeriodID"]),
                                          PeriodName = Convert.ToString(row["TimePeriod"])
                                      }
                                      ).ToList();

                        Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                        Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                        objComparsionPerformance.TotalSalesValue = TotalMarketSale;
                        objComparsionPerformance.TotalSalesUnit = Math.Round(TotalMarketUnit);

                        var UniqueTimeperiodListData = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();

                       List<TimePeriod> UniqueTimeperiodList = UniqueTimeperiodListData.Select(a=> new TimePeriod {  MonthYear = a.PeriodName, TimePeriodID=a.TimePeriod}).OrderBy(a => a.TimePeriodID).ToList();
                         // Company  Graph Data
                         var CompanyResult = from r in result
                                            group r by new
                                            {
                                                r.CompanyID,
                                                r.CompanyName
                                            } into g
                                            select new CompanyPerformance
                                            {
                                                CompanyID = g.Key.CompanyID,
                                                CompanyName = g.Key.CompanyName,
                                                TotalSales = g.Sum(a => a.TotalSales),
                                                TotalUnits = g.Sum(a => a.TotalUnits)
                                            };

                        // Process PatchLevel Data

            
                        var CompanyResultSales = CompanyResult.OrderByDescending(a => a.TotalSales).ToList();
                        objComparsionPerformance.CompanyPerformance = this.GetCompanyPerformanceList(CompanyResultSales, CompanyListToadd, objComparision, 0);
                        var CompanyResultUnit = CompanyResult.OrderByDescending(b => b.TotalUnits).ToList();
                        objComparsionPerformance.CompanyPerformanceUnit = this.GetCompanyPerformanceList(CompanyResultUnit, CompanyListToadd, objComparision, 0);
                            // Company  Graphs Data ends
                            IList<Int64> TopSelectedCompanyValue = new List<Int64>();
                            IList<Int64> TopSelectedCompanyUnit = new List<Int64>();
                            if (objComparision.includeTopSearches)
                            {
                                TopSelectedCompanyValue = CompanyResultSales.Select(a => a.CompanyID).Take(10).ToList();
                                TopSelectedCompanyUnit = CompanyResult.Select(a => a.CompanyID).Take(10).ToList();
                            }
                            
                            
                            for(int i=0; i< CompanyListToadd.Count; i++)
                            {
                                if(!TopSelectedCompanyValue.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyValue.Add(CompanyListToadd[i]);
                                }
                                if (!TopSelectedCompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyUnit.Add(CompanyListToadd[i]);
                                }
                            }


                            ArrayList BarChartArraySale = new ArrayList();
                            ArrayList BarChartArrayUnit = new ArrayList();

                            var TimePeriodResult = from r in result
                                                group r by new
                                                {
                                                    r.TimePeriod,
                                                    r.PeriodName
                                                } into g
                                                select new CompanyPerformance
                                                {
                                                    TimePeriod = g.Key.TimePeriod,
                                                    PeriodName = g.Key.PeriodName,
                                                    TotalSales = g.Sum(a => a.TotalSales),
                                                    TotalUnits = g.Sum(a => a.TotalUnits)
                                                };

                            IList<BarChartData> BarChartSales = new List<BarChartData>();
                            IList<BarChartData> BarChartUnit = new List<BarChartData>();

                            CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                            IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objComparision.PeriodType.ToLower(), objComparision.PeriodStart, objComparision.PeriodEnd);
                            objCompanyDataaccessForTimeperiod = null;
                            for (int i = 0; i < TimePeriodlist.Count; i++)
                            {
                                CompanyPerformance objBarchartData = TimePeriodResult.Where(a => a.TimePeriod == TimePeriodlist[i].TimePeriodID).FirstOrDefault();
                                if (objBarchartData != null)
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(objBarchartData.PeriodName);
                                    CurrentObjectSale.Add(objBarchartData.TotalSales);
                                    BarChartArraySale.Add(CurrentObjectSale);


                                    ArrayList CurrentObjectUnit = new ArrayList();
                                    CurrentObjectUnit.Add(objBarchartData.PeriodName);
                                    CurrentObjectUnit.Add(objBarchartData.TotalUnits);
                                    BarChartArrayUnit.Add(CurrentObjectUnit);
                                }
                                else
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectSale.Add(0);
                                    BarChartArraySale.Add(CurrentObjectSale);


                                    ArrayList CurrentObjectUnit = new ArrayList();
                                    CurrentObjectUnit.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectUnit.Add(0);
                                    BarChartArrayUnit.Add(CurrentObjectUnit);
                                }

                            }

                            BarChartData objChartDataSale = new BarChartData();
                            objChartDataSale.key = "All Brands*";
                            objChartDataSale.values = BarChartArraySale;
                            BarChartSales.Add(objChartDataSale);

                            BarChartData objChartDataUnit = new BarChartData();
                            objChartDataUnit.key = "All Brands*";
                            objChartDataUnit.values = BarChartArrayUnit;
                            BarChartUnit.Add(objChartDataUnit);


                            // Company Table Data 
                            var CompanyResultTable = from r in result
                                                 group r by new
                                                 {
                                                     r.CompanyID,
                                                     r.CompanyName,
                                                     r.TimePeriod,
                                                     r.PeriodName
                                                 } into g
                                                 select new CompanyPerformance
                                                 {
                                                     CompanyID = g.Key.CompanyID,
                                                     CompanyName = g.Key.CompanyName,
                                                     PeriodName = g.Key.PeriodName,
                                                     TimePeriod = g.Key.TimePeriod,
                                                     TotalSales = g.Sum(a => a.TotalSales),
                                                     TotalUnits = g.Sum(a => a.TotalUnits)
                                                 };




                        // Company Tablel data ends
                          CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                        string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                        ColorCodeCompanyDataAccess = null;
                        Dictionary<Int64, string> ObjCompanyColorCodeBarchartSales = this.GetCompanyColorCode(TopSelectedCompanyValue, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                        Dictionary<Int64, string> ObjCompanyColorCodeBarchartUnit = this.GetCompanyColorCode(TopSelectedCompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                            var CompanyResultSalesTable = CompanyResultTable.OrderByDescending(a => a.TotalSales).ToList();
                        var CompanyResultUnitTable = CompanyResultTable.OrderByDescending(b => b.TotalUnits).ToList();
                            string ColorCode = string.Empty;
                            for (int i = 0; i < TopSelectedCompanyValue.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();
                               
                                var TimePeriodCompanyData = CompanyResultSalesTable.Where(a => a.CompanyID == TopSelectedCompanyValue[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(objBarchartData.TotalSales);
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }

                                    
                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartSales.Add(objChartComapnyDataSale);
                                }
                            }


                            for (int i = 0; i < TopSelectedCompanyUnit.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();

                                var TimePeriodCompanyData = CompanyResultUnitTable.Where(a => a.CompanyID == TopSelectedCompanyUnit[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(objBarchartData.TotalUnits);
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();

                                 
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }
                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartUnit.Add(objChartComapnyDataSale);
                                }
                            }

                            objComparsionPerformance.BarChartSales = BarChartSales;
                            objComparsionPerformance.BarChartUnit = BarChartUnit;
                            objComparsionPerformance.CompanyPerformanceSalesTable = this.GetCompanyPerformanceListTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                        
                        objComparsionPerformance.CompanyPerformanceUnitTable = this.GetCompanyPerformanceListTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 1, UniqueTimeperiodList);

                        objComparsionPerformance.CompanyComparisonSales = new List<SalesByPatch>();
                        objComparsionPerformance.CompanyComparisonUnit = new List<SalesByPatch>();

                        objComparsionPerformance.CompanyPerformanceSalesPatchTable = new List<PatchDetailsTableList>();
                        objComparsionPerformance.CompanyPerformanceUnitPatchTable = new List<PatchDetailsTableList>();

                       IList<Int64> Top5CompanySales = CompanyResult.OrderByDescending(a => a.TotalSales).Select(a =>  a.CompanyID).Distinct().Take(5).ToList();
                        if(!objComparision.includeTopSearches)
                        {
                            Top5CompanySales = new List<Int64>();
                        }
                        
                        for(int i=0; i<CompanyListToadd.Count; i++)
                        {
                            if(!Top5CompanySales.Contains(CompanyListToadd[i]))
                                {
                                        Top5CompanySales.Add(CompanyListToadd[i]);
                                }
                        }
                        
                        Dictionary<Int64, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(Top5CompanySales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                        objComparsionPerformance.CompanyComparisonSales = this.GetSalesPatchDataList(objBrickList, result, Top5CompanySales, 0, ObjCompanyColorCodeSales, objComparision.ComparisonType);

                         IList<Int64> Top5CompanyUnit = CompanyResult.OrderByDescending(a => a.TotalUnits).Select(a =>  a.CompanyID).Distinct().Take(5).ToList();
                        if(!objComparision.includeTopSearches)
                        {
                            Top5CompanyUnit = new List<Int64>();
                        }
                        
                        for(int i=0; i<CompanyListToadd.Count; i++)
                        {
                            if(!Top5CompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                Top5CompanyUnit.Add(CompanyListToadd[i]);
                                }
                        }
                        Dictionary<Int64, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(Top5CompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                        objComparsionPerformance.CompanyComparisonUnit = this.GetSalesPatchDataList(objBrickList, result, Top5CompanyUnit, 1, ObjCompanyColorCodeUnit, objComparision.ComparisonType);

                        objComparsionPerformance.CompanyComparisonByPatchSales = new List<PatchData>();
                        objComparsionPerformance.CompanyComparisonByPatchUnit = new List<PatchData>();
                            for (int i = 0; i < objBrickList.Count; i++)
                            {
                                IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID).ToList();

                                var CompanyResultPatch = from r in ObjBrickResult
                                                         group r by new
                                                         {
                                                             r.CompanyID,
                                                             r.CompanyName
                                                         } into g
                                                         select new CompanyPerformance
                                                         {
                                                             CompanyID = g.Key.CompanyID,
                                                             CompanyName = g.Key.CompanyName,
                                                             TotalSales = g.Sum(a => a.TotalSales),
                                                             TotalUnits = g.Sum(a => a.TotalUnits)
                                                         };

                                IList<Int64> Top5CompanyPatchSales = CompanyResultPatch.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                                if (!objComparision.includeTopSearches)
                                {
                                    Top5CompanyPatchSales = new List<Int64>();
                                }

                                for (int k = 0; k < CompanyListToadd.Count; k++)
                                {
                                    if (!Top5CompanyPatchSales.Contains(CompanyListToadd[k]))
                                    {
                                        Top5CompanyPatchSales.Add(CompanyListToadd[k]);
                                    }
                                }
                                IList<BrickMaster> objBrickdata = new List<BrickMaster>();
                                objBrickdata.Add(objBrickList[i]);

                                Dictionary<Int64, string> ObjCompanyColorCodePatchSale = this.GetCompanyColorCode(Top5CompanyPatchSales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                                IList<SalesByPatch> objPatchSalesPatchListSales = this.GetSalesPatchDataList(objBrickdata, result, Top5CompanyPatchSales, 0, ObjCompanyColorCodePatchSale, objComparision.ComparisonType);
                                PatchData objPatchData = new PatchData()
                                {
                                    PatchDataDetails = objPatchSalesPatchListSales,
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName
                                };
                                objComparsionPerformance.CompanyComparisonByPatchSales.Add(objPatchData);

                                // Unit
                                IList<Int64> Top5CompanyPatchUnit = CompanyResultPatch.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                                if (!objComparision.includeTopSearches)
                                {
                                    Top5CompanyPatchUnit = new List<Int64>();
                                }

                                for (int k = 0; k < CompanyListToadd.Count; k++)
                                {
                                    if (!Top5CompanyPatchUnit.Contains(CompanyListToadd[k]))
                                    {
                                        Top5CompanyPatchUnit.Add(CompanyListToadd[k]);
                                    }
                                }
                                Dictionary<Int64, string> ObjCompanyColorCodePatchUnit = this.GetCompanyColorCode(Top5CompanyPatchUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                                IList<SalesByPatch> objPatchSalesPatchListUnit = this.GetSalesPatchDataList(objBrickdata, result, Top5CompanyPatchUnit, 1, ObjCompanyColorCodePatchUnit, objComparision.ComparisonType);
                                PatchData objPatchDataUnit = new PatchData()
                                {
                                    PatchDataDetails = objPatchSalesPatchListUnit,
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName
                                };
                                objComparsionPerformance.CompanyComparisonByPatchUnit.Add(objPatchDataUnit);



                                // Patch Table 
                                var CompanyResultPatchTable = from r in ObjBrickResult
                                                              group r by new
                                                              {
                                                                  r.CompanyID,
                                                                  r.CompanyName,
                                                                  r.TimePeriod,
                                                                  r.PeriodName
                                                              } into g
                                                              select new CompanyPerformance
                                                              {
                                                                  CompanyID = g.Key.CompanyID,
                                                                  CompanyName = g.Key.CompanyName,
                                                                  PeriodName = g.Key.PeriodName,
                                                                  TimePeriod = g.Key.TimePeriod,
                                                                  TotalSales = g.Sum(a => a.TotalSales),
                                                                  TotalUnits = g.Sum(a => a.TotalUnits)
                                                              };

                                // Company Tablel data ends
                                var CompanyResultSalesPatchTable = CompanyResultPatchTable.OrderByDescending(a => a.TotalSales).ToList();
                                IList<TableList> TableDataSales = this.GetCompanyPerformanceListTable(CompanyResultSalesPatchTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                                var CompanyResultUnitPatchTable = CompanyResultPatchTable.OrderByDescending(b => b.TotalUnits).ToList();
                                IList<TableList> TableDataUnit = this.GetCompanyPerformanceListTable(CompanyResultUnitPatchTable, CompanyListToadd, objComparision, 0, 1, UniqueTimeperiodList);

                                PatchDetailsTableList objCCPSalePatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataSales
                                };

                                PatchDetailsTableList objCCPUnitPatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataUnit
                                };
                                objComparsionPerformance.CompanyPerformanceSalesPatchTable.Add(objCCPSalePatch);
                                objComparsionPerformance.CompanyPerformanceUnitPatchTable.Add(objCCPUnitPatch);
                            }
                        }
                        #endregion
                    }
                    else if (objComparision.ComparisonType == "therapy")
                    {
                        #region Therapy
                       
                        CompanyDataAccess objCompanyDataAccessCp = new CompanyDataAccess();
                        resultds = objCompanyDataAccessCp.GetCompanyPerformance(objComparision, AccessType, TCCodes);
                                               
                        objCompanyDataAccessCp = null;

                        IList<Int64> CompanyListToadd = new List<Int64>();
                        CompanyListToadd.Add(objComparision.CompanyID);
                        if (!string.IsNullOrEmpty(objComparision.ComparisionValue))
                        {
                            if (objComparision.ComparisionValue != "all")
                            {
                                string[] SelectedCompanyList = objComparision.ComparisionValue.Split(',');
                                if (SelectedCompanyList.Length > 0)
                                {
                                    for (int j = 0; j < SelectedCompanyList.Length; j++)
                                    {
                                        CompanyListToadd.Add(Convert.ToInt64(SelectedCompanyList[j]));
                                    }
                                }
                            }
                        }
                        if (resultds.Tables.Count > 0)
                        {
                            var result = (from row in resultds.Tables[0].AsEnumerable()
                                          select new CompanyPerformance
                                          {
                                              CompanyID = Convert.ToInt32(row["CompanyID"]),
                                              CompanyName = Convert.ToString(row["CompanyName"]),
                                              TotalSales = Convert.ToDouble(row["TotalSales"]),
                                              TotalUnits = Convert.ToDouble(row["TotalUnit"]),
                                              BrickID = Convert.ToInt32(row["BrickID"]),
                                              TimePeriod = Convert.ToInt32(row["TimePeriodID"]),
                                              PeriodName = Convert.ToString(row["TimePeriod"])
                                          }
                                          ).ToList();

                            Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                            Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                            objComparsionPerformance.TotalSalesValue = TotalMarketSale;
                            objComparsionPerformance.TotalSalesUnit = Math.Round(TotalMarketUnit);

                            var UniqueTimeperiodListData = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();

                            List<TimePeriod> UniqueTimeperiodList = UniqueTimeperiodListData.Select(a => new TimePeriod { MonthYear = a.PeriodName, TimePeriodID = a.TimePeriod }).OrderBy(a => a.TimePeriodID).ToList();
                            // Company  Graph Data
                            var CompanyResult = from r in result
                                                group r by new
                                                {
                                                    r.CompanyID,
                                                    r.CompanyName
                                                } into g
                                                select new CompanyPerformance
                                                {
                                                    CompanyID = g.Key.CompanyID,
                                                    CompanyName = g.Key.CompanyName,
                                                    TotalSales = g.Sum(a => a.TotalSales),
                                                    TotalUnits = g.Sum(a => a.TotalUnits)
                                                };

                            // Process PatchLevel Data


                            var CompanyResultSales = CompanyResult.OrderByDescending(a => a.TotalSales).ToList();
                            objComparsionPerformance.CompanyPerformance = this.GetCompanyPerformanceList(CompanyResultSales, CompanyListToadd, objComparision, 0);
                            var CompanyResultUnit = CompanyResult.OrderByDescending(b => b.TotalUnits).ToList();
                            objComparsionPerformance.CompanyPerformanceUnit = this.GetCompanyPerformanceList(CompanyResultUnit, CompanyListToadd, objComparision, 0);
                            // Company  Graphs Data ends
                            IList<Int64> TopSelectedCompanyValue = new List<Int64>();
                            IList<Int64> TopSelectedCompanyUnit = new List<Int64>();
                            if (objComparision.includeTopSearches)
                            {
                                TopSelectedCompanyValue = CompanyResultSales.Select(a => a.CompanyID).Take(10).ToList();
                                TopSelectedCompanyUnit = CompanyResult.Select(a => a.CompanyID).Take(10).ToList();
                            }


                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!TopSelectedCompanyValue.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyValue.Add(CompanyListToadd[i]);
                                }
                                if (!TopSelectedCompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyUnit.Add(CompanyListToadd[i]);
                                }
                            }


                            ArrayList BarChartArraySale = new ArrayList();
                            ArrayList BarChartArrayUnit = new ArrayList();

                            var TimePeriodResult = from r in result
                                                   group r by new
                                                   {
                                                       r.TimePeriod,
                                                       r.PeriodName
                                                   } into g
                                                   select new CompanyPerformance
                                                   {
                                                       TimePeriod = g.Key.TimePeriod,
                                                       PeriodName = g.Key.PeriodName,
                                                       TotalSales = g.Sum(a => a.TotalSales),
                                                       TotalUnits = g.Sum(a => a.TotalUnits)
                                                   };

                            IList<BarChartData> BarChartSales = new List<BarChartData>();
                            IList<BarChartData> BarChartUnit = new List<BarChartData>();

                            CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                            IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objComparision.PeriodType.ToLower(), objComparision.PeriodStart, objComparision.PeriodEnd);
                            objCompanyDataaccessForTimeperiod = null;
                            for (int i = 0; i < TimePeriodlist.Count; i++)
                            {
                                CompanyPerformance objBarchartData = TimePeriodResult.Where(a => a.TimePeriod == TimePeriodlist[i].TimePeriodID).FirstOrDefault();
                                if (objBarchartData != null)
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(objBarchartData.PeriodName);
                                    CurrentObjectSale.Add(objBarchartData.TotalSales);
                                    BarChartArraySale.Add(CurrentObjectSale);


                                    ArrayList CurrentObjectUnit = new ArrayList();
                                    CurrentObjectUnit.Add(objBarchartData.PeriodName);
                                    CurrentObjectUnit.Add(objBarchartData.TotalUnits);
                                    BarChartArrayUnit.Add(CurrentObjectUnit);
                                }
                                else
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectSale.Add(0);
                                    BarChartArraySale.Add(CurrentObjectSale);


                                    ArrayList CurrentObjectUnit = new ArrayList();
                                    CurrentObjectUnit.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectUnit.Add(0);
                                    BarChartArrayUnit.Add(CurrentObjectUnit);
                                }

                            }

                            BarChartData objChartDataSale = new BarChartData();
                            objChartDataSale.key = "All Brands*";
                            objChartDataSale.values = BarChartArraySale;
                            BarChartSales.Add(objChartDataSale);

                            BarChartData objChartDataUnit = new BarChartData();
                            objChartDataUnit.key = "All Brands*";
                            objChartDataUnit.values = BarChartArrayUnit;
                            BarChartUnit.Add(objChartDataUnit);


                            // Company Table Data 
                            var CompanyResultTable = from r in result
                                                     group r by new
                                                     {
                                                         r.CompanyID,
                                                         r.CompanyName,
                                                         r.TimePeriod,
                                                         r.PeriodName
                                                     } into g
                                                     select new CompanyPerformance
                                                     {
                                                         CompanyID = g.Key.CompanyID,
                                                         CompanyName = g.Key.CompanyName,
                                                         PeriodName = g.Key.PeriodName,
                                                         TimePeriod = g.Key.TimePeriod,
                                                         TotalSales = g.Sum(a => a.TotalSales),
                                                         TotalUnits = g.Sum(a => a.TotalUnits)
                                                     };




                            // Company Tablel data ends
                            CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                            string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                            ColorCodeCompanyDataAccess = null;
                            Dictionary<Int64, string> ObjCompanyColorCodeBarchartSales = this.GetCompanyColorCode(TopSelectedCompanyValue, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                            Dictionary<Int64, string> ObjCompanyColorCodeBarchartUnit = this.GetCompanyColorCode(TopSelectedCompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                            var CompanyResultSalesTable = CompanyResultTable.OrderByDescending(a => a.TotalSales).ToList();
                            var CompanyResultUnitTable = CompanyResultTable.OrderByDescending(b => b.TotalUnits).ToList();
                            string ColorCode = string.Empty;
                            for (int i = 0; i < TopSelectedCompanyValue.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();

                                var TimePeriodCompanyData = CompanyResultSalesTable.Where(a => a.CompanyID == TopSelectedCompanyValue[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(objBarchartData.TotalSales);
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }


                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartSales.Add(objChartComapnyDataSale);
                                }
                            }


                            for (int i = 0; i < TopSelectedCompanyUnit.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();

                                var TimePeriodCompanyData = CompanyResultUnitTable.Where(a => a.CompanyID == TopSelectedCompanyUnit[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(objBarchartData.TotalUnits);
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();


                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }
                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartUnit.Add(objChartComapnyDataSale);
                                }
                            }

                            objComparsionPerformance.BarChartSales = BarChartSales;
                            objComparsionPerformance.BarChartUnit = BarChartUnit;
                            objComparsionPerformance.CompanyPerformanceSalesTable = this.GetCompanyPerformanceListTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);

                            objComparsionPerformance.CompanyPerformanceUnitTable = this.GetCompanyPerformanceListTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 1, UniqueTimeperiodList);

                            objComparsionPerformance.CompanyComparisonSales = new List<SalesByPatch>();
                            objComparsionPerformance.CompanyComparisonUnit = new List<SalesByPatch>();

                            objComparsionPerformance.CompanyPerformanceSalesPatchTable = new List<PatchDetailsTableList>();
                            objComparsionPerformance.CompanyPerformanceUnitPatchTable = new List<PatchDetailsTableList>();

                            IList<Int64> Top5CompanySales = CompanyResult.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                            if (!objComparision.includeTopSearches)
                            {
                                Top5CompanySales = new List<Int64>();
                            }

                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!Top5CompanySales.Contains(CompanyListToadd[i]))
                                {
                                    Top5CompanySales.Add(CompanyListToadd[i]);
                                }
                            }

                            Dictionary<Int64, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(Top5CompanySales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                            objComparsionPerformance.CompanyComparisonSales = this.GetSalesPatchDataList(objBrickList, result, Top5CompanySales, 0, ObjCompanyColorCodeSales, objComparision.ComparisonType);

                            IList<Int64> Top5CompanyUnit = CompanyResult.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                            if (!objComparision.includeTopSearches)
                            {
                                Top5CompanyUnit = new List<Int64>();
                            }

                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!Top5CompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                    Top5CompanyUnit.Add(CompanyListToadd[i]);
                                }
                            }
                            Dictionary<Int64, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(Top5CompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                            objComparsionPerformance.CompanyComparisonUnit = this.GetSalesPatchDataList(objBrickList, result, Top5CompanyUnit, 1, ObjCompanyColorCodeUnit, objComparision.ComparisonType);

                            objComparsionPerformance.CompanyComparisonByPatchSales = new List<PatchData>();
                            objComparsionPerformance.CompanyComparisonByPatchUnit = new List<PatchData>();
                            for (int i = 0; i < objBrickList.Count; i++)
                            {
                                IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID).ToList();

                                var CompanyResultPatch = from r in ObjBrickResult
                                                         group r by new
                                                         {
                                                             r.CompanyID,
                                                             r.CompanyName
                                                         } into g
                                                         select new CompanyPerformance
                                                         {
                                                             CompanyID = g.Key.CompanyID,
                                                             CompanyName = g.Key.CompanyName,
                                                             TotalSales = g.Sum(a => a.TotalSales),
                                                             TotalUnits = g.Sum(a => a.TotalUnits)
                                                         };

                                IList<Int64> Top5CompanyPatchSales = CompanyResultPatch.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                                if (!objComparision.includeTopSearches)
                                {
                                    Top5CompanyPatchSales = new List<Int64>();
                                }

                                for (int k = 0; k < CompanyListToadd.Count; k++)
                                {
                                    if (!Top5CompanyPatchSales.Contains(CompanyListToadd[k]))
                                    {
                                        Top5CompanyPatchSales.Add(CompanyListToadd[k]);
                                    }
                                }
                                IList<BrickMaster> objBrickdata = new List<BrickMaster>();
                                objBrickdata.Add(objBrickList[i]);

                                Dictionary<Int64, string> ObjCompanyColorCodePatchSale = this.GetCompanyColorCode(Top5CompanyPatchSales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                                IList<SalesByPatch> objPatchSalesPatchListSales = this.GetSalesPatchDataList(objBrickdata, result, Top5CompanyPatchSales, 0, ObjCompanyColorCodePatchSale, objComparision.ComparisonType);
                                PatchData objPatchData = new PatchData()
                                {
                                    PatchDataDetails = objPatchSalesPatchListSales,
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName
                                };
                                objComparsionPerformance.CompanyComparisonByPatchSales.Add(objPatchData);

                                // Unit
                                IList<Int64> Top5CompanyPatchUnit = CompanyResultPatch.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                                if (!objComparision.includeTopSearches)
                                {
                                    Top5CompanyPatchUnit = new List<Int64>();
                                }

                                for (int k = 0; k < CompanyListToadd.Count; k++)
                                {
                                    if (!Top5CompanyPatchUnit.Contains(CompanyListToadd[k]))
                                    {
                                        Top5CompanyPatchUnit.Add(CompanyListToadd[k]);
                                    }
                                }
                                Dictionary<Int64, string> ObjCompanyColorCodePatchUnit = this.GetCompanyColorCode(Top5CompanyPatchUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                                IList<SalesByPatch> objPatchSalesPatchListUnit = this.GetSalesPatchDataList(objBrickdata, result, Top5CompanyPatchUnit, 1, ObjCompanyColorCodePatchUnit, objComparision.ComparisonType);
                                PatchData objPatchDataUnit = new PatchData()
                                {
                                    PatchDataDetails = objPatchSalesPatchListUnit,
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName
                                };
                                objComparsionPerformance.CompanyComparisonByPatchUnit.Add(objPatchDataUnit);



                                // Patch Table 
                                var CompanyResultPatchTable = from r in ObjBrickResult
                                                              group r by new
                                                              {
                                                                  r.CompanyID,
                                                                  r.CompanyName,
                                                                  r.TimePeriod,
                                                                  r.PeriodName
                                                              } into g
                                                              select new CompanyPerformance
                                                              {
                                                                  CompanyID = g.Key.CompanyID,
                                                                  CompanyName = g.Key.CompanyName,
                                                                  PeriodName = g.Key.PeriodName,
                                                                  TimePeriod = g.Key.TimePeriod,
                                                                  TotalSales = g.Sum(a => a.TotalSales),
                                                                  TotalUnits = g.Sum(a => a.TotalUnits)
                                                              };

                                // Company Tablel data ends
                                var CompanyResultSalesPatchTable = CompanyResultPatchTable.OrderByDescending(a => a.TotalSales).ToList();
                                IList<TableList> TableDataSales = this.GetCompanyPerformanceListTable(CompanyResultSalesPatchTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                                var CompanyResultUnitPatchTable = CompanyResultPatchTable.OrderByDescending(b => b.TotalUnits).ToList();
                                IList<TableList> TableDataUnit = this.GetCompanyPerformanceListTable(CompanyResultUnitPatchTable, CompanyListToadd, objComparision, 0, 1, UniqueTimeperiodList);

                                PatchDetailsTableList objCCPSalePatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataSales
                                };

                                PatchDetailsTableList objCCPUnitPatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataUnit
                                };
                                objComparsionPerformance.CompanyPerformanceSalesPatchTable.Add(objCCPSalePatch);
                                objComparsionPerformance.CompanyPerformanceUnitPatchTable.Add(objCCPUnitPatch);
                            }
                        }
                        #endregion
                      
                    }
                    else
                    {

                        #region Brand SKU Data
                        BrandDataAccess objBrandDataAccessBP = new BrandDataAccess();
                        if(objComparision.ComparisonType =="brand")
                        {
                            resultds = objBrandDataAccessBP.GetBrandPerformance(objComparision, AccessType, TCCodes);

                        }
                        else
                        {
                            resultds = objBrandDataAccessBP.GetSKUPerformance(objComparision, AccessType, TCCodes);

                        }

                        objBrandDataAccessBP = null;

                        IList<Int64> CompanyListToadd = new List<Int64>();
                        if (!string.IsNullOrEmpty(objComparision.ComparisionValue))
                        {
                            if (objComparision.ComparisionValue != "all")
                            {
                                string[] SelectedCompanyList = objComparision.ComparisionValue.Split(',');
                                if (SelectedCompanyList.Length > 0)
                                {
                                    for (int j = 0; j < SelectedCompanyList.Length; j++)
                                    {
                                        CompanyListToadd.Add(Convert.ToInt64(SelectedCompanyList[j]));
                                    }
                                }
                            }
                        }

                        if (resultds.Tables.Count > 0)
                        {

                            var result = (from row in resultds.Tables[0].AsEnumerable()
                                          select new CompanyPerformance
                                          {
                                              CompanyID = Convert.ToInt64(row["BrandID"]),
                                              CompanyName = Convert.ToString(row["BrandName"]),
                                              TotalSales = Convert.ToDouble(row["TotalSales"]),
                                              TotalUnits = Convert.ToDouble(row["TotalUnit"]),
                                              BrickID = Convert.ToInt32(row["BrickID"]),
                                              TimePeriod = Convert.ToInt32(row["TimePeriodID"]),
                                              PeriodName = Convert.ToString(row["TimePeriod"])
                                          }
                                          ).ToList();


                            Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                            Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                            objComparsionPerformance.TotalSalesValue = TotalMarketSale;
                            objComparsionPerformance.TotalSalesUnit = Math.Round(TotalMarketUnit);

                            var UniqueTimeperiodListData = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();

                            List<TimePeriod> UniqueTimeperiodList = UniqueTimeperiodListData.Select(a => new TimePeriod { MonthYear = a.PeriodName, TimePeriodID = a.TimePeriod }).OrderBy(a => a.TimePeriodID).ToList();

                            // Company  Graph Data
                            var CompanyResult = from r in result
                                                group r by new
                                                {
                                                    r.CompanyID,
                                                    r.CompanyName
                                                } into g
                                                select new CompanyPerformance
                                                {
                                                    CompanyID = g.Key.CompanyID,
                                                    CompanyName = g.Key.CompanyName,
                                                    TotalSales = g.Sum(a => a.TotalSales),
                                                    TotalUnits = g.Sum(a => a.TotalUnits)
                                                };

                            // Process PatchLevel Data

                            ArrayList BarChartArraySale = new ArrayList();
                            ArrayList BarChartArrayUnit = new ArrayList();

                            var TimePeriodResult = from r in result
                                                   group r by new
                                                   {
                                                       r.TimePeriod,
                                                       r.PeriodName
                                                   } into g
                                                   select new CompanyPerformance
                                                   {
                                                       TimePeriod = g.Key.TimePeriod,
                                                       PeriodName = g.Key.PeriodName,
                                                       TotalSales = g.Sum(a => a.TotalSales),
                                                       TotalUnits = g.Sum(a => a.TotalUnits)
                                                   };


                            IList<BarChartData> BarChartSales = new List<BarChartData>();
                            IList<BarChartData> BarChartUnit = new List<BarChartData>();

                            CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                            IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objComparision.PeriodType.ToLower(), objComparision.PeriodStart, objComparision.PeriodEnd);
                            objCompanyDataaccessForTimeperiod = null;
                            for (int i = 0; i < TimePeriodlist.Count; i++)
                            {
                                CompanyPerformance objBarchartData = TimePeriodResult.Where(a => a.TimePeriod == TimePeriodlist[i].TimePeriodID).FirstOrDefault();
                                if (objBarchartData != null)
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(objBarchartData.PeriodName);
                                    CurrentObjectSale.Add(objBarchartData.TotalSales);
                                    BarChartArraySale.Add(CurrentObjectSale);


                                    ArrayList CurrentObjectUnit = new ArrayList();
                                    CurrentObjectUnit.Add(objBarchartData.PeriodName);
                                    CurrentObjectUnit.Add(objBarchartData.TotalUnits);
                                    BarChartArrayUnit.Add(CurrentObjectUnit);
                                }
                                else
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectSale.Add(0);
                                    BarChartArraySale.Add(CurrentObjectSale);


                                    ArrayList CurrentObjectUnit = new ArrayList();
                                    CurrentObjectUnit.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectUnit.Add(0);
                                    BarChartArrayUnit.Add(CurrentObjectUnit);
                                }

                            }

                            BarChartData objChartDataSale = new BarChartData();
                            objChartDataSale.key = "All Brands*";
                            objChartDataSale.values = BarChartArraySale;
                            BarChartSales.Add(objChartDataSale);

                            BarChartData objChartDataUnit = new BarChartData();
                            objChartDataUnit.key = "All Brands*";
                            objChartDataUnit.values = BarChartArrayUnit;
                            BarChartUnit.Add(objChartDataUnit);

                            var CompanyResultSales = CompanyResult.OrderByDescending(a => a.TotalSales).ToList();
                            objComparsionPerformance.CompanyPerformance = this.GetBrandPerformanceList(CompanyResultSales, CompanyListToadd, objComparision, 1);
                            var CompanyResultUnit = CompanyResult.OrderByDescending(b => b.TotalUnits).ToList();
                            objComparsionPerformance.CompanyPerformanceUnit = this.GetBrandPerformanceList(CompanyResultUnit, CompanyListToadd, objComparision, 1);
                            // Company  Graphs Data ends
                            IList<Int64> TopSelectedCompanyValue = new List<Int64>();
                            IList<Int64> TopSelectedCompanyUnit = new List<Int64>();
                            if (objComparision.includeTopSearches)
                            {
                                TopSelectedCompanyValue = CompanyResultSales.Select(a => a.CompanyID).Take(10).ToList();
                                TopSelectedCompanyUnit = CompanyResult.Select(a => a.CompanyID).Take(10).ToList();
                            }
                            
                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!TopSelectedCompanyValue.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyValue.Add(CompanyListToadd[i]);
                                }
                                if (!TopSelectedCompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyUnit.Add(CompanyListToadd[i]);
                                }
                            }
                            // Company Table Data 
                            var CompanyResultTable = from r in result
                                                     group r by new
                                                     {
                                                         r.CompanyID,
                                                         r.CompanyName,
                                                         r.TimePeriod,
                                                         r.PeriodName
                                                     } into g
                                                     select new CompanyPerformance
                                                     {
                                                         CompanyID = g.Key.CompanyID,
                                                         CompanyName = g.Key.CompanyName,
                                                         PeriodName = g.Key.PeriodName,
                                                         TimePeriod = g.Key.TimePeriod,
                                                         TotalSales = g.Sum(a => a.TotalSales),
                                                         TotalUnits = g.Sum(a => a.TotalUnits)
                                                     };

                            // Company Tablel data ends
                            
                           CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                            string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                            ColorCodeCompanyDataAccess = null;
                            Dictionary<Int64, string> ObjCompanyColorCodeBarchartSales = this.GetCompanyColorCode(TopSelectedCompanyValue, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                            Dictionary<Int64, string> ObjCompanyColorCodeBarchartUnit = this.GetCompanyColorCode(TopSelectedCompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);

                            var CompanyResultSalesTable = CompanyResultTable.OrderByDescending(a => a.TotalSales).ToList();
                            var CompanyResultUnitTable = CompanyResultTable.OrderByDescending(b => b.TotalUnits).ToList();
                            string ColorCode = string.Empty;
                            for (int i = 0; i < TopSelectedCompanyValue.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();

                                var TimePeriodCompanyData = CompanyResultSalesTable.Where(a => a.CompanyID == TopSelectedCompanyValue[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count >0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(objBarchartData.TotalSales);
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }


                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartSales.Add(objChartComapnyDataSale);
                                }
                            }


                            for (int i = 0; i < TopSelectedCompanyUnit.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();

                                var TimePeriodCompanyData = CompanyResultUnitTable.Where(a => a.CompanyID == TopSelectedCompanyUnit[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(objBarchartData.TotalUnits);
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();


                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }
                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartUnit.Add(objChartComapnyDataSale);
                                }
                            }

                            objComparsionPerformance.BarChartSales = BarChartSales;
                            objComparsionPerformance.BarChartUnit = BarChartUnit;

                            objComparsionPerformance.CompanyPerformanceSalesTable = this.GetBrandPerformanceListTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                           
                            objComparsionPerformance.CompanyPerformanceUnitTable = this.GetBrandPerformanceListTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 1, UniqueTimeperiodList);

                            objComparsionPerformance.CompanyComparisonSales = new List<SalesByPatch>();
                            objComparsionPerformance.CompanyComparisonUnit = new List<SalesByPatch>();

                            objComparsionPerformance.CompanyPerformanceSalesPatchTable = new List<PatchDetailsTableList>();
                            objComparsionPerformance.CompanyPerformanceUnitPatchTable = new List<PatchDetailsTableList>();


                            IList<Int64> Top5CompanySales = CompanyResultSales.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                            if (!objComparision.includeTopSearches)
                            {
                                Top5CompanySales = new List<Int64>();
                            }

                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!Top5CompanySales.Contains(CompanyListToadd[i]))
                                {
                                    Top5CompanySales.Add(CompanyListToadd[i]);
                                }
                            }
                        
                            Dictionary<Int64, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(Top5CompanySales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                            string ProducttypeString = "brand";
                            if (objComparision.DataType !="npm" && objComparision.ComparisonType == "brand")
                            {
                                if(!string.IsNullOrEmpty(objComparision.SKUIDs))
                                {
                                    //ProducttypeString = "sku";
                                }
                            }
                            else
                            {
                               if(objComparision.ComparisonType == "sku")
                                {
                                    ProducttypeString = "sku";
                                }
                            }
                            objComparsionPerformance.CompanyComparisonSales = this.GetSalesPatchDataListBrand(objBrickList, result, Top5CompanySales, 0, ObjCompanyColorCodeSales, ProducttypeString);

                            IList<Int64> Top5CompanyUnit = CompanyResultUnit.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                            if (!objComparision.includeTopSearches)
                            {
                                Top5CompanyUnit = new List<Int64>();
                            }

                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!Top5CompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                    Top5CompanyUnit.Add(CompanyListToadd[i]);
                                }
                            }
                            Dictionary<Int64, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(Top5CompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                            objComparsionPerformance.CompanyComparisonUnit = this.GetSalesPatchDataListBrand(objBrickList, result, Top5CompanyUnit, 1, ObjCompanyColorCodeUnit, ProducttypeString);

                            objComparsionPerformance.CompanyComparisonByPatchSales = new List<PatchData>();
                            objComparsionPerformance.CompanyComparisonByPatchUnit = new List<PatchData>();

                            for (int i = 0; i < objBrickList.Count; i++)
                            {
                                IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID).ToList();

                                var CompanyResultPatch = from r in ObjBrickResult
                                                         group r by new
                                                         {
                                                             r.CompanyID,
                                                             r.CompanyName
                                                         } into g
                                                         select new CompanyPerformance
                                                         {
                                                             CompanyID = g.Key.CompanyID,
                                                             CompanyName = g.Key.CompanyName,
                                                             TotalSales = g.Sum(a => a.TotalSales),
                                                             TotalUnits = g.Sum(a => a.TotalUnits)
                                                         };

                                IList<Int64> Top5CompanyPatchSales = CompanyResultPatch.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                                if (!objComparision.includeTopSearches)
                                {
                                    Top5CompanyPatchSales = new List<Int64>();
                                }

                                for (int k = 0; k < CompanyListToadd.Count; k++)
                                {
                                    if (!Top5CompanyPatchSales.Contains(CompanyListToadd[k]))
                                    {
                                        Top5CompanyPatchSales.Add(CompanyListToadd[k]);
                                    }
                                }
                                IList<BrickMaster> objBrickdata = new List<BrickMaster>();
                                objBrickdata.Add(objBrickList[i]);
                                Dictionary<Int64, string> ObjCompanyColorCodePatchSale = this.GetCompanyColorCode(Top5CompanyPatchSales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                                IList<SalesByPatch> objPatchSalesPatchListSales = this.GetSalesPatchDataListBrand(objBrickdata, result, Top5CompanyPatchSales, 0, ObjCompanyColorCodePatchSale, ProducttypeString);
                                PatchData objPatchData = new PatchData()
                                {
                                    PatchDataDetails = objPatchSalesPatchListSales,
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName
                                };
                                objComparsionPerformance.CompanyComparisonByPatchSales.Add(objPatchData);

                                // Unit
                                IList<Int64> Top5CompanyPatchUnit = CompanyResultPatch.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();
                                if (!objComparision.includeTopSearches)
                                {
                                    Top5CompanyPatchUnit = new List<Int64>();
                                }

                                for (int k = 0; k < CompanyListToadd.Count; k++)
                                {
                                    if (!Top5CompanyPatchUnit.Contains(CompanyListToadd[k]))
                                    {
                                        Top5CompanyPatchUnit.Add(CompanyListToadd[k]);
                                    }
                                }
                                Dictionary<Int64, string> ObjCompanyColorCodePatchUnit = this.GetCompanyColorCode(Top5CompanyPatchUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                                IList<SalesByPatch> objPatchSalesPatchListUnit = this.GetSalesPatchDataListBrand(objBrickdata, result, Top5CompanyPatchUnit, 1, ObjCompanyColorCodePatchUnit, ProducttypeString);
                                PatchData objPatchDataUnit = new PatchData()
                                {
                                    PatchDataDetails = objPatchSalesPatchListUnit,
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName
                                };
                                objComparsionPerformance.CompanyComparisonByPatchUnit.Add(objPatchDataUnit);


                                // Patch Table 
                                var CompanyResultPatchTable = from r in ObjBrickResult
                                                              group r by new
                                                              {
                                                                  r.CompanyID,
                                                                  r.CompanyName,
                                                                  r.TimePeriod,
                                                                  r.PeriodName
                                                              } into g
                                                              select new CompanyPerformance
                                                              {
                                                                  CompanyID = g.Key.CompanyID,
                                                                  CompanyName = g.Key.CompanyName,
                                                                  PeriodName = g.Key.PeriodName,
                                                                  TimePeriod = g.Key.TimePeriod,
                                                                  TotalSales = g.Sum(a => a.TotalSales),
                                                                  TotalUnits = g.Sum(a => a.TotalUnits)
                                                              };

                                // Company Tablel data ends
                                var CompanyResultSalesPatchTable = CompanyResultPatchTable.OrderByDescending(a => a.TotalSales).ToList();
                                IList<TableList> TableDataSales = this.GetBrandPerformanceListTable(CompanyResultSalesPatchTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                                var CompanyResultUnitPatchTable = CompanyResultPatchTable.OrderByDescending(b => b.TotalUnits).ToList();
                                IList<TableList> TableDataUnit = this.GetBrandPerformanceListTable(CompanyResultUnitPatchTable, CompanyListToadd, objComparision, 0, 1, UniqueTimeperiodList);

                                PatchDetailsTableList objCCPSalePatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataSales
                                };

                                PatchDetailsTableList objCCPUnitPatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataUnit
                                };
                                objComparsionPerformance.CompanyPerformanceSalesPatchTable.Add(objCCPSalePatch);
                                objComparsionPerformance.CompanyPerformanceUnitPatchTable.Add(objCCPUnitPatch);

                            }
                          
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objComparsionPerformance;
        }


        public IList<BrandComprsionPerformanceDetails> GetCompanyBrandPerformanceReport(BrandComparison_IVM objComparision)
        {


            IList<BrandComprsionPerformanceDetails> objBrandComparsionPerformanceList = new List<BrandComprsionPerformanceDetails>();
            // Validate the session 
            SessionDataAccess objSessionDataAccess = new SessionDataAccess();
            int UserID = objSessionDataAccess.ValidateSession(objComparision.SessionID);
            objSessionDataAccess = null;
            if (UserID == 0)
            {
                objBrandComparsionPerformanceList = null;
                return objBrandComparsionPerformanceList;
            }

            // Access log
            UserLog_VM objLogDetails = new UserLog_VM()
            {
                UserID = UserID,
                MenuID = 6,
                CityID = 0,
                CategoryType = "brand",
                CategoryValue = objComparision.BrandID.ToString(),
                DataType = objComparision.DataType == "npm" ? "ipm" : objComparision.DataType,
                EndPeriod = objComparision.PeriodEnd,
                StartPeriod = objComparision.PeriodStart,
                PatchID = "all",
                PeriodType = objComparision.PeriodType,
                TCCode = "all",
                TCLevel = "all",
                ProductID = "",
                SessionID = objComparision.SessionID


            };
            UserLogAccessDetails objUserLogAccessDetails = new UserLogAccessDetails();
            objUserLogAccessDetails.AddUpdateUserLog(objLogDetails);
            objUserLogAccessDetails = null;
            // Get UserAccessRights 

            AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
            UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

            int AccessType = 1;
            string TCCodes = string.Empty;
            string CityIDs = string.Empty;
            if (objUserAccessrights != null && !string.IsNullOrEmpty(objUserAccessrights.CityIDs))
            {
                CityIDs = objUserAccessrights.CityIDs;
            }
            if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
            {

                AccessType = 2;
                TCCodes = objUserAccessrights.TCValues;
                CityIDs = objUserAccessrights.CityIDs;

            }
            else
            {
                CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objComparision.CompanyID);
                if (objCompanyAccessRight != null)
                {
                    switch (objCompanyAccessRight.AccessRightType)
                    {
                        case 2:
                            AccessType = 2;
                            FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                            IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                            TCCodes = string.Join(",", TCCodesList);

                            break;
                        case 3:
                            AccessType = 2;
                            TCCodes = objCompanyAccessRight.TCValues;
                            break;
                    }
                }
            }

            objAccessRightsDataAccess = null;


            // Get Subscriped City By Company ID
            CityDataAccess objCityDataAccess = new CityDataAccess();
            IList<cityMaster> CityList = objCityDataAccess.GetCityListByCompany(objComparision.CompanyID, AccessType, CityIDs);
            objCityDataAccess = null;

            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            string param = string.Empty;
            if (objComparision != null)
            {

                BrandDataAccess objBrandDataAccessBP = new BrandDataAccess();
            resultds = objBrandDataAccessBP.GetBrandPerformanceReport(objComparision, AccessType, TCCodes);
            objBrandDataAccessBP = null;
            IList<Int64> CompanyListToadd = new List<Int64>();
            CompanyListToadd.Add(objComparision.BrandID);
                IList<CompanyPerformance> AllCityData = new List<CompanyPerformance>();
            if (resultds.Tables.Count > 0)
            {

                 AllCityData = (from row in resultds.Tables[0].AsEnumerable()
                              select new CompanyPerformance
                              {
                                  CompanyID = Convert.ToInt32(row["BrandID"]),
                                  CompanyName = Convert.ToString(row["BrandName"]),
                                  TotalSales = string.IsNullOrEmpty(objComparision.uomScale) ?  objComparision.uomType=="unit"? Convert.ToDouble(row["TotalUnit"]) : Convert.ToDouble(row["TotalSales"]) : objComparision.uomScale.ToLower() =="k"? objComparision.uomType == "unit" ? Math.Round(Convert.ToDouble(row["TotalUnit"]) / 1000,2) :  Math.Round(Convert.ToDouble(row["TotalSales"]) /1000,2) : objComparision.uomType == "unit" ? Math.Round(Convert.ToDouble(row["TotalUnit"]) / 10000000,2) : Math.Round(Convert.ToDouble(row["TotalSales"]) / 10000000,2),
                                  
                                  BrickID = Convert.ToInt32(row["BrickID"]),
                                  CityID = Convert.ToInt32(row["CityID"]),
                                  TimePeriod = Convert.ToInt32(row["TimePeriodID"]),
                                  PeriodName = Convert.ToString(row["TimePeriod"])
                              }
                              ).ToList();

                 }

                for (int S = 0; S < CityList.Count; S++)
                {
                    BrandComprsionPerformanceDetails objBrandComparsionPerformance = new BrandComprsionPerformanceDetails();
                    BrandComparsionPerformanceDetails objComparsionPerformance = new BrandComparsionPerformanceDetails();

                    int CurrentCityID = CityList[S].CityID;
                    objBrandComparsionPerformance.CityID = CurrentCityID;
                    objBrandComparsionPerformance.CityName = CityList[S].CityName;
                    BrickDataAccess objBrickDataaccess = new BrickDataAccess();
                    IList<BrickMaster> objBrickList = new List<BrickMaster>();
                    objBrickList = objBrickDataaccess.GetBrickListByCityID(CurrentCityID, objComparision.CompanyID, objComparision.DivisonID, "all");
                    objBrickDataaccess = null;


                    IList<CompanyPerformance> CompanyPerformanceList = new List<CompanyPerformance>();
                   
                    IList<SalesByPatch> objCompanyPerformancebyPatch = new List<SalesByPatch>();
                    
                    IList<CompanyPerformance> result = AllCityData.Where(a => a.CityID == CurrentCityID).ToList();
                    try
                    {

                        Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();                        
                        objComparsionPerformance.TotalSalesValue = TotalMarketSale;                     

                        var UniqueTimeperiodListData = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();

                        List<TimePeriod> UniqueTimeperiodList = UniqueTimeperiodListData.Select(a => new TimePeriod { MonthYear = a.PeriodName, TimePeriodID = a.TimePeriod }).OrderBy(a => a.TimePeriodID).ToList();

                        // Company  Graph Data
                        var CompanyResult = from r in result
                                            group r by new
                                            {
                                                r.CompanyID,
                                                r.CompanyName
                                            } into g
                                            select new CompanyPerformance
                                            {
                                                CompanyID = g.Key.CompanyID,
                                                CompanyName = g.Key.CompanyName,
                                                TotalSales = g.Sum(a => a.TotalSales)
                                            };

                        // Process PatchLevel Data

                        ArrayList BarChartArraySale = new ArrayList();                       

                        var TimePeriodResult = from r in result
                                               group r by new
                                               {
                                                   r.TimePeriod,
                                                   r.PeriodName
                                               } into g
                                               select new CompanyPerformance
                                               {
                                                   TimePeriod = g.Key.TimePeriod,
                                                   PeriodName = g.Key.PeriodName,
                                                   TotalSales = g.Sum(a => a.TotalSales)
                                               };


                        IList<BarChartData> BarChartSales = new List<BarChartData>();                     

                        CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                        IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objComparision.PeriodType.ToLower(), objComparision.PeriodStart, objComparision.PeriodEnd);
                        objCompanyDataaccessForTimeperiod = null;
                        for (int i = 0; i < TimePeriodlist.Count; i++)
                        {
                            CompanyPerformance objBarchartData = TimePeriodResult.Where(a => a.TimePeriod == TimePeriodlist[i].TimePeriodID).FirstOrDefault();
                            if (objBarchartData != null)
                            {
                                ArrayList CurrentObjectSale = new ArrayList();
                                CurrentObjectSale.Add(objBarchartData.PeriodName);
                                CurrentObjectSale.Add(objBarchartData.TotalSales);
                                BarChartArraySale.Add(CurrentObjectSale);
                                
                            }
                            else
                            {
                                ArrayList CurrentObjectSale = new ArrayList();
                                CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                CurrentObjectSale.Add(0);
                                BarChartArraySale.Add(CurrentObjectSale);
                                
                            }

                        }

                        BarChartData objChartDataSale = new BarChartData();
                        objChartDataSale.key = "All Brands*";
                        objChartDataSale.values = BarChartArraySale;
                        BarChartSales.Add(objChartDataSale);


                        var CompanyResultSales = CompanyResult.OrderByDescending(a => a.TotalSales).ToList();
                        objComparsionPerformance.CompanyPerformance = this.GetBrandPerformanceReportList(CompanyResultSales, CompanyListToadd, objComparision, 1);
                      
                        IList<Int64> TopSelectedCompanyValue = new List<Int64>();                        

                        TopSelectedCompanyValue = CompanyResultSales.Select(a => a.CompanyID).Take(10).ToList();
                        for (int i = 0; i < CompanyListToadd.Count; i++)
                        {
                            if (!TopSelectedCompanyValue.Contains(CompanyListToadd[i]))
                            {
                                TopSelectedCompanyValue.Add(CompanyListToadd[i]);
                            }
                            
                        }
                        // Company Table Data 
                        var CompanyResultTable = from r in result
                                                 group r by new
                                                 {
                                                     r.CompanyID,
                                                     r.CompanyName,
                                                     r.TimePeriod,
                                                     r.PeriodName
                                                 } into g
                                                 select new CompanyPerformance
                                                 {
                                                     CompanyID = g.Key.CompanyID,
                                                     CompanyName = g.Key.CompanyName,
                                                     PeriodName = g.Key.PeriodName,
                                                     TimePeriod = g.Key.TimePeriod,
                                                     TotalSales = g.Sum(a => a.TotalSales)
                                                    
                                                 };

                        // Company Tablel data ends

                        CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                        string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                        ColorCodeCompanyDataAccess = null;
                        Dictionary<Int64, string> ObjCompanyColorCodeBarchartSales = this.GetCompanyColorCode(TopSelectedCompanyValue, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                      
                        var CompanyResultSalesTable = CompanyResultTable.OrderByDescending(a => a.TotalSales).ToList();
                         string ColorCode = string.Empty;
                        for (int i = 0; i < TopSelectedCompanyValue.Count; i++)
                        {
                            ArrayList BarChartSaleArray = new ArrayList();

                            var TimePeriodCompanyData = CompanyResultSalesTable.Where(a => a.CompanyID == TopSelectedCompanyValue[i]).ToList();
                            if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                            {
                                string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                for (int j = 0; j < TimePeriodlist.Count; j++)
                                {
                                    CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                    if (objBarchartData != null)
                                    {
                                        ArrayList CurrentObjectSale = new ArrayList();
                                        CurrentObjectSale.Add(objBarchartData.PeriodName);
                                        CurrentObjectSale.Add(objBarchartData.TotalSales);
                                        BarChartSaleArray.Add(CurrentObjectSale);

                                    }
                                    else
                                    {
                                        ArrayList CurrentObjectSale = new ArrayList();
                                        CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                        CurrentObjectSale.Add(0);
                                        BarChartSaleArray.Add(CurrentObjectSale);
                                    }
                                }

                                BarChartData objChartComapnyDataSale = new BarChartData();
                                ColorCode = string.Empty;
                                if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                {
                                    objChartComapnyDataSale.ColorCode = ColorCode;

                                }
                                else
                                {
                                    objChartComapnyDataSale.ColorCode = ColorCode;
                                }


                                objChartComapnyDataSale.key = CompanyName;
                                objChartComapnyDataSale.values = BarChartSaleArray;
                                BarChartSales.Add(objChartComapnyDataSale);
                            }
                        }
                        objComparsionPerformance.BarChartSales = BarChartSales;
                        
                        objComparsionPerformance.CompanyPerformanceSalesTable = this.GetBrandPerformanceListReportTable(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                        
                        objComparsionPerformance.CompanyComparisonSales = new List<SalesByPatch>();
                        
                        objComparsionPerformance.CompanyPerformanceSalesPatchTable = new List<PatchDetailsTableList>();
                        
                        IList<Int64> Top5CompanySales = CompanyResult.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();


                        for (int i = 0; i < CompanyListToadd.Count; i++)
                        {
                            if (!Top5CompanySales.Contains(CompanyListToadd[i]))
                            {
                                Top5CompanySales.Add(CompanyListToadd[i]);
                            }
                        }

                        Dictionary<Int64, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(Top5CompanySales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                        string ProducttypeString = "brand";

                        objComparsionPerformance.CompanyComparisonSales = this.GetSalesPatchDataListBrand(objBrickList, result, Top5CompanySales, 0, ObjCompanyColorCodeSales, ProducttypeString);

                        IList<Int64> Top5CompanyUnit = CompanyResult.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();


                        for (int i = 0; i < CompanyListToadd.Count; i++)
                        {
                            if (!Top5CompanyUnit.Contains(CompanyListToadd[i]))
                            {
                                Top5CompanyUnit.Add(CompanyListToadd[i]);
                            }
                        }
                        Dictionary<Int64, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(Top5CompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                        for (int i = 0; i < objBrickList.Count; i++)
                        {
                            IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID).ToList();

                            var CompanyResultPatch = from r in ObjBrickResult
                                                     group r by new
                                                     {
                                                         r.CompanyID,
                                                         r.CompanyName
                                                     } into g
                                                     select new CompanyPerformance
                                                     {
                                                         CompanyID = g.Key.CompanyID,
                                                         CompanyName = g.Key.CompanyName,
                                                         TotalSales = g.Sum(a => a.TotalSales),
                                                         TotalUnits = g.Sum(a => a.TotalUnits)
                                                     };

                            IList<Int64> Top5CompanyPatchSales = CompanyResultPatch.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();




                            // Patch Table 
                            var CompanyResultPatchTable = from r in ObjBrickResult
                                                          group r by new
                                                          {
                                                              r.CompanyID,
                                                              r.CompanyName,
                                                              r.TimePeriod,
                                                              r.PeriodName
                                                          } into g
                                                          select new CompanyPerformance
                                                          {
                                                              CompanyID = g.Key.CompanyID,
                                                              CompanyName = g.Key.CompanyName,
                                                              PeriodName = g.Key.PeriodName,
                                                              TimePeriod = g.Key.TimePeriod,
                                                              TotalSales = g.Sum(a => a.TotalSales)                                                             
                                                          };

                            // Company Tablel data ends
                            var CompanyResultSalesPatchTable = CompanyResultPatchTable.OrderByDescending(a => a.TotalSales).ToList();
                            IList<TableList> TableDataSales = this.GetBrandPerformanceListReportTable(CompanyResultSalesPatchTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList);
                            
                            PatchDetailsTableList objCCPSalePatch = new PatchDetailsTableList()
                            {
                                PatchID = objBrickList[i].BrickID,
                                PatchName = objBrickList[i].BrickName,
                                TableData = TableDataSales
                            };
                            objComparsionPerformance.CompanyPerformanceSalesPatchTable.Add(objCCPSalePatch);
                           

                        }




                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    objBrandComparsionPerformance.CityComparsionData = objComparsionPerformance;
                    objBrandComparsionPerformanceList.Add(objBrandComparsionPerformance);
                }
                
            }

            return objBrandComparsionPerformanceList;
        }


        public IList<BrandComprsionPerformanceDetails> GetCompanyBrandPerformanceReportNew(BrandComparison_IVM objComparision)
        {


            IList<BrandComprsionPerformanceDetails> objBrandComparsionPerformanceList = new List<BrandComprsionPerformanceDetails>();
            // Validate the session 
            SessionDataAccess objSessionDataAccess = new SessionDataAccess();
            int UserID = objSessionDataAccess.ValidateSession(objComparision.SessionID);
            objSessionDataAccess = null;
            if (UserID == 0)
            {
                objBrandComparsionPerformanceList = null;
                return objBrandComparsionPerformanceList;
            }

            // Access log
            UserLog_VM objLogDetails = new UserLog_VM()
            {
                UserID = UserID,
                MenuID = 6,
                CityID = 0,
                CategoryType = "brand",
                CategoryValue = objComparision.BrandID.ToString(),
                DataType = objComparision.DataType == "npm" ? "ipm" : objComparision.DataType,
                EndPeriod = objComparision.PeriodEnd,
                StartPeriod = objComparision.PeriodStart,
                PatchID = "all",
                PeriodType = objComparision.PeriodType,
                TCCode = "all",
                TCLevel = "all",
                ProductID = "",
                SessionID = objComparision.SessionID


            };
            UserLogAccessDetails objUserLogAccessDetails = new UserLogAccessDetails();
            objUserLogAccessDetails.AddUpdateUserLog(objLogDetails);
            objUserLogAccessDetails = null;
            // Get UserAccessRights 

            AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
            UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

            int AccessType = 1;
            string TCCodes = string.Empty;
            string CityIDs = string.Empty;
            //   CityIDs = objUserAccessrights.CityIDs;
            if (objUserAccessrights != null && !string.IsNullOrEmpty(objUserAccessrights.CityIDs))
            {
                CityIDs = objUserAccessrights.CityIDs;
            }
            if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
            {

                AccessType = 2;
                TCCodes = objUserAccessrights.TCValues;
                CityIDs = objUserAccessrights.CityIDs;

            }
            else
            {
                CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objComparision.CompanyID);
                if (objCompanyAccessRight != null)
                {
                    switch (objCompanyAccessRight.AccessRightType)
                    {
                        case 2:
                            AccessType = 2;
                            FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                            IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                            TCCodes = string.Join(",", TCCodesList);

                            break;
                        case 3:
                            AccessType = 2;
                            TCCodes = objCompanyAccessRight.TCValues;
                            break;
                    }
                }
            }

            objAccessRightsDataAccess = null;


            // Get Subscriped City By Company ID
            CityDataAccess objCityDataAccess = new CityDataAccess();
            IList<cityMaster> CityList = objCityDataAccess.GetCityListByCompany(objComparision.CompanyID, AccessType, CityIDs);
            objCityDataAccess = null;

            DataSet resultds = new DataSet();
            DALUtility IMSBIDB = new DALUtility();
            string param = string.Empty;
            if (objComparision != null)
            {

                CompanyDataAccess objCompanyDataaccessForTimeperiod = new CompanyDataAccess();
                IList<TimePeriodData> TimePeriodlist = objCompanyDataaccessForTimeperiod.GetTimePeriodDetails(objComparision.PeriodType.ToLower(), objComparision.PeriodStart, objComparision.PeriodEnd);
                objCompanyDataaccessForTimeperiod = null;


                IList<Int64> CVMProductList = new List<Int64>();
                if (objComparision.DataType == "cvm")
                {
                    BrandDataAccess objBrandDataAccessCM = new BrandDataAccess();
                    if (string.IsNullOrEmpty(objComparision.SKUIDs))
                    {
                        CVMProductList = objBrandDataAccessCM.GetCVMProductlist(objComparision.BrandID.ToString(), "brand");
                    }
                    else
                    {
                        CVMProductList = objBrandDataAccessCM.GetCVMProductlist(objComparision.SKUIDs, "sku");
                    }
                    objBrandDataAccessCM = null;
                }

                for (int S = 0; S < CityList.Count; S++)
                {

                    BrandDataAccess objBrandDataAccessBP = new BrandDataAccess();
                    resultds = objBrandDataAccessBP.GetBrandPerformanceReportNew(objComparision, AccessType, TCCodes, CityList[S].CityID, CVMProductList);
                    objBrandDataAccessBP = null;
                    IList<Int64> CompanyListToadd = new List<Int64>();
                    CompanyListToadd.Add(objComparision.BrandID);
                    IList<CompanyPerformance> AllCityData = new List<CompanyPerformance>();
                    IList<TimePeriodSalesData> TimePeriodSalesData = new List<TimePeriodSalesData>();
                    if (resultds.Tables.Count > 0)
                    {

                        AllCityData = (from row in resultds.Tables[0].AsEnumerable()
                                       select new CompanyPerformance
                                       {
                                           CompanyID = Convert.ToInt32(row["BrandID"]),
                                           CompanyName = Convert.ToString(row["BrandName"]),
                                           TotalSales =  objComparision.uomType == "unit" ? Convert.ToDouble(row["TotalUnit"]) : Convert.ToDouble(row["TotalSales"]) ,

                                           BrickID = Convert.ToInt32(row["BrickID"]),
                                           CityID = Convert.ToInt32(row["CityID"]),
                                           TimePeriod = Convert.ToInt32(row["TimePeriodID"]),
                                           PeriodName = Convert.ToString(row["TimePeriod"])
                                       }
                                     ).ToList();


                        TimePeriodSalesData = (from row in resultds.Tables[1].AsEnumerable()
                                       select new TimePeriodSalesData
                                       {
                                         
                                           TimePeriodID = Convert.ToInt32(row["TimePeriodID"]),
                                           BrickID = Convert.ToInt32(row["BrickID"]),
                                           TotalSales =  objComparision.uomType == "unit" ? Convert.ToDouble(row["TotalUnit"]) : Convert.ToDouble(row["TotalSale"]) ,
                                       }
                                     ).ToList();



                        BrandComprsionPerformanceDetails objBrandComparsionPerformance = new BrandComprsionPerformanceDetails();
                        BrandComparsionPerformanceDetails objComparsionPerformance = new BrandComparsionPerformanceDetails();

                        int CurrentCityID = CityList[S].CityID;
                        objBrandComparsionPerformance.CityID = CurrentCityID;
                        objBrandComparsionPerformance.CityName = CityList[S].CityName;
                        BrickDataAccess objBrickDataaccess = new BrickDataAccess();
                        IList<BrickMaster> objBrickList = new List<BrickMaster>();
                        objBrickList = objBrickDataaccess.GetBrickListByCityID(CurrentCityID, objComparision.CompanyID, objComparision.DivisonID, "all");
                        objBrickDataaccess = null;


                        IList<CompanyPerformance> CompanyPerformanceList = new List<CompanyPerformance>();

                        IList<SalesByPatch> objCompanyPerformancebyPatch = new List<SalesByPatch>();

                       // IList<CompanyPerformance> result = AllCityData.Where(a => a.CityID == CurrentCityID).ToList();
                        try
                        {

                            Double TotalMarketSale = TimePeriodSalesData.Select(a => a.TotalSales).Sum();
                            objComparsionPerformance.TotalSalesValue = string.IsNullOrEmpty(objComparision.uomScale)? TotalMarketSale : objComparision.uomScale.ToLower() == "k" ? Math.Round(TotalMarketSale/1000,2):Math.Round(TotalMarketSale/ 10000000, 2);

                            var UniqueTimeperiodListData = AllCityData.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();

                            List<TimePeriod> UniqueTimeperiodList = UniqueTimeperiodListData.Select(a => new TimePeriod { MonthYear = a.PeriodName, TimePeriodID = a.TimePeriod }).OrderBy(a => a.TimePeriodID).ToList();

                            // Company  Graph Data
                            var CompanyResult = from r in AllCityData
                                                group r by new
                                                {
                                                    r.CompanyID,
                                                    r.CompanyName
                                                } into g
                                                select new CompanyPerformance
                                                {
                                                    CompanyID = g.Key.CompanyID,
                                                    CompanyName = g.Key.CompanyName,
                                                    TotalSales = g.Sum(a => a.TotalSales)
                                                };

                            // Process PatchLevel Data

                            ArrayList BarChartArraySale = new ArrayList();

                          


                            IList<BarChartData> BarChartSales = new List<BarChartData>();

                            var TimePeriodSalesDataCIty = from r in TimePeriodSalesData
                                                          group r by new
                                                {
                                                    r.TimePeriodID,
                                                    
                                                } into g
                                                select new TimePeriodSalesData
                                                {
                                                    TimePeriodID = g.Key.TimePeriodID,                                                   
                                                    TotalSales = g.Sum(a => a.TotalSales)
                                                };



                            for (int i = 0; i < TimePeriodlist.Count; i++)
                            {
                                TimePeriodSalesData objBarchartData = TimePeriodSalesDataCIty.Where(a => a.TimePeriodID == TimePeriodlist[i].TimePeriodID).FirstOrDefault();
                                if (objBarchartData != null)
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectSale.Add(string.IsNullOrEmpty(objComparision.uomScale) ? objBarchartData.TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(objBarchartData.TotalSales / 1000, 2) : Math.Round(objBarchartData.TotalSales / 10000000, 2));
                                    BarChartArraySale.Add(CurrentObjectSale);

                                }
                                else
                                {
                                    ArrayList CurrentObjectSale = new ArrayList();
                                    CurrentObjectSale.Add(TimePeriodlist[i].MonthName);
                                    CurrentObjectSale.Add(0);
                                    BarChartArraySale.Add(CurrentObjectSale);

                                }

                            }

                            BarChartData objChartDataSale = new BarChartData();
                            objChartDataSale.key = "All Brands*";
                            objChartDataSale.values = BarChartArraySale;
                            BarChartSales.Add(objChartDataSale);


                            var CompanyResultSales = CompanyResult.OrderByDescending(a => a.TotalSales).ToList();
                            objComparsionPerformance.CompanyPerformance = this.GetBrandPerformanceReportList(CompanyResultSales, CompanyListToadd, objComparision, 1);

                            IList<Int64> TopSelectedCompanyValue = new List<Int64>();

                            TopSelectedCompanyValue = CompanyResultSales.Select(a => a.CompanyID).Take(10).ToList();
                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!TopSelectedCompanyValue.Contains(CompanyListToadd[i]))
                                {
                                    TopSelectedCompanyValue.Add(CompanyListToadd[i]);
                                }

                            }
                            // Company Table Data 
                            var CompanyResultTable = from r in AllCityData
                                                     group r by new
                                                     {
                                                         r.CompanyID,
                                                         r.CompanyName,
                                                         r.TimePeriod,
                                                         r.PeriodName
                                                     } into g
                                                     select new CompanyPerformance
                                                     {
                                                         CompanyID = g.Key.CompanyID,
                                                         CompanyName = g.Key.CompanyName,
                                                         PeriodName = g.Key.PeriodName,
                                                         TimePeriod = g.Key.TimePeriod,
                                                         TotalSales = g.Sum(a => a.TotalSales)

                                                     };

                            // Company Tablel data ends

                            CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                            string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                            ColorCodeCompanyDataAccess = null;
                            Dictionary<Int64, string> ObjCompanyColorCodeBarchartSales = this.GetCompanyColorCode(TopSelectedCompanyValue, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);

                            var CompanyResultSalesTable = CompanyResultTable.OrderByDescending(a => a.TotalSales).ToList();
                            string ColorCode = string.Empty;
                            for (int i = 0; i < TopSelectedCompanyValue.Count; i++)
                            {
                                ArrayList BarChartSaleArray = new ArrayList();

                                var TimePeriodCompanyData = CompanyResultSalesTable.Where(a => a.CompanyID == TopSelectedCompanyValue[i]).ToList();
                                if (TimePeriodCompanyData != null && TimePeriodCompanyData.Count > 0)
                                {
                                    string CompanyName = TimePeriodCompanyData[0].CompanyName;
                                    Int64 CompanyID = TimePeriodCompanyData[0].CompanyID;
                                    for (int j = 0; j < TimePeriodlist.Count; j++)
                                    {
                                        CompanyPerformance objBarchartData = TimePeriodCompanyData.Where(a => a.TimePeriod == TimePeriodlist[j].TimePeriodID).FirstOrDefault();
                                        if (objBarchartData != null)
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(objBarchartData.PeriodName);
                                            CurrentObjectSale.Add(string.IsNullOrEmpty(objComparision.uomScale) ? objBarchartData.TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(objBarchartData.TotalSales / 1000, 2) : Math.Round(objBarchartData.TotalSales / 10000000, 2));
                                            BarChartSaleArray.Add(CurrentObjectSale);

                                        }
                                        else
                                        {
                                            ArrayList CurrentObjectSale = new ArrayList();
                                            CurrentObjectSale.Add(TimePeriodlist[j].MonthName);
                                            CurrentObjectSale.Add(0);
                                            BarChartSaleArray.Add(CurrentObjectSale);
                                        }
                                    }

                                    BarChartData objChartComapnyDataSale = new BarChartData();
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCodeBarchartSales.TryGetValue(CompanyID, out ColorCode))
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;

                                    }
                                    else
                                    {
                                        objChartComapnyDataSale.ColorCode = ColorCode;
                                    }


                                    objChartComapnyDataSale.key = CompanyName;
                                    objChartComapnyDataSale.values = BarChartSaleArray;
                                    BarChartSales.Add(objChartComapnyDataSale);
                                }
                            }
                            objComparsionPerformance.BarChartSales = BarChartSales;

                            objComparsionPerformance.CompanyPerformanceSalesTable = this.GetBrandPerformanceListReportTableNew(CompanyResultSalesTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList, TimePeriodSalesData);

                            objComparsionPerformance.CompanyComparisonSales = new List<SalesByPatch>();

                            objComparsionPerformance.CompanyPerformanceSalesPatchTable = new List<PatchDetailsTableList>();

                            IList<Int64> Top5CompanySales = CompanyResult.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();


                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!Top5CompanySales.Contains(CompanyListToadd[i]))
                                {
                                    Top5CompanySales.Add(CompanyListToadd[i]);
                                }
                            }

                            Dictionary<Int64, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(Top5CompanySales, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                            string ProducttypeString = "brand";

                            objComparsionPerformance.CompanyComparisonSales = this.GetSalesPatchDataListBrandNew(objBrickList, AllCityData, Top5CompanySales, 0, ObjCompanyColorCodeSales, ProducttypeString, objComparision);

                            IList<Int64> Top5CompanyUnit = CompanyResult.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).Distinct().Take(5).ToList();


                            for (int i = 0; i < CompanyListToadd.Count; i++)
                            {
                                if (!Top5CompanyUnit.Contains(CompanyListToadd[i]))
                                {
                                    Top5CompanyUnit.Add(CompanyListToadd[i]);
                                }
                            }
                            Dictionary<Int64, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(Top5CompanyUnit, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                            for (int i = 0; i < objBrickList.Count; i++)
                            {
                                IList<CompanyPerformance> ObjBrickResult = AllCityData.Where(a => a.BrickID == objBrickList[i].BrickID).ToList();

                                var CompanyResultPatch = from r in ObjBrickResult
                                                         group r by new
                                                         {
                                                             r.CompanyID,
                                                             r.CompanyName
                                                         } into g
                                                         select new CompanyPerformance
                                                         {
                                                             CompanyID = g.Key.CompanyID,
                                                             CompanyName = g.Key.CompanyName,
                                                             TotalSales = g.Sum(a => a.TotalSales),
                                                             TotalUnits = g.Sum(a => a.TotalUnits)
                                                         };

                                IList<Int64> Top5CompanyPatchSales = CompanyResultPatch.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).Distinct().Take(5).ToList();




                                // Patch Table 
                                var CompanyResultPatchTable = from r in ObjBrickResult
                                                              group r by new
                                                              {
                                                                  r.CompanyID,
                                                                  r.CompanyName,
                                                                  r.TimePeriod,
                                                                  r.PeriodName
                                                              } into g
                                                              select new CompanyPerformance
                                                              {
                                                                  CompanyID = g.Key.CompanyID,
                                                                  CompanyName = g.Key.CompanyName,
                                                                  PeriodName = g.Key.PeriodName,
                                                                  TimePeriod = g.Key.TimePeriod,
                                                                  TotalSales = g.Sum(a => a.TotalSales)
                                                              };

                                // Company Tablel data ends
                                var CompanyResultSalesPatchTable = CompanyResultPatchTable.OrderByDescending(a => a.TotalSales).ToList();
                                IList<TableList> TableDataSales = this.GetBrandPerformanceListReportTableNew(CompanyResultSalesPatchTable, CompanyListToadd, objComparision, 0, 0, UniqueTimeperiodList, TimePeriodSalesData.Where(a=>a.BrickID == objBrickList[i].BrickID).ToList());

                                PatchDetailsTableList objCCPSalePatch = new PatchDetailsTableList()
                                {
                                    PatchID = objBrickList[i].BrickID,
                                    PatchName = objBrickList[i].BrickName,
                                    TableData = TableDataSales
                                };
                                objComparsionPerformance.CompanyPerformanceSalesPatchTable.Add(objCCPSalePatch);



                            }




                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        objBrandComparsionPerformance.CityComparsionData = objComparsionPerformance;
                        objBrandComparsionPerformanceList.Add(objBrandComparsionPerformance);
                    }
                }
            }

            return objBrandComparsionPerformanceList;
        }

        public IList<SalesByPatch> GetSalesPatchDataList(IList<BrickMaster> objBrickList, IList<CompanyPerformance> result, IList<Int64> Top5CompanySales, int Type, Dictionary<Int64, string> ObjCompanyColorCode, string FilterType)
        {
            IList<SalesByPatch> ObjSalesPatchDataList = new List<SalesByPatch>();
            CompanyDataAccess objCompanyDetails = new CompanyDataAccess();
            IList<CompanyDetails> SelectedCompanyDetails = objCompanyDetails.GetZeroValueCompanyDetails(string.Join(",", Top5CompanySales), FilterType);
            objCompanyDetails = null;
            string ColorCode = string.Empty;
            for (int j = 0; j < SelectedCompanyDetails.Count; j++)
            {
                SalesByPatch objSalesPatchSales = new SalesByPatch();
                objSalesPatchSales.key = SelectedCompanyDetails[j].CompanyName;
                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(SelectedCompanyDetails[j].CompanyID, out ColorCode))
                {
                    objSalesPatchSales.ColorCode = ColorCode;

                }
                else
                {
                    objSalesPatchSales.ColorCode = ColorCode;
                }
                IList<ValueDataSalesPatch> CompanyPatchDetailList = new List<ValueDataSalesPatch>();
                for (int i = 0; i < objBrickList.Count; i++)
                {
                    IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID && a.CompanyID == SelectedCompanyDetails[j].CompanyID).ToList();

                    if (ObjBrickResult != null && ObjBrickResult.Count > 0)
                    {
                        var CompanyResultPatch = from r in ObjBrickResult
                                                 group r by new
                                                 {
                                                     r.CompanyID,
                                                     r.CompanyName
                                                 } into g
                                                 select new CompanyPerformance
                                                 {
                                                     CompanyID = g.Key.CompanyID,
                                                     CompanyName = g.Key.CompanyName,
                                                     TotalSales = g.Sum(a => a.TotalSales),
                                                     TotalUnits = g.Sum(a => a.TotalUnits)
                                                 };


                        var CompanyResultSalesPatch = CompanyResultPatch.OrderByDescending(a => a.TotalSales).FirstOrDefault();
                        if (Type == 0)
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = CompanyResultSalesPatch.TotalSales,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                        else
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = CompanyResultSalesPatch.TotalUnits,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                    }
                    else
                    {
                        ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                        {
                            x = objBrickList[i].BrickName,
                            y = 0,
                            z = objBrickList[i].BrickID
                        };

                        CompanyPatchDetailList.Add(objValueDataSalesPatch);
                    }


                }
                objSalesPatchSales.values = CompanyPatchDetailList;
                ObjSalesPatchDataList.Add(objSalesPatchSales);
            }

            return ObjSalesPatchDataList;
        }


        public IList<SalesByPatch> GetSalesPatchDataListBrand(IList<BrickMaster> objBrickList, IList<CompanyPerformance> result, IList<Int64> Top5CompanySales, int Type, Dictionary<Int64, string> ObjCompanyColorCode, string ProductType)
        {
            IList<SalesByPatch> ObjSalesPatchDataList = new List<SalesByPatch>();
            ProductDataAccess objCompanyDataAccess = new ProductDataAccess();

            IList<BrandDetails> SelectedCompanyDetails = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", Top5CompanySales), ProductType);
            objCompanyDataAccess = null;

            string ColorCode = string.Empty;
            for (int j = 0; j < SelectedCompanyDetails.Count; j++)
            {
                SalesByPatch objSalesPatchSales = new SalesByPatch();
                objSalesPatchSales.key = SelectedCompanyDetails[j].Brand;
                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(SelectedCompanyDetails[j].BrandID, out ColorCode))
                {
                    objSalesPatchSales.ColorCode = ColorCode;

                }
                else
                {
                    objSalesPatchSales.ColorCode = ColorCode;
                }
                IList<ValueDataSalesPatch> CompanyPatchDetailList = new List<ValueDataSalesPatch>();
                for (int i = 0; i < objBrickList.Count; i++)
                {
                    IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID && a.CompanyID == SelectedCompanyDetails[j].BrandID).ToList();

                    if (ObjBrickResult != null && ObjBrickResult.Count > 0)
                    {
                        var CompanyResultPatch = from r in ObjBrickResult
                                                 group r by new
                                                 {
                                                     r.CompanyID,
                                                     r.CompanyName
                                                 } into g
                                                 select new CompanyPerformance
                                                 {
                                                     CompanyID = g.Key.CompanyID,
                                                     CompanyName = g.Key.CompanyName,
                                                     TotalSales = g.Sum(a => a.TotalSales),
                                                     TotalUnits = g.Sum(a => a.TotalUnits)
                                                 };


                        var CompanyResultSalesPatch = CompanyResultPatch.OrderByDescending(a => a.TotalSales).FirstOrDefault();
                        if (Type == 0)
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = CompanyResultSalesPatch.TotalSales,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                        else
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = CompanyResultSalesPatch.TotalUnits,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                    }
                    else
                    {
                        ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                        {
                            x = objBrickList[i].BrickName,
                            y = 0,
                            z = objBrickList[i].BrickID
                        };

                        CompanyPatchDetailList.Add(objValueDataSalesPatch);
                    }


                }
                objSalesPatchSales.values = CompanyPatchDetailList;
                ObjSalesPatchDataList.Add(objSalesPatchSales);
            }

            return ObjSalesPatchDataList;
        }

        public IList<SalesByPatch> GetSalesPatchDataListBrandNew(IList<BrickMaster> objBrickList, IList<CompanyPerformance> result, IList<Int64> Top5CompanySales, int Type, Dictionary<Int64, string> ObjCompanyColorCode, string ProductType, BrandComparison_IVM objComparision)
        {
            IList<SalesByPatch> ObjSalesPatchDataList = new List<SalesByPatch>();
            ProductDataAccess objCompanyDataAccess = new ProductDataAccess();

            IList<BrandDetails> SelectedCompanyDetails = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", Top5CompanySales), ProductType);
            objCompanyDataAccess = null;

            string ColorCode = string.Empty;
            for (int j = 0; j < SelectedCompanyDetails.Count; j++)
            {
                SalesByPatch objSalesPatchSales = new SalesByPatch();
                objSalesPatchSales.key = SelectedCompanyDetails[j].Brand;
                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(SelectedCompanyDetails[j].BrandID, out ColorCode))
                {
                    objSalesPatchSales.ColorCode = ColorCode;

                }
                else
                {
                    objSalesPatchSales.ColorCode = ColorCode;
                }
                IList<ValueDataSalesPatch> CompanyPatchDetailList = new List<ValueDataSalesPatch>();
                for (int i = 0; i < objBrickList.Count; i++)
                {
                    IList<CompanyPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID && a.CompanyID == SelectedCompanyDetails[j].BrandID).ToList();

                    if (ObjBrickResult != null && ObjBrickResult.Count > 0)
                    {
                        var CompanyResultPatch = from r in ObjBrickResult
                                                 group r by new
                                                 {
                                                     r.CompanyID,
                                                     r.CompanyName
                                                 } into g
                                                 select new CompanyPerformance
                                                 {
                                                     CompanyID = g.Key.CompanyID,
                                                     CompanyName = g.Key.CompanyName,
                                                     TotalSales = g.Sum(a => a.TotalSales),
                                                     //TotalUnits = g.Sum(a => a.TotalUnits)
                                                 };


                        var CompanyResultSalesPatch = CompanyResultPatch.OrderByDescending(a => a.TotalSales).FirstOrDefault();
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = string.IsNullOrEmpty(objComparision.uomScale) ? CompanyResultSalesPatch.TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(CompanyResultSalesPatch.TotalSales / 1000, 2) : Math.Round(CompanyResultSalesPatch.TotalSales / 10000000, 2), 
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                       
                    }
                    else
                    {
                        ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                        {
                            x = objBrickList[i].BrickName,
                            y = 0,
                            z = objBrickList[i].BrickID
                        };

                        CompanyPatchDetailList.Add(objValueDataSalesPatch);
                    }


                }
                objSalesPatchSales.values = CompanyPatchDetailList;
                ObjSalesPatchDataList.Add(objSalesPatchSales);
            }

            return ObjSalesPatchDataList;
        }

        public IList<SalesByPatch> GetSalesPatchDataListBrandReport(IList<BrickMaster> objBrickList, IList<BrandPerformance> result, IList<Int64> Top5CompanySales, int Type, Dictionary<Int64, string> ObjCompanyColorCode, string ProductType)
        {
            IList<SalesByPatch> ObjSalesPatchDataList = new List<SalesByPatch>();
            ProductDataAccess objCompanyDataAccess = new ProductDataAccess();

            IList<BrandDetails> SelectedCompanyDetails = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", Top5CompanySales), ProductType);
            objCompanyDataAccess = null;
            SelectedCompanyDetails.Add(new BrandDetails { Brand = "Others", BrandID = 0 });
            string ColorCode = string.Empty;
            for (int j = 0; j < SelectedCompanyDetails.Count; j++)
            {
                SalesByPatch objSalesPatchSales = new SalesByPatch();
                objSalesPatchSales.key = SelectedCompanyDetails[j].Brand;
                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(SelectedCompanyDetails[j].BrandID, out ColorCode))
                {
                    objSalesPatchSales.ColorCode = ColorCode;

                }
                else
                {
                    objSalesPatchSales.ColorCode = ColorCode;
                }
                IList<ValueDataSalesPatch> CompanyPatchDetailList = new List<ValueDataSalesPatch>();
                for (int i = 0; i < objBrickList.Count; i++)
                {
                    if (objBrickList[i].BrickName.ToLower() != "unspecified")
                    {
                        IList<BrandPerformance> ObjBrickResult = result.Where(a => a.BrickID == objBrickList[i].BrickID && a.BrandID == SelectedCompanyDetails[j].BrandID).ToList();

                        if (ObjBrickResult != null && ObjBrickResult.Count > 0)
                        {
                            var BrandResultPatch = from r in ObjBrickResult
                                                   group r by new
                                                   {
                                                       r.BrandID,
                                                       r.BrandName
                                                   } into g
                                                   select new BrandPerformance
                                                   {
                                                       BrandID = g.Key.BrandID,
                                                       BrandName = g.Key.BrandName,
                                                       TotalSales = g.Sum(a => a.TotalSales),
                                                       TotalUnits = g.Sum(a => a.TotalUnits)
                                                   };


                            var CompanyResultSalesPatch = BrandResultPatch.OrderByDescending(a => a.TotalSales).FirstOrDefault();
                            if (Type == 0)
                            {
                                ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                                {
                                    x = objBrickList[i].BrickName,
                                    y = CompanyResultSalesPatch.TotalSales,
                                    z = objBrickList[i].BrickID
                                };

                                CompanyPatchDetailList.Add(objValueDataSalesPatch);
                            }
                            else
                            {
                                ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                                {
                                    x = objBrickList[i].BrickName,
                                    y = CompanyResultSalesPatch.TotalUnits,
                                    z = objBrickList[i].BrickID
                                };

                                CompanyPatchDetailList.Add(objValueDataSalesPatch);
                            }
                        }
                        else
                        {
                            ValueDataSalesPatch objValueDataSalesPatch = new ValueDataSalesPatch()
                            {
                                x = objBrickList[i].BrickName,
                                y = 0,
                                z = objBrickList[i].BrickID
                            };

                            CompanyPatchDetailList.Add(objValueDataSalesPatch);
                        }
                    }


                }
                objSalesPatchSales.values = CompanyPatchDetailList;
                ObjSalesPatchDataList.Add(objSalesPatchSales);
            }

            return ObjSalesPatchDataList;
        }



        public IList<CompanyPerformance> GetCompanyPerformanceList(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, Comparison_IVM objComparision, Int32 Type)
        {
            IList<CompanyPerformance> CompanyPerformanceList = new List<CompanyPerformance>();
            try
            {
                IList<Int64> AlreadyAddedCompany = new List<Int64>();

                // Code For color coding
                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();

                IList<Int64> UniqueCompanyList = result.Select(a => a.CompanyID).Take(10).ToList();

                // Top 5 Company by Sales
                IList<Int64> Top5CompanyBySale = result.Select(a => a.CompanyID).Take(5).ToList();
                UniqueCompanyList.Add(0);
                if (!objComparision.includeTopSearches)
                {
                    Top5CompanyBySale = new List<Int64>();

                }
                for (int k = 0; k < CompanyListToadd.Count; k++)
                {
                    if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                    {
                        UniqueCompanyList.Add(CompanyListToadd[k]);
                    }
                    if (!Top5CompanyBySale.Contains(CompanyListToadd[k]))
                    {
                        Top5CompanyBySale.Add(CompanyListToadd[k]);
                    }

                }

                CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                ColorCodeCompanyDataAccess = null;
                Dictionary<Int64, string> ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, Type);

                Dictionary<Int64, string> ObjCompanyColorCodePatchBySale = this.GetCompanyColorCode(Top5CompanyBySale, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, Type);

                Double OtherCompanySales = 0;
                Double OtherCompanyUnits = 0;
                string ColorCode = string.Empty;
                for (int i = 0; i < result.Count; i++)
                {
                    if (objComparision.includeTopSearches)
                    {
                        if (i < 10)
                        {
                            AlreadyAddedCompany.Add(result[i].CompanyID);
                            CompanyPerformance singleEntry = new CompanyPerformance();

                            singleEntry.Rank = i + 1;
                            singleEntry.CompanyID = result[i].CompanyID;
                            singleEntry.TotalSales = result[i].TotalSales;
                            singleEntry.TotalUnits = result[i].TotalUnits;
                            singleEntry.CompanyName = result[i].CompanyName;
                            singleEntry.MarketShare = Math.Round(((result[i].TotalSales / TotalMarketSale) * 100), 2);
                            singleEntry.MarketUnitShare = Math.Round(((result[i].TotalUnits / TotalMarketUnit) * 100), 2);
                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(result[i].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }
                            CompanyPerformanceList.Add(singleEntry);
                        }
                    }

                    if (CompanyListToadd.Contains(result[i].CompanyID))
                    {
                        if (!AlreadyAddedCompany.Contains(result[i].CompanyID))
                        {
                            CompanyPerformance singleEntry = new CompanyPerformance();
                            singleEntry.Rank = i + 1;
                            singleEntry.CompanyName = result[i].CompanyName;
                            singleEntry.CompanyID = result[i].CompanyID;
                            singleEntry.TotalSales = result[i].TotalSales;
                            singleEntry.TotalUnits = result[i].TotalUnits;
                            singleEntry.MarketUnitShare = Math.Round(((result[i].TotalUnits / TotalMarketUnit) * 100), 2);

                            singleEntry.MarketShare = Math.Round(((result[i].TotalSales / TotalMarketSale) * 100), 2);

                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(result[i].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }

                            CompanyPerformanceList.Add(singleEntry);
                            AlreadyAddedCompany.Add(result[i].CompanyID);
                        }

                    }
                    else
                    {
                        if (!AlreadyAddedCompany.Contains(result[i].CompanyID))
                        {
                            OtherCompanySales += result[i].TotalSales;
                            OtherCompanyUnits += result[i].TotalUnits;
                        }
                    }


                }
                IList<Int64> ZeroValueCompanyList = new List<Int64>();
                for (int j = 0; j < CompanyListToadd.Count; j++)
                {
                    if (!AlreadyAddedCompany.Contains(CompanyListToadd[j]))
                    {
                        ZeroValueCompanyList.Add(CompanyListToadd[j]);
                    }

                }

                // Get Zero Value Company  Details
                IList<CompanyDetails> ZeroValueCompanyDetailsForSales = new List<CompanyDetails>();
                if (ZeroValueCompanyList.Count > 0)
                {
                    CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                    ZeroValueCompanyDetailsForSales = objCompanyDataAccess.GetZeroValueCompanyDetails(string.Join(",", ZeroValueCompanyList), objComparision.ComparisonType);
                    objCompanyDataAccess = null;
                    if (ZeroValueCompanyDetailsForSales.Count > 0)
                    {
                        for (int z = 0; z < ZeroValueCompanyDetailsForSales.Count; z++)
                        {
                            CompanyPerformance singleEntry = new CompanyPerformance();
                            singleEntry.Rank = 0;
                            singleEntry.CompanyName = ZeroValueCompanyDetailsForSales[z].CompanyName;
                            singleEntry.CompanyID = ZeroValueCompanyDetailsForSales[z].CompanyID;
                            singleEntry.TotalSales = 0;
                            singleEntry.TotalUnits = 0;
                            singleEntry.MarketUnitShare = 0;
                            singleEntry.MarketShare = 0;


                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(ZeroValueCompanyDetailsForSales[z].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }
                            CompanyPerformanceList.Add(singleEntry);
                        }
                    }
                }



                CompanyPerformance singleEntryOther = new CompanyPerformance();
                singleEntryOther.Rank = 0;
                singleEntryOther.CompanyName = "Others";
                singleEntryOther.CompanyID = 0;
                singleEntryOther.TotalSales = OtherCompanySales;
                singleEntryOther.TotalUnits = OtherCompanyUnits;
                singleEntryOther.MarketUnitShare = Math.Round((OtherCompanyUnits / TotalMarketUnit) * 100);
                singleEntryOther.MarketShare = Math.Round((OtherCompanySales / TotalMarketSale) * 100);

                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(0, out ColorCode))
                {
                    singleEntryOther.ColorCode = ColorCode;

                }
                else
                {
                    singleEntryOther.ColorCode = ColorCode;
                }

                CompanyPerformanceList.Add(singleEntryOther);

            }
            catch
            {
                throw;
            }

            return CompanyPerformanceList;
        }

        public IList<TableList> GetCompanyPerformanceListTable(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, Comparison_IVM objComparision, Int32 Type, Int32 DataType, List<TimePeriod> UniqueTimeperiodList)
        {
            IList<TableList> CompanyPerformanceTableList = new List<TableList>();
            try
            {
                IList<Int32> AlreadyAddedCompany = new List<Int32>();

                // Code For color coding
                var CopmpanySales = from r in result
                                    group r by new
                                    {
                                        r.CompanyID,
                                        r.CompanyName
                                    } into g
                                    select new CompanyPerformance
                                    {
                                        CompanyID = g.Key.CompanyID,
                                        CompanyName = g.Key.CompanyName,
                                        TotalSales = g.Sum(a => a.TotalSales),
                                        TotalUnits = g.Sum(a => a.TotalUnits)
                                    };


                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                IList<Int64> UniqueCompanyList = new List<Int64>();
                IList<Int64> UniqueCompanyListData = new List<Int64>();

                if (DataType == 0)
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).ToList();

                }
                else
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).ToList();

                }
                Dictionary<Int64, Int32> CompanyRankDetails = new Dictionary<Int64, Int32>();
                for(int i=0; i< UniqueCompanyListData.Count; i++)
                {
                    if (objComparision.includeTopSearches)
                    {

                        if (i < 10)
                        {
                            if (!UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                            {
                                UniqueCompanyList.Add(UniqueCompanyListData[i]);

                            }
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }

                        }
                    }
                    if (CompanyListToadd.Count > 0)
                    {
                        if (CompanyListToadd.Contains(UniqueCompanyListData[i]) && !UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                        {
                            UniqueCompanyList.Add(UniqueCompanyListData[i]);
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }
                        }
                    }
                }

               // UniqueCompanyList.Add(0);

              

              //  var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                IList<CompanyDetails> CompanyDetailsList = objCompanyDataAccess.GetZeroValueCompanyDetails(string.Join(",", UniqueCompanyList), objComparision.ComparisonType);
                objCompanyDataAccess = null;

                Dictionary<Int32, Double> TotalPeriodDataValue = new Dictionary<int, double>();

                for (int j = 0; j < UniqueCompanyList.Count; j++)
                {
                    TableList objTableDataList = new TableList();
                    objTableDataList.ID = UniqueCompanyList[j];
                    CompanyDetails objCompanyDetails = CompanyDetailsList.Where(a => a.CompanyID == UniqueCompanyList[j]).FirstOrDefault();
                    if (objCompanyDetails != null)
                    {
                        objTableDataList.Name = objCompanyDetails.CompanyName;
                    }

                    Int32 RankCounter = 0;
                    if (CompanyRankDetails.TryGetValue(UniqueCompanyList[j], out RankCounter))
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    else
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                       
                    IList<CompanyPerformance> CompanyTimeperiodData = result.Where(a => a.CompanyID == UniqueCompanyList[j]).ToList();
                    IList<MonthData> objMonthDataList = new List<MonthData>();
                    Double TotalSales = 0;
                    for (int i = 0; i < UniqueTimeperiodList.Count; i++)
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = UniqueTimeperiodList[i].MonthYear;
                        var TimePeriodData = CompanyTimeperiodData.Where(a => a.TimePeriod == UniqueTimeperiodList[i].TimePeriodID).FirstOrDefault();
                        if (TimePeriodData != null)
                        {
                            if (DataType == 0)
                            {
                                objMonthData.Data = TimePeriodData.TotalSales;
                                TotalSales += TimePeriodData.TotalSales;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalSales;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalSales);
                                    }
                                }
                            }
                            else
                            {
                                objMonthData.Data = TimePeriodData.TotalUnits;
                                TotalSales += TimePeriodData.TotalUnits;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalUnits;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalUnits);
                                    }
                                }
                            }

                           

                        }
                        else
                        {
                            objMonthData.Data = 0;

                        }
                        objMonthDataList.Add(objMonthData);
                    }
                    objTableDataList.MonthData = objMonthDataList;
                    objTableDataList.TotalValue = TotalSales;
                    CompanyPerformanceTableList.Add(objTableDataList);
                }

                IList<MonthData> objMonthDataTotalValue = new List<MonthData>();
                IList<MonthData> objMonthDataOtherValue = new List<MonthData>();
                Double TotalCompanyGrandValue = 0;
                Double TotalCompanyOtherGrandValue = 0;
                for (int j = 0; j < UniqueTimeperiodList.Count; j++)
                {
                    Double TotalTimePeriodSale = 0;
                    if (DataType == 0)
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriod == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalSales).Sum();
                    }
                    else
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriod == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalUnits).Sum();
                    }
                    MonthData objMonth = new MonthData()
                    {
                        Data = TotalTimePeriodSale,
                        MonthName = UniqueTimeperiodList[j].MonthYear
                    };
                    objMonthDataTotalValue.Add(objMonth);
                    TotalCompanyGrandValue += TotalTimePeriodSale;

                    Double Value = 0;
                    if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[j].TimePeriodID, out Value))
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale - Value,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale - Value;

                    }
                    else
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale;
                    }

                }


                TableList objTableDataListotherTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataOtherValue,
                    Name = "Others",
                    Rank = 0,
                    TotalValue = TotalCompanyOtherGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListotherTotal);
                TableList objTableDataListGrandTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataTotalValue,
                    Name = "Grand Total",
                    Rank = 0,
                    TotalValue = TotalCompanyGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListGrandTotal);

            }
            catch
            {
                throw;
            }

            return CompanyPerformanceTableList;
        }



        public IList<CompanyPerformance> GetBrandPerformanceList(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, Comparison_IVM objComparision, Int32 Type)
        {
            IList<CompanyPerformance> CompanyPerformanceList = new List<CompanyPerformance>();
            try
            {
                IList<Int64> AlreadyAddedCompany = new List<Int64>();

                // Code For color coding
                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();

                IList<Int64> UniqueCompanyList = result.Select(a => a.CompanyID).Take(10).ToList();

                // Top 5 Company by Sales
                IList<Int64> Top5CompanyBySale = result.Select(a => a.CompanyID).Take(5).ToList();
                UniqueCompanyList.Add(0);
                if (!objComparision.includeTopSearches)
                {
                    Top5CompanyBySale = new List<Int64>();

                }
                for (int k = 0; k < CompanyListToadd.Count; k++)
                {
                    if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                    {
                        UniqueCompanyList.Add(CompanyListToadd[k]);
                    }
                    if (!Top5CompanyBySale.Contains(CompanyListToadd[k]))
                    {
                        Top5CompanyBySale.Add(CompanyListToadd[k]);
                    }

                }

                CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                ColorCodeCompanyDataAccess = null;
                Dictionary<Int64, string> ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, Type);

                Dictionary<Int64, string> ObjCompanyColorCodePatchBySale = this.GetCompanyColorCode(Top5CompanyBySale, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, Type);

                Double OtherCompanySales = 0;
                Double OtherCompanyUnits = 0;
                string ColorCode = string.Empty;
                for (int i = 0; i < result.Count; i++)
                {
                    if (objComparision.includeTopSearches)
                    {
                        if (i < 10)
                        {
                            AlreadyAddedCompany.Add(result[i].CompanyID);
                            CompanyPerformance singleEntry = new CompanyPerformance();

                            singleEntry.Rank = i + 1;
                            singleEntry.CompanyID = result[i].CompanyID;
                            singleEntry.TotalSales = result[i].TotalSales;
                            singleEntry.TotalUnits = result[i].TotalUnits;
                            singleEntry.CompanyName = result[i].CompanyName;
                            singleEntry.MarketShare = Math.Round(((result[i].TotalSales / TotalMarketSale) * 100), 2);
                            singleEntry.MarketUnitShare = Math.Round(((result[i].TotalUnits / TotalMarketUnit) * 100), 2);
                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(result[i].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }
                            CompanyPerformanceList.Add(singleEntry);
                        }
                    }

                    if (CompanyListToadd.Contains(result[i].CompanyID))
                    {
                        if (!AlreadyAddedCompany.Contains(result[i].CompanyID))
                        {
                            CompanyPerformance singleEntry = new CompanyPerformance();
                            singleEntry.Rank = i + 1;
                            singleEntry.CompanyName = result[i].CompanyName;
                            singleEntry.CompanyID = result[i].CompanyID;
                            singleEntry.TotalSales = result[i].TotalSales;
                            singleEntry.TotalUnits = result[i].TotalUnits;
                            singleEntry.MarketUnitShare = Math.Round(((result[i].TotalUnits / TotalMarketUnit) * 100), 2);

                            singleEntry.MarketShare = Math.Round(((result[i].TotalSales / TotalMarketSale) * 100), 2);

                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(result[i].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }

                            CompanyPerformanceList.Add(singleEntry);
                            AlreadyAddedCompany.Add(result[i].CompanyID);
                        }

                    }
                    else
                    {
                        if (!AlreadyAddedCompany.Contains(result[i].CompanyID))
                        {
                            OtherCompanySales += result[i].TotalSales;
                            OtherCompanyUnits += result[i].TotalUnits;
                        }
                    }


                }
                IList<Int64> ZeroValueCompanyList = new List<Int64>();
                for (int j = 0; j < CompanyListToadd.Count; j++)
                {
                    if (!AlreadyAddedCompany.Contains(CompanyListToadd[j]))
                    {
                        ZeroValueCompanyList.Add(CompanyListToadd[j]);
                    }

                }

                // Get Zero Value Company  Details
                IList<BrandDetails> ZeroValueCompanyDetailsForSales = new List<BrandDetails>();
                if (ZeroValueCompanyList.Count > 0)
                {
                    ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                    ZeroValueCompanyDetailsForSales = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", ZeroValueCompanyList), "brand");
                    objCompanyDataAccess = null;
                    if (ZeroValueCompanyDetailsForSales.Count > 0)
                    {
                        for (int z = 0; z < ZeroValueCompanyDetailsForSales.Count; z++)
                        {
                            CompanyPerformance singleEntry = new CompanyPerformance();
                            singleEntry.Rank = 0;
                            singleEntry.CompanyName = ZeroValueCompanyDetailsForSales[z].Brand;
                            singleEntry.CompanyID = ZeroValueCompanyDetailsForSales[z].BrandID;
                            singleEntry.TotalSales = 0;
                            singleEntry.TotalUnits = 0;
                            singleEntry.MarketUnitShare = 0;
                            singleEntry.MarketShare = 0;


                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(ZeroValueCompanyDetailsForSales[z].BrandID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }
                            CompanyPerformanceList.Add(singleEntry);
                        }
                    }
                }



                CompanyPerformance singleEntryOther = new CompanyPerformance();
                singleEntryOther.Rank = 0;
                singleEntryOther.CompanyName = "Others";
                singleEntryOther.CompanyID = 0;
                singleEntryOther.TotalSales = OtherCompanySales;
                singleEntryOther.TotalUnits = OtherCompanyUnits;
                singleEntryOther.MarketUnitShare = Math.Round((OtherCompanyUnits / TotalMarketUnit) * 100);
                singleEntryOther.MarketShare = Math.Round((OtherCompanySales / TotalMarketSale) * 100);

                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(0, out ColorCode))
                {
                    singleEntryOther.ColorCode = ColorCode;

                }
                else
                {
                    singleEntryOther.ColorCode = ColorCode;
                }

                CompanyPerformanceList.Add(singleEntryOther);

            }
            catch
            {
                throw;
            }

            return CompanyPerformanceList;
        }


        public IList<CompanyPerformance> GetBrandPerformanceReportList(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, BrandComparison_IVM objComparision, Int32 Type)
        {
            IList<CompanyPerformance> CompanyPerformanceList = new List<CompanyPerformance>();
            try
            {
                IList<Int64> AlreadyAddedCompany = new List<Int64>();

                // Code For color coding
                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();

                IList<Int64> UniqueCompanyList = result.Select(a => a.CompanyID).Take(10).ToList();

                // Top 5 Company by Sales
                IList<Int64> Top5CompanyBySale = result.Select(a => a.CompanyID).Take(5).ToList();
                UniqueCompanyList.Add(0);
            
                for (int k = 0; k < CompanyListToadd.Count; k++)
                {
                    if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                    {
                        UniqueCompanyList.Add(CompanyListToadd[k]);
                    }
                    if (!Top5CompanyBySale.Contains(CompanyListToadd[k]))
                    {
                        Top5CompanyBySale.Add(CompanyListToadd[k]);
                    }

                }

                CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                ColorCodeCompanyDataAccess = null;
                Dictionary<Int64, string> ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, Type);

                Dictionary<Int64, string> ObjCompanyColorCodePatchBySale = this.GetCompanyColorCode(Top5CompanyBySale, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, Type);

                Double OtherCompanySales = 0;
                Double OtherCompanyUnits = 0;
                string ColorCode = string.Empty;
                for (int i = 0; i < result.Count; i++)
                {
                    
                        if (i < 10)
                        {
                            AlreadyAddedCompany.Add(result[i].CompanyID);
                            CompanyPerformance singleEntry = new CompanyPerformance();

                            singleEntry.Rank = i + 1;
                            singleEntry.CompanyID = result[i].CompanyID;
                            singleEntry.TotalSales = string.IsNullOrEmpty(objComparision.uomScale) ? result[i].TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(result[i].TotalSales / 1000, 2) : Math.Round(result[i].TotalSales / 10000000, 2); 
                            //singleEntry.TotalUnits = result[i].TotalUnits;
                            singleEntry.CompanyName = result[i].CompanyName;
                        singleEntry.MarketShare = Math.Round(((result[i].TotalSales / TotalMarketSale) * 100), 2);
                           // singleEntry.MarketUnitShare = Math.Round(((result[i].TotalUnits / TotalMarketUnit) * 100), 2);
                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(result[i].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }
                            CompanyPerformanceList.Add(singleEntry);
                        }
                    

                    if (CompanyListToadd.Contains(result[i].CompanyID))
                    {
                        if (!AlreadyAddedCompany.Contains(result[i].CompanyID))
                        {
                            CompanyPerformance singleEntry = new CompanyPerformance();
                            singleEntry.Rank = i + 1;
                            singleEntry.CompanyName = result[i].CompanyName;
                            singleEntry.CompanyID = result[i].CompanyID;
                            singleEntry.TotalSales = string.IsNullOrEmpty(objComparision.uomScale) ? result[i].TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(result[i].TotalSales / 1000, 2) : Math.Round(result[i].TotalSales / 10000000, 2);
                            //singleEntry.TotalUnits = result[i].TotalUnits;
                            //singleEntry.MarketUnitShare = Math.Round(((result[i].TotalUnits / TotalMarketUnit) * 100), 2);

                            singleEntry.MarketShare = Math.Round(((result[i].TotalSales / TotalMarketSale) * 100), 2);

                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(result[i].CompanyID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }

                            CompanyPerformanceList.Add(singleEntry);
                            AlreadyAddedCompany.Add(result[i].CompanyID);
                        }

                    }
                    else
                    {
                        if (!AlreadyAddedCompany.Contains(result[i].CompanyID))
                        {
                            OtherCompanySales += string.IsNullOrEmpty(objComparision.uomScale) ? result[i].TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(result[i].TotalSales / 1000, 2) : Math.Round(result[i].TotalSales / 10000000, 2);
                            //OtherCompanyUnits += result[i].TotalUnits;
                        }
                    }


                }
                IList<Int64> ZeroValueCompanyList = new List<Int64>();
                for (int j = 0; j < CompanyListToadd.Count; j++)
                {
                    if (!AlreadyAddedCompany.Contains(CompanyListToadd[j]))
                    {
                        ZeroValueCompanyList.Add(CompanyListToadd[j]);
                    }

                }

                // Get Zero Value Company  Details
                IList<BrandDetails> ZeroValueCompanyDetailsForSales = new List<BrandDetails>();
                if (ZeroValueCompanyList.Count > 0)
                {
                    ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                    ZeroValueCompanyDetailsForSales = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", ZeroValueCompanyList), "brand");
                    objCompanyDataAccess = null;
                    if (ZeroValueCompanyDetailsForSales.Count > 0)
                    {
                        for (int z = 0; z < ZeroValueCompanyDetailsForSales.Count; z++)
                        {
                            CompanyPerformance singleEntry = new CompanyPerformance();
                            singleEntry.Rank = 0;
                            singleEntry.CompanyName = ZeroValueCompanyDetailsForSales[z].Brand;
                            singleEntry.CompanyID = ZeroValueCompanyDetailsForSales[z].BrandID;
                            singleEntry.TotalSales = 0;
                            singleEntry.TotalUnits = 0;
                            singleEntry.MarketUnitShare = 0;
                            singleEntry.MarketShare = 0;


                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(ZeroValueCompanyDetailsForSales[z].BrandID, out ColorCode))
                            {
                                singleEntry.ColorCode = ColorCode;

                            }
                            else
                            {
                                singleEntry.ColorCode = ColorCode;
                            }
                            CompanyPerformanceList.Add(singleEntry);
                        }
                    }
                }



                CompanyPerformance singleEntryOther = new CompanyPerformance();
                singleEntryOther.Rank = 0;
                singleEntryOther.CompanyName = "Others";
                singleEntryOther.CompanyID = 0;
                singleEntryOther.TotalSales = OtherCompanySales;
                singleEntryOther.TotalUnits = OtherCompanyUnits;
                singleEntryOther.MarketUnitShare = Math.Round((OtherCompanyUnits / TotalMarketUnit) * 100);
                singleEntryOther.MarketShare = Math.Round((OtherCompanySales / TotalMarketSale) * 100);

                ColorCode = string.Empty;
                if (ObjCompanyColorCode.TryGetValue(0, out ColorCode))
                {
                    singleEntryOther.ColorCode = ColorCode;

                }
                else
                {
                    singleEntryOther.ColorCode = ColorCode;
                }

                CompanyPerformanceList.Add(singleEntryOther);

            }
            catch
            {
                throw;
            }

            return CompanyPerformanceList;
        }

        public IList<TableList> GetBrandPerformanceListTable(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, Comparison_IVM objComparision, Int32 Type, Int32 DataType, List<TimePeriod> UniqueTimeperiodList)
        {
            IList<TableList> CompanyPerformanceTableList = new List<TableList>();
            try
            {
                IList<Int32> AlreadyAddedCompany = new List<Int32>();

                // Code For color coding
                var CopmpanySales = from r in result
                                    group r by new
                                    {
                                        r.CompanyID,
                                        r.CompanyName
                                    } into g
                                    select new CompanyPerformance
                                    {
                                        CompanyID = g.Key.CompanyID,
                                        CompanyName = g.Key.CompanyName,
                                        TotalSales = g.Sum(a => a.TotalSales),
                                        TotalUnits = g.Sum(a => a.TotalUnits)
                                    };


                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                IList<Int64> UniqueCompanyList = new List<Int64>();
                IList<Int64> UniqueCompanyListData = new List<Int64>();
                if (DataType == 0)
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).ToList();

                }
                else
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).ToList();

                }


                Dictionary<Int64, Int32> CompanyRankDetails = new Dictionary<Int64, int>();
                for (int i = 0; i < UniqueCompanyListData.Count; i++)
                {
                    if (objComparision.includeTopSearches)
                    {


                        if (i < 10)
                        {
                            if (!UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                            {
                                UniqueCompanyList.Add(UniqueCompanyListData[i]);

                            }
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }

                        }
                    }
                    if (CompanyListToadd.Count > 0)
                    {
                        if (CompanyListToadd.Contains(UniqueCompanyListData[i]) && !UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                        {
                            UniqueCompanyList.Add(UniqueCompanyListData[i]);
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }
                        }
                    }
                }
                //var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();
                ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                string DatatypeString = "brand";
                if (objComparision.DataType != "npm" && objComparision.ComparisonType=="brand")
                {
                    if (!string.IsNullOrEmpty(objComparision.SKUIDs))
                    {
                      //  DatatypeString = "sku";
                    }
                }
                else
                {
                    if(objComparision.ComparisonType =="sku")
                    {
                        DatatypeString = "sku";
                    }
                }
                IList<BrandDetails> CompanyDetailsList = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", UniqueCompanyList), DatatypeString);
                objCompanyDataAccess = null;
                Dictionary<Int32, Double> TotalPeriodDataValue = new Dictionary<int, double>();
                for (int j = 0; j < UniqueCompanyList.Count; j++)
                {
                    TableList objTableDataList = new TableList();
                    objTableDataList.ID = UniqueCompanyList[j];
                    BrandDetails objCompanyDetails = CompanyDetailsList.Where(a => a.BrandID == UniqueCompanyList[j]).FirstOrDefault();
                    if (objCompanyDetails != null )
                    {
                        objTableDataList.Name = objCompanyDetails.Brand  + " (" + objCompanyDetails.CompanyName +") "; 
                    }
                    Int32 RankCounter = 0;
                    if (CompanyRankDetails.TryGetValue(UniqueCompanyList[j], out RankCounter))
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    else
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    IList<CompanyPerformance> CompanyTimeperiodData = result.Where(a => a.CompanyID == UniqueCompanyList[j]).ToList();
                    IList<MonthData> objMonthDataList = new List<MonthData>();
                    Double TotalSales = 0;
                    for (int i = 0; i < UniqueTimeperiodList.Count; i++)
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = UniqueTimeperiodList[i].MonthYear;
                        var TimePeriodData = CompanyTimeperiodData.Where(a => a.TimePeriod == UniqueTimeperiodList[i].TimePeriodID).FirstOrDefault();
                        if (TimePeriodData != null)
                        {
                            if (DataType == 0)
                            {
                                objMonthData.Data = TimePeriodData.TotalSales;
                                TotalSales += TimePeriodData.TotalSales;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalSales;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalSales);
                                    }
                                }
                            }
                            else
                            {
                                objMonthData.Data = TimePeriodData.TotalUnits;
                                TotalSales += TimePeriodData.TotalUnits;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalUnits;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalUnits);
                                    }
                                }
                            }

                        }
                        else
                        {
                            objMonthData.Data = 0;

                        }
                        objMonthDataList.Add(objMonthData);
                    }
                    objTableDataList.MonthData = objMonthDataList;
                    objTableDataList.TotalValue = TotalSales;
                    CompanyPerformanceTableList.Add(objTableDataList);
                }


              IList<MonthData> objMonthDataTotalValue = new List<MonthData>();
                IList<MonthData> objMonthDataOtherValue = new List<MonthData>();
                Double TotalCompanyGrandValue = 0;
                Double TotalCompanyOtherGrandValue = 0;
                for (int j = 0; j < UniqueTimeperiodList.Count; j++)
                {
                    Double TotalTimePeriodSale = 0;
                    if (DataType == 0)
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriod == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalSales).Sum();
                    }
                    else
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriod == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalUnits).Sum();
                    }
                    MonthData objMonth = new MonthData()
                    {
                        Data = TotalTimePeriodSale,
                        MonthName = UniqueTimeperiodList[j].MonthYear
                    };
                    objMonthDataTotalValue.Add(objMonth);
                    TotalCompanyGrandValue += TotalTimePeriodSale;

                    Double Value = 0;
                    if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[j].TimePeriodID, out Value))
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale - Value,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale - Value;

                    }
                    else
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale;
                    }

                }


                TableList objTableDataListotherTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataOtherValue,
                    Name = "Others",
                    Rank = 0,
                    TotalValue = TotalCompanyOtherGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListotherTotal);

                TableList objTableDataListGrandTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataTotalValue,
                    Name = "Grand Total",
                    Rank = 0,
                    TotalValue = TotalCompanyGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListGrandTotal);



            }
            catch
            {
                throw;
            }

            return CompanyPerformanceTableList;
        }

        public IList<TableList> GetBrandPerformanceListReportTable(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, BrandComparison_IVM objComparision, Int32 Type, Int32 DataType, List<TimePeriod> UniqueTimeperiodList)
        {
            IList<TableList> CompanyPerformanceTableList = new List<TableList>();
            try
            {
                IList<Int32> AlreadyAddedCompany = new List<Int32>();

                // Code For color coding
                var CopmpanySales = from r in result
                                    group r by new
                                    {
                                        r.CompanyID,
                                        r.CompanyName
                                    } into g
                                    select new CompanyPerformance
                                    {
                                        CompanyID = g.Key.CompanyID,
                                        CompanyName = g.Key.CompanyName,
                                        TotalSales = g.Sum(a => a.TotalSales),
                                        TotalUnits = g.Sum(a => a.TotalUnits)
                                    };


                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                IList<Int64> UniqueCompanyList = new List<Int64>();
                IList<Int64> UniqueCompanyListData = new List<Int64>();
                if (DataType == 0)
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).ToList();

                }
                else
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).ToList();

                }


                Dictionary<Int64, Int32> CompanyRankDetails = new Dictionary<Int64, int>();
                for (int i = 0; i < UniqueCompanyListData.Count; i++)
                {
                   


                        if (i < 10)
                        {
                            if (!UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                            {
                                UniqueCompanyList.Add(UniqueCompanyListData[i]);

                            }
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }

                        }
                    
                    if (CompanyListToadd.Count > 0)
                    {
                        if (CompanyListToadd.Contains(UniqueCompanyListData[i]) && !UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                        {
                            UniqueCompanyList.Add(UniqueCompanyListData[i]);
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }
                        }
                    }
                }
                //var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();
                ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                string DatatypeString = "brand";
               
                IList<BrandDetails> CompanyDetailsList = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", UniqueCompanyList), DatatypeString);
                objCompanyDataAccess = null;
                Dictionary<Int32, Double> TotalPeriodDataValue = new Dictionary<int, double>();
                for (int j = 0; j < UniqueCompanyList.Count; j++)
                {
                    TableList objTableDataList = new TableList();
                    objTableDataList.ID = UniqueCompanyList[j];
                    BrandDetails objCompanyDetails = CompanyDetailsList.Where(a => a.BrandID == UniqueCompanyList[j]).FirstOrDefault();
                    if (objCompanyDetails != null)
                    {
                        objTableDataList.Name = objCompanyDetails.Brand + " (" + objCompanyDetails.CompanyName + ") ";
                    }
                    Int32 RankCounter = 0;
                    if (CompanyRankDetails.TryGetValue(UniqueCompanyList[j], out RankCounter))
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    else
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    IList<CompanyPerformance> CompanyTimeperiodData = result.Where(a => a.CompanyID == UniqueCompanyList[j]).ToList();
                    IList<MonthData> objMonthDataList = new List<MonthData>();
                    Double TotalSales = 0;
                    for (int i = 0; i < UniqueTimeperiodList.Count; i++)
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = UniqueTimeperiodList[i].MonthYear;
                        var TimePeriodData = CompanyTimeperiodData.Where(a => a.TimePeriod == UniqueTimeperiodList[i].TimePeriodID).FirstOrDefault();
                        if (TimePeriodData != null)
                        {
                            if (DataType == 0)
                            {
                                objMonthData.Data = TimePeriodData.TotalSales;
                                TotalSales += TimePeriodData.TotalSales;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalSales;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalSales);
                                    }
                                }
                            }
                            else
                            {
                                objMonthData.Data = TimePeriodData.TotalUnits;
                                TotalSales += TimePeriodData.TotalUnits;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalUnits;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalUnits);
                                    }
                                }
                            }

                        }
                        else
                        {
                            objMonthData.Data = 0;

                        }
                        objMonthDataList.Add(objMonthData);
                    }
                    objTableDataList.MonthData = objMonthDataList;
                    objTableDataList.TotalValue = TotalSales;
                    CompanyPerformanceTableList.Add(objTableDataList);
                }


                IList<MonthData> objMonthDataTotalValue = new List<MonthData>();
                IList<MonthData> objMonthDataOtherValue = new List<MonthData>();
                Double TotalCompanyGrandValue = 0;
                Double TotalCompanyOtherGrandValue = 0;
                for (int j = 0; j < UniqueTimeperiodList.Count; j++)
                {
                    Double TotalTimePeriodSale = 0;
                    if (DataType == 0)
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriod == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalSales).Sum();
                    }
                    else
                    {
                        TotalTimePeriodSale = result.Where(a => a.TimePeriod == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalUnits).Sum();
                    }
                    MonthData objMonth = new MonthData()
                    {
                        Data = TotalTimePeriodSale,
                        MonthName = UniqueTimeperiodList[j].MonthYear
                    };
                    objMonthDataTotalValue.Add(objMonth);
                    TotalCompanyGrandValue += TotalTimePeriodSale;

                    Double Value = 0;
                    if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[j].TimePeriodID, out Value))
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale - Value,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale - Value;

                    }
                    else
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = TotalTimePeriodSale,
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale;
                    }

                }


                TableList objTableDataListotherTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataOtherValue,
                    Name = "Others",
                    Rank = 0,
                    TotalValue = TotalCompanyOtherGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListotherTotal);

                TableList objTableDataListGrandTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataTotalValue,
                    Name = "Grand Total",
                    Rank = 0,
                    TotalValue = TotalCompanyGrandValue
                };

                CompanyPerformanceTableList.Add(objTableDataListGrandTotal);



            }
            catch
            {
                throw;
            }

            return CompanyPerformanceTableList;
        }

        public IList<TableList> GetBrandPerformanceListReportTableNew(IList<CompanyPerformance> result, IList<Int64> CompanyListToadd, BrandComparison_IVM objComparision, Int32 Type, Int32 DataType, List<TimePeriod> UniqueTimeperiodList, IList<TimePeriodSalesData> ObjTimePeriodList)
        {
            IList<TableList> CompanyPerformanceTableList = new List<TableList>();
            try
            {
                IList<Int32> AlreadyAddedCompany = new List<Int32>();

                // Code For color coding
                var CopmpanySales = from r in result
                                    group r by new
                                    {
                                        r.CompanyID,
                                        r.CompanyName
                                    } into g
                                    select new CompanyPerformance
                                    {
                                        CompanyID = g.Key.CompanyID,
                                        CompanyName = g.Key.CompanyName,
                                        TotalSales = g.Sum(a => a.TotalSales),
                                        TotalUnits = g.Sum(a => a.TotalUnits)
                                    };


                Double TotalMarketSale = result.Select(a => a.TotalSales).Sum();
                Double TotalMarketUnit = result.Select(a => a.TotalUnits).Sum();
                IList<Int64> UniqueCompanyList = new List<Int64>();
                IList<Int64> UniqueCompanyListData = new List<Int64>();
                if (DataType == 0)
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalSales).Select(a => a.CompanyID).ToList();

                }
                else
                {
                    UniqueCompanyListData = CopmpanySales.OrderByDescending(a => a.TotalUnits).Select(a => a.CompanyID).ToList();

                }


                Dictionary<Int64, Int32> CompanyRankDetails = new Dictionary<Int64, int>();
                for (int i = 0; i < UniqueCompanyListData.Count; i++)
                {



                    if (i < 10)
                    {
                        if (!UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                        {
                            UniqueCompanyList.Add(UniqueCompanyListData[i]);

                        }
                        if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                        {
                            CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                        }

                    }

                    if (CompanyListToadd.Count > 0)
                    {
                        if (CompanyListToadd.Contains(UniqueCompanyListData[i]) && !UniqueCompanyList.Contains(UniqueCompanyListData[i]))
                        {
                            UniqueCompanyList.Add(UniqueCompanyListData[i]);
                            if (!CompanyRankDetails.ContainsKey(UniqueCompanyListData[i]))
                            {
                                CompanyRankDetails.Add(UniqueCompanyListData[i], i + 1);
                            }
                        }
                    }
                }
                //var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();
                ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                string DatatypeString = "brand";

                IList<BrandDetails> CompanyDetailsList = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", UniqueCompanyList), DatatypeString);
                objCompanyDataAccess = null;
                Dictionary<Int32, Double> TotalPeriodDataValue = new Dictionary<int, double>();
                for (int j = 0; j < UniqueCompanyList.Count; j++)
                {
                    TableList objTableDataList = new TableList();
                    objTableDataList.ID = UniqueCompanyList[j];
                    BrandDetails objCompanyDetails = CompanyDetailsList.Where(a => a.BrandID == UniqueCompanyList[j]).FirstOrDefault();
                    if (objCompanyDetails != null)
                    {
                        objTableDataList.Name = objCompanyDetails.Brand + " (" + objCompanyDetails.CompanyName + ") ";
                    }
                    Int32 RankCounter = 0;
                    if (CompanyRankDetails.TryGetValue(UniqueCompanyList[j], out RankCounter))
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    else
                    {
                        objTableDataList.Rank = RankCounter;
                    }
                    IList<CompanyPerformance> CompanyTimeperiodData = result.Where(a => a.CompanyID == UniqueCompanyList[j]).ToList();
                    IList<MonthData> objMonthDataList = new List<MonthData>();
                    Double TotalSales = 0;
                    for (int i = 0; i < UniqueTimeperiodList.Count; i++)
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = UniqueTimeperiodList[i].MonthYear;
                        var TimePeriodData = CompanyTimeperiodData.Where(a => a.TimePeriod == UniqueTimeperiodList[i].TimePeriodID).FirstOrDefault();
                        if (TimePeriodData != null)
                        {
                            if (DataType == 0)
                            {
                                objMonthData.Data = string.IsNullOrEmpty(objComparision.uomScale) ? TimePeriodData.TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(TimePeriodData.TotalSales / 1000, 2) : Math.Round(TimePeriodData.TotalSales / 10000000, 2) ;
                                TotalSales += TimePeriodData.TotalSales;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalSales;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalSales);
                                    }
                                }
                            }
                            else
                            {
                                objMonthData.Data = string.IsNullOrEmpty(objComparision.uomScale) ? TimePeriodData.TotalUnits : objComparision.uomScale.ToLower() == "k" ? Math.Round(TimePeriodData.TotalUnits / 1000, 2) : Math.Round(TimePeriodData.TotalUnits / 10000000, 2); 
                                TotalSales += TimePeriodData.TotalUnits;

                                Double PeriodTotal = 0;
                                if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[i].TimePeriodID, out PeriodTotal))
                                {
                                    PeriodTotal += TimePeriodData.TotalUnits;
                                    TotalPeriodDataValue[UniqueTimeperiodList[i].TimePeriodID] = PeriodTotal;
                                }
                                else
                                {
                                    if (!TotalPeriodDataValue.ContainsKey(UniqueTimeperiodList[i].TimePeriodID))
                                    {
                                        TotalPeriodDataValue.Add(UniqueTimeperiodList[i].TimePeriodID, TimePeriodData.TotalUnits);
                                    }
                                }
                            }

                        }
                        else
                        {
                            objMonthData.Data = 0;

                        }
                        objMonthDataList.Add(objMonthData);
                    }
                    objTableDataList.MonthData = objMonthDataList;
                    objTableDataList.TotalValue = string.IsNullOrEmpty(objComparision.uomScale) ? TotalSales : objComparision.uomScale.ToLower() == "k" ? Math.Round(TotalSales / 1000, 2) : Math.Round(TotalSales / 10000000, 2); ;
                    CompanyPerformanceTableList.Add(objTableDataList);
                }


                IList<MonthData> objMonthDataTotalValue = new List<MonthData>();
                IList<MonthData> objMonthDataOtherValue = new List<MonthData>();
                Double TotalCompanyGrandValue = 0;
                Double TotalCompanyOtherGrandValue = 0;
                for (int j = 0; j < UniqueTimeperiodList.Count; j++)
                {
                    Double TotalTimePeriodSale = 0;
                   
                    TotalTimePeriodSale = ObjTimePeriodList.Where(a => a.TimePeriodID == UniqueTimeperiodList[j].TimePeriodID).Select(a => a.TotalSales).Sum();
                    
                    MonthData objMonth = new MonthData()
                    {
                        Data = string.IsNullOrEmpty(objComparision.uomScale) ? TotalTimePeriodSale : objComparision.uomScale.ToLower() == "k" ? Math.Round(TotalTimePeriodSale / 1000, 2) : Math.Round(TotalTimePeriodSale / 10000000, 2),
                        MonthName = UniqueTimeperiodList[j].MonthYear
                    };
                    objMonthDataTotalValue.Add(objMonth);
                    TotalCompanyGrandValue += TotalTimePeriodSale;

                    Double Value = 0;
                    if (TotalPeriodDataValue.TryGetValue(UniqueTimeperiodList[j].TimePeriodID, out Value))
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = string.IsNullOrEmpty(objComparision.uomScale) ? (Math.Round(TotalTimePeriodSale) - Math.Round(Value)) : objComparision.uomScale.ToLower() == "k" ? Math.Round((Math.Round(TotalTimePeriodSale) - Math.Round(Value)) / 1000, 2) : Math.Round((Math.Round(TotalTimePeriodSale) - Math.Round(Value)) / 10000000, 2),
                            
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += Math.Round(TotalTimePeriodSale) - Math.Round(Value);

                    }
                    else
                    {
                        MonthData objMontho = new MonthData()
                        {
                            Data = string.IsNullOrEmpty(objComparision.uomScale) ? TotalTimePeriodSale : objComparision.uomScale.ToLower() == "k" ? Math.Round(TotalTimePeriodSale / 1000, 2) : Math.Round(TotalTimePeriodSale / 10000000, 2),
                            MonthName = UniqueTimeperiodList[j].MonthYear
                        };
                        objMonthDataOtherValue.Add(objMontho);
                        TotalCompanyOtherGrandValue += TotalTimePeriodSale;
                    }

                }


                TableList objTableDataListotherTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataOtherValue,
                    Name = "Others",
                    Rank = 0,
                    TotalValue = string.IsNullOrEmpty(objComparision.uomScale) ? TotalCompanyOtherGrandValue : objComparision.uomScale.ToLower() == "k" ? Math.Round(TotalCompanyOtherGrandValue / 1000, 2) : Math.Round(TotalCompanyOtherGrandValue / 10000000, 2),
                    
                };

                CompanyPerformanceTableList.Add(objTableDataListotherTotal);

                TableList objTableDataListGrandTotal = new TableList()
                {
                    ID = 0,
                    MonthData = objMonthDataTotalValue,
                    Name = "Grand Total",
                    Rank = 0,
                    TotalValue = string.IsNullOrEmpty(objComparision.uomScale) ? TotalCompanyGrandValue : objComparision.uomScale.ToLower() == "k" ? Math.Round(TotalCompanyGrandValue / 1000, 2) : Math.Round(TotalCompanyGrandValue / 10000000, 2),
                    
                };

                CompanyPerformanceTableList.Add(objTableDataListGrandTotal);



            }
            catch
            {
                throw;
            }

            return CompanyPerformanceTableList;
        }

        public IList<TableList> GetCompanyBrandPerformanceListTable(IList<BrandPerformance> result, IList<Int64> BrandListToadd, Int32 DataType, IList<BrickMaster> BrickDetails, IList<BrandPerformance> resultCompany, string CityName, MarketGrowth objCityMarketGrowth)
        {
            IList<TableList> BrandPerformanceTableList = new List<TableList>();
            try
            {




                //var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();
                ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                string DatatypeString = "brand";

                IList<BrandDetails> BrandDetailsList = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", BrandListToadd), DatatypeString);
                objCompanyDataAccess = null;

                BrandDetailsList.Add(new BrandDetails { BrandID = 0, Brand = "Others" });
               
                if (!BrandListToadd.Contains(0))
                {
                    BrandListToadd.Add(0);
                }
                  

                for (int j = 0; j < BrandListToadd.Count; j++)
                {
                   
                        TableList objTableDataList = new TableList();
                        objTableDataList.ID = BrandListToadd[j];
                        BrandDetails objbrandDetails = BrandDetailsList.Where(a => a.BrandID == BrandListToadd[j]).FirstOrDefault();




                        IList<BrandPerformance> BrandBrickData = result.Where(a => a.BrandID == BrandListToadd[j]).ToList();
                        IList<MonthData> objMonthDataList = new List<MonthData>();
                        Double TotalSales = 0;
                        string CompanyName = string.Empty;
                        if (BrandBrickData.Count > 0)
                        {
                            CompanyName = BrandBrickData[0].CompanyName;
                            objTableDataList.CompanyName = CompanyName;
                        }

                        if (objbrandDetails != null)
                        {
                            if (!string.IsNullOrEmpty(CompanyName))
                            {
                                objTableDataList.Name = objbrandDetails.Brand + " (" + CompanyName + ")";
                            }
                            else
                            {
                                objTableDataList.Name = objbrandDetails.Brand;
                            }

                        }
                        BrandPerformance BrandBrickDatacity = resultCompany.Where(a => a.BrandID == BrandListToadd[j]).FirstOrDefault();
                        if (BrandBrickDatacity != null)
                        {
                            MonthData objMonthData = new MonthData();
                            objMonthData.MonthName = CityName;
                            if (DataType == 0)
                            {
                                objMonthData.Data = BrandBrickDatacity.ValueGrowth;
                               
                            }
                            else
                            {
                                objMonthData.Data = BrandBrickDatacity.UnitGrowth;
                             
                            }
                            objMonthDataList.Add(objMonthData);
                        }
                        else
                        {
                            MonthData objMonthData = new MonthData();
                            objMonthData.MonthName = CityName;
                            objMonthData.Data = 0;
                            objMonthDataList.Add(objMonthData);
                        }
                        for (int i = 0; i < BrickDetails.Count; i++)
                        {

                            if (BrickDetails[i].BrickName.ToLower() != "unspecified")
                            {
                                MonthData objMonthData = new MonthData();
                                objMonthData.MonthName = BrickDetails[i].BrickName;
                                var BrickData = BrandBrickData.Where(a => a.BrickID == BrickDetails[i].BrickID).FirstOrDefault();
                                if (BrickData != null)
                                {
                                    if (DataType == 0)
                                    {
                                        objMonthData.Data = BrickData.ValueGrowth;                                    

                                    }
                                    else
                                    {
                                        objMonthData.Data = BrickData.UnitGrowth;
                                       

                                    }

                                }
                                else
                                {
                                    objMonthData.Data = 0;

                                }
                                objMonthDataList.Add(objMonthData);
                            }
                        }
                        objTableDataList.MonthData = objMonthDataList;
                        objTableDataList.TotalValue = TotalSales;
                        BrandPerformanceTableList.Add(objTableDataList);
                    
                }

                
                TableList objTableDataListMarket = new TableList();
                objTableDataListMarket.ID = 0;
                objTableDataListMarket.Name = "Total Market";
                IList<MonthData> objMonthDataListMarket = new List<MonthData>();
                MonthData objMonthDataMarket = new MonthData();
                objMonthDataMarket.MonthName = CityName;
                if (DataType == 0)
                {
                    objMonthDataMarket.Data = objCityMarketGrowth.CityGrowthValue;
                }
                else
                {
                    objMonthDataMarket.Data = objCityMarketGrowth.CityGrowthUnit;
                }
                objMonthDataListMarket.Add(objMonthDataMarket);

                Double TotalSalesMarket = 0;
                for (int i = 0; i < BrickDetails.Count; i++)
                {
                    if (BrickDetails[i].BrickName.ToLower() != "unspecified")
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = BrickDetails[i].BrickName;
                        Double BrickTotal = 0;

                        MarketPatchGrowth objBrickmarketGrowth = objCityMarketGrowth.PatchGrowth.Where(a => a.BrickID == BrickDetails[i].BrickID).FirstOrDefault();
                        if(objBrickmarketGrowth != null)
                        {
                            if(DataType ==0)
                            {
                                objMonthData.Data = objBrickmarketGrowth.PatchGrowthValue;
                            }
                            else
                            {
                                objMonthData.Data = objBrickmarketGrowth.PatchGrowthUnit;
                            }
                        }
                        
                        objMonthDataListMarket.Add(objMonthData);
                    }
                }

                objTableDataListMarket.MonthData = objMonthDataListMarket;
                objTableDataListMarket.TotalValue = TotalSalesMarket;
                BrandPerformanceTableList.Add(objTableDataListMarket);

            }
            catch
            {
                throw;
            }

            return BrandPerformanceTableList;
        }

        public IList<TableList> GetCompanyBrandPerformanceListTableValue(IList<BrandPerformance> result, IList<Int64> BrandListToadd, Int32 DataType, IList<BrickMaster> BrickDetails, IList<BrandPerformance> resultCompany, string CityName)
        {
            IList<TableList> BrandPerformanceTableList = new List<TableList>();
            try
            {




                //var UniqueTimeperiodList = result.Select(a => new { a.PeriodName, a.TimePeriod }).Distinct().ToList();
                ProductDataAccess objCompanyDataAccess = new ProductDataAccess();
                string DatatypeString = "brand";

                IList<BrandDetails> BrandDetailsList = objCompanyDataAccess.GetZeroValueBrandDetails(string.Join(",", BrandListToadd), DatatypeString);
                objCompanyDataAccess = null;

                Dictionary<Int32, Double> ProductBrickDataValue = new Dictionary<int, double>();
                Dictionary<Int32, Double> TotalBrickDataVale = new Dictionary<int, double>();
                Double ProductCityDataValue = 0;
                
                for (int j = 0; j < BrandListToadd.Count; j++)
                {
                    if (BrandListToadd[j] > 0)
                    {
                        TableList objTableDataList = new TableList();
                        objTableDataList.ID = BrandListToadd[j];
                        BrandDetails objbrandDetails = BrandDetailsList.Where(a => a.BrandID == BrandListToadd[j]).FirstOrDefault();


                        if (objbrandDetails != null)
                        {
                            if (!string.IsNullOrEmpty(objbrandDetails.CompanyName))
                            {
                                objTableDataList.Name = objbrandDetails.Brand + " (" + objbrandDetails.CompanyName + ")";
                            }
                            else
                            {
                                objTableDataList.Name = objbrandDetails.Brand;
                            }

                        }


                        IList<BrandPerformance> BrandBrickData = result.Where(a => a.BrandID == BrandListToadd[j]).ToList();
                        IList<MonthData> objMonthDataList = new List<MonthData>();
                        Double TotalSales = 0;
                        if (BrandBrickData.Count > 0)
                            objTableDataList.CompanyName = BrandBrickData[0].CompanyName;

                        BrandPerformance BrandBrickDatacity = resultCompany.Where(a => a.BrandID == BrandListToadd[j]).FirstOrDefault();
                        if (BrandBrickDatacity != null)
                        {
                            MonthData objMonthData = new MonthData();
                            objMonthData.MonthName = CityName;
                            if (DataType == 0)
                            {
                                objMonthData.Data = Math.Round(BrandBrickDatacity.TotalSales);
                                ProductCityDataValue += BrandBrickDatacity.TotalSales;
                            }
                            else
                            {
                                objMonthData.Data = Math.Round(BrandBrickDatacity.TotalUnits);
                                ProductCityDataValue += BrandBrickDatacity.TotalUnits;
                            }
                            objMonthDataList.Add(objMonthData);
                        }
                        else
                        {
                            MonthData objMonthData = new MonthData();
                            objMonthData.MonthName = CityName;
                            objMonthData.Data = 0;
                            objMonthDataList.Add(objMonthData);
                        }
                        for (int i = 0; i < BrickDetails.Count; i++)
                        {
                            if (BrickDetails[i].BrickName.ToLower() != "unspecified")
                            {
                                MonthData objMonthData = new MonthData();
                                objMonthData.MonthName = BrickDetails[i].BrickName;
                                var BrickData = BrandBrickData.Where(a => a.BrickID == BrickDetails[i].BrickID).FirstOrDefault();
                                if (BrickData != null)
                                {
                                    if (DataType == 0)
                                    {
                                        objMonthData.Data = Math.Round(BrickData.TotalSales);

                                        Double BrickTotal = 0;
                                        if (ProductBrickDataValue.TryGetValue(BrickDetails[i].BrickID, out BrickTotal))
                                        {
                                            BrickTotal += BrickData.TotalSales;
                                            ProductBrickDataValue[BrickDetails[i].BrickID] = BrickTotal;
                                        }
                                        else
                                        {
                                            if (!ProductBrickDataValue.ContainsKey(BrickDetails[i].BrickID))
                                            {
                                                ProductBrickDataValue.Add(BrickDetails[i].BrickID, BrickData.TotalSales);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        objMonthData.Data = Math.Round(BrickData.TotalUnits);

                                        Double BrickTotal = 0;
                                        if (ProductBrickDataValue.TryGetValue(BrickDetails[i].BrickID, out BrickTotal))
                                        {
                                            BrickTotal += BrickData.TotalUnits;
                                            ProductBrickDataValue[BrickDetails[i].BrickID] = BrickTotal;
                                        }
                                        else
                                        {
                                            if (!ProductBrickDataValue.ContainsKey(BrickDetails[i].BrickID))
                                            {
                                                ProductBrickDataValue.Add(BrickDetails[i].BrickID, BrickData.TotalUnits);
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    objMonthData.Data = 0;

                                }
                                objMonthDataList.Add(objMonthData);
                            }
                        }
                        objTableDataList.MonthData = objMonthDataList;
                        objTableDataList.TotalValue = TotalSales;
                        BrandPerformanceTableList.Add(objTableDataList);
                    }
                }


                // Other growth Addition.

                Double TotalCityMarket = 0;
                if (DataType == 0)
                {
                    TotalCityMarket = resultCompany.Sum(a => a.TotalSales);
                }
                else
                {
                    TotalCityMarket = resultCompany.Sum(a => a.TotalUnits);
                }
                TableList objTableDataListOther = new TableList();
                objTableDataListOther.ID = 0;
                objTableDataListOther.Name = "Other";
                IList<MonthData> objMonthDataListOther = new List<MonthData>();
                MonthData objMonthDataOther = new MonthData();
                objMonthDataOther.MonthName = CityName;
                objMonthDataOther.Data = TotalCityMarket - ProductCityDataValue;
                objMonthDataListOther.Add(objMonthDataOther);

                Double TotalSalesOther = 0;

                for (int i = 0; i < BrickDetails.Count; i++)
                {
                    Double TotalValuedata = 0;
                    if (!TotalBrickDataVale.TryGetValue(BrickDetails[i].BrickID, out TotalValuedata))
                    {
                        if (DataType == 0)
                        {
                            TotalValuedata = result.Where(a => a.BrickID == BrickDetails[i].BrickID && a.BrandID !=0).Sum(a => a.TotalSales);
                        }
                        else
                        {
                            TotalValuedata = result.Where(a => a.BrickID == BrickDetails[i].BrickID && a.BrandID != 0).Sum(a => a.TotalUnits);
                        }
                        TotalBrickDataVale.Add(BrickDetails[i].BrickID, TotalValuedata);
                    }


                    if (BrickDetails[i].BrickName.ToLower() != "unspecified")
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = BrickDetails[i].BrickName;
                        Double BrickTotal = 0;
                        double BrickOtherTotal = 0;
                        if (ProductBrickDataValue.TryGetValue(BrickDetails[i].BrickID, out BrickTotal))
                        {
                            Double TotalDataout = 0;
                            if (TotalBrickDataVale.TryGetValue(BrickDetails[i].BrickID, out TotalDataout))
                            {
                                BrickOtherTotal = TotalDataout - BrickTotal;
                            }


                            objMonthData.Data = BrickOtherTotal;

                            TotalSalesOther += BrickOtherTotal;
                        }
                        else
                        {
                            objMonthData.Data = BrickTotal;
                        }

                        objMonthDataListOther.Add(objMonthData);
                    }
                }

                objTableDataListOther.MonthData = objMonthDataListOther;
                objTableDataListOther.TotalValue = TotalSalesOther;
                BrandPerformanceTableList.Add(objTableDataListOther);

                // Total Market.
                TableList objTableDataListMarket = new TableList();
                objTableDataListMarket.ID = 0;
                objTableDataListMarket.Name = "Total Market";
                IList<MonthData> objMonthDataListMarket = new List<MonthData>();
                MonthData objMonthDataMarket = new MonthData();
                objMonthDataMarket.MonthName = CityName;
                objMonthDataMarket.Data = TotalCityMarket;
                objMonthDataListMarket.Add(objMonthDataMarket);

                Double TotalSalesMarket = 0;
                for (int i = 0; i < BrickDetails.Count; i++)
                {
                    if (BrickDetails[i].BrickName.ToLower() != "unspecified")
                    {
                        MonthData objMonthData = new MonthData();
                        objMonthData.MonthName = BrickDetails[i].BrickName;
                        Double BrickTotal = 0;


                        if (TotalBrickDataVale.TryGetValue(BrickDetails[i].BrickID, out BrickTotal))
                        {
                            objMonthData.Data = BrickTotal;
                            TotalSalesMarket += BrickTotal;
                        }
                        else
                        {

                            objMonthData.Data = BrickTotal;
                        }

                        objMonthDataListMarket.Add(objMonthData);
                    }
                }

                objTableDataListMarket.MonthData = objMonthDataListMarket;
                objTableDataListMarket.TotalValue = TotalSalesMarket;
                BrandPerformanceTableList.Add(objTableDataListMarket);



            }
            catch
            {
                throw;
            }

            return BrandPerformanceTableList;
        }



        public IList<BulkProductMapping_OVM> BulkProductMapping(BulkProductMapping_IVM objBulkProductMapping)
        {
            IList<BulkProductMapping_OVM> objBulkProductMapping_OVM = new List<BulkProductMapping_OVM>();
            try
            {
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                string DivisionName = objCompanyDataAccess.GetDivisionName(objBulkProductMapping.DivisionID);
                objCompanyDataAccess = null;
                if (!string.IsNullOrEmpty(DivisionName))
                {
                    if (objBulkProductMapping.Productmapping != null)
                    {
                        for (int i = 0; i < objBulkProductMapping.Productmapping.Count; i++)
                        {
                            if (objBulkProductMapping.Productmapping[i].BIDivisionName.ToLower().Trim() == DivisionName.ToLower().Trim())
                            {
                                DivisionProductMapping objDivisionProductMapping = new DivisionProductMapping()
                                {
                                    CompanyID = objBulkProductMapping.CompanyID,
                                    DivisionID = objBulkProductMapping.DivisionID,
                                    IsNewIntroduction = objBulkProductMapping.Productmapping[i].IsNew,
                                    IsOnFocus = objBulkProductMapping.Productmapping[i].IsFocus,
                                    PFCID = objBulkProductMapping.Productmapping[i].PFCID
                                };

                                DivisionBusinessManger objDivisionmapping = new DivisionBusinessManger();
                                Int32 StatusID = objDivisionmapping.AddDivisionProductMapping(objDivisionProductMapping);
                                objDivisionmapping = null;
                                BulkProductMapping_OVM objMappingStatus = new BulkProductMapping_OVM();
                                objMappingStatus.PFCID = objBulkProductMapping.Productmapping[i].PFCID;
                                switch (StatusID)
                                {
                                    case 0:
                                        objMappingStatus.Status = "Unable to Map";
                                        break;
                                    case 1:
                                        objMappingStatus.Status = "Created.";
                                        break;
                                    case 2:
                                        objMappingStatus.Status = "Updated.";
                                        break;
                                }

                                objBulkProductMapping_OVM.Add(objMappingStatus);
                            }


                        }
                    }
                }

            }
            catch
            {
                throw;
            }

            return objBulkProductMapping_OVM;

        }

        public CompanyPerformanceData GetCompanyPerformanceMap(Comparison_IVM objComparision)
        {


            CompanyPerformanceData objCompanyPerformanceData = new CompanyPerformanceData();
            SessionDataAccess objSessionDataAccess = new SessionDataAccess();
            int UserID = objSessionDataAccess.ValidateSession(objComparision.SessionID);
            objSessionDataAccess = null;
            if (UserID == 0)
            {
                objCompanyPerformanceData = null;
                return objCompanyPerformanceData;
            }

            // Get UserAccessRights 
            // Access log
            UserLog_VM objLogDetails = new UserLog_VM()
            {
                UserID = UserID,
                MenuID = 3,
                CityID = objComparision.GeographyID,
                CategoryType = objComparision.ComparisonType,
                CategoryValue = objComparision.ComparisionValue,
                DataType = objComparision.DataType,
                EndPeriod = objComparision.PeriodEnd,
                StartPeriod = objComparision.PeriodStart,
                PatchID = objComparision.PatchIds,
                PeriodType = objComparision.PeriodType,
                TCCode = objComparision.TCCode,
                TCLevel = objComparision.TCLevel,
                ProductID = objComparision.SKUIDs,
                SessionID = objComparision.SessionID


            };
            UserLogAccessDetails objUserLogAccessDetails = new UserLogAccessDetails();
            objUserLogAccessDetails.AddUpdateUserLog(objLogDetails);
            objUserLogAccessDetails = null;

            AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
            UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

            int AccessType = 1;
            string TCCodes = string.Empty;
            if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
            {

                AccessType = 2;
                TCCodes = objUserAccessrights.TCValues;

            }
            else
            {
                CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objComparision.CompanyID);
                if (objCompanyAccessRight != null)
                {
                    switch (objCompanyAccessRight.AccessRightType)
                    {
                        case 2:
                            AccessType = 2;
                            FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                            IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                            TCCodes = string.Join(",", TCCodesList);
                            break;
                        case 3:
                            AccessType = 2;
                            TCCodes = objCompanyAccessRight.TCValues;
                            break;
                    }
                }
            }

            objAccessRightsDataAccess = null;



            objCompanyPerformanceData.CompanyPerformanceUnitList = new List<Mapview_OVM>();
            objCompanyPerformanceData.CompanyPerformanceValueList = new List<Mapview_OVM>();
            IList<Mapview_OVM> CompanyPerformanceList = new List<Mapview_OVM>();
            IList<Mapview_OVM> CompanyPerformanceUnitList = new List<Mapview_OVM>();

            try
            {
                DataSet resultds = new DataSet();
                DALUtility IMSBIDB = new DALUtility();
                string param = string.Empty;
                if (objComparision != null)
                {

                    if (objComparision.ComparisonType == "company" || objComparision.ComparisonType =="manuf")
                    {
                        #region Company Manuf Data

                        CompanyDataAccess objCompanyDataAccessMAP = new CompanyDataAccess();
                        if (objComparision.ComparisonType == "company")
                        {
                            resultds = objCompanyDataAccessMAP.GetCompanyPerformanceMap(objComparision, AccessType, TCCodes);
                        }
                        else
                        {
                            resultds = objCompanyDataAccessMAP.GetManufPerformanceMap(objComparision, AccessType, TCCodes);
                        }
                        objCompanyDataAccessMAP = null;

                        BrickDataAccess objBrickDataaccess = new BrickDataAccess();
                        IList<BrickMaster> BrickList = objBrickDataaccess.GetBrickListByCityID(objComparision.GeographyID, objComparision.CompanyID, objComparision.DivisonID, objComparision.PatchIds);
                        objBrickDataaccess = null;
                        
                        var result = (from row in resultds.Tables[0].AsEnumerable()
                                      select new CompanyPerformance
                                      {
                                          CompanyID = Convert.ToInt32(row["CompanyID"]),
                                          CompanyName = Convert.ToString(row["CompanyName"]),
                                          TotalSales = Convert.ToDouble(row["TotalSales"]),
                                          TotalUnits = Convert.ToDouble(row["TotalUnit"]),
                                          BrickID = Convert.ToInt32(row["BrickID"])
                                      }
                                      ).ToList();


                        IList<Int64> CompanyListToadd = new List<Int64>();
                        CompanyListToadd.Add(objComparision.CompanyID);
                        if (!string.IsNullOrEmpty(objComparision.ComparisionValue))
                        {
                            if (objComparision.ComparisionValue != "all")
                            {
                                string[] SelectedCompanyList = objComparision.ComparisionValue.Split(',');
                                if (SelectedCompanyList.Length > 0)
                                {
                                    for (int j = 0; j < SelectedCompanyList.Length; j++)
                                    {
                                        CompanyListToadd.Add(Convert.ToInt32(SelectedCompanyList[j]));
                                    }
                                }
                            }
                        }
                        IList<Int64> UniqueCompanyList = new List<Int64>();
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<Int64> UniqueCompanyListInBrick = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalSales).Select(a => a.CompanyID).Take(10).ToList();
                            for (int brickCompanyCount = 0; brickCompanyCount < UniqueCompanyListInBrick.Count; brickCompanyCount++)
                            {
                                if (!UniqueCompanyList.Contains(UniqueCompanyListInBrick[brickCompanyCount]))
                                {
                                    UniqueCompanyList.Add(UniqueCompanyListInBrick[brickCompanyCount]);
                                }
                            }
                        }
                        UniqueCompanyList.Add(0);
                        for (int k = 0; k < CompanyListToadd.Count; k++)
                        {
                            if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                            {
                                UniqueCompanyList.Add(CompanyListToadd[k]);
                            }

                        }

                        CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                        string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                        ColorCodeCompanyDataAccess = null;

                        Dictionary<Int64, string> ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<CompanyPerformance> objBrickData = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalSales).ToList();
                            Mapview_OVM CurrentMapView = new Mapview_OVM();
                            CurrentMapView.PatchID = BrickList[i].BrickID;
                            CurrentMapView.name = BrickList[i].BrickName;
                            CurrentMapView.lat = BrickList[i].lat;
                            CurrentMapView.lng = BrickList[i].lng;

                            Mapview_OVM CurrentMapViewUnit = new Mapview_OVM();
                            CurrentMapViewUnit.PatchID = BrickList[i].BrickID;
                            CurrentMapViewUnit.name = BrickList[i].BrickName;
                            CurrentMapViewUnit.lat = BrickList[i].lat;
                            CurrentMapViewUnit.lng = BrickList[i].lng;

                            ArrayList InnerArrayList = new ArrayList();

                            Double totalBrickSales = 0;
                            Double OtherBrickSales = 0;
                            IList<Int64> AlreadyAddedCompany = new List<Int64>();



                            for (int j = 0; j < objBrickData.Count; j++)
                            {
                                totalBrickSales += objBrickData[j].TotalSales;

                                if (objComparision.includeTopSearches)
                                {
                                    if (j < 10)
                                    {
                                        ArrayList Single = new ArrayList();
                                        Single.Add(objBrickData[j].CompanyName);
                                        Single.Add(objBrickData[j].TotalSales);


                                        string ColorCode1 = string.Empty;
                                        if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode1))
                                        {
                                            Single.Add(ColorCode1);

                                        }
                                        else
                                        {
                                            Single.Add(ColorCode1);
                                        }
                                        InnerArrayList.Add(Single);


                                        AlreadyAddedCompany.Add(objBrickData[j].CompanyID);




                                    }
                                }
                                if (CompanyListToadd.Contains(objBrickData[j].CompanyID))
                                {
                                    if (!AlreadyAddedCompany.Contains(objBrickData[j].CompanyID))
                                    {
                                        ArrayList Single = new ArrayList();
                                        Single.Add(objBrickData[j].CompanyName);
                                        Single.Add(objBrickData[j].TotalSales);
                                        string ColorCodeC = string.Empty;
                                        if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCodeC))
                                        {
                                            Single.Add(ColorCodeC);

                                        }
                                        else
                                        {
                                            Single.Add(ColorCodeC);
                                        }
                                        InnerArrayList.Add(Single);


                                        AlreadyAddedCompany.Add(objBrickData[j].CompanyID);
                                    }
                                }
                                else
                                {
                                    if (!AlreadyAddedCompany.Contains(objBrickData[j].CompanyID))
                                    {
                                        OtherBrickSales += objBrickData[j].TotalSales;

                                    }
                                }




                            }

                            IList<Int64> ZeroValueCompanyList = new List<Int64>();
                            for (int j = 0; j < CompanyListToadd.Count; j++)
                            {
                                if (!AlreadyAddedCompany.Contains(CompanyListToadd[j]))
                                {
                                    ZeroValueCompanyList.Add(CompanyListToadd[j]);
                                }

                            }
                            IList<CompanyDetails> ZeroValueCompanyDetails = new List<CompanyDetails>();
                            if (ZeroValueCompanyList.Count > 0)
                            {
                                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                                ZeroValueCompanyDetails = objCompanyDataAccess.GetZeroValueCompanyDetails(string.Join(",", ZeroValueCompanyList), objComparision.ComparisonType);
                                objCompanyDataAccess = null;
                            }
                            for (int k = 0; k < ZeroValueCompanyDetails.Count; k++)
                            {
                                ArrayList Single = new ArrayList();
                                Single.Add(ZeroValueCompanyDetails[k].CompanyName);
                                Single.Add(0);
                                string ColorCodeZeroValue = string.Empty;
                                if (ObjCompanyColorCode.TryGetValue(ZeroValueCompanyDetails[k].CompanyID, out ColorCodeZeroValue))
                                {
                                    Single.Add(ColorCodeZeroValue);

                                }
                                else
                                {
                                    Single.Add(ColorCodeZeroValue);
                                }
                                InnerArrayList.Add(Single);

                            }





                            ArrayList TotalBrickSalesValue = new ArrayList();
                            TotalBrickSalesValue.Add("Others");
                            TotalBrickSalesValue.Add(OtherBrickSales);
                            string ColorCode2 = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(0, out ColorCode2))
                            {
                                TotalBrickSalesValue.Add(ColorCode2);

                            }
                            else
                            {
                                TotalBrickSalesValue.Add(ColorCode2);
                            }

                            InnerArrayList.Add(TotalBrickSalesValue);


                            CurrentMapView.map_chart = new MapData();
                            CurrentMapView.map_chart.data = InnerArrayList;
                            CompanyPerformanceList.Add(CurrentMapView);


                        }



                        // Processing for Unit

                        UniqueCompanyList = new List<Int64>();
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<Int64> UniqueCompanyListInBrick = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalUnits).Select(a => a.CompanyID).Take(10).ToList();
                            for (int brickCompanyCount = 0; brickCompanyCount < UniqueCompanyListInBrick.Count; brickCompanyCount++)
                            {
                                if (!UniqueCompanyList.Contains(UniqueCompanyListInBrick[brickCompanyCount]))
                                {
                                    UniqueCompanyList.Add(UniqueCompanyListInBrick[brickCompanyCount]);
                                }
                            }
                        }
                        UniqueCompanyList.Add(0);
                        for (int k = 0; k < CompanyListToadd.Count; k++)
                        {
                            if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                            {
                                UniqueCompanyList.Add(CompanyListToadd[k]);
                            }

                        }
                        ObjCompanyColorCode = new Dictionary<Int64, string>();
                        ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 0);

                        string ColorCode = string.Empty;
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<CompanyPerformance> objBrickData = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalUnits).ToList();

                            Mapview_OVM CurrentMapViewUnit = new Mapview_OVM();
                            CurrentMapViewUnit.PatchID = BrickList[i].BrickID;
                            CurrentMapViewUnit.name = BrickList[i].BrickName;
                            CurrentMapViewUnit.lat = BrickList[i].lat;
                            CurrentMapViewUnit.lng = BrickList[i].lng;


                            ArrayList InnerArrayUnit = new ArrayList();

                            Double totalBrickUnit = 0;

                            Double OtherBrickUnits = 0;
                            IList<Int64> AlreadyAddedCompany = new List<Int64>();

                            for (int j = 0; j < objBrickData.Count; j++)
                            {

                                totalBrickUnit += objBrickData[j].TotalUnits;
                                if (objComparision.includeTopSearches)
                                {
                                    if (j < 10)
                                    {

                                        // 
                                        ArrayList SingleUnit = new ArrayList();
                                        SingleUnit.Add(objBrickData[j].CompanyName);
                                        SingleUnit.Add(objBrickData[j].TotalUnits);
                                        ColorCode = string.Empty;
                                        if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode))
                                        {
                                            SingleUnit.Add(ColorCode);

                                        }
                                        else
                                        {
                                            SingleUnit.Add(ColorCode);
                                        }


                                        InnerArrayUnit.Add(SingleUnit);

                                        AlreadyAddedCompany.Add(objBrickData[j].CompanyID);




                                    }
                                }
                                if (CompanyListToadd.Contains(objBrickData[j].CompanyID))
                                {
                                    if (!AlreadyAddedCompany.Contains(objBrickData[j].CompanyID))
                                    {

                                        ArrayList SingleUnit = new ArrayList();
                                        SingleUnit.Add(objBrickData[j].CompanyName);
                                        SingleUnit.Add(objBrickData[j].TotalUnits);
                                        ColorCode = string.Empty;
                                        if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode))
                                        {
                                            SingleUnit.Add(ColorCode);

                                        }
                                        else
                                        {
                                            SingleUnit.Add(ColorCode);
                                        }

                                        InnerArrayUnit.Add(SingleUnit);

                                        AlreadyAddedCompany.Add(objBrickData[j].CompanyID);
                                    }
                                }
                                else
                                {
                                    if (!AlreadyAddedCompany.Contains(objBrickData[j].CompanyID))
                                    {

                                        OtherBrickUnits += objBrickData[j].TotalUnits;
                                    }
                                }




                            }

                            IList<Int64> ZeroValueCompanyList = new List<Int64>();
                            for (int j = 0; j < CompanyListToadd.Count; j++)
                            {
                                if (!AlreadyAddedCompany.Contains(CompanyListToadd[j]))
                                {
                                    ZeroValueCompanyList.Add(CompanyListToadd[j]);
                                }

                            }
                            IList<CompanyDetails> ZeroValueCompanyDetails = new List<CompanyDetails>();
                            if (ZeroValueCompanyList.Count > 0)
                            {
                                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                                ZeroValueCompanyDetails = objCompanyDataAccess.GetZeroValueCompanyDetails(string.Join(",", ZeroValueCompanyList), objComparision.ComparisonType);
                                objCompanyDataAccess = null;
                            }
                            for (int k = 0; k < ZeroValueCompanyDetails.Count; k++)
                            {


                                ArrayList SingleUnit = new ArrayList();
                                SingleUnit.Add(ZeroValueCompanyDetails[k].CompanyName);
                                SingleUnit.Add(0);
                                ColorCode = string.Empty;
                                if (ObjCompanyColorCode.TryGetValue(ZeroValueCompanyDetails[k].CompanyID, out ColorCode))
                                {
                                    SingleUnit.Add(ColorCode);

                                }
                                else
                                {
                                    SingleUnit.Add(ColorCode);
                                }

                                InnerArrayUnit.Add(SingleUnit);

                            }


                            ArrayList TotalBrickUnitValue = new ArrayList();
                            TotalBrickUnitValue.Add("Others");
                            TotalBrickUnitValue.Add(OtherBrickUnits);
                            string ColorCodeC = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(0, out ColorCodeC))
                            {
                                TotalBrickUnitValue.Add(ColorCodeC);

                            }
                            else
                            {
                                TotalBrickUnitValue.Add(ColorCodeC);
                            }
                            InnerArrayUnit.Add(TotalBrickUnitValue);


                            CurrentMapViewUnit.map_chart = new MapData();
                            CurrentMapViewUnit.map_chart.data = InnerArrayUnit;
                            CompanyPerformanceUnitList.Add(CurrentMapViewUnit);
                        }


                        #endregion 


                    }
                    else
                    {
                        #region Brand SKU  Data
                        BrandDataAccess objBrandDataAccessMap = new BrandDataAccess();
                        if (objComparision.ComparisonType == "brand")
                        {
                            resultds = objBrandDataAccessMap.GetBrandPerformanceMap(objComparision, AccessType, TCCodes);
                            
                        }
                        else
                        {
                            resultds = objBrandDataAccessMap.GetSKUPerformanceMap(objComparision, AccessType, TCCodes);
                        }
                        objBrandDataAccessMap = null;
                        var result = (from row in resultds.Tables[0].AsEnumerable()
                                      select new CompanyPerformance
                                      {
                                          CompanyID = Convert.ToInt64(row["BrandID"]),
                                          CompanyName = Convert.ToString(row["BrandName"]),
                                          TotalSales = Convert.ToDouble(row["TotalSales"]),
                                          BrickID = Convert.ToInt32(row["BrickID"]),
                                          TotalUnits = Convert.ToDouble(row["TotalUnit"]),

                                      }
                                      ).ToList();


                        int TopValueCounter = 20;
                        if (objComparision.IsViewTen)
                        {
                            TopValueCounter = 10;
                        }

                        IList<Int64> CompanyListToadd = new List<Int64>();

                        if (!string.IsNullOrEmpty(objComparision.ComparisionValue))
                        {
                            if (objComparision.ComparisionValue != "all")
                            {
                                string[] SelectedCompanyList = objComparision.ComparisionValue.Split(',');
                                if (SelectedCompanyList.Length > 0)
                                {
                                    for (int j = 0; j < SelectedCompanyList.Length; j++)
                                    {
                                        CompanyListToadd.Add(Convert.ToInt64(SelectedCompanyList[j]));
                                    }
                                }
                            }
                        }



                        BrickDataAccess objBrickDataaccess = new BrickDataAccess();

                        IList<BrickMaster> BrickList = objBrickDataaccess.GetBrickListByCityID(objComparision.GeographyID, objComparision.CompanyID, objComparision.DivisonID, objComparision.PatchIds);
                        objBrickDataaccess = null;



                        IList<Int64> UniqueCompanyList = new List<Int64>();
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<Int64> UniqueCompanyListInBrick = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalSales).Select(a => a.CompanyID).Take(10).ToList();
                            for (int brickCompanyCount = 0; brickCompanyCount < UniqueCompanyListInBrick.Count; brickCompanyCount++)
                            {
                                if (!UniqueCompanyList.Contains(UniqueCompanyListInBrick[brickCompanyCount]))
                                {
                                    UniqueCompanyList.Add(UniqueCompanyListInBrick[brickCompanyCount]);
                                }
                            }
                        }
                        UniqueCompanyList.Add(0);
                        for (int k = 0; k < CompanyListToadd.Count; k++)
                        {
                            if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                            {
                                UniqueCompanyList.Add(CompanyListToadd[k]);
                            }

                        }

                        CompanyDataAccess ColorCodeCompanyDataAccess = new CompanyDataAccess();
                        string CompanyColorCode = ColorCodeCompanyDataAccess.GetSubscribedCompanyColor(objComparision.CompanyID);
                        ColorCodeCompanyDataAccess = null;
                        Dictionary<Int64, string> ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                        string ColorCode = string.Empty;
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<CompanyPerformance> objBrickData = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalSales).ToList();
                            Mapview_OVM CurrentMapView = new Mapview_OVM();
                            CurrentMapView.PatchID = BrickList[i].BrickID;
                            CurrentMapView.name = BrickList[i].BrickName;
                            CurrentMapView.lat = BrickList[i].lat;
                            CurrentMapView.lng = BrickList[i].lng;


                            ArrayList InnerArrayList = new ArrayList();

                            Double totalBrickSales = 0;

                            Double OtherBrickSales = 0;

                            IList<Int64> AlreadyAddedBrand = new List<Int64>();
                            for (int j = 0; j < objBrickData.Count; j++)
                            {
                                totalBrickSales += objBrickData[j].TotalSales;

                                if (objComparision.includeTopSearches)
                                {
                                    if (j < 10)
                                    {
                                        if (!AlreadyAddedBrand.Contains(objBrickData[j].CompanyID))
                                        {
                                            ArrayList Single = new ArrayList();
                                            Single.Add(objBrickData[j].CompanyName);
                                            Single.Add(objBrickData[j].TotalSales);

                                            ColorCode = string.Empty;
                                            if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode))
                                            {
                                                Single.Add(ColorCode);

                                            }
                                            else
                                            {
                                                Single.Add(ColorCode);
                                            }

                                            InnerArrayList.Add(Single);

                                            AlreadyAddedBrand.Add(objBrickData[j].CompanyID);
                                        }
                                    }
                                }

                                if (CompanyListToadd.Contains(objBrickData[j].CompanyID) && !AlreadyAddedBrand.Contains(objBrickData[j].CompanyID))
                                {
                                    ArrayList Single = new ArrayList();
                                    Single.Add(objBrickData[j].CompanyName);
                                    Single.Add(objBrickData[j].TotalSales);
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode))
                                    {
                                        Single.Add(ColorCode);

                                    }
                                    else
                                    {
                                        Single.Add(ColorCode);
                                    }
                                    InnerArrayList.Add(Single);

                                    AlreadyAddedBrand.Add(objBrickData[j].CompanyID);
                                }
                                else
                                {
                                    if (!AlreadyAddedBrand.Contains(objBrickData[j].CompanyID))
                                    {
                                        OtherBrickSales += objBrickData[j].TotalSales;

                                    }
                                }



                            }
                            // Zero Value Brands



                            IList<Int64> ZeroValueCompanyList = new List<Int64>();
                            for (int j = 0; j < CompanyListToadd.Count; j++)
                            {
                                if (!AlreadyAddedBrand.Contains(CompanyListToadd[j]))
                                {
                                    ZeroValueCompanyList.Add(CompanyListToadd[j]);
                                }

                            }
                            IList<BrandDetails> ZeroValueBrandDetails = new List<BrandDetails>();
                            if (ZeroValueCompanyList.Count > 0)
                            {
                                ProductDataAccess objProductDataAccess = new ProductDataAccess();
                                ZeroValueBrandDetails = objProductDataAccess.GetZeroValueBrandDetails(string.Join(",", ZeroValueCompanyList), objComparision.ComparisonType);
                                objProductDataAccess = null;
                            }
                            for (int k = 0; k < ZeroValueBrandDetails.Count; k++)
                            {
                                ArrayList Single = new ArrayList();
                                Single.Add(ZeroValueBrandDetails[k].Brand);
                                Single.Add(0);
                                ColorCode = string.Empty;
                                if (ObjCompanyColorCode.TryGetValue(ZeroValueBrandDetails[k].BrandID, out ColorCode))
                                {
                                    Single.Add(ColorCode);

                                }
                                else
                                {
                                    Single.Add(ColorCode);
                                }
                                InnerArrayList.Add(Single);

                            }

                            ArrayList TotalBrickSalesValue = new ArrayList();
                            TotalBrickSalesValue.Add("Others");
                            TotalBrickSalesValue.Add(OtherBrickSales);
                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(0, out ColorCode))
                            {
                                TotalBrickSalesValue.Add(ColorCode);

                            }
                            else
                            {
                                TotalBrickSalesValue.Add(ColorCode);
                            }
                            InnerArrayList.Add(TotalBrickSalesValue);




                            CurrentMapView.map_chart = new MapData();
                            CurrentMapView.map_chart.data = InnerArrayList;

                            CompanyPerformanceList.Add(CurrentMapView);

                        }


                        // For Unit

                        UniqueCompanyList = new List<Int64>();
                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<Int64> UniqueCompanyListInBrick = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalUnits).Select(a => a.CompanyID).Take(10).ToList();
                            for (int brickCompanyCount = 0; brickCompanyCount < UniqueCompanyListInBrick.Count; brickCompanyCount++)
                            {
                                if (!UniqueCompanyList.Contains(UniqueCompanyListInBrick[brickCompanyCount]))
                                {
                                    UniqueCompanyList.Add(UniqueCompanyListInBrick[brickCompanyCount]);
                                }
                            }
                        }
                        UniqueCompanyList.Add(0);
                        for (int k = 0; k < CompanyListToadd.Count; k++)
                        {
                            if (!UniqueCompanyList.Contains(CompanyListToadd[k]))
                            {
                                UniqueCompanyList.Add(CompanyListToadd[k]);
                            }

                        }

                        ObjCompanyColorCode = new Dictionary<Int64, string>();
                        ObjCompanyColorCode = this.GetCompanyColorCode(UniqueCompanyList, objComparision.CompanyID, CompanyColorCode, objComparision.SessionID, 1);
                        ColorCode = string.Empty;

                        for (int i = 0; i < BrickList.Count; i++)
                        {
                            IList<CompanyPerformance> objBrickData = result.Where(a => a.BrickID == BrickList[i].BrickID).OrderByDescending(b => b.TotalUnits).ToList();

                            Mapview_OVM CurrentMapViewUnit = new Mapview_OVM();
                            CurrentMapViewUnit.PatchID = BrickList[i].BrickID;
                            CurrentMapViewUnit.name = BrickList[i].BrickName;
                            CurrentMapViewUnit.lat = BrickList[i].lat;
                            CurrentMapViewUnit.lng = BrickList[i].lng;


                            ArrayList InnerArrayUnit = new ArrayList();

                            Double totalBrickUnit = 0;

                            Double OtherBrickUnits = 0;
                            IList<Int64> AlreadyAddedBrand = new List<Int64>();
                            for (int j = 0; j < objBrickData.Count; j++)
                            {

                                totalBrickUnit += objBrickData[j].TotalUnits;
                                if (objComparision.includeTopSearches)
                                {
                                    if (j < 10)
                                    {
                                        if (!AlreadyAddedBrand.Contains(objBrickData[j].CompanyID))
                                        {

                                            ArrayList SingleUnit = new ArrayList();
                                            SingleUnit.Add(objBrickData[j].CompanyName);
                                            SingleUnit.Add(objBrickData[j].TotalUnits);
                                            ColorCode = string.Empty;
                                            if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode))
                                            {
                                                SingleUnit.Add(ColorCode);

                                            }
                                            else
                                            {
                                                SingleUnit.Add(ColorCode);
                                            }
                                            InnerArrayUnit.Add(SingleUnit);

                                            AlreadyAddedBrand.Add(objBrickData[j].CompanyID);
                                        }
                                    }
                                }

                                if (CompanyListToadd.Contains(objBrickData[j].CompanyID) && !AlreadyAddedBrand.Contains(objBrickData[j].CompanyID))
                                {

                                    ArrayList SingleUnit = new ArrayList();
                                    SingleUnit.Add(objBrickData[j].CompanyName);
                                    SingleUnit.Add(objBrickData[j].TotalUnits);
                                    ColorCode = string.Empty;
                                    if (ObjCompanyColorCode.TryGetValue(objBrickData[j].CompanyID, out ColorCode))
                                    {
                                        SingleUnit.Add(ColorCode);

                                    }
                                    else
                                    {
                                        SingleUnit.Add(ColorCode);
                                    }
                                    InnerArrayUnit.Add(SingleUnit);

                                    AlreadyAddedBrand.Add(objBrickData[j].CompanyID);
                                }
                                else
                                {
                                    if (!AlreadyAddedBrand.Contains(objBrickData[j].CompanyID))
                                    {

                                        OtherBrickUnits += objBrickData[j].TotalUnits;
                                    }
                                }



                            }
                            // Zero Value Brands



                            IList<Int64> ZeroValueCompanyList = new List<Int64>();
                            for (int j = 0; j < CompanyListToadd.Count; j++)
                            {
                                if (!AlreadyAddedBrand.Contains(CompanyListToadd[j]))
                                {
                                    ZeroValueCompanyList.Add(CompanyListToadd[j]);
                                }

                            }
                            IList<BrandDetails> ZeroValueBrandDetails = new List<BrandDetails>();
                            if (ZeroValueCompanyList.Count > 0)
                            {
                                ProductDataAccess objProductDataAccess = new ProductDataAccess();
                                ZeroValueBrandDetails = objProductDataAccess.GetZeroValueBrandDetails(string.Join(",", ZeroValueCompanyList), objComparision.ComparisonType);
                                objProductDataAccess = null;
                            }
                            for (int k = 0; k < ZeroValueBrandDetails.Count; k++)
                            {

                                ArrayList SingleUnit = new ArrayList();
                                SingleUnit.Add(ZeroValueBrandDetails[k].Brand);
                                SingleUnit.Add(0);
                                ColorCode = string.Empty;
                                if (ObjCompanyColorCode.TryGetValue(ZeroValueBrandDetails[k].BrandID, out ColorCode))
                                {
                                    SingleUnit.Add(ColorCode);

                                }
                                else
                                {
                                    SingleUnit.Add(ColorCode);
                                }
                                InnerArrayUnit.Add(SingleUnit);
                            }


                            ArrayList TotalBrickUnitValue = new ArrayList();
                            TotalBrickUnitValue.Add("Others");
                            TotalBrickUnitValue.Add(OtherBrickUnits);
                            ColorCode = string.Empty;
                            if (ObjCompanyColorCode.TryGetValue(0, out ColorCode))
                            {
                                TotalBrickUnitValue.Add(ColorCode);

                            }
                            else
                            {
                                TotalBrickUnitValue.Add(ColorCode);
                            }

                            InnerArrayUnit.Add(TotalBrickUnitValue);




                            CurrentMapViewUnit.map_chart = new MapData();
                            CurrentMapViewUnit.map_chart.data = InnerArrayUnit;
                            CompanyPerformanceUnitList.Add(CurrentMapViewUnit);

                        
                        }

                        #endregion
                    }



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            objCompanyPerformanceData.CompanyPerformanceValueList = CompanyPerformanceList;
            objCompanyPerformanceData.CompanyPerformanceUnitList = CompanyPerformanceUnitList;
            return objCompanyPerformanceData;
        }

        public int AddUpdateSubscribedCompany(SubscribedCompany_IVM objCompany)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {
                if (objCompany != null)
                {
                    param = objCompany.CompanyID + ",";
                    param += objCompany.NoOfDivision + ",";
                    param += objCompany.TeamStrength + ",";
                    param += objCompany.SubscriptionTypeID + ",";
                    param += "'" + objCompany.SubscriptionLocationID + "',";
                    param += objCompany.Action + ",";
                    param += objCompany.CityID + ",";
                    param += objCompany.StateID + ",";
                    param += objCompany.Pincode + ",";
                    param += "'" + objCompany.Address + "',";
                    param += "'" + objCompany.LandMark + "',";
                    param += "'" + objCompany.Country + "',";
                    param += "'" + objCompany.SubscriptionStartDate + "',";
                    param += "'" + objCompany.SubscriptionEndDate + "',";
                    param += "'" + objCompany.ContactInfo.name + "',";
                    param += objCompany.ContactInfo.Phone + ",";
                    param += "'" + objCompany.ContactInfo.email + "',";
                    param += "'" + objCompany.ContactInfo.Address + "',";

                    param += "'" + objCompany.ProfileImage + "',";
                    param += "'" + objCompany.CityName + "',";
                    param += "'" + objCompany.ColorCode + "',";
                    param += "'" + objCompany.SubscriptionPeriodTypeIDs + "',";
                    param += "" + objCompany.DefaultPeriodTypeID + ",";
                    param += "" + objCompany.DataSubscriptionTypeID ;
                  


                }
                int ResultValidate = 0;
                if (!objCompany.isUpdate)
                {
                    string strUserValidate = "Exec" + " " + IMSBI.Common.Constants.DB_SP_ValidateUserName + " '" + objCompany.UserInfo.UserName + "'";
                    var objValidate = IMSBIDB.ReturnValue(strUserValidate);
                    ResultValidate = Convert.ToInt32(objValidate);
                }
                if (ResultValidate == 0)
                {
                    string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_InsertUpdateSubscribedCompany + " " + param;
                    var obj = IMSBIDB.ReturnValue(str);
                    result = Convert.ToInt32(obj);
                    if (result > 0)
                    {
                        // Process Admin User

                        if (objCompany.UserInfo != null && !objCompany.isUpdate)
                        {
                            User_IVM objUserIVM = new User_IVM
                            {
                                UserID = 0,
                                DivisionID = 0,
                                CompanyID = objCompany.CompanyID,
                                UserName = objCompany.UserInfo.UserName,
                                Password = objCompany.UserInfo.Password,
                                Email = "",
                                MobileNo = 0,
                                Active = 1,
                                Pincode = objCompany.Pincode,
                                RoleID = (int)UserRoleEnum.A

                            };

                            UserDataAccess objUserDataAccess = new UserDataAccess();
                            objUserDataAccess.AddUpdateUser(objUserIVM);
                            objUserDataAccess = null;
                        }

                        if (!objCompany.isUpdate && objCompany.Divisions != null)

                        {
                            // Process Division
                            string[] DivisionList = objCompany.Divisions.Split(',');
                            if (DivisionList.Length > 0)
                            {
                                DivisionDataAccess objDivisionDataaccess = new DivisionDataAccess();
                                for (int i = 0; DivisionList.Length > i; i++)
                                {
                                    Division_IVM objDivision = new Division_IVM
                                    {
                                        CompanyID = objCompany.CompanyID,
                                        DivisionID = 0,
                                        DivisionName = DivisionList[i],
                                        DataSubscriptionTypeID = objCompany.DataSubscriptionTypeID,
                                        SubscribedPeriodTypeIDs = objCompany.SubscriptionPeriodTypeIDs,
                                        DefaultPeriodTypeID = objCompany.DefaultPeriodTypeID
                                    };
                                    var DivisionID = objDivisionDataaccess.AddUpdateCompanyDivision(objDivision);
                                }
                                objDivisionDataaccess = null;
                            }
                        }
                    }
                    //Process Access rights
                    if (objCompany.CompanyAccessRights != null)
                    {
                        AccessRightsDataAccess objAccessRights = new AccessRightsDataAccess();
                        objAccessRights.CreateUpdateCompanyAccessRights(objCompany.CompanyAccessRights);
                        objAccessRights = null;
                    }

                    // Following Function will inherit the changes of Company to user
                    if (result == 0)
                    {
                        if (objCompany.CompanyAccessRights != null)
                        {
                            UserBusinessManager objUserBusinessManager = new UserBusinessManager();
                            IList<CompanyUser> objCompanyUsers = objUserBusinessManager.GetCompanyUsers(objCompany.CompanyID);
                            if (objCompanyUsers != null && objCompanyUsers.Count > 0)
                            {
                                for (int i = 0; i < objCompanyUsers.Count; i++)
                                {

                                    AccessRightsDataAccess objAccessRights = new AccessRightsDataAccess();
                                    UserAccessRights objUserAccessrights = objAccessRights.GetUserAccessRights(objCompanyUsers[i].UserID);
                                    string TCValue = string.Empty;
                                    string CityIDs = string.Empty;

                                    if (objUserAccessrights != null)
                                    {
                                        string CurrentTCValues = objUserAccessrights.TCValues;

                                        string CurrentCityIDs = objUserAccessrights.CityIDs;
                                        string CompanyTCValues = objCompany.CompanyAccessRights.TCValues;

                                        string CompanyCurrentCityIDs = objCompany.SubscriptionLocationID;
                                        if (!string.IsNullOrEmpty(CurrentTCValues))
                                        {
                                            IList<string> UserTCList = CurrentTCValues.Split(',');
                                            IList<string> CompanyTCValuesList = CompanyTCValues.Split(',');
                                            IList<string> NewTCList = new List<string>();
                                            for (int j = 0; j < UserTCList.Count; j++)
                                            {
                                                if (CompanyTCValuesList.Contains(UserTCList[j]))
                                                {
                                                    NewTCList.Add(UserTCList[j]);
                                                }
                                            }
                                            TCValue = string.Join(",", NewTCList);
                                        }

                                        if (!string.IsNullOrEmpty(CurrentCityIDs))
                                        {
                                            IList<string> CompanyCurrentCityIDList = CompanyCurrentCityIDs.Split(',');
                                            IList<string> UserCityList = CurrentCityIDs.Split(',');
                                            IList<Int32> UserNewCityID = new List<Int32>();
                                            for (int j = 0; j < UserCityList.Count; j++)
                                            {
                                                if (CompanyCurrentCityIDList.Contains(UserCityList[j]))
                                                {
                                                    UserNewCityID.Add(Convert.ToInt32(UserCityList[j]));
                                                }
                                            }
                                            CityIDs = string.Join(",", UserNewCityID);

                                        }
                                    }
                                    objAccessRights.UpdateUserAccessRights(objCompanyUsers[i].UserID, 1, TCValue, CityIDs);
                                    objAccessRights = null;

                                }
                            }
                        }
                    }
                }
                else
                {
                    return -1;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objCompany.CompanyID;
        }

        public int AddUpdateSubscriptionType(SubscriptionType_IVM objSubscriptionType)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {
                if (objSubscriptionType != null)
                {
                    param = objSubscriptionType.SubscriptionTypeID + ",";
                    param += "'" + objSubscriptionType.SubscriptionName + "',";
                    param += objSubscriptionType.active + ",";
                    param += objSubscriptionType.SubscriptionPeriod + ",";
                    param += "'" + objSubscriptionType.Gelocation + "',";
                    param += objSubscriptionType.ViewDoctorData + ",";
                    param += objSubscriptionType.NoOfDoctor + ",";
                    param += objSubscriptionType.ViewChemistData + ",";
                    param += objSubscriptionType.NoofChemist + ",";
                    param += objSubscriptionType.ViewHospitalData + ",";
                    param += objSubscriptionType.NoOfHospital + ",";
                    param += "'" + objSubscriptionType.ColorCode + "'";


                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateUpdateSubscriptionType + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public IList<SubscriptionType_OVM> GetSubscriptionTypeDetails(int SubscriptionTypeID)
        {
            IList<SubscriptionType_OVM> SubscriptionTypeDetails = new List<SubscriptionType_OVM>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSubscriptionTypeInfo + " " + SubscriptionTypeID;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SubscriptionType_OVM
                              {
                                  SubscriptionTypeID = !string.IsNullOrEmpty(Convert.ToString(row["SubscriptionTypeID"])) ? Convert.ToInt32(row["SubscriptionTypeID"]) : 0,
                                  SubscriptionName = Convert.ToString(row["SubscriptionName"]),
                                  active = Convert.ToInt32(row["active"]),
                                  SubscriptionPeriod = !string.IsNullOrEmpty(Convert.ToString(row["SubscriptionPeriod"])) ? Convert.ToInt32(row["SubscriptionPeriod"]) : 0,
                                  Gelocation = Convert.ToString(row["Geolocation"]),
                                  ViewDoctorData = !string.IsNullOrEmpty(Convert.ToString(row["ViewDoctorData"])) ? Convert.ToBoolean(row["ViewDoctorData"]) : false,
                                  NoOfDoctor = !string.IsNullOrEmpty(Convert.ToString(row["NoOfDoctor"])) ? Convert.ToInt32(row["NoOfDoctor"]) : 0,
                                  ViewChemistData = !string.IsNullOrEmpty(Convert.ToString(row["ViewChemistData"])) ? Convert.ToBoolean(row["ViewChemistData"]) : false,
                                  NoofChemist = !string.IsNullOrEmpty(Convert.ToString(row["NoofChemist"])) ? Convert.ToInt32(row["NoofChemist"]) : 0,
                                  ViewHospitalData = !string.IsNullOrEmpty(Convert.ToString(row["ViewHospitalData"])) ? Convert.ToBoolean(row["ViewHospitalData"]) : false,
                                  NoOfHospital = !string.IsNullOrEmpty(Convert.ToString(row["NoOfHospital"])) ? Convert.ToInt32(row["NoOfHospital"]) : 0,
                                  ColorCode = Convert.ToString(row["ColorCode"])


                              }
                              ).ToList();

                SubscriptionTypeDetails = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return SubscriptionTypeDetails;
        }


        // CompanyCluster information

        public int AddUpdateCluster(CompanyCluster objcluster)
        {
            int result = 0;

            try
            {
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                result = objCompanyDataAccess.AddUpdateCluster(objcluster);
                objCompanyDataAccess = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public CompanyCluster GetCompanyClusterInfo(Int32 ClusterID)
        {
            CompanyCluster ObjCompanyCluster = new CompanyCluster();
            try
            {
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                ObjCompanyCluster = objCompanyDataAccess.GetCompanyClusterInfo(ClusterID);
                objCompanyDataAccess = null;
            }
            catch
            {

            }
            return ObjCompanyCluster;
        }


        public IList<CompanyCluster> GetCompanyClusterList(int CompanyID, int CityID)
        {
            IList<CompanyCluster> CompanyClusterList = new List<CompanyCluster>();
            try
            {
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                CompanyClusterList = objCompanyDataAccess.GetCompanyClusterList(CompanyID, CityID);
                objCompanyDataAccess = null;
            }
            catch
            {
                throw;
            }
            return CompanyClusterList;
        }

        public bool GenerateCompanyCVMData(int CompanyID)
        {
            bool Status = false; ;
            try
            {
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                Status = objCompanyDataAccess.GenerateCompanyCVMData(CompanyID);
                objCompanyDataAccess = null;
            }
            catch
            {
                throw;
            }
            return Status;
        }


        /// <summary>
        /// Filter type will be 0- Company, 1- Brand and 2 - SKU
        /// </summary>
        /// <param name="CompanyIDList"></param>
        /// <param name="CompanyID"></param>
        /// <param name="CompanyColorCode"></param>
        /// <param name="SessionID"></param>
        /// <param name="FilterType"></param>
        /// <returns></returns>
        private Dictionary<Int64, string> GetCompanyColorCode(IList<Int64> CompanyIDList, Int64 CompanyID, string CompanyColorCode, string SessionID, int FilterType)
        {
            // Dictionary<int, string> objCompanyColorCode = new Dictionary<int, string>();
            string[] ColorCode = new string[] { "#8D6EB1","#53C2B4","#A3CF5A","#093A1F","#D6D627","#BDB268","#ECB32B","#523973","#F58020","#D82729","#184376","#41F447","#2784A3","#47D1FF","#FC4E77","#9F247F","#F4F441","#F441D6","#8B41F4","#41F4F4","#721B25","#6E7272","#CC0052","#BA6E05","#600080","#00FFFF","#00E68A","#ffff00","#4B5320","#FF9966","#98777B","#00FF6F","#BDB76B","#E9967A","#008000","#FFD1DC","#D1E231","#826644","#C0C0C0","#800080","#000080","#8b4513","#ff0000","#ff6347","#E8ADAA",
"#9400D3","#DFFF00","#FF8C00","#B38481" };
            if (!string.IsNullOrEmpty(CompanyColorCode))
            {
                if (ColorCode.Contains(CompanyColorCode))
                {
                    ColorCode = ColorCode.Where(val => val != CompanyColorCode).ToArray();
                }
            }

            SessionData objSessionData = new SessionData();
            if (!string.IsNullOrEmpty(SessionID))
            {
                UserDataAccess objUserDataAccess = new UserDataAccess();
                objSessionData = objUserDataAccess.GetSessionData(SessionID);
                objUserDataAccess = null;
            }
            string SessionColorCode = string.Empty;
            string UsedColorCode = string.Empty;

            switch (FilterType)
            {
                case 0:
                    SessionColorCode = objSessionData.CompanyColorCodes;
                    UsedColorCode = objSessionData.CompanyUsedColorCode;
                    break;
                case 1:
                    SessionColorCode = objSessionData.BrandColorCodes;
                    UsedColorCode = objSessionData.BrandUsedColorCode;
                    break;
                case 2:
                    SessionColorCode = objSessionData.SKUColorCodes;
                    UsedColorCode = objSessionData.SKUUsedColorCode;
                    break;
            }

            Dictionary<Int64, string> ObjSessionColorCode = new Dictionary<Int64, string>();
            if (!string.IsNullOrEmpty(SessionColorCode))
            {
                ObjSessionColorCode = JsonConvert.DeserializeObject<Dictionary<Int64, string>>(SessionColorCode);
            }


            IList<string> UsedColorCodeArray = new List<string>();
            if (!string.IsNullOrEmpty(UsedColorCode))
            {
                UsedColorCodeArray = UsedColorCode.Split(',').ToList();
            }
            // int ColorCodeLengthCounter = ColorCode.Length;
            bool IsJsonUpdate = false;
            for (int i = 0; i < CompanyIDList.Count; i++)
            {
                if (!ObjSessionColorCode.ContainsKey(CompanyIDList[i]))
                {
                    if (CompanyIDList[i] == CompanyID && !string.IsNullOrEmpty(CompanyColorCode))
                    {
                        ObjSessionColorCode.Add(CompanyIDList[i], CompanyColorCode);
                        IsJsonUpdate = true;
                    }
                    else
                    {

                        string CurrentColorCode = this.GetColorCode(UsedColorCodeArray, ColorCode);
                        ObjSessionColorCode.Add(CompanyIDList[i], CurrentColorCode);
                        IsJsonUpdate = true;
                        UsedColorCodeArray.Add(CurrentColorCode);
                    }

                }
            }

            if (IsJsonUpdate)
            {

                // Covert List to string 
                SessionColorCode = JsonConvert.SerializeObject(ObjSessionColorCode);

                UsedColorCode = string.Join(",", UsedColorCodeArray);
                switch (FilterType)
                {
                    case 0:
                        objSessionData.CompanyColorCodes = SessionColorCode;
                        objSessionData.CompanyUsedColorCode = UsedColorCode;
                        break;
                    case 1:

                        objSessionData.BrandColorCodes = SessionColorCode;
                        objSessionData.BrandUsedColorCode = UsedColorCode;
                        break;
                    case 2:

                        objSessionData.SKUColorCodes = SessionColorCode;
                        objSessionData.SKUUsedColorCode = UsedColorCode;
                        break;
                }

                // Update the data with Used ColorCode
                UserDataAccess objUserDataAccess = new UserDataAccess();
                bool status = objUserDataAccess.UpdateSessionData(SessionID, objSessionData);
                objUserDataAccess = null;
            }

            return ObjSessionColorCode;

        }

        private string GetColorCode(IList<string> UsedColorCode, string[] ColorCode)
        {
            string CurrentColorCode = string.Empty;


            bool IsColorCodeFound = false;
            for (int i = 0; i < ColorCode.Length; i++)
            {
                if (!UsedColorCode.Contains(ColorCode[i]))
                {
                    IsColorCodeFound = true;
                    return ColorCode[i];
                }
            }
            if (!IsColorCodeFound)
            {
                CurrentColorCode = "#" + RandomString(6);
            }
            return CurrentColorCode;
        }

        private static Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "ABCDEF0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public CompanyAccessRights GetCompanyAccessRights(Int32 CompanyID)
        {
            CompanyAccessRights objCompanyAccessRights = new CompanyAccessRights();
            AccessRightsDataAccess objAccessRightData = new AccessRightsDataAccess();
            objCompanyAccessRights = objAccessRightData.GetCompanyAccessRights(CompanyID);
            objAccessRightData = null;
            return objCompanyAccessRights;

        }
        public CompanySubscriptionDetails GetCompanySubscribedCityTCClass(Int32 CompanyID)
        {
            CompanySubscriptionDetails objSubscriptionDetails = new CompanySubscriptionDetails();
            AccessRightsDataAccess objAccessRightData = new AccessRightsDataAccess();
            CityDataAccess objCityDataAccess = new CityDataAccess();
            objSubscriptionDetails.CityList = objCityDataAccess.GetCityListByCompany(CompanyID, 1, "");
            objCityDataAccess = null;
            objSubscriptionDetails.IsAllAccess = true;
            CompanyAccessRights objCompanyAccessRights = objAccessRightData.GetCompanyAccessRights(CompanyID);
            if (objCompanyAccessRights != null)
                switch (objCompanyAccessRights.AccessRightType)
                {
                    case 1:
                        objSubscriptionDetails.IsAllAccess = true;
                        break;
                    case 2:
                        // Get List of TC Codes using SuperGroups 
                        FilterBusinessManger objFilterBusinessManager2 = new FilterBusinessManger();
                        IList<string> TCCodes = objFilterBusinessManager2.GetTCCodebySuperGroup(objCompanyAccessRights.TCValues);
                        objSubscriptionDetails.TherapyList = objFilterBusinessManager2.GetTherapyListByTherapyCodes(string.Join(",", TCCodes));
                        objFilterBusinessManager2 = null;
                        objSubscriptionDetails.IsAllAccess = false;
                        break;
                    case 3:
                        FilterBusinessManger objFilterBusinessManager = new FilterBusinessManger();
                        objSubscriptionDetails.TherapyList = objFilterBusinessManager.GetTherapyListByTherapyCodes(objCompanyAccessRights.TCValues);
                        objFilterBusinessManager = null;
                        objSubscriptionDetails.IsAllAccess = false;
                        break;
                    default:
                        objSubscriptionDetails.IsAllAccess = true;
                        break;

                }



            return objSubscriptionDetails;

        }


     

      
      
        public IList<ProductData> GetProductCM(Int64 ProductID)
        {
            IList<ProductData> ProductList = new List<ProductData>();
            try
            {
                CompanyDataAccess objCompanyDataAccess = new CompanyDataAccess();
                ProductList = objCompanyDataAccess.GetProductCVM(ProductID);
            }
            catch
            {
                throw;
            }
            return ProductList;

        }

        public bool AddProductCVM(AddProductCVM ProductCVM)
        {
            bool Status = false;
            try
            {
                // Delete Product CVM

                CompanyDataAccess objDataAccess = new CompanyDataAccess();
                objDataAccess.DeleteProductCVM(ProductCVM.ProductID);
                if (!string.IsNullOrEmpty(ProductCVM.MappedProductIds))
                {
                    string[] ProductIds = ProductCVM.MappedProductIds.Split(',');
                    for (int i = 0; i < ProductIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(ProductIds[i]))
                        {
                            int result = objDataAccess.AddProductCVM(ProductCVM.ProductID, ProductCVM.CompanyID, Convert.ToInt32(ProductIds[i]));
                            if (result == 1)
                            {
                                Status = true;
                            }
                        }
                    }
                }

                objDataAccess = null;


            }
            catch
            {
                throw;
            }

            return Status;

        }

        public CompanyBrandPerformance_OVM GetCompanyBrandPerformance (CompanyBrandPerformance_IVM objCompanyBrandPerformanceIVM)
        {
            CompanyBrandPerformance_OVM objCompanyBrandPerformanceOVM = new CompanyBrandPerformance_OVM();
            try
            {
                // Validate the session 
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                int UserID = objSessionDataAccess.ValidateSession(objCompanyBrandPerformanceIVM.SessionID);
                objSessionDataAccess = null;
                if (UserID == 0)
                {
                    objCompanyBrandPerformanceOVM = null;
                    return objCompanyBrandPerformanceOVM;
                }

                // Access log
                UserLog_VM objLogDetails = new UserLog_VM()
                {
                    UserID = UserID,
                    MenuID = 5,
                    CityID = objCompanyBrandPerformanceIVM.GeographyID,

                    DataType = "CVM",
                    EndPeriod = objCompanyBrandPerformanceIVM.PeriodEnd,
                    StartPeriod = objCompanyBrandPerformanceIVM.PeriodStart,
                    PatchID = objCompanyBrandPerformanceIVM.PatchIds,
                    PeriodType = objCompanyBrandPerformanceIVM.PeriodType,

                    BrandID = objCompanyBrandPerformanceIVM.BrandID.ToString(),
                    SessionID = objCompanyBrandPerformanceIVM.SessionID


                };
                UserLogAccessDetails objUserLogAccessDetails = new UserLogAccessDetails();
                objUserLogAccessDetails.AddUpdateUserLog(objLogDetails);
                objUserLogAccessDetails = null;
                // Get UserAccessRights 

                AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

                int AccessType = 1;
                string TCCodes = string.Empty;
                string CityIDs = string.Empty;
                int AccessRightsType = 1;
                if (objUserAccessrights != null && !string.IsNullOrEmpty(objUserAccessrights.CityIDs))
                {
                    CityIDs = objUserAccessrights.CityIDs;
                }
                if (objUserAccessrights != null && objUserAccessrights.AccessRightType == 1 && !string.IsNullOrEmpty(objUserAccessrights.CityIDs))
                {
                    AccessRightsType = 2;
                    CityIDs = objUserAccessrights.CityIDs;
                }
                else  if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
                {

                    AccessType = 2;
                    TCCodes = objUserAccessrights.TCValues;

                }
                else
                {
                    CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objCompanyBrandPerformanceIVM.CompanyID);
                    if (objCompanyAccessRight != null)
                    {
                        AccessRightsType = 1;
                        switch (objCompanyAccessRight.AccessRightType)
                        {
                            case 2:
                                AccessType = 2;
                                FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                                IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                                TCCodes = string.Join(",", TCCodesList);
                                break;
                            case 3:
                                AccessType = 2;
                                TCCodes = objCompanyAccessRight.TCValues;
                                break;
                        }
                    }
                }

                objAccessRightsDataAccess = null;
                // Get selected city name

                // Get Subscriped City By Company ID
                CityDataAccess objCityDataAccess = new CityDataAccess();
                IList<cityMaster> CityList = objCityDataAccess.GetCityListByCompany(objCompanyBrandPerformanceIVM.CompanyID, AccessRightsType, CityIDs );
                objCityDataAccess = null;


               
               
                // Current Period Data set
                DataSet resultds;
              
                // Previous Period DataSet
                DataSet resultdsPrev;

                ProductDataAccess objProductDataAccess = new ProductDataAccess();
                IList<ProductData> objProductList = objProductDataAccess.GetProductsByBrand(objCompanyBrandPerformanceIVM.BrandID.ToString());
                objProductDataAccess = null;
                IList<string> TCCodeList = objProductList.Select(a => a.TCCode).Distinct().ToList();
                string TCCode = string.Empty;
                if (TCCodeList != null)
                {
                    TCCode = string.Join(",", TCCodeList);
                }
                FilterBusinessManger objFilterBusinessManager2 = new FilterBusinessManger();
                IList<TherapyMaster> objTherapyList = objFilterBusinessManager2.GetTherapyListByTherapyCodes(TCCode);
                objFilterBusinessManager2 = null;
                string TherapyName = string.Empty;
                if(objTherapyList != null && objTherapyList.Count>0)
                {
                    TherapyName = string.Join(",", objTherapyList.Select(a => a.TherapyName).ToList());
                }
                CompanyDataAccess objCompanyDataAccessCp = new CompanyDataAccess();
                resultds = objCompanyDataAccessCp.GetCompanyBrandPerformance(objCompanyBrandPerformanceIVM, objCompanyBrandPerformanceIVM.PeriodStart, TCCode);
                resultdsPrev = objCompanyDataAccessCp.GetCompanyBrandPerformance(objCompanyBrandPerformanceIVM, objCompanyBrandPerformanceIVM.PeriodEnd, TCCode);
                objCompanyDataAccessCp = null;
                objCompanyBrandPerformanceOVM.TCCategoryName = TherapyName;

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new BrandPerformance
                              {
                                  BrandID = Convert.ToInt32(row["BrandID"]),
                                  BrandName = Convert.ToString(row["BrandName"]),
                                  TotalSales = Convert.ToDouble(row["TotalSales"]),
                                  TotalUnits = Convert.ToDouble(row["TotalUnit"]),
                                  BrickID = Convert.ToInt32(row["BrickID"]),
                                  CompanyName = Convert.ToString(row["CompanyName"]),
                                  CityID = Convert.ToInt32(row["CityID"])
                                  
                                
                              }
                            ).ToList();

                        var resultPrv = (from row in resultdsPrev.Tables[0].AsEnumerable()
                                      select new BrandPerformance
                                      {
                                          BrandID = Convert.ToInt32(row["BrandID"]),
                                          BrandName = Convert.ToString(row["BrandName"]),
                                          TotalSales = Convert.ToDouble(row["TotalSales"]),
                                          TotalUnits = Convert.ToDouble(row["TotalUnit"]),
                                          BrickID = Convert.ToInt32(row["BrickID"]),
                                          CompanyName = Convert.ToString(row["CompanyName"]),
                                           CityID = Convert.ToInt32(row["CityID"])

                                      }
                                  ).ToList();

                IList<CompanyBrandPerformanceData> objCompanyBrandPerformanceDataList = new List<CompanyBrandPerformanceData>();
               for (int S=0; S< CityList.Count; S++)
                {
                    CompanyBrandPerformanceData objCompanyBrandPerformanceData = new CompanyBrandPerformanceData();
                    objCompanyBrandPerformanceData.CityID = CityList[S].CityID;
                    objCompanyBrandPerformanceData.CityName = CityList[S].CityName;
                    IList<BrandPerformance> ResultCityData = result.Where(a => a.CityID == CityList[S].CityID).ToList();
                    IList<BrandPerformance> ResultCityDataPre = resultPrv.Where(a => a.CityID == CityList[S].CityID).ToList();
                    var BrandResult = from r in ResultCityData
                                      group r by new
                                      {
                                          r.BrandID,
                                          r.BrandName,
                                          r.CompanyName
                                      } into g
                                      select new BrandPerformance
                                      {
                                          BrandID = g.Key.BrandID,
                                          BrandName = g.Key.BrandName,
                                          CompanyName = g.Key.CompanyName,
                                          TotalSales = g.Sum(a => a.TotalSales),
                                          TotalUnits = g.Sum(a => a.TotalUnits)
                                      };

                    var BrickResult = from r in ResultCityData
                                      group r by new
                                      {
                                          r.BrickID,

                                      } into g
                                      select new BrandPerformance
                                      {
                                          BrickID = g.Key.BrickID,
                                          TotalSales = g.Sum(a => a.TotalSales),
                                          TotalUnits = g.Sum(a => a.TotalUnits)
                                      };

                    BrickDataAccess objBrickDataaccess = new BrickDataAccess();
                    IList<BrickMaster> objBrickList = new List<BrickMaster>();
                    objBrickList = objBrickDataaccess.GetBrickListByCityID(CityList[S].CityID, objCompanyBrandPerformanceIVM.CompanyID, objCompanyBrandPerformanceIVM.DivisonID, objCompanyBrandPerformanceIVM.PatchIds);
                    objBrickDataaccess = null;
                    IList<Patch> objtPathcList = new List<Patch>();


                    IList<BrickMaster> objCityList = new List<BrickMaster>();
                    objCityList.Add(new BrickMaster { BrickID = 0, BrickName = CityList[S].CityName });
                    objBrickList = objBrickList.OrderBy(a => a.BrickID).ToList();
                    for (int j = 0; j < objBrickList.Count; j++)
                    {
                        objtPathcList.Add(new Patch
                        {
                            PatchID = objBrickList[j].BrickID,
                            PatchName = objBrickList[j].BrickName
                        });
                    }
                    objCompanyBrandPerformanceData.ReportData = new ReportGraphData();
                    objCompanyBrandPerformanceData.ReportData.PatchList = objtPathcList;

                    IList<BrickMaster> objBrickListValue = new List<BrickMaster>();
                    IList<BrickMaster> objBrickListUnit = new List<BrickMaster>();
                    IList<Int32> MainBrickList = objBrickList.Select(a => a.BrickID).ToList();
                    IList<Int32> BrickListValue = BrickResult.OrderByDescending(a => a.TotalSales).Select(a => a.BrickID).ToList();
                    IList<Int32> AddedBrickListValue = new List<Int32>();
                    IList<Int32> AddedBrickListUnit = new List<Int32>();
                    for (int i = 0; i < BrickListValue.Count; i++)
                    {
                        BrickMaster objBrickMaster = objBrickList.Where(a => a.BrickID == BrickListValue[i]).FirstOrDefault();
                        objBrickListValue.Add(objBrickMaster);
                        AddedBrickListValue.Add(BrickListValue[i]);
                    }
                    IList<Int32> BrickListUnit = BrickResult.OrderByDescending(a => a.TotalUnits).Select(a => a.BrickID).ToList();
                    for (int i = 0; i < BrickListUnit.Count; i++)
                    {
                        BrickMaster objBrickMaster = objBrickList.Where(a => a.BrickID == BrickListValue[i]).FirstOrDefault();
                        objBrickListUnit.Add(objBrickMaster);
                        AddedBrickListUnit.Add(BrickListValue[i]);
                    }
                    for (int i = 0; i < MainBrickList.Count; i++)
                    {
                        if (!AddedBrickListValue.Contains(MainBrickList[i]))
                        {
                            BrickMaster objBrickMaster = objBrickList.Where(a => a.BrickID == MainBrickList[i]).FirstOrDefault();
                            objBrickListValue.Add(objBrickMaster);
                        }

                        if (!AddedBrickListUnit.Contains(MainBrickList[i]))
                        {
                            BrickMaster objBrickMaster = objBrickList.Where(a => a.BrickID == MainBrickList[i]).FirstOrDefault();
                            objBrickListUnit.Add(objBrickMaster);
                        }
                    }

                    var BrandResultPre = from r in ResultCityDataPre
                                         group r by new
                                         {
                                             r.BrandID,
                                             r.BrandName,
                                             r.CompanyName
                                         } into g
                                         select new BrandPerformance
                                         {
                                             BrandID = g.Key.BrandID,
                                             BrandName = g.Key.BrandName,
                                             CompanyName = g.Key.CompanyName,
                                             TotalSales = g.Sum(a => a.TotalSales),
                                             TotalUnits = g.Sum(a => a.TotalUnits)
                                         };




                    double TotalCityCurrentMarketValue = BrandResult.Sum(a => a.TotalSales);
                    double TotalCityPreMarketValue = BrandResultPre.Sum(a => a.TotalSales);
                    double TotalCityCurrentMarketUnit = BrandResult.Sum(a => a.TotalUnits);
                    double TotalCityPreMarketUnit = BrandResultPre.Sum(a => a.TotalUnits);

                    objCompanyBrandPerformanceData.ReportData.CompanyComparisonSales = new List<SalesByPatch>();
                    objCompanyBrandPerformanceData.ReportData.CompanyComparisonUnit = new List<SalesByPatch>();


                    IList<BrandPerformance> BrandDetailsCityValue = BrandResult.OrderByDescending(a => a.TotalSales).ToList();
                    IList<BrandPerformance> BrandDetailsCityPre = BrandResultPre.OrderByDescending(a => a.TotalSales).ToList();



                    IList<Int64> AddedbrandIDs = new List<Int64>();

                    IList<BrandPerformance> BrandPerformanceListValue = new List<BrandPerformance>();
                    IList<BrandPerformance> CompanyBrandPerformanceListValue = new List<BrandPerformance>();
                    double OtherTotalSaleCurrent = 0;
                    double OtherTotalSalePre = 0;
                    double OtherTotalUnitCurrent = 0;
                    double OtherTotalunitPre = 0;
                    IList<ProductBrickRank> RankValue = new List<ProductBrickRank>();
                    IList<ProductBrickRank> RankUnit = new List<ProductBrickRank>();
                    string productName = string.Empty;

                    double CurrentPrevTotalSalesValue = 0;
                 
                    double TotalSalesValue = BrandDetailsCityPre.Sum(a => a.TotalSales);
                    double TotalUnitValue = BrandDetailsCityPre.Sum(a => a.TotalUnits);

                    for (Int32 i = 0; i < BrandDetailsCityValue.Count; i++)
                    {
                        bool DataAadded = false;
                        if (i < 5)
                        {
                            BrandPerformance objBrandPerformance = new BrandPerformance();
                            objBrandPerformance.BrandID = BrandDetailsCityValue[i].BrandID;
                            objBrandPerformance.BrandName = BrandDetailsCityValue[i].BrandName;
                            objBrandPerformance.CompanyName = BrandDetailsCityValue[i].CompanyName;
                            objBrandPerformance.BrickID = 0;
                            objBrandPerformance.TotalSales = BrandDetailsCityValue[i].TotalSales;
                            objBrandPerformance.TotalUnits = BrandDetailsCityValue[i].TotalUnits;
                            BrandPerformance PreObj = BrandDetailsCityPre.Where(a => a.BrandID == BrandDetailsCityValue[i].BrandID).FirstOrDefault();
                            if (PreObj != null && PreObj.TotalSales > 0)
                            {
                                CurrentPrevTotalSalesValue += PreObj.TotalSales;
                             

                                objBrandPerformance.ValueGrowth = Convert.ToInt32(((BrandDetailsCityValue[i].TotalSales - PreObj.TotalSales) / PreObj.TotalSales) * 100);
                                objBrandPerformance.UnitGrowth = Convert.ToInt32(((BrandDetailsCityValue[i].TotalUnits - PreObj.TotalUnits) / PreObj.TotalUnits) * 100);
                            }
                            else
                            {
                                if (BrandDetailsCityValue[i].TotalSales > 0)
                                {
                                    objBrandPerformance.ValueGrowth = 100;
                                    objBrandPerformance.UnitGrowth = 100;
                                }
                                else
                                {
                                    objBrandPerformance.ValueGrowth = 0;
                                    objBrandPerformance.UnitGrowth = 0;
                                }
                            }
                            CompanyBrandPerformanceListValue.Add(objBrandPerformance);

                            AddedbrandIDs.Add(BrandDetailsCityValue[i].BrandID);
                            DataAadded = true;
                            if (BrandDetailsCityValue[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                            {
                                RankValue.Add(new ProductBrickRank
                                {
                                    BrickID = 0,
                                    BrickName = CityList[S].CityName,
                                    Rank = i + 1
                                });
                                productName = BrandDetailsCityValue[i].BrandName;
                            }
                        }

                        if (!AddedbrandIDs.Contains(BrandDetailsCityValue[i].BrandID) && BrandDetailsCityValue[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                        {
                            BrandPerformance objBrandPerformance = new BrandPerformance();
                            objBrandPerformance.BrandID = BrandDetailsCityValue[i].BrandID;
                            objBrandPerformance.BrandName = BrandDetailsCityValue[i].BrandName;
                            objBrandPerformance.CompanyName = BrandDetailsCityValue[i].CompanyName;
                            objBrandPerformance.BrickID = 0;
                            objBrandPerformance.TotalSales = BrandDetailsCityValue[i].TotalSales;
                            objBrandPerformance.TotalUnits = BrandDetailsCityValue[i].TotalUnits;

                            BrandPerformance PreObj = BrandDetailsCityPre.Where(a => a.BrandID == BrandDetailsCityValue[i].BrandID).FirstOrDefault();
                            if (PreObj != null && PreObj.TotalSales > 0)
                            {
                                CurrentPrevTotalSalesValue += PreObj.TotalSales;
                                

                                objBrandPerformance.ValueGrowth = Convert.ToInt32(((BrandDetailsCityValue[i].TotalSales - PreObj.TotalSales) / PreObj.TotalSales) * 100);
                                objBrandPerformance.UnitGrowth = Convert.ToInt32(((BrandDetailsCityValue[i].TotalUnits - PreObj.TotalUnits) / PreObj.TotalUnits) * 100);
                            }
                            else
                            {
                                if (BrandDetailsCityValue[i].TotalSales > 0)
                                {
                                    objBrandPerformance.ValueGrowth = 100;
                                    objBrandPerformance.UnitGrowth = 100;
                                }
                                else
                                {
                                    objBrandPerformance.ValueGrowth = 0;
                                    objBrandPerformance.UnitGrowth = 0;
                                }
                            }
                            CompanyBrandPerformanceListValue.Add(objBrandPerformance);

                            AddedbrandIDs.Add(BrandDetailsCityValue[i].BrandID);
                            DataAadded = true;
                            if (BrandDetailsCityValue[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                            {
                                RankValue.Add(new ProductBrickRank
                                {
                                    BrickID = 0,
                                    BrickName = CityList[S].CityName,
                                    Rank = i + 1
                                });
                                productName = BrandDetailsCityValue[i].BrandName;
                            }
                        }
                        if (!DataAadded)
                        {
                            OtherTotalSaleCurrent += BrandDetailsCityValue[i].TotalSales;
                            BrandPerformance PreObj = BrandDetailsCityPre.Where(a => a.BrandID == BrandDetailsCityValue[i].BrandID).FirstOrDefault();
                            if (PreObj != null)
                                OtherTotalSalePre += PreObj.TotalSales;

                        }

                    }

                    // Add Other Product


                    BrandPerformance objBrandPerformanceOthervalue = new BrandPerformance();
                    objBrandPerformanceOthervalue.BrandID = 0;
                    objBrandPerformanceOthervalue.BrandName = "Others";
                    objBrandPerformanceOthervalue.CompanyName = "";
                    objBrandPerformanceOthervalue.BrickID = 0;

                    objBrandPerformanceOthervalue.TotalSales = OtherTotalSaleCurrent;
                    OtherTotalSalePre = TotalSalesValue - CurrentPrevTotalSalesValue;
                   



                    if (OtherTotalSalePre > 0)
                    {
                        objBrandPerformanceOthervalue.ValueGrowth = Convert.ToInt32(((OtherTotalSaleCurrent - OtherTotalSalePre) / OtherTotalSalePre) * 100);
                    }
                    else
                    {
                        if (OtherTotalSaleCurrent > 0)
                        {
                            objBrandPerformanceOthervalue.ValueGrowth = 100;
                        }
                        else
                        {
                            objBrandPerformanceOthervalue.ValueGrowth = 0;
                        }
                    }
                    CompanyBrandPerformanceListValue.Add(objBrandPerformanceOthervalue);

                    // For Unit
                    IList<BrandPerformance> BrandDetailsCityUnit = BrandResult.OrderByDescending(a => a.TotalUnits).ToList();
                    IList<Int64> AddedbrandUnitIDs = new List<Int64>();
                    IList<BrandPerformance> BrandPerformanceListUnit = new List<BrandPerformance>();
                    IList<BrandPerformance> CompanyBrandPerformanceListUnit = new List<BrandPerformance>();
                    double CurrentPrevTotalSalesUnit = 0;
                    for (Int32 i = 0; i < BrandDetailsCityUnit.Count; i++)
                    {
                        bool DataAdded = false;
                        if (i < 5)
                        {
                            BrandPerformance objBrandPerformance = new BrandPerformance();
                            objBrandPerformance.BrandID = BrandDetailsCityUnit[i].BrandID;
                            objBrandPerformance.BrandName = BrandDetailsCityUnit[i].BrandName;
                            objBrandPerformance.CompanyName = BrandDetailsCityUnit[i].CompanyName;
                            objBrandPerformance.BrickID = 0;
                            objBrandPerformance.TotalSales = BrandDetailsCityUnit[i].TotalSales;
                            objBrandPerformance.TotalUnits = BrandDetailsCityUnit[i].TotalUnits;

                            BrandPerformance PreObj = BrandDetailsCityPre.Where(a => a.BrandID == BrandDetailsCityUnit[i].BrandID).FirstOrDefault();
                            if (PreObj != null && PreObj.TotalUnits > 0)
                            {
                                CurrentPrevTotalSalesUnit += PreObj.TotalUnits;
                                objBrandPerformance.ValueGrowth = Convert.ToInt32(((BrandDetailsCityUnit[i].TotalSales - PreObj.TotalSales) / PreObj.TotalSales) * 100);
                                objBrandPerformance.UnitGrowth = Convert.ToInt32(((BrandDetailsCityUnit[i].TotalUnits - PreObj.TotalUnits) / PreObj.TotalUnits) * 100);
                            }
                            else
                            {
                                if (BrandDetailsCityUnit[i].TotalUnits > 0)
                                {
                                    objBrandPerformance.ValueGrowth = 100;
                                    objBrandPerformance.UnitGrowth = 100;
                                }
                                else
                                {
                                    objBrandPerformance.ValueGrowth = 0;
                                    objBrandPerformance.UnitGrowth = 0;
                                }
                            }

                            CompanyBrandPerformanceListUnit.Add(objBrandPerformance);
                            AddedbrandUnitIDs.Add(BrandDetailsCityUnit[i].BrandID);
                            DataAdded = true;
                            if (BrandDetailsCityUnit[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                            {

                                RankUnit.Add(new ProductBrickRank
                                {
                                    BrickID = 0,
                                    BrickName = CityList[S].CityName,
                                    Rank = i + 1
                                });
                                productName = BrandDetailsCityValue[i].BrandName;
                            }
                        }

                        if (!AddedbrandUnitIDs.Contains(BrandDetailsCityUnit[i].BrandID) && BrandDetailsCityUnit[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                        {

                            BrandPerformance objBrandPerformance = new BrandPerformance();
                            objBrandPerformance.BrandID = BrandDetailsCityUnit[i].BrandID;
                            objBrandPerformance.BrandName = BrandDetailsCityUnit[i].BrandName;
                            objBrandPerformance.CompanyName = BrandDetailsCityUnit[i].CompanyName;
                            objBrandPerformance.BrickID = 0;
                            objBrandPerformance.TotalSales = BrandDetailsCityUnit[i].TotalSales;
                            objBrandPerformance.TotalUnits = BrandDetailsCityUnit[i].TotalUnits;

                            BrandPerformance PreObj = BrandDetailsCityPre.Where(a => a.BrandID == BrandDetailsCityUnit[i].BrandID).FirstOrDefault();
                            if (PreObj != null && PreObj.TotalUnits > 0)
                            {
                                CurrentPrevTotalSalesUnit += PreObj.TotalUnits;
                                objBrandPerformance.ValueGrowth = Convert.ToInt32(((BrandDetailsCityUnit[i].TotalSales - PreObj.TotalSales) / PreObj.TotalSales) * 100);
                                objBrandPerformance.UnitGrowth = Convert.ToInt32(((BrandDetailsCityUnit[i].TotalUnits - PreObj.TotalUnits) / PreObj.TotalUnits) * 100);

                            }
                            else
                            {
                                if (BrandDetailsCityUnit[i].TotalUnits > 0)
                                {
                                    objBrandPerformance.ValueGrowth = 100;
                                    objBrandPerformance.UnitGrowth = 100;
                                }
                                else
                                {
                                    objBrandPerformance.ValueGrowth = 0;
                                    objBrandPerformance.UnitGrowth = 0;
                                }
                            }

                            CompanyBrandPerformanceListUnit.Add(objBrandPerformance);
                            AddedbrandUnitIDs.Add(BrandDetailsCityUnit[i].BrandID);
                            DataAdded = true;
                            if (BrandDetailsCityUnit[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                            {
                                RankUnit.Add(new ProductBrickRank
                                {
                                    BrickID = 0,
                                    BrickName = CityList[S].CityName,
                                    Rank = i + 1
                                });
                                productName = BrandDetailsCityValue[i].BrandName;
                            }
                        }
                        if (!DataAdded)
                        {
                            OtherTotalUnitCurrent += BrandDetailsCityValue[i].TotalUnits;
                            BrandPerformance PreObj = BrandDetailsCityPre.Where(a => a.BrandID == BrandDetailsCityValue[i].BrandID).FirstOrDefault();
                            if (PreObj != null)
                                OtherTotalunitPre += PreObj.TotalUnits;

                        }

                    }



                    BrandPerformance objBrandPerformanceOtherUnit = new BrandPerformance();
                    objBrandPerformanceOtherUnit.BrandID = 0;
                    objBrandPerformanceOtherUnit.BrandName = "Others";
                    objBrandPerformanceOtherUnit.CompanyName = "";
                    objBrandPerformanceOtherUnit.BrickID = 0;

                    objBrandPerformanceOtherUnit.TotalUnits = OtherTotalUnitCurrent;
                    OtherTotalunitPre = TotalUnitValue - CurrentPrevTotalSalesUnit;


                    if (OtherTotalunitPre > 0)
                    {
                        objBrandPerformanceOtherUnit.UnitGrowth = Convert.ToInt32(((OtherTotalUnitCurrent - OtherTotalunitPre) / OtherTotalunitPre) * 100);
                    }
                    else
                    {
                        if (TotalCityCurrentMarketUnit > 0)
                        {
                            objBrandPerformanceOtherUnit.UnitGrowth = 100;
                        }
                        else
                        {
                            objBrandPerformanceOtherUnit.UnitGrowth = 0;
                        }
                    }
                    CompanyBrandPerformanceListUnit.Add(objBrandPerformanceOtherUnit);

                    // Add BrickLevel Data of Top  Brands
                    Dictionary<Int64, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(AddedbrandIDs, objCompanyBrandPerformanceIVM.CompanyID, "", objCompanyBrandPerformanceIVM.SessionID, 1);
                    string ProducttypeString = "brand";
                    objCompanyBrandPerformanceData.ReportData.CompanyComparisonSales = this.GetSalesPatchDataListBrandReport(objCityList, CompanyBrandPerformanceListValue, AddedbrandIDs, 0, ObjCompanyColorCodeSales, ProducttypeString);

                    Dictionary<Int64, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(AddedbrandUnitIDs, objCompanyBrandPerformanceIVM.CompanyID, "", objCompanyBrandPerformanceIVM.SessionID, 1);

                    objCompanyBrandPerformanceData.ReportData.CompanyComparisonUnit = this.GetSalesPatchDataListBrandReport(objCityList, CompanyBrandPerformanceListUnit, AddedbrandUnitIDs, 1, ObjCompanyColorCodeSales, ProducttypeString);

                    MarketGrowth objCityMarketGrowth = new MarketGrowth();
                    double TotalBrandValue = ResultCityData.Sum(a => a.TotalSales);
                    double TotalBrandUnit = ResultCityData.Sum(a => a.TotalUnits);
                    double TotalBrandValuePre = ResultCityDataPre.Sum(a => a.TotalSales);
                    double TotalBrandUnitPre = ResultCityDataPre.Sum(a => a.TotalUnits);
                    if (TotalBrandValuePre > 0)
                    {
                        objCityMarketGrowth.CityGrowthValue = Convert.ToInt32(((TotalBrandValue - TotalBrandValuePre) / TotalBrandValuePre) * 100);
                     }
                    else
                    {
                        objCityMarketGrowth.CityGrowthValue = 0;
                    }

                    if (TotalBrandUnitPre > 0)
                    {
                        objCityMarketGrowth.CityGrowthUnit = Convert.ToInt32(((TotalBrandUnit - TotalBrandUnitPre) / TotalBrandUnitPre) * 100);
                    }
                    else
                    {
                        objCityMarketGrowth.CityGrowthUnit = 0;
                    }

                    objCityMarketGrowth.PatchGrowth = new List<MarketPatchGrowth>();

                    for (int j = 0; j < objBrickList.Count; j++)
                    {
                        if (objBrickList[j].BrickID != 0)
                        {
                            IList<BrandPerformance> BrickLevelBrandValue = ResultCityData.Where(a => a.BrickID == objBrickList[j].BrickID).ToList();
                            IList<BrandPerformance> BrickLevelBrandValuePre = ResultCityDataPre.Where(a => a.BrickID == objBrickList[j].BrickID).ToList();
                            Double BrickLevelTotalCurrentSale = BrickLevelBrandValue.Sum(a => a.TotalSales);
                            Double BrickLevelTotalCurrentUnit = BrickLevelBrandValue.Sum(a => a.TotalUnits);
                            Double BrickLevelTotalPreUnit = BrickLevelBrandValuePre.Sum(a => a.TotalUnits);
                            Double BrickLevelTotalPreSale = BrickLevelBrandValuePre.Sum(a => a.TotalSales);
                            MarketPatchGrowth objCityMarketPatchGrowth = new MarketPatchGrowth();
                            objCityMarketPatchGrowth.BrickID = objBrickList[j].BrickID;
                            if (BrickLevelTotalPreSale > 0)
                            {
                                objCityMarketPatchGrowth.PatchGrowthValue = Convert.ToInt32((BrickLevelTotalCurrentSale - BrickLevelTotalPreSale) / BrickLevelTotalPreSale * 100);
                            }
                            else
                            {
                                objCityMarketPatchGrowth.PatchGrowthValue = 0;
                            }

                            if (BrickLevelTotalPreUnit > 0)
                            {
                                objCityMarketPatchGrowth.PatchGrowthUnit = Convert.ToInt32((BrickLevelTotalCurrentUnit - BrickLevelTotalPreUnit) / BrickLevelTotalPreUnit * 100);
                            }
                            else
                            {
                                objCityMarketPatchGrowth.PatchGrowthUnit = 0;
                            }
                            objCityMarketGrowth.PatchGrowth.Add(objCityMarketPatchGrowth);

                            IList<BrandPerformance> BrickLevelBrandValueOrd = BrickLevelBrandValue.OrderByDescending(a => a.TotalSales).ToList();
                            double OtherCurrentSales = 0;
                            double OtherPreSale = 0;
                            double OtherCurrentUnit = 0;
                            double OtherPreUnit = 0;
                            double SelectedBrandPreSales = 0;
                            if (BrickLevelBrandValueOrd.Count > 0)
                            {
                                for (int i = 0; i < BrickLevelBrandValueOrd.Count; i++)
                                {
                                    if (BrickLevelBrandValueOrd[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                                    {
                                        RankValue.Add(new ProductBrickRank
                                        {
                                            BrickID = objBrickList[j].BrickID,
                                            BrickName = objBrickList[j].BrickName,
                                            Rank = i + 1
                                        });

                                    }
                                    if (AddedbrandIDs.Contains(BrickLevelBrandValueOrd[i].BrandID))
                                    {
                                        BrandPerformance objBrandPerformance = new BrandPerformance();
                                        BrandPerformance CurrentObj = BrickLevelBrandValueOrd[i];
                                        if (CurrentObj != null)
                                        {
                                            objBrandPerformance.BrandID = CurrentObj.BrandID;
                                            objBrandPerformance.BrandName = CurrentObj.BrandName;
                                            objBrandPerformance.CompanyName = CurrentObj.CompanyName;
                                            objBrandPerformance.Rank = i + 1;
                                            objBrandPerformance.BrickID = objBrickList[j].BrickID;
                                            objBrandPerformance.TotalSales = CurrentObj.TotalSales;
                                            objBrandPerformance.TotalUnits = CurrentObj.TotalUnits;

                                            BrandPerformance PreObj = BrickLevelBrandValuePre.Where(a => a.BrandID == BrickLevelBrandValueOrd[i].BrandID).FirstOrDefault();
                                            if (PreObj != null && PreObj.TotalSales > 0)
                                            {
                                                
                                                objBrandPerformance.ValueGrowth = Convert.ToInt32(((CurrentObj.TotalSales - PreObj.TotalSales) / PreObj.TotalSales) * 100);
                                                objBrandPerformance.UnitGrowth = Convert.ToInt32(((CurrentObj.TotalUnits - PreObj.TotalUnits) / PreObj.TotalUnits) * 100);

                                            }
                                            else
                                            {
                                                if (CurrentObj.TotalSales > 0)
                                                {
                                                    objBrandPerformance.ValueGrowth = 100;
                                                    objBrandPerformance.UnitGrowth = 100;
                                                }
                                                else
                                                {
                                                    objBrandPerformance.ValueGrowth = 0;
                                                    objBrandPerformance.UnitGrowth = 0;
                                                }
                                            }

                                            BrandPerformanceListValue.Add(objBrandPerformance);
                                        }
                                      
                                    }
                                    else
                                    {
                                        OtherCurrentSales += BrickLevelBrandValueOrd[i].TotalSales;
                                        BrandPerformance PreObj = BrickLevelBrandValuePre.Where(a => a.BrandID == BrickLevelBrandValueOrd[i].BrandID).FirstOrDefault();
                                        if (PreObj != null)
                                            OtherPreSale += PreObj.TotalSales;

                                    }

                                }

                                for (int i = 0; i < BrickLevelBrandValuePre.Count; i++)
                                {
                                    if (AddedbrandIDs.Contains(BrickLevelBrandValuePre[i].BrandID))
                                    {
                                        SelectedBrandPreSales += BrickLevelBrandValuePre[i].TotalSales;
                                    }
                                }
                            }
                            else
                            {
                                RankValue.Add(new ProductBrickRank
                                {
                                    BrickID = objBrickList[j].BrickID,
                                    BrickName = objBrickList[j].BrickName,
                                    Rank = 0
                                });
                            }




                            BrandPerformance objBrickBrandPerformanceOtherSales = new BrandPerformance();
                            objBrickBrandPerformanceOtherSales.BrandID = 0;
                            objBrickBrandPerformanceOtherSales.BrandName = "Others";
                            objBrickBrandPerformanceOtherSales.CompanyName = "";
                            objBrickBrandPerformanceOtherSales.BrickID = objBrickList[j].BrickID;
                            objBrickBrandPerformanceOtherSales.TotalSales = OtherCurrentSales;

                            OtherPreSale = BrickLevelTotalPreSale - SelectedBrandPreSales;
                            if (OtherPreSale > 0)
                            {
                                objBrickBrandPerformanceOtherSales.ValueGrowth = ((OtherCurrentSales - OtherPreSale) / OtherPreSale) == 0 ? 0 : Convert.ToInt32(((OtherCurrentSales - OtherPreSale) / OtherPreSale) * 100);
                            }
                            else
                            { if (OtherCurrentSales > 0)
                                {
                                    objBrickBrandPerformanceOtherSales.ValueGrowth = 100;
                                }
                                 else
                                {
                                    objBrickBrandPerformanceOtherSales.ValueGrowth = 0;
                                }
                            }
                            BrandPerformanceListValue.Add(objBrickBrandPerformanceOtherSales);

                            BrandPerformance objBrickBrandPerformanceOther2Sales = new BrandPerformance();
                            objBrickBrandPerformanceOther2Sales.BrandID = -1;
                            objBrickBrandPerformanceOther2Sales.BrandName = "Others";
                            objBrickBrandPerformanceOther2Sales.CompanyName = "";
                            objBrickBrandPerformanceOther2Sales.BrickID = objBrickList[j].BrickID;
                            objBrickBrandPerformanceOther2Sales.TotalSales = OtherCurrentSales;
                            if (OtherPreSale > 0)
                            {
                                objBrickBrandPerformanceOther2Sales.ValueGrowth = ((OtherCurrentSales - OtherPreSale) / OtherPreSale) == 0 ? 0 : Convert.ToInt32(((OtherCurrentSales - OtherPreSale) / OtherPreSale) * 100);
                            }
                            else
                            {
                                if(OtherCurrentSales > 0)
                                objBrickBrandPerformanceOther2Sales.ValueGrowth = 100;
                                else
                                    objBrickBrandPerformanceOther2Sales.ValueGrowth = 0;
                            }

                            BrandPerformanceListValue.Add(objBrickBrandPerformanceOther2Sales);

                            double SelectedBrandPreUnit = 0;
                            IList<BrandPerformance> BrickLevelBrandUnitOrd = BrickLevelBrandValue.OrderByDescending(a => a.TotalUnits).ToList();
                            if (BrickLevelBrandUnitOrd.Count > 0)
                            {
                                for (int i = 0; i < BrickLevelBrandUnitOrd.Count; i++)
                                {
                                    if (BrickLevelBrandUnitOrd[i].BrandID == objCompanyBrandPerformanceIVM.BrandID)
                                    {
                                        RankUnit.Add(new ProductBrickRank
                                        {
                                            BrickID = objBrickList[j].BrickID,
                                            BrickName = objBrickList[j].BrickName,
                                            Rank = i + 1
                                        });

                                    }

                                    if (AddedbrandUnitIDs.Contains(BrickLevelBrandUnitOrd[i].BrandID))
                                    {
                                        BrandPerformance objBrandPerformance = new BrandPerformance();
                                        BrandPerformance CurrentObj = BrickLevelBrandUnitOrd[i];
                                        if (CurrentObj != null)
                                        {
                                            objBrandPerformance.BrandID = CurrentObj.BrandID;
                                            objBrandPerformance.BrandName = CurrentObj.BrandName;
                                            objBrandPerformance.CompanyName = CurrentObj.CompanyName;
                                            objBrandPerformance.BrickID = objBrickList[j].BrickID;
                                            objBrandPerformance.Rank = i + 1;
                                            objBrandPerformance.TotalSales = CurrentObj.TotalSales;
                                            objBrandPerformance.TotalUnits = CurrentObj.TotalUnits;

                                            BrandPerformance PreObj = BrickLevelBrandValuePre.Where(a => a.BrandID == BrickLevelBrandUnitOrd[i].BrandID).FirstOrDefault();
                                            if (PreObj != null && PreObj.TotalUnits > 0)
                                            {
                                                SelectedBrandPreUnit += PreObj.TotalUnits;
                                                objBrandPerformance.ValueGrowth = Convert.ToInt32(((CurrentObj.TotalSales - PreObj.TotalSales) / PreObj.TotalSales) * 100);
                                                objBrandPerformance.UnitGrowth = Convert.ToInt32(((CurrentObj.TotalUnits - PreObj.TotalUnits) / PreObj.TotalUnits) * 100);

                                            }
                                            else
                                            {
                                                if (CurrentObj.TotalUnits > 0)
                                                {
                                                    objBrandPerformance.ValueGrowth = 100;
                                                    objBrandPerformance.UnitGrowth = 100;
                                                }
                                                else
                                                {
                                                    objBrandPerformance.ValueGrowth = 0;
                                                    objBrandPerformance.UnitGrowth = 0;
                                                }
                                            }

                                            BrandPerformanceListUnit.Add(objBrandPerformance);
                                        }
                                    }
                                    else
                                    {
                                        OtherCurrentUnit += BrickLevelBrandUnitOrd[i].TotalUnits;
                                        BrandPerformance PreObj = BrickLevelBrandValuePre.Where(a => a.BrandID == BrickLevelBrandUnitOrd[i].BrandID).FirstOrDefault();
                                        if (PreObj != null)
                                            OtherPreUnit += PreObj.TotalUnits;

                                    }
                                }

                                for (int i = 0; i < BrickLevelBrandValuePre.Count; i++)
                                {
                                    if (AddedbrandIDs.Contains(BrickLevelBrandValuePre[i].BrandID))
                                    {
                                        SelectedBrandPreUnit += BrickLevelBrandValuePre[i].TotalUnits;
                                    }
                                }
                            }
                            else
                            {
                                RankUnit.Add(new ProductBrickRank
                                {
                                    BrickID = objBrickList[j].BrickID,
                                    BrickName = objBrickList[j].BrickName,
                                    Rank = 0
                                });
                            }

                            BrandPerformance objBrickBrandPerformanceOtherUnit = new BrandPerformance();
                            objBrickBrandPerformanceOtherUnit.BrandID = 0;
                            objBrickBrandPerformanceOtherUnit.BrandName = "Others";
                            objBrickBrandPerformanceOtherUnit.CompanyName = "";
                            objBrickBrandPerformanceOtherUnit.BrickID = objBrickList[j].BrickID;
                            objBrickBrandPerformanceOtherUnit.TotalUnits = OtherCurrentUnit;
                            OtherPreUnit = BrickLevelTotalPreUnit - SelectedBrandPreUnit;
                            if (OtherPreUnit > 0)
                            {
                                objBrickBrandPerformanceOtherUnit.UnitGrowth = ((OtherCurrentUnit - OtherPreUnit) / OtherPreUnit) == 0 ? 0 : Convert.ToInt32(((OtherCurrentUnit - OtherPreUnit) / OtherPreUnit) * 100);
                            }
                            else
                            {
                                if (BrickLevelTotalCurrentUnit > 0)
                                {

                                    objBrickBrandPerformanceOtherUnit.UnitGrowth = 100;
                                }
                                else
                                {
                                    objBrickBrandPerformanceOtherUnit.UnitGrowth = 0;
                                }
                            }

                            BrandPerformance objBrickBrandPerformanceOther2Unit = new BrandPerformance();
                            objBrickBrandPerformanceOther2Unit.BrandID = -1;
                            objBrickBrandPerformanceOther2Unit.BrandName = "Others";
                            objBrickBrandPerformanceOther2Unit.CompanyName = "";
                            objBrickBrandPerformanceOther2Unit.BrickID = objBrickList[j].BrickID;
                            objBrickBrandPerformanceOther2Unit.TotalUnits = OtherCurrentUnit;

                            if (OtherCurrentUnit > 0 && OtherPreUnit > 0)
                            {
                                objBrickBrandPerformanceOther2Unit.UnitGrowth = ((OtherCurrentUnit - OtherPreUnit) / OtherPreUnit) == 0 ? 0 : Convert.ToInt32(((OtherCurrentUnit - OtherPreUnit) / OtherPreUnit) * 100);
                            }
                            else
                            {
                                if (OtherCurrentUnit > 0)
                                {
                                    objBrickBrandPerformanceOther2Unit.UnitGrowth = 100;
                                }
                                else
                                {
                                    objBrickBrandPerformanceOther2Unit.UnitGrowth = 0;
                                }
                            }
                            BrandPerformanceListUnit.Add(objBrickBrandPerformanceOther2Unit);

                        }
                    }

                    AddedbrandIDs.Add(0);
                    AddedbrandUnitIDs.Add(0);

                    // Dictionary<int, string> ObjCompanyColorCodeSales = this.GetCompanyColorCode(AddedbrandIDs, objCompanyBrandPerformanceIVM.CompanyID, "", objCompanyBrandPerformanceIVM.SessionID, 0);
                    // string ProducttypeString = "brand";
                    objCompanyBrandPerformanceData.ReportData.CompanyPatchComparisonSales = this.GetSalesPatchDataListBrandReport(objBrickListValue, BrandPerformanceListValue, AddedbrandIDs, 0, ObjCompanyColorCodeSales, ProducttypeString);

                    //Dictionary<int, string> ObjCompanyColorCodeUnit = this.GetCompanyColorCode(AddedbrandUnitIDs, objCompanyBrandPerformanceIVM.CompanyID, "", objCompanyBrandPerformanceIVM.SessionID, 0);

                    objCompanyBrandPerformanceData.ReportData.CompanyPatchComparisonUnit = this.GetSalesPatchDataListBrandReport(objBrickListUnit, BrandPerformanceListUnit, AddedbrandUnitIDs, 1, ObjCompanyColorCodeSales, ProducttypeString);

                    objCompanyBrandPerformanceData.ReportData.CompanyPerformanceGrowthSalesTable = this.GetCompanyBrandPerformanceListTable(BrandPerformanceListValue, AddedbrandIDs, 0, objBrickListValue, CompanyBrandPerformanceListValue, CityList[S].CityName, objCityMarketGrowth);
                    objCompanyBrandPerformanceData.ReportData.CompanyPerformanceGrowthUnitTable = this.GetCompanyBrandPerformanceListTable(BrandPerformanceListUnit, AddedbrandUnitIDs, 1, objBrickListUnit, CompanyBrandPerformanceListUnit, CityList[S].CityName, objCityMarketGrowth);

                    objCompanyBrandPerformanceData.ReportData.CompanyPerformanceSalesTable = this.GetCompanyBrandPerformanceListTableValue (BrandPerformanceListValue, AddedbrandIDs, 0, objBrickListValue, CompanyBrandPerformanceListValue, CityList[S].CityName);
                    objCompanyBrandPerformanceData.ReportData.CompanyPerformanceUnitTable = this.GetCompanyBrandPerformanceListTableValue(BrandPerformanceListUnit, AddedbrandUnitIDs, 1, objBrickListUnit, CompanyBrandPerformanceListUnit, CityList[S].CityName);


                    IList<ProductBrickRank> objBrickRankValue = new List<ProductBrickRank>();
                    IList<ProductBrickRank> objBrickRankUnit = new List<ProductBrickRank>();
                    ProductBrickRank CityRankobjValue = RankValue.Where(a => a.BrickID == 0).FirstOrDefault();
                    if (CityRankobjValue != null)
                    {
                        objBrickRankValue.Add(CityRankobjValue);
                    }
                    else
                    {
                        objBrickRankValue.Add(new ProductBrickRank { BrickID = 0, BrickName = CityList[S].CityName, Rank = 0 });
                    }

                    ProductBrickRank CityRankobjUnit = RankUnit.Where(a => a.BrickID == 0).FirstOrDefault();
                    if (CityRankobjUnit != null)
                    {
                        objBrickRankUnit.Add(CityRankobjUnit);
                    }
                    else
                    {
                        objBrickRankUnit.Add(new ProductBrickRank { BrickID = 0, BrickName = CityList[S].CityName, Rank = 0 });
                    }


                    for (int i = 0; i < objBrickListValue.Count; i++)
                    {
                        if (objBrickListValue[i].BrickName.ToLower() != "unspecified")
                        {
                            ProductBrickRank objProductBrickRank = RankValue.Where(a => a.BrickID == objBrickListValue[i].BrickID).FirstOrDefault();
                            if (objProductBrickRank != null)
                            {
                                objBrickRankValue.Add(objProductBrickRank);
                            }
                            else
                            {
                                objBrickRankValue.Add(new ProductBrickRank { BrickID = objBrickListValue[i].BrickID, BrickName = objBrickListValue[i].BrickName, Rank = 0 });
                            }
                        }
                    }
                    for (int i = 0; i < objBrickListUnit.Count; i++)
                    {
                        if (objBrickListUnit[i].BrickName.ToLower() != "unspecified")
                        {
                            ProductBrickRank objProductBrickRank = RankUnit.Where(a => a.BrickID == objBrickListValue[i].BrickID).FirstOrDefault();
                            if (objProductBrickRank != null)
                            {
                                objBrickRankUnit.Add(objProductBrickRank);
                            }
                            else
                            {
                                objBrickRankUnit.Add(new ProductBrickRank { BrickID = objBrickListValue[i].BrickID, BrickName = objBrickListValue[i].BrickName, Rank = 0 });
                            }
                        }
                    }
                    objCompanyBrandPerformanceData.ReportData.ProductRankDetailsValue = new ProductRank()
                    {
                        ProductID = objCompanyBrandPerformanceIVM.BrandID,
                        ProductName = productName,
                        Rank = objBrickRankValue
                    };
                    objCompanyBrandPerformanceData.ReportData.ProductRankDetailsUnit = new ProductRank()
                    {
                        ProductID = objCompanyBrandPerformanceIVM.BrandID,
                        ProductName = productName,
                        Rank = objBrickRankUnit
                    };


                    objCompanyBrandPerformanceDataList.Add(objCompanyBrandPerformanceData);

                }










                objCompanyBrandPerformanceOVM.CompanyBrandPerformanceData = objCompanyBrandPerformanceDataList;


            }
            catch
            {
                throw;
            }


            return objCompanyBrandPerformanceOVM;
        }


    }


}

