﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
    public class SummaryBusinessManager
    {
        public Summary_OVM GetSummaryData(Summary_IVM objSummary)
        {
            Summary_OVM objSummaryData = new Summary_OVM();
            try
            {
                SessionDataAccess objSessionDataAccess = new SessionDataAccess();
                int UserID = objSessionDataAccess.ValidateSession(objSummary.SessionID);
                objSessionDataAccess = null;
                if (UserID == 0)
                {
                    objSummaryData = null;
                    return objSummaryData;
                }

                // Get UserAccessRights 

                AccessRightsDataAccess objAccessRightsDataAccess = new AccessRightsDataAccess();
                UserAccessRights objUserAccessrights = objAccessRightsDataAccess.GetUserAccessRights(UserID);

                int AccessType = 1;
                string TCCodes = string.Empty;
                string CityAccessRights = string.Empty;
                if(objUserAccessrights != null )
                {
                    CityAccessRights = objUserAccessrights.CityIDs;
                }
                if (objUserAccessrights != null && objUserAccessrights.AccessRightType != 1)
                {

                    AccessType = 2;
                    TCCodes = objUserAccessrights.TCValues;
                    

                }
                else
                {
                    CompanyAccessRights objCompanyAccessRight = objAccessRightsDataAccess.GetCompanyAccessRights(objSummary.CompanyID);
                    if (objCompanyAccessRight != null)
                    {
                        switch (objCompanyAccessRight.AccessRightType)
                        {
                            case 2:
                                AccessType = 2;
                                FilterBusinessManger objFilterBusineessmanager = new FilterBusinessManger();
                                IList<string> TCCodesList = objFilterBusineessmanager.GetTCCodebySuperGroup(objCompanyAccessRight.TCValues);
                                TCCodes = string.Join(",", TCCodesList);
                                break;
                            case 3:
                                AccessType = 2;
                                TCCodes = objCompanyAccessRight.TCValues;
                                break;
                        }
                    }
                }

                objAccessRightsDataAccess = null;

                // 
                objSummaryData.CityGraphDataDetails = new List<CityGraphData>();
                CityDataAccess objCityDataAccess = new CityDataAccess();
                objSummaryData.CityMasterData = objCityDataAccess.GetCityData();
                IList<SubscribedCityData> objSubscribedCityData= objCityDataAccess.GetSubscribedCityData(CityAccessRights, objSummary.PeriodType, objSummary.PeriodStart, objSummary.PeriodEnd);
                IList<CompanySalesData> ObjCompanySalesData = objCityDataAccess.GetCompanySalesData(CityAccessRights, objSummary.PeriodType, objSummary.PeriodStart, objSummary.PeriodEnd);
                IList<BrandSalesData> ObjBrandSalesData = objCityDataAccess.GetBrandSalesData(CityAccessRights, objSummary.PeriodType, objSummary.PeriodStart, objSummary.PeriodEnd);
                IList<SupergroupSalesData> ObjSupergroupSalesData = objCityDataAccess.GetSupergroupSalesData(CityAccessRights, objSummary.PeriodType, objSummary.PeriodStart, objSummary.PeriodEnd);
                objCityDataAccess = null;
                IList<string> CityList = CityAccessRights.Split(',');
                for(int i=0; i< CityList.Count; i++)
                {
                    Int32 CurrentCityID = Int32.Parse(CityList[i]);
                    SubscribedCityData objcurrentCity = objSubscribedCityData.Where(a => a.CityID == CurrentCityID).FirstOrDefault();
                    string CurrentCityName = string.Empty;
                    if(objcurrentCity == null)
                    {
                        CityData objCitydataa = objSummaryData.CityMasterData.Where(a => a.CityID == CurrentCityID).FirstOrDefault();
                        if(objCitydataa != null)
                        {
                            SubscribedCityData newdata = new SubscribedCityData()
                            {
                                CityID = objCitydataa.CityID,
                                CityName = objCitydataa.CityName,
                                TotalSales = 0,
                                TotalUnits = 0
                            };

                            objSubscribedCityData.Add(newdata);
                            CurrentCityName = objCitydataa.CityName;
                        }
                    }
                    else
                    {
                        CurrentCityName = objcurrentCity.CityName;
                    }

                    CityGraphData objCityGraphData = new CityGraphData();
                    objCityGraphData.CityID = CurrentCityID;
                    objCityGraphData.CityName = CurrentCityName;
                    
                    IList<CompanySalesData> objCurrentCityCompanyData = ObjCompanySalesData.Where(a => a.CityID == CurrentCityID).ToList();
                    objCityGraphData.TopCompanySalesDatavalue = objCurrentCityCompanyData.OrderByDescending(a => a.TotalSales).Take(10).ToList();
                    objCityGraphData.TopCompanySalesDataUnit= objCurrentCityCompanyData.OrderByDescending(a => a.TotalUnits).Take(10).ToList();

                    IList<BrandSalesData> objCurrentCityBrandData = ObjBrandSalesData.Where(a => a.CityID == CurrentCityID).ToList();
                    objCityGraphData.TopBrandSalesDataValue = objCurrentCityBrandData.OrderByDescending(a => a.TotalSales).Take(10).ToList();
                    objCityGraphData.TopBrandSalesDataUnit = objCurrentCityBrandData.OrderByDescending(a => a.TotalUnits).Take(10).ToList();

                    IList<SupergroupSalesData> objCurrentCitySupergroupData = ObjSupergroupSalesData.Where(a => a.CityID == CurrentCityID).ToList();
                    objCityGraphData.TopSuperGroupSalesDataValue = objCurrentCitySupergroupData.OrderByDescending(a => a.TotalSales).Take(10).ToList();
                    objCityGraphData.TopSuperGroupSalesDataUnit = objCurrentCitySupergroupData.OrderByDescending(a => a.TotalUnits).Take(10).ToList();

                    objSummaryData.CityGraphDataDetails.Add(objCityGraphData);

                }
                objSummaryData.SubscribedCityData = objSubscribedCityData;
                

            }
            catch
            {
                throw;
            }
            return objSummaryData;
        }
    }
}
