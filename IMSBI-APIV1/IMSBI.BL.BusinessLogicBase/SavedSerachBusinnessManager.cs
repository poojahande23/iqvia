﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
   public class SavedSerachBusinessManager
    {
        

       public IList<SavedSearches> GetSavedSearchesbyUser(int UserID, int TabType)
        {
            IList<SavedSearches> SavedSearchList = new List<SavedSearches>();
            try
            {
                DataSet resultds;
                DALUtility IMSBIDB = new DALUtility();


                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_GetSavedSearchesbyUser + " " + UserID +","+ TabType;
                resultds = IMSBIDB.ReturnDataset(str);

                var result = (from row in resultds.Tables[0].AsEnumerable()
                              select new SavedSearches
                              {
                                  SearchKeyID = Convert.ToInt32(row["SearchKeyID"]),
                                  SearchKey = Convert.ToString(row["SearchKey"]),
                                  SearchData = Convert.ToString(row["SearchData"]),

                              }
                              ).ToList();

                SavedSearchList = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return SavedSearchList;
        }

        public int AddUpdateSavedSearches(SavedSearches objSavedSearches)
        {
            int result = 0;
            DALUtility IMSBIDB = new DALUtility();
            var param = string.Empty;
            try
            {
                if(objSavedSearches.SearchKeyID == null)
                {
                    objSavedSearches.SearchKeyID = 0;
                }
                if (objSavedSearches != null)
                {
                    param = objSavedSearches.SearchKeyID + ",";
                    param += objSavedSearches.UserID + ",";
                    param += "'" + objSavedSearches.SearchKey + "',";
                    param += "'" + objSavedSearches.SearchData + "',";
                    param += "" + objSavedSearches.TabType + "";
                }

                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_CreateSavedSearches + " " + param;
                var obj = IMSBIDB.ReturnValue(str);
                result = Convert.ToInt32(obj);

              
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627) // <-- but this will
                {
                    result = -1;
                }
                else
                {
                    throw ex;
                }
            }
            return result;
        }

        public bool DeleteSavedSearches(Int32 SavedSearchID)
        {
            bool Status = false;
            try
            {
                DALUtility IMSBIDB = new DALUtility();
                string str = "Exec" + " " + IMSBI.Common.Constants.DB_SP_DeleteSavedSearches + " " + SavedSearchID;
                var obj = IMSBIDB.ReturnValue(str);
                var result = Convert.ToInt32(obj);
                if(result == 1)
                {
                    Status = true;
                }
            }
            catch
            {
                throw;
            }

            return Status;
        }
    }
}
