﻿using IMSBI.DAL;
using IMSBI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMSBI.BL.BusinessLogicBase
{
    public class DivisionBusinessManger
    {
        public int AddUpdateCompanyDivision(Division_IVM objDivision)
        {
            int result = 0;
           
            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                result = objDivisionDataAccess.AddUpdateCompanyDivision(objDivision);
                objDivisionDataAccess = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public int AddDivisionProductMapping(DivisionProductMapping objDivisionProductMapping)
        {
            int result = 0;

            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                result = objDivisionDataAccess.AddDivisionProductMapping(objDivisionProductMapping);
                objDivisionDataAccess = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public IList<Division_OVM> GetDivisionProductIDs(int CompanyID, int DivisionID)
        {
            IList<Division_OVM> DivisionProductList = new List<Division_OVM>();
            try
            {
                ProductDataAccess objProductdataAccess = new ProductDataAccess();
                DivisionProductList = objProductdataAccess.GetDivisionProducts(CompanyID, DivisionID);
                objProductdataAccess = null;
            }
            catch
            {

            }
            return DivisionProductList;
        }

        public DivisionInfo GetDivisionInfo(int DivisionID)
        {
            DivisionInfo DivisionProductList = new DivisionInfo();
            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                DivisionProductList = objDivisionDataAccess.GetDivisionInfo(DivisionID);
                objDivisionDataAccess = null;
            }
            catch
            {

            }
            return DivisionProductList;
        }


        public IList<DivisionBrand_OVM> GetDivisionBrands(int DivisionID, int CompanyID)
        {
            IList<DivisionBrand_OVM> DivisionBrandList = new List<DivisionBrand_OVM>();
            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                DivisionBrandList = objDivisionDataAccess.GetDivisionBrandList(DivisionID, CompanyID);
                objDivisionDataAccess = null;
            }
            catch
            {

            }
            return DivisionBrandList;
        }


        public int AddUpdateBrandGroup(BrandGroup objBrandGroup)
        {
            int result = 0;

            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                result = objDivisionDataAccess.AddUpdateBrandGroup(objBrandGroup);
                objDivisionDataAccess = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public BrandGroup GetBrandGroupInfo(int BrandGroupID)
        {
            BrandGroup ObjBrandGroup = new BrandGroup();
            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                ObjBrandGroup = objDivisionDataAccess.GetBrandGroupInfo(BrandGroupID);
                objDivisionDataAccess = null;
            }
            catch
            {

            }
            return ObjBrandGroup;
        }


        public IList<BrandGroup> GetBrandGroupList(int DivisionID, int CompanyID)
        {
            IList<BrandGroup> DivisionBrandGroupList = new List<BrandGroup>();
            try
            {
                DivisionDataAccess objDivisionDataAccess = new DivisionDataAccess();
                DivisionBrandGroupList = objDivisionDataAccess.GetBrandGroupList(DivisionID, CompanyID);
                objDivisionDataAccess = null;
            }
            catch
            {

            }
            return DivisionBrandGroupList;
        }



    }

}
