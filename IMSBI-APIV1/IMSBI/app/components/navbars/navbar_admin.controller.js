'use strict';

(function(){
	function NavbarAdminController ($timeout, $scope, $rootScope, $window, $state, $uibModal, adminServices, notify, accountServices) {

		/* Parse session data to object format */
		function sessionInfoToScope(){
			$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
			console.log('$scope.sessionInfo: ',$scope.sessionInfo);
		}
		sessionInfoToScope();


		/* Handle an issued event */
		$rootScope.$on('UpdateNavbarSessionInfo', function(){
			sessionInfoToScope();
		});


		/* Logout Action */
		$scope.logoutSession = function(){
            accountServices.Logout(JSON.parse($window.localStorage.BI_Tool_Info).SessionID)
            .then(function(response) {}, function(rejection) {});
		};


		/* Change Password modal starts */
	    $scope.openConfirmationModal = function(){
	        var self = $scope;
	        var changePassword = $uibModal.open({
	            templateUrl:'changePasswordModal.html',
	            size: 'md',
	            windowTopClass: 'modal-top-class',
	            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
	            	$scope.passwords = {};
	            	// $scope.passwords.OldPassword = '';
	            	$scope.passwords.UserID = self.sessionInfo.UserID;
	            	$scope.showError = false;
	                $scope.closeModal = function(){
	                    $uibModalInstance.close('close');
	                };
	                $scope.dismissModal = function(){
	                    $uibModalInstance.dismiss('dismiss');
	                };
	                $scope.setNewPassword = function(e){
	                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
	                		$scope.showError = true;
	                	}
	                	else{
	                		$scope.showError = false;
	                		self.setPassword($scope.passwords, $uibModalInstance);
	                	}
	                };
	            }]
	        });
	    };
	    /* Change Password modal ends */

	   	// Set Password
		$scope.setPassword = function(passwords, $uibModalInstance){
			adminServices.ChangePassword(passwords)
			.then(function (response) {
			    if (response.data != null && response.status === 200) {
			    	if(response.data === "updated."){
			    		$uibModalInstance.close('close');
			    		showNotification('Password Updated Successfully', '', '$scope.logoutSession()');
			    	}
				    else{
				    	showNotification(response.data, 'notify-bg', '');
				    }

			    } else {

			    }
			},
			function (rejection) { });
		};


		// Show Notification
		function showNotification(message, noteClass, routine){
			noteClass = noteClass || '';
			notify({
	            message: message,
	            classes: noteClass,
	            templateUrl: $scope.template,
	            position: 'center',
	            duration: 2000
	        });
	        eval(routine);
		}


		// Navigate back to Super admin page
		$scope.NavigateToSuperadmin = function() {
			$scope.sessionInfo.CompanyID = 0;
			$state.go('super.customerlist', {'sessionInfo':$scope.sessionInfo});
		};

	}

	angular.module('imsApp')
	  .controller('NavbarAdminController', NavbarAdminController);
})();
