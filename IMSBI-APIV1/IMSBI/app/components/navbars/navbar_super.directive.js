'use strict';

angular.module('imsApp')
	.directive('navbarsuper', [function(){
		var directiveObj = {};
		directiveObj.templateUrl = 'app/components/navbars/navbar_super.html';
		directiveObj.restrict = 'A';
		directiveObj.controller = 'NavbarSuperController';
		directiveObj.controllerAs = 'nav';
		
		return directiveObj;
	}]);

