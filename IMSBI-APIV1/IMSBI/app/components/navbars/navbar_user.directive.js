'use strict';

angular.module('imsApp')
	.directive('navbaruser', [function(){
		var directiveObj = {};
		directiveObj.templateUrl = 'app/components/navbars/navbar_user.html';
		directiveObj.restrict = 'A';
		directiveObj.controller = 'NavbarUserController';
		directiveObj.controllerAs = 'nav';
		
		return directiveObj;
	}]);
