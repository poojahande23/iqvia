﻿'use strict';
IMSModule.factory('companyServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var companyServiceFactory = {};

    //Get Company List
    var _GetCompanyList = function () {
        return $http.get(serviceBase + "Company/GetCompanyList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Subscription Type
    var _GetSubscriptionType = function (LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetCompanySubscriptionType")
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add & Update Company Subscription Type info
    var _AddUpdateSubscribedType = function (payload) {
        return $http.post(serviceBase + 'Company/AddUpdateSubscribedType', payload)
                .then(function (results) {
                    return $q.resolve(results);
                },
                function (rejection) {
                    return $q.reject(rejection);
                }
            );
    };


    // Get Subscription Type Details by subscription Id
    var _GetSubscriptionTypeDetails = function (subscriptionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetSubscriptionTypeDetails", {params: {SubscriptionTypeID:subscriptionId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    //Get Unsubscribed Company
    var _GetUnsubscribedCompanies = function () {
        return $http.get(serviceBase + "Company/GetUnsubscribedCompanyList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };

    //Get Subscribed Company
    var _GetsubscribedCompanies = function () {
        return $http.get(serviceBase + "Company/GetSubscribedCompanyList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


	// Post Company Subscription
    var _AddCompanySubscription = function (objCompanySubcription) {
        return $http.post(serviceBase + 'Company/AddUpdateSubscribedCompany', objCompanySubcription)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };


    //Get Subscribed Company
    var _GetCompanyPerformance = function (bodyObject) {
		angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + "Company/GetCompanyPerformance", bodyObject)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Performance Map
    var _GetCompanyPerformanceMap = function (bodyObject) {
		angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + "Company/GetCompanyPerformanceMap", bodyObject)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Count based on Subscriptions
    var _GetCompanyCountBasedOnSubscription = function (TimePeriodID, isViewTen) {
        return $http.get(serviceBase + "Company/GetCompanyCountBasedOnSubscription")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Divisions
    var _GetCompanyDivisions = function (companyId) {
    	// angular.element('.loader-backdrop').fadeIn();
        return $http.get(serviceBase + "Company/GetCompanyDivisions", {params: {CompanyID:companyId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                // angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                // angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    //Get Comparision Type
    var _GetComparisionTypeValue = function (filterType, sessionId, tcCode, therapyId) {
        return $http.get(serviceBase + "Filter/GetComparisionTypeValue", {params: {FilterType:filterType, SessionID:sessionId, TCcode:tcCode, TherapyID:therapyId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Therapy List by Therapy Code
    var _GetTherapyListByTherapyCode = function (tcCode, sessionId, LoaderShow, LoaderHide) {
        if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetTherapyListByTherapyCode", {params: {TCcode:tcCode, SessionID:sessionId}})
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add & Update Company Division info
    var _AddUpdateCompanyDivision = function (divisionObj) {
        return $http.post(serviceBase + 'CompanyDivison/AddUpdateCompanyDivision', divisionObj)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };


    //Get Products by Company & Divisions
    var _GetDivisionProducts = function (companyId, divisionId, disableLoader) {
    	if(disableLoader === undefined){
    		angular.element('.loader-backdrop').fadeIn();
    	}
        return $http.get(serviceBase + "CompanyDivison/GetDivisionProducts", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
            	if(disableLoader === undefined){
            		angular.element('.loader-backdrop').fadeOut();
            	}
                return $q.resolve(results);
            },
            function (rejection) {
            	if(disableLoader === undefined){
					angular.element('.loader-backdrop').fadeOut();
            	}
                return $q.reject(rejection);
            }
        );
    };


    //Get Division info by id
    var _GetDivisionInfo = function (divisionId) {
        return $http.get(serviceBase + "CompanyDivison/GetDivisionInfo", {params: {DivisionID:divisionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Pincodes based on search query
    var _GetPinCodeList = function (searchQuery) {
        return $http.get(serviceBase + "PinCode/GetPinCodeList", {params: {PinCode:searchQuery}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Pincodes based on search query
    var _GetCityStateByPinCode = function (searchQuery) {
        return $http.get(serviceBase + "PinCode/GetCityStateByPinCode", {params: {PinCode:searchQuery}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Reporting Manager Based On RoleType
    var _GetCompanyReportingManagerBasedOnRoleType = function (roleId, companyId, divisionId) {
        return $http.get(serviceBase + "Company/GetCompanyReportingManagerBasedOnRoleType", {params: {CompanyRoleID:roleId, CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Brand by Division
    var _GetDivisionBrand = function (companyId, divisionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "CompanyDivison/GetDivisionBrand", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get Brand Groups List by Division
    var _GetBrandGroupList = function (companyId, divisionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "CompanyDivison/GetBrandGroupList", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add Update Brand Groups
    var _AddUpdateBrandGroup = function (brandGroupObject) {
        return $http.post(serviceBase + "CompanyDivison/AddUpdateBrandGroup", brandGroupObject)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
	
	 var _GenerateCompanyCVMData = function (companyId) {
        return $http.get(serviceBase + "company/GenerateCompanyCVMData", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Brand Group Info by BrandId
    var _GetBrandGroupInfo = function (brandGroupId) {
        return $http.get(serviceBase + "CompanyDivison/GetBrandGroupInfo", {params: {BrandgroupID:brandGroupId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Product List by Company Id
    var _GetCompanyProductList = function (companyId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetCompanyProductList", {params: {CompanyID:companyId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add Division Product Mapping
    var _AddDivisionProductMapping = function (mappingObj) {
        return $http.post(serviceBase + "CompanyDivison/AddDivisionProductMapping", mappingObj)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Bulk : Add Division Product Mapping
    var _BulkProductMapping = function (bulkMappingObj, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + "Company/BulkProductMapping", bulkMappingObj)
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add Cluster
    var _AddUpdateCluster = function (clusterObj) {
        return $http.post(serviceBase + "Company/AddUpdateCluster", clusterObj)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Cluster List by Company Id
    var _GetCompanyClusterList = function (companyId, cityId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetCompanyClusterList", {params: {CompanyID:companyId, CityID:cityId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Cluster List by Cluster Id
    var _GetCompanyClusterInfo = function (clusterId) {
        return $http.get(serviceBase + "Company/GetCompanyClusterInfo", {params: {ClusterID:clusterId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Info by Company ID
    var _GetSubscribedCompanyInfo = function (companyId) {
        return $http.get(serviceBase + "Company/GetSubscribedCompanyInfo", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Cities and TC Classes subscribed by Company ID
    var _GetCompanySubscribedCityTCClass = function (companyId) {
        return $http.get(serviceBase + "Company/GetCompanySubscribedCityTCClass", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Company by Company ID
    var _DeleteCompany = function (companyId) {
        return $http.delete(serviceBase + "Company/DeleteCompany", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Company Division by Division ID
    var _DeleteDivision = function (divisionId) {
        return $http.delete(serviceBase + "Company/DeleteDivision", {params: {DivisionID:divisionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Division-Product Mapping
    var _DeleteDivisionProductMapping = function (divisionId, companyId, pfcId) {
        return $http.delete(serviceBase + "Company/DeleteDivisionProductMapping", {params: {DivisionID:divisionId, CompanyID:companyId, PFCID:pfcId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Brand Group by BrandGroupID
    var _DeleteBrandGroup = function (brandGroupId) {
        return $http.delete(serviceBase + "Company/DeleteBrandGroup", {params: {BrandGroupID:brandGroupId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Cluster by ClusterID
    var _DeleteCompanyCluster = function (clusterId) {
        return $http.delete(serviceBase + "Company/DeleteCompanyCluster", {params: {ClusterID:clusterId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Product CVM list by Product Id
    var _GetProductCM = function (productId) {
        return $http.get(serviceBase + "Company/GetProductCM", {params: {ProductID:productId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    //Add Company Product CVM
    var _AddProductCVM = function (payloadObj) {
        return $http.post(serviceBase + "Company/AddProductCVM", payloadObj)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    companyServiceFactory.GetCompanyList = _GetCompanyList;
    companyServiceFactory.GetSubscriptionType = _GetSubscriptionType;
    companyServiceFactory.AddUpdateSubscribedType = _AddUpdateSubscribedType;
    companyServiceFactory.GetSubscriptionTypeDetails = _GetSubscriptionTypeDetails;
    companyServiceFactory.GetUnsubscribedCompanies = _GetUnsubscribedCompanies;
    companyServiceFactory.AddCompanySubscription = _AddCompanySubscription;
    companyServiceFactory.GetsubscribedCompanies = _GetsubscribedCompanies;
    companyServiceFactory.GetCompanyPerformance = _GetCompanyPerformance;
    companyServiceFactory.GetCompanyCountBasedOnSubscription = _GetCompanyCountBasedOnSubscription;
    companyServiceFactory.GetCompanyDivisions = _GetCompanyDivisions;
    companyServiceFactory.GetComparisionTypeValue = _GetComparisionTypeValue;
    companyServiceFactory.GetCompanyPerformanceMap = _GetCompanyPerformanceMap;
    companyServiceFactory.GetTherapyListByTherapyCode = _GetTherapyListByTherapyCode;
    companyServiceFactory.AddUpdateCompanyDivision = _AddUpdateCompanyDivision;
    companyServiceFactory.GetDivisionProducts = _GetDivisionProducts;
    companyServiceFactory.GetDivisionInfo = _GetDivisionInfo;
    companyServiceFactory.GetPinCodeList = _GetPinCodeList;
    companyServiceFactory.GetCityStateByPinCode = _GetCityStateByPinCode;
    companyServiceFactory.GetCompanyReportingManagerBasedOnRoleType = _GetCompanyReportingManagerBasedOnRoleType;
    companyServiceFactory.GetDivisionBrand = _GetDivisionBrand;
    companyServiceFactory.GetBrandGroupList = _GetBrandGroupList;
    companyServiceFactory.AddUpdateBrandGroup = _AddUpdateBrandGroup;
    companyServiceFactory.GetBrandGroupInfo = _GetBrandGroupInfo;
    companyServiceFactory.GetCompanyProductList = _GetCompanyProductList;
    companyServiceFactory.AddDivisionProductMapping = _AddDivisionProductMapping;
    companyServiceFactory.BulkProductMapping = _BulkProductMapping;
    companyServiceFactory.AddUpdateCluster = _AddUpdateCluster;
    companyServiceFactory.GetCompanyClusterList = _GetCompanyClusterList;
    companyServiceFactory.GetCompanyClusterInfo = _GetCompanyClusterInfo;
    companyServiceFactory.GetSubscribedCompanyInfo = _GetSubscribedCompanyInfo;
    companyServiceFactory.GetCompanySubscribedCityTCClass = _GetCompanySubscribedCityTCClass;
    companyServiceFactory.DeleteCompany = _DeleteCompany;
    companyServiceFactory.DeleteDivision = _DeleteDivision;
    companyServiceFactory.DeleteDivisionProductMapping = _DeleteDivisionProductMapping;
    companyServiceFactory.DeleteBrandGroup = _DeleteBrandGroup;
    companyServiceFactory.DeleteCompanyCluster = _DeleteCompanyCluster;
    companyServiceFactory.GetProductCM = _GetProductCM;
    companyServiceFactory.AddProductCVM = _AddProductCVM;
	 companyServiceFactory.GenerateCompanyCVMData = _GenerateCompanyCVMData;
	
    return companyServiceFactory;
}]);
