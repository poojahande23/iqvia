'use strict';

/* This service maintainces and manages the session based on sessionID saved in LocalStorage */
IMSModule.service('sessionMgntService', ['$http', '$rootScope', '$window', 'accountServices', '$state',
                                        function ($http, $rootScope, $window, accountServices, $state) {
    var sessionMgntService_Obj = {};

    /* Generic Image Base Path */
	var baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');

    sessionMgntService_Obj = function(){

        /* Register an handler for StateChangeStartEvent */
        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
            console.log('state change just started: ',toState, fromState, event);

            /* Manage session based on localStorage.BI_Tool_Info & sessionStorage.imsSessionInfo */
            if($window.localStorage.BI_Tool_Info !== undefined){
                if($window.sessionStorage.imsSessionInfo === undefined){
                    console.log('session block 1');
                    VerifyLocalStorage_SessionID(toState.name);
                }
                else{
                    console.log('session block 2');
                    if(JSON.parse($window.sessionStorage.imsSessionInfo).UserID !== undefined){
                        if(toState.name === 'login'){
                            event.preventDefault();
                        }
                        else if((fromState.name !== '') && (fromState.name !== 'login')){       // if parent states does not match, except in case of 'superadmin' who has access to 'admin' state
                            var from_parent = fromState.name.split('.')[0];
                            var to_parent = toState.name.split('.')[0];
                            if(toState.name.split('.').length > 1){        // preventing an insurgence into parent state
                                if(from_parent !== to_parent){
                                    if(JSON.parse($window.sessionStorage.imsSessionInfo).RoleID === 1){
                                        if(to_parent === 'user'){
                                            event.preventDefault();
                                        }
                                    }
                                    else{
                                        event.preventDefault();
                                    }
                                }
                            }
                            else{
                                event.preventDefault();
                            }
                        }
                        else if(fromState.name === ''){     // besides in case of hacking, this case occurs even on page reload
                            // event.preventDefault();
                            // redirectToAccountByRoleID(event);
                        }
                    }
                }
            }
        });


        /* Redirect to an appropriate accunt based on RoleID */
        function redirectToAccountByRoleID(event){
            switch (JSON.parse($window.sessionStorage.imsSessionInfo).RoleID){
                case 1:
                event.preventDefault();
                $state.go('superadmin.customerlist');
                break;

                case 2:
                event.preventDefault();
                $state.go('admin.userlist');
                break;

                case 3:
                event.preventDefault();
                $state.go('user.analysis');
                break;
            }
        }


        /* Register an handler for StateChangeEvent */
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            var sessionInfoIMS = {};
            // set imsSessionInfo
            if($window.sessionStorage.imsSessionInfo === undefined){
                console.log('state change if block');
                sessionInfoIMS.toState = toState.name;
                sessionInfoIMS.fromState = fromState.name;
                $window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfoIMS);
            }
            else{
                console.log('state change else block');
                sessionInfoIMS = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
                sessionInfoIMS.toState = toState.name;
                sessionInfoIMS.fromState = fromState.name;
                $window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfoIMS);
                // Update Navbar's sessionInfo
                $rootScope.$emit('UpdateNavbarSessionInfo', {});
            }
        });


        /* Handle window's Storage event on Local storage - IT HAS LITTLE DIFFERENT BEHAVIOUR IN INTERNET EXPLORER 9,10 */
        angular.element($window).on('storage', function(event){
            if(event.originalEvent.key === 'BI_Tool_Info'){
                if(event.originalEvent.newValue === null){      // if logout happened on one tab, others should also logout
                    $window.sessionStorage.removeItem('imsSessionInfo');
                    $state.go('login');
                }
                else{       // if login happened on one tab, others should also login
                    VerifyLocalStorage_SessionID('dummy389.state');
                }
            }
        });
    };


    /* Verify if BI_Tool_Info.SessionID is set in the Local Storage */
    function VerifyLocalStorage_SessionID(requestedState){
        var BI_info = JSON.parse($window.localStorage.BI_Tool_Info);
        accountServices.GetUserSession(BI_info.SessionID)
        .then(function (response) {
            if(response.data != null && response.status === 200) {
                sessionCreation_Redirect(response.data, requestedState);
            }
            else {      // logout if session is expired at Server side
                if($window.sessionStorage.imsSessionInfo !== undefined){
                    $window.sessionStorage.removeItem('imsSessionInfo');
                }
                $window.localStorage.removeItem('BI_Tool_Info');
                $state.go('login');
            }
        },
        function (rejection) {});
    }


    /* Manage state redirect based on response data */
    function sessionCreation_Redirect(data, requestedState){
        var reqParentState = requestedState.split('.')[0];
        var paths = setImagePath(data.ProfileImage, data.LogoFileName);
        data.ProfileImage = paths.profileImg;
        data.LogoFileName = paths.logoImg;

        if(data.RoleID === 1){
            $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), data));
            if(reqParentState === 'super'){
                $state.go(requestedState);
            }
            else{
                $state.go('super.customerlist');
            }
        }
        else if(data.RoleID > 1){
            // Verify the account for validity
            if(!data.CompanyStatus){
                showNotification({message:'Subscription is disabled', duration: 3000, class:'notify-bg', routine:null});
            }
            else if(!compareSubscriptionDate(data.SubscriptionStartDate, data.SubscriptionEndDate)){
                showNotification({message:'Subscription is ended', duration: 3000, class:'notify-bg', routine:null});
            }
            else{
                $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), data));
                if(data.RoleID === 2){
                    if(reqParentState === 'admin'){
                        $state.go(requestedState);
                    }
                    else{
                        $state.go('admin.userlist');
                    }
                }
                else if(data.RoleID === 3){
                    if(reqParentState === 'user'){
                        $state.go(requestedState);
                    }
                    else{
                        $state.go('user.analysis');
                    }
                }
            }
        }
        else{
            alert('Invalid User Role');
        }
    }


    /* setting path for image */
    function setImagePath(profileImg, logoImg){
    	var path = {};
   		path.profileImg = (profileImg !== '') ? (baseURL + profileImg) : (baseURL + 'avatars/avatar-2-256.png');
    	path.logoImg = (logoImg !== '') ? (baseURL + logoImg) : (path.logoImg = baseURL + 'avatars/demo_logo.jpg');
    	return path;
    }


    /* Compare Subscription dates for validity */
	function compareSubscriptionDate(Sdate, Edate){
		Sdate = new Date(Sdate).getTime();
		Edate = new Date(Edate).getTime();
		var Tdate = new Date().getTime();
		if((Sdate <= Edate) && (Tdate <= Edate)){
			return true;
		}
		return false;
	}


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: null,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}


    return sessionMgntService_Obj;
}]);
