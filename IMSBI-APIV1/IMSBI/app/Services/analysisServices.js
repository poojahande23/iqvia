﻿'use strict';
IMSModule.factory('analysisServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var analysisServiceFactory = {};

    
    // Post to Get Analysis Dashboard Data
    var _GetAnalysisDashboardData = function (bodyObject) {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + 'Analysis/GetAnalysisDashboardData', bodyObject)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Post to Get Analysis Market Share Data
    var _GetAnalysisMarketShareData = function (bodyObject) {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + 'Analysis/GetAnalysisMarketShareData', bodyObject)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.reject(rejection);
                }
            );
    };


    // Post to Generate PDF
    var _GeneratePDF = function (bodyObject, LoaderShow, LoaderHide) {
        if(LoaderShow !== undefined){
            eval(LoaderShow);
        }
        return $http.post(serviceBase + 'Analysis/GeneratePDF', bodyObject)
            .then(function (results) {
                    if(LoaderHide !== undefined){
                        eval(LoaderHide);
                    }
                    return $q.resolve(results);
                },
                function (rejection) {
                    if(LoaderHide !== undefined){
                        eval(LoaderHide);
                    }
                    return $q.reject(rejection);
                }
            );
    };
    
    
    analysisServiceFactory.GetAnalysisDashboardData = _GetAnalysisDashboardData;
    analysisServiceFactory.GetAnalysisMarketShareData = _GetAnalysisMarketShareData;
    analysisServiceFactory.GeneratePDF = _GeneratePDF;
    
    return analysisServiceFactory;
}]);