﻿'use strict';
IMSModule.factory('mediaServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var mediaServiceFactory = {};

    // Post Company Subscription
    var _PostAnImage = function (mediaObject, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
    	var fd = new FormData();
        fd.append('file', mediaObject);
        console.log('mediaObject: ',mediaObject);
        return $http.post(serviceBase + 'UploadImage', fd, {
	        	transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
			}
        )
        .success(function(results){
        	if(LoaderHide !== undefined){
            	eval(LoaderHide);
            }
            return $q.resolve(results);
        })
        .error(function(rejection){
        	if(LoaderHide !== undefined){
            	eval(LoaderHide);
            }
            return $q.reject(rejection);
        });
    };
    
    
    mediaServiceFactory.PostAnImage = _PostAnImage;
    
    return mediaServiceFactory;
}]);