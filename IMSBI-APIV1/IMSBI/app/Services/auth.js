﻿'use strict';
IMSModule.factory('Auth', [ '$window', function ($window) {

return{
    setUser : function(aUser){
        $window.sessionStorage.setItem('user',aUser);
    },
    resetUser : function(aUser){
        $window.sessionStorage.removeItem('user');
    },
    getUser : function(){
        $window.sessionStorage.getItem('user');
    },
    isLoggedIn : function(){
        var user = $window.sessionStorage.getItem('user');
        if(!!user){
            return user;
        }else{
            return false;
        }
    }
  }
}]);