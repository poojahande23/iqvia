'use strict';

var IMSModule = angular.module('imsApp', ['kendo.directives', 'ui.router', 'ngAnimate', 'ngFader', 'ui.bootstrap', 'nvd3', 'ngMap', 'googlechart',
											'cgNotify', 'ngSanitize', 'ui.select', 'duScroll', 'ui.slimscroll']);

	IMSModule.ServiceBaseUrl = "http://35.154.246.198/IMSBIAPIV2/api/";

	IMSModule.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

		// Make it 'false' to keep the page on refresh
		$locationProvider.html5Mode({
		    enabled: false,
		    requireBase: false
		});

		$urlRouterProvider.when('/', '/login')
		.otherwise('/');

		IMSModule.config(function ($httpProvider) {
		    $httpProvider.defaults.headers.common = {};
		    $httpProvider.defaults.headers.post = {};
		    $httpProvider.defaults.headers.put = {};
		    $httpProvider.defaults.headers.patch = {};
		});


		/* ----------------------- User States ------------------------- */

		$stateProvider.state('login', {
            url: '/login',
            templateUrl: 'app/views/login.html',
            controller: 'LoginController',
            onEnter: function(){ console.log('Login state...');}
        })

        .state('user', {
            url: '/user',
            templateUrl: 'app/views/layout_user.html',
            controller: 'LayoutUserController',
            // abstract: true,
            onEnter: function(){ console.log('Layout User state...');}
        })

        .state('user.analysis', {
            url: '/analysis',
            templateUrl: 'app/views/analysis.html',
            controller: 'AnalysisController',
            onEnter: function(){ console.log('Analysis state...');}
        })

        .state('user.mapview', {
            url: '/mapview',
            templateUrl: 'app/views/mapview.html',
            controller: 'MapViewController',
            onEnter: function(){ console.log('MapView state...');}
        })

        .state('user.competitiveanalysis', {
            url: '/competitiveanalysis',
            templateUrl: 'app/views/competitiveanalysis.html',
            controller: 'CompetitiveAnalysisController',
            onEnter: function(){ console.log('Competitive Analysis state...');}
        })

        .state('user.competitiveteam', {
            url: '/competitiveteam',
            templateUrl: 'app/views/competitiveteam.html',
            controller: 'CompetitiveTeamController',
            onEnter: function(){ console.log('Competitive Team state...');}
        })

        .state('user.userprofile', {
            url: '/userprofile',
            templateUrl: 'app/views/user_profile.html',
            controller: 'UserProfileController',
            onEnter: function(){ console.log('User Profile state...');}
        })


        /* ----------------------- Admin States ------------------------- */

		.state('admin', {
            url: '/admin',
            templateUrl: 'app/views/layout_admin.html',
            controller: 'LayoutAdminController',
            // abstract: true,
            onEnter: function(){ console.log('Layout Admin state...');}
        })

        .state('admin.userlist', {
            url: '/userlist',
            templateUrl: 'app/views/userlist.html',
            controller: 'UserListController',
            onEnter: function(){ console.log('User List state...');},
            params: {
            	sessionInfo: ''
            }
        })

        .state('admin.addnewuser', {
            url: '/addnewuser',
            templateUrl: 'app/views/addnewuser.html',
            controller: 'AddNewUserController',
            onEnter: function(){ console.log('Add User state...');},
            params: {
            	userId: ''
            }
        })

        .state('admin.teamspermissions', {
            url: '/teamspermissions',
            templateUrl: 'app/views/teams_permissions.html',
            controller: 'TeamsPermissionsController',
            onEnter: function(){ console.log('Teams Permissions state...');}
        })

        .state('admin.addteam', {
            url: '/addteam',
            templateUrl: 'app/views/addteam.html',
            controller: 'AddTeamController',
            onEnter: function(){ console.log('Add Team state...');}
        })

        .state('admin.mapusertoteam', {
            url: '/map_user_to_team',
            templateUrl: 'app/views/map_user_to_team.html',
            controller: 'MapUserToTeamController',
            onEnter: function(){ console.log('Map User To Team state...');}
        })

        .state('admin.userroles', {
            url: '/user_roles',
            templateUrl: 'app/views/user_roles.html',
            controller: 'UserRolesController',
            onEnter: function(){ console.log('User Roles state...');}
        })

        .state('admin.addrole', {
            url: '/add_role',
            templateUrl: 'app/views/addrole.html',
            controller: 'AddRoleController',
            onEnter: function(){ console.log('Add Role state...');},
            params: {
            	roleId: ''
            }
        })

        .state('admin.divisions', {
            url: '/divisions',
            templateUrl: 'app/views/divisions.html',
            controller: 'DivisionsController',
            onEnter: function(){ console.log('Divisions state...');}
        })

        .state('admin.add_division', {
            url: '/add_division',
            templateUrl: 'app/views/add_division.html',
            controller: 'AddDivisionController',
            onEnter: function(){ console.log('Add Division state...');},
            params: {
                id: ''
            }
        })

        .state('admin.map_product', {
            url: '/map_product',
            templateUrl: 'app/views/map_product.html',
            controller: 'MapProductController',
            onEnter: function(){ console.log('Map Product state...');}
        })

        .state('admin.products', {
            url: '/products',
            templateUrl: 'app/views/products.html',
            controller: 'ProductsController',
            onEnter: function(){ console.log('Products state...');}
        })

		.state('admin.add_cvm', {
            url: '/add_cvm',
            templateUrl: 'app/views/add_cvm.html',
            controller: 'AddCVMController',
            onEnter: function(){ console.log('Add CVM state...');},
            params: {
                pcfId: ''
            }
        })

        .state('admin.brandgroup', {
            url: '/brand_group',
            templateUrl: 'app/views/brand_group.html',
            controller: 'BrandGroupController',
            onEnter: function(){ console.log('Brand Group state...');}
        })

        .state('admin.addbrandgroup', {
            url: '/add_brand_group',
            templateUrl: 'app/views/add_brand_group.html',
            controller: 'AddBrandGroupController',
            onEnter: function(){ console.log('Add Brand Group state...');},
            params: {
                brandGroupId: ''
            }
        })

        .state('admin.userprofile', {
            url: '/admin_profile',
            templateUrl: 'app/views/user_profile.html',
            controller: 'UserProfileController',
            onEnter: function(){ console.log('Admin Profile state...');}
        })



        /* ----------------------- Super States ------------------------- */

       .state('super', {
            url: '/super',
            templateUrl: 'app/views/layout_super.html',
            controller: 'LayoutSuperController',
            // abstract: true,
            onEnter: function(){ console.log('Layout Super state...');}
        })

        .state('super.customerlist', {
            url: '/customerlist',
            templateUrl: 'app/views/customerlist.html',
            controller: 'CustomerListController',
            onEnter: function(){ console.log('Customer List state...');},
            params: {
            	sessionInfo: ''
            }
        })

        .state('super.addcustomer', {
            url: '/addcustomer',
            templateUrl: 'app/views/addcustomer.html',
            controller: 'AddCustomerController',
            onEnter: function(){ console.log('Add Customer state...');},
            params: {
            	customerId: '',
            	CompanyName: ''
            }
        })

        .state('super.subscriptions', {
            url: '/subscriptions',
            templateUrl: 'app/views/subscriptions.html',
            controller: 'SubscriptionsController',
            onEnter: function(){ console.log('Subscriptions state...');}
        })

        .state('super.addnewsubscription', {
            url: '/addnewsubscription',
            templateUrl: 'app/views/add_new_subscription.html',
            controller: 'AddNewSubscriptionController',
            onEnter: function(){ console.log('Add New Subscriptions state...'); },
            params: {
            	subscriptionId: ''
            }
        })

        .state('super.clusters', {
            url: '/clusters',
            templateUrl: 'app/views/clusters.html',
            controller: 'ClustersController',
            onEnter: function(){ console.log('Clusters state...'); }
        })

        .state('super.addcluster', {
            url: '/addcluster',
            templateUrl: 'app/views/add_cluster.html',
            controller: 'AddClusterController',
            onEnter: function(){ console.log('Add New Cluster state...'); },
            params: {
                clusterId: ''
            }
        })

        .state('super.userprofile', {
            url: '/userprofile',
            templateUrl: 'app/views/user_profile.html',
            controller: 'UserProfileController',
            onEnter: function(){ console.log('Super Admin Profile state...');}
        })

	});


	// Listen to Local & Session storage events
	IMSModule.run(['$window', '$rootScope', '$state', 'accountServices', 'sessionMgntService',
                   function($window, $rootScope, $state, accountServices, sessionMgntService){
		console.log('run....block....executed');

        /* Check if Local & Session Storage api is supported by Browser */
        if(typeof(Storage) === undefined){
            alert('Session Storage is not supported by this Browser');
        }

        /* Session Management Service */
        sessionMgntService();


		// Global settings for Slim-scroll options
		$rootScope.slimScrollOptions = { 'height':'auto', 'width':'auto', 'alwaysVisible':true, 'color':'rgba(100, 100, 100, 0.7)' };
		$rootScope.slimScrollOptions_2 = { 'height':'auto', 'width':'auto', 'alwaysVisible':true, 'color':'rgba(100,100,100,0.7)' };

	}]);
