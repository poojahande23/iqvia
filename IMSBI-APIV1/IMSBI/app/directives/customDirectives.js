IMSModule.directive('bindFile', [function() {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function($scope, el, attrs, ngModel) {
            el.bind('change', function(event) {
                ngModel.$setViewValue(event.target.files[0]);
                angular.element(event.target).siblings('label.file-label').html(event.target.files[0].name);
                $scope.$apply();
            });

            $scope.$watch(function() {
                return ngModel.$viewValue;
            }, function(value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);


IMSModule.directive('bindFileContent', [function() {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function($scope, el, attrs, ngModel) {
            el.bind('change', function(event) {
            	if(event.target.files[0] !== undefined){
            		var file = event.target.files[0];
            		fileReader.readAsArrayBuffer(file);
	                angular.element(event.target).parent('label').find('span.name').html(file.name);
	                $scope.$apply();
            	}
            });

            var fileReader = new FileReader();
            fileReader.onload = function(event){
            	ngModel.$setViewValue(event.target.result);
            };

            $scope.$watch(function() {
                return ngModel.$viewValue;
            }, function(value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);


// Directive to pickup a dropped file
IMSModule.directive("fileDropZone", [function() {
    return {
    	require: "ngModel",
        restrict : "A",
        link: function (scope, elem, attrs, ngModel) {
        	elem.on('dragover', function(e) { elem.parent('label').addClass('drag-area'); e.preventDefault(); e.stopPropagation(); });
        	elem.on('dragleave', function(e) { elem.parent('label').removeClass('drag-area'); e.preventDefault(); e.stopPropagation(); });
        	elem.on('dragenter', function(e) { e.preventDefault(); e.stopPropagation(); });
            elem.on('drop', function(e) {
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files[0];
                fileReader.readAsArrayBuffer(file);
                elem.parent('label').find('p.title').html(file.name);
            });

            var fileReader = new FileReader();
            fileReader.onload = function(event){
            	ngModel.$setViewValue(event.target.result);
            };
        }
    };
}]);


/* Directive to assign file object to ng-model */
IMSModule.directive('fileInput', function(){
	return{
		require: "ngModel",
		restrict : 'A',
		link: function(scope, elem, attrs, ngModel){
			elem.bind('change', function(event){
				if(event.target.files[0] !== undefined){
					scope.$apply(function(){
						ngModel.$setViewValue(event.target.files[0]);
					});
				}
			});
		}
	}
});


/* Directive to hide sidemenu */
IMSModule.directive('hideSidemenu', ['$rootScope', function($rootScope){
    var directive = {};
    directive.restrict = 'A';
    directive.template = '<a href="javascript:void(0)" class="sidemenu-trigger"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>';
    directive.replace = true;
    directive.link = function(attr, elem, scope){
        elem.on('click', function(event){
            var body = angular.element('body');
            if(body.hasClass('hide-slidemenu')){
                body.removeClass('hide-slidemenu');
                if($rootScope.refreshCharts_Analysis !== undefined){
                    $rootScope.refreshCharts_Analysis();
                }
                if($rootScope.resizeMap !== undefined){
                    $rootScope.resizeMap();
                }
                if($rootScope.refreshCharts_CompAn !== undefined){
                    $rootScope.refreshCharts_CompAn();
                }
            }
            else{
                body.addClass('hide-slidemenu');
                if($rootScope.refreshCharts_Analysis !== undefined){
                    $rootScope.refreshCharts_Analysis();
                }
                if($rootScope.resizeMap !== undefined){
                    $rootScope.resizeMap();
                }
                if($rootScope.refreshCharts_CompAn !== undefined){
                    $rootScope.refreshCharts_CompAn();
                }
            }
        });
    };
    return directive;
}]);


/* Directive-Attribute to Export an element to PDF */
IMSModule.directive('exportToPdfAttr', function(){
	directiveObj = {};

    directiveObj.restrict = 'A';

    directiveObj.scope = false;

    directiveObj.replace = true;

    directiveObj.link = function(scope, el, attr){
        scope.pdf_name = attr.pdfName || 'Default PDF name';

        angular.element(el).on('click', function(){
            kendo.drawing.drawDOM(angular.element('#'+attr.exportElement), {
                avoidLinks: true
            })
    		.then(function(group){
    			return kendo.drawing.exportPDF(group, {
    				paperSize: "auto",
            margin: "2cm",
            pageSize: 9
    			});
    		})
    		.done(function(data){
    			kendo.saveAs({
    				dataURI: data,
    				fileName: scope.pdf_name
    			});
    		});
        });
    };

    return directiveObj;
});


/* Directive-Attribute to Export an element to Power Point */
IMSModule.directive('exportToPowerPointAttr', function(){
	directiveObj = {};

    directiveObj.restrict = 'A';

    directiveObj.scope = false;

    directiveObj.replace = true;

    directiveObj.link = function(scope, el, attr){
        scope.ppt_name = attr.pptName || 'Default PPT name';

        angular.element(el).on('click', function(){
            getImageBySelector(angular.element('#'+attr.exportElement),function(canvas){
                var base64Image = canvas.toDataURL();
                exportToPPT(base64Image);
            });
        });

        function getImageBySelector(selector,callback) {
            html2canvas(selector, {
                onrendered:callback
            });
        }

        function exportToPPT(base64Data){
            var pptx = new PptxGenJS();
            var slide = pptx.addNewSlide();
            slide.addImage({ x:0, y:0.8, w:10, h:4, data:base64Data});
          //  slide.addText('Overall Comparision',{ x:0.0, y:0.25, data:base64Data});
            pptx.save(scope.ppt_name);
        }
    };

    return directiveObj;
});
/* Directive to Export an element to PDF, PPT */
IMSModule.directive('exportToDocuments', ['loaderServices', function(loaderServices){
	directiveObj = {};

    directiveObj.restrict = 'E';

    directiveObj.scope = {
        config_obj: '=configData'
    };

    directiveObj.replace = true;

    directiveObj.template ='<div class="ui dropdown" tabindex="0">'
                            +'<div class="toggle-div dropbtn">Export To <i class="dropdown icon"></i></div>'
                            +'<div class="menu dropdown-content" tabindex="-1">'
                            +'<a class="item" href="javascript:void(0)" ng-repeat="item in config_obj"'
                            +'ng-click="selectFeature(item.action, item.id)" ng-bind-html="item.icon+item.label"></a>'
                            +'</div></div>';

    directiveObj.link = function(scope, el, attr){
        scope.file_name = attr.fileName || 'Default File Name';

        scope.selectFeature = function(action, id){
            switch (action){
                case 'pdf':
                    if(id){
                        loaderServices.startLoader();
                        angular.element(el).hide();
                        var gp = [];
                        var i = 0;
                        makeElementGroups(id, i, gp);
                        function makeElementGroups(id, i, gp){
                            if(i < id.length){
                                if(angular.element('#'+id[i]).find('.wt-content').length > 0){
                                    angular.element('#'+id[i]).find('.wt-content').css({'display':'block'});
                                }
                                kendo.drawing.drawDOM(angular.element('#'+id[i]), {
                                    avoidLinks: true
                                })
                                .then(function(group){
                                    if(angular.element('#'+id[i]).find('.wt-content').length > 0){
                                        angular.element('#'+id[i]).find('.wt-content').css({'display':'none'});
                                    }
                                    if(i === 0){ gp = group; }
                                    else{ gp.children.push(group.children[0]); }
                                    makeElementGroups(id, ++i, gp);
                                });
                            }
                            else{
                                angular.element(el).show();
                                kendo.drawing.exportPDF(gp, {
                                    paperSize: "auto",
                                    margin: "2cm",
                                    multiPage: true
                                })
                                .done(function(data){
                                    kendo.saveAs({
                                        dataURI: data,
                                        fileName: scope.file_name
                                    });
                                    loaderServices.stopLoader();
                                });
                            }
                        }
                    }
                    break;
                case 'ppt':
                    if(id){
                        loaderServices.startLoader();
                        angular.element(el).hide();
                        var pptx = new PptxGenJS();
                        var s_text = null, base64Image = null;
                        var i = 0;
                        makeSlide(id, i);
                        function makeSlide(id, i){
                            if(i < id.length){
                                if(id[i].newSlide){
                                    slide = pptx.addNewSlide();
                                }
                                switch(id[i].type){
                                    case 'txt':
                                        s_text = angular.element('#'+id[i].value).text();
                                        slide.addText(s_text, { x:0.2, y:0.15, font_size:16, font_face:'Lato', color:'0088CC' });
                                        makeSlide(id, ++i);
                                        break;
                                    case 'img':
                                        angular.element('#'+id[i].value).find('.wt-content').css({'display':'block'});
                                        kendo.drawing.drawDOM(angular.element('#'+id[i].value))
                                        .then(function(group){
                                            return kendo.drawing.exportImage(group);
                                        })
                                        .done(function(base64Image){
                                            slide.addImage({ x:0.2, y:0.6, w:9.6, h:4.5, data:base64Image, autoPage:true ,newPageStartY:6 });
                                            angular.element('#'+id[i].value).find('.wt-content').css({'display':'none'});
                                            makeSlide(id, ++i);
                                        });
                                        break;
                                    default:
                                        console.log('default case of makeSlide function');
                                }
                            }
                            else{
                                angular.element(el).show();
                                pptx.save(scope.file_name);
                                loaderServices.stopLoader();
                            }
                        }
                    }
                    break;
                default:
                    console.log('Default case of Export Documents directive');
            }
        }
    };

    return directiveObj;
}]);
