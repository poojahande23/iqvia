angular.module("imsApp")
.controller("TeamsPermissionsController", ['$scope', '$state', function($scope, $state){
	
	console.log('Teams Permissions controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	$scope.tableData = [
								{designation:"Head Field India", teamhead:"Pradeep Desai", action:""},
								{designation:"Medical Reps", teamhead:"Manish Pradhan", action:"" },
								{designation:"Regional Manager", teamhead:"Sachin Mule", action:""},
								{designation:"Zonal Manager", teamhead:"Jayshankar Krish", action:""}
  							];
	$scope.filteredList = $scope.tableData;
  							
	$scope.gridColumns = [
		{ field: "designation", title:"Designation",headerTemplate: "Designation <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "teamhead", title:"Team Head", headerTemplate: "Team Head <span class='k-icon k-i-sort-desc'></span>",width:"800px"  },
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'><i class='fa fa-lock' aria-hidden='true'></i> <i class='fa fa-times' aria-hidden='true'></i> <a ui-sref='admin.addteam'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"150px" }
	];
	
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if(($scope.tableData !== undefined) || ($scope.tableData !== null)){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			$scope.filteredList = $scope.tableData.filter(function(item){
									return regExpression.test(item.designation);
								});
		}
	}
	
		
}]);
