angular.module("imsApp")
.controller("MapUserToTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Map User To Team controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	$scope.gridData = [
						{id: 1, fullname:"Sagar Hoda", designation:"Medical Reps", teamhead:"Unsigned"},
						{id: 2, fullname:"Rahul Vaidya", designation:"Zonal Manager", teamhead:"Sachin Mule"},
						{id: 3, fullname:"Parineeti Chouhan", designation:"Regional Manager", teamhead:"Pradeep Desai"},
						{id: 4, fullname:"Shivraj Patel", designation:"Zonal Manager", teamhead:"Unsigned"},
						{id: 5, fullname:"Mithila Desai", designation:"Medical Reps", teamhead:"Unsigned"},
						{id: 6, fullname:"Rahul Jadeja", designation:"Medical Reps", teamhead:"Nitin Kapoor"},
						{id: 7, fullname:"Dinesh Juneja", designation:"Regional Manager", teamhead:"Nitin Kapoor"},
						{id: 8, fullname:"Lakshmi Bhaskar", designation:"Medical Reps", teamhead:"Unsigned"},
						{id: 9, fullname:"Radha Mohan", designation:"Zonal Manager", teamhead:"Unsigned"},
						{id: 10, fullname:"Sujoy Biswas", designation:"Medical Reps", teamhead:"Unsigned"}
					];
					
	$scope.gridColumns = [
		{ field: "fullname", title:"Full Name", template: "<div class='customer-photo'" + "style='background-image: url(content/img/avatars.png);'></div>"
		+ "<div class='customer-name'>#:fullname#</div>" + "<div class='checkboxshow'>"
		+ "<div class='checkbox'><input type='checkbox'><input type='checkbox' id='#:id#'><label for='#:id#'></label></div>"},
		{ field: "designation", title:"Designation" },
		{ field: "teamhead", title:"Team Head" }
	];
	
	$scope.searchDesignation=[
		{searchdesig:"Search Name or Designation"},
		{searchdesig:"Search Name or Designation"}
	];
  
	$scope.mapuserTeamhead=[
		{teamhead:"Pradeep Desai"},
		{teamhead:"Sachin Mule"},
		{teamhead:"Nitin Kapoor"}
	];
		
}]);
