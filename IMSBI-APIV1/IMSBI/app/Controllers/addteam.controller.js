angular.module("imsApp")
.controller("AddTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Add Teams controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	$scope.people = [
				{ permission: "Permission 1", view:"", edit:"1", delet:"d1" },
				{ permission: "Permission 2", view:"", edit:"2", delet:"d2" },
				{ permission: "Permission 3", view:"", edit:"3", delet:"d3" },
				{ permission: "Permission 4", view:"", edit:"4", delet:"d4" },
				{ permission: "Permission 5", view:"", edit:"5", delet:"d5" },
				{ permission: "Permission 6", view:"", edit:"6", delet:"d6" },
				{ permission: "Permission 7", view:"", edit:"7", delet:"d7" },
				{ permission: "Permission 8", view:"", edit:"8", delet:"d8" }
			  ];
			  
			  
	$scope.gridColumns = [
					{ title: 'Permission Type', field: 'permission', template:"" ,width:"450px"},
					{ title: 'View', field: 'view', template: "<div class='checkbox'><input type='checkbox' id='#:permission#'><label for='#:permission#'></label></div>", width:"250px"},
					{ title: 'Edit', field: 'edit', template: "<div class='checkbox'><input type='checkbox'><input type='checkbox' id='#:edit#'><label for='#:edit#'></label></div>" },
					{ title: 'Delete', field: 'delet', template: "<div class='checkbox'><input type='checkbox'><input type='checkbox' id='#:delet#'><label for='#:delet#'></label></div>" }
	];
		
}]);
