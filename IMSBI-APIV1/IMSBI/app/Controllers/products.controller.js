angular.module("imsApp")
.controller("ProductsController", ['$scope', '$state', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, companyServices, $window, $uibModal, notify){

	console.log('Products controller...', $state);

	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));

	angular.element(document).ready(function(){
		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);

        angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
        });

        $scope.selectedTabPanel = 1;
	});

	/* Angular Provision for Tab Panel */
	$scope.showTab = function(id1, id2){
		angular.element(id1).fadeOut(500, function(){
			angular.element(id2).fadeIn(function(){
				if(id1 === '#tabs-4-tab-2'){
					$scope.selectedTabPanel = 1;
				}
				else {
					$scope.selectedTabPanel = 2;
				}
			});
		});
	}

	/* Initialize Models */
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.CompanyID = $scope.sessionInfo.CompanyID;


	/*---------------------- Products Starts ----------------------*/
	/* Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.DivisionID_brand = $scope.divisionColl[0].DivisionID;
				$scope.FetchProductsByCompanyDivisions();
				$scope.FetchDivisionBrand();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();


	/* Fetch Products by Company Divisions */
	$scope.FetchProductsByCompanyDivisions = function () {
		companyServices.GetDivisionProducts($scope.CompanyID, $scope.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.products.data(response.data);
			} else {
				$scope.products.data = false;
			}
		},
		function (rejection) {});
	};
	
	/* Fetch Products by Company Divisions */
	$scope.UpdateCompanyCVM = function () {
		companyServices.GenerateCompanyCVMData($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				
			} else {
				
			}
		},
		function (rejection) {});
	};

	$scope.gridColumns = [
		{ field: "PFCID", title:"PFC ID",headerTemplate: "PFC ID <span class='k-icon k-i-sort-desc'></span>", width:"100px"},
		{ field: "ProductName", title:"Product Name",headerTemplate: "Product Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action",headerTemplate: "Add CVM <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='action-link'><a ui-sref='admin.add_cvm({pcfId:#=PFCID#})'>Add CVM</a></div>", width:"15%"
		},
		{ field: "Manuf_Name", title:"Manufacturer",headerTemplate: "Manufacturer <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "IsOnFocus", title:"Is Focused",headerTemplate: "Is Focused <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='focus-#=PFCID#' ng-checked='(#=IsOnFocus# == 1)' ng-true-value='1' k-ng-model='#:IsOnFocus#'><label for='focus-#=PFCID#'></label></div>", width:"80px" },
		{ field: "IsNewIntroduction", title:"Is New Intro",headerTemplate: "Is New Intro <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='intro-#=PFCID#' ng-checked='(#=IsNewIntroduction# == 1)' ng-true-value='1' k-ng-model='#:IsNewIntroduction#'><label for='intro-#=PFCID#'></label></div>", width:"80px" },
		{ field: "", title:"Delete", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='deleteConfirmationModal(#=PFCID#, \"#=ProductName#\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>", width:"70px" }
	];

	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};

	// Kendo Observable Array
	$scope.products = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "ProductName",
            dir: "asc"
        }
	});

	// Delete Selected Product
	$scope.deleteSelectedProduct = function($uibModalInstance, id, name){
		companyServices.DeleteDivisionProductMapping($scope.DivisionID, $scope.CompanyID, id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchProductsByCompanyDivisions();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }

		    } else {}
		},
		function (rejection) { });
	};
	/*---------------------- Products Ends ----------------------*/


	/*---------------------- Brands Starts ----------------------*/
	/* Fetch Brands by Company Divisions */
	$scope.FetchDivisionBrand = function () {
		companyServices.GetDivisionBrand($scope.CompanyID, $scope.DivisionID_brand, showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.brands.data(response.data);
			} else {
				$scope.brands.data = false;
			}
		},
		function (rejection) {});
	};

	$scope.gridColumns2 = [
		{ field: "BrandID", title:"Brand ID",headerTemplate: "Brand ID <span class='k-icon k-i-sort-desc'></span>", width:"100px"},
		{ field: "BrandName", title:"Brand Name",headerTemplate: "Brand Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"}
	];

	$scope.gridOptions2 = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};

	// Kendo Observable Array
	$scope.brands = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "BrandName",
            dir: "asc"
        }
	});
	/*---------------------- Brands Ends ----------------------*/


	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	if(self.selectedTabPanel === 1){
                		self.deleteSelectedProduct($uibModalInstance, recordId, recordName);
                	}
                	else{

                	}
                };
            }]
        });
    };
    /* Delete Confimation modal ends */


	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}

}]);
