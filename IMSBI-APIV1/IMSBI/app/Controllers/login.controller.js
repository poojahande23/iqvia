angular.module("imsApp")
.controller("LoginController", ['$scope', '$state', 'accountServices', '$window', 'notify', '$timeout',
						function($scope, $state, accountServices, $window, notify, $timeout){

	console.log('Login controller...', $state);

	if(typeof(Storage) === undefined){
		alert('Session Storage is not supported by this Browser');
	}

	// Generic Image Base Path
	$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');

	// Sign in redirect logic
    $scope.check_login = function(){
        loginRedirectFunction();
    }

    
    /* Login Redirect Functionality */
    function loginRedirectFunction(){
        accountServices.Login($scope.login.email, $scope.login.password)
        .then(function (response) {
            if (response.data != null && response.status === 200) {
                var paths = setImagePath(response.data.ProfileImage, response.data.LogoFileName);
                response.data.ProfileImage = paths.profileImg;
                response.data.LogoFileName = paths.logoImg;

                if(response.data.RoleID === 1){
                    $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), response.data));
                    $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':response.data.SessionID}));
                    $state.go('super.customerlist');
                }
                else if(response.data.RoleID > 1){
                    // Verify the account for validity
                    if(!response.data.CompanyStatus){
                        showNotification({message:'Subscription is disabled', duration: 3000, class:'notify-bg', routine:null});
                    }
                    else if(!compareSubscriptionDate(response.data.SubscriptionStartDate, response.data.SubscriptionEndDate)){
                        showNotification({message:'Subscription is ended', duration: 3000, class:'notify-bg', routine:null});
                    }
                    else{
                        if(response.data.RoleID === 2){
                            $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), response.data));
                            $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':response.data.SessionID}));
                            $state.go('admin.userlist');
                        }
                        else if(response.data.RoleID === 3){
                            $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), response.data));
                            $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':response.data.SessionID}));
                            $state.go('user.analysis');
                        }
                    }
                }
                else{
                    showNotification({message:'Invalid User Role: '+response.data, duration: 3000, class:'notify-bg', routine:null});
                }
            } else {
                showNotification({message:'Invalid login response status: '+response.status, duration: 3000, class:'notify-bg', routine:null});
            }
        },
        function (rejection) {
            if(rejection.status === 404){
                showNotification({message:'Invalid Login Info', duration: 3000, class:'notify-bg', routine:null});
            }
        });
    }
    

    /* setting path for image */
    function setImagePath(profileImg, logoImg){
    	var path = {};
   		path.profileImg = (profileImg !== '') ? ($scope.baseURL + profileImg) : ($scope.baseURL + 'avatars/avatar-2-256.png');
    	path.logoImg = (logoImg !== '') ? ($scope.baseURL + logoImg) : (path.logoImg = $scope.baseURL + 'avatars/demo_logo.jpg');
    	return path;
    }


    /* Compare Subscription dates for validity */
	function compareSubscriptionDate(Sdate, Edate){
		Sdate = new Date(Sdate).getTime();
		Edate = new Date(Edate).getTime();
		var Tdate = new Date().getTime();
		if((Sdate <= Edate) && (Tdate <= Edate)){
			return true;
		}
		return false;
	}


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}


	/* Images for Slider */
	$scope.images = [{
		src: 'content/img/slide/1.jpg',
			alt: 'Slide1'
		}, {
			src: 'content/img/slide/2.jpg',
			alt: 'Slide2'
		}, {
			src: 'content/img/slide/3.jpg',
			alt: 'Slide3'
		}, {
			src: 'content/img/slide/4.jpg',
			alt: 'Slide4'
		}, {
			src: 'content/img/slide/5.jpg',
			alt: 'Slide5'
		}
	];


}]);
