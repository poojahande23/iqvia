angular.module("imsApp")
.controller("AddCVMController", ['$scope', '$state', '$stateParams', 'companyServices', 'userServices', '$window', 'notify', 'loaderServices',
							function($scope, $state, $stateParams, companyServices, userServices, $window, notify, loaderServices){

	console.log('Add CVM controller...', $state);

	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));

	angular.element(document).ready(function(){});


	/* Initialize Models */
	initController();
	function initController(){
		loaderServices.startLoader();		// Loader 1 starts
		$scope.payload = {CompanyID: $scope.sessionInfo.CompanyID, ProductID: $stateParams.pcfId, MappedProductIds: null};
		initTwoSidedMultiselectWidget();
		initFlowBasedOnStateParams();
	}


	// create or edit flow based on State Param
	function initFlowBasedOnStateParams(){
		if($stateParams.pcfId){
			fetchProductCVM($stateParams.pcfId);
		}
		else{
			loaderServices.stopLoader();		// Loader 1 stops
			$state.go('admin.products');
		}
	}


	/* Fetch Company TC Classes */
	function fetchProductCVM(productId) {
		companyServices.GetProductCM(productId)
		.then(function (response) {
			if ((response.status === 200) && Boolean(response.data)) {
				$scope.ProductCVM = response.data;
			} else {
				$scope.ProductCVM = [];
			}
			$scope.rightArray = $scope.ProductCVM;
			fetchMasterTherapyList();
		},
		function (rejection) {});
	};


	/* Fetch Master Therapy list */
	function fetchMasterTherapyList() {
		userServices.GetTherapyListByTherapyCode('tc4', null)
		.then(function (response) {
			if ((response.status === 200) && Boolean(response.data)) {
				$scope.masterTherapyList = response.data;
				$scope.selectedTCode = $scope.masterTherapyList[0].TherapyCode;
				$scope.fetchProductsByTherapyCode();
			} else {
				$scope.masterTherapyList = [];
			}
		},
		function (rejection) {});
	};


	/* Fetch Products by Therapy Code */
	$scope.fetchProductsByTherapyCode = function() {
		loaderServices.startLoader();		// Loader 2 starts
		userServices.GetProductsByTherapyCode($scope.selectedTCode)
		.then(function (response) {
			loaderServices.stopLoader(true);		// Loader 1,2 stops
			if ((response.status === 200) && Boolean(response.data)) {
				$scope.productsList = response.data;
			} else {
				$scope.productsList = [];
			}
			applyProductList_leftArray($scope.productsList);
		},
		function (rejection) {});
	};


	/* Apply Product list to LeftArray */
	function applyProductList_leftArray(data){
		$scope.leftArray = data.filter(function(item){
			if($scope.rightArray.findIndex(x => x.ProductID === item.ProductID) === -1){
				return true;
			}
			return false;
		});
	}


	/* Add Product CVM */
	$scope.addProductCVM = function(){
		loaderServices.startLoader();		// Loader 3 starts
		$scope.payload.MappedProductIds = $scope.rightArray.map(function(item){ return item.ProductID; }).toString();
		companyServices.AddProductCVM($scope.payload)
		.then(function (response) {
			loaderServices.stopLoader();		// Loader 3 stops
			console.log('response: ',response);
			if((response.status === 200) && Boolean(response.data)){
				showNotification({message:'Product CVM added successfully', duration: 2000, class:null, routine:"$state.go('admin.products')"});
			}
			else{
				showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			}
		},
		function (rejection) {});
	}


	/* Two sided multiselect widget begins */
	function initTwoSidedMultiselectWidget(){
		$scope.leftBag = [];
		$scope.rightBag = [];
		$scope.leftArray = [];
		$scope.rightArray = [];
	}

	$scope.reserveElement = function(key, id){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var local_i = idsPool.indexOf(id);
		if(local_i > -1){
			idsPool.splice(local_i, 1);
		}
		else{
			idsPool.push(id);
		}
	};

	$scope.moveElements = function(key, source, destination){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var temp_i, temp_item = null;
		for(var i = 0; i < idsPool.length; i++){
			temp_i = source.findIndex(x => (x.ProductID === idsPool[i]));
			temp_item = source.splice(temp_i, 1)[0];
			destination.push(temp_item);
		}
		if(key === 'left'){ $scope.leftBag = []; }
		else{ $scope.rightBag = []; }
	};
	/* Two sided multiselect widget ends */


	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}

}]);
