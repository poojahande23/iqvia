angular.module("imsApp")
.controller("AddNewUserController", ['$scope', '$state', 'adminServices', 'companyServices', 'mediaServices', '$window', '$stateParams', '$uibModal', 'notify', '$rootScope',
							function($scope, $state, adminServices, companyServices, mediaServices, $window, $stateParams, $uibModal, notify, $rootScope){

	console.log('Add User controller...', $state);

	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));

	angular.element(document).ready(function(){});


	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
	  	hideLoader = "angular.element('.loader-backdrop').fadeOut();";
		$scope.rootScope = $rootScope;

		$scope.user = {};
		$scope.user.CompanyID = $scope.sessionInfo.CompanyID;
		$scope.user.Active = 1;

		$scope.imgBrowsed = false;
		$scope.avatarSelected = false;
		$scope.imgConfirmed = false;
		$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
		$scope.imgDataUrl = $scope.baseURL + 'avatars/avatar-2-256.png';
		$scope.user.ProfileImage = 'avatars/avatar-2-256.png';

		$scope.accessRightData = {};
		$scope.accessRightData.IsAllAccess = true;
		$scope.user.UserAccessRights = {};
		$scope.user.UserAccessRights.CityIDs = [];
		initTwoSidedMultiselectWidget();
	}

	// State Parmas
	if($stateParams.userId !== ''){
		FetchUserInfoById($stateParams.userId);
	}
	else{
		$scope.user.isUpdate = 0;
		FetchCompanyDivisions();
		FetchCompanySubscribedCityTCClass();
	}


	/* User Info by userId */
	function FetchUserInfoById(userId) {
		adminServices.GetUserInfo(userId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.user = response.data;
				applyUserInfoResponse();
			} else {
				$scope.user = false;
			}
		},
		function (rejection) {});
	}


	function applyUserInfoResponse() {
		$scope.user.isUpdate = 1;
		$scope.user.UserAccessRights = (Boolean($scope.user.UserAccessRights)) ? $scope.user.UserAccessRights : {};		// Boolean of null, '' & 0 is false; but {}, [] is true
		FetchCompanyDivisions({'initEditFlow':true});
		FetchCompanySubscribedCityTCClass();
		$scope.imgDataUrl = ($scope.user.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + $scope.user.ProfileImage);
	}


	/* Divisions */
	function FetchCompanyDivisions(inputObj) {		// {'initEditFlow':boolean}
		companyServices.GetCompanyDivisions($scope.user.CompanyID)
		.then(function (response) {
			if ((response.data.length > 0) && (response.status === 200)) {
				$scope.divisionColl = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.user.DivisionID !== null)){
					$scope.user.DivisionID = $scope.user.DivisionID;
				}
				else{
					$scope.user.DivisionID = $scope.divisionColl[0].DivisionID;
				}
				$scope.FetchUserRolesByCompany(inputObj);
			} else {
				showNotification({message:"Please create Divisions first", duration: 3000, class:"notify-bg", routine:"$state.go('admin.divisions');"});
			}
		},
		function (rejection) {});
	};


	/* User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function (inputObj) {		// {'initEditFlow':boolean}
		adminServices.GetUserRolesByCompany($scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if ((response.data.length > 0) && (response.status === 200)) {
				$scope.RolesColl = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.user.CompanyRoleID !== null)){
					$scope.user.CompanyRoleID = $scope.user.CompanyRoleID;
				}
				else{
					$scope.user.CompanyRoleID = $scope.RolesColl[0].CompanyUserRoleID;
				}
				$scope.FetchReportingPerson(inputObj);
			} else{
				showNotification({message:"Please create Roles first", duration: 3000, class:"notify-bg", routine:"$state.go('admin.userroles');"});
			}
		},
		function (rejection) {});
	};


	/* Fetch Company Reporting Manager based on Role Type */
	$scope.FetchReportingPerson = function(inputObj) {		// {'initEditFlow':boolean}
		companyServices.GetCompanyReportingManagerBasedOnRoleType($scope.user.CompanyRoleID, $scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if ((response.data != null) && (response.status === 200)) {
				$scope.ReportingPersons = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.user.ReportingManagerID !== null)){
					$scope.user.ReportingManagerID = $scope.user.ReportingManagerID;
				}
				else{
					$scope.user.ReportingManagerID = ($scope.ReportingPersons.length > 0) ? $scope.ReportingPersons[0].PersonID : null;
				}
			} else {
				$scope.ReportingPersons = [];
			}
		},
		function (rejection) {});
	};


	/* Image functionality begins */
    $scope.imageView = function(){
    	$scope.imgBrowsed = true;
		var reader = new FileReader();
		reader.onload = $scope.imageIsLoaded;
		reader.readAsDataURL($scope.ProfileImage);
	};


	$scope.imageIsLoaded = function(e){
	    $scope.$apply(function() {
	    	$scope.imgUrl = e.target.result;
	    });
	};


	function fetchAvatars() {
		$scope.avatarsList = ['avtar1.png', 'avtar2.png', 'avtar3.png', 'avtar4.png', 'avtar5.png', 'avtar6.png', 'avtar7.png', 'avtar8.png',
								'avtar9.png', 'avtar10.png', 'avtar11.png', 'avtar12.png', 'avtar13.png', 'avtar14.png', 'avtar15.png', 'avtar16.png'];
	}
	fetchAvatars();


	$scope.browsedConfirmed = function() {
		$scope.imgConfirmed = true;
		$scope.imgDataUrl = $scope.imgUrl;
		$scope.user.ProfileImage = $scope.ProfileImage;
	};


	$scope.avatarIsSelected = function(avatar) {
		$scope.avatarSelected = true;
		$scope.imgUrl = $scope.baseURL + 'avatars/' + avatar;
		$scope.ProfileImage = 'avatars/' + avatar;
	};

	/* Image Change modal begins */
	$scope.openImgChangeModal = function(UserID){
        var parent = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'imgChangeModal.html',
            windowTopClass: 'imageChangeModal',
            controller:['$scope','$uibModalInstance', function($scope,$uibModalInstance){
            	$scope.parent = parent;
				parent.imgUrl = parent.imgDataUrl;
				parent.ProfileImage = parent.user.ProfileImage;
				parent.imgBrowsed = false;
                $scope.closeModal = function(){
					$uibModalInstance.close('close');
					parent.browsedConfirmed();
                };

                $scope.dismissModal = function(){
                	parent.imgBrowsed = false;
                	parent.avatarSelected = false;
                    $uibModalInstance.dismiss('dismiss');
                };
            }]
        });
    };
    /* Image Change modal ends */


	/* Upload file to Database and return path */
	$scope.uploadFile = function(routine) {
        mediaServices.PostAnImage($scope.user.ProfileImage)
		.then(function (response) {
		    if (response.data != null && response.status === 201) {
		        if(response.data.Message === 'Image Updated Successfully.'){
		        	$scope.user.ProfileImage = $scope.user.ProfileImage.name;
		        	if(routine !== undefined){
		        		eval(routine);
		        	}
		        }
		    } else {
		      	console.log('Failed to upload image: ',response);
		    }
		},
		function (rejection) { });
    };


	/* Fetch List of Cities and TC Classes subscribed by Company */
	function FetchCompanySubscribedCityTCClass(){
		companyServices.GetCompanySubscribedCityTCClass($scope.user.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.accessRightData = response.data;
				if($scope.accessRightData.TherapyList !== null){
					filterTherapyListData($scope.accessRightData.TherapyList);
				}
				if($scope.user.isUpdate === 1){
					$scope.user.UserAccessRights.CityIDs
					= (!angular.element.isEmptyObject($scope.user.UserAccessRights) && Boolean($scope.user.UserAccessRights.CityIDs))
						? $scope.user.UserAccessRights.CityIDs.split(',').map(Number) : [];
				}
			} else {}
		},
		function (rejection) {});
	}

	/* Filter therapy list data into two arrays to feed into Two sided multiselect */
	function filterTherapyListData(data){
		if($scope.user.isUpdate === 0){
			$scope.leftArray = data;
		}
		else{
			if(!angular.element.isEmptyObject($scope.user.UserAccessRights) && Boolean($scope.user.UserAccessRights.TCValues)){
				var TcValues = $scope.user.UserAccessRights.TCValues.split(',');
				if(TcValues.length > 0){
					for(var i = 0; i < data.length; i++){
						if(TcValues.indexOf(data[i].TherapyCode) === -1){
							$scope.leftArray.push(data[i]);
						}
						else{
							$scope.rightArray.push(data[i]);
						}
					}
				}
				else{
					$scope.leftArray = data;
				}
			}
			else{
				$scope.leftArray = data;
			}
		}
	}

	/* Two sided multiselect widget begins */
	function initTwoSidedMultiselectWidget(){
		$scope.leftBag = [];
		$scope.rightBag = [];
		$scope.leftArray = [];
		$scope.rightArray = [];
	}

	$scope.reserveElement = function(key, id){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var local_i = idsPool.indexOf(id);
		if(local_i > -1){
			idsPool.splice(local_i, 1);
		}
		else{
			idsPool.push(id);
		}
	};

	$scope.moveElements = function(key, source, destination){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var temp_i, temp_item = null;
		for(var i = 0; i < idsPool.length; i++){
			temp_i = source.findIndex(x => (x.TherapyCode === idsPool[i]));
			temp_item = source.splice(temp_i, 1)[0];
			destination.push(temp_item);
		}
		if(key === 'left'){ $scope.leftBag = []; }
		else{ $scope.rightBag = []; }
	};
	/* Two sided multiselect widget ends */


	/* Prepare User info to be saved */
	function prepareUserToBeSaved(){
		$scope.user.RoleID = 3;
		$scope.user.UserAccessRights.AccessRightType = ($scope.accessRightData.IsAllAccess) ? 1 : 2;
		$scope.user.UserAccessRights.CityIDs = ($scope.user.UserAccessRights.CityIDs.length > 0) ? $scope.user.UserAccessRights.CityIDs.toString() : null;
		if($scope.rightArray.length > 0){
			$scope.user.UserAccessRights.TCValues = $scope.rightArray.map(function(item){return item.TherapyCode;}).toString();
		}
		else{
			$scope.user.UserAccessRights.TCValues = null;
		}
	}

	// Save User
	$scope.saveUser = function () {
		prepareUserToBeSaved();
	    adminServices.AddUpdateUser($scope.user)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('admin.userlist');
		    } else {}
		},
		function (rejection) { });
	};


	/* Upload Image file before saving user info */
	$scope.interceptToUploadImage = function() {
		if($scope.imgConfirmed && !($scope.avatarSelected)){
			$scope.uploadFile('$scope.saveUser();');
		}
		else{
			$scope.saveUser();
		}
	};


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}


}]);
