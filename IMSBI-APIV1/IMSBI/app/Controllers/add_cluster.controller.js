angular.module("imsApp")
.controller("AddClusterController", ['$scope', '$state', '$stateParams', 'userServices', 'companyServices', 'notify', '$window',
							function($scope, $state, $stateParams, userServices, companyServices, notify, $window){
	
	console.log('Add Cluster controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.addCluster = {};
	$scope.GeographyFilter = 'city';
	
	
	/* State Params */
	if($stateParams.clusterId !== ''){
		FetchClusterInfo($stateParams.clusterId);
	}
	else{
		FetchsubscribedCompanyList();
	}
	
	
	/* Fetch Cluster info by clusterId */
	function FetchClusterInfo(clusterId) {
		companyServices.GetCompanyClusterInfo(clusterId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.addCluster = response.data;
				FetchsubscribedCompanyList();
			} else {
				$scope.addCluster = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* Subscribed Companies List */
	function FetchsubscribedCompanyList() {
	    companyServices.GetsubscribedCompanies()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.customerList = response.data;
		        if($scope.addCluster.ClusterID === undefined){
		        	$scope.addCluster.CompanyID = response.data[0].CompanyID;
		        }
		        $scope.FetchCities();
		    } else {
		        $scope.customerList = false;
		    }
		},
		function (rejection) { });
	}
	
	
	/* City */
	$scope.FetchCities = function() {
		userServices.GetGeographyValues($scope.GeographyFilter, $scope.addCluster.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.cityColl = response.data;
				if($scope.addCluster.ClusterID === undefined){
					$scope.addCluster.CityID = $scope.cityColl[0].GeographyID;
				}
				$scope.FetchPatchesByCity();
			} else {
				$scope.cityColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Patches */
	$scope.FetchPatchesByCity = function () {
		userServices.GetPatchListBycityID($scope.addCluster.CityID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.patchesColl = response.data;
				if($scope.addCluster.ClusterID !== undefined){
					$scope.selectedPatches = $scope.addCluster.PatchIDs.split(',');
					$scope.selectedPatches = $scope.selectedPatches.map(function(item){ return Number(item); });
				}
			} else {
				$scope.patchesColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Save Cluster */
	$scope.saveCluster = function(){
		$scope.addCluster.PatchIDs = $scope.selectedPatches.toString();
		companyServices.AddUpdateCluster($scope.addCluster)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
		        $state.go('super.clusters');
			} else {
				notify({
		            message: 'Failed to save Cluster',
		            classes: 'notify-bg',
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			}
		},
		function (rejection) {});
	};
	
		
}]);
