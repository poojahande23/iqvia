angular.module("imsApp")
.controller("UserListController", ['$scope', '$state', '$stateParams', 'adminServices', '$window', '$uibModal', 'notify',
									function($scope, $state, $stateParams, adminServices, $window, $uibModal, notify){
	
	console.log('UserList controller...', $state);
	
	// Overwrite Session Info with state parameter data
	if($stateParams.sessionInfo !== ''){
		$window.sessionStorage.setItem('imsSessionInfo',JSON.stringify($stateParams.sessionInfo));
	}
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	// Model Initialization
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
	
	$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
	
	// Fetch company users count
	$scope.FetchCompanyUsersCount = function () {
	    adminServices.GetCompanyUserCount($scope.CompanyID)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.UserCountColl = response.data;
		        $scope.totalUserCount = computeTotalUserCount(response.data);
		    } else {
		        $scope.UserCountColl = false;
		    }
		},
		function (rejection) { });
	}
	$scope.FetchCompanyUsersCount();
	
	
	// Compute Total Users Count
	function computeTotalUserCount(data){
		var total = 0;
		for(var i = 0; i < data.length; i++){
			total += data[i].UserCount;
		}
		return total;
	}
	
	
	// Fetch company users
	$scope.FetchCompanyUsers = function () {
	    adminServices.GetCompanyUsers($scope.CompanyID, showLoader, hideLoader)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.users.data(response.data);
		        $scope.TotalData = response.data;
		    } else {
		        $scope.TotalData = false;
		    }
		},
		function (rejection) { });
	};
	$scope.FetchCompanyUsers();
	
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.UserName);
								});
			$scope.users.data(data);
		}
	};
	    
	// Grid columns configuration
	$scope.userListCols = [
		{ field: "UserName", title:"Name",headerTemplate: "Name<span class='k-icon k-i-sort-desc'></span>" ,template: "<div class='customer-photo-user-list'" 
			+ "style='background-image: url("+ $scope.baseURL +"#:ProfileImage#);'></div>" + "<div class='customer-name-user-list'>#:UserName#</div>", width:'250px' },
		{ field: "DivisionName", title: "Division", headerTemplate: "Division<span class='k-icon k-i-sort-desc'></span>", width:'100px' },
		{ field: "RoleName", title: "Designation", headerTemplate: "Designation<span class='k-icon k-i-sort-desc'></span>", width:'120px' },
		{ field: "ReportingManagerName", title: "Reporting Person", headerTemplate: "Reporting Person<span class='k-icon k-i-sort-desc'></span>", width:'150px' },
		{ field: "Email", title:"Email Address", headerTemplate: "Email Address<span class='k-icon k-i-sort-desc'></span>",width:"250px"},
		{ field: "MobileNo", title:"Phone No.", headerTemplate: "Phone No.<span class='k-icon k-i-sort-desc'></span>", width:'120px' },
		{ field: "Active", title: "Active", headerTemplate: "Active<span class='k-icon k-i-sort-desc'></span>", template: "<div><i class='glyphicon glyphicon-ok' aria-hidden='true' ng-if='(#=Active# === 1)'></i>"
			+ "<i class='glyphicon glyphicon-remove' aria-hidden='true' ng-if='(#=Active# === 0)'></i></div>", width: "90px" },
		{ field: "", title:"Edit", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ui-sref='admin.addnewuser({userId:#=UserID#})'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></div>", width:"70px" },
		{ field: "", title:"PWD", template:"<div class='icon-team-permission'>"+
			"<a href='javascript:void(0)' ng-click='openConfirmationModal(#=UserID#);'><i class='fa fa-key' aria-hidden='true'></i></a></div>", width:"70px" },
		{ field: "", title:"Delete", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='deleteConfirmationModal(#=UserID#, \"#=UserName#\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>", width:"70px" }
	];
	
	// Kendo Observable Array
	$scope.users = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "UserName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	
	$scope.teamBrkup=[
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
	];
	  
	$scope.statusUserlist=[
		{status:"Active"},
		{status:"Inactive"}
	];
	
	
	/* Change Password modal starts */
    $scope.openConfirmationModal = function(UserID){
        var self = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'setPasswordModal.html',
            size: 'md',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.passwords = {};
            	$scope.passwords.isAdmin = 1;
            	$scope.passwords.UserID = UserID;
            	$scope.showError = false;
                $scope.closeModal = function(){
                    $uibModalInstance.close('close');
                };
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.setNewPassword = function(e){
                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
                		$scope.showError = true;
                	}
                	else{
                		$scope.showError = false;
                		self.setPassword($scope.passwords, $uibModalInstance);
                	}
                };
            }]
        });
    };
    /* Change Password modal ends */
   
   	// Set Password
	$scope.setPassword = function(passwords, $uibModalInstance){
		adminServices.ChangePassword(passwords)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data === "updated."){
		    		$uibModalInstance.close('close');
		    		showNotification({message:'Password Updated Successfully', duration: 2000, class:null, routine:null});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		adminServices.DeleteUser(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchCompanyUsers();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
		
}]);
