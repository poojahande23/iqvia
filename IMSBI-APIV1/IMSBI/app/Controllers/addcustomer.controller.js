angular.module("imsApp")
.controller("AddCustomerController", ['$scope', '$state', 'userServices', 'companyServices', 'mediaServices', '$stateParams', '$uibModal', '$rootScope',
								function($scope, $state, userServices, companyServices, mediaServices, $stateParams, $uibModal, $rootScope){

	console.log('Add Customer controller...', $state);

	angular.element(document).ready(function(){});


	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
        hideLoader = "angular.element('.loader-backdrop').fadeOut();";

		$scope.rootScope = $rootScope;
		$scope.customer = {};
		$scope.customer.Country = 'India';
		$scope.customer.ContactInfo = {};
		$scope.customer.Action = 1;
        $scope.customer.ColorCode = '#874396';
		$scope.SubscriptionLocationArray = [];

		$scope.imgBrowsed = false;
		$scope.avatarSelected = false;
		$scope.imgConfirmed = false;
		$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
		$scope.imgDataUrl = $scope.baseURL + 'avatars/demo_logo.jpg';
		$scope.customer.ProfileImage = 'avatars/demo_logo.jpg';

		$scope.customer.CompanyAccessRights = {};
		$scope.accessRightTypesColl = [ {value:1, label:'All'}, {value:2, label:'Super Group'}, {value:3, label:'TC4 Classes'} ];
		$scope.customer.CompanyAccessRights.AccessRightType = 1;
		TherapyList_2 = []; TherapyList_3 = [];		// Global Variables
		initTwoSidedMultiselectWidget();

		$scope.SubscriptionPeriodTypes = [];
		$scope.customer.SubscriptionPeriodTypeIDs = null;
		$scope.customer.DefaultPeriodTypeID = 0;
	}


	// State Paramas
	if(($stateParams.customerId !== '') && ($stateParams.CompanyName !== '')){
		$scope.CompanyName = $stateParams.CompanyName;
		FetchCompanyInfoById($stateParams.customerId);
	}
	else{
		$scope.customer.isUpdate = 0;
		FetchCompanyList();
		FetchCityList();
		FetchSubscriptionPeriodTypes();
	}

	FetchStateList();

	// Fetch company info based on company_id
	function FetchCompanyInfoById(company_id) {
		companyServices.GetSubscribedCompanyInfo(company_id)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.customer = response.data;
				applyCompanyInfoResponse();
			} else {
				$scope.customer = {};
			}
		},
		function (rejection) {});
	}

	function applyCompanyInfoResponse() {
		$scope.customer.isUpdate = 1;
		$scope.customer.CompanyAccessRights = (Boolean($scope.customer.CompanyAccessRights)) ? $scope.customer.CompanyAccessRights : {};		// Boolean of null, '' & 0 is false; but {}, [] is true
		FetchSubscriptionPeriodTypes();
		FetchCityList();
		$scope.customer.Country = ($scope.customer.Country === '') ? 'India' : $scope.customer.Country;
		$scope.customer.SubscriptionStartDate = new Date($scope.customer.SubscriptionStartDate);
		$scope.customer.SubscriptionEndDate = new Date($scope.customer.SubscriptionEndDate);
		$scope.imgDataUrl = ($scope.customer.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + $scope.customer.ProfileImage);
		$scope.accessRightTypeChanged();
		$scope.customer.ContactInfo.is_same_as_company =
			(($scope.customer.ContactInfo.Address !== undefined) && ($scope.customer.ContactInfo.Address === $scope.customer.Address)) ? 1 : 0;
	}

	// Provision for Divisions
	$scope.addFields = function() {
		$scope.Divisions = [];
		for(var i = 0; i < Number($scope.customer.NoOfDivision); i++){
			$scope.Divisions.push({name: ''});
		}
	};


	// Datepicker initialization
	$scope.datePickerOptions = {
		start: 'date',
		depth: 'year'
	};

	$scope.customer.SubscriptionStartDate = new Date();
	$scope.customer.SubscriptionEndDate = new Date();


	$scope.dateChange = function(){
		if($scope.customer.SubscriptionStartDate.getTime() > $scope.customer.SubscriptionEndDate.getTime()){
			$scope.customer.SubscriptionStartDate = $scope.customer.SubscriptionEndDate;
		}
	};


	/* Company */
	function FetchCompanyList() {
		companyServices.GetUnsubscribedCompanies()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.unsubscribedCompanies = response.data;
			} else {
				$scope.unsubscribedCompanies = false;
			}
		},
		function (rejection) {});
	}

	/* City */
	function FetchCityList() {
		userServices.GetAllCities()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.cityList = response.data;
				if($stateParams.customerId !== ''){
					$scope.SubscriptionLocationArray = $scope.customer.SubscriptionLocationID.split(',').map(Number);
					if(($scope.customer.CityID === 0) || ($scope.customer.CityID === null)){
						$scope.customer.CityID = $scope.customer.CityName;
					}
				}
			} else {
				$scope.cityList = false;
			}
		},
		function (rejection) {});
	};

	/* State */
	function FetchStateList() {
		userServices.GetAllStates()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.stateList = response.data;
			} else {
				$scope.stateList = false;
			}
		},
		function (rejection) {});
	};

	/* Pincodes List based on search */
	$scope.SearchPincodesList = function(event) {
		companyServices.GetPinCodeList(Number(event.filter.value))
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.pincodesList = response.data;
				if($scope.pincodesList.length === 0){
					$scope.customer.Pincode = event.filter.value;
				}
			} else {
				$scope.pincodesList = false;
			}
		},
		function (rejection) {});
	};


	$scope.FetchCityStateByPinCode = function() {
		if($scope.customer.Pincode === null){
			$scope.selectedPinResult = [];
			$scope.customer.CityID = 0;
			$scope.customer.StateID = 0;
			$scope.customer.Country = 'India';
		}
		else{
			companyServices.GetCityStateByPinCode($scope.customer.Pincode)
			.then(function (response) {
				if (response.data != null && response.status === 200) {
					$scope.selectedPinResult = response.data;
					$scope.customer.CityID = $scope.selectedPinResult.CityID;
					$scope.customer.StateID = $scope.selectedPinResult.StateID;
					$scope.customer.Country = $scope.selectedPinResult.CountryName;
				} else {
					$scope.selectedPinResult = false;
				}
			},
			function (rejection) {});
		}
	};


	/* Capture City name if city does not exist in DB */
	$scope.getEnteredCityName = function(event) {
		$scope.customer.CityName = event.filter.value;
	};

	/* Set CityID on close */
	$scope.setCityIDonClose = function(event) {
		$scope.customer.CityID = ($scope.customer.CityID === null) ? 0 : $scope.customer.CityID;
	};


	/* Company Subscription Types */
	$scope.FetchSubscriptionTypes = function() {
		companyServices.GetSubscriptionType()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.subscriptionTypes = response.data;
			} else {
				$scope.subscriptionTypes = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchSubscriptionTypes();


	/* Secondary Address is same as Primary Address */
	$scope.sameAsAbove = function() {
		$scope.customer.ContactInfo.Address = ($scope.customer.ContactInfo.is_same_as_company === 1) ? $scope.customer.Address : '';
	};


    /* Image functionality begins */
    $scope.imageView = function() {
    	$scope.imgBrowsed = true;
		var reader = new FileReader();
		reader.onload = $scope.imageIsLoaded;
		reader.readAsDataURL($scope.ProfileImage);
	};


	$scope.imageIsLoaded = function(e){
	    $scope.$apply(function() {
	    	$scope.imgUrl = e.target.result;
	    });
	};


	function fetchAvatars() {
		$scope.avatarsList = ['demo_logo.jpg'];
	}
	fetchAvatars();


	$scope.browsedConfirmed = function() {
		$scope.imgConfirmed = true;
		$scope.imgDataUrl = $scope.imgUrl;
		$scope.customer.ProfileImage = $scope.ProfileImage;
	};


	$scope.avatarIsSelected = function(avatar) {
		$scope.avatarSelected = true;
		$scope.imgUrl = $scope.baseURL + 'avatars/' + avatar;
		$scope.ProfileImage = 'avatars/' + avatar;
	};

	/* Image Change modal begins */
	$scope.openImgChangeModal = function(UserID){
        var parent = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'imgChangeModal.html',
            windowTopClass: 'imageChangeModal',
            controller:['$scope','$uibModalInstance', function($scope,$uibModalInstance){
            	$scope.parent = parent;
				parent.imgUrl = parent.imgDataUrl;
				parent.ProfileImage = parent.customer.ProfileImage;
				parent.imgBrowsed = false;
                $scope.closeModal = function(){
					$uibModalInstance.close('close');
					parent.browsedConfirmed();
                };

                $scope.dismissModal = function(){
                	parent.imgBrowsed = false;
                	parent.avatarSelected = false;
                    $uibModalInstance.dismiss('dismiss');
                };
            }]
        });
    };
    /* Image Change modal ends */


	/* Upload file to Database and return path */
	$scope.uploadFile = function(routine) {
        mediaServices.PostAnImage($scope.customer.ProfileImage)
		.then(function (response) {
		    if (response.data != null && response.status === 201) {
		        if(response.data.Message === 'Image Updated Successfully.'){
		        	$scope.customer.ProfileImage = $scope.customer.ProfileImage.name;
		        	if(routine !== undefined){
		        		eval(routine);
		        	}
		        }
		    } else {
		      	console.log('Failed to upload image: ',response);
		    }
		},
		function (rejection) { });
    };


	/* Fetch TC values for changes in access rights */
	$scope.accessRightTypeChanged = function(){
		if($scope.customer.CompanyAccessRights.AccessRightType === 1){
		}
		else if($scope.customer.CompanyAccessRights.AccessRightType === 2){
			initTwoSidedMultiselectWidget();
			// Caching
			if(TherapyList_2.length > 0){ filterTherapyListData(TherapyList_2); }
			else{ fetchSuperGroupList(); }
		}
		else{
			initTwoSidedMultiselectWidget();
			// Caching
			if(TherapyList_3.length > 0){ filterTherapyListData(TherapyList_3); }
			else{ fetchTC4_TherapyList(); }
		}
	};

	/* Fetch Super Group Therapy list */
	function fetchSuperGroupList(){
		userServices.GetSupergroupList(showLoader, hideLoader)
		.then(function (response) {
			if (response.data !== null && response.status === 200 && response.data.length > 0) {
				TherapyList_2 = response.data;
				filterTherapyListData(TherapyList_2);
			}
		},
		function (rejection) {});
	}

	/* Fetch TC4 Therapy list */
	function fetchTC4_TherapyList(){
		companyServices.GetTherapyListByTherapyCode('tc4', null, showLoader, hideLoader)
        .then(function (response) {
            if (response.data !== null && response.status === 200 && response.data.length > 0) {
				TherapyList_3 = response.data;
				filterTherapyListData(TherapyList_3);
            }
        },
        function (rejection) {});
	}

	/* Filter therapy list data into two arrays to feed into Two sided multiselect */
	function filterTherapyListData(data){
		if($scope.customer.isUpdate === 0){
			$scope.leftArray = data;
		}
		else{
			if(!angular.element.isEmptyObject($scope.customer.CompanyAccessRights) && Boolean($scope.customer.CompanyAccessRights.TCValues)){
				var TcValues = $scope.customer.CompanyAccessRights.TCValues.split(',');
				if(TcValues.length > 0){
					for(var i = 0; i < data.length; i++){
						if(TcValues.indexOf(data[i].TherapyCode) === -1){
							$scope.leftArray.push(data[i]);
						}
						else{
							$scope.rightArray.push(data[i]);
						}
					}
				}
				else{
					$scope.leftArray = data;
				}
			}
			else{
				$scope.leftArray = data;
			}
		}
	}


	/* Two sided multiselect widget begins */
	function initTwoSidedMultiselectWidget(){
		$scope.leftBag = [];
		$scope.rightBag = [];
		$scope.leftArray = [];
		$scope.rightArray = [];
	}

	$scope.reserveElement = function(key, id){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var local_i = idsPool.indexOf(id);
		if(local_i > -1){
			idsPool.splice(local_i, 1);
		}
		else{
			idsPool.push(id);
		}
	};

	$scope.moveElements = function(key, source, destination){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var temp_i, temp_item = null;
		for(var i = 0; i < idsPool.length; i++){
			temp_i = source.findIndex(x => (x.TherapyCode === idsPool[i]));
			temp_item = source.splice(temp_i, 1)[0];
			destination.push(temp_item);
		}
		if(key === 'left'){ $scope.leftBag = []; }
		else{ $scope.rightBag = []; }
	};
	/* Two sided multiselect widget ends */


	/* Subscription Period Types begins */
	function FetchSubscriptionPeriodTypes(){
		userServices.GetSubscriptionPeriodTypes()
		.then(function (response) {
			if (response.data !== null && response.status === 200 && response.data.length > 0) {
				$scope.SubscriptionPeriodTypes = response.data;
				mutateSubscriptionPeriodTypesArray();
			}
		},
		function (rejection) {});
	}


	/* Mutate Subscription Period types array */
	function mutateSubscriptionPeriodTypesArray(){
		if(($scope.customer.isUpdate === 1) && Boolean($scope.customer.SubscriptionPeriodTypeIDs)){
			var selectedPeriodsArr = $scope.customer.SubscriptionPeriodTypeIDs.split(',').map(Number);
			for(var i = 0; i < $scope.SubscriptionPeriodTypes.length; i++){
				if(selectedPeriodsArr.indexOf($scope.SubscriptionPeriodTypes[i].SubscriptionPeriodTypeID) !== -1){
					$scope.SubscriptionPeriodTypes[i].selectedTypeID = $scope.SubscriptionPeriodTypes[i].SubscriptionPeriodTypeID;
				}
				else{
					$scope.SubscriptionPeriodTypes[i].selectedTypeID = 0;
				}
			}
			$scope.customer.DefaultPeriodTypeID = Boolean($scope.customer.DefaultPeriodID) ? $scope.customer.DefaultPeriodID : 0;
		}
		else{
			$scope.SubscriptionPeriodTypes = $scope.SubscriptionPeriodTypes.map(function(item){
												item.selectedTypeID = 0;
												return item;
											});
		}
	}


	// Disable Default if Period Type is unchecked
	$scope.SubPeriodTypeChanged = function(event){
		if(event.$parent.item.selectedTypeID === 0){
			if(event.$parent.item.SubscriptionPeriodTypeID === $scope.customer.DefaultPeriodTypeID){
				$scope.customer.DefaultPeriodTypeID = 0;
			}
		}
	};
	/* Subscription Period Types ends */


	/* Prepare Customer info to be saved */
	function prepareCustomerToBeSaved(){
		if($scope.customer.isUpdate === 0){
			$scope.customer.Divisions = $scope.Divisions.filter(function(item){ return (item.name !== ''); }).map(function(item){ return (item.name); });
			$scope.customer.Divisions = $scope.customer.Divisions.toString();
			$scope.customer.CompanyAccessRights.CompanyID = $scope.customer.CompanyID;		// Company Access Rights
		}
		$scope.customer.SubscriptionLocationID = ($scope.SubscriptionLocationArray.length > 0) ? $scope.SubscriptionLocationArray.toString() : null;
		/* Company Access Rights */
		if($scope.customer.CompanyAccessRights.AccessRightType === 1){
			$scope.customer.CompanyAccessRights.TCValues = null;
		}
		else{
			if($scope.rightArray.length > 0){
				$scope.customer.CompanyAccessRights.TCValues = $scope.rightArray.map(function(item){return item.TherapyCode;}).toString();
			}
			else{
				$scope.customer.CompanyAccessRights.TCValues = null;
			}
		}
		/* Subscription Period Types */
		var periodTypeArr = [];
		if($scope.SubscriptionPeriodTypes.length > 0){
			for(var i = 0; i < $scope.SubscriptionPeriodTypes.length; i++){
				if($scope.SubscriptionPeriodTypes[i].selectedTypeID !== 0){
					periodTypeArr.push($scope.SubscriptionPeriodTypes[i].selectedTypeID);
				}
			}
		}
		$scope.customer.SubscriptionPeriodTypeIDs = (periodTypeArr.length > 0) ? periodTypeArr.toString() : null;
	}

	/* Create Customer */
	$scope.saveCustomer = function () {
		prepareCustomerToBeSaved();
	    companyServices.AddCompanySubscription(JSON.stringify($scope.customer))
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('super.customerlist');
		    } else {}
		},
		function (rejection) { });
	};


	/* Upload Image file before saving user info */
	$scope.interceptToUploadImage = function() {
		if($scope.imgConfirmed && !($scope.avatarSelected)){
			$scope.uploadFile('$scope.saveCustomer();');
		}
		else{
			$scope.saveCustomer();
		}
	};


}]);
