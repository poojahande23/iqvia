angular.module("imsApp")
.controller("CustomerListController", ['$scope', '$state', '$stateParams', '$window', 'companyServices', 'adminServices', '$uibModal', 'notify',
								function($scope, $state, $stateParams, $window, companyServices, adminServices, $uibModal, notify){
	
	console.log('CustomerList controller...', $state);
	
	// Overwrite Session Info with state parameter data
	if($stateParams.sessionInfo !== ''){
		$window.sessionStorage.setItem('imsSessionInfo',JSON.stringify($stateParams.sessionInfo));
	}
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
                        
	$scope.statuscustlist = [ { action: "Active" }, { action: "Inactive" } ];
	
	// Subscription Count
	$scope.FetchCompanyCountBasedOnSubscription = function () {
	    companyServices.GetCompanyCountBasedOnSubscription()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.companyCountSubscription = response.data;
		    } else {
		        $scope.companyCountSubscription = false;
		    }
		},
		function (rejection) { });
	}
	$scope.FetchCompanyCountBasedOnSubscription();
	
	
	// Customer or Company List
	$scope.FetchsubscribedCompanyList = function () {
	    companyServices.GetsubscribedCompanies()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
		    } else {
		        $scope.customerList = false;
		    }
		},
		function (rejection) { });
	}
	$scope.FetchsubscribedCompanyList();

	
	$scope.customerListCols = [
						 { field: "CompanyName", title: "Company Name", headerTemplate: "Company Name<span class='k-icon k-i-sort-desc'></span>", width: "200px" },
						 { field: "LocationName", title: "Locations", headerTemplate: "Locations<span class='k-icon k-i-sort-desc'></span>", width: "150px" },
						 { field: "Division", title: "Division", headerTemplate: "Division<span class='k-icon k-i-sort-desc'></span>", width: "100px" },
						 { field: "TeamStrength", title: "Team Strength", headerTemplate: "Team Str<span class='k-icon k-i-sort-desc'></span>", width: "120px" },
						 { field: "ActiveUsers", title: "Active Users", headerTemplate: "Active Users<span class='k-icon k-i-sort-desc'></span>", width: "120px" },
						 { field: "SubscriptionName", title: "Subscription Type", headerTemplate: "Subscription Type<span class='k-icon k-i-sort-desc'></span>",
						 	template: "<span class='subscription-box' style='background-color:#:ColorCode#'>" + "<span class='box-text'>#:SubscriptionName#</span></span>"
						 	+ "<p class='filter-section-title'>#:DaysRemaning# days to go</p>", width: "180px" },
						 { field: "SubscriptionLocation", title: "Subscription Location", headerTemplate: "Subscription Location<span class='k-icon k-i-sort-desc'></span>",
						 	template: "<div>#=SubscriptionLocation#</div>", width: "200px" },
						 { field: "", title:"Contact Info", template:"<div>#=ContactPersonName#</div><div>#=ContactPersonPhone#</div>", width:"150px" },
						 { field: "Action", title: "Action", headerTemplate: "Action<span class='k-icon k-i-sort-desc'></span>", template: "<div><i class='glyphicon glyphicon-ok' aria-hidden='true' ng-if='(#=Action# === 1)'></i>"
						 	+ "<i class='glyphicon glyphicon-remove' aria-hidden='true' ng-if='(#=Action# === 0)'></i></div>", width: "100px" },
						 { field: "", title:"Edit", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ui-sref='super.addcustomer({customerId:#=CompanyID#, CompanyName:\"#=CompanyName#\"})'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></div>", width:"50px" },
						 { field: "", title:"PWD", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='openConfirmationModal(#=AdminUserID#);'><i class='fa fa-key' aria-hidden='true'></i></a></div>", width:"70px" },
						 { field: "", title:"Delete", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='deleteConfirmationModal(#=CompanyID#, \"#=CompanyName#\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>", width:"70px" },
						 { field: "", title: "", headerTemplate: "", template: "<a ng-click='NavigateToComapany(#=CompanyID#);' class='icon-custom-1' title='To Dashboard'><i class='glyphicon glyphicon-circle-arrow-right' aria-hidden='true'></i></a>", width: "100px" }
					];
					
	
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 5,
		sort: {
            field: "CompanyName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 5, 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
				
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if(($scope.TotalData !== undefined) || ($scope.TotalData !== null) || ($scope.TotalData.length > 0)){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
							return regExpression.test(item.CompanyName);
						});
			$scope.observableList.data(data);
		}
	}
	
	
	/* Change Password modal starts */
    $scope.openConfirmationModal = function(UserID){
        var self = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'setPasswordModal.html',
            size: 'md',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.passwords = {};
            	$scope.passwords.isAdmin = 1;
            	$scope.passwords.UserID = UserID;
            	$scope.showError = false;
                $scope.closeModal = function(){
                    $uibModalInstance.close('close');
                };
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.setNewPassword = function(e){
                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
                		$scope.showError = true;
                	}
                	else{
                		$scope.showError = false;
                		self.setPassword($scope.passwords, $uibModalInstance);
                	}
                };
            }]
        });
    };
    /* Change Password modal ends */
   
   
   	// Set Password
	$scope.setPassword = function(passwords, $uibModalInstance){
		adminServices.ChangePassword(passwords)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data === "updated."){
		    		$uibModalInstance.close('close');
		    		showNotification({message:'Password Updated Successfully', duration: 2000, class:null, routine:null});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteCompany(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchsubscribedCompanyList();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
	
	// Navigate to the selected Company page
	$scope.NavigateToComapany = function(companyId) {
		$scope.sessionInfo.CompanyID = companyId;
		$state.go('admin.userlist', {'sessionInfo':$scope.sessionInfo});
	};
	
}]);
