angular.module("imsApp")
.controller("AnalysisController", ['$scope', '$state', 'userServices', 'companyServices', 'analysisServices', 'savedSearhcesServices', 'notify', '$window', '$document', '$rootScope', '$timeout','$uibModal',
							function($scope, $state, userServices, companyServices, analysisServices, savedSearhcesServices, notify, $window, $document, $rootScope, $timeout, $uibModal){
	console.log('Analysis controller...', $state);

	/* Make $rootScope available in Local $scope */
	$scope.rootScope = $rootScope;

	/* Parse session data to object format */
	function sessionInfoToScope(){
		$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	}
	sessionInfoToScope();

	/* Static Data & Values */
	$scope.categoryColl = [
		{name:"SKU", val:'sku'},
		{name:"Brand", val:'brand'},
		{name:"Brand Group", val:'brandgp'},
		{name:"Focus Brand", val:'focusbr'},
		{name:"New Introduction", val:'newintro'}
	];

	$scope.indexMatrix=[
		{matrix:"KPI Matrix"},
		{matrix:"Performance Slope"}
	];
	$scope.indexMatrix1=[
		{matrix1:"Product"},
		{matrix1:"Geography"},
		{matrix1:"Manufacturers"}
	];
	$scope.indexMatrix2=[
		{matrix2:"Market Share %"},
		{matrix2:"Sales Growth"},
		{matrix2:"Growth % vs 3M"},
	];
	$scope.indexMatrix3=[
		{matrix3:"Trend"},
		{matrix3:"Year on year"},
		{matrix3:"Z chart"}
	];


	/* Initialize Models & Variables */
	initVariables();
	function initVariables() {
        showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
        hideLoader = "angular.element('.loader-backdrop').fadeOut();";
		BarBgColor = '#B7E0EE';
		maxY_bar_Value = 0, maxY_line_Value = 0, maxY_bar_Unit = 0, maxY_line_Unit = 0;

        $scope.fetchData = {};
        $scope.fetchData.CompanyID = $scope.sessionInfo.CompanyID;
		$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;		// Division to which user's role is associated
        $scope.fetchData.GeographyFilter = 'city';
        $scope.fetchData.CategoryType = 'brand';
        $scope.marketSalesRadio = 'overallSales';
        $scope.selectedPatchId = undefined;
        $scope.fetchData.TCCode = 2;
        $scope.fetchData.TCValue = 'all';
        $scope.userId = $scope.sessionInfo.UserID;
        $scope.tabType = 1;
        $scope.isCluster = false;
        $scope.fetchData.includeTopSearches = 1;
        $scope.filteredCategoryValues = [];
        $scope.isPreset = false;
        $scope.RegExNumForm = /(\d)(?=(\d{2})+\d\.)/g;
        // Unit measures dropdown
        $scope.unitOfMeasuresColls = [
                                        {key:'value', label:'Values', divisor:1, unit:'', decimal:0},
                                        {key:'value', label:'Values(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'value', label:'Values(Cr)', divisor:10000000, unit:'Cr', decimal:2},
                                        {key:'unit', label:'Units', divisor:1, unit:'', decimal:0},
                                        {key:'unit', label:'Units(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'unit', label:'Units(Cr)', divisor:10000000, unit:'Cr', decimal:2}
                                    ];
        $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
        $scope.UOM_set_flag = false;
        $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[0];
        $scope.UOM_MS_set_flag = false;
    }

	/* Edit Session info */
	function updateSessionInfo(GeographyID, PeriodTypeId){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		if(GeographyID !== undefined){ sessionInfo.GeographyID = GeographyID; }
		if(PeriodTypeId !== undefined){ sessionInfo.PeriodTypeID = PeriodTypeId; }
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
	}

	/* Divisions
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.fetchData.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.fetchData.DivisonID = $scope.divisionColl[0].DivisionID;
				$scope.FetchGeographyValues();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();*/

	/* Geography */
	$scope.FetchGeographyValues = function (presetData) {
		userServices.GetGeographyValues($scope.fetchData.GeographyFilter, $scope.fetchData.CompanyID, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.geographyColl = response.data;
				if($scope.sessionInfo.GeographyID === undefined){
					$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
					updateSessionInfo($scope.geographyColl[0].GeographyID, undefined);
				}
				else{
					$scope.fetchData.GeographyID = $scope.sessionInfo.GeographyID;
				}
				FetchPeriodTypes(presetData);
			} else {
				$scope.geographyColl = [];
			}
		},
		function (rejection) {});
	};


	/* begin invoking service calls */
	initServiceCalls();
	function initServiceCalls(){
		$scope.FetchGeographyValues();
	}


	/* Period Type */
	function FetchPeriodTypes(presetData) {
		userServices.GetSubscriptionPeriodType($scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.periodTypeColl = response.data;
				if($scope.sessionInfo.PeriodTypeID === undefined){
					$scope.PeriodTypeID = $scope.periodTypeColl.filter(function(item){ return (item.isDefault); })[0].SubscriptionPeriodTypeID;
		            updateSessionInfo(undefined, $scope.PeriodTypeID);
		        }
		        else{
		            $scope.PeriodTypeID = $scope.sessionInfo.PeriodTypeID;
		        }
				$scope.FetchTimePeriods({presetData:presetData, callNextFunciton:true});
			} else {
				$scope.periodTypeColl = [];
			}
		},
		function (rejection) {});
	}


	/* Time Periods */
	$scope.FetchTimePeriods = function (inputObj) {		// Accepts object of form {presetData:String, callNextFunciton:Boolean}
		$scope.period = {};
		userServices.GetTimePeriods($scope.fetchData.GeographyID, $scope.PeriodTypeID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.timePeriods = response.data;
				if((inputObj === undefined) || (inputObj.presetData === undefined)){
					$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
					$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
				}
				// Queue in next function based on flow type
				if((inputObj !== undefined) && inputObj.callNextFunciton){
					$scope.FetchBricksByGeography(inputObj.presetData);
				}
			} else {
				$scope.timePeriods = [];
			}
		},
		function (rejection) {});
	};


	/* Period Type changed */
	$scope.periodTypeChanged = function(){
		updateSessionInfo(undefined, $scope.PeriodTypeID);
		$scope.FetchTimePeriods();
	};


	/* Validating the time selected */
	$scope.timeSelected = function(event){
		if($scope.period.startPeriod > $scope.period.endPeriod){
			$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
			$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		}
	};


	/* Geography selected from dropdown by user */
	$scope.geographySelected = function(){
		updateSessionInfo($scope.fetchData.GeographyID, undefined);
        $scope.FetchTimePeriods();
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				$scope.selectedPatches = [];
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
		},
		function (rejection) {});
	};


	/* Bricks */
	$scope.FetchBricksByGeography = function (presetData) {
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
			// Fetch values for first category by default
			if(presetData !== undefined){
				$scope.isPreset = true;
				$scope.FetchCategoryValues('$scope.displayResult('+presetData+')');
			}
			else{
				$scope.isPreset = false;
				$scope.FetchCategoryValues('$scope.displayResult()');
			}
		},
		function (rejection) {});
	};


	/* Fetch Category Values */
	$scope.FetchCategoryValues = function (customRoutine) {
		$scope.cats = {};
		$scope.cats.selectedCatValues = [];
		$scope.filteredCategoryValues = [];
		userServices.GetCategoryValues($scope.fetchData.CategoryType, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.categoryValues = response.data;
				if($scope.isPreset){
					$scope.filteredCategoryValues = $scope.categoryValues;
				}
				eval(customRoutine);
			} else {
				$scope.categoryValues = false;
			}
		},
		function (rejection) {});
	};


	/* Filter Category values array */
	$scope.filterCategoryValues = function(searchQuery) {
		if(searchQuery !== ''){
			var regExpression = new RegExp(searchQuery, 'i');
			$scope.filteredCategoryValues = $scope.categoryValues.filter(function(item){ return regExpression.test(item.CategoryValue); });
		}
	};


	$scope.displayResult = function (presetData) {
		if(presetData !== undefined){
			applyPresetData(presetData);
		}
		else{
			// Extract PeriodShortName based on its ID
			$scope.fetchData.PeriodType = $scope.periodTypeColl.filter(function(item){ return (item.SubscriptionPeriodTypeID === $scope.PeriodTypeID); })[0].PeriodShortName;
			$scope.fetchData.PeriodStart = $scope.period.startPeriod;
			$scope.fetchData.PeriodEnd = $scope.period.endPeriod;


			if(($scope.cats.selectedCatValues !== undefined) && ($scope.cats.selectedCatValues.length > 0) && ($scope.cats.selectedCatValues !== 'all')){
			    $scope.fetchData.CategoryValue = $scope.cats.selectedCatValues.toString();
			}
			else{
				$scope.fetchData.CategoryValue = 'all';
				$scope.fetchData.includeTopSearches = 1;
			}


			if(($scope.selectedPatches !== undefined) && ($scope.selectedPatches.length > 0)){
				$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
			}
			else{
				$scope.fetchData.PatchIds = 'all';
			}

			$scope.selectedPatchId = undefined;
			$scope.fetchData.TCValue = 'all';
			fetchPatchesByCluster();
		}
	};

	// Get Patches by Clusters
	function fetchPatchesByCluster(){
		if($scope.isCluster && ($scope.fetchData.PatchIds !== 'all')){
			userServices.GetPatchIDsByClusterIDs($scope.fetchData.PatchIds, showLoader, hideLoader)
			.then(function (response) {
				if (response.data != null && response.status === 200) {
					$scope.fetchData.PatchIds = response.data;
					// Fetch Final Result
					if($scope.tabPanelState === 1){
						fetchAnalysisDashboardData();
					}
					else if($scope.tabPanelState === 2){
						fetchAnalysisMarketShareData();
					}
				} else {
					$scope.fetchData.PatchIds = '';
				}
			},
			function (rejection) {});
		}
		else{
			// Fetch Final Result
			if($scope.tabPanelState === 1){
				fetchAnalysisDashboardData();
			}
			else if($scope.tabPanelState === 2){
				fetchAnalysisMarketShareData();
			}
		}
	}


	/* Fetch Analysis Dashboard Data */
	function fetchAnalysisDashboardData () {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
		analysisServices.GetAnalysisDashboardData($scope.fetchData)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
				$scope.analysisDashboardData = response.data;
				$scope.patchList = $scope.analysisDashboardData.PatchList;
				// Split Sales by Period chart
				initLineBarChart();
				// Split Sales by Patch chart
				initMultiBarChart();

                resetUOM_MinValueLength($scope.analysisDashboardData.BarChartSales);
				$scope.unitOfMeasuresChanged();

			} else {
				$scope.analysisDashboardData = false;
			}
		},
		function (rejection) {});
	}


	/* Fetch Analysis Market Share Data */
	function fetchAnalysisMarketShareData() {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
		analysisServices.GetAnalysisMarketShareData($scope.fetchData)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
			    $scope.analysisMarketShareData = response.data;
			    $scope.TherapyList = $scope.analysisMarketShareData.TherapyList;

                resetUOM_MS_MinValueLength($scope.analysisMarketShareData.AnalysisMarketShareProductList);
			    $scope.unitOfMeasuresChanged_MS();
			} else {
				$scope.analysisMarketShareData = false;
			}
		},
		function (rejection) {});
	}


	/* Select Preset functionality Begins */
	$scope.savePreset = function(){
		savedSearhcesServices.AddUpdateSavedSearches(preparePresetData())
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
				initializeOnPreset();
			    notify({
		            message: 'Presets saved Successfully',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			} else {

			}
		},
		function (rejection) {});
	};


	function preparePresetData () {
		var presetObject = {};
		presetObject.SearchKey = $scope.PresetName;
		presetObject.UserID = $scope.userId;
		presetObject.TabType = $scope.tabType;

		// Extract PeriodShortName based on its ID
		$scope.fetchData.PeriodType = $scope.periodTypeColl.filter(function(item){ return (item.SubscriptionPeriodTypeID === $scope.PeriodTypeID); })[0].PeriodShortName;
		$scope.fetchData.PeriodStart = $scope.period.startPeriod;
		$scope.fetchData.PeriodEnd = $scope.period.endPeriod;

		if(($scope.selectedPatches !== undefined) && ($scope.selectedPatches.length > 0)){
			$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
		}
		else{
			$scope.fetchData.PatchIds = 'all';
		}

		if(($scope.cats.selectedCatValues !== undefined) && ($scope.cats.selectedCatValues.length > 0) && ($scope.cats.selectedCatValues !== 'all')){
		    $scope.fetchData.CategoryValue = $scope.cats.selectedCatValues.toString();
		}
		else{
			$scope.fetchData.CategoryValue = 'all';
			$scope.fetchData.includeTopSearches = 1;
		}

        // Remove SessionID before saving preset
        if($scope.fetchData.SessionID !== undefined){
            delete $scope.fetchData.SessionID;
        }
		presetObject.SearchData = JSON.stringify($scope.fetchData);

		return presetObject;
	}


	/* Get Presets */
	function LoadPresets(){
		savedSearhcesServices.GetSavedSearchesbyUser($scope.userId, $scope.tabType)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.presetsColl = response.data;
				console.log($scope.presetsColl);
			} else {
				$scope.presetsColl = false;
			}
		},
		function (rejection) {});
	}
	LoadPresets();

	$scope.applySelectedPreset = function() {
		angular.element('.loader-backdrop').fadeIn();
		var presetData = $scope.presetsColl.filter(function(item){ return (item.SearchKeyID === $scope.selectedPreset); })[0].SearchData;
		var presetData_obj = JSON.parse(presetData);
		$scope.fetchData.GeographyFilter = presetData_obj.GeographyFilter;
		$scope.fetchData.DivisonID = presetData_obj.DivisonID;
		$scope.fetchData.CategoryType = presetData_obj.CategoryType;
		var local_PeriodID = $scope.periodTypeColl.filter(function(item){ return (item.PeriodShortName === presetData_obj.PeriodType); })[0].SubscriptionPeriodTypeID;
		updateSessionInfo(presetData_obj.GeographyID, local_PeriodID);
		$scope.FetchGeographyValues(presetData);
		console.log($scope.presetsColl);
	};

 $(".kendo-del").kendoComboBox({
	 template: '<a class="k-button" href="#"><span class="k-icon k-i-copy"></span> Copy</a>',
		});

	// Apply Preset data to fetchData
	function applyPresetData(presetData){
		$scope.fetchData = presetData;

		if($scope.fetchData.PeriodStart !== 0){
			$scope.period.startPeriod = $scope.fetchData.PeriodStart;
			$scope.period.endPeriod = $scope.fetchData.PeriodEnd;
		}

		if($scope.fetchData.PatchIds !== 'all'){
			$scope.selectedPatches = $scope.fetchData.PatchIds.split(',').map(Number);
		}
		else{
			$scope.selectedPatches = [];
		}

		if($scope.fetchData.CategoryValue !== 'all'){
		     var selectedCatValues_local = $scope.fetchData.CategoryValue.split(',');
		     $scope.cats.selectedCatValues = selectedCatValues_local.map(function(item){ return Number(item); });
		}
		else{
			$scope.cats.selectedCatValues = [];
		}
		angular.element('.loader-backdrop').fadeOut();
	}


	// Initialize filter choices on preset
	function initializeOnPreset(){
		LoadPresets();
		$scope.PresetName = '';
		$scope.SavePresetFlag = false;
		$scope.fetchData.GeographyFilter = 'city';
		$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
		$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
		$scope.selectedPatches = [];
		$scope.PeriodTypeID = $scope.sessionInfo.PeriodTypeID;
		$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
		$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		$scope.fetchData.CategoryType = 'brand';
		$scope.cats.selectedCatValues = [];
		$scope.fetchData.includeTopSearches = 1;
		$scope.filteredCategoryValues = [];
		$scope.isPreset = false;
	}
	/* Select Preset functionality Ends */



	/* Graphs & Charts start */

	$scope.salesRadioChanged = function(){
		if($scope.marketSalesRadio === 'overallSales'){
			$timeout(function(){
				$scope.apiLineBar.refresh();
			}, 100);
		}
		else if($scope.marketSalesRadio === 'productSales'){
			$timeout(function(){
				$scope.apiMultiBar.refresh();
			}, 100);
		}
	};


	/* Structure for Top Products table */
	$scope.gridOptions = {
		columns: $scope.topProductsColl,
		dataSource: $scope.topProducts,
		// toolbar: ["excel"],
		excel: {
			fileName: "Market Sales Potential.xlsx"
		}
	};

	var topProductsCollBluePrint = [
	  { field: "Rank", title:"Rank", width:"50px", headerTemplate:"Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>"},
	  { field: "", title:"Brand", width:"150px", template:"#:Brand#" }
	];

	// Dynamically add columns to data grid
	function alterProductListGridStructure(firstProd) {
		$scope.topProductsColl = topProductsCollBluePrint.slice();
		var measure = ($scope.unitOfMeasures.key === 'value') ? 'Value' : 'Unit';

		for(var i = 0; i < firstProd.MonthData.length; i++){
			$scope.topProductsColl.push({ field: "MonthData["+i+"]."+measure, title: "", headerTemplate: firstProd.MonthData[i].MonthName +$scope.figureQualifier+ " <span class='k-icon k-i-sort-desc'></span>",
				template: "#= MonthData["+i+"] !== undefined ? kendo.toString(MonthData["+i+"]."+measure+"/"+$scope.unitOfMeasures.divisor+", 'n"+$scope.unitOfMeasures.decimal+"') : 0#", width: "130px" });
		}
	}


    /* Reset UOM based on Min value length */
    function resetUOM_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_set_flag)){
            var formattedArray = [], minVal = 0, min_local = 0, minLen = 1;
            for(var i = 0; i < data.length; i++){
                // Find min from multidimensional array
                formattedArray = data[i].values.filter(function(item){ return (item[1] >= 1); }).map(function(item){ return (item[1]); });
                min_local = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
                if(i === 0){ minVal = min_local; }
                if(min_local < minVal){
                    minVal = min_local;
                }
            }
            minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[2];
            }
        }
    }


	/* Action of Sales Radio changed */
	$scope.unitOfMeasuresChanged = function(inputObj){
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

		if($scope.analysisDashboardData !== undefined){
			// Prepare a Figure Qualifier
	    	$scope.figureQualifier = ($scope.unitOfMeasures.unit !== '') ? (" ("+$scope.unitOfMeasures.unit+")") : '';
			var LineBarData = [];
			var maxObj = {};

			if($scope.unitOfMeasures.key === 'value'){
				$scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductList;
				console.log($scope.topProducts);
				$scope.tableDataArrColm = $scope.analysisDashboardData.GetAnalysisProductList[0].MonthData.map(function(item){
					return(item.MonthName);
				});

				$scope.repeatTableDataList = $scope.analysisDashboardData.AnalysisComparisonSalesPatchTable;
				$scope.TableDataListColm = $scope.analysisDashboardData.AnalysisComparisonSalesPatchTable[0].TableData[0].MonthData.map(function(item){
					return(item.MonthName);
				});

				// var measure = ($scope.unitOfMeasures.key === 'value') ? 'Value' : 'Unit';
			    // Line bar chart
			    if($scope.analysisDashboardData.BarChartSales[0].originalKey === undefined){
			    	LineBarData = $scope.analysisDashboardData.BarChartSales;
				    LineBarData[0].bar = true;
				    maxObj =  dateToMilliseconds(LineBarData);
					maxY_bar_Value = maxObj.maxBar;
					maxY_line_Value = maxObj.maxLine;
				    $scope.line_bar.data = LineBarData;
				    $scope.line_bar.data.map(function(series) {
						                series.values = series.values.map(function(d) { return {x: d[0], y: d[1]} });
						                return series;
						            });
			    }
			    else{
			    	$scope.line_bar.data = $scope.analysisDashboardData.BarChartSales;
			    }

			    // Force Y axis range to certain max values
				if(($scope.analysisDashboardData.BarChartSales[0].disabled === undefined) || !($scope.analysisDashboardData.BarChartSales[0].disabled)){
					$scope.line_bar.options.chart.forceY = [0, maxY_bar_Value];
				}
				else{
					$scope.line_bar.options.chart.forceY = [0, maxY_line_Value];
				}
				$scope.line_bar.options.chart.y1Axis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiLineBar.update();

	         	// Multi-bar chart & Data Grid
	         	if($scope.selectedPatchId === undefined){
		         	$scope.multibar.data = $scope.analysisDashboardData.AnalysisComparisonSales;
		         	$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
		         	$scope.apiMultiBar.update();

                    // Product List Table
                    alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductList[0]);
                    $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductList;

		        }
		        else{
		        	mutateGroupChartForSales();
                    productListByPatchForSales();
		        }
			}
			else{
				$scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductListUnit;
				console.log($scope.topProducts);
				$scope.tableDataArrColm = $scope.analysisDashboardData.GetAnalysisProductListUnit[0].MonthData.map(function(item){
					return(item.MonthName);
				});
				$scope.repeatTableDataList = $scope.analysisDashboardData.AnalysisComparisonUnitPatchTable;
				$scope.TableDataListColm = $scope.analysisDashboardData.AnalysisComparisonUnitPatchTable[0].TableData[0].MonthData.map(function(item){
					return(item.MonthName);
				});
			    // Line bar chart
			    if($scope.analysisDashboardData.BarChartUnit[0].originalKey === undefined){
				    LineBarData = $scope.analysisDashboardData.BarChartUnit;
				    LineBarData[0].bar = true;
					maxObj =  dateToMilliseconds(LineBarData);
					maxY_bar_Unit = maxObj.maxBar;
					maxY_line_Unit = maxObj.maxLine;
					$scope.line_bar.data = LineBarData;
					$scope.line_bar.data.map(function(series) {
						                series.values = series.values.map(function(d) { return {x: d[0], y: d[1]} });
						                return series;
						            });

				}
				else{
			    	$scope.line_bar.data = $scope.analysisDashboardData.BarChartUnit;
			    }

			    // Force Y axis range to certain max values
				if(($scope.analysisDashboardData.BarChartUnit[0].disabled === undefined) || !($scope.analysisDashboardData.BarChartUnit[0].disabled)){
					$scope.line_bar.options.chart.forceY = [0, maxY_bar_Unit];
				}
				else{
					$scope.line_bar.options.chart.forceY = [0, maxY_line_Unit];
				}
				$scope.line_bar.options.chart.y1Axis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiLineBar.update();

				// Multi-bar chart
				if($scope.selectedPatchId === undefined){
					$scope.multibar.data = $scope.analysisDashboardData.AnalysisComparisonUnit;
		         	$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		         	$scope.apiMultiBar.update();

                    // Product List Table
                    alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductListUnit[0]);
                    $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductListUnit;
				}
		        else{
		        	mutateGroupChartForUnits();
                    productListByPatchForSales();
		        }
			}
		}
	};


	/*--------------------- Divide a number(n) by (d) and round upto (x) decimal places ---------------------*/
	$scope.DivideAndRound = function(n, d, x) {
		if(x === 0){
			n = (n/d).toFixed(1);
			n = (n.replace($scope.RegExNumForm, '$1,'));
			n = n.substring(0, (n.length-2));
		}
		else{
			n = (n/d).toFixed(x);
			n = (n.replace($scope.RegExNumForm, '$1,'));
		}
		return n;
	}


	// Convert Periods into Milliseconds since ?/?/1970
	function dateToMilliseconds(lineBarData){
		var output = {maxBar:0, maxLine:0};
		var max_local = 0;
		for(var i = 0; i < lineBarData.length; i++){
			if(i === 0){
				// Find max from multidimensional array
	        	output.maxBar = lineBarData[i].values.reduce(function(accu,item){ return ((accu[1] > item[1]) ? accu : item); })[1];
			}
			else{
				max_local = lineBarData[i].values.reduce(function(accu,item){ return ((accu[1] > item[1]) ? accu : item); })[1];
				if(output.maxLine < max_local){
					output.maxLine = max_local;
				}
			}

			for(var j = 0; j < lineBarData[i].values.length; j++){
				// lineBarData[i].values[j][0] = new Date(lineBarData[i].values[j][0]).getTime();
			}
		}
		return output;
	}


	/* Patch Id selected */
	$scope.patchIdSelected = function(event){
		if(event.item === undefined){
			$scope.selectedPatchId = undefined;
			$scope.repeatTableDataList = $scope.analysisDashboardData.AnalysisComparisonSalesPatchTable;
			$scope.TableDataListColm = $scope.analysisDashboardData.AnalysisComparisonSalesPatchTable[0].TableData[0].MonthData.map(function(item){
				return(item.MonthName);
			});
			if($scope.unitOfMeasures.key === 'value'){
				$scope.multibar.data = $scope.analysisDashboardData.AnalysisComparisonSales;
		     	$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
		     	$scope.apiMultiBar.update();
				//  alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductList[0]);
				// $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductList;

			}
			else{
				$scope.multibar.data = $scope.analysisDashboardData.AnalysisComparisonUnit;
		     	$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;

					$scope.apiMultiBar.update();
				// alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductListUnit[0]);
				// $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductListUnit;

			}
            // Product List Table

		}
		else{
			$scope.selectedPatchId = event.item.PatchID;
			$scope.tableDataList = $scope.analysisDashboardData.AnalysisComparisonSalesPatchTable
									.filter(function(item){
										return (item.PatchID === $scope.selectedPatchId);
									});
			for(var i=0;i<$scope.tableDataList.length;i++){
						$scope.tableDataList2 = $scope.tableDataList[i].TableData;
						console.log($scope.tableDataList2);
					}
			for(var i=0; i<$scope.tableDataList.length;i++ ){
				$scope.PatchData =  $scope.tableDataList[i];
			}
			if($scope.unitOfMeasures.key === 'value'){
				mutateGroupChartForSales();
			}
			else{
				mutateGroupChartForUnits();
			}
		}
	};

	/* Mutate "GrouChartSalesPatch" array for Sales data */
	function mutateGroupChartForSales(){
		var dataArrData = $scope.analysisDashboardData.AnalysisComparisonByPatchSales.filter(function(item){
								return (item.PatchID === $scope.selectedPatchId);
							});


		$scope.multibar.data = [];
		if(dataArrData.length>0)
		{
			var dataArr = dataArrData[0].PatchDataDetails;
			for(var i = 0; i < dataArr.length; i++){
				$scope.multibar.data.push({"key":dataArr[i].key, "ColorCode":dataArr[i].ColorCode, "values":[]});
				$scope.multibar.data[i].values = dataArr[i].values;
			}
		}
		$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		$scope.tableDataList = $scope.analysisDashboardData.AnalysisComparisonSalesPatchTable
								.filter(function(item){
									return (item.PatchID === $scope.selectedPatchId);
								});

		for(var i=0;i<$scope.tableDataList.length;i++){
			$scope.tableDataList2 = $scope.tableDataList[i].TableData;
		}
		$scope.apiMultiBar.update();
	}

	/* Mutate "GrouChartSalesPatch" array for Units data */
	function mutateGroupChartForUnits(){
		var dataArrData  = $scope.analysisDashboardData.AnalysisComparisonByPatchUnit.filter(function(item){
								return (item.PatchID === $scope.selectedPatchId);
							});

		$scope.multibar.data = [];
		if(dataArrData.length>0)
		{
				var dataArr = dataArrData[0].PatchDataDetails;
				for(var i = 0; i < dataArr.length; i++){
					$scope.multibar.data.push({"key":dataArr[i].key, "ColorCode":dataArr[i].ColorCode, "values":[]});
					$scope.multibar.data[i].values = dataArr[i].values;
				}
		}
		$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		$scope.tableDataList = $scope.analysisDashboardData.AnalysisComparisonUnitPatchTable
								.filter(function(item){
									return (item.PatchID === $scope.selectedPatchId);
								});

		for(var i=0;i<$scope.tableDataList.length;i++){
			$scope.tableDataList2 = $scope.tableDataList[i].TableData;
		}
		$scope.apiMultiBar.update();
	}

	/* Therapy Id selected */
	$scope.TherapyIdSelected = function(event) {
		if(event.item === undefined){
			$scope.fetchData.TCValue = 'all';
		}
		else{
			$scope.fetchData.TCValue = event.item.TherapyID;
		}
        fetchAnalysisMarketShareData();
	};


    /* Reset UOM_MS based on Min value length */
    function resetUOM_MS_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_MS_set_flag)){
            var formattedArray = data.filter(function(item){ return (item.MarketValue >= 1); }).map(function(item){ return (item.MarketValue); });
            var minVal = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
            var minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[2];
            }
        }
    }


	/* Unit of Measures Changed in Market Share */
	$scope.unitOfMeasuresChanged_MS = function(inputObj) {
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

		// Prepare a Figure Qualifier
    	$scope.figureQualifier_2 = ($scope.unitOfMeasures_MS.unit !== '') ? (" ("+$scope.unitOfMeasures_MS.unit+")") : '';

		if($scope.unitOfMeasures_MS.key === 'value'){
			// prepareStackedAreaChart($scope.analysisMarketShareData.AnalysisMarketShareGraphData);
            prepareMultiBarChart_2($scope.analysisMarketShareData.AnalysisMarketShareGraphData);

            // Data Grid formation
			prepareGrid_MSV();
		}
		else{
			// prepareStackedAreaChart($scope.analysisMarketShareData.AnalysisMarketShareGraphDataUnit);
            prepareMultiBarChart_2($scope.analysisMarketShareData.AnalysisMarketShareGraphDataUnit);

            // Data Grid formation
			prepareGrid_MSU();
		}
		$scope.topProducts2 = $scope.analysisMarketShareData.AnalysisMarketShareProductList;
	};


	/* Column structure for Top Products table in Market Share */
	function prepareGrid_MSV(){
		$scope.topProductsColl2 = [
		  { field: "Rank", title:"Rank", width:"50px", headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width:"55px"},
		  { field: "", title:"Brand", template: "#:Brand#", width:"150px" },
		  { field: "", title:"TC Code", template: "#:TCCode#", width:"70px" },
		  { field: "TotalSale", title: "Value", headerTemplate: "Value "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(TotalSale/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketValue", title: "Market Value", headerTemplate: "Market Value * "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(MarketValue/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketShare", title: "% Market Share", headerTemplate: "% Market Share <span class='k-icon k-i-sort-desc'></span>",
		  	template: "#=kendo.toString(MarketShare, 'n')#", width: "130px" }
		];
	}

	function prepareGrid_MSU(){
		$scope.topProductsColl2 = [
		  { field: "Rank", title:"Rank", width:"50px", headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width:"55px"},
		  { field: "", title:"Brand", template: "#:Brand#", width:"150px" },
		  { field: "", title:"TC Code", template: "#:TCCode#", width:"70px" },
		  { field: "NoOfUnit", title: "Units", headerTemplate: "Units "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(NoOfUnit/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketUnit", title: "Market Unit", headerTemplate: "Market Unit * "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(MarketUnit/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketUnitShare", title: "% Market Share", headerTemplate: "% Market Share <span class='k-icon k-i-sort-desc'></span>",
		  	template: "#=kendo.toString(MarketUnitShare, 'n')#", width: "130px" }
		];
	}



	/* ============================================ NVD3 - Line Bar Chart ============================================ */

		function initLineBarChart(){
			$scope.line_bar = {};

			$scope.line_bar.options = {
	            chart: {
	                type: 'linePlusBarChart',
	                height: 465,
	                margin: {
	                    top: 20,
	                    right: 100,
	                    bottom: 120,
	                    left: 75
	                },
	                bars: {
	                    forceY: [0]
	                },
	                bars2: {
	                    forceY: [0]
	                },
	                x: function(d,i) { return i },
	                xAxis: {
	                    axisLabel: 'Period',
	                    tickFormat: function(d) {
	                        var dx = $scope.line_bar.data[0].values[d] && $scope.line_bar.data[0].values[d].x || 0;
	                        if(dx) {
								return dx;
	                        }
	                        return null;
	                    }
	                    // ,ticks: 12
	                },
	                x2Axis: {
	                    tickFormat: function(d) {
	                        /*var dx = $scope.line_bar.data[0].values[d] && $scope.line_bar.data[1].values[d].x || 0;
	                        if(dx > 0) {
	                        	return d3.time.format('%b-%Y')(new Date(dx));
	                        }
	                        return null;*/
	                    },
	                    showMaxMin: false
	                },
	                y: function(d){
	                	return d.y;
	                },
	                y1Axis: {
	                    axisLabel: 'Sales (k)',
	                    tickFormat: function(d){
	                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	                    },
	                    axisLabelDistance: 12
	                },
	                y2Axis: {
	                    axisLabel: 'Sales',
	                    tickFormat: function(d) {
	                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	                    },
	                    axisLabelDistance: 10
	                },
	                focusEnable: false,
                    color: function (d, i) {
                        if(i === 0){
                            return BarBgColor;
                        }
                        else{
                            return d.ColorCode;
                        }
                    },
					noData: 'No Data to display',
	                legend: {
	                	maxKeyLength: 30,
	                	key: function(d){
	                		return d.originalKey
	                	},
	                	margin: {
	                		top: 0,
	                		right: 0,
	                		bottom: 0,
	                		left: 0
	                	},
						dispatch: {
							legendClick: function(t,u){
								if(t.bar){
									$timeout(function(){
										if((t.disabled === undefined) || t.disabled){
											$scope.line_bar.options.chart.forceY = ($scope.unitOfMeasures.key === 'value') ? [0, maxY_line_Value] : [0, maxY_line_Unit];
											$scope.apiLineBar.refresh();
										}
										else{
											$scope.line_bar.options.chart.forceY = ($scope.unitOfMeasures.key === 'value') ? [0, maxY_bar_Value] : [0, maxY_bar_Unit];
											$scope.apiLineBar.refresh();
										}
									}, 100);
								}
							}
						}
	                },
	                switchYAxisOrder: false,
	                tooltip: {
	                	keyFormatter: function(d, i){
	                		return ((d !== undefined) ? d.replace('(right axis)','') : d);
	                	},
	                	valueFormatter: function (d, i) {
							return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
						}
	                },
	                pointShape: function(d){
	                	return d.shape || 'square';
	                }
	            }
	        };

			var lineBarData = [];

			$scope.line_bar.data = lineBarData.map(function(series) {
                                        series.values = series.values.map(function(d) { return {x: d[0], y: d[1]} });
                                        return series;
                                    });
		}



	/* ============================================ NVD3 - Multibar Chart ============================================ */

		function initMultiBarChart(){
			$scope.multibar = {};
			$scope.multibar.data = [];
			$scope.multibar.options = {
	            chart: {
	                type: 'multiBarChart',
	                height: 480,
	                margin: {
	                    top: 40,
	                    right: 25,
	                    bottom: 140,
	                    left: 75
	                },
	                clipEdge: true,
	                //staggerLabels: true,
	                showLegend: true,
	                legend: {
	                	maxKeyLength: 11
	                },
	                duration: 500,
	                stacked: true,
	                xAxis: {
	                    axisLabel: 'Patches',
	                    showMaxMin: false,
	                    tickFormat: function(d){
	                        return (d.length > 15) ? (d.substr(0,14) + '...') : d;
	                    },
	                    staggerLabels: true,
	                    axisLabelDistance: 15
	                },
	                yAxis: {
	                    axisLabel: 'Sales (k)',
	                    axisLabelDistance: 12,
	                    tickFormat: function(d){
	                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	                    }
	                },
	                stacked: false,
	                noData: 'No Data to display',
                    color: function (d, i) {
                        return d.ColorCode;
                    },
	                tooltip: {
	                	valueFormatter: function (d, i) {
							return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
						}
	                }
	            }
	        };

	        $scope.multibar.data = [{
			    "key": "Stream0",
			    "values": []
			}];
       }


    /* ============================================ NVD3 - Multibar Chart for Market Share ============================================ */

        function prepareMultiBarChart_2(data){
            // Convert Periods into Milliseconds since ?/?/1970
            if (data.length > 0) {
                if (data[0].values[0].x === undefined) {
                    for (var i = 0; i < data.length; i++) {
                        data[i].values = data[i].values.map(function(item){
                            return {x: item[0], y: item[1]};
                        });
                    }
                }
            }
            if (data.length > 0) {
                $scope.multibar_2.data = data;
            }
            else {
                $scope.multibar_2.data = [
                    {
                        "key": "",
                        "values": []
                    }
                ];
            }
            $scope.apiMultiBar_2.update();
        }

        initMultiBarChart_2();
        function initMultiBarChart_2(){
            $scope.multibar_2 = {};

            $scope.multibar_2.options = {
                chart: {
                    type: 'multiBarChart',
                    height: 450,
                    margin: {
                        top: 40,
                        right: 25,
                        bottom: 140,
                        left: 75
                    },
                    clipEdge: true,
                    //staggerLabels: true,
                    showLegend: true,
                    legend: {
                        maxKeyLength: 25
                    },
                    duration: 500,
                    xAxis: {
                        axisLabel: 'Periods',
                        showMaxMin: false,
                        tickFormat: function(d){
                            return d;
                        },
                        staggerLabels: true,
                        axisLabelDistance: 0
                    },
                    yAxis: {
                        axisLabel: 'Market Share %',
                        axisLabelDistance: 12,
                        tickFormat: function(d){
                            return (d);
                        }
                    },
                    stacked: false,
                    showControls: false,
                    noData: 'No Data to display',
                    color: function (d, i) {
                        return d.ColorCode;
                    }
                }
            };

            $scope.multibar_2.data = [{
                "key": "Stream0",
                "values": []
            }];
        }


	/* ============================================ NVD3 - Stacked Area Chart ============================================ */

	/*function prepareStackedAreaChart(data) {
	// Convert Periods into Milliseconds since ?/?/1970
		if (data.length > 0) {
			if (Math.ceil(Math.log((data[0].values[0][0]) + 1) / Math.LN10) === 6) {
				for (var i = 0; i < data.length; i++) {
					for (var j = 0; j < data[i].values.length; j++) {
						data[i].values[j][0] = data[i].values[j][0].toString();
						data[i].values[j][0] = data[i].values[j][0].substr(0, 4) + ' ' + data[i].values[j][0].substr(4, 2);
						data[i].values[j][0] = new Date(data[i].values[j][0]).getTime();
					}
				}
			}
		}
		if (data.length > 0) {
			$scope.stacked.data = data;
		}
		else {
			$scope.stacked.data = [
				{
					"key": "",
					"values": []
				}
			];
		}
		$scope.apiStacked.update();
	}

		initStackedAreaChart();
		function initStackedAreaChart() {
			$scope.stacked = {};

			$scope.stacked.options = {
	              chart: {
	                  type: 'stackedAreaChart',
	                  height: 450,
	                  margin : {
	                      top: 50,
	                      right: 30,
	                      bottom: 100,
	                      left: 70
	                  },
	                  x: function(d){return d[0];},
	                  y: function(d){return d[1];},
	                  useVoronoi: false,
	                  clipEdge: true,
	                  duration: 100,
	                  useInteractiveGuideline: true,
	                  xAxis: {
	                      axisLabel: 'Period',
	                      showMaxMin: true,
	                      tickFormat: function (d) {
	                          return d3.time.format('%b-%Y')(new Date(d));
	                      }
	                  },
	                  yAxis: {
	                      axisLabel: 'Market Share %',
	                      tickFormat: function (d) {
                              return d3.format(',.1f')(d);
	                      }
	                  },
					  color: function (d, i) {
                          return d.ColorCode;
                      },
	                  showLegend: true,
	                  showControls:false,
					  legend: {
								margin: {
									top: 10,
									right: 0,
									bottom: 5,
									left: 0
								}
					  },
	                  noData: 'No Data to display'
	              }
	          };

	          $scope.stacked.data = [
	              {
	                  "key" : "" ,
	                  "values" : []
	              }
	          ];
		}*/


	/* ============================================ NVD3 - Pie Chart ============================================ */


    /* Assign function for refreshing all charts and graphs to rootScope */
    $rootScope.refreshCharts_Analysis = function(){
        $timeout(function(){
            $scope.apiLineBar.refresh();
            $scope.apiMultiBar.refresh();
            $scope.apiMultiBar_2.refresh();
        }, 500);
    };

	/* Graphs & Charts end */



	/* Export functionality starts */
	$scope.exportToPDF = function(){
		/* workign-block-1 >>>>> var _svg = document.getElementById('svgChart');
		console.log(_svg.innerHTML);
		var _canvas = document.getElementById('canvas');
		canvg(_canvas, _svg.innerHTML);
		var imgData = canvas.toDataURL('image/png');
		var doc = new jsPDF('p', 'mm');
        doc.addImage(imgData, 'PNG', 10, 10);
        doc.save('sample-file-1.pdf'); <<<<< workign-block-1 */

		/*kendo.drawing.drawDOM(angular.element('#marketSalesWrapper'))
		.then(function(group){
			// kendo.drawing.pdf.saveAs(group, 'Market Sales Potential Chart.pdf');
			return kendo.drawing.exportPDF(group, {
				paperSize: "auto"
			});
		})
		.done(function(data){
			kendo.saveAs({
				dataURI: data,
				fileName: "Market Sales Potential Chart.pdf"
			});
		});*/


		/*var reqBody = {};
		reqBody.PDFName = 'MarketSales';
        reqBody.PDFData = angular.element('#marketSalesWrapper').html();
        analysisServices.GeneratePDF(reqBody, showLoader, hideLoader)
		.then(function (response) {
				if (response.data !== null && response.status === 200) {
                    kendo.saveAs({
                        dataURI: response.data,
                        fileName: "Market Sales Potential Chart.pdf"
                    });
				} else {
					// $scope.analysisDashboardData = false;
				}
			},
			function (rejection) {});*/


		/*var svgElements = $("#marketSalesWrapper").find('svg.nvd3-svg').css({'width':'992px'});

      //replace all svgs with a temp canvas
      svgElements.each(function() {
        var canvas, xml;

        // canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
        $.each($(this).find('[style*=em]'), function(index, el) {
          $(this).css('font-size', getStyle(el, 'font-size'));
        });

        canvas = document.createElement("canvas");
        canvas.className = "screenShotTempCanvas";
        //convert SVG into a XML string
        xml = (new XMLSerializer()).serializeToString(this);

        // Removing the name space as IE throws an error
        xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

        //draw the SVG onto a canvas
        canvg(canvas, xml);
        $(canvas).insertAfter(this);
        //hide the SVG element
        ////this.className = "tempHide";
        $(this).attr('class', 'tempHide');
        $(this).hide();
      });

		html2canvas(document.getElementById('marketSalesWrapper'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("html2canvas.pdf");
            }
        });

        angular.element("#marketSalesWrapper").find('.screenShotTempCanvas').remove();
      	angular.element("#marketSalesWrapper").find('.tempHide').show().removeClass('tempHide');*/

        /* Create fabric instance */
		/*var canvas = new fabric.StaticCanvas( undefined, {
		    width: 500,
		    height: 500,
		} );

		// Load SVG document; add to canvas;
		fabric.parseSVGDocument(document.getElementById('marketSalesWrapper').getElementsByTagName("svg")[0],function(objects, options) {
		    var g = fabric.util.groupSVGElements( objects, options );
		    var data = "";

		    // ADD ELEMENTS
		    canvas.add( g );

		    // EXPORT TO DATA URL
		    data = canvas.toDataURL({
		        format: "pdf"
		    });

		    console.log('fabric data: ',data);

		    var docDefinition = {
                content: [{
                    image: data,
                    width: 500,
                }]
            };
            pdfMake.createPdf(docDefinition).download("fabricjs.pdf");

		// REVIVER
		},function(svg,group) {
		    // possiblitiy to filter the svg elements or adapt the fabric object
		});*/
	};
	/* Export functionality ends */


	/* Document Ready starts */
	angular.element(document).ready(function() {
		/* Angular Provision for Tab Panel */
		$scope.tabRadio = true;
		$scope.tabPanelState = 1;
		$scope.filtersAltered = true;
		$scope.showTab = function(id1, id2){
			$scope.tabRadio = !($scope.tabRadio);
			angular.element(id1).fadeOut(500, function(){
				angular.element(id2).fadeIn(500, function(){
					if(id2 === '#tabs-5-tab-2'){
						$scope.apiMultiBar_2.refresh();
						$scope.tabPanelState = 2;
						if($scope.analysisMarketShareData === undefined || $scope.filtersAltered){
							$scope.filtersAltered = false;
							$scope.displayResult();
						}
					}
					else{
						$scope.tabPanelState = 1;
						if($scope.filtersAltered){
							$scope.filtersAltered = false;
							$scope.displayResult();
						}
						else{
							$scope.apiLineBar.refresh();
							$scope.apiMultiBar.refresh();
						}
					}
				});
			});
		};

		angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
    	});


		/*----- Enlarged-Panel functionality starts -----*/

        angular.element('#resize_map').click(function() {
        	if($scope.tabPanelState === 1){
		        angular.element('#filter-overlay-1').show();
		        angular.element('.filter-close').show();
		        angular.element('#container_graph_1').addClass('enlarged');
				if($scope.marketSalesRadio === 'overallSales'){
					$scope.apiLineBar.refresh();
				}
				else{
					$scope.apiMultiBar.refresh();
				}
			}
			else{
				angular.element('#filter-overlay-2').show();
		        angular.element('.filter-close').show();
		        angular.element('#container_graph_2').addClass('enlarged');
				$scope.apiMultiBar_2.refresh();
			}
        });

        angular.element('.tab-content').on('click', '.filter-close', function() {
        	if($scope.tabPanelState === 1){
			    angular.element('#filter-overlay-1').hide();
			    angular.element('.filter-close').hide();
			    angular.element('#container_graph_1').removeClass('enlarged');
			    if($scope.marketSalesRadio === 'overallSales'){
			    	$scope.apiLineBar.refresh();
			    }
			    else{
			    	$scope.apiMultiBar.refresh();
			    }
			}
			else{
				angular.element('#filter-overlay-2').hide();
			    angular.element('.filter-close').hide();
			    angular.element('#container_graph_2').removeClass('enlarged');
			    $scope.apiMultiBar_2.refresh();
			}
		});

		/*----- Enlarged-Panel functionality ends -----*/


		/* Scroll to div by id */
		$scope.scrollById = function() {
			if($scope.tabPanelState === 1){
                $document.scrollToElementAnimated(angular.element('#dataGrid1'), 125);
			}
			else{
                $document.scrollToElementAnimated(angular.element('#dataGrid2'), 125);
			}
		};

		/*Scroll to Preset input box when its checkbox is checked*/
		$scope.PresetFlagChanged = function() {
			if($scope.SavePresetFlag){
				// angular.element('#dummy-presetname').scrollTop = 200;
				console.log('if flag');
			}
			else{
				console.log('else flag');
			}
		};

		$scope.itemTemplate = $("#template").html();
		$scope.valueTemplate = "<b>{{ dataItem.SearchKey }}</b>";


		/* Delete Confimation modal starts */
	    $scope.deleteConfirmationModal = function(recordId){
				console.log(recordId);
	        var self = $scope;
	        var confirmDeleteModal = $uibModal.open({
	            templateUrl:'deleteConfimationModal.html',
	            size: 'sm',
	            windowTopClass: 'modal-top-class',
	            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
	            	$scope.message = "Do you want delete the Selected Preset";
	                $scope.dismissModal = function(){
	                    $uibModalInstance.dismiss('dismiss');
	                };
	                $scope.confirmDelete = function(e){
	                	self.deleteSelected($uibModalInstance, recordId);
										console.log(recordId);
	                };
	            }]
	        });
	    };
	    /* Delete Confimation modal ends */

		$scope.deleteSelected = function($uibModalInstance, id){
			savedSearhcesServices.DeleteSavedSearches(id)
			.then(function (response) {
					if (response.data != null && response.status === 200) {
					if(response.data){
						 $scope.msg ="delete";
						 $uibModalInstance.close('close');
						 showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'LoadPresets()'});
					 }
				 else{
					 showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
				 }

			 } else {}
		 },
		 function (rejection) { });
		}

		// Show Notification
		function showNotification(options){
			notify({
	            message: options.message,
	            classes: options.class,
	            templateUrl: $scope.template,
	            position: 'center',
	            duration: (options.duration !== null) ? options.duration : 2000
	        });
	        eval(options.routine);
		}

	});
	/* Document Ready ends */

}]);
