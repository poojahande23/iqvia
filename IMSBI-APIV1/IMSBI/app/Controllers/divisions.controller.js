angular.module("imsApp")
.controller("DivisionsController", ['$scope', '$state', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, companyServices, $window, $uibModal, notify){
	
	console.log('Divisions controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
	
	/* Fetch Company Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
	
  							
	$scope.gridColumns = [
		{ field: "DivisionName", title:"Division Name",headerTemplate: "Division Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=DivisionID#, \"#=DivisionName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='admin.add_division({id:#=data.DivisionID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"30px"
		}
	];
	
	
	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "DivisionName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.divisionName);
								});
			$scope.observableList.data(data);
		}
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteDivision(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchCompanyDivisions();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}	
	
}]);
