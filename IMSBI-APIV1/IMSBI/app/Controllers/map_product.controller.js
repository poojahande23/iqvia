angular.module("imsApp")
.controller("MapProductController", ['$scope', '$state', '$window', 'companyServices', 'notify', '$http',
								function($scope, $state, $window, companyServices, notify, $http){
	
	console.log('Map Product controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.submitted = false;
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
  	
  	function initVariables() {
  		$scope.mapProdDiv = {};
		$scope.mapProdDiv.CompanyID = $scope.sessionInfo.CompanyID;
		angular.element('.dropZoneContainer .title').html("Drag File here to Upload");
		angular.element('.fileContainer .name').html("Click here to upload Excel File");
		console.log('$scope.dropFileMsg: ',$scope.dropFileMsg);
  	}
  	initVariables();
	
	
	/* Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.mapProdDiv.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.mapProdDiv.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.FetchProductsByCompanyDivisions();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
	
	
	/* Fetch Company Product list */
	$scope.FetchCompanyProductList = function () {
		companyServices.GetCompanyProductList($scope.mapProdDiv.CompanyID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.productList = response.data;
			} else {
				$scope.productList = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyProductList();
	
	if(-1){
		console.log('if');
	}
	else{
		console.log('else');
	}
	/*------------ BULK MAPPING begins ------------*/
	
	/* Excel Upload */
	$scope.ExcelToJSON = function(){
		if($scope.ExcelFile !== undefined){
			var data = new Uint8Array($scope.ExcelFile);
			var arr_local = new Array();
			for(var i = 0; i < data.length; i++){
				arr_local[i] = String.fromCharCode(data[i]);
			}
			var str_local = arr_local.join('');
			
			var spreadsheet = XLSX.read(str_local, {type:'binary'});
			
			var worksheet = spreadsheet.Sheets[spreadsheet.SheetNames[0]];
			var worksheet_json = XLSX.utils.sheet_to_json(worksheet,{raw:true});
			var work_json_filtered = worksheet_json.filter(function(item){
					 return ($scope.divisionColl.findIndex(x => (x.DivisionName === item['BI Division Name']) ) !== -1);
				});
			if(work_json_filtered.length > 0) {
				var mutated_list = work_json_filtered.map(function(item){
					item.FOCUSED = (/^TRUE$/i.test(item.FOCUSED) || item.FOCUSED === 1) ? 1 : 0;
					item.New = (/^TRUE$/i.test(item.New) || item.New === 1) ? 1 : 0; 
					return ({'PFCID':item.PFC, 'IsFocus':item.FOCUSED, 'IsNew':item.New, 'BIDivisionName':item['BI Division Name']}); 
				});
				return {status:true, data:mutated_list};
			}
			else{
				return {status:false, data:'No valid row to map'};
			}
		}
		else{
		    return {status:false, data:'Please upload an excel'};
		}
	};
	
	$scope.bulkMapProductsToDivision = function(){
		var body = {};
		body.CompanyID = $scope.mapProdDiv.CompanyID;
		body.DivisionID = $scope.mapProdDiv.DivisionID;
		var excel_JSON = $scope.ExcelToJSON();
		if(excel_JSON.status){
			body.Productmapping = excel_JSON.data;
			companyServices.BulkProductMapping(body)
			.then(function (response) {
			    if (response.data != null && response.status === 200) {
			    	showNotification({message:'Products Mapped with Division successfully', duration: 2000, class:null, 
			    		routine:'$scope.FetchProductsByCompanyDivisions(); initVariables();'});
			    } else {
			       
			    }
			},
			function (rejection) { });
		}
		else{
			showNotification({message:excel_JSON.data, duration: 2000, class:'notify-bg', routine:null});
		}
			
	};

	/*------------ BULK MAPPING ends ------------*/
	
	
	/*------------ INDIVIDUAL MAPPING begins ------------*/
	/* Add Product to Division */
	$scope.mapProductToDivision = function(){
		companyServices.AddDivisionProductMapping($scope.mapProdDiv)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	// show success notification
		    	showNotification({message:'Product Mapped with Division successfully', duration: 2000, class:null, 
		    		routine:'$scope.FetchProductsByCompanyDivisions(); initVariables();'});
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	/* Fetch Products by Company Divisions */
	$scope.FetchProductsByCompanyDivisions = function () {
		companyServices.GetDivisionProducts($scope.mapProdDiv.CompanyID, $scope.mapProdDiv.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.products.data(response.data);
			} else {
				$scope.products.data = false;
			}
		},
		function (rejection) {});
	};
	
						
	$scope.gridColumns = [
		{ field: "PFCID", title:"PFC ID",headerTemplate: "PFC ID <span class='k-icon k-i-sort-desc'></span>", width:"100px"},
		{ field: "ProductName", title:"Product Name",headerTemplate: "Product Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "Manuf_Name", title:"Manufacturer",headerTemplate: "Manufacturer <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "IsOnFocus", title:"Is Focused",headerTemplate: "Is Focused <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='focus-#=PFCID#' ng-checked='(#=IsOnFocus# == 1)' ng-true-value='1' k-ng-model='#:IsOnFocus#'><label for='focus-#=PFCID#'></label></div>", width:"80px" },
		{ field: "IsNewIntroduction", title:"Is New Intro",headerTemplate: "Is New Intro <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='intro-#=PFCID#' ng-checked='(#=IsNewIntroduction# == 1)' ng-true-value='1' k-ng-model='#:IsNewIntroduction#'><label for='intro-#=PFCID#'></label></div>", width:"80px" }
	];
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	// Kendo Observable Array
	$scope.products = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "ProductName",
            dir: "asc"
        }
	});
	/*------------ INDIVIDUAL MAPPING ends ------------*/
	
	/*----- Show Notification -----*/
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
 }]);
