angular.module("imsApp")
.controller("CompetitiveAnalysisController", ['$scope', '$state', 'userServices', 'companyServices', 'notify', 'savedSearhcesServices', '$window', '$rootScope', '$timeout','$uibModal',
											function($scope, $state, userServices, companyServices, notify, savedSearhcesServices, $window, $rootScope, $timeout, $uibModal){
	console.log('Competitive Analysis controller...', $state);

	/* Make $rootScope available in Local $scope */
	$scope.rootScope = $rootScope;

	/* Parse session data to object format */
	function sessionInfoToScope(){
		$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	}
	sessionInfoToScope();

	/* Static Data & Values */
	// Comparison Type
	$scope.comparisonType = [
        { value: 'company', title: "Company vs Company" },
        { value: 'brand', title: "Brand vs Brand" }
    ];

    $scope.TherapyClassColl = [
		{ type: "Therapy Class 1", value: 'tc1' },
		{ type: "Therapy Class 2", value: 'tc2' },
		{ type: "Therapy Class 3", value: 'tc3' },
		{ type: "Therapy Class 4", value: 'tc4' },
		{ type: "Super Group", value: 'sup' }
	];

	/* Initialize Models & Variables */
	initVariables();
	function initVariables() {
		showLoader = "angular.element('.loader-backdrop').fadeIn();";
	  	hideLoader = "angular.element('.loader-backdrop').fadeOut();";

		$scope.fetchData = {};
		$scope.fetchData.CompanyID = $scope.sessionInfo.CompanyID;
		$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;		// Division to which user's role is associated
		$scope.fetchData.GeographyFilter = 'city';
		$scope.fetchData.ComparisonType = 'company';
		$scope.fetchData.DataType ="npm";
		$scope.comparisonData = $scope.fetchData.ComparisonType;
		$scope.fetchData.IsViewTen = true;
		$scope.userId = $scope.sessionInfo.UserID;
		$scope.tabType = 3;
		$scope.isCluster = false;
		$scope.selected = {};
		$scope.selected.Therapies = [];
		$scope.fetchData.TCCode = null;
		$scope.selected.selectedTypes = [];
		$scope.fetchData.includeTopSearches = 1;
		$scope.filteredCategoryValues = [];
		$scope.ComparisonTypeLabel = "Company";
		$scope.isPreset = false;
		$scope.RegExNumForm = /(\d)(?=(\d{2})+\d\.)/g;
		// Unit measures dropdown
		$scope.unitOfMeasuresColls = [
										{key:'value', label:'Values', divisor:1, unit:'', decimal:0},
										{key:'value', label:'Values(k)', divisor:1000, unit:'k', decimal:0},
										{key:'value', label:'Values(Cr)', divisor:10000000, unit:'Cr', decimal:2},
										{key:'unit', label:'Units', divisor:1, unit:'', decimal:0},
										{key:'unit', label:'Units(k)', divisor:1000, unit:'k', decimal:0},
										{key:'unit', label:'Units(Cr)', divisor:10000000, unit:'Cr', decimal:2}
									];
		$scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
        $scope.UOM_set_flag = false;
		// Toggle Chart Radios
		$scope.chartRadio = 'NoPatch';
		$scope.selectedPatchId = undefined;
		$scope.propertyName = 'Rank';
    $scope.reverse = true;
	}

	$scope.textField ="export-"


	$scope.textField ="export-"
	$scope.sortBy = function(propertyName) {
		$scope.asc = ($scope.propertyName === propertyName) ? !$scope.asc : false;
		 $scope.propertyName = propertyName;
	 };

	/* Edit Session info */
	function updateSessionInfo(GeographyID, PeriodTypeId){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		if(GeographyID !== undefined){ sessionInfo.GeographyID = GeographyID; }
		if(PeriodTypeId !== undefined){ sessionInfo.PeriodTypeID = PeriodTypeId; }
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
	}

	/* Divisions
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.fetchData.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.fetchData.DivisonID = $scope.divisionColl[0].DivisionID;
				$scope.FetchGeographyValues();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();*/

	/* Geography */
	$scope.FetchGeographyValues = function (presetData) {
		userServices.GetGeographyValues($scope.fetchData.GeographyFilter, $scope.fetchData.CompanyID, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.geographyColl = response.data;
				if($scope.sessionInfo.GeographyID === undefined){
					$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
					updateSessionInfo($scope.geographyColl[0].GeographyID, undefined);
				}
				else{
					$scope.fetchData.GeographyID = $scope.sessionInfo.GeographyID;
				}
				FetchPeriodTypes(presetData);
			} else {
				$scope.geographyColl = [];
			}
		},
		function (rejection) {});
	};


	/* begin invoking service calls */
	initServiceCalls();
	function initServiceCalls(){
		$scope.FetchGeographyValues();
	}


	/* Period Type */
	function FetchPeriodTypes(presetData) {
		userServices.GetSubscriptionPeriodType($scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.periodTypeColl = response.data;
				if($scope.sessionInfo.PeriodTypeID === undefined){
					$scope.PeriodTypeID = $scope.periodTypeColl.filter(function(item){ return (item.isDefault); })[0].SubscriptionPeriodTypeID;
		            updateSessionInfo(undefined, $scope.PeriodTypeID);
		        }
		        else{
		            $scope.PeriodTypeID = $scope.sessionInfo.PeriodTypeID;
		        }
				$scope.FetchTimePeriods({presetData:presetData, callNextFunciton:true});
			} else {
				$scope.periodTypeColl = [];
			}
		},
		function (rejection) {});
	}


	/* Time Periods */
	$scope.FetchTimePeriods = function (inputObj) {		// Accepts object of form {presetData:String, callNextFunciton:Boolean}
		$scope.period = {};
		userServices.GetTimePeriods($scope.fetchData.GeographyID, $scope.PeriodTypeID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.timePeriods = response.data;
				if((inputObj === undefined) || (inputObj.presetData === undefined)){
					$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
					$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
				}
				// Queue in next function based on flow type
				if((inputObj !== undefined) && inputObj.callNextFunciton){
					$scope.FetchBricksByGeography(inputObj.presetData);
				}
			} else {
				$scope.timePeriods = [];
			}
		},
		function (rejection) {});
	};


	/* Period Type changed */
	$scope.periodTypeChanged = function(){
		updateSessionInfo(undefined, $scope.PeriodTypeID);
		$scope.FetchTimePeriods();
	};


	/* Validating the time selected */
	$scope.timeSelected = function(event){
		if($scope.period.startPeriod > $scope.period.endPeriod){
			$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
			$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		}
	};


	/* Map Geography Id to Geography Name */
	function mapGeographyIdToName(){
		$scope.GeographyName = $scope.geographyColl.filter(function(item){ return (item.GeographyID === $scope.fetchData.GeographyID); })[0].GeographyValue;
	}


	/* Geography selected from dropdown by user */
	$scope.geographySelected = function(){
		updateSessionInfo($scope.fetchData.GeographyID, undefined);
        $scope.FetchTimePeriods();
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				$scope.selectedPatches = [];
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
		},
		function (rejection) {});
	};


	/* Bricks */
	$scope.FetchBricksByGeography = function (presetData) {
		if(presetData !== undefined){
			$scope.fetchData.GeographyID = JSON.parse(presetData).GeographyID;
		}
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
			// Fetch values for first comparison type
			if(presetData !== undefined){
				if(Boolean(JSON.parse(presetData).TCLevel)){
					$scope.fetchTherapyListByTherapyCode('$scope.displayResult('+presetData+')', JSON.parse(presetData).TCLevel, JSON.parse(presetData).TCCode);
				}
				else{
					$scope.isPreset = true;
					$scope.fetchComparisonTypeValue('$scope.displayResult('+presetData+')');
				}
			}
			else{
				$scope.isPreset = false;
				$scope.fetchComparisonTypeValue('$scope.displayResult()');
			}
		},
		function (rejection) {});
	};


	// Comparison Type changed
	$scope.comparisonTypeChanged = function(){
		$scope.isPreset = false;
		if($scope.fetchData.ComparisonType === 'company'){
			$scope.fetchData.TCLevel = undefined;
			$scope.fetchData.TCCode = undefined;
			$scope.fetchComparisonTypeValue();
		}
		else{
			$scope.fetchData.TCLevel = 'tc1';

			$scope.fetchTherapyListByTherapyCode('$scope.fetchComparisonTypeValue()', undefined);
		}
	};


	// Fetch Therapy List for selected therapy class
	$scope.fetchTherapyListByTherapyCode = function(customRoutine, TCLevel, TCCode){
		$scope.fetchData.TCLevel = (TCLevel !== undefined) ? TCLevel : $scope.fetchData.TCLevel;

		companyServices.GetTherapyListByTherapyCode($scope.fetchData.TCLevel, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.therapyList = response.data;
				$scope.therapyList.unshift({'TherapyCode':'all', 'TherapyName':'All'});
				if(TCCode === undefined){
					$scope.fetchData.TCCode = $scope.therapyList[0].TherapyCode;
					$scope.selected.Therapies = [];
				}
				else{
					$scope.fetchData.TCCode = TCCode;
					$scope.selected.Therapies = ($scope.fetchData.TCCode !== 'all') ? $scope.fetchData.TCCode.split(',') : [];
				}

				if(TCCode !== undefined){
					$scope.isPreset = true;
					$scope.fetchComparisonTypeValue(customRoutine);
				}
				else if(customRoutine !== undefined){
					$scope.isPreset = false;
					eval(customRoutine);
				}
				else{
					$scope.isPreset = false;
					$scope.fetchComparisonTypeValue();
				}
			} else {
				$scope.therapyList = false;
			}
		},
		function (rejection) {});
	};


	/* Therapies selected from the list */
	$scope.therapiesSelected = function(isOpen){
		if(!isOpen){	// denoting that therapies have been selected by user
			$scope.isPreset = false;
			$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
			$scope.fetchComparisonTypeValue();
		}
	};

	/* Therapy removed from the list */
	$scope.therapiesRemoved = function($item, $model){
		$scope.isPreset = false;
		$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
		$scope.fetchComparisonTypeValue();
	};


	/* Map Therapy Id to Therapy Name */
	function mapTherapyIdToName(){
		$scope.TherapyName = $scope.therapyList.filter(function(item){ return (item.TherapyCode === $scope.fetchData.TCCode); })[0].TherapyName;
	}


	// Fetch Company or Brand List
	$scope.fetchComparisonTypeValue = function(customRoutine){

		var TherapyClass = ($scope.fetchData.TCLevel === undefined) ? null : $scope.fetchData.TCLevel;
		var selectedTherapy = ($scope.fetchData.TCCode === undefined) ? null : $scope.fetchData.TCCode;
		$scope.selected.selectedTypes = [];
		$scope.filteredCategoryValues = [];
		companyServices.GetComparisionTypeValue($scope.fetchData.ComparisonType, $scope.sessionInfo.SessionID, TherapyClass, selectedTherapy)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.comparisonColl = response.data;
				if($scope.isPreset){
					$scope.filteredCategoryValues = $scope.comparisonColl;
				}
				if(customRoutine !== undefined){
					eval(customRoutine);
				}
			} else {
				$scope.comparisonColl = false;
			}
		},
		function (rejection) {});
	};


	/* Filter Category values array */
	$scope.filterCategoryValues = function(searchQuery) {
		if(searchQuery !== ''){
			var regExpression = new RegExp(searchQuery, 'i');
			$scope.filteredCategoryValues = $scope.comparisonColl.filter(function(item){ return regExpression.test(item.ComparisonValue); });
		}
	};


	// Display Result
	$scope.displayResult = function (presetData) {
		if(presetData !== undefined){
			applyPresetData(presetData);
		}
		else{

			// Extract PeriodShortName based on its ID
			$scope.fetchData.PeriodType = $scope.periodTypeColl.filter(function(item){ return (item.SubscriptionPeriodTypeID === $scope.PeriodTypeID); })[0].PeriodShortName;
			$scope.fetchData.PeriodStart = $scope.period.startPeriod;
			$scope.fetchData.PeriodEnd = $scope.period.endPeriod;

			// Companies or Brands
			if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
				$scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
			}
			else{
				$scope.fetchData.ComparisionValue = 'all';
				$scope.fetchData.includeTopSearches = 1;
			}

			// Patches
			if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
				$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
			}
			else{
				$scope.fetchData.PatchIds = 'all';
			}

			$scope.selectedPatchId = undefined;
			fetchPatchesByCluster();
		}

	};

	// Get Patches by Clusters
	function fetchPatchesByCluster(){
		if($scope.isCluster && ($scope.fetchData.PatchIds !== 'all')){
			userServices.GetPatchIDsByClusterIDs($scope.fetchData.PatchIds, showLoader, hideLoader)
			.then(function (response) {
				if (response.data != null && response.status === 200) {
					$scope.fetchData.PatchIds = response.data;
					// Fetch Final Result
					$scope.FetchCompanyPerformance();
				} else {
					$scope.fetchData.PatchIds = '';
				}
			},
			function (rejection) {});
		}
		else{
			// Fetch Final Result
			$scope.FetchCompanyPerformance();
		}
	}


	/* Company Performance */
	$scope.FetchCompanyPerformance = function () {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
		companyServices.GetCompanyPerformance($scope.fetchData)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.CompAnalysisData = response.data;
				mapGeographyIdToName();
				$scope.comparisonData = $scope.fetchData.ComparisonType;
				// Discrete-Bar chart
                initDiscreteBarChart();

                resetUOM_MinValueLength($scope.CompAnalysisData.CompanyPerformance);
                $scope.unitOfMeasuresChanged();

                // Check for Comparison type
				if($scope.fetchData.ComparisonType === 'company'){
					$scope.ComparisonTypeLabel = 'Company';
					$scope.TherapyName = '';
				}
				else if($scope.fetchData.ComparisonType === 'brand'){
					$scope.ComparisonTypeLabel = 'Brand';
					mapTherapyIdToName();
				}
			} else {
				$scope.CompAnalysisData = false;
			}
		},
		function (rejection) {});
	};


	/* Select Preset functionality Begins */
	$scope.savePreset = function(){
		savedSearhcesServices.AddUpdateSavedSearches(preparePresetData())
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
				initializeOnPreset();
			    notify({
		            message: 'Presets saved Successfully',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			} else {}
		},
		function (rejection) {});
	};


	function preparePresetData () {
		var presetObject = {};
		presetObject.SearchKey = $scope.PresetName;
		presetObject.UserID = $scope.userId;
		presetObject.TabType = $scope.tabType;

		// Extract PeriodShortName based on its ID
		$scope.fetchData.PeriodType = $scope.periodTypeColl.filter(function(item){ return (item.SubscriptionPeriodTypeID === $scope.PeriodTypeID); })[0].PeriodShortName;
		$scope.fetchData.PeriodStart = $scope.period.startPeriod;
		$scope.fetchData.PeriodEnd = $scope.period.endPeriod;

		// Patches
		if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
			$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
		}
		else{
			$scope.fetchData.PatchIds = 'all';
		}

		// Companies or Brands
		if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
			$scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
		}
		else{
			$scope.fetchData.ComparisionValue = 'all';
			$scope.fetchData.includeTopSearches = 1;
		}

        // Remove SessionID before saving preset
        if($scope.fetchData.SessionID !== undefined){
            delete $scope.fetchData.SessionID;
        }
		presetObject.SearchData = JSON.stringify($scope.fetchData);
		return presetObject;
	}


	/* Get Presets */
	function LoadPresets(){
		savedSearhcesServices.GetSavedSearchesbyUser($scope.userId, $scope.tabType)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.presetsColl = response.data;
			} else {
				$scope.presetsColl = false;
			}
		},
		function (rejection) {});
	}
	LoadPresets();

	$scope.applySelectedPreset = function() {
		angular.element('.loader-backdrop').fadeIn();
		var presetData = $scope.presetsColl.filter(function(item){ return (item.SearchKeyID === $scope.selectedPreset); })[0].SearchData;
		var presetData_obj = JSON.parse(presetData);
		$scope.fetchData.GeographyFilter = presetData_obj.GeographyFilter;
		$scope.fetchData.DivisonID = presetData_obj.DivisonID;
		$scope.fetchData.ComparisonType = presetData_obj.ComparisonType;
		var local_PeriodID = $scope.periodTypeColl.filter(function(item){ return (item.PeriodShortName === presetData_obj.PeriodType); })[0].SubscriptionPeriodTypeID;
		updateSessionInfo(presetData_obj.GeographyID, local_PeriodID);
		$scope.FetchGeographyValues(presetData);
	};


	// Apply Preset data to fetchData
	function applyPresetData(presetData){
		$scope.fetchData = presetData;

		if($scope.fetchData.PeriodStart !== 0){
			$scope.period.startPeriod = $scope.fetchData.PeriodStart;
			$scope.period.endPeriod = $scope.fetchData.PeriodEnd;
		}

		if($scope.fetchData.PatchIds !== 'all'){
			$scope.selectedPatches = $scope.fetchData.PatchIds.split(',').map(Number);
		}
		else{
			$scope.selectedPatches = [];
		}

		if($scope.fetchData.ComparisionValue !== 'all'){
		     $scope.selected.selectedTypes = $scope.fetchData.ComparisionValue.split(',').map(Number);
		}
		else{
			$scope.selected.selectedTypes = [];
		}

		angular.element('.loader-backdrop').fadeOut();
	}


	// Initialize filter choices on preset
	function initializeOnPreset(){
		LoadPresets();
		$scope.PresetName = '';
		$scope.SavePresetFlag = false;
		$scope.fetchData.GeographyFilter = 'city';
		$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
		$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
		$scope.selectedPatches = [];
		$scope.PeriodTypeID = $scope.sessionInfo.PeriodTypeID;
		$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
		$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		$scope.fetchData.ComparisonType = 'company';
		$scope.selected.selectedTypes = [];
		$scope.fetchData.TCLevel = null;
		$scope.selected.Therapies = [];
		$scope.fetchData.TCCode = null;
		$scope.fetchData.IsViewTen = true;
		$scope.fetchData.includeTopSearches = 1;
		$scope.filteredCategoryValues = [];
		$scope.ComparisonTypeLabel = "Company";
		$scope.isPreset = false;
	}
	/* Select Preset functionality Ends */


	/*--------------------- Divide a number(n) by (d) and round upto (x) decimal places ---------------------*/
	$scope.DivideAndRound = function(n, d, x) {
		if(x === 0){
			n = (n/d).toFixed(1);
			n = (n.replace($scope.RegExNumForm, '$1,'));
			n = n.substring(0, (n.length-2));
		}
		else{
			n = (n/d).toFixed(x);
			n = (n.replace($scope.RegExNumForm, '$1,'));
		}
		return n;
	};


    /* Reset UOM based on Min value length */
    function resetUOM_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_set_flag)){
            var formattedArray = data.filter(function(item){ return (item.TotalSales >= 1); }).map(function(item){ return (item.TotalSales); });
            var minVal = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
            var minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[2];
            }
        }
    }


	/* Unit of Measures Changed in Market Share */
	$scope.unitOfMeasuresChanged = function(inputObj) {
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

		// Prepare a Figure Qualifier
	    $scope.figureQualifier = ($scope.unitOfMeasures.unit !== '') ? (" ("+$scope.unitOfMeasures.unit+")") : '';

		if($scope.unitOfMeasures.key === 'value'){
			$scope.companyPerformance = $scope.CompAnalysisData.CompanyPerformance;
			$scope.tableDataArr = $scope.CompAnalysisData.CompanyPerformanceSalesTable;
			$scope.tableDataArrColm = $scope.CompAnalysisData.CompanyPerformanceSalesTable[0].MonthData.map(function(item){
				return(item.MonthName);
			});
			$scope.SelectedPatchNameData =  $scope.CompAnalysisData.CompanyPerformanceSalesPatchTable;
			for(var i=0; i<$scope.SelectedPatchNameData;i++ ){
				$scope.SelectedPatchName =  $scope.SelectedPatchNameData[i].PatchName;
				console.log('patch name',$scope.SelectedPatchName);
			}


				$scope.repeatTableDataList = $scope.CompAnalysisData.CompanyPerformanceSalesPatchTable;
				$scope.TableDataListColm = $scope.CompAnalysisData.CompanyPerformanceSalesPatchTable[0].TableData[0].MonthData.map(function(item){
					return(item.MonthName);
				});




			// Descrete Bar Chart
            $scope.discrete_bar.options.chart.y = function(d) { return d.TotalSales; };
            $scope.discrete_bar.options.chart.yAxis.axisLabel = 'Sales Values ' + $scope.figureQualifier;
			$scope.discrete_bar.data[0].values = $scope.companyPerformance.filter(function(item){ return (item.CompanyID > 0); });
		    $scope.apiDiscreteBar.update();

		    // Pie-Chart
		    initPieChart($scope.CompAnalysisData.TotalSalesValue, 'TotalSales');

		    // Data Grid
			prepareDataGrid_V();

			// Multi-bar chart & Data Grid
			if($scope.selectedPatchId === undefined){
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonSales;
				$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
				// Product List Table

			}
			else{
				mutate_ComparisionByPatch_Value();
			}
		}
		else{
			$scope.companyPerformance = $scope.CompAnalysisData.CompanyPerformanceUnit;
			$scope.tableDataArr = $scope.CompAnalysisData.CompanyPerformanceUnitTable;
			$scope.tableDataArrColm = $scope.CompAnalysisData.CompanyPerformanceUnitTable[0].MonthData.map(function(item){
				return(item.MonthName);
			});
			$scope.repeatTableDataList = $scope.CompAnalysisData.CompanyPerformanceUnitPatchTable;
			$scope.TableDataListColm = $scope.CompAnalysisData.CompanyPerformanceUnitPatchTable[0].TableData[0].MonthData.map(function(item){
				return(item.MonthName);
			});




			// Descrete Bar Chart
            $scope.discrete_bar.options.chart.y = function(d) { return d.TotalUnits; };
            $scope.discrete_bar.options.chart.yAxis.axisLabel = 'Sales Units ' + $scope.figureQualifier;
			$scope.discrete_bar.data[0].values = $scope.companyPerformance.filter(function(item){ return (item.CompanyID > 0); });
		    $scope.apiDiscreteBar.update();

		    // Pie-Chart
		    initPieChart($scope.CompAnalysisData.TotalSalesUnit, 'TotalUnits');

		    // Data Grid
			prepareDataGrid_U();

			// Multi-bar chart
			if($scope.selectedPatchId === undefined){
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonUnit;
				$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
				// Product List Table
			}
			else{
				mutate_ComparisionByPatch_Unit();
			}
		}
	};



	// Prepare Grid Structure (Company comparison)
	function prepareDataGrid_V(){
		$scope.performanceColumns = [
			{ title: 'Rank', field: 'Rank', headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width: "50px" },
			{ title: 'Company Name', field: 'CompanyName',headerTemplate: $scope.ComparisonTypeLabel+" <span class='k-icon k-i-sort-desc'></span>", width:"180px;"},
			//{ title: 'Month Data', field: 'MonthData',headerTemplate: $scope.ComparisonTypeLabel+" <span class='k-icon k-i-sort-desc'></span>", width:"280px;"},
			{ title: 'Total Value (CR)', field: 'TotalSales', headerTemplate: "Total Value "+$scope.figureQualifier+"<span class='k-icon k-i-sort-desc'></span>",
				template: "#:kendo.toString(TotalSales/"+$scope.unitOfMeasures.divisor+", 'n"+$scope.unitOfMeasures.decimal+"')#", width: "130px" },
			{ title  : 'Market Share', field: 'MarketShare',headerTemplate: "Market Share <span class='k-icon k-i-sort-desc'></span>", template:"<span>#:kendo.toString(MarketShare, 'n')# %</span>", width: "130px"}
		];
	}

	function prepareDataGrid_U(){
		$scope.performanceColumns = [
			{ title: 'Rank', field: 'Rank', headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width: "50px" },
			{ title: 'Company Name', field: 'CompanyName',headerTemplate: $scope.ComparisonTypeLabel+" <span class='k-icon k-i-sort-desc'></span>", width:"180px;"},
			{ title: 'Total Units (CR)', field: 'TotalUnits', headerTemplate: "Total Value "+$scope.figureQualifier+"<span class='k-icon k-i-sort-desc'></span>",
				template: "#:kendo.toString(TotalUnits/"+$scope.unitOfMeasures.divisor+", 'n"+$scope.unitOfMeasures.decimal+"')#", width: "130px" },
			{ title  : 'Market Share', field: 'MarketUnitShare',headerTemplate: "Market Share <span class='k-icon k-i-sort-desc'></span>", template:"<span>#:kendo.toString(MarketUnitShare, 'n')# %</span>", width: "130px"}
		];
	}

	/* Patch Id selected */
	$scope.patchIdSelected = function(event){
		if(event.item === undefined){
			angular.element('.loader-backdrop').fadeIn();
			angular.element('.loader-backdrop').fadeOut();
			$scope.selectedPatchId  = undefined;


			if($scope.unitOfMeasures.key === 'value'){
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonSales;
				$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
			}
			else{
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonUnit;
				$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
			}
		}
		else{			
			angular.element('.loader-backdrop').fadeIn();
			angular.element('.loader-backdrop').fadeOut();
			$scope.selectedPatchId = event.item.PatchID;
			$scope.tableDataList = $scope.CompAnalysisData.CompanyPerformanceSalesPatchTable
									.filter(function(item){
										return (item.PatchID === $scope.selectedPatchId);
									});

			for(var i=0;i<$scope.tableDataList.length;i++){
				$scope.tableDataList2 = $scope.tableDataList[i].TableData;
			}

			for(var i=0; i<$scope.tableDataList.length;i++ ){
				$scope.PatchData =  $scope.tableDataList[i];
			}

			if($scope.unitOfMeasures.key === 'value'){
				mutate_ComparisionByPatch_Value();
			}
			else{
				mutate_ComparisionByPatch_Unit();
			}
		}
	};

	/* Mutate "GrouChartSalesPatch" array for Sales data */
	function mutate_ComparisionByPatch_Value(){
		var dataArrData = $scope.CompAnalysisData.CompanyComparisonByPatchSales.filter(function(item){
								return (item.PatchID === $scope.selectedPatchId);
							});

		$scope.multibar.data = [];
		if(dataArrData.length>0)
		{
			var dataArr = dataArrData[0].PatchDataDetails;
			for(var i = 0; i < dataArr.length; i++){
				$scope.multibar.data.push({"key":dataArr[i].key, "ColorCode":dataArr[i].ColorCode, "values":[]});
				$scope.multibar.data[i].values = dataArr[i].values;
			}
		}
		$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		$scope.tableDataList = $scope.CompAnalysisData.CompanyPerformanceSalesPatchTable
								.filter(function(item){
									return (item.PatchID === $scope.selectedPatchId);
								});

		for(var i=0;i<$scope.tableDataList.length;i++){
			$scope.tableDataList2 = $scope.tableDataList[i].TableData;
		}
		$scope.apiMultiBar.update();

	}

	/* Mutate "GrouChartSalesPatch" array for Units data */
	function mutate_ComparisionByPatch_Unit(){
		var dataArrData = $scope.CompAnalysisData.CompanyComparisonByPatchUnit.filter(function(item){
								return (item.PatchID === $scope.selectedPatchId);
							});
		$scope.multibar.data = [];
		if(dataArrData.length>0)
		{
			var dataArr = dataArrData[0].PatchDataDetails;
			for(var i = 0; i < dataArr.length; i++){
				$scope.multibar.data.push({"key":dataArr[i].key, "ColorCode":dataArr[i].ColorCode, "values":[]});
				$scope.multibar.data[i].values = dataArr[i].values;
			}
		}
		$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		$scope.tableDataList = $scope.CompAnalysisData.CompanyPerformanceUnitPatchTable
								.filter(function(item){
									return (item.PatchID === $scope.selectedPatchId);
								});

		for(var i=0;i<$scope.tableDataList.length;i++){
			$scope.tableDataList2 = $scope.tableDataList[i].TableData;
		}
		$scope.apiMultiBar.update();
	}


	/* Charts & Graphs initializations */
	$scope.competCombo1 = [
	                        { field: "All Head India Field" },
	                        { field: "All Head India Field" },
													{ field: "All Head India Field" },
													{ field: "All Head India Field" }
                        ];

	$scope.competCombo2 = [
                            { period: "Mat" },
                            { period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" }
                        ];


	$scope.myTeam = [
						{name:"Pradeep Desai", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer1", path:"content/img/avatars.png"},
						{name:"Ashish Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer2", path:"content/img/avatars2.png"},
						{name:"Jayshankar Krish", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer3", path:"content/img/avatars3.png"},
						{name:"Shivraj Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer4", path:"content/img/avatars.png"}
				  	];

	$scope.gridColumns = [
							 { field: "name", title:"Name", headerTemplate: "Name <span class='k-icon k-i-sort-desc'></span>", template: "<div class='customer-photo-competitive'" + "style='background-image: url(#:path#);'></div>"
							 	+"<a class='competitve-user-list' href='javascript:void(0)' ui-sref='user.competitiveteam'><div class='customer-name-competitive'>#:name#</div></a>", width:"310px;"},
						     { field: "designation", title:"Team Designation", headerTemplate: "Team Designation <span class='k-icon k-i-sort-desc'></span>",width:"147px" },
						     { field: "target", title:"Sales Target",headerTemplate: "Sales Target <span class='k-icon k-i-sort-desc'></span>"},
							 { field: "acheived", title:"Sales Acheived",headerTemplate: "Sales Acheived <span class='k-icon k-i-sort-desc'></span>", template:"<div class='qwer'>#:acheived#</div>" + "<i class='fa fa-caret-up' aria-hidden='true'></i>"},
							 { field: "chart", title:"Chart",headerTemplate: "Chart <span class='k-icon k-i-sort-desc'></span>", template:"<div class='#:piechart#'" + "style='max-width: 100px; height:100px;margin: 0 auto;'></div>"}
						];


    /* ============================================ NVD3 - Discrete Bar Chart ============================================ */

	function initDiscreteBarChart() {
		$scope.discrete_bar = {};

		$scope.discrete_bar.data = [
            { values: [] }
	    ];

	    $scope.discrete_bar.options = {
	        chart: {
	            type: 'discreteBarChart',
	            height: 450,
	            margin: {
	                top: 40,
	                right: 20,
	                bottom: 50,
	                left: 80
	            },
	            x: function (d) { return d.CompanyName; },
	            y: function (d) { return d.TotalSales; },
                showLegend: false,
	            showValues: true,
	            valueFormat: function (d) {
	                return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	            },
	            duration: 500,
	            xAxis: {
	                axisLabel: 'Company Name'
	            },
	            showXAxis: false,
	            yAxis: {
	                axisLabel: 'Sales Values',
	                axisLabelDistance: 20,
	                tickFormat: function(d){
						return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
					}
	            },
	            noData: 'No Data to display',
                color: function (d, i) {
                    return d.ColorCode;
                },
	            tooltip: {
                	valueFormatter: function (d, i) {
						return ($scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || ''));
					}
                }
	        }
	    };
	}


	/* ============================================ NVD3 - Pie Chart ============================================ */
	function initPieChart(totalValue, y_variable) {
	    $scope.pieChart1Options = {
	        chart: {
	            type: 'pieChart',
	            height: 400,
	            x: function (d) { return d.CompanyName; },
	            y: function (d) { return d[y_variable]; },
	            showLabels: true,
	            labelType: 'percent',
	            duration: 500,
	            labelThreshold: 0.01,
	            labelSunbeamLayout: true,
	            showLegend: true,
	            donut:true,
                donutRatio: 0.35,
	            title:$scope.DivideAndRound(totalValue, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || ''),
                color: function(d,i){
                    return d.ColorCode;
                },
                tooltip: {
                	valueFormatter: function (d, i) {
						return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
					}
                }
	        }
	    };
	}


	/* ============================================ NVD3 - Multibar Chart ============================================ */
	initMultiBarChart();
	function initMultiBarChart(){
		$scope.multibar = {};
		$scope.multibar.data = [];
		$scope.multibar.options = {
            chart: {
                type: 'multiBarChart',
                height: 480,
                margin: {
                    top: 40,
                    right: 25,
                    bottom: 140,
                    left: 75
                },
                clipEdge: true,
                //staggerLabels: true,
                showLegend: true,
                legend: {
                	maxKeyLength: 11
                },
                duration: 500,
                stacked: true,
                xAxis: {
                    axisLabel: 'Patches',
                    showMaxMin: false,
                    tickFormat: function(d){
                        return (d.length > 15) ? (d.substr(0,14) + '...') : d;
                    },
                    staggerLabels: true,
                    axisLabelDistance: 15
                },
                yAxis: {
                    axisLabel: 'Sales (k)',
                    axisLabelDistance: 12,
                    tickFormat: function(d){
                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
                    }
                },
                stacked: false,
                noData: 'No Data to display',
                color: function (d, i) {
                    return d.ColorCode;
                },
                tooltip: {
                	valueFormatter: function (d, i) {
						return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
					}
                }
            }
        };

        $scope.multibar.data = [];
   }


	/* Refresh charts on toggle */
	$scope.chartRadioChanged = function(){
		if($scope.chartRadio === 'NoPatch'){
			$timeout(function(){
				$scope.apiDiscreteBar.refresh();
				$scope.apiPie1.refresh();
			}, 100);
		}
		else if($scope.chartRadio === 'ByPatch'){
			$timeout(function(){
				$scope.apiMultiBar.refresh();
			}, 100);
		}
	};
	
	/* Npm Cvm on toggle */
	$scope.cvmRadioChanged = function(){
		
		$scope.FetchCompanyPerformance();
	
	};
    /* Assign function for refreshing all charts and graphs to rootScope */
    $rootScope.refreshCharts_CompAn = function(){
        $timeout(function(){
            $scope.apiDiscreteBar.refresh();
            $scope.apiPie1.refresh();
			$scope.multibar.refresh();
        }, 500);
    };


	/* Document Ready starts */
	angular.element(document).ready(function(){
		/* Angular Provision for Tab Panel */
		$scope.showTab = function(id1, id2){
			angular.element(id1).fadeOut(500, function(){
				angular.element(id2).fadeIn(function(){
					if(id1 === '#tabs-4-tab-2'){
						// $scope.apiPie2.refresh();
					}
				});
			});
		}

		// Default Prevention function
		$scope.defaultPrevention = function(event){
			event.preventDefault();
		}

		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);




        // angular.element("#tabTable li").on('click',function(){
            // angular.element(this).closest('ul').find('li').removeClass('selected');
            // angular.element(this).addClass('selected');
        // });

	});
	/* Document Ready ends */


	/*$scope.window_onload = function() {
		//load a svg snippet in the canvas with id = 'drawingArea'
		var _svg = document.getElementById('svgChart');
		var _canvas = document.getElementById('canvas');
		canvg(_canvas, _svg.innerHTML);
		var imgData = canvas.toDataURL('image/png');
		var doc = new jsPDF('p', 'mm');
        doc.addImage(imgData, 'PNG', 10, 10);
        doc.save('sample-file-1.pdf');
	};*/


	$scope.itemTemplate = $("#template").html();
	$scope.valueTemplate = "<b>{{ dataItem.SearchKey }}</b>";


	/* Delete Confimation modal starts */
		$scope.deleteConfirmationModal = function(recordId){
				var self = $scope;
				var confirmDeleteModal = $uibModal.open({
						templateUrl:'deleteConfimationModal.html',
						size: 'sm',
						windowTopClass: 'modal-top-class',
						controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
							$scope.message = "Do you want delete the Selected Preset";
								$scope.dismissModal = function(){
										$uibModalInstance.dismiss('dismiss');
								};
								$scope.confirmDelete = function(e){
									self.deleteSelected($uibModalInstance, recordId);
								};
						}]
				});
		};
		/* Delete Confimation modal ends */

	$scope.deleteSelected = function($uibModalInstance, id){
		savedSearhcesServices.DeleteSavedSearches(id)
		.then(function (response) {
				if (response.data != null && response.status === 200) {
				if(response.data){
					 $scope.msg ="delete";
					 $uibModalInstance.close('close');
					 showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'LoadPresets()'});
				 }
			 else{
				 showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			 }

		 } else {}
	 },
	 function (rejection) { });
	}


	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}

    // Input radio-group visual controls
    $('.radio-group label').on('click', function(){
        $(this).removeClass('not-active').siblings().addClass('not-active');
		});

	$scope.sortType= 'Rank'; // set the default sort type
 $scope.sortReverse  = false;  // set the default sort order

}]);
