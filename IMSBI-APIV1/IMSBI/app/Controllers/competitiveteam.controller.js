angular.module("imsApp")
.controller("CompetitiveTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Competitive Team controller...', $state);
	
	angular.element(document).ready(function(){
		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);
			      
        angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
        });
        
        initHighChart();
        
        initPieChart();
        initPieCharts_Grid();
	});
	
	/* Angular Provision for Tab Panel */
	$scope.showTab = function(id1, id2){
		angular.element(id1).fadeOut(500, function(){
			angular.element(id2).fadeIn();
		});
	}
	
	$scope.competCombo1 = [
                            { feildHead: "Jayshankar Krish" },
                            { feildHead: "Sachin Mule" },
							{ feildHead: "Pradeep Desai" },
							{ feildHead: "Ashish Patel" }
                        ];
                        
	$scope.competCombo2 = [
                            { zonal: "ALl Managers Sales Performance" },
                            { zonal: "Top 20" },
							{ zonal: "Top 20" },
							{ zonal: "Top 20" }
                        ];
                        
	$scope.competCombo3 = [
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" }
	];
	
	$scope.competCombo4 = [
							{ period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" }
	];
	
   	$scope.dataSource = [
							{ city: "New Delhi" },
                            { city: "Maharashtra" },
							{ city: "Chennai" },
							{ city: "Kolkata" }
                        ];
                        
	$scope.dataSource2 = [
                            { comparision: "Company to Company" },
                            { comparision: "Company to Company" },
							{ comparision: "Company to Company" },
							{ comparision: "Company to Company" }
                        ];
                        
	$scope.dataSource4 = [
                            { period: "New Delhi" },
                            { period: "Maharashtra" },
							{ period: "Chennai" },
							{ period: "Kolkata" }
                        ];
                        
	$scope.teamPerformance = [
							{name:"Pradeep Desai", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer1", path:"content/img/avatars.png"},
							{name:"Ashish Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer2", path:"content/img/avatars2.png"},
							{name:"Jayshankar Krish", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer3", path:"content/img/avatars3.png"},
							{name:"Shivraj Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer4" , path:"content/img/avatars.png"}
				  ];
				  
	$scope.comparision = [
					{ rank: "2", company:"Torrent Pharma", sales:"Rs. 8,25,26,000", market:"18%" },
					{ rank: "4", company:"Biocon", sales:"Rs. 8,25,26,000", market:"4%" },
					{ rank: "5", company:"Avantis Pharma", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "8", company:"Surya Pharma", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "12", company:"GlaxoSmithKline", sales:"Rs. 8,25,26,000", market:"18%" },
					{ rank: "13", company:"Glenmark", sales:"Rs. 8,25,26,000", market:"32%" },
					{ rank: "16", company:"Divis Labs", sales:"Rs. 8,25,26,000", market:"9%" },
					{ rank: "24", company:"Orchid Chemical", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "36", company:"Abbott India", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "75", company:"Sterling Bio", sales:"Rs. 8,25,26,000", market:"24%" }
				  ];

	$scope.gridColumns = [
				 { field: "name", title:"Name",headerTemplate: "Name<span class='k-icon k-i-sort-desc'></span>",template: "<div class='customer-photo-competitive'" + "style='background-image: url(#:path#);'></div>" + "<div class='customer-name-competitive'>#:name#</div>", width:'300px'},
			     { field: "designation", title:"Team Designation",headerTemplate: "Team Designation<span class='k-icon k-i-sort-desc'></span>", width:"170px" },
			     { field: "target", title:"Sales Target", headerTemplate: "Sales Target<span class='k-icon k-i-sort-desc'></span>" },
				 { field: "acheived", title:"Sales Acheived",headerTemplate: "Sales Acheived <span class='k-icon k-i-sort-desc'></span>" , template:"<div class='qwer'>#:acheived#</div>" + "<i class='fa fa-caret-up' aria-hidden='true'></i>", width:'' },
				 { field: "chart", title:"Chart",headerTemplate: "Chart <span class='k-icon k-i-sort-desc'></span>",  template:"<div class='#:piechart#'" + "style='max-width: 100px; height:100px;margin: 0 auto;'></div>"}
				];
					
	 $scope.gridColumns2 = [
		{ title: 'Rank', field: 'rank',headerTemplate: "Rank<span class='k-icon k-i-sort-desc'></span>", width:"80px"},
		{ title: 'Company', field: 'company',headerTemplate: "Company<span class='k-icon k-i-sort-desc'></span>", width:"180px" },
		{ title: 'Sales Volume', field: 'sales',headerTemplate: "Sales Volume<span class='k-icon k-i-sort-desc'></span>", width:"180px" },
		{ title  : 'Market Share', field: 'market',headerTemplate: "Market Share<span class='k-icon k-i-sort-desc'></span>" }
	];

	$scope.competitiveteamCompanies=[
		{comp:"Aventis Pharma"},
		{comp:"GlaxoSmithKline"},
		{comp:"Surya Pharma"},
		{comp:"Torrent Pharma"},
		{comp:"Divis Labs"},
		{comp:"Biocon"},
		{comp:"Orchid Chemical"},
		{comp:"Abbott India"},
		{comp:"Sterling Bio"}
	];
	
	
	// Initialization for Highcharts
	function initHighChart(){
		angular.element('#container').highcharts({
	        chart: {
	            type: 'pie'
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	         exporting: {
	         enabled: false
	        },
	        credits: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: false,
	                    format: '{point.name}: {point.y:.1f}%'
	                }
	            }
	        },
	
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'Aventis Pharma',
	                y: 56.33,
	                drilldown: 'Aventis Pharma '
	            }, {
	                name: 'GlaxoSmithKline',
	                y: 24.03,
	                drilldown: 'GlaxoSmithKline'
	            }, {
	                name: 'Surya Pharma',
	                y: 10.38,
	                drilldown: 'Surya Pharma'
	            }, {
	                name: 'Torrent Pharma',
	                y: 4.77,
	                drilldown: 'Torrent Pharma'
	            }, {
	                name: 'Glenmark',
	                y: 0.91,
	                drilldown: 'Glenmark'
	            }, {
	                name: 'Divis Labs',
	                y: 0.2,
	                drilldown: 'Divis Labs'
	            }]
	        }],
	        drilldown: {
	            series: [{
	                name: 'Microsoft Internet Explorer',
	                id: 'Microsoft Internet Explorer',
	                data: [
	                    ['v11.0', 24.13],
	                    ['v8.0', 17.2],
	                    ['v9.0', 8.11],
	                    ['v10.0', 5.33],
	                    ['v6.0', 1.06],
	                    ['v7.0', 0.5]
	                ]
	            }, {
	                name: 'Chrome',
	                id: 'Chrome',
	                data: [
	                    ['v40.0', 5],
	                    ['v41.0', 4.32],
	                    ['v42.0', 3.68],
	                    ['v39.0', 2.96],
	                    ['v36.0', 2.53],
	                    ['v43.0', 1.45],
	                    ['v31.0', 1.24],
	                    ['v35.0', 0.85],
	                    ['v38.0', 0.6],
	                    ['v32.0', 0.55],
	                    ['v37.0', 0.38],
	                    ['v33.0', 0.19],
	                    ['v34.0', 0.14],
	                    ['v30.0', 0.14]
	                ]
	            }, {
	                name: 'Firefox',
	                id: 'Firefox',
	                data: [
	                    ['v35', 2.76],
	                    ['v36', 2.32],
	                    ['v37', 2.31],
	                    ['v34', 1.27],
	                    ['v38', 1.02],
	                    ['v31', 0.33],
	                    ['v33', 0.22],
	                    ['v32', 0.15]
	                ]
	            }, {
	                name: 'Safari',
	                id: 'Safari',
	                data: [
	                    ['v8.0', 2.56],
	                    ['v7.1', 0.77],
	                    ['v5.1', 0.42],
	                    ['v5.0', 0.3],
	                    ['v6.1', 0.29],
	                    ['v7.0', 0.26],
	                    ['v6.2', 0.17]
	                ]
	            }, {
	                name: 'Opera',
	                id: 'Opera',
	                data: [
	                    ['v12.x', 0.34],
	                    ['v28', 0.24],
	                    ['v27', 0.17],
	                    ['v29', 0.16]
	                ]
	            }]
	        }
	    });
	}
	
	
	
	// Initialization for Pie-Chart
	function initPieChart(){
		$('.piechartcontainer').highcharts({
            chart: {
                backgroundColor: 'transparent',
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
             exporting: {
                enabled: false
               },
               credits: {
                   enabled: false
               },
            plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Sales Acheived',
                    y: 56.33
                }, {
                    name: 'Sales Target',
                    y: 24.03,
                    selected: true
                }]
            }]
        });
    }    
	
	
	function initPieCharts_Grid(){
		
        $('.piechartcontainer1').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });

        $('.piechartcontainer2').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
    	});

        $('.piechartcontainer3').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });

        $('.piechartcontainer4').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                enabled: false
               },
               credits: {
                   enabled: false
               },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });
	}
		
		
}]);
