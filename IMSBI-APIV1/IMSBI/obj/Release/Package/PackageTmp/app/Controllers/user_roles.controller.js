angular.module("imsApp")
.controller("UserRolesController", ['$scope', '$state', 'adminServices', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, adminServices, companyServices, $window, $uibModal, notify){

	console.log('User Roles controller...', $state);

	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));

	angular.element(document).ready(function(){});


	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
	  	showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
	  	hideLoader = "angular.element('.loader-backdrop').fadeOut();";
		$scope.CompanyID = $scope.sessionInfo.CompanyID;
  	}

  	/* Divisions */
	fetchCompanyDivisions();
	function fetchCompanyDivisions() {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.FetchUserRolesByCompany();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};


  	/* Fetch User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function () {
		adminServices.GetUserRolesByCompany($scope.CompanyID, $scope.DivisionID, showLoader, hideLoader)
		.then(function (response) {
			if ((response.data !== null) && (response.status === 200)) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};

	$scope.gridColumns = [
		{ field: "RoleName", title:"Role Name", headerTemplate: "Role Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "ParentRoleName", title:"Parent Role Name", headerTemplate: "Parent Role Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action", headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=CompanyUserRoleID#, \"#=RoleName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='admin.addrole({roleId:#:CompanyUserRoleID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"40px" }
	];

	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "RoleName",
            dir: "asc"
        }
	});

	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};

	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
							return regExpression.test(item.RoleName);
						});
			$scope.observableList.data(data);
		}
	}


	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */

	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		adminServices.DeleteCompanyUserRole(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchUserRolesByCompany();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }

		    } else {}
		},
		function (rejection) { });
	};


	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}

}]);
