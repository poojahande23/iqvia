angular.module("imsApp")
.controller("AddRoleController", ['$scope', '$state', '$stateParams', 'adminServices', 'companyServices', 'notify', 'userServices', '$window', 'notify',
							function($scope, $state, $stateParams, adminServices, companyServices, notify, userServices, $window, notify){

	console.log('Add Role controller...', $state);

	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));

	angular.element(document).ready(function(){});


	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		$scope.roleObj = {};
		$scope.roleObj.CompanyID = $scope.sessionInfo.CompanyID;
	}


	/* State Params */
	if($stateParams.roleId !== ''){
		$scope.isUpdate = true;
		FetchRoleByRoleId({'roleId':$stateParams.roleId, 'initEditFlow':true});
	}
	else{
		$scope.isUpdate = false;
		FetchCompanyDivisions();
	}


	/* Fetch Role by role id */
	function FetchRoleByRoleId(inputObj) {		// {'roleId':num, 'initEditFlow':boolean}
		adminServices.GetCompanyUserRoleByRoleID(inputObj.roleId)
		.then(function (response) {
			if ((response.data !== null) && (response.status === 200)) {
				$scope.roleObj = response.data;
				delete inputObj.roleId;
				FetchCompanyDivisions(inputObj);
			} else {
				$scope.roleObj = {};
			}
		},
		function (rejection) {});
	}


	/* Divisions */
	function FetchCompanyDivisions(inputObj) {		// {'initEditFlow':boolean}
		companyServices.GetCompanyDivisions($scope.roleObj.CompanyID)
		.then(function (response) {
			if ((response.data.length > 0) && (response.status === 200)) {
				$scope.divisionColl = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.roleObj.DivisionID !== null)){
					$scope.roleObj.DivisionID = $scope.roleObj.DivisionID;
				}
				else{
					$scope.roleObj.DivisionID = ($scope.divisionColl.length > 0) ? $scope.divisionColl[0].DivisionID : null;
				}
				$scope.FetchUserRolesByCompany(inputObj);
			} else {
				showNotification({message:"Please create Divisions first", duration: 3000, class:"notify-bg", routine:"$state.go('admin.divisions');"});
			}
		},
		function (rejection) {});
	};


	/* User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function(inputObj) {		// {'initEditFlow':boolean}
		adminServices.GetUserRolesByCompany($scope.roleObj.CompanyID, $scope.roleObj.DivisionID)
		.then(function (response) {
			if ((response.data !== null) && (response.status === 200)) {
				$scope.parentRolesColl = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.roleObj.ParentRoleID !== 0)){
					$scope.roleObj.ParentRoleID = $scope.roleObj.ParentRoleID;
				}
				else{
					$scope.roleObj.ParentRoleID = null;
				}
			} else {
				$scope.parentRolesColl = [];
			}
		},
		function (rejection) {});
	};


	/* Fetch Application Permission List */
	$scope.FetchApplicationPermissionList = function () {
		userServices.GetApplicationPermissionList()
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.appPermissionList = response.data;
			} else {
				$scope.appPermissionList = false;
			}
			setPermissionStatus($scope.appPermissionList);
		},
		function (rejection) {});
	};
	$scope.FetchApplicationPermissionList();


	function setPermissionStatus(list){
		if($scope.roleObj.permissions !== undefined){
			for(var i = 0; i < list.length; i++){
				list[i].Permission = $scope.roleObj.permissions.filter(function(item){ return (item.ApplicationID === list[i].PermissionID); })[0].Permission;
			}
		}
		else{
			for(var i = 0; i < list.length; i++){
				list[i].Permission = false;
			}
		}
	}


	// Grid Columns structure
	$scope.gridColumns = [
		{ title: 'Permission Name', field: 'PermissionName', template: "", width:"450px"},
		{ title: 'View', field: 'PermissionID', template: "<div class='checkbox'><input type='checkbox' class='statusCheckBox' ng-checked='#:Permission#' id='#:PermissionID#'><label for='#:PermissionID#'></label></div>", width:"100px"}
	];


	/* Save Role */
	$scope.saveRole = function(){
		$scope.roleObj.ParentRoleID = (($scope.roleObj.ParentRoleID === undefined) || ($scope.roleObj.ParentRoleID === null)) ? 0 : $scope.roleObj.ParentRoleID;
		$scope.roleObj.permissions = fetchCheckedIds();
		adminServices.AddUpdateCompanyUserRole($scope.roleObj)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
		        $state.go('admin.userroles');
			} else {
				notify({
		            message: 'Failed to save role',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			}
		},
		function (rejection) {});
	};


	// Fetch checked checkboxes ids
	function fetchCheckedIds(){
		var checkIds = [];
		angular.element('input[type="checkbox"].statusCheckBox').each(function(index){
			checkIds.push({
				'ApplicationID': Number(angular.element(this).attr('id')),
				'Permission': angular.element(this).prop('checked')
			});
		});
		return checkIds;
	}


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}

}]);
