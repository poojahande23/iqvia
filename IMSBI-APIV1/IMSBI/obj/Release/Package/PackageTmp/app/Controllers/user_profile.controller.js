angular.module("imsApp")
.controller("UserProfileController", ['$scope', '$rootScope', '$state', 'adminServices', 'companyServices', 'mediaServices', '$window', 'notify', '$uibModal',
							function($scope, $rootScope, $state, adminServices, companyServices, mediaServices, $window, notify, $uibModal){
	
	console.log('User Profile controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	/* Parse session data to object format */
	function sessionInfoToScope(){
		$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	}
	sessionInfoToScope();
	
	
	/* Update Session info */
	function updateSessionInfo(profileImage){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		sessionInfo.ProfileImage = profileImage;
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
		// Update Navbar's sessionInfo
		$rootScope.$emit('UpdateNavbarSessionInfo', {});
		$state.go($scope.sessionInfo.fromState);
	}
	
	
	// Model Initialization
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.user = {};
	$scope.user.Active = 1;
	FetchUserProfileInfo();
	
	$scope.imgBrowsed = false;
	$scope.avatarSelected = false;
	$scope.imgConfirmed = false;
	$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
	$scope.imgDataUrl = $scope.baseURL + 'avatars/avatar-2-256.png';
	$scope.user.ProfileImage = 'avatars/avatar-2-256.png';
	
	
	/* State Params functionality */
	function FetchUserProfileInfo(){
    	adminServices.GetUserInfo($scope.sessionInfo.UserID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.user = response.data;
				applyUserInfoResponse();
			} else {
				$scope.user = false;
			}
		},
		function (rejection) {});
   	}
   	
   	
   	/* Fetch User Info by ID */
	function FetchUserInfoByID(userId){
    	adminServices.GetUserInfo(userId, showLoader, hideLoader)
		.then(function (response) {
			var profileImage = '';
			if (response.data != null && response.status === 200) {
				profileImage = (response.data.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + response.data.ProfileImage);
				updateSessionInfo(profileImage);
			} else {
				
			}
		},
		function (rejection) {});
   	}
   	
   	
   	/* Apply fetched user info to page */
   	function applyUserInfoResponse() {
		FetchCompanyDivisions();
		$scope.imgDataUrl = ($scope.user.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + $scope.user.ProfileImage);
	}
   	
	
	/* Divisions */
	function FetchCompanyDivisions() {
		companyServices.GetCompanyDivisions($scope.user.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.FetchUserRolesByCompany();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function () {
		adminServices.GetUserRolesByCompany($scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.RolesColl = response.data;
				$scope.FetchReportingPerson();
			} else {
				$scope.RolesColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Fetch Company Reporting Manager based on Role Type */
	$scope.FetchReportingPerson = function(){
		companyServices.GetCompanyReportingManagerBasedOnRoleType($scope.user.CompanyRoleID, $scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.ReportingPersons = response.data;
			} else {
				$scope.ReportingPersons = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Image functionality */
    $scope.imageView = function(){
    	$scope.imgBrowsed = true;
		var reader = new FileReader();
		reader.onload = $scope.imageIsLoaded; 
		reader.readAsDataURL($scope.ProfileImage);
	};
 
 
	$scope.imageIsLoaded = function(e){
	    $scope.$apply(function() {
	    	$scope.imgUrl = e.target.result;
	    });
	};
	
	
	function fetchAvatars() {
		$scope.avatarsList = ['avtar1.png', 'avtar2.png', 'avtar3.png', 'avtar4.png', 'avtar5.png', 'avtar6.png', 'avtar7.png', 'avtar8.png',
								'avtar9.png', 'avtar10.png', 'avtar11.png', 'avtar12.png', 'avtar13.png', 'avtar14.png', 'avtar15.png', 'avtar16.png'];
	}
	fetchAvatars();
	
	
	$scope.browsedConfirmed = function() {
		$scope.imgConfirmed = true;
		$scope.imgDataUrl = $scope.imgUrl;
		$scope.user.ProfileImage = $scope.ProfileImage;
	};
	
	
	$scope.avatarIsSelected = function(avatar) {
		$scope.avatarSelected = true;
		$scope.imgUrl = $scope.baseURL + 'avatars/' + avatar;
		$scope.ProfileImage = 'avatars/' + avatar;
	};
	
	/* Image Change modal begins */
	$scope.openImgChangeModal = function(UserID){
        var parent = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'imgChangeModal.html',
            windowTopClass: 'imageChangeModal',
            controller:['$scope','$uibModalInstance', function($scope,$uibModalInstance){
            	$scope.parent = parent;
				parent.imgUrl = parent.imgDataUrl;
				parent.ProfileImage = parent.user.ProfileImage;
				parent.imgBrowsed = false;
                $scope.closeModal = function(){
					$uibModalInstance.close('close');
					parent.browsedConfirmed();
                };
                
                $scope.dismissModal = function(){
                	parent.imgBrowsed = false;
                	parent.avatarSelected = false;
                    $uibModalInstance.dismiss('dismiss');
                };
            }]
        });
    };
    /* Image Change modal ends */
	
	
	/* Upload file to Database and return path */
	$scope.uploadFile = function(routine) {
        mediaServices.PostAnImage($scope.user.ProfileImage)
		.then(function (response) {
		    if (response.data != null && response.status === 201) {
		        if(response.data.Message === 'Image Updated Successfully.'){
		        	$scope.user.ProfileImage = $scope.user.ProfileImage.name;
		        	if(routine !== undefined){
		        		eval(routine);
		        	}
		        }
		    } else {
		      	console.log('Failed to upload image: ',response);
		    }
		},
		function (rejection) { });
    };
    
	
	// Save User
	$scope.saveUser = function () {
	    adminServices.AddUpdateUser($scope.user)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        copyObject($scope.sessionInfo, response.data);
		        $window.sessionStorage.imsSessionInfo = JSON.stringify($scope.sessionInfo);
		        showNotification('Profile info updated successfully', '', 'FetchUserInfoByID('+response.data+')');
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	$scope.saveFormData = function() {
		if($scope.imgConfirmed && !($scope.avatarSelected)){
			$scope.uploadFile('$scope.saveUser();');
		}
		else{
			$scope.saveUser();
		}
	};
	
	
	// Copy the kay values of source object into destination object
    function copyObject(source, dest){
    	for(var prop in source){
			for(var key in dest){
				if(prop === key){
					dest[key] = source[prop];
				}
			}
		}
    }
    
    
    // Show Notification
	function showNotification(message, noteClass, routine){
		noteClass = noteClass || '';
		notify({
            message: message,
            classes: noteClass,
            templateUrl: $scope.template,
            position: 'center',
            duration: 2000
        });
        eval(routine);
	}

		
}]);
