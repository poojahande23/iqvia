angular.module("imsApp")
.controller("ClustersController", ['$scope', '$state', 'userServices', 'companyServices', '$uibModal', 'notify',
							function($scope, $state, userServices, companyServices, $uibModal, notify){
	
	console.log('Clusters controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
  	
  	
  	/* Initialize Models */
  	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
  	$scope.GeographyFilter = 'city';
  	
  	
  	/* Subscribed Companies List */
	function FetchsubscribedCompanyList() {
	    companyServices.GetsubscribedCompanies()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.customerList = response.data;
		        $scope.CompanyID = response.data[0].CompanyID;
		        $scope.FetchCities();
		    } else {
		        $scope.customerList = false;
		    }
		},
		function (rejection) { });
	}
	FetchsubscribedCompanyList();
	
  	
  	/* City */
	$scope.FetchCities = function() {
		userServices.GetGeographyValues($scope.GeographyFilter, $scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.cityColl = response.data;
				$scope.CityID = $scope.cityColl[0].GeographyID;
				$scope.FetchCompanyClusterListByCity();
			} else {
				$scope.cityColl = false;
			}
		},
		function (rejection) {});
	};
  	
  	/* Fetch Cluster List by City */
	$scope.FetchCompanyClusterListByCity = function () {
		companyServices.GetCompanyClusterList($scope.CompanyID, $scope.CityID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};
	
	$scope.gridColumns = [
		{ field: "ClusterName", title:"Cluster Name", headerTemplate: "Cluster Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=ClusterID#, \"#=ClusterName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='super.addcluster({clusterId:#:ClusterID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"30px" }
	];
	
	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "ClusterName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.ClusterName);
								});
			$scope.observableList.data(data);
		}
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteCompanyCluster(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchCompanyClusterListByCity();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
		
}]);
