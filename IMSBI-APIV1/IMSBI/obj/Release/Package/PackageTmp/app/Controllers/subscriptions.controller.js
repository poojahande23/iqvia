angular.module("imsApp")
.controller("SubscriptionsController", ['$scope', '$state', 'companyServices', 'userServices',
								function($scope, $state, companyServices, userServices){
	
	console.log('Subscriptions controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* initialize models and variables*/
	initParameters();
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	
	function initParameters() {
		fetchCompanySubscriptionTypes();
		fetchSubscriptionLocations();
	}
	
	
	/* Kendo Grid configuration for Subscription Types */
	$scope.gridColumns = [
		{field:"subs", title:"Subscription Type", template:"<span class='colorbox' style='background-color:#:ColorCode#'><span class='notSet' ng-if='(#:ColorCode === \"\"#)'>NotSet</span></span>"
		+ "<span class='plat-subs'>#:SubscriptionName#</span>", width:"300px"},
		{field:"action", title:"Action", headerTemplate: "<div class='text-center'>Action</div>", template:"<div class='icon-subs text-center'>"
		+ "<a href='javascript:void(0)' ui-sref='super.addnewsubscription({subscriptionId:#=SubscriptionTypeID#})' title='Edit'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"80px"}
	];
	
	
	$scope.gridOptions = {
		columns: $scope.gridColumns,
		pageable: {
	        numeric: true,
	        pageSizes: [ 5, 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 5,
		sort: {
            field: "SubscriptionName",
            dir: "asc"
        }
	});
	
	
	/* Fetch Company Subscription Types */
	function fetchCompanySubscriptionTypes() {
		companyServices.GetSubscriptionType(showLoader, hideLoader)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	$scope.observableList.data(response.data);
		    } else {
		        $scope.observableList.data([]);
		    }
		},
		function (rejection) { });
	}
	
	
	/* Kendo Grid configuration for Locations */
	$scope.gridColumns2 = [
		{field:"CityName",title:"Location", width:"300px"}
	];
	
	
	$scope.gridOptions2 = {
		columns: $scope.gridColumns2,
		pageable: {
	        numeric: true,
	        pageSizes: [ 5, 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	
	$scope.observableList2 = new kendo.data.DataSource({
		data: [],
		pageSize: 5,
		sort: {
            field: "CityName",
            dir: "asc"
        }
	});
	
	
	/* Fetch Subscription Locations */
	function fetchSubscriptionLocations() {
		userServices.GetAllCities()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.TotalData = response.data
				$scope.observableList2.data($scope.TotalData);
			} else {
				$scope.TotalData = [];
			}
		},
		function (rejection) {});
	}
	
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if(($scope.TotalData !== undefined) || ($scope.TotalData !== null)){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
							return regExpression.test(item.CityName);
						});
			$scope.observableList2.data(data);
		}
	}
	

}]);
