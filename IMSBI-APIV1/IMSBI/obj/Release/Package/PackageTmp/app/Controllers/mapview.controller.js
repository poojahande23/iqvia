angular.module("imsApp")
    .controller("MapViewController", ['$scope', '$state', 'userServices', 'companyServices', 'NgMap', 'notify', 'savedSearhcesServices', '$window', '$rootScope', '$timeout','$uibModal',
		function($scope, $state, userServices, companyServices, NgMap, notify, savedSearhcesServices, $window, $rootScope, $timeout, $uibModal){
    console.log('MapView controller...', $state);

    /* Make $rootScope available in Local $scope */
	$scope.rootScope = $rootScope;

	/* Parse session data to object format */
    function sessionInfoToScope(){
        $scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
    }
    sessionInfoToScope();

    angular.element(document).ready(function(){
        // loadMap();
    });


	/* Static Data & Values */
    $scope.comparisonType = [
        { title:"Company vs Company", value:"company" },
        { title: "Brand vs Brand", value: 'brand' }
    ];

    $scope.TherapyClassColl = [
        { type: "Therapy Class 1", value: 'tc1' },
        { type: "Therapy Class 2", value: 'tc2' },
        { type: "Therapy Class 3", value: 'tc3' },
        { type: "Therapy Class 4", value: 'tc4' },
		{ type: "Super Group", value: 'sup' }
    ];

    /* Initialize Models & Variables */
	initVariables();
	function initVariables() {
        showLoader = "angular.element('.loader-backdrop').fadeIn();";
        hideLoader = "angular.element('.loader-backdrop').fadeOut();";

        $scope.fetchData = {};
        $scope.fetchData.CompanyID = $scope.sessionInfo.CompanyID;
        $scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;		// Division to which user's role is associated
        $scope.fetchData.GeographyFilter = 'city';
        $scope.fetchData.ComparisonType = 'company';
		$scope.fetchData.DataType ="npm";
        $scope.fetchData.TCLevel = null;
        $scope.fetchData.TCCode = null;
        $scope.comparisonData = $scope.fetchData.ComparisonType;
        $scope.userId = $scope.sessionInfo.UserID;
        $scope.tabType = 2;
        $scope.isCluster = false;
        $scope.selected = {};
        $scope.selected.Therapies = [];
		$scope.fetchData.TCCode = null;
        $scope.selected.selectedTypes = [];
        $scope.fetchData.includeTopSearches = 1;
        $scope.filteredCategoryValues = [];
        $scope.percentArray_Sales = [];
        $scope.percentArray_Units = [];
        $scope.isPreset = false;
        $scope.RegExNumForm = /(\d)(?=(\d{2})+\d\.)/g;
        // Unit measures dropdown
        $scope.unitOfMeasuresColls = [
                                        {key:'value', label:'Values', divisor:1, unit:'', decimal:0},
                                        {key:'value', label:'Values(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'value', label:'Values(Cr)', divisor:10000000, unit:'Cr', decimal:2},
                                        {key:'unit', label:'Units', divisor:1, unit:'', decimal:0},
                                        {key:'unit', label:'Units(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'unit', label:'Units(Cr)', divisor:10000000, unit:'Cr', decimal:2}
                                    ];
        $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
        $scope.UOM_set_flag = false;
    }

	/* Edit Session info */
    function updateSessionInfo(GeographyID, PeriodTypeId){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		if(GeographyID !== undefined){ sessionInfo.GeographyID = GeographyID; }
		if(PeriodTypeId !== undefined){ sessionInfo.PeriodTypeID = PeriodTypeId; }
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
	}

	/* Divisions
	 $scope.FetchCompanyDivisions = function () {
	 companyServices.GetCompanyDivisions($scope.fetchData.CompanyID)
	 .then(function (response) {
	 if (response.data != null && response.status === 200) {
	 $scope.divisionColl = response.data;
	 $scope.fetchData.DivisonID = $scope.divisionColl[0].DivisionID;
	 $scope.FetchGeographyValues();
	 } else {
	 $scope.divisionColl = false;
	 }
	 },
	 function (rejection) {});
	 };
	 $scope.FetchCompanyDivisions();*/

	/* Geography */
    $scope.FetchGeographyValues = function (presetData) {
        userServices.GetGeographyValues($scope.fetchData.GeographyFilter, $scope.fetchData.CompanyID, $scope.sessionInfo.SessionID)
        .then(function (response) {
            if (response.data != null && response.status === 200 && response.data.length > 0) {
                $scope.geographyColl = response.data;
                if(JSON.parse($window.sessionStorage.imsSessionInfo).GeographyID === undefined){
                    $scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
                    updateSessionInfo($scope.geographyColl[0].GeographyID, undefined);
                }
                else{
                    $scope.fetchData.GeographyID = $scope.sessionInfo.GeographyID;
                }
                FetchPeriodTypes(presetData);
            } else {
                $scope.geographyColl = [];
            }
        },
        function (rejection) {});
    };


    /* begin invoking service calls */
	initServiceCalls();
	function initServiceCalls(){
		$scope.FetchGeographyValues();
	}


    /* Period Type */
	function FetchPeriodTypes(presetData) {
		userServices.GetSubscriptionPeriodType($scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.periodTypeColl = response.data;
				if($scope.sessionInfo.PeriodTypeID === undefined){
					$scope.PeriodTypeID = $scope.periodTypeColl.filter(function(item){ return (item.isDefault); })[0].SubscriptionPeriodTypeID;
		            updateSessionInfo(undefined, $scope.PeriodTypeID);
		        }
		        else{
		            $scope.PeriodTypeID = $scope.sessionInfo.PeriodTypeID;
		        }
				$scope.FetchTimePeriods({presetData:presetData, callNextFunciton:true});
			} else {
				$scope.periodTypeColl = [];
			}
		},
		function (rejection) {});
	}


	/* Time Periods */
	$scope.FetchTimePeriods = function (inputObj) {		// Accepts object of form {presetData:String, callNextFunciton:Boolean}
		$scope.period = {};
		userServices.GetTimePeriods($scope.fetchData.GeographyID, $scope.PeriodTypeID)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				$scope.timePeriods = response.data;
				if((inputObj === undefined) || (inputObj.presetData === undefined)){
					$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
					$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
				}
                // Queue in next function based on flow type
				if((inputObj !== undefined) && inputObj.callNextFunciton){
					$scope.FetchBricksByGeography(inputObj.presetData);
				}
			} else {
				$scope.timePeriods = [];
			}
		},
		function (rejection) {});
	};


	/* Period Type changed */
	$scope.periodTypeChanged = function(){
		updateSessionInfo(undefined, $scope.PeriodTypeID);
		$scope.FetchTimePeriods();
	};


	/* Validating the time selected */
	$scope.timeSelected = function(event){
		if($scope.period.startPeriod > $scope.period.endPeriod){
            $scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
			$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		}
	};


	/* Map Geography Id to Geography Name */
    function mapGeographyIdToName(){
        $scope.GeographyName = $scope.geographyColl.filter(function(item){ return (item.GeographyID === $scope.fetchData.GeographyID); })[0].GeographyValue;
    }


	/* Geography selected from dropdown by user */
    $scope.geographySelected = function(){
        updateSessionInfo($scope.fetchData.GeographyID, undefined);
        $scope.FetchTimePeriods();
        userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
        .then(function (response) {
            if (response.data != null && response.status === 200) {
                $scope.bricksColl = response.data;
                $scope.selectedPatches = [];
                if(response.data[0].IsCluster){
                    $scope.isCluster = true;
                }
                else{
                    $scope.isCluster = false;
                }
            } else {
                $scope.bricksColl = false;
            }
        },
        function (rejection) {});
    }


	/* Bricks */
    $scope.FetchBricksByGeography = function (presetData) {
        userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
        .then(function (response) {
            if (response.data != null && response.status === 200) {
                $scope.bricksColl = response.data;
                if(response.data[0].IsCluster){
                    $scope.isCluster = true;
                }
                else{
                    $scope.isCluster = false;
                }
            } else {
                $scope.bricksColl = false;
            }
            // Fetch values for first comparison type
            if(presetData !== undefined){
                if(Boolean(JSON.parse(presetData).TCLevel)){
                    $scope.fetchTherapyListByTherapyCode('$scope.displayResult('+presetData+')', JSON.parse(presetData).TCLevel, JSON.parse(presetData).TCCode);
                }
                else{
                    $scope.isPreset = true;
                    $scope.fetchComparisonTypeValue('$scope.displayResult('+presetData+')');
                }
            }
            else{
                $scope.isPreset = false;
                $scope.fetchComparisonTypeValue('$scope.displayResult()');
            }
        },
        function (rejection) {});
    }


    // Comparison Type changed
    $scope.comparisonTypeChanged = function(){
        $scope.isPreset = false;
        if($scope.fetchData.ComparisonType === 'company'){
            $scope.fetchData.TCLevel = undefined;
			$scope.fetchData.TCCode = undefined;
            $scope.fetchComparisonTypeValue();
        }
        else{
            $scope.fetchData.TCLevel = 'tc1';
            $scope.fetchTherapyListByTherapyCode('$scope.fetchComparisonTypeValue()');
        }
    };


    // Fetch Therapy List for selected therapy class
    $scope.fetchTherapyListByTherapyCode = function(customRoutine, TCLevel, TCCode){
        $scope.fetchData.TCLevel = (TCLevel !== undefined) ? TCLevel : $scope.fetchData.TCLevel;

        companyServices.GetTherapyListByTherapyCode($scope.fetchData.TCLevel, $scope.sessionInfo.SessionID)
        .then(function (response) {
                if (response.data != null && response.status === 200) {
                    $scope.therapyList = response.data;
                    $scope.therapyList.unshift({'TherapyCode':'all', 'TherapyName':'All'});
                    if(TCCode === undefined){
                        $scope.fetchData.TCCode = $scope.therapyList[0].TherapyCode;
                        $scope.selected.Therapies = [];
                    }
                    else{
                        $scope.fetchData.TCCode = TCCode;
                        $scope.selected.Therapies = ($scope.fetchData.TCCode !== 'all') ? $scope.fetchData.TCCode.split(',') : [];
                    }

                    if(TCCode !== undefined){
                        $scope.isPreset = true;
                        $scope.fetchComparisonTypeValue(customRoutine);
                    }
                    else if(customRoutine !== undefined){
                        $scope.isPreset = false;
                        eval(customRoutine);
                    }
                    else{
                        $scope.isPreset = false;
                    }
                } else {
                    $scope.therapyList = false;
                }
            },
            function (rejection) {});
    };


    /* Therapies selected from the list */
	$scope.therapiesSelected = function(isOpen){
		if(!isOpen){	// denoting that therapies have been selected by user
            $scope.isPreset = false;
			$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
			$scope.fetchComparisonTypeValue();
		}
	};

	/* Therapy removed from the list */
	$scope.therapiesRemoved = function($item, $model){
        $scope.isPreset = false;
		$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
		$scope.fetchComparisonTypeValue();
	};


	/* Map Therapy Id to Therapy Name */
    function mapTherapyIdToName(){
        $scope.TherapyName = $scope.therapyList.filter(function(item){ return (item.TherapyCode === $scope.fetchData.TCCode); })[0].TherapyName;
    }


    // Fetch Company or Brand List
    $scope.fetchComparisonTypeValue = function(customRoutine){
        var TherapyClass = ($scope.fetchData.TCLevel === undefined) ? null : $scope.fetchData.TCLevel;
        var selectedTherapy = ($scope.fetchData.TCCode === undefined) ? null : $scope.fetchData.TCCode;
        $scope.selected.selectedTypes = [];
        $scope.filteredCategoryValues = [];
        companyServices.GetComparisionTypeValue($scope.fetchData.ComparisonType, $scope.sessionInfo.SessionID, TherapyClass, selectedTherapy)
        .then(function (response) {
                if (response.data != null && response.status === 200) {
                    $scope.comparisonColl = response.data;
                    if($scope.isPreset){
                        $scope.filteredCategoryValues = $scope.comparisonColl;
                    }
                    if(customRoutine !== undefined){
                        eval(customRoutine);
                    }
                } else {
                    $scope.comparisonColl = false;
                }
            },
            function (rejection) {});
    };


	/* Filter Category values array */
    $scope.filterCategoryValues = function(searchQuery) {
        if(searchQuery !== ''){
            var regExpression = new RegExp(searchQuery, 'i');
            $scope.filteredCategoryValues = $scope.comparisonColl.filter(function(item){ return regExpression.test(item.ComparisonValue); });
        }
    };


    // Display Result
    $scope.displayResult = function (presetData) {
        if(presetData !== undefined){
            applyPresetData(presetData);
        }
        else{
            // Extract PeriodShortName based on its ID
			$scope.fetchData.PeriodType = $scope.periodTypeColl.filter(function(item){ return (item.SubscriptionPeriodTypeID === $scope.PeriodTypeID); })[0].PeriodShortName;
			$scope.fetchData.PeriodStart = $scope.period.startPeriod;
			$scope.fetchData.PeriodEnd = $scope.period.endPeriod;

            // Companies or Brands
            if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
                $scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
            }
            else{
                $scope.fetchData.ComparisionValue = 'all';
                $scope.fetchData.includeTopSearches = 1;
            }

            // Patches
            if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
                $scope.fetchData.PatchIds = $scope.selectedPatches.toString();
            }
            else{
                $scope.fetchData.PatchIds = 'all';
            }

            fetchPatchesByCluster();
        }
    };


    // Get Patches by Clusters
    function fetchPatchesByCluster(){
        if($scope.isCluster && ($scope.fetchData.PatchIds !== 'all')){
            userServices.GetPatchIDsByClusterIDs($scope.fetchData.PatchIds, showLoader, hideLoader)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.fetchData.PatchIds = response.data;
                        // Fetch Final Result
                        $scope.FetchCompanyPerformanceMap();
                    } else {
                        $scope.fetchData.PatchIds = '';
                    }
                },
                function (rejection) {});
        }
        else{
            // Fetch Final Result
            $scope.FetchCompanyPerformanceMap();
        }
    }


	/* Company Performance Map */
    $scope.FetchCompanyPerformanceMap = function () {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
        companyServices.GetCompanyPerformanceMap($scope.fetchData)
        .then(function (response) {
            if (response.data != null && response.status === 200) {
                $scope.mapviewData = response.data;

                resetUOM_MinValueLength($scope.mapviewData.CompanyPerformanceValueList);
                $scope.UOM_changed();
                mapGeographyIdToName();
                $scope.comparisonData = $scope.fetchData.ComparisonType;
                if($scope.fetchData.ComparisonType === 'company'){
                    $scope.TherapyName = '';
                }
                else if($scope.fetchData.ComparisonType === 'brand'){
                    mapTherapyIdToName();
                }
            } else {
                $scope.mapviewData = false;
            }
        },
        function (rejection) {});
    };


    /* Reset UOM based on Min value length */
    function resetUOM_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_set_flag)){
            var formattedArray = [], minVal = 0, min_local = 0, minLen = 1;
            for(var i = 0; i < data.length; i++){
                // Find min from multidimensional array
                formattedArray = data[i].map_chart.data.filter(function(item){ return (item[1] >= 1); }).map(function(item){ return (item[1]); });
                min_local = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
                if(i === 0){ minVal = min_local; }
                if(min_local < minVal){
                    minVal = min_local;
                }
            }
            minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[2];
            }
        }
    }


    $scope.UOM_changed = function(inputObj) {
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

        if($scope.mapviewData !== undefined){
            if($scope.unitOfMeasures.key === 'value'){
                preparePerformanceMap($scope.mapviewData.CompanyPerformanceValueList);
            }
            else{
                preparePerformanceMap($scope.mapviewData.CompanyPerformanceUnitList);
            }
        }
    };


    /*--------------------- Divide a number(n) by (d) and round upto (x) decimal places ---------------------*/
	$scope.DivideAndRound = function(n, d, x) {
		if(x === 0){
			n = (n/d).toFixed(1);
			n = (n.replace($scope.RegExNumForm, '$1,'));
			n = n.substring(0, (n.length-2));
		}
		else{
			n = (n/d).toFixed(x);
			n = (n.replace($scope.RegExNumForm, '$1,'));
		}
		return n;
	}


	/* Select Preset functionality Begins */
    $scope.savePreset = function(){
        savedSearhcesServices.AddUpdateSavedSearches(preparePresetData())
        .then(function (response) {
            if (response.data !== null && response.status === 200) {
                initializeOnPreset();
                notify({
                    message: 'Presets saved Successfully',
                    classes: $scope.classes,
                    templateUrl: $scope.template,
                    position: 'center',
                    duration: 2000
                });
            } else {}
        },
        function (rejection) {});
    };


    function preparePresetData () {
        var presetObject = {};
        presetObject.SearchKey = $scope.PresetName;
        presetObject.UserID = $scope.userId;
        presetObject.TabType = $scope.tabType;

        // Extract PeriodShortName based on its ID
        $scope.fetchData.PeriodType = $scope.periodTypeColl.filter(function(item){ return (item.SubscriptionPeriodTypeID === $scope.PeriodTypeID); })[0].PeriodShortName;
        $scope.fetchData.PeriodStart = $scope.period.startPeriod;
        $scope.fetchData.PeriodEnd = $scope.period.endPeriod;

        // Patches
        if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
            $scope.fetchData.PatchIds = $scope.selectedPatches.toString();
        }
        else{
            $scope.fetchData.PatchIds = 'all';
        }

        // Companies or Brands
        if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
            $scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
        }
        else{
            $scope.fetchData.ComparisionValue = 'all';
            $scope.fetchData.includeTopSearches = 1;
        }

        // Remove SessionID before saving preset
        if($scope.fetchData.SessionID !== undefined){
            delete $scope.fetchData.SessionID;
        }
        presetObject.SearchData = JSON.stringify($scope.fetchData);
        return presetObject;
    }


	/* Get Presets */
    function LoadPresets(){
        savedSearhcesServices.GetSavedSearchesbyUser($scope.userId, $scope.tabType)
        .then(function (response) {
                if (response.data != null && response.status === 200) {
                    $scope.presetsColl = response.data;
                } else {
                    $scope.presetsColl = false;
                }
            },
            function (rejection) {});
    }
    LoadPresets();

    $scope.applySelectedPreset = function() {
        angular.element('.loader-backdrop').fadeIn();
        var presetData = $scope.presetsColl.filter(function(item){ return (item.SearchKeyID === $scope.selectedPreset); })[0].SearchData;
        var presetData_obj = JSON.parse(presetData);
        $scope.fetchData.GeographyFilter = presetData_obj.GeographyFilter;
        $scope.fetchData.DivisonID = presetData_obj.DivisonID;
        $scope.fetchData.ComparisonType = presetData_obj.ComparisonType;
        var local_PeriodID = $scope.periodTypeColl.filter(function(item){ return (item.PeriodShortName === presetData_obj.PeriodType); })[0].SubscriptionPeriodTypeID;
		updateSessionInfo(presetData_obj.GeographyID, local_PeriodID);
        $scope.FetchGeographyValues(presetData);
    };


    // Apply Preset data to fetchData
    function applyPresetData(presetData){
        $scope.fetchData = presetData;

        if($scope.fetchData.PeriodStart !== 0){
            $scope.period.startPeriod = $scope.fetchData.PeriodStart;
            $scope.period.endPeriod = $scope.fetchData.PeriodEnd;
        }

        if($scope.fetchData.PatchIds !== 'all'){
            $scope.selectedPatches = $scope.fetchData.PatchIds.split(',').map(Number);
        }
        else{
            $scope.selectedPatches = [];
        }

        if($scope.fetchData.ComparisionValue !== 'all'){
            var selectedTypes_local = $scope.fetchData.ComparisionValue.split(',');
            $scope.selected.selectedTypes = selectedTypes_local.map(function(item){ return Number(item); });
        }
        else{
            $scope.selected.selectedTypes = [];
        }

        angular.element('.loader-backdrop').fadeOut();
    }


    // Initialize filter choices on preset
    function initializeOnPreset(){
        LoadPresets();
        $scope.PresetName = '';
        $scope.SavePresetFlag = false;
        $scope.fetchData.GeographyFilter = 'city';
        $scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
        $scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
        $scope.selectedPatches = [];
        $scope.PeriodTypeID = $scope.sessionInfo.PeriodTypeID;
        $scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
        $scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
        $scope.fetchData.ComparisonType = 'company';
        $scope.selected.selectedTypes = [];
        $scope.fetchData.TCLevel = null;
        $scope.fetchData.TCCode = null;
        $scope.fetchData.includeTopSearches = 1;
        $scope.filteredCategoryValues = [];
        $scope.isPreset = false;
    }
	/* Select Preset functionality Ends */




    $scope.mapviewCompany=[
        {company:"Company to Company"},
        {company:"Brand to Brand"}
    ];



	/*------------------------ Google map customized styling ------------------------*/
    var styleArray =
        [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#e31111"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#9787f0"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f3f2ed"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#cae7f0"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d4f2cb"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#eacbe5"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#e3bcd9"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e6e7e9"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e8eaed"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d9ecec"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ];

    $scope.options = {
        styles: styleArray
    };

	/* ng-map with polygons covering pincodes */
	/*$scope.mapCenter = [15.5356433,73.8634543];		// Map-center for Goa

	 $scope.polyCoords = [];
	 // SANTA CRUZ 403005
	 $scope.polyCoords.push([[15.479827,73.836313],[15.478305,73.835379],[15.477633,73.834864],[15.476861,73.834557],[15.476593,73.834559],[15.475826,73.835185],[15.47556,73.835463],[15.475666,73.836603],[15.475567,73.83688],[15.475202,73.837504],[15.47487,73.83792],[15.474874,73.83875],[15.474944,73.839406],[15.474984,73.840511],[15.474484,73.840963],[15.474116,73.841],[15.473779,73.840725],[15.473207,73.840072],[15.472635,73.839591],[15.472032,73.839595],[15.470927,73.839601],[15.470224,73.839605],[15.469821,73.839503],[15.469719,73.839089],[15.469277,73.837882],[15.468505,73.837507],[15.465597,73.836829],[15.464928,73.836902],[15.464426,73.837112],[15.463961,73.837736],[15.463433,73.839191],[15.463036,73.840229],[15.462907,73.841128],[15.462742,73.841578],[15.461772,73.84186],[15.459863,73.841802],[15.459092,73.841633],[15.458523,73.841567],[15.458054,73.841605],[15.456081,73.842194],[15.456101,73.845965],[15.45591,73.848999],[15.456181,73.851498],[15.45676,73.853835],[15.45713,73.855642],[15.457343,73.856917],[15.458076,73.858988],[15.458853,73.859729],[15.459941,73.860787],[15.461081,73.861844],[15.462736,73.862953],[15.464361,73.863817],[15.465463,73.86288],[15.465748,73.862456],[15.465869,73.862032],[15.466312,73.860422],[15.467004,73.859445],[15.467616,73.858934],[15.468229,73.858508],[15.469046,73.85808],[15.469947,73.857737],[15.470888,73.857351],[15.472625,73.857261],[15.472793,73.858191],[15.472961,73.858867],[15.473417,73.859837],[15.474078,73.860806],[15.474514,73.860869],[15.475098,73.860346],[15.475454,73.859435],[15.475771,73.858771],[15.476447,73.857858],[15.477567,73.857646],[15.478682,73.856234],[15.479158,73.855487],[15.479583,73.855128],[15.479996,73.85478],[15.481155,73.854195],[15.482432,73.853279],[15.483548,73.852239],[15.484341,73.850788],[15.484816,73.849711],[15.485292,73.848716],[15.48573,73.848176],[15.486447,73.847552],[15.487125,73.846887],[15.488395,73.844607],[15.491628,73.839692],[15.490901,73.839851],[15.490215,73.840495],[15.488487,73.842048],[15.488213,73.84243],[15.487876,73.842686],[15.487294,73.842991],[15.486956,73.842977],[15.486616,73.842661],[15.486397,73.841901],[15.486163,73.841284],[15.48573,73.840747],[15.48539,73.840511],[15.485098,73.840481],[15.484745,73.840499],[15.484284,73.840422],[15.482565,73.839783],[15.481291,73.839509],[15.480106,73.838766],[15.479552,73.837081],[15.479827,73.836313]]);
	 // RIBANDAR 403006
	 $scope.polyCoords.push([[15.497405,73.868714],[15.49674,73.868799],[15.495921,73.869247],[15.49534,73.870381],[15.495149,73.871432],[15.495,73.872847],[15.494889,73.874099],[15.493882,73.876084],[15.493143,73.876976],[15.492599,73.877746],[15.492054,73.878476],[15.491628,73.879367],[15.491085,73.880299],[15.490384,73.880989],[15.48972,73.881195],[15.488977,73.881199],[15.488311,73.881162],[15.485745,73.88134],[15.485278,73.881867],[15.484813,73.882799],[15.484428,73.884053],[15.484042,73.885105],[15.483955,73.885196],[15.483614,73.885551],[15.482718,73.886323],[15.482212,73.887053],[15.482059,73.88774],[15.482063,73.888548],[15.482065,73.888952],[15.481638,73.88964],[15.481226,73.891686],[15.481475,73.892814],[15.481874,73.893659],[15.482397,73.894849],[15.482795,73.895443],[15.483743,73.89663],[15.484614,73.897252],[15.485224,73.897688],[15.485774,73.898281],[15.48696,73.898306],[15.487506,73.898021],[15.488081,73.897548],[15.48846,73.898164],[15.488301,73.898526],[15.488062,73.899095],[15.487704,73.899997],[15.48764,73.90058],[15.487591,73.901647],[15.487957,73.902296],[15.488176,73.903154],[15.489028,73.904341],[15.488985,73.904425],[15.488912,73.90452],[15.489236,73.904534],[15.490515,73.904392],[15.491499,73.904286],[15.493073,73.904209],[15.49432,73.904135],[15.495595,73.903282],[15.496512,73.902972],[15.497528,73.902798],[15.498577,73.902488],[15.499821,73.902007],[15.500541,73.901732],[15.500863,73.901082],[15.501061,73.90068],[15.500857,73.899293],[15.501208,73.897225],[15.501171,73.896413],[15.500735,73.894553],[15.500718,73.893382],[15.501056,73.892816],[15.501077,73.891702],[15.501035,73.891172],[15.500965,73.89048],[15.501112,73.888196],[15.500956,73.88512],[15.501434,73.883782],[15.502558,73.881995],[15.503248,73.879932],[15.503835,73.878761],[15.504109,73.877898],[15.504472,73.876754],[15.504843,73.875416],[15.504998,73.874191],[15.504988,73.872132],[15.50493,73.871044],[15.50262,73.870705],[15.501718,73.870266],[15.500621,73.869949],[15.49964,73.869469],[15.498464,73.869072],[15.497405,73.868714]]);
	 // CHORAO 403102
	 $scope.polyCoords.push([[15.503571,73.854451],[15.503477,73.856448],[15.503364,73.858839],[15.503372,73.86039],[15.503898,73.865234],[15.50493,73.871044],[15.504988,73.872132],[15.504998,73.874191],[15.504843,73.875416],[15.504472,73.876754],[15.504109,73.877898],[15.504293,73.878023],[15.505554,73.879753],[15.506465,73.880861],[15.507245,73.881391],[15.509021,73.882851],[15.51306,73.887907],[15.519738,73.894462],[15.52379,73.896853],[15.52483,73.897632],[15.526218,73.899192],[15.52719,73.900185],[15.527816,73.901179],[15.528375,73.902459],[15.529071,73.903666],[15.529835,73.904446],[15.530596,73.90487],[15.530973,73.905494],[15.531662,73.905276],[15.534407,73.904378],[15.534654,73.904342],[15.535445,73.904372],[15.536088,73.90459],[15.536714,73.904688],[15.537308,73.904889],[15.537639,73.905261],[15.538341,73.906499],[15.538739,73.907709],[15.539177,73.906536],[15.539569,73.905473],[15.540262,73.904922],[15.540756,73.904372],[15.541414,73.903309],[15.541807,73.902349],[15.542399,73.901559],[15.543519,73.900117],[15.544012,73.899362],[15.544638,73.898674],[15.545398,73.898362],[15.545995,73.898428],[15.546661,73.899006],[15.547326,73.899412],[15.547923,73.899683],[15.547302,73.901362],[15.546776,73.902117],[15.546549,73.903247],[15.546423,73.904548],[15.545764,73.905269],[15.545073,73.906162],[15.543684,73.906649],[15.54266,73.90727],[15.542066,73.907888],[15.541538,73.908267],[15.54121,73.909022],[15.541219,73.910766],[15.540671,73.91366],[15.541623,73.914776],[15.541864,73.915673],[15.542953,73.917326],[15.544337,73.919583],[15.544642,73.919221],[15.545493,73.918777],[15.546798,73.917921],[15.549846,73.915261],[15.550694,73.914282],[15.551049,73.91394],[15.551875,73.913144],[15.552875,73.912039],[15.553875,73.911122],[15.554024,73.911121],[15.553519,73.909244],[15.555008,73.907225],[15.557096,73.905203],[15.557497,73.902574],[15.555435,73.902737],[15.552591,73.903371],[15.550421,73.904001],[15.549445,73.90362],[15.549805,73.900678],[15.550323,73.899283],[15.551366,73.89804],[15.552336,73.897262],[15.553385,73.897334],[15.554141,73.898567],[15.556457,73.89724],[15.558393,73.894832],[15.560253,73.892192],[15.561069,73.890563],[15.560992,73.889909],[15.560834,73.888553],[15.559522,73.885199],[15.558723,73.883158],[15.558177,73.881761],[15.557703,73.879149],[15.55756,73.878361],[15.555828,73.876591],[15.553427,73.875985],[15.550882,73.876541],[15.549332,73.876698],[15.549486,73.875158],[15.549803,73.873885],[15.55031,73.871676],[15.550662,73.870872],[15.550592,73.869803],[15.549965,73.867533],[15.549961,73.866831],[15.547693,73.866609],[15.54695,73.866847],[15.546239,73.867118],[15.545397,73.867257],[15.537163,73.868564],[15.533944,73.869532],[15.530494,73.870501],[15.527726,73.869566],[15.525188,73.868867],[15.52081,73.868416],[15.517138,73.862823],[15.515668,73.860958],[15.513036,73.859039],[15.510107,73.85774],[15.508606,73.857207],[15.506653,73.85629],[15.505224,73.855293],[15.503571,73.854451]]);
	 // TARVALEM 403103
	 $scope.polyCoords.push([[15.447487,74.165839],[15.446412,74.167024],[15.445792,74.167296],[15.445205,74.167602],[15.443901,74.168417],[15.442759,74.169062],[15.440475,74.170049],[15.439887,74.170321],[15.438353,74.170867],[15.437624,74.17076],[15.437907,74.169674],[15.436286,74.166932],[15.436404,74.16527],[15.436394,74.162522],[15.435543,74.160461],[15.435299,74.159987],[15.43481,74.158641],[15.435287,74.156791],[15.435597,74.155541],[15.435302,74.154293],[15.434476,74.153672],[15.433488,74.151387],[15.434166,74.148734],[15.434161,74.147248],[15.434154,74.145503],[15.433896,74.14363],[15.43359,74.1431],[15.431548,74.144438],[15.428683,74.144995],[15.427326,74.14539],[15.426123,74.146485],[15.425297,74.1475],[15.424015,74.147661],[15.422506,74.147667],[15.418657,74.14786],[15.417451,74.147921],[15.414512,74.148789],[15.412928,74.148796],[15.41127,74.149347],[15.409538,74.15021],[15.40803,74.150606],[15.406902,74.1517],[15.403935,74.156638],[15.399499,74.160704],[15.397764,74.160788],[15.395203,74.162122],[15.392649,74.165012],[15.391295,74.166184],[15.389186,74.167205],[15.387,74.167914],[15.386165,74.166594],[15.384271,74.164345],[15.382913,74.164506],[15.381859,74.165288],[15.37953,74.167866],[15.377948,74.168495],[15.375458,74.168582],[15.369205,74.165791],[15.363009,74.163481],[15.361044,74.16271],[15.360138,74.162403],[15.356653,74.158681],[15.35529,74.157442],[15.354231,74.156668],[15.352797,74.156674],[15.351667,74.157223],[15.350688,74.157538],[15.349782,74.157542],[15.348569,74.15599],[15.347506,74.154205],[15.346671,74.152885],[15.345519,74.151512],[15.344902,74.152258],[15.344545,74.152949],[15.343315,74.154068],[15.341715,74.155126],[15.339304,74.154969],[15.337825,74.155009],[15.33715,74.155144],[15.336316,74.155512],[15.335386,74.15598],[15.334455,74.156514],[15.334071,74.156814],[15.333819,74.156877],[15.333268,74.157016],[15.331903,74.157189],[15.330814,74.158353],[15.330398,74.158819],[15.329053,74.160051],[15.327705,74.160553],[15.326163,74.160858],[15.324681,74.159969],[15.324132,74.159407],[15.32342,74.158051],[15.322903,74.157357],[15.320343,74.156036],[15.319341,74.154714],[15.318565,74.153391],[15.317757,74.1524],[15.316338,74.151213],[15.315532,74.150586],[15.312725,74.147913],[15.311361,74.147025],[15.31151,74.148072],[15.311176,74.149778],[15.309672,74.150536],[15.307659,74.151549],[15.306444,74.151585],[15.304135,74.151625],[15.301051,74.151268],[15.301206,74.152255],[15.301361,74.153242],[15.301568,74.154697],[15.302785,74.156408],[15.303496,74.157601],[15.30437,74.162276],[15.30503,74.163418],[15.305649,74.167003],[15.306186,74.168938],[15.306297,74.17045],[15.305845,74.1711],[15.305849,74.172217],[15.306481,74.173079],[15.308203,74.175881],[15.308902,74.175915],[15.309845,74.175839],[15.311032,74.175618],[15.312045,74.17565],[15.313023,74.175574],[15.314242,74.174669],[15.314905,74.174342],[15.315988,74.174266],[15.317068,74.173685],[15.31757,74.172998],[15.318518,74.172534],[15.319357,74.172646],[15.320421,74.173102],[15.32165,74.173098],[15.326232,74.172676],[15.327182,74.172672],[15.3283,74.172668],[15.329586,74.172605],[15.330984,74.17283],[15.333723,74.172992],[15.335051,74.172898],[15.335458,74.173051],[15.336641,74.173601],[15.337056,74.173715],[15.33741,74.174057],[15.337529,74.174441],[15.33753,74.174723],[15.337315,74.174987],[15.336807,74.175373],[15.33622,74.175638],[15.33577,74.17572],[15.335399,74.175964],[15.335399,74.176166],[15.335557,74.176428],[15.336127,74.176891],[15.33709,74.177574],[15.337581,74.177916],[15.338112,74.17852],[15.338371,74.17967],[15.338378,74.181529],[15.338301,74.181934],[15.338145,74.182318],[15.337854,74.182519],[15.336597,74.184045],[15.336105,74.185029],[15.335841,74.185697],[15.335767,74.18609],[15.335921,74.186718],[15.336152,74.187502],[15.33623,74.187816],[15.336117,74.188327],[15.335776,74.188603],[15.334138,74.188413],[15.334407,74.189001],[15.335058,74.190255],[15.335175,74.190804],[15.335597,74.191745],[15.336168,74.19186],[15.336549,74.191859],[15.337195,74.191464],[15.338713,74.190241],[15.340159,74.189999],[15.342746,74.189283],[15.344552,74.189436],[15.346269,74.19049],[15.34677,74.192176],[15.346814,74.193904],[15.347166,74.196572],[15.348018,74.200417],[15.349396,74.202492],[15.349713,74.202868],[15.349915,74.201736],[15.350425,74.201131],[15.353568,74.200818],[15.354738,74.200813],[15.356203,74.201335],[15.357087,74.203142],[15.357827,74.204807],[15.360067,74.203061],[15.365719,74.198656],[15.366706,74.198824],[15.370661,74.199498],[15.373121,74.200528],[15.374009,74.202278],[15.375653,74.20422],[15.37812,74.207393],[15.3797,74.209141],[15.380652,74.211215],[15.381415,74.213291],[15.382745,74.215234],[15.384134,74.216463],[15.386155,74.218144],[15.387731,74.218658],[15.390047,74.218322],[15.390827,74.21864],[15.39079,74.218052],[15.391089,74.217058],[15.391249,74.215612],[15.392011,74.213567],[15.393215,74.21206],[15.395991,74.211026],[15.398852,74.210287],[15.402412,74.208008],[15.404521,74.205891],[15.408908,74.202179],[15.410715,74.20053],[15.412992,74.198037],[15.414832,74.197269],[15.416534,74.19655],[15.41948,74.195588],[15.421312,74.194835],[15.423007,74.1946],[15.423966,74.194672],[15.424853,74.195506],[15.42611,74.196413],[15.427958,74.197775],[15.429067,74.198608],[15.43032,74.198831],[15.431721,74.198826],[15.432458,74.198823],[15.434004,74.198208],[15.439011,74.196364],[15.4407,74.194684],[15.443525,74.190955],[15.443671,74.190934],[15.446039,74.190925],[15.448451,74.189956],[15.44887,74.189778],[15.448672,74.189232],[15.448505,74.188269],[15.448503,74.187595],[15.448688,74.187257],[15.449292,74.186315],[15.449732,74.185253],[15.45001,74.184059],[15.449163,74.182255],[15.448949,74.1811],[15.448992,74.180184],[15.449944,74.178831],[15.450804,74.177719],[15.4512,74.177308],[15.452315,74.175858],[15.452641,74.17552],[15.452999,74.175218],[15.453133,74.175131],[15.453246,74.174987],[15.453221,74.174505],[15.452938,74.173928],[15.452727,74.173568],[15.452398,74.172991],[15.451952,74.17239],[15.451622,74.171717],[15.451505,74.170567],[15.451395,74.170295],[15.450861,74.169063],[15.450549,74.168773],[15.44978,74.167994],[15.448253,74.1672],[15.44785,74.166825],[15.447487,74.165839]]);
	 // NEURA 403104
	 $scope.polyCoords.push([[15.461887,73.901224],[15.461704,73.900357],[15.460929,73.900608],[15.460038,73.901289],[15.458848,73.901973],[15.456351,73.90371],[15.455102,73.904517],[15.454392,73.905751],[15.454578,73.907166],[15.455298,73.908086],[15.455545,73.909808],[15.454592,73.91006],[15.453223,73.910621],[15.452033,73.911304],[15.450789,73.912186],[15.450625,73.911973],[15.450299,73.911744],[15.449852,73.911433],[15.44871,73.910309],[15.448078,73.909705],[15.447214,73.908738],[15.447018,73.909079],[15.446494,73.909693],[15.445199,73.911504],[15.444313,73.912052],[15.442801,73.912672],[15.441057,73.912681],[15.440431,73.912684],[15.439971,73.912856],[15.439346,73.913063],[15.438622,73.913135],[15.438406,73.913144],[15.437892,73.912018],[15.437592,73.911272],[15.436898,73.910698],[15.436602,73.910768],[15.435898,73.910941],[15.435272,73.910877],[15.434678,73.910574],[15.43428,73.910101],[15.434144,73.909218],[15.43414,73.908369],[15.433969,73.907181],[15.432816,73.906881],[15.432189,73.906749],[15.431694,73.906411],[15.431461,73.905903],[15.431294,73.905462],[15.431063,73.90526],[15.430766,73.905126],[15.430107,73.905129],[15.429647,73.905335],[15.42932,73.905711],[15.429091,73.906051],[15.428896,73.906562],[15.428634,73.906903],[15.428306,73.907075],[15.427943,73.907009],[15.427284,73.906842],[15.426988,73.906844],[15.426232,73.907187],[15.425774,73.907869],[15.425315,73.908245],[15.424692,73.908656],[15.423937,73.909169],[15.423414,73.910021],[15.423385,73.910769],[15.423454,73.911482],[15.423657,73.912568],[15.423496,73.913316],[15.423072,73.914167],[15.422517,73.914985],[15.422189,73.915327],[15.421008,73.91625],[15.420711,73.916676],[15.420714,73.917273],[15.420344,73.917705],[15.417766,73.919695],[15.416297,73.922965],[15.41611,73.924004],[15.414587,73.933608],[15.41652,73.935105],[15.418749,73.935918],[15.421604,73.936729],[15.425178,73.937078],[15.426222,73.937395],[15.426991,73.938252],[15.428213,73.93925],[15.429368,73.940822],[15.430172,73.941894],[15.431092,73.94519],[15.431555,73.947519],[15.432017,73.949704],[15.432769,73.952121],[15.433057,73.952499],[15.433504,73.953086],[15.434063,73.953621],[15.434795,73.954048],[15.435666,73.954474],[15.437719,73.954966],[15.438171,73.954928],[15.438657,73.954746],[15.439386,73.954527],[15.440533,73.954522],[15.441679,73.954085],[15.44275,73.953606],[15.442268,73.952059],[15.441095,73.949987],[15.441045,73.948879],[15.441218,73.947677],[15.441304,73.946845],[15.442196,73.946194],[15.44327,73.946189],[15.445242,73.946733],[15.445958,73.946729],[15.447166,73.946492],[15.448234,73.945332],[15.44881,73.944083],[15.449344,73.943341],[15.450548,73.941755],[15.451083,73.941884],[15.451795,73.941881],[15.452714,73.941968],[15.453575,73.942178],[15.454496,73.942448],[15.455178,73.942475],[15.45589,73.942472],[15.456334,73.942378],[15.456837,73.942069],[15.457078,73.941883],[15.457775,73.942112],[15.458364,73.942398],[15.459039,73.942771],[15.459574,73.943259],[15.460024,73.943662],[15.461968,73.943739],[15.462585,73.943881],[15.463622,73.943847],[15.464041,73.9437],[15.464121,73.942832],[15.464534,73.941239],[15.465147,73.940398],[15.466437,73.939503],[15.468582,73.937612],[15.469906,73.936069],[15.470812,73.93498],[15.470981,73.933696],[15.470716,73.933281],[15.470434,73.932939],[15.469976,73.932436],[15.469392,73.930947],[15.469999,73.929661],[15.470922,73.928644],[15.471848,73.927989],[15.472458,73.927461],[15.472851,73.925381],[15.472883,73.924784],[15.472756,73.923863],[15.472595,73.923231],[15.472173,73.92289],[15.471713,73.92188],[15.47104,73.920438],[15.470842,73.919927],[15.468436,73.920218],[15.467613,73.92067],[15.466919,73.920673],[15.466267,73.920363],[15.466235,73.920208],[15.466049,73.919304],[15.466038,73.91701],[15.465709,73.916396],[15.465029,73.915868],[15.464376,73.915368],[15.464074,73.914586],[15.464043,73.913775],[15.464498,73.912542],[15.464197,73.912124],[15.463654,73.911819],[15.462944,73.910843],[15.462806,73.910397],[15.463043,73.908969],[15.463581,73.908071],[15.464011,73.907229],[15.463734,73.906112],[15.46327,73.905555],[15.462917,73.905333],[15.462481,73.904859],[15.462178,73.903994],[15.461887,73.901224]]);
	 // VELGUEM 403105
	 $scope.polyCoords.push([[15.634837,74.092112],[15.633222,74.09292],[15.632676,74.093204],[15.632335,74.093418],[15.631925,74.093525],[15.631549,74.093492],[15.630453,74.092755],[15.628133,74.092008],[15.626488,74.092038],[15.623756,74.092491],[15.622624,74.09276],[15.621531,74.093401],[15.620265,74.093972],[15.618677,74.094145],[15.618317,74.094254],[15.617974,74.094103],[15.617253,74.093532],[15.616347,74.092914],[15.615138,74.092033],[15.613904,74.090747],[15.61281,74.089531],[15.612624,74.08934],[15.61179,74.089603],[15.610337,74.090856],[15.609485,74.091182],[15.609153,74.091506],[15.608529,74.091595],[15.607841,74.091512],[15.607445,74.09132],[15.606672,74.090743],[15.6059,74.090489],[15.604983,74.090385],[15.604234,74.090388],[15.603485,74.090542],[15.602431,74.091151],[15.601705,74.091692],[15.601,74.092577],[15.600315,74.093182],[15.599714,74.093786],[15.598946,74.094349],[15.598114,74.094589],[15.59745,74.095173],[15.595974,74.095803],[15.594834,74.097055],[15.592613,74.098935],[15.591332,74.099441],[15.590861,74.100524],[15.590192,74.101375],[15.589488,74.10296],[15.589045,74.104312],[15.588936,74.105084],[15.58842,74.106629],[15.588497,74.107362],[15.588847,74.108098],[15.588799,74.108322],[15.588481,74.109287],[15.588257,74.109549],[15.587956,74.109869],[15.587027,74.11091],[15.586769,74.11128],[15.586266,74.111957],[15.584136,74.113313],[15.583306,74.114118],[15.582705,74.114803],[15.582075,74.115369],[15.581388,74.115995],[15.58026,74.116684],[15.579745,74.117191],[15.579658,74.117306],[15.579253,74.116992],[15.578316,74.116996],[15.57753,74.116781],[15.576516,74.115596],[15.576302,74.114849],[15.576086,74.113696],[15.575777,74.112076],[15.575473,74.111485],[15.575048,74.110894],[15.57432,74.110243],[15.573686,74.110245],[15.57251,74.110594],[15.571635,74.11094],[15.570911,74.11113],[15.570098,74.111664],[15.569256,74.112634],[15.568141,74.113231],[15.566483,74.113799],[15.566266,74.113838],[15.566343,74.114152],[15.565329,74.115171],[15.563195,74.117333],[15.561444,74.118755],[15.55971,74.11948],[15.557572,74.120724],[15.554982,74.122034],[15.553881,74.122376],[15.551746,74.123715],[15.5509,74.12475],[15.550459,74.12563],[15.550204,74.12651],[15.549949,74.127695],[15.549028,74.12873],[15.54833,74.129764],[15.547849,74.130463],[15.547175,74.130706],[15.546578,74.130521],[15.545489,74.130633],[15.544685,74.130636],[15.543648,74.130748],[15.542585,74.130859],[15.540695,74.131423],[15.541029,74.13169],[15.541589,74.133136],[15.542649,74.135133],[15.543781,74.13652],[15.54521,74.138268],[15.546837,74.138909],[15.549073,74.1393],[15.549683,74.139545],[15.550463,74.140647],[15.551116,74.141399],[15.551831,74.142951],[15.552278,74.143773],[15.553611,74.144835],[15.554321,74.145198],[15.555599,74.145405],[15.556035,74.145313],[15.556535,74.144853],[15.556767,74.143693],[15.556735,74.143114],[15.556473,74.142465],[15.556797,74.141993],[15.557317,74.141823],[15.55787,74.141753],[15.55857,74.141683],[15.559173,74.141664],[15.559852,74.140535],[15.560045,74.139777],[15.56027,74.13907],[15.560578,74.138649],[15.561,74.138412],[15.561814,74.138207],[15.56253,74.138137],[15.563214,74.138369],[15.563753,74.138636],[15.564129,74.139054],[15.564884,74.140504],[15.565246,74.140001],[15.565765,74.139424],[15.566593,74.137555],[15.567249,74.136229],[15.567904,74.134822],[15.568834,74.134216],[15.569298,74.133612],[15.569313,74.132549],[15.569486,74.131846],[15.570574,74.130089],[15.571364,74.128361],[15.572409,74.127133],[15.574451,74.122651],[15.576212,74.120858],[15.576404,74.120606],[15.578554,74.121141],[15.579189,74.12188],[15.579683,74.123183],[15.579742,74.123569],[15.579685,74.123717],[15.579571,74.124044],[15.579257,74.124431],[15.578743,74.125175],[15.578658,74.125531],[15.578315,74.126096],[15.578461,74.126481],[15.578579,74.12746],[15.578526,74.128468],[15.577269,74.130402],[15.576555,74.131443],[15.576409,74.131873],[15.576614,74.132732],[15.577222,74.133946],[15.577542,74.135042],[15.577689,74.135724],[15.577835,74.136465],[15.577953,74.137087],[15.578043,74.138066],[15.578475,74.138479],[15.579885,74.138977],[15.581793,74.139266],[15.582603,74.139263],[15.583455,74.139427],[15.584267,74.139716],[15.585322,74.140047],[15.586052,74.140211],[15.586255,74.14024],[15.586314,74.140549],[15.586716,74.141711],[15.586718,74.142282],[15.58672,74.142806],[15.586458,74.143515],[15.585997,74.144338],[15.585359,74.145093],[15.584522,74.145895],[15.584371,74.146763],[15.58453,74.147743],[15.584931,74.148654],[15.58633,74.150405],[15.58746,74.151313],[15.588055,74.151749],[15.589255,74.152606],[15.58999,74.154177],[15.591405,74.156013],[15.591657,74.157325],[15.59173,74.157799],[15.59171,74.158508],[15.593394,74.158559],[15.594104,74.159359],[15.594638,74.160415],[15.594927,74.161984],[15.594931,74.163078],[15.595464,74.16377],[15.597094,74.164931],[15.598019,74.166314],[15.599084,74.167587],[15.601417,74.168228],[15.601454,74.16805],[15.601868,74.167649],[15.602115,74.167334],[15.602527,74.166561],[15.602719,74.166103],[15.603077,74.165645],[15.603434,74.165015],[15.60379,74.164214],[15.604065,74.163671],[15.604421,74.162756],[15.604668,74.162183],[15.604831,74.161583],[15.604994,74.160783],[15.605212,74.15984],[15.605567,74.158582],[15.605869,74.158095],[15.606364,74.157465],[15.606972,74.157291],[15.607829,74.157202],[15.608631,74.157198],[15.609655,74.157337],[15.6109,74.157418],[15.612052,74.157609],[15.612605,74.157521],[15.613075,74.157348],[15.613491,74.157161],[15.613504,74.156693],[15.613639,74.156458],[15.613795,74.156329],[15.613978,74.156118],[15.614125,74.15594],[15.614272,74.155692],[15.614415,74.155411],[15.614625,74.154964],[15.61468,74.154758],[15.614639,74.154593],[15.61415,74.153829],[15.613983,74.153427],[15.613847,74.153031],[15.613841,74.152641],[15.613805,74.152449],[15.613841,74.152097],[15.613984,74.151607],[15.61435,74.151196],[15.614986,74.150454],[15.615913,74.147623],[15.61593,74.146796],[15.615482,74.145068],[15.615687,74.144064],[15.615996,74.143693],[15.616764,74.143836],[15.617422,74.14439],[15.617822,74.144872],[15.618245,74.145112],[15.619303,74.146099],[15.622164,74.147224],[15.624368,74.147747],[15.625164,74.147744],[15.626265,74.147739],[15.627506,74.14771],[15.631061,74.148109],[15.631762,74.147864],[15.633072,74.147327],[15.63368,74.147034],[15.634265,74.146983],[15.635085,74.147222],[15.636915,74.147964],[15.637572,74.148155],[15.639064,74.148563],[15.639696,74.148463],[15.640139,74.14805],[15.640349,74.147759],[15.640793,74.147395],[15.641307,74.147078],[15.642079,74.146809],[15.642687,74.146661],[15.643389,74.146634],[15.652752,74.149772],[15.652879,74.148881],[15.65177,74.140715],[15.650755,74.138048],[15.650215,74.136627],[15.653338,74.122757],[15.653387,74.119792],[15.653654,74.119421],[15.646911,74.119507],[15.644303,74.116454],[15.642281,74.11397],[15.640897,74.113311],[15.6419,74.110947],[15.642031,74.110003],[15.642106,74.108588],[15.641995,74.1077],[15.641372,74.106482],[15.640578,74.104675],[15.637123,74.100888],[15.636339,74.099504],[15.636228,74.098756],[15.636253,74.098367],[15.63652,74.097756],[15.637157,74.096088],[15.636992,74.095145],[15.636575,74.093964],[15.634837,74.092112]]);
	 // SANT ESTEVAM 403106
	 $scope.polyCoords.push([[15.520471,73.935078],[15.520107,73.935521],[15.519193,73.936488],[15.518719,73.937081],[15.518303,73.937753],[15.517906,73.938562],[15.517378,73.939765],[15.516915,73.940338],[15.515853,73.941651],[15.515519,73.942729],[15.515232,73.943507],[15.514896,73.94436],[15.514853,73.945462],[15.514886,73.946154],[15.514907,73.946588],[15.515226,73.947388],[15.515591,73.947662],[15.516393,73.948008],[15.516542,73.948708],[15.516373,73.94891],[15.516156,73.949211],[15.515964,73.949488],[15.515894,73.950164],[15.515871,73.950515],[15.515873,73.95079],[15.516169,73.95179],[15.51697,73.952972],[15.518957,73.951108],[15.519919,73.95088],[15.520161,73.951152],[15.520285,73.951798],[15.520265,73.952594],[15.520269,73.953538],[15.520224,73.954284],[15.520252,73.95508],[15.520521,73.955874],[15.52086,73.956344],[15.521372,73.95751],[15.521615,73.958081],[15.521756,73.958398],[15.521834,73.958577],[15.522708,73.959865],[15.523672,73.960109],[15.525651,73.960919],[15.526037,73.960967],[15.527161,73.962087],[15.527911,73.962903],[15.528516,73.963422],[15.529528,73.963442],[15.531337,73.96398],[15.531891,73.964102],[15.532448,73.964646],[15.53298,73.96529],[15.533537,73.965759],[15.533947,73.965857],[15.53485,73.967071],[15.534944,73.967208],[15.537266,73.965754],[15.539912,73.963483],[15.541984,73.960837],[15.542964,73.958843],[15.54339,73.955035],[15.542631,73.953068],[15.542078,73.951542],[15.5371,73.944674],[15.536178,73.942981],[15.534706,73.94082],[15.533541,73.939128],[15.533107,73.937528],[15.532922,73.936595],[15.530468,73.937768],[15.530072,73.937991],[15.529677,73.93815],[15.529372,73.938215],[15.528187,73.939323],[15.527455,73.939358],[15.526444,73.940117],[15.526231,73.940055],[15.525803,73.9399],[15.525344,73.939682],[15.524793,73.939401],[15.524396,73.939341],[15.524029,73.93909],[15.523291,73.93796],[15.522892,73.937459],[15.522707,73.93705],[15.5224,73.936548],[15.522031,73.936077],[15.520471,73.935078]]);
	 // CUMBHARJUA 403107
	 $scope.polyCoords.push([[15.514886,73.946154],[15.514194,73.946712],[15.513497,73.947271],[15.51307,73.947939],[15.512806,73.948856],[15.512542,73.94994],[15.512357,73.950579],[15.512117,73.951052],[15.511015,73.951252],[15.510477,73.951199],[15.509912,73.951008],[15.50905,73.950846],[15.50819,73.95085],[15.50631,73.951443],[15.506042,73.951638],[15.505264,73.952003],[15.50454,73.952506],[15.502973,73.953438],[15.501853,73.955414],[15.501556,73.955941],[15.50132,73.95636],[15.501082,73.957166],[15.500816,73.957862],[15.500282,73.958725],[15.499163,73.960867],[15.498325,73.961434],[15.496498,73.961609],[15.494711,73.962383],[15.494099,73.961475],[15.493585,73.961025],[15.493121,73.960876],[15.492342,73.960931],[15.491928,73.961109],[15.491345,73.961363],[15.49088,73.961561],[15.490566,73.961694],[15.489715,73.961924],[15.48918,73.962329],[15.488475,73.962534],[15.48551,73.962674],[15.484219,73.962731],[15.483,73.962787],[15.482391,73.96284],[15.481904,73.962868],[15.480386,73.96237],[15.480249,73.962721],[15.479996,73.964029],[15.479917,73.964566],[15.48013,73.965084],[15.480457,73.965568],[15.480637,73.965751],[15.481126,73.966218],[15.481306,73.966535],[15.481454,73.966836],[15.481471,73.967188],[15.48144,73.967423],[15.481247,73.967775],[15.480339,73.967947],[15.479852,73.968134],[15.47927,73.968556],[15.477608,73.970725],[15.476966,73.972336],[15.475918,73.973866],[15.475493,73.974947],[15.475592,73.975432],[15.475627,73.975935],[15.474949,73.976776],[15.474139,73.980599],[15.474145,73.981939],[15.474246,73.98286],[15.474643,73.984612],[15.475103,73.985748],[15.475251,73.986183],[15.4754,73.986836],[15.475402,73.987322],[15.475357,73.988109],[15.475391,73.988478],[15.475344,73.98893],[15.475268,73.989249],[15.475573,73.989193],[15.476034,73.988943],[15.476525,73.989013],[15.476917,73.989021],[15.477628,73.988925],[15.477838,73.98881],[15.479628,73.987632],[15.479829,73.987631],[15.480551,73.987814],[15.482587,73.988446],[15.483179,73.988412],[15.484443,73.988612],[15.485017,73.988972],[15.485415,73.989067],[15.486414,73.989251],[15.486691,73.989179],[15.486655,73.989489],[15.486631,73.991058],[15.48654,73.991738],[15.486212,73.992128],[15.485916,73.992582],[15.485135,73.993201],[15.484854,73.993315],[15.483931,73.993757],[15.483292,73.994585],[15.482762,73.99538],[15.481808,73.995708],[15.481306,73.995678],[15.48031,73.99596],[15.480014,73.99635],[15.479561,73.996594],[15.478917,73.996484],[15.47843,73.996228],[15.477787,73.99615],[15.477254,73.996185],[15.476161,73.997015],[15.475896,73.997405],[15.475492,73.998216],[15.475229,73.999058],[15.474693,74.000321],[15.475319,74.000375],[15.475966,74.001011],[15.476834,74.001058],[15.477279,74.000775],[15.477749,74.000517],[15.479234,73.999972],[15.48025,73.999788],[15.480845,73.999683],[15.481762,73.99973],[15.482458,74.000008],[15.4837,74.000642],[15.484817,74.000969],[15.485265,74.001248],[15.485826,74.001275],[15.486822,74.002268],[15.487814,74.002212],[15.488532,74.00208],[15.489226,74.001975],[15.489697,74.001896],[15.490091,74.001433],[15.490363,74.001125],[15.490634,74.000919],[15.492939,74.000729],[15.494009,74.00144],[15.495153,74.002279],[15.495545,74.002999],[15.496018,74.003355],[15.496787,74.003556],[15.49845,74.003906],[15.499346,74.004257],[15.503799,74.003],[15.506148,74.002591],[15.507571,74.001908],[15.510755,73.99879],[15.511638,73.99799],[15.513129,73.995199],[15.513943,73.993498],[15.514509,73.99406],[15.516037,73.995743],[15.518033,73.997484],[15.518798,73.998385],[15.519856,73.999708],[15.520738,74.00073],[15.52185,74.000906],[15.523255,74.00102],[15.524952,74.001253],[15.526239,74.001307],[15.527528,74.001845],[15.528173,74.002143],[15.529051,74.002381],[15.52999,74.00304],[15.530693,74.003339],[15.530671,74.003752],[15.531561,74.0025],[15.531922,74.001982],[15.532004,74.000012],[15.532316,73.998744],[15.532677,73.998086],[15.533311,73.997755],[15.534081,73.997188],[15.534261,73.996844],[15.534892,73.995637],[15.535341,73.994556],[15.536153,73.993285],[15.537467,73.992388],[15.539323,73.990925],[15.540138,73.990217],[15.54077,73.989323],[15.541312,73.988664],[15.541438,73.986365],[15.541251,73.985194],[15.54043,73.984588],[15.538791,73.983799],[15.538015,73.983099],[15.538009,73.981739],[15.53837,73.981221],[15.539557,73.979272],[15.540782,73.978984],[15.542374,73.979352],[15.544146,73.979155],[15.544177,73.978149],[15.544185,73.977889],[15.544001,73.97728],[15.543265,73.975501],[15.542396,73.974239],[15.541166,73.973542],[15.539983,73.973266],[15.538302,73.973228],[15.535801,73.972724],[15.533108,73.97269],[15.530588,73.972256],[15.532594,73.970861],[15.535742,73.968379],[15.534944,73.967208],[15.53485,73.967071],[15.533947,73.965857],[15.533537,73.965759],[15.53298,73.96529],[15.532448,73.964646],[15.531891,73.964102],[15.531337,73.96398],[15.529528,73.963442],[15.528516,73.963422],[15.527911,73.962903],[15.527161,73.962087],[15.526037,73.960967],[15.525651,73.960919],[15.523672,73.960109],[15.522708,73.959865],[15.521834,73.958577],[15.521756,73.958398],[15.521615,73.958081],[15.521372,73.95751],[15.52086,73.956344],[15.520521,73.955874],[15.520252,73.95508],[15.520224,73.954284],[15.520269,73.953538],[15.520265,73.952594],[15.520285,73.951798],[15.520161,73.951152],[15.519919,73.95088],[15.518957,73.951108],[15.51697,73.952972],[15.516169,73.95179],[15.515873,73.95079],[15.515871,73.950515],[15.515894,73.950164],[15.515964,73.949488],[15.516156,73.949211],[15.516373,73.94891],[15.516542,73.948708],[15.516393,73.948008],[15.515591,73.947662],[15.515226,73.947388],[15.514907,73.946588],[15.514886,73.946154]]);
	 // N B VEREM 403109
	 $scope.polyCoords.push([[15.526155,73.79868],[15.525821,73.799042],[15.522668,73.8004],[15.520879,73.800323],[15.52024,73.801199],[15.519475,73.802055],[15.51876,73.802229],[15.51777,73.802235],[15.516504,73.802072],[15.515348,73.802079],[15.513534,73.802373],[15.512271,73.802664],[15.510954,73.80341],[15.509649,73.805812],[15.50841,73.807591],[15.508414,73.808356],[15.510141,73.810279],[15.510888,73.811201],[15.511397,73.811721],[15.512418,73.812923],[15.512937,73.815094],[15.512588,73.816094],[15.512189,73.816638],[15.511412,73.8176],[15.51085,73.818986],[15.51073,73.819517],[15.510199,73.820103],[15.510317,73.820899],[15.510724,73.822166],[15.510916,73.822372],[15.511478,73.821504],[15.513259,73.819301],[15.514214,73.818209],[15.517841,73.815996],[15.52314,73.810652],[15.524984,73.809314],[15.525651,73.809267],[15.526488,73.809994],[15.527242,73.810549],[15.527867,73.810588],[15.52899,73.810152],[15.529822,73.80976],[15.530278,73.80937],[15.530858,73.808635],[15.531189,73.808074],[15.531767,73.807124],[15.533178,73.805329],[15.533093,73.805204],[15.53209,73.804041],[15.531541,73.802969],[15.531039,73.802317],[15.530037,73.801434],[15.528944,73.800272],[15.52731,73.799627],[15.526155,73.79868]]);
	 // CORLIM IE
	 $scope.polyCoords.push([[15.503509,73.915206],[15.501858,73.915001],[15.500988,73.914325],[15.500161,73.913946],[15.499625,73.914076],[15.499174,73.914504],[15.498805,73.915017],[15.498472,73.914508],[15.498305,73.913998],[15.497767,73.913703],[15.496861,73.914048],[15.49608,73.914734],[15.495175,73.915334],[15.494521,73.916487],[15.494277,73.917255],[15.494237,73.917468],[15.493989,73.917427],[15.493823,73.917257],[15.493202,73.916707],[15.492746,73.916411],[15.492291,73.916201],[15.492002,73.91599],[15.491588,73.915864],[15.49101,73.915654],[15.490391,73.915657],[15.490186,73.915914],[15.490519,73.916636],[15.49164,73.917993],[15.491932,73.918587],[15.49132,73.920038],[15.490659,73.919914],[15.490411,73.919873],[15.488511,73.919329],[15.486983,73.919167],[15.48562,73.918833],[15.484671,73.918795],[15.483516,73.918801],[15.480697,73.918996],[15.477237,73.919367],[15.473074,73.919657],[15.470842,73.919927],[15.47104,73.920438],[15.471713,73.92188],[15.472173,73.92289],[15.472595,73.923231],[15.472756,73.923863],[15.472883,73.924784],[15.472851,73.925381],[15.472458,73.927461],[15.471848,73.927989],[15.470922,73.928644],[15.469999,73.929661],[15.469392,73.930947],[15.469976,73.932436],[15.470434,73.932939],[15.470716,73.933281],[15.470981,73.933696],[15.470812,73.93498],[15.469906,73.936069],[15.468582,73.937612],[15.466437,73.939503],[15.465147,73.940398],[15.464534,73.941239],[15.464121,73.942832],[15.464041,73.9437],[15.463622,73.943847],[15.462585,73.943881],[15.461968,73.943739],[15.460024,73.943662],[15.459574,73.943259],[15.459039,73.942771],[15.458364,73.942398],[15.457775,73.942112],[15.457078,73.941883],[15.456837,73.942069],[15.456334,73.942378],[15.45589,73.942472],[15.455178,73.942475],[15.454496,73.942448],[15.453575,73.942178],[15.452714,73.941968],[15.451795,73.941881],[15.451083,73.941884],[15.450548,73.941755],[15.449344,73.943341],[15.44881,73.944083],[15.448234,73.945332],[15.447166,73.946492],[15.445958,73.946729],[15.445242,73.946733],[15.44327,73.946189],[15.442196,73.946194],[15.441304,73.946845],[15.441218,73.947677],[15.441045,73.948879],[15.441095,73.949987],[15.442268,73.952059],[15.44275,73.953606],[15.443136,73.953432],[15.443761,73.953358],[15.446528,73.953201],[15.448893,73.95344],[15.450564,73.95379],[15.45182,73.954681],[15.453634,73.956071],[15.454506,73.95664],[15.455205,73.957354],[15.455977,73.959072],[15.456508,73.960971],[15.456728,73.963409],[15.457287,73.964016],[15.457916,73.964659],[15.460168,73.965375],[15.461767,73.965331],[15.462358,73.96522],[15.463364,73.964821],[15.464059,73.964745],[15.464689,73.964571],[15.465481,73.963985],[15.467142,73.962255],[15.467901,73.961031],[15.468246,73.960456],[15.468696,73.960166],[15.469251,73.959877],[15.469945,73.959407],[15.470778,73.959295],[15.473072,73.959032],[15.474869,73.959106],[15.476895,73.959825],[15.477972,73.960866],[15.478311,73.961077],[15.480386,73.96237],[15.481904,73.962868],[15.482391,73.96284],[15.483,73.962787],[15.484219,73.962731],[15.48551,73.962674],[15.488475,73.962534],[15.48918,73.962329],[15.489715,73.961924],[15.490566,73.961694],[15.49088,73.961561],[15.491345,73.961363],[15.491928,73.961109],[15.492342,73.960931],[15.493121,73.960876],[15.493585,73.961025],[15.494099,73.961475],[15.494711,73.962383],[15.496498,73.961609],[15.498325,73.961434],[15.499163,73.960867],[15.500282,73.958725],[15.500816,73.957862],[15.501082,73.957166],[15.50132,73.95636],[15.501556,73.955941],[15.501853,73.955414],[15.502973,73.953438],[15.50454,73.952506],[15.505264,73.952003],[15.506042,73.951638],[15.50631,73.951443],[15.50819,73.95085],[15.50905,73.950846],[15.509912,73.951008],[15.510477,73.951199],[15.511015,73.951252],[15.512117,73.951052],[15.512357,73.950579],[15.512542,73.94994],[15.512806,73.948856],[15.51307,73.947939],[15.513497,73.947271],[15.514194,73.946712],[15.514886,73.946154],[15.514853,73.945462],[15.514896,73.94436],[15.515232,73.943507],[15.515519,73.942729],[15.515853,73.941651],[15.516915,73.940338],[15.517378,73.939765],[15.517906,73.938562],[15.518303,73.937753],[15.518719,73.937081],[15.519193,73.936488],[15.520107,73.935521],[15.520471,73.935078],[15.519684,73.934438],[15.517346,73.932108],[15.516328,73.931393],[15.515477,73.930796],[15.510338,73.926859],[15.508116,73.924708],[15.507021,73.923533],[15.506946,73.923453],[15.504545,73.920283],[15.503509,73.915206]]);

	 // Polygon Centers
	 $scope.polyCenters = [
	 {name:'Himayat Nagar', lat:17.4376264, lng:78.4803954},
	 {name:'Panjagutta', lat:17.409548, lng:78.469749},
	 {name:'Bagum Bazar & Sultan Bazar', lat:17.388524, lng:78.473189},
	 {name:'L.B. Nagar / Gaddiannaram & Uppal Kalan & Malakpet ESTEVAM', lat:17.286061, lng:78.486889},
	 {name:'Secunderabad & Kapra & Malkajgiri & Quthbullapur & Alwal', lat:17.416418, lng:78.535540},
	 {name:'Kukatpally & Serilingampally & Ramachandra Puram / Patancheru', lat:17.468631, lng:78.475139},
	 {name:'Mehdipatnam & Rajendra Nagar', lat:17.392388, lng:78.409868}
	 ];*/


    // Center at which map should be focused
    $scope.mapCenter = [28.6228164,77.2028251];



	/*------------------------ Pie-chart on Google map ------------------------*/

    var pieChartType = "PieChart";

    var pieChartOptions = {
        width: 90,
        height: 60,
        chartArea: {left:10,top:10,bottom:0,height:"100%"},
        colorAxis: {colors: ['#aec7e8', '#1f77b4']},
        displayMode: 'regions',
        legend: 'none',
        backgroundColor: 'transparent',
        tooltip: {trigger: 'none'},
        pieSliceBorderColor : 'transparent'
    };

	/*var chart1 = {};
	 chart1.data = [
	 [ 'Aventis Pharma', 20000 ],
	 [ 'Glaxosmithkline', 20000 ],
	 [ 'Surya pharma', 10000 ]
	 ];

	 var chart2 = {};
	 chart2.data = [
	 [ 'Sun Pharma', 30000 ],
	 [ 'Phizer', 20000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];

	 var chart3 = {};
	 chart3.data = [
	 [ 'Sun Pharma', 30000 ],
	 [ 'Phizer', 70000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];

	 var chart4 = {};
	 chart4.data = [
	 [ 'Sun Pharma', 30000 ],
	 [ 'Phizer', 70000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];

	 var chart5 = {};
	 chart5.data = [
	 [ 'Sun Pharma', 20000 ],
	 [ 'Phizer', 50000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];


	 $scope.mapviewData = [
	 { map_chart:chart1, name:'Himayat Nagar', lat:17.4376264, lng:78.4803954},
	 { map_chart:chart2, name:'Panjagutta', lat:17.409548, lng:78.469749},
	 { map_chart:chart3, name:'Bagum Bazar & Sultan Bazar', lat:17.388524, lng:78.473189},
	 { map_chart:chart4, name:'L.B. Nagar / Gaddiannaram & Uppal Kalan & Malakpet ESTEVAM', lat:17.286061, lng:78.486889},
	 { map_chart:chart5, name:'Secunderabad & Kapra & Malkajgiri & Quthbullapur & Alwal', lat:17.416418, lng:78.535540},
	 // { map_chart:chart2, name:'Kukatpally & Serilingampally & Ramachandra Puram / Patancheru', lat:17.468631, lng:78.475139},
	 // { map_chart:chart1, name:'Mehdipatnam & Rajendra Nagar', lat:17.392388, lng:78.409868}
	 ];*/



    // Logic for preparing performance map
    function preparePerformanceMap(chartData){
        if(chartData[0].map_chart.type === undefined){
            $scope.percentArray = ($scope.unitOfMeasures.key === 'value') ? $scope.percentArray_Sales : $scope.percentArray_Units;
            $scope.mapCenter = [chartData[0].lat, chartData[0].lng];
            for(var i = 0; i < chartData.length; i++){
                chartData[i].map_chart.type = pieChartType;
                chartData[i].map_chart.options = angular.copy(pieChartOptions);     // Creates a copy of an Object
                $scope.percentArray.push([]);
                $scope.percentArray[i] = computePercentContribution(chartData[i].map_chart.data);
                chartData[i].map_chart.options.slices = chartData[i].map_chart.data.map(function(item){
                    if(item[2] === null){
                        return {'color':'#AAAAAA'};
                    }
                    return {'color':item[2]};
                });
                chartData[i].map_chart.data.unshift(['Company', 'Sales', 'Color']);
            }
            $scope.pie_charts = chartData;
        }
        else{
            $scope.pie_charts = chartData;
            $scope.percentArray = ($scope.unitOfMeasures.key === 'value') ? $scope.percentArray_Sales : $scope.percentArray_Units;
        }
    }


    // Calculate percent contribution for each element in array
    function computePercentContribution(sourceArray){
        var totalSum = 0;
        for(var i = 0; i < sourceArray.length; i++){
            totalSum += sourceArray[i][1];
        }
        var arr = [];
        arr.push({'name':'Market Value', 'totalValue':totalSum});
        var percent = 0;
        for(var i = 0; i < sourceArray.length; i++){
            percent = ((sourceArray[i][1] / totalSum) *100 ).toFixed(2);
            arr.push({'name':sourceArray[i][0], 'value':sourceArray[i][1], 'percent':percent, 'color':sourceArray[i][2], 'omitted':false});
        }
        return arr;
    }

	/* Pie chart zooming with map zoom */
    NgMap.getMap().then(function(map) {
        var old_current_zoom = null;
        $scope.zoomChanged = function(e) {
            var new_current_zoom = map.getZoom();
            if(new_current_zoom < old_current_zoom){
                pieChartOptions.height =  pieChartOptions.height -  new_current_zoom * 2;
                pieChartOptions.width = pieChartOptions.width -  new_current_zoom * 2;
            }
            else{
                pieChartOptions.height =  pieChartOptions.height +  new_current_zoom * 2;
                pieChartOptions.width = pieChartOptions.width +  new_current_zoom * 2;
            }
            old_current_zoom = new_current_zoom;
        }
    });


    /* Exclude/include a section in Pie chart when the corresponding legend in click */
    $scope.toggleSelectedSection = function(thisObj, index){
		var locVar = thisObj.$parent.$parent.item.map_chart.data[index];
		thisObj.$parent.elem.omitted = !(thisObj.$parent.elem.omitted);
		if(locVar[1] !== null){
			// remove the value at [1] & append it to [0] (i.e. Label) and set [1] to null
			locVar[0] = locVar[0] + '-' + locVar[1];
	        locVar[1] = null;
		}
		else{
			var locArr = locVar[0].split('-');
			// extract numeric value at the end of [0] (i.e. Label) & set it to [1]
			if(locArr.length > 1){
				locVar[1] = Number(locArr.pop());
				locVar[0] = locArr.join('');
			}
		}
	};
	
	/* Npm Cvm on toggle */
	$scope.cvmRadioChanged = function(){
		
		$scope.FetchCompanyPerformanceMap();
	
	};


    /* Assign resize map function to globle rootScope */
    $rootScope.resizeMap = function(){
        NgMap.getMap({id: 'comparison-map'}).then(function(response){
            $timeout(function(){
                google.maps.event.trigger(response, 'resize');
            }, 1000);
        });
	};

  $scope.itemTemplate = $("#template").html();
	$scope.valueTemplate = "<b>{{ dataItem.SearchKey }}</b>";


	/* Delete Confimation modal starts */
		$scope.deleteConfirmationModal = function(recordId){
			console.log(recordId);
				var self = $scope;
				var confirmDeleteModal = $uibModal.open({
						templateUrl:'deleteConfimationModal.html',
						size: 'sm',
						windowTopClass: 'modal-top-class',
						controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
							$scope.message = "Do you want delete the Selected Preset";
								$scope.dismissModal = function(){
										$uibModalInstance.dismiss('dismiss');
								};
								$scope.confirmDelete = function(e){
									self.deleteSelected($uibModalInstance, recordId);
									console.log(recordId);
								};
						}]
				});
		};
		/* Delete Confimation modal ends */

	$scope.deleteSelected = function($uibModalInstance){
		savedSearhcesServices.DeleteSavedSearches($scope.selectedPreset)
		.then(function (response) {
				if (response.data != null && response.status === 200) {
				if(response.data){
					 $scope.msg ="delete";
					 $uibModalInstance.close('close');
					 showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'LoadPresets()'});
				 }
			 else{
				 showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			 }

		 } else {}
	 },
	 function (rejection) { });
	}

  // Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
	 // Input radio-group visual controls
    $('.radio-group label').on('click', function(){
        $(this).removeClass('not-active').siblings().addClass('not-active');
		});

}]);
