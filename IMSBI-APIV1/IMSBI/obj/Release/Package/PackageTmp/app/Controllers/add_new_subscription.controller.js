angular.module("imsApp")
.controller("AddNewSubscriptionController", ['$scope', '$state', '$stateParams', '$uibModal', 'companyServices', 'userServices',
										function($scope, $state, $stateParams, $uibModal, companyServices, userServices){
	
	console.log('Add New Subscription controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		$scope.subscription = {};
		$scope.subscription.ColorCode = '#8C4299';
		$scope.subscription_periods = [
			{ id: 1, name: "Quaterly" },
			{ id: 2, name: "Monthly" },
			{ id: 3, name: "Half Yearly" },
			{ id: 4, name: "Yearly" }
		];
		$scope.subscription.period = 1;
		$scope.subscription.active = 1;
	}
	
	
	/* State Paramas */
	if($stateParams.subscriptionId !== ''){
		fetchSubscriptionInfoById($stateParams.subscriptionId);
		$scope.subscription.isUpdate = 1;
	}
	else{
		$scope.subscription.isUpdate = 0;
		fetchSubscriptionLocations();
	}
	
	
	/* Fetch Subscription Info by Id */
	function fetchSubscriptionInfoById(subscriptionId) {
		companyServices.GetSubscriptionTypeDetails(subscriptionId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.subscription = response.data[0];
				applySubscriptionInfo();
			} else {
				$scope.subscription = false;
			}
		},
		function (rejection) {});
	}
	
	
	function applySubscriptionInfo() {
		fetchSubscriptionLocations();
	}
	
	
	/* Fetch Subscription Locations */
	function fetchSubscriptionLocations() {
		userServices.GetAllCities()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.subLocations = response.data;
				if($stateParams.subscriptionId !== ''){
					$scope.subscription.Gelocation = $scope.subscription.Gelocation.split(',').map(Number);
				}
			} else {
				$scope.subLocations = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* Save Subscription Data */
	$scope.addNewSubscription = function() {
		$scope.subscription.Gelocation = $scope.subscription.Gelocation.toString();
		companyServices.AddUpdateSubscribedType($scope.subscription)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('super.subscriptions');
		    } else {
		      	 
		    }
		},
		function (rejection) { });
	};
	
}]);
