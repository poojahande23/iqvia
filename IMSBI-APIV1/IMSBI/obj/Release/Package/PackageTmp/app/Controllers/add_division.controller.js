angular.module("imsApp")
.controller("AddDivisionController", ['$scope', '$state', '$stateParams', 'companyServices', '$window',
							function($scope, $state, $stateParams, companyServices, $window){
	
	console.log('Add Division controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.Action = true;
	$scope.division = {};
	$scope.division.CompanyID = $scope.sessionInfo.CompanyID;
	
	
	if($stateParams.id !== ""){
		$scope.division.DivisionID = $stateParams.id;
    	companyServices.GetDivisionInfo($scope.division.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.division = response.data;
			} else {
				$scope.division = false;
			}
		},
		function (rejection) {});
   	};
	
	$scope.saveFormData = function(){
		companyServices.AddUpdateCompanyDivision(JSON.stringify($scope.division))
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('admin.divisions');
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
		
}]);
