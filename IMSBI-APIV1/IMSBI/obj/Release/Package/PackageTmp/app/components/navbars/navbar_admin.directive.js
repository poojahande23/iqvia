'use strict';

angular.module('imsApp')
	.directive('navbaradmin', [function(){
		var directiveObj = {};
		directiveObj.templateUrl = 'app/components/navbars/navbar_admin.html';
		directiveObj.restrict = 'A';
		directiveObj.controller = 'NavbarAdminController';
		directiveObj.controllerAs = 'nav';
		
		return directiveObj;
	}]);
