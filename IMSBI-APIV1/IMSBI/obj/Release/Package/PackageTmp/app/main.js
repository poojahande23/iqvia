angular.module("imsApp")
.controller("LayoutUserController", ['$scope', '$state', '$window', function($scope, $state, $window){
	console.log('Layout User controller...', $state);
	if($state.current.name === 'layout'){
		// $state.go('.analysis');
	}
	
}]);

'use strict';

(function(){
	function NavbarUserController ($timeout, $scope, $rootScope, $window, $state, $uibModal, adminServices, notify, accountServices) {
		
		/* Parse session data to object format */
		function sessionInfoToScope(){
			$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
			console.log('$scope.sessionInfo: ',$scope.sessionInfo);
		}
		sessionInfoToScope();
		
		
		/* Handle an issued event */
		$rootScope.$on('UpdateNavbarSessionInfo', function(){
			sessionInfoToScope();
		});
		
		
		/* copied from app.js */
		// Left mobile menu
		angular.element('.hamburger').click(function(){
			if (angular.element('body').hasClass('menu-left-opened')) {
				angular.element(this).removeClass('is-active');
				angular.element('body').removeClass('menu-left-opened');
				angular.element('html').css('overflow','auto');
			} else {
				angular.element(this).addClass('is-active');
				angular.element('body').addClass('menu-left-opened');
				angular.element('html').css('overflow','hidden');
			}
		});
	
		angular.element('.mobile-menu-left-overlay').click(function(){
			angular.element('.hamburger').removeClass('is-active');
			angular.element('body').removeClass('menu-left-opened');
			angular.element('html').css('overflow','auto');
		});
		
		// Right mobile menu
		angular.element('.site-header .burger-right').click(function(){
			if (angular.element('body').hasClass('menu-right-opened')) {
				angular.element('body').removeClass('menu-right-opened');
				angular.element('html').css('overflow','auto');
			} else {
				angular.element('.hamburger').removeClass('is-active');
				angular.element('body').removeClass('menu-left-opened');
				angular.element('body').addClass('menu-right-opened');
				angular.element('html').css('overflow','hidden');
			}
		});
		
		angular.element('.mobile-menu-right-overlay').click(function(){
			angular.element('body').removeClass('menu-right-opened');
			angular.element('html').css('overflow','auto');
		});
		/* copied from app.js */
		
        
		/* Logout Action */
		$scope.logoutSession = function(){
            accountServices.Logout(JSON.parse($window.localStorage.BI_Tool_Info).SessionID)
            .then(function(response) {}, function(rejection) {});
		};
		
		
		/* Change Password modal starts */
	    $scope.openConfirmationModal = function(){
	        var self = $scope;
	        var changePassword = $uibModal.open({
	            templateUrl:'changePasswordModal.html',
	            size: 'md',
	            windowTopClass: 'modal-top-class',
	            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
	            	$scope.passwords = {};
	            	// $scope.passwords.OldPassword = '';
	            	$scope.passwords.UserID = self.sessionInfo.UserID;
	            	$scope.showError = false;
	                $scope.closeModal = function(){
	                    $uibModalInstance.close('close');
	                };
	                $scope.dismissModal = function(){
	                    $uibModalInstance.dismiss('dismiss');
	                };
	                $scope.setNewPassword = function(e){
	                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
	                		$scope.showError = true;
	                	}
	                	else{
	                		$scope.showError = false;
	                		self.setPassword($scope.passwords, $uibModalInstance);
	                	}
	                };
	            }]
	        });
	    };
	    /* Change Password modal ends */
	   
	   	// Set Password
		$scope.setPassword = function(passwords, $uibModalInstance){
			adminServices.ChangePassword(passwords)
			.then(function (response) {
			    if (response.data != null && response.status === 200) {
			    	if(response.data === "updated."){
			    		$uibModalInstance.close('close');
			    		showNotification('Password Updated Successfully', '', '$scope.logoutSession()');
			    	}
				    else{
				    	showNotification(response.data, 'notify-bg', '');
				    }
				        
			    } else {
			       
			    }
			},
			function (rejection) { });
		};
		
		
		// Show Notification
		function showNotification(message, noteClass, routine){
			noteClass = noteClass || '';
			notify({
	            message: message,
	            classes: noteClass,
	            templateUrl: $scope.template,
	            position: 'center',
	            duration: 2000
	        });
	        eval(routine);
		}
		
	}
	
	angular.module('imsApp')
	  .controller('NavbarUserController', NavbarUserController);
})();

'use strict';

angular.module('imsApp')
	.directive('navbaruser', [function(){
		var directiveObj = {};
		directiveObj.templateUrl = 'app/components/navbars/navbar_user.html';
		directiveObj.restrict = 'A';
		directiveObj.controller = 'NavbarUserController';
		directiveObj.controllerAs = 'nav';
		
		return directiveObj;
	}]);

angular.module("imsApp")
.controller("AnalysisController", ['$scope', '$state', 'userServices', 'companyServices', 'analysisServices', '$http', 'savedSearhcesServices', 'notify', '$window', '$document', '$rootScope', '$timeout',
							function($scope, $state, userServices, companyServices, analysisServices, $http, savedSearhcesServices, notify, $window, $document, $rootScope, $timeout){
	console.log('Analysis controller...', $state);

	/* Make $rootScope available in Local $scope */
	$scope.rootScope = $rootScope;

	/* Parse session data to object format */
	function sessionInfoToScope(){
		$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	}
	sessionInfoToScope();

	/* Static Data & Values */
	$scope.periodTypeColl = [
		{type:"MAT", value:'mat', active:false},
		{type:"MTH", value:'mth', active:true},
		{type:"YTD-Finance", value:'ytdf', active:false},
		{type:"YTD-Calendar", value:'ytdc', active:false}
	];

	$scope.categoryColl = [
		{name:"SKU", val:'sku'},
		{name:"Brand", val:'brand'},
		{name:"Brand Group", val:'brandgp'},
		{name:"Focus Brand", val:'focusbr'},
		{name:"New Introduction", val:'newintro'}
	];

	$scope.indexMatrix=[
		{matrix:"KPI Matrix"},
		{matrix:"Performance Slope"}
	];
	$scope.indexMatrix1=[
		{matrix1:"Product"},
		{matrix1:"Geography"},
		{matrix1:"Manufacturers"}
	];
	$scope.indexMatrix2=[
		{matrix2:"Market Share %"},
		{matrix2:"Sales Growth"},
		{matrix2:"Growth % vs 3M"},
	];
	$scope.indexMatrix3=[
		{matrix3:"Trend"},
		{matrix3:"Year on year"},
		{matrix3:"Z chart"}
	];


	/* Initialize Models & Variables */
	initVariables();
	function initVariables() {
        showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
        hideLoader = "angular.element('.loader-backdrop').fadeOut();";
		BarBgColor = '#B7E0EE';
		maxY_bar_Value = 0, maxY_line_Value = 0, maxY_bar_Unit = 0, maxY_line_Unit = 0;

        $scope.fetchData = {};
        $scope.fetchData.CompanyID = $scope.sessionInfo.CompanyID;
        $scope.fetchData.GeographyFilter = 'city';
        if($scope.sessionInfo.PeriodType === undefined){
            $scope.fetchData.PeriodType = 'mth';
            updateSessionInfo(undefined, 'mth');
        }
        else{
            $scope.fetchData.PeriodType = $scope.sessionInfo.PeriodType;
        }
        $scope.fetchData.CategoryType = 'brand';
        $scope.marketSalesRadio = 'overallSales';
        $scope.selectedPatchId = undefined;
        $scope.fetchData.TCCode = 2;
        $scope.fetchData.TCValue = 'all';
        $scope.userId = $scope.sessionInfo.UserID;
        $scope.tabType = 1;
        $scope.isCluster = false;
        $scope.fetchData.includeTopSearches = 1;
        $scope.filteredCategoryValues = [];
        $scope.isPreset = false;
        $scope.RegExNumForm = /(\d)(?=(\d{2})+\d\.)/g;
        // Unit measures dropdown
        $scope.unitOfMeasuresColls = [
                                        {key:'value', label:'Values', divisor:1, unit:'', decimal:0},
                                        {key:'value', label:'Values(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'value', label:'Values(Cr)', divisor:10000000, unit:'Cr', decimal:2},
                                        {key:'unit', label:'Units', divisor:1, unit:'', decimal:0},
                                        {key:'unit', label:'Units(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'unit', label:'Units(Cr)', divisor:10000000, unit:'Cr', decimal:2}
                                    ];
        $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
        $scope.UOM_set_flag = false;
        $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[0];
        $scope.UOM_MS_set_flag = false;
    }

	/* Edit Session info */
	function updateSessionInfo(GeographyID, PeriodType){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		if(GeographyID !== undefined){ sessionInfo.GeographyID = GeographyID; }
		if(PeriodType !== undefined){ sessionInfo.PeriodType = PeriodType; }
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
	}

	/* Divisions
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.fetchData.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.fetchData.DivisonID = $scope.divisionColl[0].DivisionID;
				$scope.FetchGeographyValues();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();*/

	/* Geography */
	$scope.FetchGeographyValues = function (presetData) {
		userServices.GetGeographyValues($scope.fetchData.GeographyFilter, $scope.fetchData.CompanyID, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.geographyColl = response.data;
				if($scope.sessionInfo.GeographyID === undefined){
					$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
					updateSessionInfo($scope.geographyColl[0].GeographyID, undefined);
				}
				else{
					$scope.fetchData.GeographyID = $scope.sessionInfo.GeographyID;
				}
				$scope.FetchBricksByGeography(presetData);
			} else {
				$scope.geographyColl = false;
			}
		},
		function (rejection) {});
	};

	/* Division to which user's role is associated */
	$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
	$scope.FetchGeographyValues();


	/* Geography selected */
	$scope.geographySelected = function(){
		updateSessionInfo($scope.fetchData.GeographyID, undefined);
        $scope.FetchTimePeriods();
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				$scope.selectedPatches = [];
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
		},
		function (rejection) {});
	};


	/* Bricks */
	$scope.FetchBricksByGeography = function (presetData) {
		if(presetData !== undefined){
			$scope.fetchData.GeographyID = JSON.parse(presetData).GeographyID;
		}
        $scope.FetchTimePeriods();
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
			// Fetch values for first category by default
			if(presetData !== undefined){
				$scope.isPreset = true;
				$scope.FetchCategoryValues('$scope.displayResult('+presetData+')');
			}
			else{
				$scope.isPreset = false;
				$scope.FetchCategoryValues('$scope.displayResult()');
			}
		},
		function (rejection) {});
	};


	/* Time Periods */
	$scope.FetchTimePeriods = function () {
		$scope.period = {};
		userServices.GetTimePeriods($scope.fetchData.GeographyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.timePeriods = response.data;
				$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
				$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
			} else {
				$scope.timePeriods = false;
			}
		},
		function (rejection) {});
	};

	/* Period Type changed or Selected */
	$scope.periodTypeSelected = function(event) {
		if(!event.dataItem.active){
			event.preventDefault();
		}
	};

	$scope.periodTypeChanged = function(){
		updateSessionInfo(undefined, $scope.fetchData.PeriodType);
	};

	/* Validating the time selected */
	$scope.timeSelected = function(event){
		if($scope.period.startPeriod > $scope.period.endPeriod){
			$scope.period.endPeriod = $scope.timePeriods[0].TimePeriodID;
			$scope.period.startPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		}
	};


	/* Fetch Category Values */
	$scope.FetchCategoryValues = function (customRoutine) {
		$scope.cats = {};
		$scope.cats.selectedCatValues = [];
		$scope.filteredCategoryValues = [];
		userServices.GetCategoryValues($scope.fetchData.CategoryType, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.categoryValues = response.data;
				if($scope.isPreset){
					$scope.filteredCategoryValues = $scope.categoryValues;
				}
				eval(customRoutine);
			} else {
				$scope.categoryValues = false;
			}
		},
		function (rejection) {});
	};


	/* Filter Category values array */
	$scope.filterCategoryValues = function(searchQuery) {
		if(searchQuery !== ''){
			var regExpression = new RegExp(searchQuery, 'i');
			$scope.filteredCategoryValues = $scope.categoryValues.filter(function(item){ return regExpression.test(item.CategoryValue); });
		}
	};


	$scope.displayResult = function (presetData) {
		if(presetData !== undefined){
			applyPresetData(presetData);
		}
		else{
			if($scope.fetchData.PeriodType === 'mth'){
				$scope.fetchData.PeriodStart = $scope.period.startPeriod;
				$scope.fetchData.PeriodEnd = $scope.period.endPeriod;
			}
			else{
				$scope.fetchData.PeriodStart = 0;
				$scope.fetchData.PeriodEnd = 0;
			}


			if(($scope.cats.selectedCatValues !== undefined) && ($scope.cats.selectedCatValues.length > 0) && ($scope.cats.selectedCatValues !== 'all')){
			    $scope.fetchData.CategoryValue = $scope.cats.selectedCatValues.toString();
			}
			else{
				$scope.fetchData.CategoryValue = 'all';
				$scope.fetchData.includeTopSearches = 1;
			}


			if(($scope.selectedPatches !== undefined) && ($scope.selectedPatches.length > 0)){
				$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
			}
			else{
				$scope.fetchData.PatchIds = 'all';
			}

			$scope.selectedPatchId = undefined;
			$scope.fetchData.TCValue = 'all';
			fetchPatchesByCluster();
		}
	};

	// Get Patches by Clusters
	function fetchPatchesByCluster(){
		if($scope.isCluster && ($scope.fetchData.PatchIds !== 'all')){
			userServices.GetPatchIDsByClusterIDs($scope.fetchData.PatchIds, showLoader, hideLoader)
			.then(function (response) {
				if (response.data != null && response.status === 200) {
					$scope.fetchData.PatchIds = response.data;
					// Fetch Final Result
					if($scope.tabPanelState === 1){
						fetchAnalysisDashboardData();
					}
					else if($scope.tabPanelState === 2){
						fetchAnalysisMarketShareData();
					}
				} else {
					$scope.fetchData.PatchIds = '';
				}
			},
			function (rejection) {});
		}
		else{
			// Fetch Final Result
			if($scope.tabPanelState === 1){
				fetchAnalysisDashboardData();
			}
			else if($scope.tabPanelState === 2){
				fetchAnalysisMarketShareData();
			}
		}
	}


	/* Fetch Analysis Dashboard Data */
	function fetchAnalysisDashboardData () {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
		analysisServices.GetAnalysisDashboardData($scope.fetchData)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
				$scope.analysisDashboardData = response.data;
				// Split Sales by Period chart
				initLineBarChart();
				// Split Sales by Patch chart
				initMultiBarChart();

                resetUOM_MinValueLength($scope.analysisDashboardData.BarChartSales);
				$scope.unitOfMeasuresChanged();
				$scope.patchList = $scope.analysisDashboardData.PatchList;
			} else {
				$scope.analysisDashboardData = false;
			}
		},
		function (rejection) {});
	}


	/* Fetch Analysis Market Share Data */
	function fetchAnalysisMarketShareData() {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
		analysisServices.GetAnalysisMarketShareData($scope.fetchData)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
			    $scope.analysisMarketShareData = response.data;
			    $scope.TherapyList = $scope.analysisMarketShareData.TherapyList;

                resetUOM_MS_MinValueLength($scope.analysisMarketShareData.AnalysisMarketShareProductList);
			    $scope.unitOfMeasuresChanged_MS();
			} else {
				$scope.analysisMarketShareData = false;
			}
		},
		function (rejection) {});
	}


	/* Select Preset functionality Begins */
	$scope.savePreset = function(){
		savedSearhcesServices.AddUpdateSavedSearches(preparePresetData())
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
				initializeOnPreset();
			    notify({
		            message: 'Presets saved Successfully',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			} else {

			}
		},
		function (rejection) {});
	};


	function preparePresetData () {
		var presetObject = {};
		presetObject.SearchKey = $scope.PresetName;
		presetObject.UserID = $scope.userId;
		presetObject.TabType = $scope.tabType;

		if($scope.fetchData.PeriodType === 'mth'){
			$scope.fetchData.PeriodStart = $scope.period.startPeriod;
			$scope.fetchData.PeriodEnd = $scope.period.endPeriod;
		}
		else{
			$scope.fetchData.PeriodStart = 0;
			$scope.fetchData.PeriodEnd = 0;
		}

		if(($scope.selectedPatches !== undefined) && ($scope.selectedPatches.length > 0)){
			$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
		}
		else{
			$scope.fetchData.PatchIds = 'all';
		}

		if(($scope.cats.selectedCatValues !== undefined) && ($scope.cats.selectedCatValues.length > 0) && ($scope.cats.selectedCatValues !== 'all')){
		    $scope.fetchData.CategoryValue = $scope.cats.selectedCatValues.toString();
		}
		else{
			$scope.fetchData.CategoryValue = 'all';
			$scope.fetchData.includeTopSearches = 1;
		}

        // Remove SessionID before saving preset
        if($scope.fetchData.SessionID !== undefined){
            delete $scope.fetchData.SessionID;
        }
		presetObject.SearchData = JSON.stringify($scope.fetchData);

		return presetObject;
	}


	/* Get Presets */
	function LoadPresets(){
		savedSearhcesServices.GetSavedSearchesbyUser($scope.userId, $scope.tabType)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.presetsColl = response.data;
			} else {
				$scope.presetsColl = false;
			}
		},
		function (rejection) {});
	}
	LoadPresets();

	$scope.applySelectedPreset = function() {
		angular.element('.loader-backdrop').fadeIn();
		var presetData = $scope.presetsColl.filter(function(item){ return (item.SearchKeyID === $scope.selectedPreset); })[0].SearchData;
		var presetData_obj = JSON.parse(presetData);
		$scope.fetchData.GeographyFilter = presetData_obj.GeographyFilter;
		$scope.fetchData.DivisonID = presetData_obj.DivisonID;
		$scope.fetchData.CategoryType = presetData_obj.CategoryType;
		$scope.FetchGeographyValues(presetData);
	};


	// Apply Preset data to fetchData
	function applyPresetData(presetData){
		$scope.fetchData = presetData;
		updateSessionInfo($scope.fetchData.GeographyID, $scope.fetchData.PeriodType);

		if($scope.fetchData.PeriodStart !== 0){
			$scope.period.startPeriod = $scope.fetchData.PeriodStart;
			$scope.period.endPeriod = $scope.fetchData.PeriodEnd;
		}

		if($scope.fetchData.PatchIds !== 'all'){
			$scope.selectedPatches = $scope.fetchData.PatchIds.split(',').map(Number);
		}
		else{
			$scope.selectedPatches = [];
		}

		if($scope.fetchData.CategoryValue !== 'all'){
		     var selectedCatValues_local = $scope.fetchData.CategoryValue.split(',');
		     $scope.cats.selectedCatValues = selectedCatValues_local.map(function(item){ return Number(item); });
		}
		else{
			$scope.cats.selectedCatValues = [];
		}
		angular.element('.loader-backdrop').fadeOut();
	}


	// Initialize filter choices on preset
	function initializeOnPreset(){
		LoadPresets();
		$scope.PresetName = '';
		$scope.SavePresetFlag = false;
		$scope.fetchData.GeographyFilter = 'city';
		$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
		$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
		$scope.selectedPatches = [];
		$scope.fetchData.PeriodType = $scope.sessionInfo.PeriodType;
		$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
		$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		$scope.fetchData.CategoryType = 'brand';
		$scope.cats.selectedCatValues = [];
		$scope.fetchData.includeTopSearches = 1;
		$scope.filteredCategoryValues = [];
		$scope.isPreset = false;
	}
	/* Select Preset functionality Ends */



	/* Graphs & Charts start */

	$scope.salesRadioChanged = function(){
		if($scope.marketSalesRadio === 'overallSales'){
			$timeout(function(){
				$scope.apiLineBar.refresh();
			}, 100);
		}
		else if($scope.marketSalesRadio === 'productSales'){
			$timeout(function(){
				$scope.apiMultiBar.refresh();
			}, 100);
		}
	};


	/* Structure for Top Products table */
	$scope.gridOptions = {
		columns: $scope.topProductsColl,
		dataSource: $scope.topProducts,
		// toolbar: ["excel"],
		excel: {
			fileName: "Market Sales Potential.xlsx"
		}
	};

	var topProductsCollBluePrint = [
	  { field: "Rank", title:"Rank", width:"50px", headerTemplate:"Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>"},
	  { field: "", title:"Brand", width:"150px", template:"#:Brand#" }
	];

	// Dynamically add columns to data grid
	function alterProductListGridStructure(firstProd) {
		$scope.topProductsColl = topProductsCollBluePrint.slice();
		var measure = ($scope.unitOfMeasures.key === 'value') ? 'Value' : 'Unit';

		for(var i = 0; i < firstProd.MonthData.length; i++){
			$scope.topProductsColl.push({ field: "MonthData["+i+"]."+measure, title: "", headerTemplate: firstProd.MonthData[i].MonthName +$scope.figureQualifier+ " <span class='k-icon k-i-sort-desc'></span>",
				template: "#= MonthData["+i+"] !== undefined ? kendo.toString(MonthData["+i+"]."+measure+"/"+$scope.unitOfMeasures.divisor+", 'n"+$scope.unitOfMeasures.decimal+"') : 0#", width: "130px" });
		}
	}


    /* Reset UOM based on Min value length */
    function resetUOM_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_set_flag)){
            var formattedArray = [], minVal = 0, min_local = 0, minLen = 1;
            for(var i = 0; i < data.length; i++){
                // Find min from multidimensional array
                formattedArray = data[i].values.filter(function(item){ return (item[1] >= 1); }).map(function(item){ return (item[1]); });
                min_local = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
                if(i === 0){ minVal = min_local; }
                if(min_local < minVal){
                    minVal = min_local;
                }
            }
            minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[2];
            }
        }
    }


	/* Action of Sales Radio changed */
	$scope.unitOfMeasuresChanged = function(inputObj){
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

		if($scope.analysisDashboardData !== undefined){
			// Prepare a Figure Qualifier
	    	$scope.figureQualifier = ($scope.unitOfMeasures.unit !== '') ? (" ("+$scope.unitOfMeasures.unit+")") : '';
			var LineBarData = [];
			var maxObj = {};

			if($scope.unitOfMeasures.key === 'value'){
			    // Line bar chart
			    if($scope.analysisDashboardData.BarChartSales[0].originalKey === undefined){
			    	LineBarData = $scope.analysisDashboardData.BarChartSales;
				    LineBarData[0].bar = true;
				    maxObj =  dateToMilliseconds(LineBarData);
					maxY_bar_Value = maxObj.maxBar;
					maxY_line_Value = maxObj.maxLine;
				    $scope.line_bar.data = LineBarData;
				    $scope.line_bar.data.map(function(series) {
						                series.values = series.values.map(function(d) { return {x: d[0], y: d[1]} });
						                return series;
						            });
			    }
			    else{
			    	$scope.line_bar.data = $scope.analysisDashboardData.BarChartSales;
			    }

			    // Force Y axis range to certain max values
				if(($scope.analysisDashboardData.BarChartSales[0].disabled === undefined) || !($scope.analysisDashboardData.BarChartSales[0].disabled)){
					$scope.line_bar.options.chart.forceY = [0, maxY_bar_Value];
				}
				else{
					$scope.line_bar.options.chart.forceY = [0, maxY_line_Value];
				}
				$scope.line_bar.options.chart.y1Axis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiLineBar.update();

	         	// Multi-bar chart & Data Grid
	         	if($scope.selectedPatchId === undefined){
		         	$scope.multibar.data = $scope.analysisDashboardData.GroupChartSales;
		         	$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
		         	$scope.apiMultiBar.update();

                    // Product List Table
                    alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductList[0]);
                    $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductList;
		        }
		        else{
		        	mutateGroupChartForSales();
                    productListByPatchForSales();
		        }
			}
			else{
			    // Line bar chart
			    if($scope.analysisDashboardData.BarChartUnit[0].originalKey === undefined){
				    LineBarData = $scope.analysisDashboardData.BarChartUnit;
				    LineBarData[0].bar = true;
					maxObj =  dateToMilliseconds(LineBarData);
					maxY_bar_Unit = maxObj.maxBar;
					maxY_line_Unit = maxObj.maxLine;
					$scope.line_bar.data = LineBarData;
					$scope.line_bar.data.map(function(series) {
						                series.values = series.values.map(function(d) { return {x: d[0], y: d[1]} });
						                return series;
						            });
				}
				else{
			    	$scope.line_bar.data = $scope.analysisDashboardData.BarChartUnit;
			    }

			    // Force Y axis range to certain max values
				if(($scope.analysisDashboardData.BarChartUnit[0].disabled === undefined) || !($scope.analysisDashboardData.BarChartUnit[0].disabled)){
					$scope.line_bar.options.chart.forceY = [0, maxY_bar_Unit];
				}
				else{
					$scope.line_bar.options.chart.forceY = [0, maxY_line_Unit];
				}
				$scope.line_bar.options.chart.y1Axis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiLineBar.update();

				// Multi-bar chart
				if($scope.selectedPatchId === undefined){
					$scope.multibar.data = $scope.analysisDashboardData.GroupChartUnit;
		         	$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		         	$scope.apiMultiBar.update();

                    // Product List Table
                    alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductList[0]);
                    $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductList;
				}
		        else{
		        	mutateGroupChartForUnits();
                    productListByPatchForSales();
		        }
			}
		}
	};


	/*--------------------- Divide a number(n) by (d) and round upto (x) decimal places ---------------------*/
	$scope.DivideAndRound = function(n, d, x) {
		if(x === 0){
			n = (n/d).toFixed(1);
			n = (n.replace($scope.RegExNumForm, '$1,'));
			n = n.substring(0, (n.length-2));
		}
		else{
			n = (n/d).toFixed(x);
			n = (n.replace($scope.RegExNumForm, '$1,'));
		}
		return n;
	}


	// Convert Periods into Milliseconds since ?/?/1970
	function dateToMilliseconds(lineBarData){
		var output = {maxBar:0, maxLine:0};
		var max_local = 0;
		for(var i = 0; i < lineBarData.length; i++){
			if(i === 0){
				// Find max from multidimensional array
	        	output.maxBar = lineBarData[i].values.reduce(function(accu,item){ return ((accu[1] > item[1]) ? accu : item); })[1];
			}
			else{
				max_local = lineBarData[i].values.reduce(function(accu,item){ return ((accu[1] > item[1]) ? accu : item); })[1];
				if(output.maxLine < max_local){
					output.maxLine = max_local;
				}
			}

			for(var j = 0; j < lineBarData[i].values.length; j++){
				lineBarData[i].values[j][0] = new Date(lineBarData[i].values[j][0]).getTime();
			}
		}
		return output;
	}


	/* Patch Id selected */
	$scope.patchIdSelected = function(event){
		if(event.item === undefined){
			$scope.selectedPatchId = undefined;
			if($scope.unitOfMeasures.key === 'value'){
				$scope.multibar.data = $scope.analysisDashboardData.GroupChartSales;
		     	$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
		     	$scope.apiMultiBar.update();
			}
			else{
				$scope.multibar.data = $scope.analysisDashboardData.GroupChartUnit;
		     	$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		     	$scope.apiMultiBar.update();
			}
            // Product List Table
            alterProductListGridStructure($scope.analysisDashboardData.GetAnalysisProductList[0]);
            $scope.topProducts = $scope.analysisDashboardData.GetAnalysisProductList;
		}
		else{
			$scope.selectedPatchId = event.item.PatchID;
			if($scope.unitOfMeasures.key === 'value'){
				mutateGroupChartForSales();
			}
			else{
				mutateGroupChartForUnits();
			}
            productListByPatchForSales();
		}
	};

	/* Mutate "GrouChartSalesPatch" array for Sales data */
	function mutateGroupChartForSales(){
		$scope.multibar.data = $scope.analysisDashboardData.GrouChartSalesPatch
								.filter(function(item){
									return (item.PatchID === $scope.selectedPatchId);
								})
								.map(function(item){
									return (item.GroupChartSales);
								});
     	$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
     	$scope.apiMultiBar.update();
	}

	/* Mutate "GrouChartSalesPatch" array for Units data */
	function mutateGroupChartForUnits(){
		$scope.multibar.data = $scope.analysisDashboardData.GrouChartSalesPatch
								.filter(function(item){
									return (item.PatchID === $scope.selectedPatchId);
								})
								.map(function(item){
									return (item.GroupChartUnit);
								});
     	$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
     	$scope.apiMultiBar.update();
	}


	/* For Table in Market Sales Potential */
	function productListByPatchForSales(){
		var list_local = $scope.analysisDashboardData.AnalysisProductListPatch
			.filter(function(item){
				return (item.PatchID === $scope.selectedPatchId);
			});
		if(list_local.length > 0){
            alterProductListGridStructure(list_local[0].ProductList[0]);
            $scope.topProducts = list_local[0].ProductList;
		}
		else{
            $scope.topProducts = [];
		}
	}


	/* Therapy Id selected */
	$scope.TherapyIdSelected = function(event) {
		if(event.item === undefined){
			$scope.fetchData.TCValue = 'all';
		}
		else{
			$scope.fetchData.TCValue = event.item.TherapyID;
		}
        fetchAnalysisMarketShareData();
	};


    /* Reset UOM_MS based on Min value length */
    function resetUOM_MS_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_MS_set_flag)){
            var formattedArray = data.filter(function(item){ return (item.MarketValue >= 1); }).map(function(item){ return (item.MarketValue); });
            var minVal = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
            var minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures_MS = $scope.unitOfMeasuresColls[2];
            }
        }
    }


	/* Unit of Measures Changed in Market Share */
	$scope.unitOfMeasuresChanged_MS = function(inputObj) {
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

		// Prepare a Figure Qualifier
    	$scope.figureQualifier_2 = ($scope.unitOfMeasures_MS.unit !== '') ? (" ("+$scope.unitOfMeasures_MS.unit+")") : '';

		if($scope.unitOfMeasures_MS.key === 'value'){
			// prepareStackedAreaChart($scope.analysisMarketShareData.AnalysisMarketShareGraphData);
            prepareMultiBarChart_2($scope.analysisMarketShareData.AnalysisMarketShareGraphData);

            // Data Grid formation
			prepareGrid_MSV();
		}
		else{
			// prepareStackedAreaChart($scope.analysisMarketShareData.AnalysisMarketShareGraphDataUnit);
            prepareMultiBarChart_2($scope.analysisMarketShareData.AnalysisMarketShareGraphDataUnit);

            // Data Grid formation
			prepareGrid_MSU();
		}
		$scope.topProducts2 = $scope.analysisMarketShareData.AnalysisMarketShareProductList;
	};


	/* Column structure for Top Products table in Market Share */
	function prepareGrid_MSV(){
		$scope.topProductsColl2 = [
		  { field: "Rank", title:"Rank", width:"50px", headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width:"55px"},
		  { field: "", title:"Brand", template: "#:Brand#", width:"150px" },
		  { field: "", title:"TC Code", template: "#:TCCode#", width:"70px" },
		  { field: "TotalSale", title: "Value", headerTemplate: "Value "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(TotalSale/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketValue", title: "Market Value", headerTemplate: "Market Value * "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(MarketValue/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketShare", title: "% Market Share", headerTemplate: "% Market Share <span class='k-icon k-i-sort-desc'></span>",
		  	template: "#=kendo.toString(MarketShare, 'n')#", width: "130px" }
		];
	}

	function prepareGrid_MSU(){
		$scope.topProductsColl2 = [
		  { field: "Rank", title:"Rank", width:"50px", headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width:"55px"},
		  { field: "", title:"Brand", template: "#:Brand#", width:"150px" },
		  { field: "", title:"TC Code", template: "#:TCCode#", width:"70px" },
		  { field: "NoOfUnit", title: "Units", headerTemplate: "Units "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(NoOfUnit/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketUnit", title: "Market Unit", headerTemplate: "Market Unit * "+$scope.figureQualifier_2+"<span class='k-icon k-i-sort-desc'></span>",
		  	template: "#:kendo.toString(MarketUnit/"+$scope.unitOfMeasures_MS.divisor+", 'n"+$scope.unitOfMeasures_MS.decimal+"')#", width: "130px" },
		  { field: "MarketUnitShare", title: "% Market Share", headerTemplate: "% Market Share <span class='k-icon k-i-sort-desc'></span>",
		  	template: "#=kendo.toString(MarketUnitShare, 'n')#", width: "130px" }
		];
	}



	/* ============================================ NVD3 - Line Bar Chart ============================================ */

		function initLineBarChart(){
			$scope.line_bar = {};

			$scope.line_bar.options = {
	            chart: {
	                type: 'linePlusBarChart',
	                height: 465,
	                margin: {
	                    top: 20,
	                    right: 100,
	                    bottom: 120,
	                    left: 75
	                },
	                bars: {
	                    forceY: [0]
	                },
	                bars2: {
	                    forceY: [0]
	                },
	                x: function(d,i) { return i },
	                xAxis: {
	                    axisLabel: 'Period',
	                    tickFormat: function(d) {
	                        var dx = $scope.line_bar.data[0].values[d] && $scope.line_bar.data[0].values[d].x || 0;
	                        if(dx > 0) {
	                            return d3.time.format('%b-%Y')(new Date(dx));
	                        }
	                        return null;
	                    }
	                    // ,ticks: 12
	                },
	                x2Axis: {
	                    tickFormat: function(d) {
	                        var dx = $scope.line_bar.data[0].values[d] && $scope.line_bar.data[1].values[d].x || 0;
	                        if(dx > 0) {
	                        	return d3.time.format('%b-%Y')(new Date(dx));
	                        }
	                        return null;
	                    },
	                    showMaxMin: false
	                },
	                y: function(d){
	                	return d.y;
	                },
	                y1Axis: {
	                    axisLabel: 'Sales (k)',
	                    tickFormat: function(d){
	                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	                    },
	                    axisLabelDistance: 12
	                },
	                y2Axis: {
	                    axisLabel: 'Sales',
	                    tickFormat: function(d) {
	                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	                    },
	                    axisLabelDistance: 10
	                },
	                focusEnable: false,
                    color: function (d, i) {
                        if(i === 0){
                            return BarBgColor;
                        }
                        else{
                            return d.ColorCode;
                        }
                    },
					noData: 'No Data to display',
	                legend: {
	                	maxKeyLength: 30,
	                	key: function(d){
	                		return d.originalKey
	                	},
	                	margin: {
	                		top: 0,
	                		right: 0,
	                		bottom: 0,
	                		left: 0
	                	},
						dispatch: {
							legendClick: function(t,u){
								if(t.bar){
									$timeout(function(){
										if((t.disabled === undefined) || t.disabled){
											$scope.line_bar.options.chart.forceY = ($scope.unitOfMeasures.key === 'value') ? [0, maxY_line_Value] : [0, maxY_line_Unit];
											$scope.apiLineBar.refresh();
										}
										else{
											$scope.line_bar.options.chart.forceY = ($scope.unitOfMeasures.key === 'value') ? [0, maxY_bar_Value] : [0, maxY_bar_Unit];
											$scope.apiLineBar.refresh();
										}
									}, 100);
								}
							}
						}
	                },
	                switchYAxisOrder: false,
	                tooltip: {
	                	keyFormatter: function(d, i){
	                		return ((d !== undefined) ? d.replace('(right axis)','') : d);
	                	},
	                	valueFormatter: function (d, i) {
							return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
						}
	                },
	                pointShape: function(d){
	                	return d.shape || 'square';
	                }
	            }
	        };

			var lineBarData = [];

			$scope.line_bar.data = lineBarData.map(function(series) {
                                        series.values = series.values.map(function(d) { return {x: d[0], y: d[1]} });
                                        return series;
                                    });
		}



	/* ============================================ NVD3 - Multibar Chart ============================================ */

		function initMultiBarChart(){
			$scope.multibar = {};
			$scope.multibar.data = [];
			$scope.multibar.options = {
	            chart: {
	                type: 'multiBarChart',
	                height: 480,
	                margin: {
	                    top: 40,
	                    right: 25,
	                    bottom: 140,
	                    left: 75
	                },
	                clipEdge: true,
	                //staggerLabels: true,
	                showLegend: true,
	                legend: {
	                	maxKeyLength: 25
	                },
	                duration: 500,
	                stacked: true,
	                xAxis: {
	                    axisLabel: 'Patches',
	                    showMaxMin: false,
	                    tickFormat: function(d){
	                        return (d.length > 15) ? (d.substr(0,14) + '...') : d;
	                    },
	                    staggerLabels: true,
	                    axisLabelDistance: 15
	                },
	                yAxis: {
	                    axisLabel: 'Sales (k)',
	                    axisLabelDistance: 12,
	                    tickFormat: function(d){
	                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	                    }
	                },
	                stacked: false,
	                noData: 'No Data to display',
                    color: function (d, i) {
                        return d.ColorCode;
                    },
	                tooltip: {
	                	valueFormatter: function (d, i) {
							return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
						}
	                }
	            }
	        };

	        $scope.multibar.data = [{
			    "key": "Stream0",
			    "values": []
			}];
       }


    /* ============================================ NVD3 - Multibar Chart for Market Share ============================================ */

        function prepareMultiBarChart_2(data){
            // Convert Periods into Milliseconds since ?/?/1970
            if (data.length > 0) {
                if (data[0].values[0].x === undefined) {
                    for (var i = 0; i < data.length; i++) {
                        data[i].values = data[i].values.map(function(item){
                            item[0] = item[0].toString();
                            item[0] = item[0].substr(0, 4) + ' ' + item[0].substr(4, 2);
                            item[0] = new Date(item[0]).getTime();
                            return {x: item[0], y: item[1]};
                        });
                    }
                }
            }
            if (data.length > 0) {
                $scope.multibar_2.data = data;
            }
            else {
                $scope.multibar_2.data = [
                    {
                        "key": "",
                        "values": []
                    }
                ];
            }
            $scope.apiMultiBar_2.update();
        }

        initMultiBarChart_2();
        function initMultiBarChart_2(){
            $scope.multibar_2 = {};

            $scope.multibar_2.options = {
                chart: {
                    type: 'multiBarChart',
                    height: 450,
                    margin: {
                        top: 40,
                        right: 25,
                        bottom: 140,
                        left: 75
                    },
                    clipEdge: true,
                    //staggerLabels: true,
                    showLegend: true,
                    legend: {
                        maxKeyLength: 25
                    },
                    duration: 500,
                    xAxis: {
                        axisLabel: 'Periods',
                        showMaxMin: false,
                        tickFormat: function(d){
                            return d3.time.format('%b-%Y')(new Date(d));
                        },
                        staggerLabels: true,
                        axisLabelDistance: 0
                    },
                    yAxis: {
                        axisLabel: 'Market Share %',
                        axisLabelDistance: 12,
                        tickFormat: function(d){
                            return (d);
                        }
                    },
                    stacked: false,
                    showControls: false,
                    noData: 'No Data to display',
                    color: function (d, i) {
                        return d.ColorCode;
                    }
                }
            };

            $scope.multibar_2.data = [{
                "key": "Stream0",
                "values": []
            }];
        }


	/* ============================================ NVD3 - Stacked Area Chart ============================================ */

	/*function prepareStackedAreaChart(data) {
	// Convert Periods into Milliseconds since ?/?/1970
		if (data.length > 0) {
			if (Math.ceil(Math.log((data[0].values[0][0]) + 1) / Math.LN10) === 6) {
				for (var i = 0; i < data.length; i++) {
					for (var j = 0; j < data[i].values.length; j++) {
						data[i].values[j][0] = data[i].values[j][0].toString();
						data[i].values[j][0] = data[i].values[j][0].substr(0, 4) + ' ' + data[i].values[j][0].substr(4, 2);
						data[i].values[j][0] = new Date(data[i].values[j][0]).getTime();
					}
				}
			}
		}
		if (data.length > 0) {
			$scope.stacked.data = data;
		}
		else {
			$scope.stacked.data = [
				{
					"key": "",
					"values": []
				}
			];
		}
		$scope.apiStacked.update();
	}

		initStackedAreaChart();
		function initStackedAreaChart() {
			$scope.stacked = {};

			$scope.stacked.options = {
	              chart: {
	                  type: 'stackedAreaChart',
	                  height: 450,
	                  margin : {
	                      top: 50,
	                      right: 30,
	                      bottom: 100,
	                      left: 70
	                  },
	                  x: function(d){return d[0];},
	                  y: function(d){return d[1];},
	                  useVoronoi: false,
	                  clipEdge: true,
	                  duration: 100,
	                  useInteractiveGuideline: true,
	                  xAxis: {
	                      axisLabel: 'Period',
	                      showMaxMin: true,
	                      tickFormat: function (d) {
	                          return d3.time.format('%b-%Y')(new Date(d));
	                      }
	                  },
	                  yAxis: {
	                      axisLabel: 'Market Share %',
	                      tickFormat: function (d) {
                              return d3.format(',.1f')(d);
	                      }
	                  },
					  color: function (d, i) {
                          return d.ColorCode;
                      },
	                  showLegend: true,
	                  showControls:false,
					  legend: {
								margin: {
									top: 10,
									right: 0,
									bottom: 5,
									left: 0
								}
					  },
	                  noData: 'No Data to display'
	              }
	          };

	          $scope.stacked.data = [
	              {
	                  "key" : "" ,
	                  "values" : []
	              }
	          ];
		}*/


	/* ============================================ NVD3 - Pie Chart ============================================ */


    /* Assign function for refreshing all charts and graphs to rootScope */
    $rootScope.refreshCharts_Analysis = function(){
        $timeout(function(){
            $scope.apiLineBar.refresh();
            $scope.apiMultiBar.refresh();
            $scope.apiMultiBar_2.refresh();
        }, 500);
    };

	/* Graphs & Charts end */



	/* Export functionality starts */
	$scope.exportToPDF = function(){
		var _svg = document.getElementById('svgChart');
		console.log(_svg.innerHTML);
		var _canvas = document.getElementById('canvas');
		canvg(_canvas, _svg.innerHTML);
		var imgData = canvas.toDataURL('image/png');
		var doc = new jsPDF('p', 'mm');
        doc.addImage(imgData, 'PNG', 10, 10);
        doc.save('sample-file-1.pdf');

		/*kendo.drawing.drawDOM(angular.element('#marketSalesWrapper'))
		.then(function(group){
			// kendo.drawing.pdf.saveAs(group, 'Market Sales Potential Chart.pdf');
			return kendo.drawing.exportPDF(group, {
				paperSize: "auto"
			});
		})
		.done(function(data){
			kendo.saveAs({
				dataURI: data,
				fileName: "Market Sales Potential Chart.pdf"
			});
		});*/


		/*var reqBody = {};
		reqBody.PDFName = 'MarketSales';
        reqBody.PDFData = angular.element('#marketSalesWrapper').html();
        analysisServices.GeneratePDF(reqBody, showLoader, hideLoader)
		.then(function (response) {
				if (response.data !== null && response.status === 200) {
                    kendo.saveAs({
                        dataURI: response.data,
                        fileName: "Market Sales Potential Chart.pdf"
                    });
				} else {
					// $scope.analysisDashboardData = false;
				}
			},
			function (rejection) {});*/


		/*var svgElements = $("#marketSalesWrapper").find('svg.nvd3-svg').css({'width':'992px'});

      //replace all svgs with a temp canvas
      svgElements.each(function() {
        var canvas, xml;

        // canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
        $.each($(this).find('[style*=em]'), function(index, el) {
          $(this).css('font-size', getStyle(el, 'font-size'));
        });

        canvas = document.createElement("canvas");
        canvas.className = "screenShotTempCanvas";
        //convert SVG into a XML string
        xml = (new XMLSerializer()).serializeToString(this);

        // Removing the name space as IE throws an error
        xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

        //draw the SVG onto a canvas
        canvg(canvas, xml);
        $(canvas).insertAfter(this);
        //hide the SVG element
        ////this.className = "tempHide";
        $(this).attr('class', 'tempHide');
        $(this).hide();
      });

		html2canvas(document.getElementById('marketSalesWrapper'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("html2canvas.pdf");
            }
        });

        angular.element("#marketSalesWrapper").find('.screenShotTempCanvas').remove();
      	angular.element("#marketSalesWrapper").find('.tempHide').show().removeClass('tempHide');*/

        /* Create fabric instance */
		/*var canvas = new fabric.StaticCanvas( undefined, {
		    width: 500,
		    height: 500,
		} );

		// Load SVG document; add to canvas;
		fabric.parseSVGDocument(document.getElementById('marketSalesWrapper').getElementsByTagName("svg")[0],function(objects, options) {
		    var g = fabric.util.groupSVGElements( objects, options );
		    var data = "";

		    // ADD ELEMENTS
		    canvas.add( g );

		    // EXPORT TO DATA URL
		    data = canvas.toDataURL({
		        format: "pdf"
		    });

		    console.log('fabric data: ',data);

		    var docDefinition = {
                content: [{
                    image: data,
                    width: 500,
                }]
            };
            pdfMake.createPdf(docDefinition).download("fabricjs.pdf");

		// REVIVER
		},function(svg,group) {
		    // possiblitiy to filter the svg elements or adapt the fabric object
		});*/
	};
	/* Export functionality ends */


	/* Document Ready starts */
	angular.element(document).ready(function() {
		/* Angular Provision for Tab Panel */
		$scope.tabRadio = true;
		$scope.tabPanelState = 1;
		$scope.filtersAltered = true;
		$scope.showTab = function(id1, id2){
			$scope.tabRadio = !($scope.tabRadio);
			angular.element(id1).fadeOut(500, function(){
				angular.element(id2).fadeIn(500, function(){
					if(id2 === '#tabs-5-tab-2'){
						$scope.apiMultiBar_2.refresh();
						$scope.tabPanelState = 2;
						if($scope.analysisMarketShareData === undefined || $scope.filtersAltered){
							$scope.filtersAltered = false;
							$scope.displayResult();
						}
					}
					else{
						if($scope.filtersAltered){
							$scope.filtersAltered = false;
							$scope.displayResult();
						}
						else{
							$scope.apiLineBar.refresh();
							$scope.apiMultiBar.refresh();
						}
						$scope.tabPanelState = 1;
					}
				});
			});
		};


        // angular.element(".header-section-content-load").fadeIn(200);
        // angular.element(".main-page-content-load").show(100);

		angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
    	});


		/*----- Enlarged-Panel functionality starts -----*/

        angular.element('#resize_map').click(function() {
        	if($scope.tabPanelState === 1){
		        angular.element('#filter-overlay-1').show();
		        angular.element('.filter-close').show();
		        angular.element('#container_graph_1').addClass('enlarged');
				// angular.element('.productSalesGraph').css({'width':'90%'});
				// angular.element('.productSplitSalesGraph').css({'width':'90%'});
				if($scope.marketSalesRadio === 'overallSales'){
					$scope.apiLineBar.refresh();
				}
				else{
					$scope.apiMultiBar.refresh();
				}
			}
			else{
				angular.element('#filter-overlay-2').show();
		        angular.element('.filter-close').show();
		        angular.element('#container_graph_2').addClass('enlarged');
				// angular.element('.multiBarChart_2 svg.nvd3-svg').css({'width':'85%'});
				$scope.apiMultiBar_2.refresh();
			}
        });

        angular.element('.tab-content').on('click', '.filter-close', function() {
        	if($scope.tabPanelState === 1){
			    angular.element('#filter-overlay-1').hide();
			    angular.element('.filter-close').hide();
			    angular.element('#container_graph_1').removeClass('enlarged');
			    // angular.element('.productSalesGraph').css({'width':'100%'});
			    // angular.element('.productSplitSalesGraph').css({'width':'100%'});
			    if($scope.marketSalesRadio === 'overallSales'){
			    	$scope.apiLineBar.refresh();
			    }
			    else{
			    	$scope.apiMultiBar.refresh();
			    }
			}
			else{
				angular.element('#filter-overlay-2').hide();
			    angular.element('.filter-close').hide();
			    angular.element('#container_graph_2').removeClass('enlarged');
			    // angular.element('.multiBarChart_2 svg.nvd3-svg').css({'width':'100%'});
			    $scope.apiMultiBar_2.refresh();
			}
		});

		/*----- Enlarged-Panel functionality ends -----*/


		/* Scroll to div by id */
		$scope.scrollById = function() {
			if($scope.tabPanelState === 1){
                $document.scrollToElementAnimated(angular.element('#dataGrid1'), 125);
			}
			else{
                $document.scrollToElementAnimated(angular.element('#dataGrid2'), 125);
			}
		};

		/*Scroll to Preset input box when its checkbox is checked*/
		$scope.PresetFlagChanged = function() {
			if($scope.SavePresetFlag){
				// angular.element('#dummy-presetname').scrollTop = 200;
				console.log('if flag');
			}
			else{
				console.log('else flag');
			}
		};


	});
	/* Document Ready ends */

}]);


angular.module("imsApp")
.controller("LoginController", ['$scope', '$state', 'accountServices', '$window', 'notify', '$timeout',
						function($scope, $state, accountServices, $window, notify, $timeout){

	console.log('Login controller...', $state);

	if(typeof(Storage) === undefined){
		alert('Session Storage is not supported by this Browser');
	}

	// Generic Image Base Path
	$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');

	// Sign in redirect logic
    $scope.check_login = function(){
        loginRedirectFunction();
    }

    
    /* Login Redirect Functionality */
    function loginRedirectFunction(){
        accountServices.Login($scope.login.email, $scope.login.password)
        .then(function (response) {
            if (response.data != null && response.status === 200) {
                var paths = setImagePath(response.data.ProfileImage, response.data.LogoFileName);
                response.data.ProfileImage = paths.profileImg;
                response.data.LogoFileName = paths.logoImg;

                if(response.data.RoleID === 1){
                    $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), response.data));
                    $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':response.data.SessionID}));
                    $state.go('super.customerlist');
                }
                else if(response.data.RoleID > 1){
                    // Verify the account for validity
                    if(!response.data.CompanyStatus){
                        showNotification({message:'Subscription is disabled', duration: 3000, class:'notify-bg', routine:null});
                    }
                    else if(!compareSubscriptionDate(response.data.SubscriptionStartDate, response.data.SubscriptionEndDate)){
                        showNotification({message:'Subscription is ended', duration: 3000, class:'notify-bg', routine:null});
                    }
                    else{
                        if(response.data.RoleID === 2){
                            $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), response.data));
                            $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':response.data.SessionID}));
                            $state.go('admin.userlist');
                        }
                        else if(response.data.RoleID === 3){
                            $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), response.data));
                            $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':response.data.SessionID}));
                            // $window.localStorage.setItem('imsSessionInfo',JSON.stringify(response.data));
                            $state.go('user.analysis');
                        }
                    }
                }
                else{
                    showNotification({message:'Invalid User Role: '+response.data, duration: 3000, class:'notify-bg', routine:null});
                }
            } else {
                showNotification({message:'Invalid login response status: '+response.status, duration: 3000, class:'notify-bg', routine:null});
            }
        },
        function (rejection) {
            if(rejection.status === 404){
                showNotification({message:'Invalid Login Info', duration: 3000, class:'notify-bg', routine:null});
            }
        });
    }
    

    /* setting path for image */
    function setImagePath(profileImg, logoImg){
    	var path = {};
   		path.profileImg = (profileImg !== '') ? ($scope.baseURL + profileImg) : ($scope.baseURL + 'avatars/avatar-2-256.png');
    	path.logoImg = (logoImg !== '') ? ($scope.baseURL + logoImg) : (path.logoImg = $scope.baseURL + 'avatars/demo_logo.jpg');
    	return path;
    }


    /* Compare Subscription dates for validity */
	function compareSubscriptionDate(Sdate, Edate){
		Sdate = new Date(Sdate).getTime();
		Edate = new Date(Edate).getTime();
		var Tdate = new Date().getTime();
		if((Sdate <= Edate) && (Tdate <= Edate)){
			return true;
		}
		return false;
	}


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}


	/* Images for Slider */
	$scope.images = [{
		src: 'content/img/slide/1.jpg',
			alt: 'Slide1'
		}, {
			src: 'content/img/slide/2.jpg',
			alt: 'Slide2'
		}, {
			src: 'content/img/slide/3.jpg',
			alt: 'Slide3'
		}, {
			src: 'content/img/slide/4.jpg',
			alt: 'Slide4'
		}, {
			src: 'content/img/slide/5.jpg',
			alt: 'Slide5'
		}
	];


}]);

angular.module("imsApp")
    .controller("MapViewController", ['$scope', '$state', 'userServices', 'companyServices', 'NgMap', 'notify', 'savedSearhcesServices', '$window', '$rootScope', '$timeout',
		function($scope, $state, userServices, companyServices, NgMap, notify, savedSearhcesServices, $window, $rootScope, $timeout){
    console.log('MapView controller...', $state);

    /* Make $rootScope available in Local $scope */
	$scope.rootScope = $rootScope;

	/* Parse session data to object format */
    function sessionInfoToScope(){
        $scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
    }
    sessionInfoToScope();

    angular.element(document).ready(function(){
        // loadMap();
    });


	/* Static Data & Values */
    $scope.periodTypeColl = [
        { type: "MAT", value: 'mat', active: false },
        { type: "MTH", value: 'mth', active: true },
        { type: "YTD-Finance", value: 'ytdf', active: false },
        { type: "YTD-Calendar", value: 'ytdc', active: false }
    ];

    $scope.comparisonType = [
        { title:"Company vs Company", value:"company" },
        { title: "Brand vs Brand", value: 'brand' }
    ];

    $scope.TherapyClassColl = [
        { type: "Therapy Class 1", value: 'tc1' },
        { type: "Therapy Class 2", value: 'tc2' },
        { type: "Therapy Class 3", value: 'tc3' },
        { type: "Therapy Class 4", value: 'tc4' }
    ];

    /* Initialize Models & Variables */
	initVariables();
	function initVariables() {
        showLoader = "angular.element('.loader-backdrop').fadeIn();";
        hideLoader = "angular.element('.loader-backdrop').fadeOut();";

        $scope.fetchData = {};
        $scope.fetchData.CompanyID = $scope.sessionInfo.CompanyID;
        $scope.fetchData.GeographyFilter = 'city';
        if($scope.sessionInfo.PeriodType === undefined){
            $scope.fetchData.PeriodType = 'mth';
            updateSessionInfo(undefined, 'mth');
        }
        else{
            $scope.fetchData.PeriodType = $scope.sessionInfo.PeriodType;
        }
        $scope.fetchData.ComparisonType = 'company';
        $scope.fetchData.TCLevel = null;
        $scope.fetchData.TCCode = null;
        $scope.comparisonData = $scope.fetchData.ComparisonType;
        $scope.userId = $scope.sessionInfo.UserID;
        $scope.tabType = 2;
        $scope.isCluster = false;
        $scope.selected = {};
        $scope.selected.Therapies = [];
		$scope.fetchData.TCCode = null;
        $scope.selected.selectedTypes = [];
        $scope.fetchData.includeTopSearches = 1;
        $scope.filteredCategoryValues = [];
        $scope.percentArray_Sales = [];
        $scope.percentArray_Units = [];
        $scope.isPreset = false;
        $scope.RegExNumForm = /(\d)(?=(\d{2})+\d\.)/g;
        // Unit measures dropdown
        $scope.unitOfMeasuresColls = [
                                        {key:'value', label:'Values', divisor:1, unit:'', decimal:0},
                                        {key:'value', label:'Values(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'value', label:'Values(Cr)', divisor:10000000, unit:'Cr', decimal:2},
                                        {key:'unit', label:'Units', divisor:1, unit:'', decimal:0},
                                        {key:'unit', label:'Units(k)', divisor:1000, unit:'k', decimal:0},
                                        {key:'unit', label:'Units(Cr)', divisor:10000000, unit:'Cr', decimal:2}
                                    ];
        $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
        $scope.UOM_set_flag = false;
    }

	/* Edit Session info */
    function updateSessionInfo(GeographyID, PeriodType){
        var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
        if(GeographyID !== undefined){ sessionInfo.GeographyID = GeographyID; }
        if(PeriodType !== undefined){ sessionInfo.PeriodType = PeriodType; }
        $window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
        sessionInfoToScope();
    }

	/* Divisions
	 $scope.FetchCompanyDivisions = function () {
	 companyServices.GetCompanyDivisions($scope.fetchData.CompanyID)
	 .then(function (response) {
	 if (response.data != null && response.status === 200) {
	 $scope.divisionColl = response.data;
	 $scope.fetchData.DivisonID = $scope.divisionColl[0].DivisionID;
	 $scope.FetchGeographyValues();
	 } else {
	 $scope.divisionColl = false;
	 }
	 },
	 function (rejection) {});
	 };
	 $scope.FetchCompanyDivisions();*/

	/* Geography */
    $scope.FetchGeographyValues = function (presetData) {
        userServices.GetGeographyValues($scope.fetchData.GeographyFilter, $scope.fetchData.CompanyID, $scope.sessionInfo.SessionID)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.geographyColl = response.data;
                        if(JSON.parse($window.sessionStorage.imsSessionInfo).GeographyID === undefined){
                            $scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
                            updateSessionInfo($scope.geographyColl[0].GeographyID, undefined);
                        }
                        else if(presetData !== undefined){
                            $scope.fetchData.GeographyID = JSON.parse(presetData).GeographyID;
                        }
                        else{
                            $scope.fetchData.GeographyID = JSON.parse($window.sessionStorage.imsSessionInfo).GeographyID;
                        }
                        $scope.FetchBricksByGeography(presetData);
                    } else {
                        $scope.geographyColl = false;
                    }
                },
                function (rejection) {});
    };

	/* Division to which user's role is associated */
    $scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
    $scope.FetchGeographyValues();


	/* Map Geography Id to Geography Name */
    function mapGeographyIdToName(){
        $scope.GeographyName = $scope.geographyColl.filter(function(item){ return (item.GeographyID === $scope.fetchData.GeographyID); })[0].GeographyValue;
    }

	/* Bricks */
    $scope.geographySelected = function(){
        updateSessionInfo($scope.fetchData.GeographyID, undefined);
        $scope.FetchTimePeriods();
        userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.bricksColl = response.data;
                        $scope.selectedPatches = [];
                        if(response.data[0].IsCluster){
                            $scope.isCluster = true;
                        }
                        else{
                            $scope.isCluster = false;
                        }
                    } else {
                        $scope.bricksColl = false;
                    }
                },
                function (rejection) {});
    }


	/* Bricks */
    $scope.FetchBricksByGeography = function (presetData) {
        if(presetData !== undefined){
            $scope.fetchData.GeographyID = JSON.parse(presetData).GeographyID;
        }
        $scope.FetchTimePeriods();
        userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.bricksColl = response.data;
                        if(response.data[0].IsCluster){
                            $scope.isCluster = true;
                        }
                        else{
                            $scope.isCluster = false;
                        }
                    } else {
                        $scope.bricksColl = false;
                    }
                    // Fetch values for first comparison type
                    if(presetData !== undefined){
                        if(JSON.parse(presetData).TCLevel !== undefined){
                            $scope.fetchTherapyListByTherapyCode('$scope.displayResult('+presetData+')', JSON.parse(presetData).TCLevel, JSON.parse(presetData).TCCode);
                        }
                        else{
                            $scope.isPreset = true;
                            $scope.fetchComparisonTypeValue('$scope.displayResult('+presetData+')');
                        }
                    }
                    else{
                        $scope.isPreset = false;
                        $scope.fetchComparisonTypeValue('$scope.displayResult()');
                    }
                },
                function (rejection) {});
    }


	/* Time Periods */
    $scope.FetchTimePeriods = function () {
        $scope.period = {};
        userServices.GetTimePeriods($scope.fetchData.GeographyID)
        .then(function (response) {
            if (response.data != null && response.status === 200) {
                $scope.timePeriods = response.data;
                $scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
                $scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
            } else {
                $scope.timePeriods = false;
            }
        },
        function (rejection) {});
    };

    /* Period Type changed or Selected */
	$scope.periodTypeSelected = function(event) {
		if(!event.dataItem.active){
			event.preventDefault();
		}
	};

	/* Period Type changed */
    $scope.periodTypeChanged = function(){
        updateSessionInfo(undefined, $scope.fetchData.PeriodType);
    };

    $scope.timeSelected = function(event){
        if($scope.period.startPeriod > $scope.period.endPeriod){
            $scope.period.endPeriod = $scope.timePeriods[0].TimePeriodID;
            $scope.period.startPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
        }
    };


    // Comparison Type changed
    $scope.comparisonTypeChanged = function(){
        $scope.isPreset = false;
        if($scope.fetchData.ComparisonType === 'company'){
            $scope.fetchData.TCLevel = undefined;
			$scope.fetchData.TCCode = undefined;
            $scope.fetchComparisonTypeValue();
        }
        else{
            $scope.fetchData.TCLevel = 'tc1';
            $scope.fetchTherapyListByTherapyCode('$scope.fetchComparisonTypeValue()');
        }
    };


    // Fetch Therapy List for selected therapy class
    $scope.fetchTherapyListByTherapyCode = function(customRoutine, TCLevel, TCCode){
        $scope.fetchData.TCLevel = (TCLevel !== undefined) ? TCLevel : $scope.fetchData.TCLevel;

        companyServices.GetTherapyListByTherapyCode($scope.fetchData.TCLevel, $scope.sessionInfo.SessionID)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.therapyList = response.data;
                        $scope.therapyList.unshift({'TherapyCode':'all', 'TherapyName':'All'});
                        if(TCCode === undefined){
                            $scope.fetchData.TCCode = $scope.therapyList[0].TherapyCode;
                            $scope.selected.Therapies = [];
                        }
                        else{
                            $scope.fetchData.TCCode = TCCode;
                            $scope.selected.Therapies = ($scope.fetchData.TCCode !== 'all') ? $scope.fetchData.TCCode.split(',') : [];
                        }

                        if(TCCode !== undefined){
                            $scope.isPreset = true;
                            $scope.fetchComparisonTypeValue(customRoutine);
                        }
                        else if(customRoutine !== undefined){
                            $scope.isPreset = false;
                            eval(customRoutine);
                        }
                        else{
                            $scope.isPreset = false;
                        }
                    } else {
                        $scope.therapyList = false;
                    }
                },
                function (rejection) {});
    };


    /* Therapies selected from the list */
	$scope.therapiesSelected = function(isOpen){
		if(!isOpen){	// denoting that therapies have been selected by user
            $scope.isPreset = false;
			$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
			$scope.fetchComparisonTypeValue();
		}
	};

	/* Therapy removed from the list */
	$scope.therapiesRemoved = function($item, $model){
        $scope.isPreset = false;
		$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
		$scope.fetchComparisonTypeValue();
	};


	/* Map Therapy Id to Therapy Name */
    function mapTherapyIdToName(){
        $scope.TherapyName = $scope.therapyList.filter(function(item){ return (item.TherapyCode === $scope.fetchData.TCCode); })[0].TherapyName;
    }


    // Fetch Company or Brand List
    $scope.fetchComparisonTypeValue = function(customRoutine){
        var TherapyClass = ($scope.fetchData.TCLevel === undefined) ? null : $scope.fetchData.TCLevel;
        var selectedTherapy = ($scope.fetchData.TCCode === undefined) ? null : $scope.fetchData.TCCode;
        $scope.selected.selectedTypes = [];
        $scope.filteredCategoryValues = [];
        companyServices.GetComparisionTypeValue($scope.fetchData.ComparisonType, $scope.sessionInfo.SessionID, TherapyClass, selectedTherapy)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.comparisonColl = response.data;
                        if($scope.isPreset){
                            $scope.filteredCategoryValues = $scope.comparisonColl;
                        }
                        if(customRoutine !== undefined){
                            eval(customRoutine);
                        }
                    } else {
                        $scope.comparisonColl = false;
                    }
                },
                function (rejection) {});
    };


	/* Filter Category values array */
    $scope.filterCategoryValues = function(searchQuery) {
        if(searchQuery !== ''){
            var regExpression = new RegExp(searchQuery, 'i');
            $scope.filteredCategoryValues = $scope.comparisonColl.filter(function(item){ return regExpression.test(item.ComparisonValue); });
        }
    };


    // Display Result
    $scope.displayResult = function (presetData) {
        if(presetData !== undefined){
            applyPresetData(presetData);
        }
        else{
            if($scope.fetchData.PeriodType === 'mth'){
                $scope.fetchData.PeriodStart = $scope.period.startPeriod;
                $scope.fetchData.PeriodEnd = $scope.period.endPeriod;
            }
            else{
                $scope.fetchData.PeriodStart = 0;
                $scope.fetchData.PeriodEnd = 0;
            }

            // Companies or Brands
            if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
                $scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
            }
            else{
                $scope.fetchData.ComparisionValue = 'all';
                $scope.fetchData.includeTopSearches = 1;
            }

            // Patches
            if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
                $scope.fetchData.PatchIds = $scope.selectedPatches.toString();
            }
            else{
                $scope.fetchData.PatchIds = 'all';
            }

            fetchPatchesByCluster();
        }
    };


    // Get Patches by Clusters
    function fetchPatchesByCluster(){
        if($scope.isCluster && ($scope.fetchData.PatchIds !== 'all')){
            userServices.GetPatchIDsByClusterIDs($scope.fetchData.PatchIds, showLoader, hideLoader)
                .then(function (response) {
                        if (response.data != null && response.status === 200) {
                            $scope.fetchData.PatchIds = response.data;
                            // Fetch Final Result
                            $scope.FetchCompanyPerformanceMap();
                        } else {
                            $scope.fetchData.PatchIds = '';
                        }
                    },
                    function (rejection) {});
        }
        else{
            // Fetch Final Result
            $scope.FetchCompanyPerformanceMap();
        }
    }


	/* Company Performance Map */
    $scope.FetchCompanyPerformanceMap = function () {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
        companyServices.GetCompanyPerformanceMap($scope.fetchData)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.mapviewData = response.data;

                        resetUOM_MinValueLength($scope.mapviewData.CompanyPerformanceValueList);
                        $scope.UOM_changed();
                        mapGeographyIdToName();
                        $scope.comparisonData = $scope.fetchData.ComparisonType;
                        if($scope.fetchData.ComparisonType === 'company'){
                            $scope.TherapyName = '';
                        }
                        else if($scope.fetchData.ComparisonType === 'brand'){
                            mapTherapyIdToName();
                        }
                    } else {
                        $scope.mapviewData = false;
                    }
                },
                function (rejection) {});
    };


    /* Reset UOM based on Min value length */
    function resetUOM_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_set_flag)){
            var formattedArray = [], minVal = 0, min_local = 0, minLen = 1;
            for(var i = 0; i < data.length; i++){
                // Find min from multidimensional array
                formattedArray = data[i].map_chart.data.filter(function(item){ return (item[1] >= 1); }).map(function(item){ return (item[1]); });
                min_local = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
                if(i === 0){ minVal = min_local; }
                if(min_local < minVal){
                    minVal = min_local;
                }
            }
            minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[2];
            }
        }
    }


    $scope.UOM_changed = function(inputObj) {
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

        if($scope.mapviewData !== undefined){
            if($scope.unitOfMeasures.key === 'value'){
                preparePerformanceMap($scope.mapviewData.CompanyPerformanceValueList);
            }
            else{
                preparePerformanceMap($scope.mapviewData.CompanyPerformanceUnitList);
            }
        }
    };


    /*--------------------- Divide a number(n) by (d) and round upto (x) decimal places ---------------------*/
	$scope.DivideAndRound = function(n, d, x) {
		if(x === 0){
			n = (n/d).toFixed(1);
			n = (n.replace($scope.RegExNumForm, '$1,'));
			n = n.substring(0, (n.length-2));
		}
		else{
			n = (n/d).toFixed(x);
			n = (n.replace($scope.RegExNumForm, '$1,'));
		}
		return n;
	}


	/* Select Preset functionality Begins */
    $scope.savePreset = function(){
        savedSearhcesServices.AddUpdateSavedSearches(preparePresetData())
            .then(function (response) {
                if (response.data !== null && response.status === 200) {
                    initializeOnPreset();
                    notify({
                        message: 'Presets saved Successfully',
                        classes: $scope.classes,
                        templateUrl: $scope.template,
                        position: 'center',
                        duration: 2000
                    });
                } else {

                }
            },
            function (rejection) {});
    };


    function preparePresetData () {
        var presetObject = {};
        presetObject.SearchKey = $scope.PresetName;
        presetObject.UserID = $scope.userId;
        presetObject.TabType = $scope.tabType;

        if($scope.fetchData.PeriodType === 'mth'){
            $scope.fetchData.PeriodStart = $scope.period.startPeriod;
            $scope.fetchData.PeriodEnd = $scope.period.endPeriod;
        }
        else{
            $scope.fetchData.PeriodStart = 0;
            $scope.fetchData.PeriodEnd = 0;
        }

        // Patches
        if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
            $scope.fetchData.PatchIds = $scope.selectedPatches.toString();
        }
        else{
            $scope.fetchData.PatchIds = 'all';
        }

        // Companies or Brands
        if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
            $scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
        }
        else{
            $scope.fetchData.ComparisionValue = 'all';
            $scope.fetchData.includeTopSearches = 1;
        }

        // Remove SessionID before saving preset
        if($scope.fetchData.SessionID !== undefined){
            delete $scope.fetchData.SessionID;
        }
        presetObject.SearchData = JSON.stringify($scope.fetchData);
        return presetObject;
    }


	/* Get Presets */
    function LoadPresets(){
        savedSearhcesServices.GetSavedSearchesbyUser($scope.userId, $scope.tabType)
            .then(function (response) {
                    if (response.data != null && response.status === 200) {
                        $scope.presetsColl = response.data;
                    } else {
                        $scope.presetsColl = false;
                    }
                },
                function (rejection) {});
    }
    LoadPresets();

    $scope.applySelectedPreset = function() {
        angular.element('.loader-backdrop').fadeIn();
        var presetData = $scope.presetsColl.filter(function(item){ return (item.SearchKeyID === $scope.selectedPreset); })[0].SearchData;
        var presetData_obj = JSON.parse(presetData);
        $scope.fetchData.GeographyFilter = presetData_obj.GeographyFilter;
        $scope.fetchData.DivisonID = presetData_obj.DivisonID;
        $scope.fetchData.ComparisonType = presetData_obj.ComparisonType;
        $scope.FetchGeographyValues(presetData);
    };


    // Apply Preset data to fetchData
    function applyPresetData(presetData){
        $scope.fetchData = presetData;
        updateSessionInfo($scope.fetchData.GeographyID, $scope.fetchData.PeriodType);

        if($scope.fetchData.PeriodStart !== 0){
            $scope.period.startPeriod = $scope.fetchData.PeriodStart;
            $scope.period.endPeriod = $scope.fetchData.PeriodEnd;
        }

        if($scope.fetchData.PatchIds !== 'all'){
            $scope.selectedPatches = $scope.fetchData.PatchIds.split(',').map(Number);
        }
        else{
            $scope.selectedPatches = [];
        }

        if($scope.fetchData.ComparisionValue !== 'all'){
            var selectedTypes_local = $scope.fetchData.ComparisionValue.split(',');
            $scope.selected.selectedTypes = selectedTypes_local.map(function(item){ return Number(item); });
        }
        else{
            $scope.selected.selectedTypes = [];
        }

        angular.element('.loader-backdrop').fadeOut();
    }


    // Initialize filter choices on preset
    function initializeOnPreset(){
        LoadPresets();
        $scope.PresetName = '';
        $scope.SavePresetFlag = false;
        $scope.fetchData.GeographyFilter = 'city';
        $scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
        $scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
        $scope.selectedPatches = [];
        $scope.fetchData.PeriodType = $scope.sessionInfo.PeriodType;
        $scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
        $scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
        $scope.fetchData.ComparisonType = 'company';
        $scope.selected.selectedTypes = [];
        $scope.fetchData.TCLevel = null;
        $scope.fetchData.TCCode = null;
        $scope.fetchData.includeTopSearches = 1;
        $scope.filteredCategoryValues = [];
        $scope.isPreset = false;
    }
	/* Select Preset functionality Ends */




    $scope.mapviewCompany=[
        {company:"Company to Company"},
        {company:"Brand to Brand"}
    ];



	/*------------------------ Google map customized styling ------------------------*/
    var styleArray =
        [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#e31111"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#9787f0"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f3f2ed"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#cae7f0"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d4f2cb"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#eacbe5"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#e3bcd9"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e6e7e9"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e8eaed"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d9ecec"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ];

    $scope.options = {
        styles: styleArray
    };

	/* ng-map with polygons covering pincodes */
	/*$scope.mapCenter = [15.5356433,73.8634543];		// Map-center for Goa

	 $scope.polyCoords = [];
	 // SANTA CRUZ 403005
	 $scope.polyCoords.push([[15.479827,73.836313],[15.478305,73.835379],[15.477633,73.834864],[15.476861,73.834557],[15.476593,73.834559],[15.475826,73.835185],[15.47556,73.835463],[15.475666,73.836603],[15.475567,73.83688],[15.475202,73.837504],[15.47487,73.83792],[15.474874,73.83875],[15.474944,73.839406],[15.474984,73.840511],[15.474484,73.840963],[15.474116,73.841],[15.473779,73.840725],[15.473207,73.840072],[15.472635,73.839591],[15.472032,73.839595],[15.470927,73.839601],[15.470224,73.839605],[15.469821,73.839503],[15.469719,73.839089],[15.469277,73.837882],[15.468505,73.837507],[15.465597,73.836829],[15.464928,73.836902],[15.464426,73.837112],[15.463961,73.837736],[15.463433,73.839191],[15.463036,73.840229],[15.462907,73.841128],[15.462742,73.841578],[15.461772,73.84186],[15.459863,73.841802],[15.459092,73.841633],[15.458523,73.841567],[15.458054,73.841605],[15.456081,73.842194],[15.456101,73.845965],[15.45591,73.848999],[15.456181,73.851498],[15.45676,73.853835],[15.45713,73.855642],[15.457343,73.856917],[15.458076,73.858988],[15.458853,73.859729],[15.459941,73.860787],[15.461081,73.861844],[15.462736,73.862953],[15.464361,73.863817],[15.465463,73.86288],[15.465748,73.862456],[15.465869,73.862032],[15.466312,73.860422],[15.467004,73.859445],[15.467616,73.858934],[15.468229,73.858508],[15.469046,73.85808],[15.469947,73.857737],[15.470888,73.857351],[15.472625,73.857261],[15.472793,73.858191],[15.472961,73.858867],[15.473417,73.859837],[15.474078,73.860806],[15.474514,73.860869],[15.475098,73.860346],[15.475454,73.859435],[15.475771,73.858771],[15.476447,73.857858],[15.477567,73.857646],[15.478682,73.856234],[15.479158,73.855487],[15.479583,73.855128],[15.479996,73.85478],[15.481155,73.854195],[15.482432,73.853279],[15.483548,73.852239],[15.484341,73.850788],[15.484816,73.849711],[15.485292,73.848716],[15.48573,73.848176],[15.486447,73.847552],[15.487125,73.846887],[15.488395,73.844607],[15.491628,73.839692],[15.490901,73.839851],[15.490215,73.840495],[15.488487,73.842048],[15.488213,73.84243],[15.487876,73.842686],[15.487294,73.842991],[15.486956,73.842977],[15.486616,73.842661],[15.486397,73.841901],[15.486163,73.841284],[15.48573,73.840747],[15.48539,73.840511],[15.485098,73.840481],[15.484745,73.840499],[15.484284,73.840422],[15.482565,73.839783],[15.481291,73.839509],[15.480106,73.838766],[15.479552,73.837081],[15.479827,73.836313]]);
	 // RIBANDAR 403006
	 $scope.polyCoords.push([[15.497405,73.868714],[15.49674,73.868799],[15.495921,73.869247],[15.49534,73.870381],[15.495149,73.871432],[15.495,73.872847],[15.494889,73.874099],[15.493882,73.876084],[15.493143,73.876976],[15.492599,73.877746],[15.492054,73.878476],[15.491628,73.879367],[15.491085,73.880299],[15.490384,73.880989],[15.48972,73.881195],[15.488977,73.881199],[15.488311,73.881162],[15.485745,73.88134],[15.485278,73.881867],[15.484813,73.882799],[15.484428,73.884053],[15.484042,73.885105],[15.483955,73.885196],[15.483614,73.885551],[15.482718,73.886323],[15.482212,73.887053],[15.482059,73.88774],[15.482063,73.888548],[15.482065,73.888952],[15.481638,73.88964],[15.481226,73.891686],[15.481475,73.892814],[15.481874,73.893659],[15.482397,73.894849],[15.482795,73.895443],[15.483743,73.89663],[15.484614,73.897252],[15.485224,73.897688],[15.485774,73.898281],[15.48696,73.898306],[15.487506,73.898021],[15.488081,73.897548],[15.48846,73.898164],[15.488301,73.898526],[15.488062,73.899095],[15.487704,73.899997],[15.48764,73.90058],[15.487591,73.901647],[15.487957,73.902296],[15.488176,73.903154],[15.489028,73.904341],[15.488985,73.904425],[15.488912,73.90452],[15.489236,73.904534],[15.490515,73.904392],[15.491499,73.904286],[15.493073,73.904209],[15.49432,73.904135],[15.495595,73.903282],[15.496512,73.902972],[15.497528,73.902798],[15.498577,73.902488],[15.499821,73.902007],[15.500541,73.901732],[15.500863,73.901082],[15.501061,73.90068],[15.500857,73.899293],[15.501208,73.897225],[15.501171,73.896413],[15.500735,73.894553],[15.500718,73.893382],[15.501056,73.892816],[15.501077,73.891702],[15.501035,73.891172],[15.500965,73.89048],[15.501112,73.888196],[15.500956,73.88512],[15.501434,73.883782],[15.502558,73.881995],[15.503248,73.879932],[15.503835,73.878761],[15.504109,73.877898],[15.504472,73.876754],[15.504843,73.875416],[15.504998,73.874191],[15.504988,73.872132],[15.50493,73.871044],[15.50262,73.870705],[15.501718,73.870266],[15.500621,73.869949],[15.49964,73.869469],[15.498464,73.869072],[15.497405,73.868714]]);
	 // CHORAO 403102
	 $scope.polyCoords.push([[15.503571,73.854451],[15.503477,73.856448],[15.503364,73.858839],[15.503372,73.86039],[15.503898,73.865234],[15.50493,73.871044],[15.504988,73.872132],[15.504998,73.874191],[15.504843,73.875416],[15.504472,73.876754],[15.504109,73.877898],[15.504293,73.878023],[15.505554,73.879753],[15.506465,73.880861],[15.507245,73.881391],[15.509021,73.882851],[15.51306,73.887907],[15.519738,73.894462],[15.52379,73.896853],[15.52483,73.897632],[15.526218,73.899192],[15.52719,73.900185],[15.527816,73.901179],[15.528375,73.902459],[15.529071,73.903666],[15.529835,73.904446],[15.530596,73.90487],[15.530973,73.905494],[15.531662,73.905276],[15.534407,73.904378],[15.534654,73.904342],[15.535445,73.904372],[15.536088,73.90459],[15.536714,73.904688],[15.537308,73.904889],[15.537639,73.905261],[15.538341,73.906499],[15.538739,73.907709],[15.539177,73.906536],[15.539569,73.905473],[15.540262,73.904922],[15.540756,73.904372],[15.541414,73.903309],[15.541807,73.902349],[15.542399,73.901559],[15.543519,73.900117],[15.544012,73.899362],[15.544638,73.898674],[15.545398,73.898362],[15.545995,73.898428],[15.546661,73.899006],[15.547326,73.899412],[15.547923,73.899683],[15.547302,73.901362],[15.546776,73.902117],[15.546549,73.903247],[15.546423,73.904548],[15.545764,73.905269],[15.545073,73.906162],[15.543684,73.906649],[15.54266,73.90727],[15.542066,73.907888],[15.541538,73.908267],[15.54121,73.909022],[15.541219,73.910766],[15.540671,73.91366],[15.541623,73.914776],[15.541864,73.915673],[15.542953,73.917326],[15.544337,73.919583],[15.544642,73.919221],[15.545493,73.918777],[15.546798,73.917921],[15.549846,73.915261],[15.550694,73.914282],[15.551049,73.91394],[15.551875,73.913144],[15.552875,73.912039],[15.553875,73.911122],[15.554024,73.911121],[15.553519,73.909244],[15.555008,73.907225],[15.557096,73.905203],[15.557497,73.902574],[15.555435,73.902737],[15.552591,73.903371],[15.550421,73.904001],[15.549445,73.90362],[15.549805,73.900678],[15.550323,73.899283],[15.551366,73.89804],[15.552336,73.897262],[15.553385,73.897334],[15.554141,73.898567],[15.556457,73.89724],[15.558393,73.894832],[15.560253,73.892192],[15.561069,73.890563],[15.560992,73.889909],[15.560834,73.888553],[15.559522,73.885199],[15.558723,73.883158],[15.558177,73.881761],[15.557703,73.879149],[15.55756,73.878361],[15.555828,73.876591],[15.553427,73.875985],[15.550882,73.876541],[15.549332,73.876698],[15.549486,73.875158],[15.549803,73.873885],[15.55031,73.871676],[15.550662,73.870872],[15.550592,73.869803],[15.549965,73.867533],[15.549961,73.866831],[15.547693,73.866609],[15.54695,73.866847],[15.546239,73.867118],[15.545397,73.867257],[15.537163,73.868564],[15.533944,73.869532],[15.530494,73.870501],[15.527726,73.869566],[15.525188,73.868867],[15.52081,73.868416],[15.517138,73.862823],[15.515668,73.860958],[15.513036,73.859039],[15.510107,73.85774],[15.508606,73.857207],[15.506653,73.85629],[15.505224,73.855293],[15.503571,73.854451]]);
	 // TARVALEM 403103
	 $scope.polyCoords.push([[15.447487,74.165839],[15.446412,74.167024],[15.445792,74.167296],[15.445205,74.167602],[15.443901,74.168417],[15.442759,74.169062],[15.440475,74.170049],[15.439887,74.170321],[15.438353,74.170867],[15.437624,74.17076],[15.437907,74.169674],[15.436286,74.166932],[15.436404,74.16527],[15.436394,74.162522],[15.435543,74.160461],[15.435299,74.159987],[15.43481,74.158641],[15.435287,74.156791],[15.435597,74.155541],[15.435302,74.154293],[15.434476,74.153672],[15.433488,74.151387],[15.434166,74.148734],[15.434161,74.147248],[15.434154,74.145503],[15.433896,74.14363],[15.43359,74.1431],[15.431548,74.144438],[15.428683,74.144995],[15.427326,74.14539],[15.426123,74.146485],[15.425297,74.1475],[15.424015,74.147661],[15.422506,74.147667],[15.418657,74.14786],[15.417451,74.147921],[15.414512,74.148789],[15.412928,74.148796],[15.41127,74.149347],[15.409538,74.15021],[15.40803,74.150606],[15.406902,74.1517],[15.403935,74.156638],[15.399499,74.160704],[15.397764,74.160788],[15.395203,74.162122],[15.392649,74.165012],[15.391295,74.166184],[15.389186,74.167205],[15.387,74.167914],[15.386165,74.166594],[15.384271,74.164345],[15.382913,74.164506],[15.381859,74.165288],[15.37953,74.167866],[15.377948,74.168495],[15.375458,74.168582],[15.369205,74.165791],[15.363009,74.163481],[15.361044,74.16271],[15.360138,74.162403],[15.356653,74.158681],[15.35529,74.157442],[15.354231,74.156668],[15.352797,74.156674],[15.351667,74.157223],[15.350688,74.157538],[15.349782,74.157542],[15.348569,74.15599],[15.347506,74.154205],[15.346671,74.152885],[15.345519,74.151512],[15.344902,74.152258],[15.344545,74.152949],[15.343315,74.154068],[15.341715,74.155126],[15.339304,74.154969],[15.337825,74.155009],[15.33715,74.155144],[15.336316,74.155512],[15.335386,74.15598],[15.334455,74.156514],[15.334071,74.156814],[15.333819,74.156877],[15.333268,74.157016],[15.331903,74.157189],[15.330814,74.158353],[15.330398,74.158819],[15.329053,74.160051],[15.327705,74.160553],[15.326163,74.160858],[15.324681,74.159969],[15.324132,74.159407],[15.32342,74.158051],[15.322903,74.157357],[15.320343,74.156036],[15.319341,74.154714],[15.318565,74.153391],[15.317757,74.1524],[15.316338,74.151213],[15.315532,74.150586],[15.312725,74.147913],[15.311361,74.147025],[15.31151,74.148072],[15.311176,74.149778],[15.309672,74.150536],[15.307659,74.151549],[15.306444,74.151585],[15.304135,74.151625],[15.301051,74.151268],[15.301206,74.152255],[15.301361,74.153242],[15.301568,74.154697],[15.302785,74.156408],[15.303496,74.157601],[15.30437,74.162276],[15.30503,74.163418],[15.305649,74.167003],[15.306186,74.168938],[15.306297,74.17045],[15.305845,74.1711],[15.305849,74.172217],[15.306481,74.173079],[15.308203,74.175881],[15.308902,74.175915],[15.309845,74.175839],[15.311032,74.175618],[15.312045,74.17565],[15.313023,74.175574],[15.314242,74.174669],[15.314905,74.174342],[15.315988,74.174266],[15.317068,74.173685],[15.31757,74.172998],[15.318518,74.172534],[15.319357,74.172646],[15.320421,74.173102],[15.32165,74.173098],[15.326232,74.172676],[15.327182,74.172672],[15.3283,74.172668],[15.329586,74.172605],[15.330984,74.17283],[15.333723,74.172992],[15.335051,74.172898],[15.335458,74.173051],[15.336641,74.173601],[15.337056,74.173715],[15.33741,74.174057],[15.337529,74.174441],[15.33753,74.174723],[15.337315,74.174987],[15.336807,74.175373],[15.33622,74.175638],[15.33577,74.17572],[15.335399,74.175964],[15.335399,74.176166],[15.335557,74.176428],[15.336127,74.176891],[15.33709,74.177574],[15.337581,74.177916],[15.338112,74.17852],[15.338371,74.17967],[15.338378,74.181529],[15.338301,74.181934],[15.338145,74.182318],[15.337854,74.182519],[15.336597,74.184045],[15.336105,74.185029],[15.335841,74.185697],[15.335767,74.18609],[15.335921,74.186718],[15.336152,74.187502],[15.33623,74.187816],[15.336117,74.188327],[15.335776,74.188603],[15.334138,74.188413],[15.334407,74.189001],[15.335058,74.190255],[15.335175,74.190804],[15.335597,74.191745],[15.336168,74.19186],[15.336549,74.191859],[15.337195,74.191464],[15.338713,74.190241],[15.340159,74.189999],[15.342746,74.189283],[15.344552,74.189436],[15.346269,74.19049],[15.34677,74.192176],[15.346814,74.193904],[15.347166,74.196572],[15.348018,74.200417],[15.349396,74.202492],[15.349713,74.202868],[15.349915,74.201736],[15.350425,74.201131],[15.353568,74.200818],[15.354738,74.200813],[15.356203,74.201335],[15.357087,74.203142],[15.357827,74.204807],[15.360067,74.203061],[15.365719,74.198656],[15.366706,74.198824],[15.370661,74.199498],[15.373121,74.200528],[15.374009,74.202278],[15.375653,74.20422],[15.37812,74.207393],[15.3797,74.209141],[15.380652,74.211215],[15.381415,74.213291],[15.382745,74.215234],[15.384134,74.216463],[15.386155,74.218144],[15.387731,74.218658],[15.390047,74.218322],[15.390827,74.21864],[15.39079,74.218052],[15.391089,74.217058],[15.391249,74.215612],[15.392011,74.213567],[15.393215,74.21206],[15.395991,74.211026],[15.398852,74.210287],[15.402412,74.208008],[15.404521,74.205891],[15.408908,74.202179],[15.410715,74.20053],[15.412992,74.198037],[15.414832,74.197269],[15.416534,74.19655],[15.41948,74.195588],[15.421312,74.194835],[15.423007,74.1946],[15.423966,74.194672],[15.424853,74.195506],[15.42611,74.196413],[15.427958,74.197775],[15.429067,74.198608],[15.43032,74.198831],[15.431721,74.198826],[15.432458,74.198823],[15.434004,74.198208],[15.439011,74.196364],[15.4407,74.194684],[15.443525,74.190955],[15.443671,74.190934],[15.446039,74.190925],[15.448451,74.189956],[15.44887,74.189778],[15.448672,74.189232],[15.448505,74.188269],[15.448503,74.187595],[15.448688,74.187257],[15.449292,74.186315],[15.449732,74.185253],[15.45001,74.184059],[15.449163,74.182255],[15.448949,74.1811],[15.448992,74.180184],[15.449944,74.178831],[15.450804,74.177719],[15.4512,74.177308],[15.452315,74.175858],[15.452641,74.17552],[15.452999,74.175218],[15.453133,74.175131],[15.453246,74.174987],[15.453221,74.174505],[15.452938,74.173928],[15.452727,74.173568],[15.452398,74.172991],[15.451952,74.17239],[15.451622,74.171717],[15.451505,74.170567],[15.451395,74.170295],[15.450861,74.169063],[15.450549,74.168773],[15.44978,74.167994],[15.448253,74.1672],[15.44785,74.166825],[15.447487,74.165839]]);
	 // NEURA 403104
	 $scope.polyCoords.push([[15.461887,73.901224],[15.461704,73.900357],[15.460929,73.900608],[15.460038,73.901289],[15.458848,73.901973],[15.456351,73.90371],[15.455102,73.904517],[15.454392,73.905751],[15.454578,73.907166],[15.455298,73.908086],[15.455545,73.909808],[15.454592,73.91006],[15.453223,73.910621],[15.452033,73.911304],[15.450789,73.912186],[15.450625,73.911973],[15.450299,73.911744],[15.449852,73.911433],[15.44871,73.910309],[15.448078,73.909705],[15.447214,73.908738],[15.447018,73.909079],[15.446494,73.909693],[15.445199,73.911504],[15.444313,73.912052],[15.442801,73.912672],[15.441057,73.912681],[15.440431,73.912684],[15.439971,73.912856],[15.439346,73.913063],[15.438622,73.913135],[15.438406,73.913144],[15.437892,73.912018],[15.437592,73.911272],[15.436898,73.910698],[15.436602,73.910768],[15.435898,73.910941],[15.435272,73.910877],[15.434678,73.910574],[15.43428,73.910101],[15.434144,73.909218],[15.43414,73.908369],[15.433969,73.907181],[15.432816,73.906881],[15.432189,73.906749],[15.431694,73.906411],[15.431461,73.905903],[15.431294,73.905462],[15.431063,73.90526],[15.430766,73.905126],[15.430107,73.905129],[15.429647,73.905335],[15.42932,73.905711],[15.429091,73.906051],[15.428896,73.906562],[15.428634,73.906903],[15.428306,73.907075],[15.427943,73.907009],[15.427284,73.906842],[15.426988,73.906844],[15.426232,73.907187],[15.425774,73.907869],[15.425315,73.908245],[15.424692,73.908656],[15.423937,73.909169],[15.423414,73.910021],[15.423385,73.910769],[15.423454,73.911482],[15.423657,73.912568],[15.423496,73.913316],[15.423072,73.914167],[15.422517,73.914985],[15.422189,73.915327],[15.421008,73.91625],[15.420711,73.916676],[15.420714,73.917273],[15.420344,73.917705],[15.417766,73.919695],[15.416297,73.922965],[15.41611,73.924004],[15.414587,73.933608],[15.41652,73.935105],[15.418749,73.935918],[15.421604,73.936729],[15.425178,73.937078],[15.426222,73.937395],[15.426991,73.938252],[15.428213,73.93925],[15.429368,73.940822],[15.430172,73.941894],[15.431092,73.94519],[15.431555,73.947519],[15.432017,73.949704],[15.432769,73.952121],[15.433057,73.952499],[15.433504,73.953086],[15.434063,73.953621],[15.434795,73.954048],[15.435666,73.954474],[15.437719,73.954966],[15.438171,73.954928],[15.438657,73.954746],[15.439386,73.954527],[15.440533,73.954522],[15.441679,73.954085],[15.44275,73.953606],[15.442268,73.952059],[15.441095,73.949987],[15.441045,73.948879],[15.441218,73.947677],[15.441304,73.946845],[15.442196,73.946194],[15.44327,73.946189],[15.445242,73.946733],[15.445958,73.946729],[15.447166,73.946492],[15.448234,73.945332],[15.44881,73.944083],[15.449344,73.943341],[15.450548,73.941755],[15.451083,73.941884],[15.451795,73.941881],[15.452714,73.941968],[15.453575,73.942178],[15.454496,73.942448],[15.455178,73.942475],[15.45589,73.942472],[15.456334,73.942378],[15.456837,73.942069],[15.457078,73.941883],[15.457775,73.942112],[15.458364,73.942398],[15.459039,73.942771],[15.459574,73.943259],[15.460024,73.943662],[15.461968,73.943739],[15.462585,73.943881],[15.463622,73.943847],[15.464041,73.9437],[15.464121,73.942832],[15.464534,73.941239],[15.465147,73.940398],[15.466437,73.939503],[15.468582,73.937612],[15.469906,73.936069],[15.470812,73.93498],[15.470981,73.933696],[15.470716,73.933281],[15.470434,73.932939],[15.469976,73.932436],[15.469392,73.930947],[15.469999,73.929661],[15.470922,73.928644],[15.471848,73.927989],[15.472458,73.927461],[15.472851,73.925381],[15.472883,73.924784],[15.472756,73.923863],[15.472595,73.923231],[15.472173,73.92289],[15.471713,73.92188],[15.47104,73.920438],[15.470842,73.919927],[15.468436,73.920218],[15.467613,73.92067],[15.466919,73.920673],[15.466267,73.920363],[15.466235,73.920208],[15.466049,73.919304],[15.466038,73.91701],[15.465709,73.916396],[15.465029,73.915868],[15.464376,73.915368],[15.464074,73.914586],[15.464043,73.913775],[15.464498,73.912542],[15.464197,73.912124],[15.463654,73.911819],[15.462944,73.910843],[15.462806,73.910397],[15.463043,73.908969],[15.463581,73.908071],[15.464011,73.907229],[15.463734,73.906112],[15.46327,73.905555],[15.462917,73.905333],[15.462481,73.904859],[15.462178,73.903994],[15.461887,73.901224]]);
	 // VELGUEM 403105
	 $scope.polyCoords.push([[15.634837,74.092112],[15.633222,74.09292],[15.632676,74.093204],[15.632335,74.093418],[15.631925,74.093525],[15.631549,74.093492],[15.630453,74.092755],[15.628133,74.092008],[15.626488,74.092038],[15.623756,74.092491],[15.622624,74.09276],[15.621531,74.093401],[15.620265,74.093972],[15.618677,74.094145],[15.618317,74.094254],[15.617974,74.094103],[15.617253,74.093532],[15.616347,74.092914],[15.615138,74.092033],[15.613904,74.090747],[15.61281,74.089531],[15.612624,74.08934],[15.61179,74.089603],[15.610337,74.090856],[15.609485,74.091182],[15.609153,74.091506],[15.608529,74.091595],[15.607841,74.091512],[15.607445,74.09132],[15.606672,74.090743],[15.6059,74.090489],[15.604983,74.090385],[15.604234,74.090388],[15.603485,74.090542],[15.602431,74.091151],[15.601705,74.091692],[15.601,74.092577],[15.600315,74.093182],[15.599714,74.093786],[15.598946,74.094349],[15.598114,74.094589],[15.59745,74.095173],[15.595974,74.095803],[15.594834,74.097055],[15.592613,74.098935],[15.591332,74.099441],[15.590861,74.100524],[15.590192,74.101375],[15.589488,74.10296],[15.589045,74.104312],[15.588936,74.105084],[15.58842,74.106629],[15.588497,74.107362],[15.588847,74.108098],[15.588799,74.108322],[15.588481,74.109287],[15.588257,74.109549],[15.587956,74.109869],[15.587027,74.11091],[15.586769,74.11128],[15.586266,74.111957],[15.584136,74.113313],[15.583306,74.114118],[15.582705,74.114803],[15.582075,74.115369],[15.581388,74.115995],[15.58026,74.116684],[15.579745,74.117191],[15.579658,74.117306],[15.579253,74.116992],[15.578316,74.116996],[15.57753,74.116781],[15.576516,74.115596],[15.576302,74.114849],[15.576086,74.113696],[15.575777,74.112076],[15.575473,74.111485],[15.575048,74.110894],[15.57432,74.110243],[15.573686,74.110245],[15.57251,74.110594],[15.571635,74.11094],[15.570911,74.11113],[15.570098,74.111664],[15.569256,74.112634],[15.568141,74.113231],[15.566483,74.113799],[15.566266,74.113838],[15.566343,74.114152],[15.565329,74.115171],[15.563195,74.117333],[15.561444,74.118755],[15.55971,74.11948],[15.557572,74.120724],[15.554982,74.122034],[15.553881,74.122376],[15.551746,74.123715],[15.5509,74.12475],[15.550459,74.12563],[15.550204,74.12651],[15.549949,74.127695],[15.549028,74.12873],[15.54833,74.129764],[15.547849,74.130463],[15.547175,74.130706],[15.546578,74.130521],[15.545489,74.130633],[15.544685,74.130636],[15.543648,74.130748],[15.542585,74.130859],[15.540695,74.131423],[15.541029,74.13169],[15.541589,74.133136],[15.542649,74.135133],[15.543781,74.13652],[15.54521,74.138268],[15.546837,74.138909],[15.549073,74.1393],[15.549683,74.139545],[15.550463,74.140647],[15.551116,74.141399],[15.551831,74.142951],[15.552278,74.143773],[15.553611,74.144835],[15.554321,74.145198],[15.555599,74.145405],[15.556035,74.145313],[15.556535,74.144853],[15.556767,74.143693],[15.556735,74.143114],[15.556473,74.142465],[15.556797,74.141993],[15.557317,74.141823],[15.55787,74.141753],[15.55857,74.141683],[15.559173,74.141664],[15.559852,74.140535],[15.560045,74.139777],[15.56027,74.13907],[15.560578,74.138649],[15.561,74.138412],[15.561814,74.138207],[15.56253,74.138137],[15.563214,74.138369],[15.563753,74.138636],[15.564129,74.139054],[15.564884,74.140504],[15.565246,74.140001],[15.565765,74.139424],[15.566593,74.137555],[15.567249,74.136229],[15.567904,74.134822],[15.568834,74.134216],[15.569298,74.133612],[15.569313,74.132549],[15.569486,74.131846],[15.570574,74.130089],[15.571364,74.128361],[15.572409,74.127133],[15.574451,74.122651],[15.576212,74.120858],[15.576404,74.120606],[15.578554,74.121141],[15.579189,74.12188],[15.579683,74.123183],[15.579742,74.123569],[15.579685,74.123717],[15.579571,74.124044],[15.579257,74.124431],[15.578743,74.125175],[15.578658,74.125531],[15.578315,74.126096],[15.578461,74.126481],[15.578579,74.12746],[15.578526,74.128468],[15.577269,74.130402],[15.576555,74.131443],[15.576409,74.131873],[15.576614,74.132732],[15.577222,74.133946],[15.577542,74.135042],[15.577689,74.135724],[15.577835,74.136465],[15.577953,74.137087],[15.578043,74.138066],[15.578475,74.138479],[15.579885,74.138977],[15.581793,74.139266],[15.582603,74.139263],[15.583455,74.139427],[15.584267,74.139716],[15.585322,74.140047],[15.586052,74.140211],[15.586255,74.14024],[15.586314,74.140549],[15.586716,74.141711],[15.586718,74.142282],[15.58672,74.142806],[15.586458,74.143515],[15.585997,74.144338],[15.585359,74.145093],[15.584522,74.145895],[15.584371,74.146763],[15.58453,74.147743],[15.584931,74.148654],[15.58633,74.150405],[15.58746,74.151313],[15.588055,74.151749],[15.589255,74.152606],[15.58999,74.154177],[15.591405,74.156013],[15.591657,74.157325],[15.59173,74.157799],[15.59171,74.158508],[15.593394,74.158559],[15.594104,74.159359],[15.594638,74.160415],[15.594927,74.161984],[15.594931,74.163078],[15.595464,74.16377],[15.597094,74.164931],[15.598019,74.166314],[15.599084,74.167587],[15.601417,74.168228],[15.601454,74.16805],[15.601868,74.167649],[15.602115,74.167334],[15.602527,74.166561],[15.602719,74.166103],[15.603077,74.165645],[15.603434,74.165015],[15.60379,74.164214],[15.604065,74.163671],[15.604421,74.162756],[15.604668,74.162183],[15.604831,74.161583],[15.604994,74.160783],[15.605212,74.15984],[15.605567,74.158582],[15.605869,74.158095],[15.606364,74.157465],[15.606972,74.157291],[15.607829,74.157202],[15.608631,74.157198],[15.609655,74.157337],[15.6109,74.157418],[15.612052,74.157609],[15.612605,74.157521],[15.613075,74.157348],[15.613491,74.157161],[15.613504,74.156693],[15.613639,74.156458],[15.613795,74.156329],[15.613978,74.156118],[15.614125,74.15594],[15.614272,74.155692],[15.614415,74.155411],[15.614625,74.154964],[15.61468,74.154758],[15.614639,74.154593],[15.61415,74.153829],[15.613983,74.153427],[15.613847,74.153031],[15.613841,74.152641],[15.613805,74.152449],[15.613841,74.152097],[15.613984,74.151607],[15.61435,74.151196],[15.614986,74.150454],[15.615913,74.147623],[15.61593,74.146796],[15.615482,74.145068],[15.615687,74.144064],[15.615996,74.143693],[15.616764,74.143836],[15.617422,74.14439],[15.617822,74.144872],[15.618245,74.145112],[15.619303,74.146099],[15.622164,74.147224],[15.624368,74.147747],[15.625164,74.147744],[15.626265,74.147739],[15.627506,74.14771],[15.631061,74.148109],[15.631762,74.147864],[15.633072,74.147327],[15.63368,74.147034],[15.634265,74.146983],[15.635085,74.147222],[15.636915,74.147964],[15.637572,74.148155],[15.639064,74.148563],[15.639696,74.148463],[15.640139,74.14805],[15.640349,74.147759],[15.640793,74.147395],[15.641307,74.147078],[15.642079,74.146809],[15.642687,74.146661],[15.643389,74.146634],[15.652752,74.149772],[15.652879,74.148881],[15.65177,74.140715],[15.650755,74.138048],[15.650215,74.136627],[15.653338,74.122757],[15.653387,74.119792],[15.653654,74.119421],[15.646911,74.119507],[15.644303,74.116454],[15.642281,74.11397],[15.640897,74.113311],[15.6419,74.110947],[15.642031,74.110003],[15.642106,74.108588],[15.641995,74.1077],[15.641372,74.106482],[15.640578,74.104675],[15.637123,74.100888],[15.636339,74.099504],[15.636228,74.098756],[15.636253,74.098367],[15.63652,74.097756],[15.637157,74.096088],[15.636992,74.095145],[15.636575,74.093964],[15.634837,74.092112]]);
	 // SANT ESTEVAM 403106
	 $scope.polyCoords.push([[15.520471,73.935078],[15.520107,73.935521],[15.519193,73.936488],[15.518719,73.937081],[15.518303,73.937753],[15.517906,73.938562],[15.517378,73.939765],[15.516915,73.940338],[15.515853,73.941651],[15.515519,73.942729],[15.515232,73.943507],[15.514896,73.94436],[15.514853,73.945462],[15.514886,73.946154],[15.514907,73.946588],[15.515226,73.947388],[15.515591,73.947662],[15.516393,73.948008],[15.516542,73.948708],[15.516373,73.94891],[15.516156,73.949211],[15.515964,73.949488],[15.515894,73.950164],[15.515871,73.950515],[15.515873,73.95079],[15.516169,73.95179],[15.51697,73.952972],[15.518957,73.951108],[15.519919,73.95088],[15.520161,73.951152],[15.520285,73.951798],[15.520265,73.952594],[15.520269,73.953538],[15.520224,73.954284],[15.520252,73.95508],[15.520521,73.955874],[15.52086,73.956344],[15.521372,73.95751],[15.521615,73.958081],[15.521756,73.958398],[15.521834,73.958577],[15.522708,73.959865],[15.523672,73.960109],[15.525651,73.960919],[15.526037,73.960967],[15.527161,73.962087],[15.527911,73.962903],[15.528516,73.963422],[15.529528,73.963442],[15.531337,73.96398],[15.531891,73.964102],[15.532448,73.964646],[15.53298,73.96529],[15.533537,73.965759],[15.533947,73.965857],[15.53485,73.967071],[15.534944,73.967208],[15.537266,73.965754],[15.539912,73.963483],[15.541984,73.960837],[15.542964,73.958843],[15.54339,73.955035],[15.542631,73.953068],[15.542078,73.951542],[15.5371,73.944674],[15.536178,73.942981],[15.534706,73.94082],[15.533541,73.939128],[15.533107,73.937528],[15.532922,73.936595],[15.530468,73.937768],[15.530072,73.937991],[15.529677,73.93815],[15.529372,73.938215],[15.528187,73.939323],[15.527455,73.939358],[15.526444,73.940117],[15.526231,73.940055],[15.525803,73.9399],[15.525344,73.939682],[15.524793,73.939401],[15.524396,73.939341],[15.524029,73.93909],[15.523291,73.93796],[15.522892,73.937459],[15.522707,73.93705],[15.5224,73.936548],[15.522031,73.936077],[15.520471,73.935078]]);
	 // CUMBHARJUA 403107
	 $scope.polyCoords.push([[15.514886,73.946154],[15.514194,73.946712],[15.513497,73.947271],[15.51307,73.947939],[15.512806,73.948856],[15.512542,73.94994],[15.512357,73.950579],[15.512117,73.951052],[15.511015,73.951252],[15.510477,73.951199],[15.509912,73.951008],[15.50905,73.950846],[15.50819,73.95085],[15.50631,73.951443],[15.506042,73.951638],[15.505264,73.952003],[15.50454,73.952506],[15.502973,73.953438],[15.501853,73.955414],[15.501556,73.955941],[15.50132,73.95636],[15.501082,73.957166],[15.500816,73.957862],[15.500282,73.958725],[15.499163,73.960867],[15.498325,73.961434],[15.496498,73.961609],[15.494711,73.962383],[15.494099,73.961475],[15.493585,73.961025],[15.493121,73.960876],[15.492342,73.960931],[15.491928,73.961109],[15.491345,73.961363],[15.49088,73.961561],[15.490566,73.961694],[15.489715,73.961924],[15.48918,73.962329],[15.488475,73.962534],[15.48551,73.962674],[15.484219,73.962731],[15.483,73.962787],[15.482391,73.96284],[15.481904,73.962868],[15.480386,73.96237],[15.480249,73.962721],[15.479996,73.964029],[15.479917,73.964566],[15.48013,73.965084],[15.480457,73.965568],[15.480637,73.965751],[15.481126,73.966218],[15.481306,73.966535],[15.481454,73.966836],[15.481471,73.967188],[15.48144,73.967423],[15.481247,73.967775],[15.480339,73.967947],[15.479852,73.968134],[15.47927,73.968556],[15.477608,73.970725],[15.476966,73.972336],[15.475918,73.973866],[15.475493,73.974947],[15.475592,73.975432],[15.475627,73.975935],[15.474949,73.976776],[15.474139,73.980599],[15.474145,73.981939],[15.474246,73.98286],[15.474643,73.984612],[15.475103,73.985748],[15.475251,73.986183],[15.4754,73.986836],[15.475402,73.987322],[15.475357,73.988109],[15.475391,73.988478],[15.475344,73.98893],[15.475268,73.989249],[15.475573,73.989193],[15.476034,73.988943],[15.476525,73.989013],[15.476917,73.989021],[15.477628,73.988925],[15.477838,73.98881],[15.479628,73.987632],[15.479829,73.987631],[15.480551,73.987814],[15.482587,73.988446],[15.483179,73.988412],[15.484443,73.988612],[15.485017,73.988972],[15.485415,73.989067],[15.486414,73.989251],[15.486691,73.989179],[15.486655,73.989489],[15.486631,73.991058],[15.48654,73.991738],[15.486212,73.992128],[15.485916,73.992582],[15.485135,73.993201],[15.484854,73.993315],[15.483931,73.993757],[15.483292,73.994585],[15.482762,73.99538],[15.481808,73.995708],[15.481306,73.995678],[15.48031,73.99596],[15.480014,73.99635],[15.479561,73.996594],[15.478917,73.996484],[15.47843,73.996228],[15.477787,73.99615],[15.477254,73.996185],[15.476161,73.997015],[15.475896,73.997405],[15.475492,73.998216],[15.475229,73.999058],[15.474693,74.000321],[15.475319,74.000375],[15.475966,74.001011],[15.476834,74.001058],[15.477279,74.000775],[15.477749,74.000517],[15.479234,73.999972],[15.48025,73.999788],[15.480845,73.999683],[15.481762,73.99973],[15.482458,74.000008],[15.4837,74.000642],[15.484817,74.000969],[15.485265,74.001248],[15.485826,74.001275],[15.486822,74.002268],[15.487814,74.002212],[15.488532,74.00208],[15.489226,74.001975],[15.489697,74.001896],[15.490091,74.001433],[15.490363,74.001125],[15.490634,74.000919],[15.492939,74.000729],[15.494009,74.00144],[15.495153,74.002279],[15.495545,74.002999],[15.496018,74.003355],[15.496787,74.003556],[15.49845,74.003906],[15.499346,74.004257],[15.503799,74.003],[15.506148,74.002591],[15.507571,74.001908],[15.510755,73.99879],[15.511638,73.99799],[15.513129,73.995199],[15.513943,73.993498],[15.514509,73.99406],[15.516037,73.995743],[15.518033,73.997484],[15.518798,73.998385],[15.519856,73.999708],[15.520738,74.00073],[15.52185,74.000906],[15.523255,74.00102],[15.524952,74.001253],[15.526239,74.001307],[15.527528,74.001845],[15.528173,74.002143],[15.529051,74.002381],[15.52999,74.00304],[15.530693,74.003339],[15.530671,74.003752],[15.531561,74.0025],[15.531922,74.001982],[15.532004,74.000012],[15.532316,73.998744],[15.532677,73.998086],[15.533311,73.997755],[15.534081,73.997188],[15.534261,73.996844],[15.534892,73.995637],[15.535341,73.994556],[15.536153,73.993285],[15.537467,73.992388],[15.539323,73.990925],[15.540138,73.990217],[15.54077,73.989323],[15.541312,73.988664],[15.541438,73.986365],[15.541251,73.985194],[15.54043,73.984588],[15.538791,73.983799],[15.538015,73.983099],[15.538009,73.981739],[15.53837,73.981221],[15.539557,73.979272],[15.540782,73.978984],[15.542374,73.979352],[15.544146,73.979155],[15.544177,73.978149],[15.544185,73.977889],[15.544001,73.97728],[15.543265,73.975501],[15.542396,73.974239],[15.541166,73.973542],[15.539983,73.973266],[15.538302,73.973228],[15.535801,73.972724],[15.533108,73.97269],[15.530588,73.972256],[15.532594,73.970861],[15.535742,73.968379],[15.534944,73.967208],[15.53485,73.967071],[15.533947,73.965857],[15.533537,73.965759],[15.53298,73.96529],[15.532448,73.964646],[15.531891,73.964102],[15.531337,73.96398],[15.529528,73.963442],[15.528516,73.963422],[15.527911,73.962903],[15.527161,73.962087],[15.526037,73.960967],[15.525651,73.960919],[15.523672,73.960109],[15.522708,73.959865],[15.521834,73.958577],[15.521756,73.958398],[15.521615,73.958081],[15.521372,73.95751],[15.52086,73.956344],[15.520521,73.955874],[15.520252,73.95508],[15.520224,73.954284],[15.520269,73.953538],[15.520265,73.952594],[15.520285,73.951798],[15.520161,73.951152],[15.519919,73.95088],[15.518957,73.951108],[15.51697,73.952972],[15.516169,73.95179],[15.515873,73.95079],[15.515871,73.950515],[15.515894,73.950164],[15.515964,73.949488],[15.516156,73.949211],[15.516373,73.94891],[15.516542,73.948708],[15.516393,73.948008],[15.515591,73.947662],[15.515226,73.947388],[15.514907,73.946588],[15.514886,73.946154]]);
	 // N B VEREM 403109
	 $scope.polyCoords.push([[15.526155,73.79868],[15.525821,73.799042],[15.522668,73.8004],[15.520879,73.800323],[15.52024,73.801199],[15.519475,73.802055],[15.51876,73.802229],[15.51777,73.802235],[15.516504,73.802072],[15.515348,73.802079],[15.513534,73.802373],[15.512271,73.802664],[15.510954,73.80341],[15.509649,73.805812],[15.50841,73.807591],[15.508414,73.808356],[15.510141,73.810279],[15.510888,73.811201],[15.511397,73.811721],[15.512418,73.812923],[15.512937,73.815094],[15.512588,73.816094],[15.512189,73.816638],[15.511412,73.8176],[15.51085,73.818986],[15.51073,73.819517],[15.510199,73.820103],[15.510317,73.820899],[15.510724,73.822166],[15.510916,73.822372],[15.511478,73.821504],[15.513259,73.819301],[15.514214,73.818209],[15.517841,73.815996],[15.52314,73.810652],[15.524984,73.809314],[15.525651,73.809267],[15.526488,73.809994],[15.527242,73.810549],[15.527867,73.810588],[15.52899,73.810152],[15.529822,73.80976],[15.530278,73.80937],[15.530858,73.808635],[15.531189,73.808074],[15.531767,73.807124],[15.533178,73.805329],[15.533093,73.805204],[15.53209,73.804041],[15.531541,73.802969],[15.531039,73.802317],[15.530037,73.801434],[15.528944,73.800272],[15.52731,73.799627],[15.526155,73.79868]]);
	 // CORLIM IE
	 $scope.polyCoords.push([[15.503509,73.915206],[15.501858,73.915001],[15.500988,73.914325],[15.500161,73.913946],[15.499625,73.914076],[15.499174,73.914504],[15.498805,73.915017],[15.498472,73.914508],[15.498305,73.913998],[15.497767,73.913703],[15.496861,73.914048],[15.49608,73.914734],[15.495175,73.915334],[15.494521,73.916487],[15.494277,73.917255],[15.494237,73.917468],[15.493989,73.917427],[15.493823,73.917257],[15.493202,73.916707],[15.492746,73.916411],[15.492291,73.916201],[15.492002,73.91599],[15.491588,73.915864],[15.49101,73.915654],[15.490391,73.915657],[15.490186,73.915914],[15.490519,73.916636],[15.49164,73.917993],[15.491932,73.918587],[15.49132,73.920038],[15.490659,73.919914],[15.490411,73.919873],[15.488511,73.919329],[15.486983,73.919167],[15.48562,73.918833],[15.484671,73.918795],[15.483516,73.918801],[15.480697,73.918996],[15.477237,73.919367],[15.473074,73.919657],[15.470842,73.919927],[15.47104,73.920438],[15.471713,73.92188],[15.472173,73.92289],[15.472595,73.923231],[15.472756,73.923863],[15.472883,73.924784],[15.472851,73.925381],[15.472458,73.927461],[15.471848,73.927989],[15.470922,73.928644],[15.469999,73.929661],[15.469392,73.930947],[15.469976,73.932436],[15.470434,73.932939],[15.470716,73.933281],[15.470981,73.933696],[15.470812,73.93498],[15.469906,73.936069],[15.468582,73.937612],[15.466437,73.939503],[15.465147,73.940398],[15.464534,73.941239],[15.464121,73.942832],[15.464041,73.9437],[15.463622,73.943847],[15.462585,73.943881],[15.461968,73.943739],[15.460024,73.943662],[15.459574,73.943259],[15.459039,73.942771],[15.458364,73.942398],[15.457775,73.942112],[15.457078,73.941883],[15.456837,73.942069],[15.456334,73.942378],[15.45589,73.942472],[15.455178,73.942475],[15.454496,73.942448],[15.453575,73.942178],[15.452714,73.941968],[15.451795,73.941881],[15.451083,73.941884],[15.450548,73.941755],[15.449344,73.943341],[15.44881,73.944083],[15.448234,73.945332],[15.447166,73.946492],[15.445958,73.946729],[15.445242,73.946733],[15.44327,73.946189],[15.442196,73.946194],[15.441304,73.946845],[15.441218,73.947677],[15.441045,73.948879],[15.441095,73.949987],[15.442268,73.952059],[15.44275,73.953606],[15.443136,73.953432],[15.443761,73.953358],[15.446528,73.953201],[15.448893,73.95344],[15.450564,73.95379],[15.45182,73.954681],[15.453634,73.956071],[15.454506,73.95664],[15.455205,73.957354],[15.455977,73.959072],[15.456508,73.960971],[15.456728,73.963409],[15.457287,73.964016],[15.457916,73.964659],[15.460168,73.965375],[15.461767,73.965331],[15.462358,73.96522],[15.463364,73.964821],[15.464059,73.964745],[15.464689,73.964571],[15.465481,73.963985],[15.467142,73.962255],[15.467901,73.961031],[15.468246,73.960456],[15.468696,73.960166],[15.469251,73.959877],[15.469945,73.959407],[15.470778,73.959295],[15.473072,73.959032],[15.474869,73.959106],[15.476895,73.959825],[15.477972,73.960866],[15.478311,73.961077],[15.480386,73.96237],[15.481904,73.962868],[15.482391,73.96284],[15.483,73.962787],[15.484219,73.962731],[15.48551,73.962674],[15.488475,73.962534],[15.48918,73.962329],[15.489715,73.961924],[15.490566,73.961694],[15.49088,73.961561],[15.491345,73.961363],[15.491928,73.961109],[15.492342,73.960931],[15.493121,73.960876],[15.493585,73.961025],[15.494099,73.961475],[15.494711,73.962383],[15.496498,73.961609],[15.498325,73.961434],[15.499163,73.960867],[15.500282,73.958725],[15.500816,73.957862],[15.501082,73.957166],[15.50132,73.95636],[15.501556,73.955941],[15.501853,73.955414],[15.502973,73.953438],[15.50454,73.952506],[15.505264,73.952003],[15.506042,73.951638],[15.50631,73.951443],[15.50819,73.95085],[15.50905,73.950846],[15.509912,73.951008],[15.510477,73.951199],[15.511015,73.951252],[15.512117,73.951052],[15.512357,73.950579],[15.512542,73.94994],[15.512806,73.948856],[15.51307,73.947939],[15.513497,73.947271],[15.514194,73.946712],[15.514886,73.946154],[15.514853,73.945462],[15.514896,73.94436],[15.515232,73.943507],[15.515519,73.942729],[15.515853,73.941651],[15.516915,73.940338],[15.517378,73.939765],[15.517906,73.938562],[15.518303,73.937753],[15.518719,73.937081],[15.519193,73.936488],[15.520107,73.935521],[15.520471,73.935078],[15.519684,73.934438],[15.517346,73.932108],[15.516328,73.931393],[15.515477,73.930796],[15.510338,73.926859],[15.508116,73.924708],[15.507021,73.923533],[15.506946,73.923453],[15.504545,73.920283],[15.503509,73.915206]]);

	 // Polygon Centers
	 $scope.polyCenters = [
	 {name:'Himayat Nagar', lat:17.4376264, lng:78.4803954},
	 {name:'Panjagutta', lat:17.409548, lng:78.469749},
	 {name:'Bagum Bazar & Sultan Bazar', lat:17.388524, lng:78.473189},
	 {name:'L.B. Nagar / Gaddiannaram & Uppal Kalan & Malakpet ESTEVAM', lat:17.286061, lng:78.486889},
	 {name:'Secunderabad & Kapra & Malkajgiri & Quthbullapur & Alwal', lat:17.416418, lng:78.535540},
	 {name:'Kukatpally & Serilingampally & Ramachandra Puram / Patancheru', lat:17.468631, lng:78.475139},
	 {name:'Mehdipatnam & Rajendra Nagar', lat:17.392388, lng:78.409868}
	 ];*/


    // Center at which map should be focused
    $scope.mapCenter = [28.6228164,77.2028251];



	/*------------------------ Pie-chart on Google map ------------------------*/

    var pieChartType = "PieChart";

    var pieChartOptions = {
        width: 90,
        height: 60,
        chartArea: {left:10,top:10,bottom:0,height:"100%"},
        colorAxis: {colors: ['#aec7e8', '#1f77b4']},
        displayMode: 'regions',
        legend: 'none',
        backgroundColor: 'transparent',
        tooltip: {trigger: 'none'},
        pieSliceBorderColor : 'transparent'
    };

	/*var chart1 = {};
	 chart1.data = [
	 [ 'Aventis Pharma', 20000 ],
	 [ 'Glaxosmithkline', 20000 ],
	 [ 'Surya pharma', 10000 ]
	 ];

	 var chart2 = {};
	 chart2.data = [
	 [ 'Sun Pharma', 30000 ],
	 [ 'Phizer', 20000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];

	 var chart3 = {};
	 chart3.data = [
	 [ 'Sun Pharma', 30000 ],
	 [ 'Phizer', 70000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];

	 var chart4 = {};
	 chart4.data = [
	 [ 'Sun Pharma', 30000 ],
	 [ 'Phizer', 70000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];

	 var chart5 = {};
	 chart5.data = [
	 [ 'Sun Pharma', 20000 ],
	 [ 'Phizer', 50000 ],
	 [ 'Ranbaxy pharma', 60000 ]
	 ];


	 $scope.mapviewData = [
	 { map_chart:chart1, name:'Himayat Nagar', lat:17.4376264, lng:78.4803954},
	 { map_chart:chart2, name:'Panjagutta', lat:17.409548, lng:78.469749},
	 { map_chart:chart3, name:'Bagum Bazar & Sultan Bazar', lat:17.388524, lng:78.473189},
	 { map_chart:chart4, name:'L.B. Nagar / Gaddiannaram & Uppal Kalan & Malakpet ESTEVAM', lat:17.286061, lng:78.486889},
	 { map_chart:chart5, name:'Secunderabad & Kapra & Malkajgiri & Quthbullapur & Alwal', lat:17.416418, lng:78.535540},
	 // { map_chart:chart2, name:'Kukatpally & Serilingampally & Ramachandra Puram / Patancheru', lat:17.468631, lng:78.475139},
	 // { map_chart:chart1, name:'Mehdipatnam & Rajendra Nagar', lat:17.392388, lng:78.409868}
	 ];*/



    // Logic for preparing performance map
    function preparePerformanceMap(chartData){
        if(chartData[0].map_chart.type === undefined){
            $scope.percentArray = ($scope.unitOfMeasures.key === 'value') ? $scope.percentArray_Sales : $scope.percentArray_Units;
            $scope.mapCenter = [chartData[0].lat, chartData[0].lng];
            for(var i = 0; i < chartData.length; i++){
                chartData[i].map_chart.type = pieChartType;
                chartData[i].map_chart.options = angular.copy(pieChartOptions);     // Creates a copy of an Object
                $scope.percentArray.push([]);
                $scope.percentArray[i] = computePercentContribution(chartData[i].map_chart.data);
                chartData[i].map_chart.options.slices = chartData[i].map_chart.data.map(function(item){
                    if(item[2] === null){
                        return {'color':'#AAAAAA'};
                    }
                    return {'color':item[2]};
                });
                chartData[i].map_chart.data.unshift(['Company', 'Sales', 'Color']);
            }
            $scope.pie_charts = chartData;
        }
        else{
            $scope.pie_charts = chartData;
            $scope.percentArray = ($scope.unitOfMeasures.key === 'value') ? $scope.percentArray_Sales : $scope.percentArray_Units;
        }
    }


    // Calculate percent contribution for each element in array
    function computePercentContribution(sourceArray){
        var totalSum = 0;
        for(var i = 0; i < sourceArray.length; i++){
            totalSum += sourceArray[i][1];
        }
        var arr = [];
        arr.push({'name':'Market Value', 'totalValue':totalSum});
        var percent = 0;
        for(var i = 0; i < sourceArray.length; i++){
            percent = ((sourceArray[i][1] / totalSum) *100 ).toFixed(2);
            arr.push({'name':sourceArray[i][0], 'value':sourceArray[i][1], 'percent':percent, 'color':sourceArray[i][2], 'omitted':false});
        }
        return arr;
    }

	/* Pie chart zooming with map zoom */
    NgMap.getMap().then(function(map) {
        var old_current_zoom = null;
        $scope.zoomChanged = function(e) {
            var new_current_zoom = map.getZoom();
            if(new_current_zoom < old_current_zoom){
                pieChartOptions.height =  pieChartOptions.height -  new_current_zoom * 2;
                pieChartOptions.width = pieChartOptions.width -  new_current_zoom * 2;
            }
            else{
                pieChartOptions.height =  pieChartOptions.height +  new_current_zoom * 2;
                pieChartOptions.width = pieChartOptions.width +  new_current_zoom * 2;
            }
            old_current_zoom = new_current_zoom;
        }
    });


    /* Exclude/include a section in Pie chart when the corresponding legend in click */
    $scope.toggleSelectedSection = function(thisObj, index){
		var locVar = thisObj.$parent.$parent.item.map_chart.data[index];
		thisObj.$parent.elem.omitted = !(thisObj.$parent.elem.omitted);
		if(locVar[1] !== null){
			// remove the value at [1] & append it to [0] (i.e. Others) and set [1] to null
			locVar[0] = locVar[0] + '-' + locVar[1];
	        locVar[1] = null;
		}
		else{
			var locArr = locVar[0].split('-');
			// extract numeric value at the end of [0] (i.e. Others) & set it to [1]
			if(locArr.length > 1){
				locVar[1] = Number(locArr.pop());
				locVar[0] = locArr.join('');
			}
		}
	};


    /* Assign resize map function to globle rootScope */
    $rootScope.resizeMap = function(){
        NgMap.getMap({id: 'comparison-map'}).then(function(response){
            $timeout(function(){
                google.maps.event.trigger(response, 'resize');
            }, 1000);
        });
	};

}]);

angular.module("imsApp")
.controller("CompetitiveAnalysisController", ['$scope', '$state', 'userServices', 'companyServices', 'notify', 'savedSearhcesServices', '$window', '$rootScope', '$timeout',
											function($scope, $state, userServices, companyServices, notify, savedSearhcesServices, $window, $rootScope, $timeout){
	console.log('Competitive Analysis controller...', $state);

	/* Make $rootScope available in Local $scope */
	$scope.rootScope = $rootScope;

	/* Parse session data to object format */
	function sessionInfoToScope(){
		$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	}
	sessionInfoToScope();

	/* Static Data & Values */
	$scope.periodTypeColl = [
		{ type: "MAT", value: 'mat', active: false },
		{ type: "MTH", value: 'mth', active: true },
		{ type: "YTD-Finance", value: 'ytdf', active: false },
		{ type: "YTD-Calendar", value: 'ytdc', active: false }
	];

	// Comparison Type
	$scope.comparisonType = [
        { value: 'company', title: "Company vs Company" },
        { value: 'brand', title: "Brand vs Brand" }
    ];

    $scope.TherapyClassColl = [
		{ type: "Therapy Class 1", value: 'tc1' },
		{ type: "Therapy Class 2", value: 'tc2' },
		{ type: "Therapy Class 3", value: 'tc3' },
		{ type: "Therapy Class 4", value: 'tc4' }
	];

	/* Initialize Models & Variables */
	initVariables();
	function initVariables() {
		showLoader = "angular.element('.loader-backdrop').fadeIn();";
	  	hideLoader = "angular.element('.loader-backdrop').fadeOut();";

		$scope.fetchData = {};
		$scope.fetchData.CompanyID = $scope.sessionInfo.CompanyID;
		$scope.fetchData.GeographyFilter = 'city';
		if($scope.sessionInfo.PeriodType === undefined){
			$scope.fetchData.PeriodType = 'mth';
			updateSessionInfo(undefined, 'mth');
		}
		else{
			$scope.fetchData.PeriodType = $scope.sessionInfo.PeriodType;
		}
		$scope.fetchData.ComparisonType = 'company';
		$scope.comparisonData = $scope.fetchData.ComparisonType;
		$scope.fetchData.IsViewTen = true;
		$scope.userId = $scope.sessionInfo.UserID;
		$scope.tabType = 3;
		$scope.isCluster = false;
		$scope.selected = {};
		$scope.selected.Therapies = [];
		$scope.fetchData.TCCode = null;
		$scope.selected.selectedTypes = [];
		$scope.fetchData.includeTopSearches = 1;
		$scope.filteredCategoryValues = [];
		$scope.ComparisonTypeLabel = "Company";
		$scope.isPreset = false;
		$scope.RegExNumForm = /(\d)(?=(\d{2})+\d\.)/g;
		// Unit measures dropdown
		$scope.unitOfMeasuresColls = [
										{key:'value', label:'Values', divisor:1, unit:'', decimal:0},
										{key:'value', label:'Values(k)', divisor:1000, unit:'k', decimal:0},
										{key:'value', label:'Values(Cr)', divisor:10000000, unit:'Cr', decimal:2},
										{key:'unit', label:'Units', divisor:1, unit:'', decimal:0},
										{key:'unit', label:'Units(k)', divisor:1000, unit:'k', decimal:0},
										{key:'unit', label:'Units(Cr)', divisor:10000000, unit:'Cr', decimal:2}
									];
		$scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
        $scope.UOM_set_flag = false;
		// Toggle Chart Radios
		$scope.chartRadio = 'NoPatch';
		$scope.selectedPatchId = undefined;
	}


	/* Edit Session info */
	function updateSessionInfo(GeographyID, PeriodType){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		if(GeographyID !== undefined){ sessionInfo.GeographyID = GeographyID; }
		if(PeriodType !== undefined){ sessionInfo.PeriodType = PeriodType; }
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
	}

	/* Divisions
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.fetchData.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.fetchData.DivisonID = $scope.divisionColl[0].DivisionID;
				$scope.FetchGeographyValues();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();*/

	/* Geography */
	$scope.FetchGeographyValues = function (presetData) {
		userServices.GetGeographyValues($scope.fetchData.GeographyFilter, $scope.fetchData.CompanyID, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.geographyColl = response.data;
				if($scope.sessionInfo.GeographyID === undefined){
					$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
					updateSessionInfo($scope.geographyColl[0].GeographyID, undefined);
				}
				else{
					$scope.fetchData.GeographyID = $scope.sessionInfo.GeographyID;
				}
				$scope.FetchBricksByGeography(presetData);
			} else {
				$scope.geographyColl = false;
			}
		},
		function (rejection) {});
	};

	/* Division to which user's role is associated */
	$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
	$scope.FetchGeographyValues();


	/* Map Geography Id to Geography Name */
	function mapGeographyIdToName(){
		$scope.GeographyName = $scope.geographyColl.filter(function(item){ return (item.GeographyID === $scope.fetchData.GeographyID); })[0].GeographyValue;
	}


	/* Bricks */
	$scope.geographySelected = function(){
		updateSessionInfo($scope.fetchData.GeographyID, undefined);
        $scope.FetchTimePeriods();
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				$scope.selectedPatches = [];
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
		},
		function (rejection) {});
	};


	/* Bricks */
	$scope.FetchBricksByGeography = function (presetData) {
		if(presetData !== undefined){
			$scope.fetchData.GeographyID = JSON.parse(presetData).GeographyID;
		}
        $scope.FetchTimePeriods();
		userServices.GetBricksByGeography($scope.fetchData.GeographyFilter, $scope.fetchData.GeographyID, $scope.fetchData.CompanyID, $scope.fetchData.DivisonID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.bricksColl = response.data;
				if(response.data[0].IsCluster){
					$scope.isCluster = true;
				}
				else{
					$scope.isCluster = false;
				}
			} else {
				$scope.bricksColl = false;
			}
			// Fetch values for first comparison type
			if(presetData !== undefined){
				if(JSON.parse(presetData).TCLevel !== undefined){
					$scope.fetchTherapyListByTherapyCode('$scope.displayResult('+presetData+')', JSON.parse(presetData).TCLevel, JSON.parse(presetData).TCCode);
				}
				else{
					$scope.isPreset = true;
					$scope.fetchComparisonTypeValue('$scope.displayResult('+presetData+')');
				}
			}
			else{
				$scope.isPreset = false;
				$scope.fetchComparisonTypeValue('$scope.displayResult()');
			}
		},
		function (rejection) {});
	};


	/* Time Periods */
	$scope.FetchTimePeriods = function () {
		$scope.period = {};
		userServices.GetTimePeriods($scope.fetchData.GeographyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.timePeriods = response.data;
				$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
				$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
			} else {
				$scope.timePeriods = false;
			}
		},
		function (rejection) {});
	};

	/* Period Type changed or Selected */
	$scope.periodTypeSelected = function(event) {
		if(!event.dataItem.active){
			event.preventDefault();
		}
	};

	/* Period Type changed */
	$scope.periodTypeChanged = function(){
		updateSessionInfo(undefined, $scope.fetchData.PeriodType);
	};

	$scope.timeSelected = function(event){
		if($scope.period.startPeriod > $scope.period.endPeriod){
			$scope.period.endPeriod = $scope.timePeriods[0].TimePeriodID;
			$scope.period.startPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		}
	};


	// Comparison Type changed
	$scope.comparisonTypeChanged = function(){
		$scope.isPreset = false;
		if($scope.fetchData.ComparisonType === 'company'){
			$scope.fetchData.TCLevel = undefined;
			$scope.fetchData.TCCode = undefined;
			$scope.fetchComparisonTypeValue();
		}
		else{
			$scope.fetchData.TCLevel = 'tc1';
			$scope.fetchTherapyListByTherapyCode('$scope.fetchComparisonTypeValue()', undefined);
		}
	};


	// Fetch Therapy List for selected therapy class
	$scope.fetchTherapyListByTherapyCode = function(customRoutine, TCLevel, TCCode){
		$scope.fetchData.TCLevel = (TCLevel !== undefined) ? TCLevel : $scope.fetchData.TCLevel;

		companyServices.GetTherapyListByTherapyCode($scope.fetchData.TCLevel, $scope.sessionInfo.SessionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.therapyList = response.data;
				$scope.therapyList.unshift({'TherapyCode':'all', 'TherapyName':'All'});
				if(TCCode === undefined){
					$scope.fetchData.TCCode = $scope.therapyList[0].TherapyCode;
					$scope.selected.Therapies = [];
				}
				else{
					$scope.fetchData.TCCode = TCCode;
					$scope.selected.Therapies = ($scope.fetchData.TCCode !== 'all') ? $scope.fetchData.TCCode.split(',') : [];
				}

				if(TCCode !== undefined){
					$scope.isPreset = true;
					$scope.fetchComparisonTypeValue(customRoutine);
				}
				else if(customRoutine !== undefined){
					$scope.isPreset = false;
					eval(customRoutine);
				}
				else{
					$scope.isPreset = false;
					$scope.fetchComparisonTypeValue();
				}
			} else {
				$scope.therapyList = false;
			}
		},
		function (rejection) {});
	};


	/* Therapies selected from the list */
	$scope.therapiesSelected = function(isOpen){
		if(!isOpen){	// denoting that therapies have been selected by user
			$scope.isPreset = false;
			$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
			$scope.fetchComparisonTypeValue();
		}
	};

	/* Therapy removed from the list */
	$scope.therapiesRemoved = function($item, $model){
		$scope.isPreset = false;
		$scope.fetchData.TCCode = ($scope.selected.Therapies.length > 0) ? $scope.selected.Therapies.toString() : $scope.therapyList[0].TherapyCode;
		$scope.fetchComparisonTypeValue();
	};


	/* Map Therapy Id to Therapy Name */
	function mapTherapyIdToName(){
		$scope.TherapyName = $scope.therapyList.filter(function(item){ return (item.TherapyCode === $scope.fetchData.TCCode); })[0].TherapyName;
	}


	// Fetch Company or Brand List
	$scope.fetchComparisonTypeValue = function(customRoutine){
		var TherapyClass = ($scope.fetchData.TCLevel === undefined) ? null : $scope.fetchData.TCLevel;
		var selectedTherapy = ($scope.fetchData.TCCode === undefined) ? null : $scope.fetchData.TCCode;
		$scope.selected.selectedTypes = [];
		$scope.filteredCategoryValues = [];
		companyServices.GetComparisionTypeValue($scope.fetchData.ComparisonType, $scope.sessionInfo.SessionID, TherapyClass, selectedTherapy)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.comparisonColl = response.data;
				if($scope.isPreset){
					$scope.filteredCategoryValues = $scope.comparisonColl;
				}
				if(customRoutine !== undefined){
					eval(customRoutine);
				}
			} else {
				$scope.comparisonColl = false;
			}
		},
		function (rejection) {});
	};


	/* Filter Category values array */
	$scope.filterCategoryValues = function(searchQuery) {
		if(searchQuery !== ''){
			var regExpression = new RegExp(searchQuery, 'i');
			$scope.filteredCategoryValues = $scope.comparisonColl.filter(function(item){ return regExpression.test(item.ComparisonValue); });
		}
	};


	// Display Result
	$scope.displayResult = function (presetData) {
		if(presetData !== undefined){
			applyPresetData(presetData);
		}
		else{
			if($scope.fetchData.PeriodType === 'mth'){
				$scope.fetchData.PeriodStart = $scope.period.startPeriod;
				$scope.fetchData.PeriodEnd = $scope.period.endPeriod;
			}
			else{
				$scope.fetchData.PeriodStart = 0;
				$scope.fetchData.PeriodEnd = 0;
			}

			// Companies or Brands
			if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
				$scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
			}
			else{
				$scope.fetchData.ComparisionValue = 'all';
				$scope.fetchData.includeTopSearches = 1;
			}

			// Patches
			if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
				$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
			}
			else{
				$scope.fetchData.PatchIds = 'all';
			}

			$scope.selectedPatchId = undefined;
			fetchPatchesByCluster();
		}

	};

	// Get Patches by Clusters
	function fetchPatchesByCluster(){
		if($scope.isCluster && ($scope.fetchData.PatchIds !== 'all')){
			userServices.GetPatchIDsByClusterIDs($scope.fetchData.PatchIds, showLoader, hideLoader)
			.then(function (response) {
				if (response.data != null && response.status === 200) {
					$scope.fetchData.PatchIds = response.data;
					// Fetch Final Result
					$scope.FetchCompanyPerformance();
				} else {
					$scope.fetchData.PatchIds = '';
				}
			},
			function (rejection) {});
		}
		else{
			// Fetch Final Result
			$scope.FetchCompanyPerformance();
		}
	}


	/* Company Performance */
	$scope.FetchCompanyPerformance = function () {
        $scope.fetchData.SessionID = $scope.sessionInfo.SessionID;      // Append SessionID for authourisation & other purposes like color-codes
		companyServices.GetCompanyPerformance($scope.fetchData)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.CompAnalysisData = response.data;
				mapGeographyIdToName();
				$scope.comparisonData = $scope.fetchData.ComparisonType;
				// Discrete-Bar chart
                initDiscreteBarChart();

                resetUOM_MinValueLength($scope.CompAnalysisData.CompanyPerformance);
                $scope.unitOfMeasuresChanged();

                // Check for Comparison type
				if($scope.fetchData.ComparisonType === 'company'){
					$scope.ComparisonTypeLabel = 'Company';
					$scope.TherapyName = '';
				}
				else if($scope.fetchData.ComparisonType === 'brand'){
					$scope.ComparisonTypeLabel = 'Brand';
					mapTherapyIdToName();
				}
			} else {
				$scope.CompAnalysisData = false;
			}
		},
		function (rejection) {});
	};


	/* Select Preset functionality Begins */
	$scope.savePreset = function(){
		savedSearhcesServices.AddUpdateSavedSearches(preparePresetData())
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
				initializeOnPreset();
			    notify({
		            message: 'Presets saved Successfully',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			} else {

			}
		},
		function (rejection) {});
	};


	function preparePresetData () {
		var presetObject = {};
		presetObject.SearchKey = $scope.PresetName;
		presetObject.UserID = $scope.userId;
		presetObject.TabType = $scope.tabType;

		if($scope.fetchData.PeriodType === 'mth'){
			$scope.fetchData.PeriodStart = $scope.period.startPeriod;
			$scope.fetchData.PeriodEnd = $scope.period.endPeriod;
		}
		else{
			$scope.fetchData.PeriodStart = 0;
			$scope.fetchData.PeriodEnd = 0;
		}

		// Patches
		if($scope.selectedPatches !== undefined && $scope.selectedPatches.length > 0){
			$scope.fetchData.PatchIds = $scope.selectedPatches.toString();
		}
		else{
			$scope.fetchData.PatchIds = 'all';
		}

		// Companies or Brands
		if($scope.selected.selectedTypes !== undefined && $scope.selected.selectedTypes.length > 0){
			$scope.fetchData.ComparisionValue = $scope.selected.selectedTypes.toString();
		}
		else{
			$scope.fetchData.ComparisionValue = 'all';
			$scope.fetchData.includeTopSearches = 1;
		}

        // Remove SessionID before saving preset
        if($scope.fetchData.SessionID !== undefined){
            delete $scope.fetchData.SessionID;
        }
		presetObject.SearchData = JSON.stringify($scope.fetchData);
		return presetObject;
	}


	/* Get Presets */
	function LoadPresets(){
		savedSearhcesServices.GetSavedSearchesbyUser($scope.userId, $scope.tabType)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.presetsColl = response.data;
			} else {
				$scope.presetsColl = false;
			}
		},
		function (rejection) {});
	}
	LoadPresets();

	$scope.applySelectedPreset = function() {
		angular.element('.loader-backdrop').fadeIn();
		var presetData = $scope.presetsColl.filter(function(item){ return (item.SearchKeyID === $scope.selectedPreset); })[0].SearchData;
		var presetData_obj = JSON.parse(presetData);
		$scope.fetchData.GeographyFilter = presetData_obj.GeographyFilter;
		$scope.fetchData.DivisonID = presetData_obj.DivisonID;
		$scope.fetchData.ComparisonType = presetData_obj.ComparisonType;
		$scope.FetchGeographyValues(presetData);
	};


	// Apply Preset data to fetchData
	function applyPresetData(presetData){
		$scope.fetchData = presetData;
		updateSessionInfo($scope.fetchData.GeographyID, $scope.fetchData.PeriodType);

		if($scope.fetchData.PeriodStart !== 0){
			$scope.period.startPeriod = $scope.fetchData.PeriodStart;
			$scope.period.endPeriod = $scope.fetchData.PeriodEnd;
		}

		if($scope.fetchData.PatchIds !== 'all'){
			$scope.selectedPatches = $scope.fetchData.PatchIds.split(',').map(Number);
		}
		else{
			$scope.selectedPatches = [];
		}

		if($scope.fetchData.ComparisionValue !== 'all'){
		     $scope.selected.selectedTypes = $scope.fetchData.ComparisionValue.split(',').map(Number);
		}
		else{
			$scope.selected.selectedTypes = [];
		}

		angular.element('.loader-backdrop').fadeOut();
	}


	// Initialize filter choices on preset
	function initializeOnPreset(){
		LoadPresets();
		$scope.PresetName = '';
		$scope.SavePresetFlag = false;
		$scope.fetchData.GeographyFilter = 'city';
		$scope.fetchData.DivisonID = $scope.sessionInfo.DivisionID;
		$scope.fetchData.GeographyID = $scope.geographyColl[0].GeographyID;
		$scope.selectedPatches = [];
		$scope.fetchData.PeriodType = $scope.sessionInfo.PeriodType;
		$scope.period.startPeriod = $scope.timePeriods[0].TimePeriodID;
		$scope.period.endPeriod = $scope.timePeriods[$scope.timePeriods.length - 1].TimePeriodID;
		$scope.fetchData.ComparisonType = 'company';
		$scope.selected.selectedTypes = [];
		$scope.fetchData.TCLevel = null;
		$scope.selected.Therapies = [];
		$scope.fetchData.TCCode = null;
		$scope.fetchData.IsViewTen = true;
		$scope.fetchData.includeTopSearches = 1;
		$scope.filteredCategoryValues = [];
		$scope.ComparisonTypeLabel = "Company";
		$scope.isPreset = false;
	}
	/* Select Preset functionality Ends */


	/*--------------------- Divide a number(n) by (d) and round upto (x) decimal places ---------------------*/
	$scope.DivideAndRound = function(n, d, x) {
		if(x === 0){
			n = (n/d).toFixed(1);
			n = (n.replace($scope.RegExNumForm, '$1,'));
			n = n.substring(0, (n.length-2));
		}
		else{
			n = (n/d).toFixed(x);
			n = (n.replace($scope.RegExNumForm, '$1,'));
		}
		return n;
	};


    /* Reset UOM based on Min value length */
    function resetUOM_MinValueLength(data) {
        var standardLen = 5;
        if(!($scope.UOM_set_flag)){
            var formattedArray = data.filter(function(item){ return (item.TotalSales >= 1); }).map(function(item){ return (item.TotalSales); });
            var minVal = (formattedArray.length > 0) ? Math.min.apply(Math, formattedArray) : 1;
            var minLen = Math.ceil(Math.log(minVal + 1) / Math.LN10);

            if(minLen <= standardLen){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[0];
            }
            else if((minLen > standardLen) && (minLen <= (standardLen + 2))){
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[1];
            }
            else{
                $scope.unitOfMeasures = $scope.unitOfMeasuresColls[2];
            }
        }
    }


	/* Unit of Measures Changed in Market Share */
	$scope.unitOfMeasuresChanged = function(inputObj) {
        // Set a flag to denote that UOM has been changed by User
        if((inputObj !== undefined) && (inputObj.uomFlag !== undefined)){
            eval(inputObj.uomFlag);
        }

		// Prepare a Figure Qualifier
	    $scope.figureQualifier = ($scope.unitOfMeasures.unit !== '') ? (" ("+$scope.unitOfMeasures.unit+")") : '';

		if($scope.unitOfMeasures.key === 'value'){
			$scope.companyPerformance = $scope.CompAnalysisData.CompanyPerformance;

			// Descrete Bar Chart
            $scope.discrete_bar.options.chart.y = function(d) { return d.TotalSales; };
            $scope.discrete_bar.options.chart.yAxis.axisLabel = 'Sales Values ' + $scope.figureQualifier;
			$scope.discrete_bar.data[0].values = $scope.companyPerformance.filter(function(item){ return (item.CompanyID > 0); });
		    $scope.apiDiscreteBar.update();

		    // Pie-Chart
		    initPieChart($scope.CompAnalysisData.TotalSalesValue, 'TotalSales');

		    // Data Grid
			prepareDataGrid_V();

			// Multi-bar chart & Data Grid
			if($scope.selectedPatchId === undefined){
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonByPatchSales;
				$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
			}
			else{
				mutate_ComparisionByPatch_Value();
			}
		}
		else{
			$scope.companyPerformance = $scope.CompAnalysisData.CompanyPerformanceUnit;

			// Descrete Bar Chart
            $scope.discrete_bar.options.chart.y = function(d) { return d.TotalUnits; };
            $scope.discrete_bar.options.chart.yAxis.axisLabel = 'Sales Units ' + $scope.figureQualifier;
			$scope.discrete_bar.data[0].values = $scope.companyPerformance.filter(function(item){ return (item.CompanyID > 0); });
		    $scope.apiDiscreteBar.update();

		    // Pie-Chart
		    initPieChart($scope.CompAnalysisData.TotalSalesUnit, 'TotalUnits');

		    // Data Grid
			prepareDataGrid_U();

			// Multi-bar chart
			if($scope.selectedPatchId === undefined){
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonByPatchUnit;
				$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
			}
			else{
				mutate_ComparisionByPatch_Unit();
			}
		}
	};


	// Prepare Grid Structure (Company comparison)
	function prepareDataGrid_V(){
		$scope.performanceColumns = [
			{ title: 'Rank', field: 'Rank', headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width: "50px" },
			{ title: 'Company Name', field: 'CompanyName',headerTemplate: $scope.ComparisonTypeLabel+" <span class='k-icon k-i-sort-desc'></span>", width:"280px;"},
			{ title: 'Total Value (CR)', field: 'TotalSales', headerTemplate: "Total Value "+$scope.figureQualifier+"<span class='k-icon k-i-sort-desc'></span>",
				template: "#:kendo.toString(TotalSales/"+$scope.unitOfMeasures.divisor+", 'n"+$scope.unitOfMeasures.decimal+"')#", width: "130px" },
			{ title  : 'Market Share', field: 'MarketShare',headerTemplate: "Market Share <span class='k-icon k-i-sort-desc'></span>", template:"<span>#:kendo.toString(MarketShare, 'n')# %</span>", width: "130px"}
		];
	}

	function prepareDataGrid_U(){
		$scope.performanceColumns = [
			{ title: 'Rank', field: 'Rank', headerTemplate: "Rank <span class='k-icon k-i-sort-desc'></span>", template: "<span ng-if='#=Rank# > 0'>#=Rank#</span> <span ng-if='#=Rank# === 0'>NA</span>", width: "50px" },
			{ title: 'Company Name', field: 'CompanyName',headerTemplate: $scope.ComparisonTypeLabel+" <span class='k-icon k-i-sort-desc'></span>", width:"280px;"},
			{ title: 'Total Units (CR)', field: 'TotalUnits', headerTemplate: "Total Value "+$scope.figureQualifier+"<span class='k-icon k-i-sort-desc'></span>",
				template: "#:kendo.toString(TotalUnits/"+$scope.unitOfMeasures.divisor+", 'n"+$scope.unitOfMeasures.decimal+"')#", width: "130px" },
			{ title  : 'Market Share', field: 'MarketUnitShare',headerTemplate: "Market Share <span class='k-icon k-i-sort-desc'></span>", template:"<span>#:kendo.toString(MarketUnitShare, 'n')# %</span>", width: "130px"}
		];
	}


	/* Patch Id selected */
	$scope.patchIdSelected = function(event){
		if(event.item === undefined){
			$scope.selectedPatchId = undefined;
			if($scope.unitOfMeasures.key === 'value'){
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonByPatchSales;
				$scope.multibar.options.chart.yAxis.axisLabel = 'Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
			}
			else{
				$scope.multibar.data = $scope.CompAnalysisData.CompanyComparisonByPatchUnit;
				$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
				$scope.apiMultiBar.update();
			}
		}
		else{
			$scope.selectedPatchId = event.item.PatchID;
			if($scope.unitOfMeasures.key === 'value'){
				mutate_ComparisionByPatch_Value();
			}
			else{
				mutate_ComparisionByPatch_Unit();
			}
		}
	};

	/* Mutate "GrouChartSalesPatch" array for Sales data */
	function mutate_ComparisionByPatch_Value(){
		var dataArr = $scope.CompAnalysisData.CompanyComparisonByPatchSales;
		$scope.multibar.data = [];
		for(var i = 0; i < dataArr.length; i++){
			$scope.multibar.data.push({"key":dataArr[i].key, "ColorCode":dataArr[i].ColorCode, "values":[]});
			$scope.multibar.data[i].values = dataArr[i].values.filter(function(item){
								return (item.z === $scope.selectedPatchId);
							});
		}
		$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		$scope.apiMultiBar.update();
	}

	/* Mutate "GrouChartSalesPatch" array for Units data */
	function mutate_ComparisionByPatch_Unit(){
		var dataArr = $scope.CompAnalysisData.CompanyComparisonByPatchUnit;
		$scope.multibar.data = [];
		for(var i = 0; i < dataArr.length; i++){
			$scope.multibar.data.push({"key":dataArr[i].key, "ColorCode":dataArr[i].ColorCode, "values":[]});
			$scope.multibar.data[i].values = dataArr[i].values.filter(function(item){
								return (item.z === $scope.selectedPatchId);
							});
		}
		$scope.multibar.options.chart.yAxis.axisLabel ='Sales ' + $scope.figureQualifier;
		$scope.apiMultiBar.update();
	}


	/* Charts & Graphs initializations */
	$scope.competCombo1 = [
	                        { field: "All Head India Field" },
	                        { field: "All Head India Field" },
							{ field: "All Head India Field" },
							{ field: "All Head India Field" }
                        ];

	$scope.competCombo2 = [
                            { period: "Mat" },
                            { period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" }
                        ];


	$scope.myTeam = [
						{name:"Pradeep Desai", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer1", path:"content/img/avatars.png"},
						{name:"Ashish Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer2", path:"content/img/avatars2.png"},
						{name:"Jayshankar Krish", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer3", path:"content/img/avatars3.png"},
						{name:"Shivraj Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer4", path:"content/img/avatars.png"}
				  	];

	$scope.gridColumns = [
							 { field: "name", title:"Name", headerTemplate: "Name <span class='k-icon k-i-sort-desc'></span>", template: "<div class='customer-photo-competitive'" + "style='background-image: url(#:path#);'></div>"
							 	+"<a class='competitve-user-list' href='javascript:void(0)' ui-sref='user.competitiveteam'><div class='customer-name-competitive'>#:name#</div></a>", width:"310px;"},
						     { field: "designation", title:"Team Designation", headerTemplate: "Team Designation <span class='k-icon k-i-sort-desc'></span>",width:"147px" },
						     { field: "target", title:"Sales Target",headerTemplate: "Sales Target <span class='k-icon k-i-sort-desc'></span>"},
							 { field: "acheived", title:"Sales Acheived",headerTemplate: "Sales Acheived <span class='k-icon k-i-sort-desc'></span>", template:"<div class='qwer'>#:acheived#</div>" + "<i class='fa fa-caret-up' aria-hidden='true'></i>"},
							 { field: "chart", title:"Chart",headerTemplate: "Chart <span class='k-icon k-i-sort-desc'></span>", template:"<div class='#:piechart#'" + "style='max-width: 100px; height:100px;margin: 0 auto;'></div>"}
						];


    /* ============================================ NVD3 - Discrete Bar Chart ============================================ */

	function initDiscreteBarChart() {
		$scope.discrete_bar = {};

		$scope.discrete_bar.data = [
            { values: [] }
	    ];

	    $scope.discrete_bar.options = {
	        chart: {
	            type: 'discreteBarChart',
	            height: 450,
	            margin: {
	                top: 40,
	                right: 20,
	                bottom: 50,
	                left: 80
	            },
	            x: function (d) { return d.CompanyName; },
	            y: function (d) { return d.TotalSales; },
                showLegend: false,
	            showValues: true,
	            valueFormat: function (d) {
	                return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
	            },
	            duration: 500,
	            xAxis: {
	                axisLabel: 'Company Name'
	            },
	            showXAxis: false,
	            yAxis: {
	                axisLabel: 'Sales Values',
	                axisLabelDistance: 20,
	                tickFormat: function(d){
						return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
					}
	            },
	            noData: 'No Data to display',
                color: function (d, i) {
                    return d.ColorCode;
                },
	            tooltip: {
                	valueFormatter: function (d, i) {
						return ($scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || ''));
					}
                }
	        }
	    };
	}


	/* ============================================ NVD3 - Pie Chart ============================================ */
	function initPieChart(totalValue, y_variable) {
	    $scope.pieChart1Options = {
	        chart: {
	            type: 'pieChart',
	            height: 400,
	            x: function (d) { return d.CompanyName; },
	            y: function (d) { return d[y_variable]; },
	            showLabels: true,
	            labelType: 'percent',
	            duration: 500,
	            labelThreshold: 0.01,
	            labelSunbeamLayout: true,
	            showLegend: true,
	            donut:true,
                donutRatio: 0.35,
	            title:$scope.DivideAndRound(totalValue, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || ''),
                color: function(d,i){
                    return d.ColorCode;
                },
                tooltip: {
                	valueFormatter: function (d, i) {
						return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
					}
                }
	        }
	    };
	}


	/* ============================================ NVD3 - Multibar Chart ============================================ */
	initMultiBarChart();
	function initMultiBarChart(){
		$scope.multibar = {};
		$scope.multibar.data = [];
		$scope.multibar.options = {
            chart: {
                type: 'multiBarChart',
                height: 480,
                margin: {
                    top: 40,
                    right: 25,
                    bottom: 140,
                    left: 75
                },
                clipEdge: true,
                //staggerLabels: true,
                showLegend: true,
                legend: {
                	maxKeyLength: 25
                },
                duration: 500,
                stacked: true,
                xAxis: {
                    axisLabel: 'Patches',
                    showMaxMin: false,
                    tickFormat: function(d){
                        return (d.length > 15) ? (d.substr(0,14) + '...') : d;
                    },
                    staggerLabels: true,
                    axisLabelDistance: 15
                },
                yAxis: {
                    axisLabel: 'Sales (k)',
                    axisLabelDistance: 12,
                    tickFormat: function(d){
                        return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal);
                    }
                },
                stacked: false,
                noData: 'No Data to display',
                color: function (d, i) {
                    return d.ColorCode;
                },
                tooltip: {
                	valueFormatter: function (d, i) {
						return $scope.DivideAndRound(d, $scope.unitOfMeasures.divisor, $scope.unitOfMeasures.decimal) + ' ' + ($scope.unitOfMeasures.unit || '');
					}
                }
            }
        };

        $scope.multibar.data = [];
   }


	/* Refresh charts on toggle */
	$scope.chartRadioChanged = function(){
		if($scope.chartRadio === 'NoPatch'){
			$timeout(function(){
				$scope.apiDiscreteBar.refresh();
				$scope.apiPie1.refresh();
			}, 100);
		}
		else if($scope.chartRadio === 'ByPatch'){
			$timeout(function(){
				$scope.apiMultiBar.refresh();
			}, 100);
		}
	};

    /* Assign function for refreshing all charts and graphs to rootScope */
    $rootScope.refreshCharts_CompAn = function(){
        $timeout(function(){
            $scope.apiDiscreteBar.refresh();
            $scope.apiPie1.refresh();
			$scope.multibar.refresh();
        }, 500);
    };


	/* Document Ready starts */
	angular.element(document).ready(function(){
		/* Angular Provision for Tab Panel */
		$scope.showTab = function(id1, id2){
			angular.element(id1).fadeOut(500, function(){
				angular.element(id2).fadeIn(function(){
					if(id1 === '#tabs-4-tab-2'){
						// $scope.apiPie2.refresh();
					}
				});
			});
		}

		// Default Prevention function
		$scope.defaultPrevention = function(event){
			event.preventDefault();
		}

		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);

        // angular.element("#tabTable li").on('click',function(){
            // angular.element(this).closest('ul').find('li').removeClass('selected');
            // angular.element(this).addClass('selected');
        // });

	});
	/* Document Ready ends */


	/*$scope.window_onload = function() {
		//load a svg snippet in the canvas with id = 'drawingArea'
		var _svg = document.getElementById('svgChart');
		var _canvas = document.getElementById('canvas');
		canvg(_canvas, _svg.innerHTML);
		var imgData = canvas.toDataURL('image/png');
		var doc = new jsPDF('p', 'mm');
        doc.addImage(imgData, 'PNG', 10, 10);
        doc.save('sample-file-1.pdf');
	};*/

}]);


angular.module("imsApp")
.controller("CompetitiveTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Competitive Team controller...', $state);
	
	angular.element(document).ready(function(){
		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);
			      
        angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
        });
        
        initHighChart();
        
        initPieChart();
        initPieCharts_Grid();
	});
	
	/* Angular Provision for Tab Panel */
	$scope.showTab = function(id1, id2){
		angular.element(id1).fadeOut(500, function(){
			angular.element(id2).fadeIn();
		});
	}
	
	$scope.competCombo1 = [
                            { feildHead: "Jayshankar Krish" },
                            { feildHead: "Sachin Mule" },
							{ feildHead: "Pradeep Desai" },
							{ feildHead: "Ashish Patel" }
                        ];
                        
	$scope.competCombo2 = [
                            { zonal: "ALl Managers Sales Performance" },
                            { zonal: "Top 20" },
							{ zonal: "Top 20" },
							{ zonal: "Top 20" }
                        ];
                        
	$scope.competCombo3 = [
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" }
	];
	
	$scope.competCombo4 = [
							{ period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" }
	];
	
   	$scope.dataSource = [
							{ city: "New Delhi" },
                            { city: "Maharashtra" },
							{ city: "Chennai" },
							{ city: "Kolkata" }
                        ];
                        
	$scope.dataSource2 = [
                            { comparision: "Company to Company" },
                            { comparision: "Company to Company" },
							{ comparision: "Company to Company" },
							{ comparision: "Company to Company" }
                        ];
                        
	$scope.dataSource4 = [
                            { period: "New Delhi" },
                            { period: "Maharashtra" },
							{ period: "Chennai" },
							{ period: "Kolkata" }
                        ];
                        
	$scope.teamPerformance = [
							{name:"Pradeep Desai", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer1", path:"content/img/avatars.png"},
							{name:"Ashish Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer2", path:"content/img/avatars2.png"},
							{name:"Jayshankar Krish", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer3", path:"content/img/avatars3.png"},
							{name:"Shivraj Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer4" , path:"content/img/avatars.png"}
				  ];
				  
	$scope.comparision = [
					{ rank: "2", company:"Torrent Pharma", sales:"Rs. 8,25,26,000", market:"18%" },
					{ rank: "4", company:"Biocon", sales:"Rs. 8,25,26,000", market:"4%" },
					{ rank: "5", company:"Avantis Pharma", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "8", company:"Surya Pharma", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "12", company:"GlaxoSmithKline", sales:"Rs. 8,25,26,000", market:"18%" },
					{ rank: "13", company:"Glenmark", sales:"Rs. 8,25,26,000", market:"32%" },
					{ rank: "16", company:"Divis Labs", sales:"Rs. 8,25,26,000", market:"9%" },
					{ rank: "24", company:"Orchid Chemical", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "36", company:"Abbott India", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "75", company:"Sterling Bio", sales:"Rs. 8,25,26,000", market:"24%" }
				  ];

	$scope.gridColumns = [
				 { field: "name", title:"Name",headerTemplate: "Name<span class='k-icon k-i-sort-desc'></span>",template: "<div class='customer-photo-competitive'" + "style='background-image: url(#:path#);'></div>" + "<div class='customer-name-competitive'>#:name#</div>", width:'300px'},
			     { field: "designation", title:"Team Designation",headerTemplate: "Team Designation<span class='k-icon k-i-sort-desc'></span>", width:"170px" },
			     { field: "target", title:"Sales Target", headerTemplate: "Sales Target<span class='k-icon k-i-sort-desc'></span>" },
				 { field: "acheived", title:"Sales Acheived",headerTemplate: "Sales Acheived <span class='k-icon k-i-sort-desc'></span>" , template:"<div class='qwer'>#:acheived#</div>" + "<i class='fa fa-caret-up' aria-hidden='true'></i>", width:'' },
				 { field: "chart", title:"Chart",headerTemplate: "Chart <span class='k-icon k-i-sort-desc'></span>",  template:"<div class='#:piechart#'" + "style='max-width: 100px; height:100px;margin: 0 auto;'></div>"}
				];
					
	 $scope.gridColumns2 = [
		{ title: 'Rank', field: 'rank',headerTemplate: "Rank<span class='k-icon k-i-sort-desc'></span>", width:"80px"},
		{ title: 'Company', field: 'company',headerTemplate: "Company<span class='k-icon k-i-sort-desc'></span>", width:"180px" },
		{ title: 'Sales Volume', field: 'sales',headerTemplate: "Sales Volume<span class='k-icon k-i-sort-desc'></span>", width:"180px" },
		{ title  : 'Market Share', field: 'market',headerTemplate: "Market Share<span class='k-icon k-i-sort-desc'></span>" }
	];

	$scope.competitiveteamCompanies=[
		{comp:"Aventis Pharma"},
		{comp:"GlaxoSmithKline"},
		{comp:"Surya Pharma"},
		{comp:"Torrent Pharma"},
		{comp:"Divis Labs"},
		{comp:"Biocon"},
		{comp:"Orchid Chemical"},
		{comp:"Abbott India"},
		{comp:"Sterling Bio"}
	];
	
	
	// Initialization for Highcharts
	function initHighChart(){
		angular.element('#container').highcharts({
	        chart: {
	            type: 'pie'
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	         exporting: {
	         enabled: false
	        },
	        credits: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: false,
	                    format: '{point.name}: {point.y:.1f}%'
	                }
	            }
	        },
	
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'Aventis Pharma',
	                y: 56.33,
	                drilldown: 'Aventis Pharma '
	            }, {
	                name: 'GlaxoSmithKline',
	                y: 24.03,
	                drilldown: 'GlaxoSmithKline'
	            }, {
	                name: 'Surya Pharma',
	                y: 10.38,
	                drilldown: 'Surya Pharma'
	            }, {
	                name: 'Torrent Pharma',
	                y: 4.77,
	                drilldown: 'Torrent Pharma'
	            }, {
	                name: 'Glenmark',
	                y: 0.91,
	                drilldown: 'Glenmark'
	            }, {
	                name: 'Divis Labs',
	                y: 0.2,
	                drilldown: 'Divis Labs'
	            }]
	        }],
	        drilldown: {
	            series: [{
	                name: 'Microsoft Internet Explorer',
	                id: 'Microsoft Internet Explorer',
	                data: [
	                    ['v11.0', 24.13],
	                    ['v8.0', 17.2],
	                    ['v9.0', 8.11],
	                    ['v10.0', 5.33],
	                    ['v6.0', 1.06],
	                    ['v7.0', 0.5]
	                ]
	            }, {
	                name: 'Chrome',
	                id: 'Chrome',
	                data: [
	                    ['v40.0', 5],
	                    ['v41.0', 4.32],
	                    ['v42.0', 3.68],
	                    ['v39.0', 2.96],
	                    ['v36.0', 2.53],
	                    ['v43.0', 1.45],
	                    ['v31.0', 1.24],
	                    ['v35.0', 0.85],
	                    ['v38.0', 0.6],
	                    ['v32.0', 0.55],
	                    ['v37.0', 0.38],
	                    ['v33.0', 0.19],
	                    ['v34.0', 0.14],
	                    ['v30.0', 0.14]
	                ]
	            }, {
	                name: 'Firefox',
	                id: 'Firefox',
	                data: [
	                    ['v35', 2.76],
	                    ['v36', 2.32],
	                    ['v37', 2.31],
	                    ['v34', 1.27],
	                    ['v38', 1.02],
	                    ['v31', 0.33],
	                    ['v33', 0.22],
	                    ['v32', 0.15]
	                ]
	            }, {
	                name: 'Safari',
	                id: 'Safari',
	                data: [
	                    ['v8.0', 2.56],
	                    ['v7.1', 0.77],
	                    ['v5.1', 0.42],
	                    ['v5.0', 0.3],
	                    ['v6.1', 0.29],
	                    ['v7.0', 0.26],
	                    ['v6.2', 0.17]
	                ]
	            }, {
	                name: 'Opera',
	                id: 'Opera',
	                data: [
	                    ['v12.x', 0.34],
	                    ['v28', 0.24],
	                    ['v27', 0.17],
	                    ['v29', 0.16]
	                ]
	            }]
	        }
	    });
	}
	
	
	
	// Initialization for Pie-Chart
	function initPieChart(){
		$('.piechartcontainer').highcharts({
            chart: {
                backgroundColor: 'transparent',
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
             exporting: {
                enabled: false
               },
               credits: {
                   enabled: false
               },
            plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Sales Acheived',
                    y: 56.33
                }, {
                    name: 'Sales Target',
                    y: 24.03,
                    selected: true
                }]
            }]
        });
    }    
	
	
	function initPieCharts_Grid(){
		
        $('.piechartcontainer1').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });

        $('.piechartcontainer2').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
    	});

        $('.piechartcontainer3').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });

        $('.piechartcontainer4').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                enabled: false
               },
               credits: {
                   enabled: false
               },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });
	}
		
		
}]);


angular.module("imsApp")
.controller("CompetitiveTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Competitive Team controller...', $state);
	
	angular.element(document).ready(function(){
		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);
			      
        angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
        });
        
        initHighChart();
        
        initPieChart();
        initPieCharts_Grid();
	});
	
	/* Angular Provision for Tab Panel */
	$scope.showTab = function(id1, id2){
		angular.element(id1).fadeOut(500, function(){
			angular.element(id2).fadeIn();
		});
	}
	
	$scope.competCombo1 = [
                            { feildHead: "Jayshankar Krish" },
                            { feildHead: "Sachin Mule" },
							{ feildHead: "Pradeep Desai" },
							{ feildHead: "Ashish Patel" }
                        ];
                        
	$scope.competCombo2 = [
                            { zonal: "ALl Managers Sales Performance" },
                            { zonal: "Top 20" },
							{ zonal: "Top 20" },
							{ zonal: "Top 20" }
                        ];
                        
	$scope.competCombo3 = [
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" },
							{ filter: "Medical Rep" }
	];
	
	$scope.competCombo4 = [
							{ period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" },
							{ period: "Mat" }
	];
	
   	$scope.dataSource = [
							{ city: "New Delhi" },
                            { city: "Maharashtra" },
							{ city: "Chennai" },
							{ city: "Kolkata" }
                        ];
                        
	$scope.dataSource2 = [
                            { comparision: "Company to Company" },
                            { comparision: "Company to Company" },
							{ comparision: "Company to Company" },
							{ comparision: "Company to Company" }
                        ];
                        
	$scope.dataSource4 = [
                            { period: "New Delhi" },
                            { period: "Maharashtra" },
							{ period: "Chennai" },
							{ period: "Kolkata" }
                        ];
                        
	$scope.teamPerformance = [
							{name:"Pradeep Desai", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer1", path:"content/img/avatars.png"},
							{name:"Ashish Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer2", path:"content/img/avatars2.png"},
							{name:"Jayshankar Krish", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer3", path:"content/img/avatars3.png"},
							{name:"Shivraj Patel", designation:"Field Head India,North Zone", target:"Rs 45,20,000", acheived:"Rs 56,20,000", chart:"", piechart:"piechartcontainer4" , path:"content/img/avatars.png"}
				  ];
				  
	$scope.comparision = [
					{ rank: "2", company:"Torrent Pharma", sales:"Rs. 8,25,26,000", market:"18%" },
					{ rank: "4", company:"Biocon", sales:"Rs. 8,25,26,000", market:"4%" },
					{ rank: "5", company:"Avantis Pharma", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "8", company:"Surya Pharma", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "12", company:"GlaxoSmithKline", sales:"Rs. 8,25,26,000", market:"18%" },
					{ rank: "13", company:"Glenmark", sales:"Rs. 8,25,26,000", market:"32%" },
					{ rank: "16", company:"Divis Labs", sales:"Rs. 8,25,26,000", market:"9%" },
					{ rank: "24", company:"Orchid Chemical", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "36", company:"Abbott India", sales:"Rs. 8,25,26,000", market:"24%" },
					{ rank: "75", company:"Sterling Bio", sales:"Rs. 8,25,26,000", market:"24%" }
				  ];

	$scope.gridColumns = [
				 { field: "name", title:"Name",headerTemplate: "Name<span class='k-icon k-i-sort-desc'></span>",template: "<div class='customer-photo-competitive'" + "style='background-image: url(#:path#);'></div>" + "<div class='customer-name-competitive'>#:name#</div>", width:'300px'},
			     { field: "designation", title:"Team Designation",headerTemplate: "Team Designation<span class='k-icon k-i-sort-desc'></span>", width:"170px" },
			     { field: "target", title:"Sales Target", headerTemplate: "Sales Target<span class='k-icon k-i-sort-desc'></span>" },
				 { field: "acheived", title:"Sales Acheived",headerTemplate: "Sales Acheived <span class='k-icon k-i-sort-desc'></span>" , template:"<div class='qwer'>#:acheived#</div>" + "<i class='fa fa-caret-up' aria-hidden='true'></i>", width:'' },
				 { field: "chart", title:"Chart",headerTemplate: "Chart <span class='k-icon k-i-sort-desc'></span>",  template:"<div class='#:piechart#'" + "style='max-width: 100px; height:100px;margin: 0 auto;'></div>"}
				];
					
	 $scope.gridColumns2 = [
		{ title: 'Rank', field: 'rank',headerTemplate: "Rank<span class='k-icon k-i-sort-desc'></span>", width:"80px"},
		{ title: 'Company', field: 'company',headerTemplate: "Company<span class='k-icon k-i-sort-desc'></span>", width:"180px" },
		{ title: 'Sales Volume', field: 'sales',headerTemplate: "Sales Volume<span class='k-icon k-i-sort-desc'></span>", width:"180px" },
		{ title  : 'Market Share', field: 'market',headerTemplate: "Market Share<span class='k-icon k-i-sort-desc'></span>" }
	];

	$scope.competitiveteamCompanies=[
		{comp:"Aventis Pharma"},
		{comp:"GlaxoSmithKline"},
		{comp:"Surya Pharma"},
		{comp:"Torrent Pharma"},
		{comp:"Divis Labs"},
		{comp:"Biocon"},
		{comp:"Orchid Chemical"},
		{comp:"Abbott India"},
		{comp:"Sterling Bio"}
	];
	
	
	// Initialization for Highcharts
	function initHighChart(){
		angular.element('#container').highcharts({
	        chart: {
	            type: 'pie'
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: ''
	        },
	         exporting: {
	         enabled: false
	        },
	        credits: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: false,
	                    format: '{point.name}: {point.y:.1f}%'
	                }
	            }
	        },
	
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'Aventis Pharma',
	                y: 56.33,
	                drilldown: 'Aventis Pharma '
	            }, {
	                name: 'GlaxoSmithKline',
	                y: 24.03,
	                drilldown: 'GlaxoSmithKline'
	            }, {
	                name: 'Surya Pharma',
	                y: 10.38,
	                drilldown: 'Surya Pharma'
	            }, {
	                name: 'Torrent Pharma',
	                y: 4.77,
	                drilldown: 'Torrent Pharma'
	            }, {
	                name: 'Glenmark',
	                y: 0.91,
	                drilldown: 'Glenmark'
	            }, {
	                name: 'Divis Labs',
	                y: 0.2,
	                drilldown: 'Divis Labs'
	            }]
	        }],
	        drilldown: {
	            series: [{
	                name: 'Microsoft Internet Explorer',
	                id: 'Microsoft Internet Explorer',
	                data: [
	                    ['v11.0', 24.13],
	                    ['v8.0', 17.2],
	                    ['v9.0', 8.11],
	                    ['v10.0', 5.33],
	                    ['v6.0', 1.06],
	                    ['v7.0', 0.5]
	                ]
	            }, {
	                name: 'Chrome',
	                id: 'Chrome',
	                data: [
	                    ['v40.0', 5],
	                    ['v41.0', 4.32],
	                    ['v42.0', 3.68],
	                    ['v39.0', 2.96],
	                    ['v36.0', 2.53],
	                    ['v43.0', 1.45],
	                    ['v31.0', 1.24],
	                    ['v35.0', 0.85],
	                    ['v38.0', 0.6],
	                    ['v32.0', 0.55],
	                    ['v37.0', 0.38],
	                    ['v33.0', 0.19],
	                    ['v34.0', 0.14],
	                    ['v30.0', 0.14]
	                ]
	            }, {
	                name: 'Firefox',
	                id: 'Firefox',
	                data: [
	                    ['v35', 2.76],
	                    ['v36', 2.32],
	                    ['v37', 2.31],
	                    ['v34', 1.27],
	                    ['v38', 1.02],
	                    ['v31', 0.33],
	                    ['v33', 0.22],
	                    ['v32', 0.15]
	                ]
	            }, {
	                name: 'Safari',
	                id: 'Safari',
	                data: [
	                    ['v8.0', 2.56],
	                    ['v7.1', 0.77],
	                    ['v5.1', 0.42],
	                    ['v5.0', 0.3],
	                    ['v6.1', 0.29],
	                    ['v7.0', 0.26],
	                    ['v6.2', 0.17]
	                ]
	            }, {
	                name: 'Opera',
	                id: 'Opera',
	                data: [
	                    ['v12.x', 0.34],
	                    ['v28', 0.24],
	                    ['v27', 0.17],
	                    ['v29', 0.16]
	                ]
	            }]
	        }
	    });
	}
	
	
	
	// Initialization for Pie-Chart
	function initPieChart(){
		$('.piechartcontainer').highcharts({
            chart: {
                backgroundColor: 'transparent',
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
             exporting: {
                enabled: false
               },
               credits: {
                   enabled: false
               },
            plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Sales Acheived',
                    y: 56.33
                }, {
                    name: 'Sales Target',
                    y: 24.03,
                    selected: true
                }]
            }]
        });
    }    
	
	
	function initPieCharts_Grid(){
		
        $('.piechartcontainer1').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });

        $('.piechartcontainer2').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
    	});

        $('.piechartcontainer3').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                    enabled: false
                   },
                   credits: {
                       enabled: false
                   },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });

        $('.piechartcontainer4').highcharts({
             chart: {
                 backgroundColor: 'transparent',
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                 exporting: {
                enabled: false
               },
               credits: {
                   enabled: false
               },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: false,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Aventis Pharma',
                        y: 56.33,
                        drilldown: 'Aventis Pharma '
                    }, {
                        name: 'GlaxoSmithKline',
                        y: 24.03,
                        drilldown: 'GlaxoSmithKline'
                    }]
                }],
                drilldown: {
                    series: [{}]
                }
        });
	}
		
		
}]);

angular.module("imsApp")
.controller("UserProfileController", ['$scope', '$rootScope', '$state', 'adminServices', 'companyServices', 'mediaServices', '$window', 'notify', '$uibModal',
							function($scope, $rootScope, $state, adminServices, companyServices, mediaServices, $window, notify, $uibModal){
	
	console.log('User Profile controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	/* Parse session data to object format */
	function sessionInfoToScope(){
		$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	}
	sessionInfoToScope();
	
	
	/* Update Session info */
	function updateSessionInfo(profileImage){
		var sessionInfo = JSON.parse($window.sessionStorage.imsSessionInfo);
		sessionInfo.ProfileImage = profileImage;
		$window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfo);
		sessionInfoToScope();
		// Update Navbar's sessionInfo
		$rootScope.$emit('UpdateNavbarSessionInfo', {});
		$state.go($scope.sessionInfo.fromState);
	}
	
	
	// Model Initialization
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.user = {};
	$scope.user.Active = 1;
	FetchUserProfileInfo();
	
	$scope.imgBrowsed = false;
	$scope.avatarSelected = false;
	$scope.imgConfirmed = false;
	$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
	$scope.imgDataUrl = $scope.baseURL + 'avatars/avatar-2-256.png';
	$scope.user.ProfileImage = 'avatars/avatar-2-256.png';
	
	
	/* State Params functionality */
	function FetchUserProfileInfo(){
    	adminServices.GetUserInfo($scope.sessionInfo.UserID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.user = response.data;
				applyUserInfoResponse();
			} else {
				$scope.user = false;
			}
		},
		function (rejection) {});
   	}
   	
   	
   	/* Fetch User Info by ID */
	function FetchUserInfoByID(userId){
    	adminServices.GetUserInfo(userId, showLoader, hideLoader)
		.then(function (response) {
			var profileImage = '';
			if (response.data != null && response.status === 200) {
				profileImage = (response.data.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + response.data.ProfileImage);
				updateSessionInfo(profileImage);
			} else {
				
			}
		},
		function (rejection) {});
   	}
   	
   	
   	/* Apply fetched user info to page */
   	function applyUserInfoResponse() {
		FetchCompanyDivisions();
		$scope.imgDataUrl = ($scope.user.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + $scope.user.ProfileImage);
	}
   	
	
	/* Divisions */
	function FetchCompanyDivisions() {
		companyServices.GetCompanyDivisions($scope.user.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.FetchUserRolesByCompany();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function () {
		adminServices.GetUserRolesByCompany($scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.RolesColl = response.data;
				$scope.FetchReportingPerson();
			} else {
				$scope.RolesColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Fetch Company Reporting Manager based on Role Type */
	$scope.FetchReportingPerson = function(){
		companyServices.GetCompanyReportingManagerBasedOnRoleType($scope.user.CompanyRoleID, $scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.ReportingPersons = response.data;
			} else {
				$scope.ReportingPersons = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Image functionality */
    $scope.imageView = function(){
    	$scope.imgBrowsed = true;
		var reader = new FileReader();
		reader.onload = $scope.imageIsLoaded; 
		reader.readAsDataURL($scope.ProfileImage);
	};
 
 
	$scope.imageIsLoaded = function(e){
	    $scope.$apply(function() {
	    	$scope.imgUrl = e.target.result;
	    });
	};
	
	
	function fetchAvatars() {
		$scope.avatarsList = ['avtar1.png', 'avtar2.png', 'avtar3.png', 'avtar4.png', 'avtar5.png', 'avtar6.png', 'avtar7.png', 'avtar8.png',
								'avtar9.png', 'avtar10.png', 'avtar11.png', 'avtar12.png', 'avtar13.png', 'avtar14.png', 'avtar15.png', 'avtar16.png'];
	}
	fetchAvatars();
	
	
	$scope.browsedConfirmed = function() {
		$scope.imgConfirmed = true;
		$scope.imgDataUrl = $scope.imgUrl;
		$scope.user.ProfileImage = $scope.ProfileImage;
	};
	
	
	$scope.avatarIsSelected = function(avatar) {
		$scope.avatarSelected = true;
		$scope.imgUrl = $scope.baseURL + 'avatars/' + avatar;
		$scope.ProfileImage = 'avatars/' + avatar;
	};
	
	/* Image Change modal begins */
	$scope.openImgChangeModal = function(UserID){
        var parent = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'imgChangeModal.html',
            windowTopClass: 'imageChangeModal',
            controller:['$scope','$uibModalInstance', function($scope,$uibModalInstance){
            	$scope.parent = parent;
				parent.imgUrl = parent.imgDataUrl;
				parent.ProfileImage = parent.user.ProfileImage;
				parent.imgBrowsed = false;
                $scope.closeModal = function(){
					$uibModalInstance.close('close');
					parent.browsedConfirmed();
                };
                
                $scope.dismissModal = function(){
                	parent.imgBrowsed = false;
                	parent.avatarSelected = false;
                    $uibModalInstance.dismiss('dismiss');
                };
            }]
        });
    };
    /* Image Change modal ends */
	
	
	/* Upload file to Database and return path */
	$scope.uploadFile = function(routine) {
        mediaServices.PostAnImage($scope.user.ProfileImage)
		.then(function (response) {
		    if (response.data != null && response.status === 201) {
		        if(response.data.Message === 'Image Updated Successfully.'){
		        	$scope.user.ProfileImage = $scope.user.ProfileImage.name;
		        	if(routine !== undefined){
		        		eval(routine);
		        	}
		        }
		    } else {
		      	console.log('Failed to upload image: ',response);
		    }
		},
		function (rejection) { });
    };
    
	
	// Save User
	$scope.saveUser = function () {
	    adminServices.AddUpdateUser($scope.user)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        copyObject($scope.sessionInfo, response.data);
		        $window.sessionStorage.imsSessionInfo = JSON.stringify($scope.sessionInfo);
		        showNotification('Profile info updated successfully', '', 'FetchUserInfoByID('+response.data+')');
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	$scope.saveFormData = function() {
		if($scope.imgConfirmed && !($scope.avatarSelected)){
			$scope.uploadFile('$scope.saveUser();');
		}
		else{
			$scope.saveUser();
		}
	};
	
	
	// Copy the kay values of source object into destination object
    function copyObject(source, dest){
    	for(var prop in source){
			for(var key in dest){
				if(prop === key){
					dest[key] = source[prop];
				}
			}
		}
    }
    
    
    // Show Notification
	function showNotification(message, noteClass, routine){
		noteClass = noteClass || '';
		notify({
            message: message,
            classes: noteClass,
            templateUrl: $scope.template,
            position: 'center',
            duration: 2000
        });
        eval(routine);
	}

		
}]);

angular.module("imsApp")
.controller("LayoutAdminController", ['$scope', '$state', function($scope, $state){
	console.log('Layout Admin controller...', $state);
	if($state.current.name === 'layout'){
		// $state.go('.analysis');
	}
		
}]);

'use strict';

(function(){
	function NavbarAdminController ($timeout, $scope, $rootScope, $window, $state, $uibModal, adminServices, notify, accountServices) {
		
		/* Parse session data to object format */
		function sessionInfoToScope(){
			$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
			console.log('$scope.sessionInfo: ',$scope.sessionInfo);
		}
		sessionInfoToScope();
		
		
		/* Handle an issued event */
		$rootScope.$on('UpdateNavbarSessionInfo', function(){
			sessionInfoToScope();
		});
		
		
		/* copied from app.js in HTML version */
		// Left mobile menu
		angular.element('.hamburger').click(function(){
			if (angular.element('body').hasClass('menu-left-opened')) {
				angular.element(this).removeClass('is-active');
				angular.element('body').removeClass('menu-left-opened');
				angular.element('html').css('overflow','auto');
			} else {
				angular.element(this).addClass('is-active');
				angular.element('body').addClass('menu-left-opened');
				angular.element('html').css('overflow','hidden');
			}
		});
	
		angular.element('.mobile-menu-left-overlay').click(function(){
			angular.element('.hamburger').removeClass('is-active');
			angular.element('body').removeClass('menu-left-opened');
			angular.element('html').css('overflow','auto');
		});
		
		// Right mobile menu
		angular.element('.site-header .burger-right').click(function(){
			if (angular.element('body').hasClass('menu-right-opened')) {
				angular.element('body').removeClass('menu-right-opened');
				angular.element('html').css('overflow','auto');
			} else {
				angular.element('.hamburger').removeClass('is-active');
				angular.element('body').removeClass('menu-left-opened');
				angular.element('body').addClass('menu-right-opened');
				angular.element('html').css('overflow','hidden');
			}
		});
		
		angular.element('.mobile-menu-right-overlay').click(function(){
			angular.element('body').removeClass('menu-right-opened');
			angular.element('html').css('overflow','auto');
		});
		/* copied from app.js */
		
		
		/* Logout Action */
		$scope.logoutSession = function(){
            accountServices.Logout(JSON.parse($window.localStorage.BI_Tool_Info).SessionID)
            .then(function(response) {}, function(rejection) {});
		};
		
		
		/* Change Password modal starts */
	    $scope.openConfirmationModal = function(){
	        var self = $scope;
	        var changePassword = $uibModal.open({
	            templateUrl:'changePasswordModal.html',
	            size: 'md',
	            windowTopClass: 'modal-top-class',
	            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
	            	$scope.passwords = {};
	            	// $scope.passwords.OldPassword = '';
	            	$scope.passwords.UserID = self.sessionInfo.UserID;
	            	$scope.showError = false;
	                $scope.closeModal = function(){
	                    $uibModalInstance.close('close');
	                };
	                $scope.dismissModal = function(){
	                    $uibModalInstance.dismiss('dismiss');
	                };
	                $scope.setNewPassword = function(e){
	                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
	                		$scope.showError = true;
	                	}
	                	else{
	                		$scope.showError = false;
	                		self.setPassword($scope.passwords, $uibModalInstance);
	                	}
	                };
	            }]
	        });
	    };
	    /* Change Password modal ends */
	   
	   	// Set Password
		$scope.setPassword = function(passwords, $uibModalInstance){
			adminServices.ChangePassword(passwords)
			.then(function (response) {
			    if (response.data != null && response.status === 200) {
			    	if(response.data === "updated."){
			    		$uibModalInstance.close('close');
			    		showNotification('Password Updated Successfully', '', '$scope.logoutSession()');
			    	}
				    else{
				    	showNotification(response.data, 'notify-bg', '');
				    }
				        
			    } else {
			       
			    }
			},
			function (rejection) { });
		};
		
		
		// Show Notification
		function showNotification(message, noteClass, routine){
			noteClass = noteClass || '';
			notify({
	            message: message,
	            classes: noteClass,
	            templateUrl: $scope.template,
	            position: 'center',
	            duration: 2000
	        });
	        eval(routine);
		}
		
		
		// Navigate back to Super admin page
		$scope.NavigateToSuperadmin = function() {
			$scope.sessionInfo.CompanyID = 0;
			$state.go('super.customerlist', {'sessionInfo':$scope.sessionInfo});
		};
		
	}
	
	angular.module('imsApp')
	  .controller('NavbarAdminController', NavbarAdminController);
})();

'use strict';

angular.module('imsApp')
	.directive('navbaradmin', [function(){
		var directiveObj = {};
		directiveObj.templateUrl = 'app/components/navbars/navbar_admin.html';
		directiveObj.restrict = 'A';
		directiveObj.controller = 'NavbarAdminController';
		directiveObj.controllerAs = 'nav';
		
		return directiveObj;
	}]);

angular.module("imsApp")
.controller("UserListController", ['$scope', '$state', '$stateParams', 'adminServices', '$window', '$uibModal', 'notify',
									function($scope, $state, $stateParams, adminServices, $window, $uibModal, notify){
	
	console.log('UserList controller...', $state);
	
	// Overwrite Session Info with state parameter data
	if($stateParams.sessionInfo !== ''){
		$window.sessionStorage.setItem('imsSessionInfo',JSON.stringify($stateParams.sessionInfo));
	}
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	// Model Initialization
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
	
	$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
	
	// Fetch company users count
	$scope.FetchCompanyUsersCount = function () {
	    adminServices.GetCompanyUserCount($scope.CompanyID)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.UserCountColl = response.data;
		        $scope.totalUserCount = computeTotalUserCount(response.data);
		    } else {
		        $scope.UserCountColl = false;
		    }
		},
		function (rejection) { });
	}
	$scope.FetchCompanyUsersCount();
	
	
	// Compute Total Users Count
	function computeTotalUserCount(data){
		var total = 0;
		for(var i = 0; i < data.length; i++){
			total += data[i].UserCount;
		}
		return total;
	}
	
	
	// Fetch company users
	$scope.FetchCompanyUsers = function () {
	    adminServices.GetCompanyUsers($scope.CompanyID, showLoader, hideLoader)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.users.data(response.data);
		        $scope.TotalData = response.data;
		    } else {
		        $scope.TotalData = false;
		    }
		},
		function (rejection) { });
	};
	$scope.FetchCompanyUsers();
	
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.UserName);
								});
			$scope.users.data(data);
		}
	};
	    
	// Grid columns configuration
	$scope.userListCols = [
		{ field: "UserName", title:"Name",headerTemplate: "Name<span class='k-icon k-i-sort-desc'></span>" ,template: "<div class='customer-photo-user-list'" 
			+ "style='background-image: url("+ $scope.baseURL +"#:ProfileImage#);'></div>" + "<div class='customer-name-user-list'>#:UserName#</div>", width:'250px' },
		{ field: "DivisionName", title: "Division", headerTemplate: "Division<span class='k-icon k-i-sort-desc'></span>", width:'100px' },
		{ field: "RoleName", title: "Designation", headerTemplate: "Designation<span class='k-icon k-i-sort-desc'></span>", width:'120px' },
		{ field: "ReportingManagerName", title: "Reporting Person", headerTemplate: "Reporting Person<span class='k-icon k-i-sort-desc'></span>", width:'150px' },
		{ field: "Email", title:"Email Address", headerTemplate: "Email Address<span class='k-icon k-i-sort-desc'></span>",width:"250px"},
		{ field: "MobileNo", title:"Phone No.", headerTemplate: "Phone No.<span class='k-icon k-i-sort-desc'></span>", width:'120px' },
		{ field: "Active", title: "Active", headerTemplate: "Active<span class='k-icon k-i-sort-desc'></span>", template: "<div><i class='glyphicon glyphicon-ok' aria-hidden='true' ng-if='(#=Active# === 1)'></i>"
			+ "<i class='glyphicon glyphicon-remove' aria-hidden='true' ng-if='(#=Active# === 0)'></i></div>", width: "90px" },
		{ field: "", title:"Edit", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ui-sref='admin.addnewuser({userId:#=UserID#})'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></div>", width:"70px" },
		{ field: "", title:"PWD", template:"<div class='icon-team-permission'>"+
			"<a href='javascript:void(0)' ng-click='openConfirmationModal(#=UserID#);'><i class='fa fa-key' aria-hidden='true'></i></a></div>", width:"70px" },
		{ field: "", title:"Delete", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='deleteConfirmationModal(#=UserID#, \"#=UserName#\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>", width:"70px" }
	];
	
	// Kendo Observable Array
	$scope.users = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "UserName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	
	$scope.teamBrkup=[
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
		{team:"Medical Reps (128)"},
	];
	  
	$scope.statusUserlist=[
		{status:"Active"},
		{status:"Inactive"}
	];
	
	
	/* Change Password modal starts */
    $scope.openConfirmationModal = function(UserID){
        var self = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'setPasswordModal.html',
            size: 'md',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.passwords = {};
            	$scope.passwords.isAdmin = 1;
            	$scope.passwords.UserID = UserID;
            	$scope.showError = false;
                $scope.closeModal = function(){
                    $uibModalInstance.close('close');
                };
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.setNewPassword = function(e){
                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
                		$scope.showError = true;
                	}
                	else{
                		$scope.showError = false;
                		self.setPassword($scope.passwords, $uibModalInstance);
                	}
                };
            }]
        });
    };
    /* Change Password modal ends */
   
   	// Set Password
	$scope.setPassword = function(passwords, $uibModalInstance){
		adminServices.ChangePassword(passwords)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data === "updated."){
		    		$uibModalInstance.close('close');
		    		showNotification({message:'Password Updated Successfully', duration: 2000, class:null, routine:null});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		adminServices.DeleteUser(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchCompanyUsers();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
		
}]);

angular.module("imsApp")
.controller("AddNewUserController", ['$scope', '$state', 'adminServices', 'companyServices', 'mediaServices', '$window', '$stateParams', '$uibModal', 'notify', '$rootScope',
							function($scope, $state, adminServices, companyServices, mediaServices, $window, $stateParams, $uibModal, notify, $rootScope){

	console.log('Add User controller...', $state);

	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));

	angular.element(document).ready(function(){

	});


	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
	  	hideLoader = "angular.element('.loader-backdrop').fadeOut();";
		$scope.rootScope = $rootScope;

		$scope.user = {};
		$scope.user.CompanyID = $scope.sessionInfo.CompanyID;
		$scope.user.Active = 1;

		$scope.imgBrowsed = false;
		$scope.avatarSelected = false;
		$scope.imgConfirmed = false;
		$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
		$scope.imgDataUrl = $scope.baseURL + 'avatars/avatar-2-256.png';
		$scope.user.ProfileImage = 'avatars/avatar-2-256.png';

		$scope.accessRightData = {};
		$scope.accessRightData.IsAllAccess = true;
		$scope.user.UserAccessRights = {};
		initTwoSidedMultiselectWidget();
	}

	// State Parmas
	if($stateParams.userId !== ''){
		$scope.user.isUpdate = 1;
		FetchUserInfoById($stateParams.userId);
	}
	else{
		$scope.user.isUpdate = 0;
		FetchCompanyDivisions();
		FetchCompanySubscribedCityTCClass();
	}


	/* User Info by userId */
	function FetchUserInfoById(userId) {
		adminServices.GetUserInfo(userId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.user = response.data;
				applyUserInfoResponse();
			} else {
				$scope.user = false;
			}
		},
		function (rejection) {});
	}


	function applyUserInfoResponse() {
		FetchCompanyDivisions({'initEditFlow':true});
		FetchCompanySubscribedCityTCClass();
		$scope.imgDataUrl = ($scope.user.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + $scope.user.ProfileImage);
	}


	/* Divisions */
	function FetchCompanyDivisions(inputObj) {		// {'initEditFlow':boolean}
		companyServices.GetCompanyDivisions($scope.user.CompanyID)
		.then(function (response) {
			if ((response.data.length > 0) && (response.status === 200)) {
				$scope.divisionColl = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.user.DivisionID !== null)){
					$scope.user.DivisionID = $scope.user.DivisionID;
				}
				else{
					$scope.user.DivisionID = $scope.divisionColl[0].DivisionID;
				}
				$scope.FetchUserRolesByCompany(inputObj);
			} else {
				showNotification({message:"Please create Divisions first", duration: 3000, class:"notify-bg", routine:"$state.go('admin.divisions');"});
			}
		},
		function (rejection) {});
	};


	/* User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function (inputObj) {		// {'initEditFlow':boolean}
		adminServices.GetUserRolesByCompany($scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if ((response.data.length > 0) && (response.status === 200)) {
				$scope.RolesColl = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.user.CompanyRoleID !== null)){
					$scope.user.CompanyRoleID = $scope.user.CompanyRoleID;
				}
				else{
					$scope.user.CompanyRoleID = $scope.RolesColl[0].CompanyUserRoleID;
				}
				$scope.FetchReportingPerson(inputObj);
			} else{
				showNotification({message:"Please create Roles first", duration: 3000, class:"notify-bg", routine:"$state.go('admin.userroles');"});
			}
		},
		function (rejection) {});
	};


	/* Fetch Company Reporting Manager based on Role Type */
	$scope.FetchReportingPerson = function(inputObj) {		// {'initEditFlow':boolean}
		companyServices.GetCompanyReportingManagerBasedOnRoleType($scope.user.CompanyRoleID, $scope.user.CompanyID, $scope.user.DivisionID)
		.then(function (response) {
			if ((response.data != null) && (response.status === 200)) {
				$scope.ReportingPersons = response.data;
				if((inputObj !== undefined) && (inputObj.initEditFlow !== undefined) && inputObj.initEditFlow && ($scope.user.ReportingManagerID !== null)){
					$scope.user.ReportingManagerID = $scope.user.ReportingManagerID;
				}
				else{
					$scope.user.ReportingManagerID = ($scope.ReportingPersons.length > 0) ? $scope.ReportingPersons[0].PersonID : null;
				}
			} else {
				$scope.ReportingPersons = [];
			}
		},
		function (rejection) {});
	};


	/* Image functionality begins */
    $scope.imageView = function(){
    	$scope.imgBrowsed = true;
		var reader = new FileReader();
		reader.onload = $scope.imageIsLoaded;
		reader.readAsDataURL($scope.ProfileImage);
	};


	$scope.imageIsLoaded = function(e){
	    $scope.$apply(function() {
	    	$scope.imgUrl = e.target.result;
	    });
	};


	function fetchAvatars() {
		$scope.avatarsList = ['avtar1.png', 'avtar2.png', 'avtar3.png', 'avtar4.png', 'avtar5.png', 'avtar6.png', 'avtar7.png', 'avtar8.png',
								'avtar9.png', 'avtar10.png', 'avtar11.png', 'avtar12.png', 'avtar13.png', 'avtar14.png', 'avtar15.png', 'avtar16.png'];
	}
	fetchAvatars();


	$scope.browsedConfirmed = function() {
		$scope.imgConfirmed = true;
		$scope.imgDataUrl = $scope.imgUrl;
		$scope.user.ProfileImage = $scope.ProfileImage;
	};


	$scope.avatarIsSelected = function(avatar) {
		$scope.avatarSelected = true;
		$scope.imgUrl = $scope.baseURL + 'avatars/' + avatar;
		$scope.ProfileImage = 'avatars/' + avatar;
	};

	/* Image Change modal begins */
	$scope.openImgChangeModal = function(UserID){
        var parent = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'imgChangeModal.html',
            windowTopClass: 'imageChangeModal',
            controller:['$scope','$uibModalInstance', function($scope,$uibModalInstance){
            	$scope.parent = parent;
				parent.imgUrl = parent.imgDataUrl;
				parent.ProfileImage = parent.user.ProfileImage;
				parent.imgBrowsed = false;
                $scope.closeModal = function(){
					$uibModalInstance.close('close');
					parent.browsedConfirmed();
                };

                $scope.dismissModal = function(){
                	parent.imgBrowsed = false;
                	parent.avatarSelected = false;
                    $uibModalInstance.dismiss('dismiss');
                };
            }]
        });
    };
    /* Image Change modal ends */


	/* Upload file to Database and return path */
	$scope.uploadFile = function(routine) {
        mediaServices.PostAnImage($scope.user.ProfileImage)
		.then(function (response) {
		    if (response.data != null && response.status === 201) {
		        if(response.data.Message === 'Image Updated Successfully.'){
		        	$scope.user.ProfileImage = $scope.user.ProfileImage.name;
		        	if(routine !== undefined){
		        		eval(routine);
		        	}
		        }
		    } else {
		      	console.log('Failed to upload image: ',response);
		    }
		},
		function (rejection) { });
    };


	/* Fetch List of Cities and TC Classes subscribed by Company */
	function FetchCompanySubscribedCityTCClass(){
		companyServices.GetCompanySubscribedCityTCClass($scope.user.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.accessRightData = response.data;
				if($scope.accessRightData.TherapyList !== null){
					filterTherapyListData($scope.accessRightData.TherapyList);
				}
				if($scope.user.isUpdate === 1){
					$scope.user.UserAccessRights.CityIDs
					= ($scope.user.UserAccessRights.CityIDs !== null) ? $scope.user.UserAccessRights.CityIDs.split(',').map(Number) : [];
				}
			} else {}
		},
		function (rejection) {});
	}

	/* Filter therapy list data into two arrays to feed into Two sided multiselect */
	function filterTherapyListData(data){
		if($scope.user.isUpdate === 0){
			$scope.leftArray = data;
		}
		else{
			if(($scope.user.UserAccessRights !== null) && ($scope.user.UserAccessRights.TCValues !== null)){
				var TcValues = $scope.user.UserAccessRights.TCValues.split(',');
				if(TcValues.length > 0){
					for(var i = 0; i < data.length; i++){
						if(TcValues.indexOf(data[i].TherapyCode) === -1){
							$scope.leftArray.push(data[i]);
						}
						else{
							$scope.rightArray.push(data[i]);
						}
					}
				}
				else{
					$scope.leftArray = data;
				}
			}
			else{
				$scope.leftArray = data;
			}
		}
	}

	/* Two sided multisided widget begins */
	function initTwoSidedMultiselectWidget(){
		$scope.leftBag = [];
		$scope.rightBag = [];
		$scope.leftArray = [];
		$scope.rightArray = [];
	}

	$scope.reserveElement = function(key, id){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var local_i = idsPool.indexOf(id);
		if(local_i > -1){
			idsPool.splice(local_i, 1);
		}
		else{
			idsPool.push(id);
		}
	};

	$scope.moveElements = function(key, source, destination){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var temp_i, temp_item = null;
		for(var i = 0; i < idsPool.length; i++){
			temp_i = source.findIndex(x => (x.TherapyCode === idsPool[i]));
			temp_item = source.splice(temp_i, 1)[0];
			destination.push(temp_item);
		}
		if(key === 'left'){ $scope.leftBag = []; }
		else{ $scope.rightBag = []; }
	};
	/* Two sided multisided widget ends */


	/* Prepare User info to be saved */
	function prepareUserToBeSaved(){
	    $scope.user.RoleID = 3;
	    if ($scope.user.UserAccessRights === null) {
	        $scope.user.UserAccessRights = {};
	    }
		$scope.user.UserAccessRights.AccessRightType = ($scope.accessRightData.IsAllAccess) ? 1 : 2;
		$scope.user.UserAccessRights.CityIDs = ($scope.user.UserAccessRights.CityIDs.length > 0) ? $scope.user.UserAccessRights.CityIDs.toString() : null;
		if($scope.rightArray.length > 0){
			$scope.user.UserAccessRights.TCValues = $scope.rightArray.map(function(item){return item.TherapyCode;}).toString();
		}
		else{
			$scope.user.UserAccessRights.TCValues = null;
		}
	}

	// Save User
	$scope.saveUser = function () {
		prepareUserToBeSaved();
	    adminServices.AddUpdateUser($scope.user)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('admin.userlist');
		    } else {}
		},
		function (rejection) { });
	};


	/* Upload Image file before saving user info */
	$scope.interceptToUploadImage = function() {
		if($scope.imgConfirmed && !($scope.avatarSelected)){
			$scope.uploadFile('$scope.saveUser();');
		}
		else{
			$scope.saveUser();
		}
	};


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}


}]);

angular.module("imsApp")
.controller("TeamsPermissionsController", ['$scope', '$state', function($scope, $state){
	
	console.log('Teams Permissions controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	$scope.tableData = [
								{designation:"Head Field India", teamhead:"Pradeep Desai", action:""},
								{designation:"Medical Reps", teamhead:"Manish Pradhan", action:"" },
								{designation:"Regional Manager", teamhead:"Sachin Mule", action:""},
								{designation:"Zonal Manager", teamhead:"Jayshankar Krish", action:""}
  							];
	$scope.filteredList = $scope.tableData;
  							
	$scope.gridColumns = [
		{ field: "designation", title:"Designation",headerTemplate: "Designation <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "teamhead", title:"Team Head", headerTemplate: "Team Head <span class='k-icon k-i-sort-desc'></span>",width:"800px"  },
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'><i class='fa fa-lock' aria-hidden='true'></i> <i class='fa fa-times' aria-hidden='true'></i> <a ui-sref='admin.addteam'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"150px" }
	];
	
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if(($scope.tableData !== undefined) || ($scope.tableData !== null)){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			$scope.filteredList = $scope.tableData.filter(function(item){
									return regExpression.test(item.designation);
								});
		}
	}
	
		
}]);

angular.module("imsApp")
.controller("AddTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Add Teams controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	$scope.people = [
				{ permission: "Permission 1", view:"", edit:"1", delet:"d1" },
				{ permission: "Permission 2", view:"", edit:"2", delet:"d2" },
				{ permission: "Permission 3", view:"", edit:"3", delet:"d3" },
				{ permission: "Permission 4", view:"", edit:"4", delet:"d4" },
				{ permission: "Permission 5", view:"", edit:"5", delet:"d5" },
				{ permission: "Permission 6", view:"", edit:"6", delet:"d6" },
				{ permission: "Permission 7", view:"", edit:"7", delet:"d7" },
				{ permission: "Permission 8", view:"", edit:"8", delet:"d8" }
			  ];
			  
			  
	$scope.gridColumns = [
					{ title: 'Permission Type', field: 'permission', template:"" ,width:"450px"},
					{ title: 'View', field: 'view', template: "<div class='checkbox'><input type='checkbox' id='#:permission#'><label for='#:permission#'></label></div>", width:"250px"},
					{ title: 'Edit', field: 'edit', template: "<div class='checkbox'><input type='checkbox'><input type='checkbox' id='#:edit#'><label for='#:edit#'></label></div>" },
					{ title: 'Delete', field: 'delet', template: "<div class='checkbox'><input type='checkbox'><input type='checkbox' id='#:delet#'><label for='#:delet#'></label></div>" }
	];
		
}]);


angular.module("imsApp")
.controller("MapUserToTeamController", ['$scope', '$state', function($scope, $state){
	
	console.log('Map User To Team controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	$scope.gridData = [
						{id: 1, fullname:"Sagar Hoda", designation:"Medical Reps", teamhead:"Unsigned"},
						{id: 2, fullname:"Rahul Vaidya", designation:"Zonal Manager", teamhead:"Sachin Mule"},
						{id: 3, fullname:"Parineeti Chouhan", designation:"Regional Manager", teamhead:"Pradeep Desai"},
						{id: 4, fullname:"Shivraj Patel", designation:"Zonal Manager", teamhead:"Unsigned"},
						{id: 5, fullname:"Mithila Desai", designation:"Medical Reps", teamhead:"Unsigned"},
						{id: 6, fullname:"Rahul Jadeja", designation:"Medical Reps", teamhead:"Nitin Kapoor"},
						{id: 7, fullname:"Dinesh Juneja", designation:"Regional Manager", teamhead:"Nitin Kapoor"},
						{id: 8, fullname:"Lakshmi Bhaskar", designation:"Medical Reps", teamhead:"Unsigned"},
						{id: 9, fullname:"Radha Mohan", designation:"Zonal Manager", teamhead:"Unsigned"},
						{id: 10, fullname:"Sujoy Biswas", designation:"Medical Reps", teamhead:"Unsigned"}
					];
					
	$scope.gridColumns = [
		{ field: "fullname", title:"Full Name", template: "<div class='customer-photo'" + "style='background-image: url(content/img/avatars.png);'></div>"
		+ "<div class='customer-name'>#:fullname#</div>" + "<div class='checkboxshow'>"
		+ "<div class='checkbox'><input type='checkbox'><input type='checkbox' id='#:id#'><label for='#:id#'></label></div>"},
		{ field: "designation", title:"Designation" },
		{ field: "teamhead", title:"Team Head" }
	];
	
	$scope.searchDesignation=[
		{searchdesig:"Search Name or Designation"},
		{searchdesig:"Search Name or Designation"}
	];
  
	$scope.mapuserTeamhead=[
		{teamhead:"Pradeep Desai"},
		{teamhead:"Sachin Mule"},
		{teamhead:"Nitin Kapoor"}
	];
		
}]);

angular.module("imsApp")
.controller("UserRolesController", ['$scope', '$state', 'adminServices', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, adminServices, companyServices, $window, $uibModal, notify){
	
	console.log('User Roles controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
  	
  	
  	/* Initialize Models */
  	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
  	
  	
  	/* Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.FetchUserRolesByCompany();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
  	
  	
  	/* Fetch User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function () {
		adminServices.GetUserRolesByCompany($scope.CompanyID, $scope.DivisionID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};
	
	$scope.gridColumns = [
		{ field: "RoleName", title:"Role Name", headerTemplate: "Role Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "ParentRoleName", title:"Parent Role Name", headerTemplate: "Parent Role Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action", headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=CompanyUserRoleID#, \"#=RoleName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='admin.addrole({roleId:#:CompanyUserRoleID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"40px" }
	];
	
	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "RoleName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
							return regExpression.test(item.RoleName);
						});
			$scope.observableList.data(data);
		}
	}
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		adminServices.DeleteCompanyUserRole(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchUserRolesByCompany();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
		
}]);


angular.module("imsApp")
.controller("AddRoleController", ['$scope', '$state', '$stateParams', 'adminServices', 'companyServices', 'notify', 'userServices', '$window',
							function($scope, $state, $stateParams, adminServices, companyServices, notify, userServices, $window){
	
	console.log('Add Role controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.roleObj = {};
	$scope.roleObj.CompanyID = $scope.sessionInfo.CompanyID;
	
	
	/* State Params */
	if($stateParams.roleId !== ''){
		FetchRoleByRoleId($stateParams.roleId);
		$scope.isUpdate = true;
	}
	else{
		FetchCompanyDivisions();
		$scope.isUpdate = false;
	}
	
	
	/* Fetch Role by role id */
	function FetchRoleByRoleId(roleId) {
		adminServices.GetCompanyUserRoleByRoleID(roleId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.roleObj = response.data;
				FetchCompanyDivisions();
			} else {
				$scope.roleObj = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* Divisions */
	function FetchCompanyDivisions() {
		companyServices.GetCompanyDivisions($scope.roleObj.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				if($scope.roleObj.DivisionID === undefined){
					$scope.roleObj.DivisionID = $scope.divisionColl[0].DivisionID;
				}
				$scope.FetchUserRolesByCompany();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* User Roles by Divisions */
	$scope.FetchUserRolesByCompany = function () {
		adminServices.GetUserRolesByCompany($scope.roleObj.CompanyID, $scope.roleObj.DivisionID)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.parentRolesColl = response.data;
				if($scope.roleObj.ParentRoleID === undefined){
					$scope.roleObj.ParentRoleID = $scope.parentRolesColl[0].CompanyUserRoleID;
				}
			} else {
				$scope.parentRolesColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Fetch Application Permission List */
	$scope.FetchApplicationPermissionList = function () {
		userServices.GetApplicationPermissionList()
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.appPermissionList = response.data;
			} else {
				$scope.appPermissionList = false;
			}
			setPermissionStatus($scope.appPermissionList);
		},
		function (rejection) {});
	};
	$scope.FetchApplicationPermissionList();
	
	
	function setPermissionStatus(list){
		if($scope.roleObj.permissions !== undefined){
			for(var i = 0; i < list.length; i++){
				list[i].Permission = $scope.roleObj.permissions.filter(function(item){ return (item.ApplicationID === list[i].PermissionID); })[0].Permission;
			}
		}
		else{
			for(var i = 0; i < list.length; i++){
				list[i].Permission = false;
			}
		}
	}
	
	
	// Grid Columns structure
	$scope.gridColumns = [
		{ title: 'Permission Name', field: 'PermissionName', template: "", width:"450px"},
		{ title: 'View', field: 'PermissionID', template: "<div class='checkbox'><input type='checkbox' class='statusCheckBox' ng-checked='#:Permission#' id='#:PermissionID#'><label for='#:PermissionID#'></label></div>", width:"100px"}
	];
	
	
	/* Save Role */
	$scope.saveRole = function(){
		$scope.roleObj.ParentRoleID = (($scope.roleObj.ParentRoleID === undefined) || ($scope.roleObj.ParentRoleID === null)) ? 0 : $scope.roleObj.ParentRoleID;
		$scope.roleObj.permissions = fetchCheckedIds();
		adminServices.AddUpdateCompanyUserRole($scope.roleObj)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
		        $state.go('admin.userroles');
			} else {
				notify({
		            message: 'Failed to save role',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			}
		},
		function (rejection) {});
	};
	
	
	// Fetch checked checkboxes ids
	function fetchCheckedIds(){
		var checkIds = [];
		angular.element('input[type="checkbox"].statusCheckBox').each(function(index){
			checkIds.push({
				'ApplicationID': Number(angular.element(this).attr('id')),
				'Permission': angular.element(this).prop('checked')
			});
		});
		return checkIds;
	}
	
		
}]);

angular.module("imsApp")
.controller("DivisionsController", ['$scope', '$state', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, companyServices, $window, $uibModal, notify){
	
	console.log('Divisions controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
	
	/* Fetch Company Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
	
  							
	$scope.gridColumns = [
		{ field: "DivisionName", title:"Division Name",headerTemplate: "Division Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=DivisionID#, \"#=DivisionName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='admin.add_division({id:#=data.DivisionID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"30px"
		}
	];
	
	
	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "DivisionName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.divisionName);
								});
			$scope.observableList.data(data);
		}
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteDivision(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchCompanyDivisions();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}	
	
}]);

angular.module("imsApp")
.controller("AddDivisionController", ['$scope', '$state', '$stateParams', 'companyServices', '$window',
							function($scope, $state, $stateParams, companyServices, $window){
	
	console.log('Add Division controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.Action = true;
	$scope.division = {};
	$scope.division.CompanyID = $scope.sessionInfo.CompanyID;
	
	
	if($stateParams.id !== ""){
		$scope.division.DivisionID = $stateParams.id;
    	companyServices.GetDivisionInfo($scope.division.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.division = response.data;
			} else {
				$scope.division = false;
			}
		},
		function (rejection) {});
   	};
	
	$scope.saveFormData = function(){
		companyServices.AddUpdateCompanyDivision(JSON.stringify($scope.division))
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('admin.divisions');
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
		
}]);

angular.module("imsApp")
.controller("MapProductController", ['$scope', '$state', '$window', 'companyServices', 'notify', '$http',
								function($scope, $state, $window, companyServices, notify, $http){
	
	console.log('Map Product controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	$scope.submitted = false;
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
  	
  	function initVariables() {
  		$scope.mapProdDiv = {};
		$scope.mapProdDiv.CompanyID = $scope.sessionInfo.CompanyID;
		angular.element('.dropZoneContainer .title').html("Drag File here to Upload");
		angular.element('.fileContainer .name').html("Click here to upload Excel File");
		console.log('$scope.dropFileMsg: ',$scope.dropFileMsg);
  	}
  	initVariables();
	
	
	/* Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.mapProdDiv.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.mapProdDiv.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.FetchProductsByCompanyDivisions();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
	
	
	/* Fetch Company Product list */
	$scope.FetchCompanyProductList = function () {
		companyServices.GetCompanyProductList($scope.mapProdDiv.CompanyID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.productList = response.data;
			} else {
				$scope.productList = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyProductList();
	
	if(-1){
		console.log('if');
	}
	else{
		console.log('else');
	}
	/*------------ BULK MAPPING begins ------------*/
	
	/* Excel Upload */
	$scope.ExcelToJSON = function(){
		if($scope.ExcelFile !== undefined){
			var data = new Uint8Array($scope.ExcelFile);
			var arr_local = new Array();
			for(var i = 0; i < data.length; i++){
				arr_local[i] = String.fromCharCode(data[i]);
			}
			var str_local = arr_local.join('');
			
			var spreadsheet = XLSX.read(str_local, {type:'binary'});
			
			var worksheet = spreadsheet.Sheets[spreadsheet.SheetNames[0]];
			var worksheet_json = XLSX.utils.sheet_to_json(worksheet,{raw:true});
			var work_json_filtered = worksheet_json.filter(function(item){
					 return ($scope.divisionColl.findIndex(x => (x.DivisionName === item['BI Division Name']) ) !== -1);
				});
			if(work_json_filtered.length > 0) {
				var mutated_list = work_json_filtered.map(function(item){
					item.FOCUSED = (/^TRUE$/i.test(item.FOCUSED) || item.FOCUSED === 1) ? 1 : 0;
					item.New = (/^TRUE$/i.test(item.New) || item.New === 1) ? 1 : 0; 
					return ({'PFCID':item.PFC, 'IsFocus':item.FOCUSED, 'IsNew':item.New, 'BIDivisionName':item['BI Division Name']}); 
				});
				return {status:true, data:mutated_list};
			}
			else{
				return {status:false, data:'No valid row to map'};
			}
		}
		else{
		    return {status:false, data:'Please upload an excel'};
		}
	};
	
	$scope.bulkMapProductsToDivision = function(){
		var body = {};
		body.CompanyID = $scope.mapProdDiv.CompanyID;
		body.DivisionID = $scope.mapProdDiv.DivisionID;
		var excel_JSON = $scope.ExcelToJSON();
		if(excel_JSON.status){
			body.Productmapping = excel_JSON.data;
			companyServices.BulkProductMapping(body)
			.then(function (response) {
			    if (response.data != null && response.status === 200) {
			    	showNotification({message:'Products Mapped with Division successfully', duration: 2000, class:null, 
			    		routine:'$scope.FetchProductsByCompanyDivisions(); initVariables();'});
			    } else {
			       
			    }
			},
			function (rejection) { });
		}
		else{
			showNotification({message:excel_JSON.data, duration: 2000, class:'notify-bg', routine:null});
		}
			
	};

	/*------------ BULK MAPPING ends ------------*/
	
	
	/*------------ INDIVIDUAL MAPPING begins ------------*/
	/* Add Product to Division */
	$scope.mapProductToDivision = function(){
		companyServices.AddDivisionProductMapping($scope.mapProdDiv)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	// show success notification
		    	showNotification({message:'Product Mapped with Division successfully', duration: 2000, class:null, 
		    		routine:'$scope.FetchProductsByCompanyDivisions(); initVariables();'});
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	/* Fetch Products by Company Divisions */
	$scope.FetchProductsByCompanyDivisions = function () {
		companyServices.GetDivisionProducts($scope.mapProdDiv.CompanyID, $scope.mapProdDiv.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.products.data(response.data);
			} else {
				$scope.products.data = false;
			}
		},
		function (rejection) {});
	};
	
						
	$scope.gridColumns = [
		{ field: "PFCID", title:"PFC ID",headerTemplate: "PFC ID <span class='k-icon k-i-sort-desc'></span>", width:"100px"},
		{ field: "ProductName", title:"Product Name",headerTemplate: "Product Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "Manuf_Name", title:"Manufacturer",headerTemplate: "Manufacturer <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "IsOnFocus", title:"Is Focused",headerTemplate: "Is Focused <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='focus-#=PFCID#' ng-checked='(#=IsOnFocus# == 1)' ng-true-value='1' k-ng-model='#:IsOnFocus#'><label for='focus-#=PFCID#'></label></div>", width:"80px" },
		{ field: "IsNewIntroduction", title:"Is New Intro",headerTemplate: "Is New Intro <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='intro-#=PFCID#' ng-checked='(#=IsNewIntroduction# == 1)' ng-true-value='1' k-ng-model='#:IsNewIntroduction#'><label for='intro-#=PFCID#'></label></div>", width:"80px" }
	];
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	// Kendo Observable Array
	$scope.products = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "ProductName",
            dir: "asc"
        }
	});
	/*------------ INDIVIDUAL MAPPING ends ------------*/
	
	/*----- Show Notification -----*/
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
 }]);

angular.module("imsApp")
.controller("ProductsController", ['$scope', '$state', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, companyServices, $window, $uibModal, notify){
	
	console.log('Products controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		angular.element(".header-section-content-load").fadeIn(200);
		angular.element(".main-page-content-load").show(100);
			      
        angular.element("#tabTable li").on('click',function(){
            angular.element(this).closest('ul').find('li').removeClass('selected');
            angular.element(this).addClass('selected');
        });
        
        $scope.selectedTabPanel = 1;
	});
	
	/* Angular Provision for Tab Panel */
	$scope.showTab = function(id1, id2){
		angular.element(id1).fadeOut(500, function(){
			angular.element(id2).fadeIn(function(){
				if(id1 === '#tabs-4-tab-2'){
					$scope.selectedTabPanel = 1;
				}
				else {
					$scope.selectedTabPanel = 2;
				}
			});
		});
	}
	
	/* Initialize Models */
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
	
	
	/*---------------------- Products Starts ----------------------*/
	/* Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.DivisionID_brand = $scope.divisionColl[0].DivisionID;
				$scope.FetchProductsByCompanyDivisions();
				$scope.FetchDivisionBrand();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
	
	
	/* Fetch Products by Company Divisions */
	$scope.FetchProductsByCompanyDivisions = function () {
		companyServices.GetDivisionProducts($scope.CompanyID, $scope.DivisionID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.products.data(response.data);
			} else {
				$scope.products.data = false;
			}
		},
		function (rejection) {});
	};
	
	$scope.gridColumns = [
		{ field: "PFCID", title:"PFC ID",headerTemplate: "PFC ID <span class='k-icon k-i-sort-desc'></span>", width:"100px"},
		{ field: "ProductName", title:"Product Name",headerTemplate: "Product Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "Manuf_Name", title:"Manufacturer",headerTemplate: "Manufacturer <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "IsOnFocus", title:"Is Focused",headerTemplate: "Is Focused <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='focus-#=PFCID#' ng-checked='(#=IsOnFocus# == 1)' ng-true-value='1' k-ng-model='#:IsOnFocus#'><label for='focus-#=PFCID#'></label></div>", width:"80px" },
		{ field: "IsNewIntroduction", title:"Is New Intro",headerTemplate: "Is New Intro <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='checkbox'><input type='checkbox' id='intro-#=PFCID#' ng-checked='(#=IsNewIntroduction# == 1)' ng-true-value='1' k-ng-model='#:IsNewIntroduction#'><label for='intro-#=PFCID#'></label></div>", width:"80px" },
		{ field: "", title:"Delete", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='deleteConfirmationModal(#=PFCID#, \"#=ProductName#\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>", width:"70px" }
	];
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	// Kendo Observable Array
	$scope.products = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "ProductName",
            dir: "asc"
        }
	});
	
	// Delete Selected Product
	$scope.deleteSelectedProduct = function($uibModalInstance, id, name){
		companyServices.DeleteDivisionProductMapping($scope.DivisionID, $scope.CompanyID, id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchProductsByCompanyDivisions();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	/*---------------------- Products Ends ----------------------*/
	
	
	/*---------------------- Brands Starts ----------------------*/
	/* Fetch Brands by Company Divisions */
	$scope.FetchDivisionBrand = function () {
		companyServices.GetDivisionBrand($scope.CompanyID, $scope.DivisionID_brand, showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.brands.data(response.data);
			} else {
				$scope.brands.data = false;
			}
		},
		function (rejection) {});
	};
	
	$scope.gridColumns2 = [
		{ field: "BrandID", title:"Brand ID",headerTemplate: "Brand ID <span class='k-icon k-i-sort-desc'></span>", width:"100px"},
		{ field: "BrandName", title:"Brand Name",headerTemplate: "Brand Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"}
	];
	
	$scope.gridOptions2 = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	// Kendo Observable Array
	$scope.brands = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "BrandName",
            dir: "asc"
        }
	});
	/*---------------------- Brands Ends ----------------------*/
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	if(self.selectedTabPanel === 1){
                		self.deleteSelectedProduct($uibModalInstance, recordId, recordName);
                	}
                	else{
                		
                	}
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
}]);

angular.module("imsApp")
.controller("BrandGroupController", ['$scope', '$state', 'adminServices', 'companyServices', '$window', '$uibModal', 'notify',
							function($scope, $state, adminServices, companyServices, $window, $uibModal, notify){
	
	console.log('Brand Group controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
  	
  	
  	/* Initialize Models */
  	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.CompanyID = $scope.sessionInfo.CompanyID;
  	
  	
  	/* Divisions */
	$scope.FetchCompanyDivisions = function () {
		companyServices.GetCompanyDivisions($scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				$scope.DivisionID = $scope.divisionColl[0].DivisionID;
				$scope.FetchBrandGroupListByDivision();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchCompanyDivisions();
  	
  	
  	/* Fetch Brand Group List by Divisions */
	$scope.FetchBrandGroupListByDivision = function () {
		companyServices.GetBrandGroupList($scope.CompanyID, $scope.DivisionID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};
	
	$scope.gridColumns = [
		{ field: "BrandGroupName", title:"Brand Group Name",headerTemplate: "Brand Group Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=BrandGroupID#, \"#=BrandGroupName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='admin.addbrandgroup({brandGroupId:#:BrandGroupID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"30px" }
	];
	
	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "BrandGroupName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.BrandGroupName);
								});
			$scope.observableList.data(data);
		}
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteBrandGroup(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchBrandGroupListByDivision();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
		
}]);

angular.module("imsApp")
.controller("AddBrandGroupController", ['$scope', '$state', '$stateParams', 'adminServices', 'companyServices', 'notify', 'userServices', '$window',
							function($scope, $state, $stateParams, adminServices, companyServices, notify, userServices, $window){
	
	console.log('Add Brand Group controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.addBrandGroup = {};
	$scope.addBrandGroup.CompanyID = $scope.sessionInfo.CompanyID;
	
	
	/* State Params */
	if($stateParams.brandGroupId !== ''){
		FetchBrandGroupInfo($stateParams.brandGroupId);
	}
	else{
		FetchCompanyDivisions();
	}
	
	
	/* Fetch Brand info by brandId */
	function FetchBrandGroupInfo(brandGroupId) {
		companyServices.GetBrandGroupInfo(brandGroupId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.addBrandGroup = response.data;
				FetchCompanyDivisions();
			} else {
				$scope.addBrandGroup = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* Divisions */
	function FetchCompanyDivisions() {
		companyServices.GetCompanyDivisions($scope.addBrandGroup.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.divisionColl = response.data;
				if($scope.addBrandGroup.DivisionID === undefined){
					$scope.addBrandGroup.DivisionID = $scope.divisionColl[0].DivisionID;
				}
				$scope.FetchDivisionBrand();
			} else {
				$scope.divisionColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Fetch Brands by Company Divisions */
	$scope.FetchDivisionBrand = function () {
		companyServices.GetDivisionBrand($scope.addBrandGroup.CompanyID, $scope.addBrandGroup.DivisionID, showLoader, hideLoader)
		.then(function (response) {
			if ((response.data != null) && (response.status === 200)) {
				$scope.brandColls = response.data;
				if($scope.addBrandGroup.BrandIDs !== undefined){
					$scope.selectedBrands = $scope.addBrandGroup.BrandIDs.split(',');
					$scope.selectedBrands = $scope.selectedBrands.map(function(item){ return Number(item); });
				}
			} else {
				$scope.brandColls = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Save Brand Group */
	$scope.saveBrandGroup = function(){
		$scope.addBrandGroup.BrandIDs = $scope.selectedBrands.toString();
		companyServices.AddUpdateBrandGroup($scope.addBrandGroup)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
		        $state.go('admin.brandgroup');
			} else {
				notify({
		            message: 'Failed to save Brand Group',
		            classes: $scope.classes,
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			}
		},
		function (rejection) {});
	};
	
		
}]);

angular.module("imsApp")
.controller("LayoutSuperController", ['$scope', '$state', function($scope, $state){
	console.log('Layout Super controller...', $state);
	if($state.current.name === 'layout'){
		// $state.go('.analysis');
	}
		
}]);

'use strict';

(function($scope){
	function NavbarSuperController ($scope, $rootScope, $timeout, $window, $state, $uibModal, adminServices, notify, accountServices) {
		
		/* Parse session data to object format */
		function sessionInfoToScope(){
			$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
			console.log('$scope.sessionInfo: ',$scope.sessionInfo);
		}
		sessionInfoToScope();
		
		
		/* Handle an issued event */
		$rootScope.$on('UpdateNavbarSessionInfo', function(){
			sessionInfoToScope();
		});
		
		
		/* copied from app.js in HTML version */
		// Left mobile menu
		angular.element('.hamburger').click(function(){
			if (angular.element('body').hasClass('menu-left-opened')) {
				angular.element(this).removeClass('is-active');
				angular.element('body').removeClass('menu-left-opened');
				angular.element('html').css('overflow','auto');
			} else {
				angular.element(this).addClass('is-active');
				angular.element('body').addClass('menu-left-opened');
				angular.element('html').css('overflow','hidden');
			}
		});
	
		angular.element('.mobile-menu-left-overlay').click(function(){
			angular.element('.hamburger').removeClass('is-active');
			angular.element('body').removeClass('menu-left-opened');
			angular.element('html').css('overflow','auto');
		});
		
		// Right mobile menu
		angular.element('.site-header .burger-right').click(function(){
			if (angular.element('body').hasClass('menu-right-opened')) {
				angular.element('body').removeClass('menu-right-opened');
				angular.element('html').css('overflow','auto');
			} else {
				angular.element('.hamburger').removeClass('is-active');
				angular.element('body').removeClass('menu-left-opened');
				angular.element('body').addClass('menu-right-opened');
				angular.element('html').css('overflow','hidden');
			}
		});
		
		angular.element('.mobile-menu-right-overlay').click(function(){
			angular.element('body').removeClass('menu-right-opened');
			angular.element('html').css('overflow','auto');
		});
		/* copied from app.js */
		
		
		/* Logout Action */
		$scope.logoutSession = function(){
            accountServices.Logout(JSON.parse($window.localStorage.BI_Tool_Info).SessionID)
            .then(function(response) {}, function(rejection) {});
		};
		
		
		/* Change Password modal starts */
	    $scope.openConfirmationModal = function(){
	        var self = $scope;
	        var changePassword = $uibModal.open({
	            templateUrl:'changePasswordModal.html',
	            size: 'md',
	            windowTopClass: 'modal-top-class',
	            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
	            	$scope.passwords = {};
	            	$scope.passwords.UserID = self.sessionInfo.UserID;
	            	$scope.showError = false;
	                $scope.closeModal = function(){
	                    $uibModalInstance.close('close');
	                };
	                $scope.dismissModal = function(){
	                    $uibModalInstance.dismiss('dismiss');
	                };
	                $scope.setNewPassword = function(e){
	                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
	                		$scope.showError = true;
	                	}
	                	else{
	                		$scope.showError = false;
	                		self.setPassword($scope.passwords, $uibModalInstance);
	                	}
	                };
	            }]
	        });
	    };
	    /* Change Password modal ends */
	   
	   	// Set Password
		$scope.setPassword = function(passwords, $uibModalInstance){
			adminServices.ChangePassword(passwords)
			.then(function (response) {
			    if (response.data != null && response.status === 200) {
			    	if(response.data === "updated."){
			    		$uibModalInstance.close('close');
			    		showNotification('Password Updated Successfully', '', '$scope.logoutSession()');
			    	}
				    else{
				    	showNotification(response.data, 'notify-bg', '');
				    }
				        
			    } else {
			       
			    }
			},
			function (rejection) { });
		};
		
		
		// Show Notification
		function showNotification(message, noteClass, routine){
			noteClass = noteClass || '';
			notify({
	            message: message,
	            classes: noteClass,
	            templateUrl: $scope.template,
	            position: 'center',
	            duration: 2000
	        });
	        eval(routine);
		}
	}
	
	angular.module('imsApp')
	  .controller('NavbarSuperController', NavbarSuperController);
})();

'use strict';

angular.module('imsApp')
	.directive('navbarsuper', [function(){
		var directiveObj = {};
		directiveObj.templateUrl = 'app/components/navbars/navbar_super.html';
		directiveObj.restrict = 'A';
		directiveObj.controller = 'NavbarSuperController';
		directiveObj.controllerAs = 'nav';
		
		return directiveObj;
	}]);

angular.module("imsApp")
.controller("CustomerListController", ['$scope', '$state', '$stateParams', '$window', 'companyServices', 'adminServices', '$uibModal', 'notify',
								function($scope, $state, $stateParams, $window, companyServices, adminServices, $uibModal, notify){
	
	console.log('CustomerList controller...', $state);
	
	// Overwrite Session Info with state parameter data
	if($stateParams.sessionInfo !== ''){
		$window.sessionStorage.setItem('imsSessionInfo',JSON.stringify($stateParams.sessionInfo));
	}
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
                        
	$scope.statuscustlist = [ { action: "Active" }, { action: "Inactive" } ];
	
	// Subscription Count
	$scope.FetchCompanyCountBasedOnSubscription = function () {
	    companyServices.GetCompanyCountBasedOnSubscription()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.companyCountSubscription = response.data;
		    } else {
		        $scope.companyCountSubscription = false;
		    }
		},
		function (rejection) { });
	}
	$scope.FetchCompanyCountBasedOnSubscription();
	
	
	// Customer or Company List
	$scope.FetchsubscribedCompanyList = function () {
	    companyServices.GetsubscribedCompanies()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
		    } else {
		        $scope.customerList = false;
		    }
		},
		function (rejection) { });
	}
	$scope.FetchsubscribedCompanyList();

	
	$scope.customerListCols = [
						 { field: "CompanyName", title: "Company Name", headerTemplate: "Company Name<span class='k-icon k-i-sort-desc'></span>", width: "200px" },
						 { field: "LocationName", title: "Locations", headerTemplate: "Locations<span class='k-icon k-i-sort-desc'></span>", width: "150px" },
						 { field: "Division", title: "Division", headerTemplate: "Division<span class='k-icon k-i-sort-desc'></span>", width: "100px" },
						 { field: "TeamStrength", title: "Team Strength", headerTemplate: "Team Str<span class='k-icon k-i-sort-desc'></span>", width: "120px" },
						 { field: "ActiveUsers", title: "Active Users", headerTemplate: "Active Users<span class='k-icon k-i-sort-desc'></span>", width: "120px" },
						 { field: "SubscriptionName", title: "Subscription Type", headerTemplate: "Subscription Type<span class='k-icon k-i-sort-desc'></span>",
						 	template: "<span class='subscription-box' style='background-color:#:ColorCode#'>" + "<span class='box-text'>#:SubscriptionName#</span></span>"
						 	+ "<p class='filter-section-title'>#:DaysRemaning# days to go</p>", width: "180px" },
						 { field: "SubscriptionLocation", title: "Subscription Location", headerTemplate: "Subscription Location<span class='k-icon k-i-sort-desc'></span>",
						 	template: "<div>#=SubscriptionLocation#</div>", width: "200px" },
						 { field: "", title:"Contact Info", template:"<div>#=ContactPersonName#</div><div>#=ContactPersonPhone#</div>", width:"150px" },
						 { field: "Action", title: "Action", headerTemplate: "Action<span class='k-icon k-i-sort-desc'></span>", template: "<div><i class='glyphicon glyphicon-ok' aria-hidden='true' ng-if='(#=Action# === 1)'></i>"
						 	+ "<i class='glyphicon glyphicon-remove' aria-hidden='true' ng-if='(#=Action# === 0)'></i></div>", width: "100px" },
						 { field: "", title:"Edit", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ui-sref='super.addcustomer({customerId:#=CompanyID#, CompanyName:\"#=CompanyName#\"})'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></div>", width:"50px" },
						 { field: "", title:"PWD", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='openConfirmationModal(#=AdminUserID#);'><i class='fa fa-key' aria-hidden='true'></i></a></div>", width:"70px" },
						 { field: "", title:"Delete", template:"<div class='icon-team-permission'><a href='javascript:void(0)' ng-click='deleteConfirmationModal(#=CompanyID#, \"#=CompanyName#\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a></div>", width:"70px" },
						 { field: "", title: "", headerTemplate: "", template: "<a ng-click='NavigateToComapany(#=CompanyID#);' class='icon-custom-1' title='To Dashboard'><i class='glyphicon glyphicon-circle-arrow-right' aria-hidden='true'></i></a>", width: "100px" }
					];
					
	
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 5,
		sort: {
            field: "CompanyName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 5, 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
				
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if(($scope.TotalData !== undefined) || ($scope.TotalData !== null) || ($scope.TotalData.length > 0)){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
							return regExpression.test(item.CompanyName);
						});
			$scope.observableList.data(data);
		}
	}
	
	
	/* Change Password modal starts */
    $scope.openConfirmationModal = function(UserID){
        var self = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'setPasswordModal.html',
            size: 'md',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.passwords = {};
            	$scope.passwords.isAdmin = 1;
            	$scope.passwords.UserID = UserID;
            	$scope.showError = false;
                $scope.closeModal = function(){
                    $uibModalInstance.close('close');
                };
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.setNewPassword = function(e){
                	if(($scope.passwords.NewPassword === undefined) || ($scope.passwords.NewPassword !== $scope.ConfirmPassword)){
                		$scope.showError = true;
                	}
                	else{
                		$scope.showError = false;
                		self.setPassword($scope.passwords, $uibModalInstance);
                	}
                };
            }]
        });
    };
    /* Change Password modal ends */
   
   
   	// Set Password
	$scope.setPassword = function(passwords, $uibModalInstance){
		adminServices.ChangePassword(passwords)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data === "updated."){
		    		$uibModalInstance.close('close');
		    		showNotification({message:'Password Updated Successfully', duration: 2000, class:null, routine:null});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {
		       
		    }
		},
		function (rejection) { });
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteCompany(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchsubscribedCompanyList();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
	
	// Navigate to the selected Company page
	$scope.NavigateToComapany = function(companyId) {
		$scope.sessionInfo.CompanyID = companyId;
		$state.go('admin.userlist', {'sessionInfo':$scope.sessionInfo});
	};
	
}]);

angular.module("imsApp")
.controller("AddCustomerController", ['$scope', '$state', 'userServices', 'companyServices', 'mediaServices', '$stateParams', '$uibModal', '$rootScope',
								function($scope, $state, userServices, companyServices, mediaServices, $stateParams, $uibModal, $rootScope){

	console.log('Add Customer controller...', $state);

	angular.element(document).ready(function(){

	});


	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		showLoader = "angular.element('.loader-backdrop').fadeIn();";		// Global Variables
        hideLoader = "angular.element('.loader-backdrop').fadeOut();";

		$scope.rootScope = $rootScope;
		$scope.customer = {};
		$scope.customer.Country = 'India';
		$scope.customer.ContactInfo = {};
		$scope.customer.Action = 1;
        $scope.customer.ColorCode = '#874396';

		$scope.imgBrowsed = false;
		$scope.avatarSelected = false;
		$scope.imgConfirmed = false;
		$scope.baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');
		$scope.imgDataUrl = $scope.baseURL + 'avatars/demo_logo.jpg';
		$scope.customer.ProfileImage = 'avatars/demo_logo.jpg';

		$scope.customer.CompanyAccessRights = {};
		$scope.accessRightTypesColl = [ {value:1, label:'All'}, {value:2, label:'Super Group'}, {value:3, label:'TC4 Classes'} ];
		$scope.customer.CompanyAccessRights.AccessRightType = 1;
		TherapyList_2 = []; TherapyList_3 = [];		// Global Variables
		initTwoSidedMultiselectWidget();
	}


	// State Paramas
	if(($stateParams.customerId !== '') && ($stateParams.CompanyName !== '')){
		$scope.customer.isUpdate = 1;
		$scope.CompanyName = $stateParams.CompanyName;
		FetchCompanyInfoById($stateParams.customerId);
	}
	else{
		$scope.customer.isUpdate = 0;
		FetchCompanyList();
		FetchCityList();
	}

	FetchStateList();

	// Fetch company info based on company_id
	function FetchCompanyInfoById(company_id) {
		companyServices.GetSubscribedCompanyInfo(company_id)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.customer = response.data;
				applyCompanyInfoResponse();
			} else {
				$scope.customer = false;
			}
		},
		function (rejection) {});
	}

	function applyCompanyInfoResponse() {
		FetchCityList();
		$scope.customer.Country = ($scope.customer.Country === '') ? 'India' : $scope.customer.Country;
		$scope.customer.SubscriptionStartDate = new Date($scope.customer.SubscriptionStartDate);
		$scope.customer.SubscriptionEndDate = new Date($scope.customer.SubscriptionEndDate);
		$scope.imgDataUrl = ($scope.customer.ProfileImage === '') ? $scope.imgDataUrl : ($scope.baseURL + $scope.customer.ProfileImage);
		$scope.accessRightTypeChanged();
	}

	// Provision for Divisions
	$scope.addFields = function() {
		$scope.Divisions = [];
		for(var i = 0; i < Number($scope.customer.NoOfDivision); i++){
			$scope.Divisions.push({name: ''});
		}
	};


	// Datepicker initialization
	$scope.datePickerOptions = {
		start: 'date',
		depth: 'year'
	};

	$scope.customer.SubscriptionStartDate = new Date();
	$scope.customer.SubscriptionEndDate = new Date();


	$scope.dateChange = function(){
		if($scope.customer.SubscriptionStartDate.getTime() > $scope.customer.SubscriptionEndDate.getTime()){
			$scope.customer.SubscriptionStartDate = $scope.customer.SubscriptionEndDate;
		}
	};


	/* Company */
	function FetchCompanyList() {
		companyServices.GetUnsubscribedCompanies()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.unsubscribedCompanies = response.data;
			} else {
				$scope.unsubscribedCompanies = false;
			}
		},
		function (rejection) {});
	}

	/* City */
	function FetchCityList() {
		userServices.GetAllCities()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.cityList = response.data;
				if($stateParams.customerId !== ''){
					$scope.SubscriptionLocationArray = $scope.customer.SubscriptionLocationID.split(',').map(Number);
					if(($scope.customer.CityID === 0) || ($scope.customer.CityID === null)){
						$scope.customer.CityID = $scope.customer.CityName;
					}
				}
			} else {
				$scope.cityList = false;
			}
		},
		function (rejection) {});
	};

	/* State */
	function FetchStateList() {
		userServices.GetAllStates()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.stateList = response.data;
			} else {
				$scope.stateList = false;
			}
		},
		function (rejection) {});
	};

	/* Pincodes List based on search */
	$scope.SearchPincodesList = function(event) {
		companyServices.GetPinCodeList(Number(event.filter.value))
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.pincodesList = response.data;
				if($scope.pincodesList.length === 0){
					$scope.customer.Pincode = event.filter.value;
				}
			} else {
				$scope.pincodesList = false;
			}
		},
		function (rejection) {});
	};


	$scope.FetchCityStateByPinCode = function() {
		if($scope.customer.Pincode === null){
			$scope.selectedPinResult = [];
			$scope.customer.CityID = 0;
			$scope.customer.StateID = 0;
			$scope.customer.Country = 'India';
		}
		else{
			companyServices.GetCityStateByPinCode($scope.customer.Pincode)
			.then(function (response) {
				if (response.data != null && response.status === 200) {
					$scope.selectedPinResult = response.data;
					$scope.customer.CityID = $scope.selectedPinResult.CityID;
					$scope.customer.StateID = $scope.selectedPinResult.StateID;
					$scope.customer.Country = $scope.selectedPinResult.CountryName;
				} else {
					$scope.selectedPinResult = false;
				}
			},
			function (rejection) {});
		}
	};


	/* Capture City name if city does not exist in DB */
	$scope.getEnteredCityName = function(event) {
		$scope.customer.CityName = event.filter.value;
	};

	/* Set CityID on close */
	$scope.setCityIDonClose = function(event) {
		$scope.customer.CityID = ($scope.customer.CityID === null) ? 0 : $scope.customer.CityID;
	};


	/* Company Subscription Types */
	$scope.FetchSubscriptionTypes = function() {
		companyServices.GetSubscriptionType()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.subscriptionTypes = response.data;
			} else {
				$scope.subscriptionTypes = false;
			}
		},
		function (rejection) {});
	};
	$scope.FetchSubscriptionTypes();


	/* Secondary Address is same as Primary Address */
	$scope.sameAsAbove = function() {
		$scope.customer.ContactInfo.Address = ($scope.customer.ContactInfo.is_same_as_company) ? $scope.customer.Address : '';
	};


    /* Image functionality begins */
    $scope.imageView = function() {
    	$scope.imgBrowsed = true;
		var reader = new FileReader();
		reader.onload = $scope.imageIsLoaded;
		reader.readAsDataURL($scope.ProfileImage);
	};


	$scope.imageIsLoaded = function(e){
	    $scope.$apply(function() {
	    	$scope.imgUrl = e.target.result;
	    });
	};


	function fetchAvatars() {
		$scope.avatarsList = ['demo_logo.jpg'];
	}
	fetchAvatars();


	$scope.browsedConfirmed = function() {
		$scope.imgConfirmed = true;
		$scope.imgDataUrl = $scope.imgUrl;
		$scope.customer.ProfileImage = $scope.ProfileImage;
	};


	$scope.avatarIsSelected = function(avatar) {
		$scope.avatarSelected = true;
		$scope.imgUrl = $scope.baseURL + 'avatars/' + avatar;
		$scope.ProfileImage = 'avatars/' + avatar;
	};

	/* Image Change modal begins */
	$scope.openImgChangeModal = function(UserID){
        var parent = $scope;
        var changePassword = $uibModal.open({
            templateUrl:'imgChangeModal.html',
            windowTopClass: 'imageChangeModal',
            controller:['$scope','$uibModalInstance', function($scope,$uibModalInstance){
            	$scope.parent = parent;
				parent.imgUrl = parent.imgDataUrl;
				parent.ProfileImage = parent.customer.ProfileImage;
				parent.imgBrowsed = false;
                $scope.closeModal = function(){
					$uibModalInstance.close('close');
					parent.browsedConfirmed();
                };

                $scope.dismissModal = function(){
                	parent.imgBrowsed = false;
                	parent.avatarSelected = false;
                    $uibModalInstance.dismiss('dismiss');
                };
            }]
        });
    };
    /* Image Change modal ends */


	/* Upload file to Database and return path */
	$scope.uploadFile = function(routine) {
        mediaServices.PostAnImage($scope.customer.ProfileImage)
		.then(function (response) {
		    if (response.data != null && response.status === 201) {
		        if(response.data.Message === 'Image Updated Successfully.'){
		        	$scope.customer.ProfileImage = $scope.customer.ProfileImage.name;
		        	if(routine !== undefined){
		        		eval(routine);
		        	}
		        }
		    } else {
		      	console.log('Failed to upload image: ',response);
		    }
		},
		function (rejection) { });
    };


	/* Fetch TC values for changes in access rights */
	$scope.accessRightTypeChanged = function(){
		if($scope.customer.CompanyAccessRights.AccessRightType === 1){
		}
		else if($scope.customer.CompanyAccessRights.AccessRightType === 2){
			initTwoSidedMultiselectWidget();
			// Verify Buffer
			if(TherapyList_2.length > 0){ filterTherapyListData(TherapyList_2); }
			else{ fetchSuperGroupList(); }
		}
		else{
			initTwoSidedMultiselectWidget();
			// Verify Buffer
			if(TherapyList_3.length > 0){ filterTherapyListData(TherapyList_3); }
			else{ fetchTC4_TherapyList(); }
		}
	};

	/* Fetch Super Group Therapy list */
	function fetchSuperGroupList(){
		userServices.GetSupergroupList(showLoader, hideLoader)
		.then(function (response) {
			if (response.data != null && response.status === 200 && response.data.length > 0) {
				TherapyList_2 = response.data;
				if(TherapyList_2 !== null){
					filterTherapyListData(TherapyList_2);
				}
			} else {}
		},
		function (rejection) {});
	}

	/* Fetch TC4 Therapy list */
	function fetchTC4_TherapyList(){
		companyServices.GetTherapyListByTherapyCode('tc4', null, showLoader, hideLoader)
        .then(function (response) {
            if (response.data != null && response.status === 200 && response.data.length > 0) {
				TherapyList_3 = response.data;
				if(TherapyList_3 !== null){
					filterTherapyListData(TherapyList_3);
				}
            } else {}
        },
        function (rejection) {});
	}

	/* Filter therapy list data into two arrays to feed into Two sided multiselect */
	function filterTherapyListData(data){
		if($scope.customer.isUpdate === 0){
			$scope.leftArray = data;
		}
		else{
			if($scope.customer.CompanyAccessRights.TCValues !== null){
				var TcValues = $scope.customer.CompanyAccessRights.TCValues.split(',');
				if(TcValues.length > 0){
					for(var i = 0; i < data.length; i++){
						if(TcValues.indexOf(data[i].TherapyCode) === -1){
							$scope.leftArray.push(data[i]);
						}
						else{
							$scope.rightArray.push(data[i]);
						}
					}
				}
				else{
					$scope.leftArray = data;
				}
			}
			else{
				$scope.leftArray = data;
			}
		}
	}


	/* Two sided multisided widget begins */
	function initTwoSidedMultiselectWidget(){
		$scope.leftBag = [];
		$scope.rightBag = [];
		$scope.leftArray = [];
		$scope.rightArray = [];
	}

	$scope.reserveElement = function(key, id){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var local_i = idsPool.indexOf(id);
		if(local_i > -1){
			idsPool.splice(local_i, 1);
		}
		else{
			idsPool.push(id);
		}
	};

	$scope.moveElements = function(key, source, destination){
		var idsPool = (key === 'left') ? $scope.leftBag : $scope.rightBag;
		var temp_i, temp_item = null;
		for(var i = 0; i < idsPool.length; i++){
			temp_i = source.findIndex(x => (x.TherapyCode === idsPool[i]));
			temp_item = source.splice(temp_i, 1)[0];
			destination.push(temp_item);
		}
		if(key === 'left'){ $scope.leftBag = []; }
		else{ $scope.rightBag = []; }
	};
	/* Two sided multisided widget ends */


	/* Prepare Customer info to be saved */
	function prepareCustomerToBeSaved(){
		$scope.customer.ContactInfo.is_same_as_company = ($scope.customer.ContactInfo.is_same_as_company === true) ? 1 : 0;
		if($scope.customer.isUpdate === 0){
			$scope.customer.Divisions = $scope.Divisions.filter(function(item){ return (item.name !== ''); }).map(function(item){ return (item.name); });
			$scope.customer.Divisions = $scope.customer.Divisions.toString();
			$scope.customer.CompanyAccessRights.CompanyID = $scope.customer.CompanyID;		// Company Access Rights
		}
		/* Company Access Rights */
		if($scope.customer.CompanyAccessRights.AccessRightType === 1){
			$scope.customer.CompanyAccessRights.TCValues = null;
		}
		else{
			if($scope.rightArray.length > 0){
				$scope.customer.CompanyAccessRights.TCValues = $scope.rightArray.map(function(item){return item.TherapyCode;}).toString();
			}
			else{
				$scope.customer.CompanyAccessRights.TCValues = null;
			}
		}
		$scope.customer.SubscriptionLocationID = $scope.SubscriptionLocationArray.toString();
	}

	/* Create Customer */
	$scope.saveCustomer = function () {
		prepareCustomerToBeSaved();
	    companyServices.AddCompanySubscription(JSON.stringify($scope.customer))
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('super.customerlist');
		    } else {}
		},
		function (rejection) { });
	};


	/* Upload Image file before saving user info */
	$scope.interceptToUploadImage = function() {
		if($scope.imgConfirmed && !($scope.avatarSelected)){
			$scope.uploadFile('$scope.saveCustomer();');
		}
		else{
			$scope.saveCustomer();
		}
	};


}]);

angular.module("imsApp")
.controller("SubscriptionsController", ['$scope', '$state', 'companyServices', 'userServices',
								function($scope, $state, companyServices, userServices){
	
	console.log('Subscriptions controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* initialize models and variables*/
	initParameters();
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	
	function initParameters() {
		fetchCompanySubscriptionTypes();
		fetchSubscriptionLocations();
	}
	
	
	/* Kendo Grid configuration for Subscription Types */
	$scope.gridColumns = [
		{field:"subs", title:"Subscription Type", template:"<span class='colorbox' style='background-color:#:ColorCode#'><span class='notSet' ng-if='(#:ColorCode === \"\"#)'>NotSet</span></span>"
		+ "<span class='plat-subs'>#:SubscriptionName#</span>", width:"300px"},
		{field:"action", title:"Action", headerTemplate: "<div class='text-center'>Action</div>", template:"<div class='icon-subs text-center'>"
		+ "<a href='javascript:void(0)' ui-sref='super.addnewsubscription({subscriptionId:#=SubscriptionTypeID#})' title='Edit'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"80px"}
	];
	
	
	$scope.gridOptions = {
		columns: $scope.gridColumns,
		pageable: {
	        numeric: true,
	        pageSizes: [ 5, 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 5,
		sort: {
            field: "SubscriptionName",
            dir: "asc"
        }
	});
	
	
	/* Fetch Company Subscription Types */
	function fetchCompanySubscriptionTypes() {
		companyServices.GetSubscriptionType(showLoader, hideLoader)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	$scope.observableList.data(response.data);
		    } else {
		        $scope.observableList.data([]);
		    }
		},
		function (rejection) { });
	}
	
	
	/* Kendo Grid configuration for Locations */
	$scope.gridColumns2 = [
		{field:"CityName",title:"Location", width:"300px"}
	];
	
	
	$scope.gridOptions2 = {
		columns: $scope.gridColumns2,
		pageable: {
	        numeric: true,
	        pageSizes: [ 5, 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	
	$scope.observableList2 = new kendo.data.DataSource({
		data: [],
		pageSize: 5,
		sort: {
            field: "CityName",
            dir: "asc"
        }
	});
	
	
	/* Fetch Subscription Locations */
	function fetchSubscriptionLocations() {
		userServices.GetAllCities()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.TotalData = response.data
				$scope.observableList2.data($scope.TotalData);
			} else {
				$scope.TotalData = [];
			}
		},
		function (rejection) {});
	}
	
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if(($scope.TotalData !== undefined) || ($scope.TotalData !== null)){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
							return regExpression.test(item.CityName);
						});
			$scope.observableList2.data(data);
		}
	}
	

}]);

angular.module("imsApp")
.controller("AddNewSubscriptionController", ['$scope', '$state', '$stateParams', '$uibModal', 'companyServices', 'userServices',
										function($scope, $state, $stateParams, $uibModal, companyServices, userServices){
	
	console.log('Add New Subscription controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Models & Variables Initialization */
	initParameters();
	function initParameters() {
		$scope.subscription = {};
		$scope.subscription.ColorCode = '#8C4299';
		$scope.subscription_periods = [
			{ id: 1, name: "Quaterly" },
			{ id: 2, name: "Monthly" },
			{ id: 3, name: "Half Yearly" },
			{ id: 4, name: "Yearly" }
		];
		$scope.subscription.period = 1;
		$scope.subscription.active = 1;
	}
	
	
	/* State Paramas */
	if($stateParams.subscriptionId !== ''){
		fetchSubscriptionInfoById($stateParams.subscriptionId);
		$scope.subscription.isUpdate = 1;
	}
	else{
		$scope.subscription.isUpdate = 0;
		fetchSubscriptionLocations();
	}
	
	
	/* Fetch Subscription Info by Id */
	function fetchSubscriptionInfoById(subscriptionId) {
		companyServices.GetSubscriptionTypeDetails(subscriptionId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.subscription = response.data[0];
				applySubscriptionInfo();
			} else {
				$scope.subscription = false;
			}
		},
		function (rejection) {});
	}
	
	
	function applySubscriptionInfo() {
		fetchSubscriptionLocations();
	}
	
	
	/* Fetch Subscription Locations */
	function fetchSubscriptionLocations() {
		userServices.GetAllCities()
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.subLocations = response.data;
				if($stateParams.subscriptionId !== ''){
					$scope.subscription.Gelocation = $scope.subscription.Gelocation.split().map(Number);
				}
			} else {
				$scope.subLocations = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* Save Subscription Data */
	$scope.addNewSubscription = function() {
		$scope.subscription.Gelocation = $scope.subscription.Gelocation.toString();
		companyServices.AddUpdateSubscribedType($scope.subscription)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $state.go('super.subscriptions');
		    } else {
		      	 
		    }
		},
		function (rejection) { });
	};
	
}]);

angular.module("imsApp")
.controller("AddClusterController", ['$scope', '$state', '$stateParams', 'userServices', 'companyServices', 'notify', '$window',
							function($scope, $state, $stateParams, userServices, companyServices, notify, $window){
	
	console.log('Add Cluster controller...', $state);
	
	// Parse session data to object format
	$scope.sessionInfo = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
	
	angular.element(document).ready(function(){
		
	});
	
	
	/* Initialize Models */
	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
	$scope.addCluster = {};
	$scope.GeographyFilter = 'city';
	
	
	/* State Params */
	if($stateParams.clusterId !== ''){
		FetchClusterInfo($stateParams.clusterId);
	}
	else{
		FetchsubscribedCompanyList();
	}
	
	
	/* Fetch Cluster info by clusterId */
	function FetchClusterInfo(clusterId) {
		companyServices.GetCompanyClusterInfo(clusterId)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.addCluster = response.data;
				FetchsubscribedCompanyList();
			} else {
				$scope.addCluster = false;
			}
		},
		function (rejection) {});
	}
	
	
	/* Subscribed Companies List */
	function FetchsubscribedCompanyList() {
	    companyServices.GetsubscribedCompanies()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.customerList = response.data;
		        if($scope.addCluster.ClusterID === undefined){
		        	$scope.addCluster.CompanyID = response.data[0].CompanyID;
		        }
		        $scope.FetchCities();
		    } else {
		        $scope.customerList = false;
		    }
		},
		function (rejection) { });
	}
	
	
	/* City */
	$scope.FetchCities = function() {
		userServices.GetGeographyValues($scope.GeographyFilter, $scope.addCluster.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.cityColl = response.data;
				if($scope.addCluster.ClusterID === undefined){
					$scope.addCluster.CityID = $scope.cityColl[0].GeographyID;
				}
				$scope.FetchPatchesByCity();
			} else {
				$scope.cityColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Patches */
	$scope.FetchPatchesByCity = function () {
		userServices.GetPatchListBycityID($scope.addCluster.CityID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.patchesColl = response.data;
				if($scope.addCluster.ClusterID !== undefined){
					$scope.selectedPatches = $scope.addCluster.PatchIDs.split(',');
					$scope.selectedPatches = $scope.selectedPatches.map(function(item){ return Number(item); });
				}
			} else {
				$scope.patchesColl = false;
			}
		},
		function (rejection) {});
	};
	
	
	/* Save Cluster */
	$scope.saveCluster = function(){
		$scope.addCluster.PatchIDs = $scope.selectedPatches.toString();
		companyServices.AddUpdateCluster($scope.addCluster)
		.then(function (response) {
			if (response.data !== null && response.status === 200) {
		        $state.go('super.clusters');
			} else {
				notify({
		            message: 'Failed to save Cluster',
		            classes: 'notify-bg',
		            templateUrl: $scope.template,
		            position: 'center',
		            duration: 2000
		        });
			}
		},
		function (rejection) {});
	};
	
		
}]);

angular.module("imsApp")
.controller("ClustersController", ['$scope', '$state', 'userServices', 'companyServices', '$uibModal', 'notify',
							function($scope, $state, userServices, companyServices, $uibModal, notify){
	
	console.log('Clusters controller...', $state);
	
	angular.element(document).ready(function(){
		
	});
  	
  	
  	/* Initialize Models */
  	var showLoader = "angular.element('.loader-backdrop').fadeIn();";
  	var hideLoader = "angular.element('.loader-backdrop').fadeOut();";
  	$scope.GeographyFilter = 'city';
  	
  	
  	/* Subscribed Companies List */
	function FetchsubscribedCompanyList() {
	    companyServices.GetsubscribedCompanies()
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		        $scope.customerList = response.data;
		        $scope.CompanyID = response.data[0].CompanyID;
		        $scope.FetchCities();
		    } else {
		        $scope.customerList = false;
		    }
		},
		function (rejection) { });
	}
	FetchsubscribedCompanyList();
	
  	
  	/* City */
	$scope.FetchCities = function() {
		userServices.GetGeographyValues($scope.GeographyFilter, $scope.CompanyID)
		.then(function (response) {
			if (response.data != null && response.status === 200) {
				$scope.cityColl = response.data;
				$scope.CityID = $scope.cityColl[0].GeographyID;
				$scope.FetchCompanyClusterListByCity();
			} else {
				$scope.cityColl = false;
			}
		},
		function (rejection) {});
	};
  	
  	/* Fetch Cluster List by City */
	$scope.FetchCompanyClusterListByCity = function () {
		companyServices.GetCompanyClusterList($scope.CompanyID, $scope.CityID, showLoader, hideLoader)
		.then(function (response) {
			if (response.data.length > 0  && response.status === 200) {
				$scope.observableList.data(response.data);
				$scope.TotalData = response.data;
			} else {
				$scope.TotalData = false;
			}
		},
		function (rejection) {});
	};
	
	$scope.gridColumns = [
		{ field: "ClusterName", title:"Cluster Name", headerTemplate: "Cluster Name <span class='k-icon k-i-sort-desc'></span>", width:"270px"},
		{ field: "action", title:"Action",headerTemplate: "Action <span class='k-icon k-i-sort-desc'></span>",
			template:"<div class='icon-team-permission'>"+
					"<a class='m-r-1' ng-click='deleteConfirmationModal(#=ClusterID#, \"#=ClusterName#\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
					"<a ui-sref='super.addcluster({clusterId:#:ClusterID#})'><i class='fa fa-pencil' aria-hidden='true'></i></a></div>", width:"30px" }
	];
	
	// Kendo Observable Array
	$scope.observableList = new kendo.data.DataSource({
		data: [],
		pageSize: 10,
		sort: {
            field: "ClusterName",
            dir: "asc"
        }
	});
	
	$scope.gridOptions = {
		pageable: {
	        numeric: true,
	        pageSizes: [ 10, 25, 50, 'All' ],
	        previousNext: true,
	        input: false,
	        info: false
		}
	};
	
	/* Search Functionality */
	$scope.fetchResultSet = function(){
		if($scope.TotalData.length > 0){
			var regExpression = new RegExp($scope.searchQuery, 'i');
			var data = $scope.TotalData.filter(function(item){
									return regExpression.test(item.ClusterName);
								});
			$scope.observableList.data(data);
		}
	};
	
	
	/* Delete Confimation modal starts */
    $scope.deleteConfirmationModal = function(recordId, recordName){
        var self = $scope;
        var confirmDeleteModal = $uibModal.open({
            templateUrl:'deleteConfimationModal.html',
            size: 'sm',
            windowTopClass: 'modal-top-class',
            controller:['$scope','$uibModalInstance',function($scope,$uibModalInstance){
            	$scope.message = "Do you want delete the record for " + recordName + " ?";
                $scope.dismissModal = function(){
                    $uibModalInstance.dismiss('dismiss');
                };
                $scope.confirmDelete = function(e){
                	self.deleteSelected($uibModalInstance, recordId, recordName);
                };
            }]
        });
    };
    /* Delete Confimation modal ends */
	
	// Delete Selected Record
	$scope.deleteSelected = function($uibModalInstance, id, name){
		companyServices.DeleteCompanyCluster(id)
		.then(function (response) {
		    if (response.data != null && response.status === 200) {
		    	if(response.data){
		    		$uibModalInstance.close('close');
		    		showNotification({message:name+' Deleted Successfully', duration: 2000, class:null, routine:'$scope.FetchCompanyClusterListByCity();'});
		    	}
			    else{
			    	showNotification({message:response.data, duration: 2000, class:'notify-bg', routine:null});
			    }
			        
		    } else {}
		},
		function (rejection) { });
	};
	
	
	// Show Notification
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: $scope.template,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}
	
		
}]);


'use strict';
IMSModule.factory('userServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var userServiceFactory = {};

    //Get All Cities
    var _GetAllCities = function () {
        return $http.get(serviceBase + "Filter/GetAllCities")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get All States
    var _GetAllStates = function () {
        return $http.get(serviceBase + "Filter/GetAllState")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get All Zones
    var _GetAllZones = function () {
        return $http.get(serviceBase + "Filter/GetAllZone")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get All Geography
    var _GetGeographyValues = function (filterType, companyId, sessionId) {
        return $http.get(serviceBase + "Filter/GetGeographyValues", {params: {FilterType: filterType, CompanyID: companyId, SessionID: sessionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get by TestName
    var _GetTimePeriods = function (cityId) {
        return $http.get(serviceBase + "Filter/GetAllTimePeriods", {params: {CityID:cityId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    //Get by Bricks By Geography
    var _GetBricksByGeography = function (filterType, geographyId, companyId, divisionId) {
        return $http.get(serviceBase + "Filter/GetBricksByGeography", {params: {FilterType:filterType, GeographyID:geographyId, CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


	// Category Values for Selected category type
    var _GetCategoryValues = function (catType, companyId, divisionId, sessionId) {
        return $http.get(serviceBase + "Filter/GetCategoryValues", {params: {CategoryType:catType, CompanyID:companyId, DivisionID:divisionId, SessionID:sessionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    // Category Values for Selected category type
    var _GetApplicationPermissionList = function () {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.get(serviceBase + "Filter/GetApplicationPermissionList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    // Get patches by CityID
    var _GetPatchListBycityID = function (cityId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetPatchListBycityID", {params: {CityID: cityId}})
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get PacthIDs by ClusterIDs
    var _GetPatchIDsByClusterIDs = function (clusterIds, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetPatchIDsByClusterIDs", {params: {ClusterIDs: clusterIds}})
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get List of TC values for supergroup
    var _GetSupergroupList = function (LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetSupergroupList")
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    userServiceFactory.GetAllCities = _GetAllCities;
    userServiceFactory.GetAllStates = _GetAllStates;
    userServiceFactory.GetAllZones = _GetAllZones;
    userServiceFactory.GetGeographyValues = _GetGeographyValues;
    userServiceFactory.GetTimePeriods = _GetTimePeriods;
    userServiceFactory.GetBricksByGeography = _GetBricksByGeography;
    userServiceFactory.GetCategoryValues = _GetCategoryValues;
    userServiceFactory.GetApplicationPermissionList = _GetApplicationPermissionList;
    userServiceFactory.GetPatchListBycityID = _GetPatchListBycityID;
    userServiceFactory.GetPatchIDsByClusterIDs = _GetPatchIDsByClusterIDs;
    userServiceFactory.GetSupergroupList = _GetSupergroupList;

    return userServiceFactory;
}]);

'use strict';
IMSModule.factory('companyServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var companyServiceFactory = {};

    //Get Company List
    var _GetCompanyList = function () {
        return $http.get(serviceBase + "Company/GetCompanyList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Subscription Type
    var _GetSubscriptionType = function (LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetCompanySubscriptionType")
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add & Update Company Subscription Type info
    var _AddUpdateSubscribedType = function (payload) {
        return $http.post(serviceBase + 'Company/AddUpdateSubscribedType', payload)
                .then(function (results) {
                    return $q.resolve(results);
                },
                function (rejection) {
                    return $q.reject(rejection);
                }
            );
    };


    // Get Subscription Type Details by subscription Id
    var _GetSubscriptionTypeDetails = function (subscriptionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetSubscriptionTypeDetails", {params: {SubscriptionTypeID:subscriptionId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    //Get Unsubscribed Company
    var _GetUnsubscribedCompanies = function () {
        return $http.get(serviceBase + "Company/GetUnsubscribedCompanyList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };

    //Get Subscribed Company
    var _GetsubscribedCompanies = function () {
        return $http.get(serviceBase + "Company/GetSubscribedCompanyList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


	// Post Company Subscription
    var _AddCompanySubscription = function (objCompanySubcription) {
        return $http.post(serviceBase + 'Company/AddUpdateSubscribedCompany', objCompanySubcription)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };


    //Get Subscribed Company
    var _GetCompanyPerformance = function (bodyObject) {
		angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + "Company/GetCompanyPerformance", bodyObject)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Performance Map
    var _GetCompanyPerformanceMap = function (bodyObject) {
		angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + "Company/GetCompanyPerformanceMap", bodyObject)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
				angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Count based on Subscriptions
    var _GetCompanyCountBasedOnSubscription = function (TimePeriodID, isViewTen) {
        return $http.get(serviceBase + "Company/GetCompanyCountBasedOnSubscription")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Company Divisions
    var _GetCompanyDivisions = function (companyId) {
    	// angular.element('.loader-backdrop').fadeIn();
        return $http.get(serviceBase + "Company/GetCompanyDivisions", {params: {CompanyID:companyId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                // angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                // angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    //Get Comparision Type
    var _GetComparisionTypeValue = function (filterType, sessionId, tcCode, therapyId) {
        return $http.get(serviceBase + "Filter/GetComparisionTypeValue", {params: {FilterType:filterType, SessionID:sessionId, TCcode:tcCode, TherapyID:therapyId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Therapy List by Therapy Code
    var _GetTherapyListByTherapyCode = function (tcCode, sessionId, LoaderShow, LoaderHide) {
        if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetTherapyListByTherapyCode", {params: {TCcode:tcCode, SessionID:sessionId}})
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add & Update Company Division info
    var _AddUpdateCompanyDivision = function (divisionObj) {
        return $http.post(serviceBase + 'CompanyDivison/AddUpdateCompanyDivision', divisionObj)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };


    //Get Products by Company & Divisions
    var _GetDivisionProducts = function (companyId, divisionId, disableLoader) {
    	if(disableLoader === undefined){
    		angular.element('.loader-backdrop').fadeIn();
    	}
        return $http.get(serviceBase + "CompanyDivison/GetDivisionProducts", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
            	if(disableLoader === undefined){
            		angular.element('.loader-backdrop').fadeOut();
            	}
                return $q.resolve(results);
            },
            function (rejection) {
            	if(disableLoader === undefined){
					angular.element('.loader-backdrop').fadeOut();
            	}
                return $q.reject(rejection);
            }
        );
    };


    //Get Division info by id
    var _GetDivisionInfo = function (divisionId) {
        return $http.get(serviceBase + "CompanyDivison/GetDivisionInfo", {params: {DivisionID:divisionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Pincodes based on search query
    var _GetPinCodeList = function (searchQuery) {
        return $http.get(serviceBase + "PinCode/GetPinCodeList", {params: {PinCode:searchQuery}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Pincodes based on search query
    var _GetCityStateByPinCode = function (searchQuery) {
        return $http.get(serviceBase + "PinCode/GetCityStateByPinCode", {params: {PinCode:searchQuery}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Reporting Manager Based On RoleType
    var _GetCompanyReportingManagerBasedOnRoleType = function (roleId, companyId, divisionId) {
        return $http.get(serviceBase + "Company/GetCompanyReportingManagerBasedOnRoleType", {params: {CompanyRoleID:roleId, CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Brand by Division
    var _GetDivisionBrand = function (companyId, divisionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "CompanyDivison/GetDivisionBrand", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get Brand Groups List by Division
    var _GetBrandGroupList = function (companyId, divisionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "CompanyDivison/GetBrandGroupList", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add Update Brand Groups
    var _AddUpdateBrandGroup = function (brandGroupObject) {
        return $http.post(serviceBase + "CompanyDivison/AddUpdateBrandGroup", brandGroupObject)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Brand Group Info by BrandId
    var _GetBrandGroupInfo = function (brandGroupId) {
        return $http.get(serviceBase + "CompanyDivison/GetBrandGroupInfo", {params: {BrandgroupID:brandGroupId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Product List by Company Id
    var _GetCompanyProductList = function (companyId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetCompanyProductList", {params: {CompanyID:companyId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add Division Product Mapping
    var _AddDivisionProductMapping = function (mappingObj) {
        return $http.post(serviceBase + "CompanyDivison/AddDivisionProductMapping", mappingObj)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Bulk : Add Division Product Mapping
    var _BulkProductMapping = function (bulkMappingObj, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + "Company/BulkProductMapping", bulkMappingObj)
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Add Cluster
    var _AddUpdateCluster = function (clusterObj) {
        return $http.post(serviceBase + "Company/AddUpdateCluster", clusterObj)
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Cluster List by Company Id
    var _GetCompanyClusterList = function (companyId, cityId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Company/GetCompanyClusterList", {params: {CompanyID:companyId, CityID:cityId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Cluster List by Cluster Id
    var _GetCompanyClusterInfo = function (clusterId) {
        return $http.get(serviceBase + "Company/GetCompanyClusterInfo", {params: {ClusterID:clusterId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Company Info by Company ID
    var _GetSubscribedCompanyInfo = function (companyId) {
        return $http.get(serviceBase + "Company/GetSubscribedCompanyInfo", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Cities and TC Classes subscribed by Company ID
    var _GetCompanySubscribedCityTCClass = function (companyId) {
        return $http.get(serviceBase + "Company/GetCompanySubscribedCityTCClass", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Company by Company ID
    var _DeleteCompany = function (companyId) {
        return $http.delete(serviceBase + "Company/DeleteCompany", {params: {CompanyID:companyId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Company Division by Division ID
    var _DeleteDivision = function (divisionId) {
        return $http.delete(serviceBase + "Company/DeleteDivision", {params: {DivisionID:divisionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Division-Product Mapping
    var _DeleteDivisionProductMapping = function (divisionId, companyId, pfcId) {
        return $http.delete(serviceBase + "Company/DeleteDivisionProductMapping", {params: {DivisionID:divisionId, CompanyID:companyId, PFCID:pfcId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Brand Group by BrandGroupID
    var _DeleteBrandGroup = function (brandGroupId) {
        return $http.delete(serviceBase + "Company/DeleteBrandGroup", {params: {BrandGroupID:brandGroupId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Delete Cluster by ClusterID
    var _DeleteCompanyCluster = function (clusterId) {
        return $http.delete(serviceBase + "Company/DeleteCompanyCluster", {params: {ClusterID:clusterId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    companyServiceFactory.GetCompanyList = _GetCompanyList;
    companyServiceFactory.GetSubscriptionType = _GetSubscriptionType;
    companyServiceFactory.AddUpdateSubscribedType = _AddUpdateSubscribedType;
    companyServiceFactory.GetSubscriptionTypeDetails = _GetSubscriptionTypeDetails;
    companyServiceFactory.GetUnsubscribedCompanies = _GetUnsubscribedCompanies;
    companyServiceFactory.AddCompanySubscription = _AddCompanySubscription;
    companyServiceFactory.GetsubscribedCompanies = _GetsubscribedCompanies;
    companyServiceFactory.GetCompanyPerformance = _GetCompanyPerformance;
    companyServiceFactory.GetCompanyCountBasedOnSubscription = _GetCompanyCountBasedOnSubscription;
    companyServiceFactory.GetCompanyDivisions = _GetCompanyDivisions;
    companyServiceFactory.GetComparisionTypeValue = _GetComparisionTypeValue;
    companyServiceFactory.GetCompanyPerformanceMap = _GetCompanyPerformanceMap;
    companyServiceFactory.GetTherapyListByTherapyCode = _GetTherapyListByTherapyCode;
    companyServiceFactory.AddUpdateCompanyDivision = _AddUpdateCompanyDivision;
    companyServiceFactory.GetDivisionProducts = _GetDivisionProducts;
    companyServiceFactory.GetDivisionInfo = _GetDivisionInfo;
    companyServiceFactory.GetPinCodeList = _GetPinCodeList;
    companyServiceFactory.GetCityStateByPinCode = _GetCityStateByPinCode;
    companyServiceFactory.GetCompanyReportingManagerBasedOnRoleType = _GetCompanyReportingManagerBasedOnRoleType;
    companyServiceFactory.GetDivisionBrand = _GetDivisionBrand;
    companyServiceFactory.GetBrandGroupList = _GetBrandGroupList;
    companyServiceFactory.AddUpdateBrandGroup = _AddUpdateBrandGroup;
    companyServiceFactory.GetBrandGroupInfo = _GetBrandGroupInfo;
    companyServiceFactory.GetCompanyProductList = _GetCompanyProductList;
    companyServiceFactory.AddDivisionProductMapping = _AddDivisionProductMapping;
    companyServiceFactory.BulkProductMapping = _BulkProductMapping;
    companyServiceFactory.AddUpdateCluster = _AddUpdateCluster;
    companyServiceFactory.GetCompanyClusterList = _GetCompanyClusterList;
    companyServiceFactory.GetCompanyClusterInfo = _GetCompanyClusterInfo;
    companyServiceFactory.GetSubscribedCompanyInfo = _GetSubscribedCompanyInfo;
    companyServiceFactory.GetCompanySubscribedCityTCClass = _GetCompanySubscribedCityTCClass;
    companyServiceFactory.DeleteCompany = _DeleteCompany;
    companyServiceFactory.DeleteDivision = _DeleteDivision;
    companyServiceFactory.DeleteDivisionProductMapping = _DeleteDivisionProductMapping;
    companyServiceFactory.DeleteBrandGroup = _DeleteBrandGroup;
    companyServiceFactory.DeleteCompanyCluster = _DeleteCompanyCluster;

    return companyServiceFactory;
}]);

'use strict';
IMSModule.factory('adminServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var adminServiceFactory = {};

    // Get User by Company
    var _GetCompanyUsers = function (companyid, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "User/GetCompanyUsers", {params: {CompanyID:companyid}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Roles
    var _GetUserRoles = function () {
        return $http.get(serviceBase + "User/GetUserRoles")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Add-Update User
    var _AddUpdateUser = function (userInfo) {
        return $http.post(serviceBase + "User/AddUpdateUser", userInfo)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Add or Update Company User Role
    var _AddUpdateCompanyUserRole = function (objRole) {
        return $http.post(serviceBase + 'User/AddUpdateCompanyUserRole', objRole)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Get User Roles by Company & division ids
    var _GetUserRolesByCompany = function (companyId, divisionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "User/GetUserRolesByCompany", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Roles by Role id
    var _GetCompanyUserRoleByRoleID = function (roleId) {
        return $http.get(serviceBase + "User/GetCompanyUserRoleByRoleID", {params: {CompanyUserRoleID:roleId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Roles by Role id
    var _GetCompanyUserCount = function (companyId) {
        return $http.get(serviceBase + "User/GetCompanyUserCount", {params: {CompanyID:companyId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Info by User Id
    var _GetUserInfo = function (userId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "User/GetUserInfo", {params: {UserID:userId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Change Password
    var _ChangePassword = function (bodyObj, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + 'User/ChangePassword', bodyObj)
                .then(function (results) {
                    if(LoaderHide !== undefined){
	                	eval(LoaderHide);
	                }
                    return $q.resolve(results);
                },
                function (rejection) {
                    if(LoaderHide !== undefined){
	                	eval(LoaderHide);
	                }
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Delete User by User ID
    var _DeleteUser = function (userId) {
        return $http.delete(serviceBase + "User/DeleteUser", {params: {UserID:userId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Delete Company User Role by CompanyRoleID
    var _DeleteCompanyUserRole = function (companyRoleId) {
        return $http.delete(serviceBase + "User/DeleteCompanyUserRole", {params: {CompanyRoleID:companyRoleId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    adminServiceFactory.GetCompanyUsers = _GetCompanyUsers;
    adminServiceFactory.GetUserRoles = _GetUserRoles;
    adminServiceFactory.AddUpdateUser = _AddUpdateUser;
    adminServiceFactory.AddUpdateCompanyUserRole = _AddUpdateCompanyUserRole;
    adminServiceFactory.GetUserRolesByCompany = _GetUserRolesByCompany;
    adminServiceFactory.GetCompanyUserRoleByRoleID = _GetCompanyUserRoleByRoleID;
    adminServiceFactory.GetCompanyUserCount = _GetCompanyUserCount;
    adminServiceFactory.GetUserInfo = _GetUserInfo;
    adminServiceFactory.ChangePassword = _ChangePassword;
    adminServiceFactory.DeleteUser = _DeleteUser;
    adminServiceFactory.DeleteCompanyUserRole = _DeleteCompanyUserRole;
    
    return adminServiceFactory;
}]);

'use strict';
IMSModule.factory('analysisServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var analysisServiceFactory = {};

    
    // Post to Get Analysis Dashboard Data
    var _GetAnalysisDashboardData = function (bodyObject) {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + 'Analysis/GetAnalysisDashboardData', bodyObject)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Post to Get Analysis Market Share Data
    var _GetAnalysisMarketShareData = function (bodyObject) {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.post(serviceBase + 'Analysis/GetAnalysisMarketShareData', bodyObject)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.reject(rejection);
                }
            );
    };


    // Post to Generate PDF
    var _GeneratePDF = function (bodyObject, LoaderShow, LoaderHide) {
        if(LoaderShow !== undefined){
            eval(LoaderShow);
        }
        return $http.post(serviceBase + 'Analysis/GeneratePDF', bodyObject)
            .then(function (results) {
                    if(LoaderHide !== undefined){
                        eval(LoaderHide);
                    }
                    return $q.resolve(results);
                },
                function (rejection) {
                    if(LoaderHide !== undefined){
                        eval(LoaderHide);
                    }
                    return $q.reject(rejection);
                }
            );
    };
    
    
    analysisServiceFactory.GetAnalysisDashboardData = _GetAnalysisDashboardData;
    analysisServiceFactory.GetAnalysisMarketShareData = _GetAnalysisMarketShareData;
    analysisServiceFactory.GeneratePDF = _GeneratePDF;
    
    return analysisServiceFactory;
}]);

'use strict';
IMSModule.factory('savedSearhcesServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var savedSearhcesServiceFactory = {};
	
	
	// Add-Update User
    var _AddUpdateSavedSearches = function (userInfo) {
        return $http.post(serviceBase + "SavedSearches/AddUpdateSavedSearches", userInfo)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User by Company
    var _GetSavedSearchesbyUser = function (userId, tabType) {
        return $http.get(serviceBase + "SavedSearches/GetSavedSearchesbyUser", {params: {UserID:userId, TabType:tabType}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    savedSearhcesServiceFactory.AddUpdateSavedSearches = _AddUpdateSavedSearches;
    savedSearhcesServiceFactory.GetSavedSearchesbyUser = _GetSavedSearchesbyUser;
    return savedSearhcesServiceFactory;
}]);

'use strict';
IMSModule.factory('accountServices', ['$q', '$http', '$state', '$window', function ($q, $http, $state, $window) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var accountServiceFactory = {};

    // Add or Update Company User Role
    var _Login = function (username, pwd) {
        return $http.post(serviceBase + 'Login/Login?userName='+username+'&password='+ pwd)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Change Password
    var _ChangePassword = function (passwords, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + "Account/ChangePassword", passwords)
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Set Password
    var _SetPassword = function (passwords, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + "Account/SetPassword", passwords)
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    //Get User Session Info by SessionID
    var _GetUserSession = function (sessionId) {
        return $http.get(serviceBase + "Login/GetUserSession", {params: {SessionID:sessionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    //Terminate User session in server
    var _Logout = function (sessionId) {
        return $http.get(serviceBase + "Login/Logout", {params: {SessionID:sessionId}})
            .then(function (results) {
                if(results.data != null && results.status === 200){
                    $window.sessionStorage.removeItem('imsSessionInfo');
                    if($window.localStorage.BI_Tool_Info !== undefined){
                        $window.localStorage.removeItem('BI_Tool_Info');
                    }
                    $state.go('login');
                }
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    accountServiceFactory.Login = _Login;
    accountServiceFactory.ChangePassword = _ChangePassword;
    accountServiceFactory.SetPassword = _SetPassword;
    accountServiceFactory.GetUserSession = _GetUserSession;
    accountServiceFactory.Logout = _Logout;
    return accountServiceFactory;
}]);

'use strict';
IMSModule.factory('mediaServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var mediaServiceFactory = {};

    // Post Company Subscription
    var _PostAnImage = function (mediaObject, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
    	var fd = new FormData();
        fd.append('file', mediaObject);
        console.log('mediaObject: ',mediaObject);
        return $http.post(serviceBase + 'UploadImage', fd, {
	        	transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
			}
        )
        .success(function(results){
        	if(LoaderHide !== undefined){
            	eval(LoaderHide);
            }
            return $q.resolve(results);
        })
        .error(function(rejection){
        	if(LoaderHide !== undefined){
            	eval(LoaderHide);
            }
            return $q.reject(rejection);
        });
    };
    
    
    mediaServiceFactory.PostAnImage = _PostAnImage;
    
    return mediaServiceFactory;
}]);

'use strict';

/* This service maintainces and manages the session based on sessionID saved in LocalStorage */
IMSModule.service('sessionMgntService', ['$http', '$rootScope', '$window', 'accountServices', '$state',
                                        function ($http, $rootScope, $window, accountServices, $state) {
    var sessionMgntService_Obj = {};

    /* Generic Image Base Path */
	var baseURL = IMSModule.ServiceBaseUrl.replace('api/', 'Userimage/');

    sessionMgntService_Obj = function(){

        /* Register an handler for StateChangeStartEvent */
        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
            console.log('state change just started: ',toState, fromState, event);

            /* Manage session based on localStorage.BI_Tool_Info & sessionStorage.imsSessionInfo */
            if($window.localStorage.BI_Tool_Info !== undefined){
                if($window.sessionStorage.imsSessionInfo === undefined){
                    console.log('session block 1');
                    VerifyLocalStorage_SessionID(toState.name);
                }
                else{
                    console.log('session block 2');
                    if(JSON.parse($window.sessionStorage.imsSessionInfo).UserID !== undefined){
                        if(toState.name === 'login'){
                            event.preventDefault();
                        }

                        // if parent states does not match, except in case of 'superadmin' whose has access to 'admin' state
                        if((fromState.name !== '') && (fromState.name !== 'login')){
                            var from_parent = fromState.name.split('.')[0];
                            var to_parent = toState.name.split('.')[0];
                            if(from_parent !== to_parent){
                                if(JSON.parse($window.sessionStorage.imsSessionInfo).RoleID === 1){
                                    if(to_parent === 'user'){
                                        event.preventDefault();
                                    }
                                }
                                else{
                                    event.preventDefault();
                                }
                            }
                        }
                    }
                }
            }
        });


        /* Register an handler for StateChangeEvent */
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            var sessionInfoIMS = {};
            // set imsSessionInfo
            if($window.sessionStorage.imsSessionInfo === undefined){
                console.log('state change if block');
                sessionInfoIMS.toState = toState.name;
                sessionInfoIMS.fromState = fromState.name;
                $window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfoIMS);
            }
            else{
                console.log('state change else block');
                sessionInfoIMS = JSON.parse($window.sessionStorage.getItem('imsSessionInfo'));
                sessionInfoIMS.toState = toState.name;
                sessionInfoIMS.fromState = fromState.name;
                $window.sessionStorage.imsSessionInfo = JSON.stringify(sessionInfoIMS);
                // Update Navbar's sessionInfo
                $rootScope.$emit('UpdateNavbarSessionInfo', {});
            }
        });


        /* Handle window's Storage event on Local storage - IT HAS LITTLE DIFFERENT BEHAVIOUR IN INTERNET EXPLORER 9,10 */
        angular.element($window).on('storage', function(event){
            if(event.originalEvent.key === 'BI_Tool_Info'){
                if(event.originalEvent.newValue === null){      // if logout happened on one tab, others should also logout
                    $window.sessionStorage.removeItem('imsSessionInfo');
                    $state.go('login');
                }
                else{       // if login happened on one tab, others should also login
                    VerifyLocalStorage_SessionID('dummy389.state');
                }
            }
        });
    };


    /* Verify if BI_Tool_Info.SessionID is set in the Local Storage */
    function VerifyLocalStorage_SessionID(requestedState){
        var BI_info = JSON.parse($window.localStorage.BI_Tool_Info);
        accountServices.GetUserSession(BI_info.SessionID)
        .then(function (response) {
            if(response.data != null && response.status === 200) {
                sessionCreation_Redirect(response.data, requestedState);
            }
            else {      // logout if session is expired at Server side
                if($window.sessionStorage.imsSessionInfo !== undefined){
                    $window.sessionStorage.removeItem('imsSessionInfo');
                }
                $window.localStorage.removeItem('BI_Tool_Info');
                $state.go('login');
            }
        },
        function (rejection) {});
    }


    /* Manage state redirect based on response data */
    function sessionCreation_Redirect(data, requestedState){
        var reqParentState = requestedState.split('.')[0];
        var paths = setImagePath(data.ProfileImage, data.LogoFileName);
        data.ProfileImage = paths.profileImg;
        data.LogoFileName = paths.logoImg;

        if(data.RoleID === 1){
            $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), data));
            $window.localStorage.setItem('BI_Tool_Info', JSON.stringify({'SessionID':data.SessionID}));
            if(reqParentState === 'super'){
                $state.go(requestedState);
            }
            else{
                $state.go('super.customerlist');
            }
        }
        else if(data.RoleID > 1){
            // Verify the account for validity
            if(!data.CompanyStatus){
                showNotification({message:'Subscription is disabled', duration: 3000, class:'notify-bg', routine:null});
            }
            else if(!compareSubscriptionDate(data.SubscriptionStartDate, data.SubscriptionEndDate)){
                showNotification({message:'Subscription is ended', duration: 3000, class:'notify-bg', routine:null});
            }
            else{
                if(data.RoleID === 2){
                    $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), data));
                    if(reqParentState === 'admin'){
                        $state.go(requestedState);
                    }
                    else{
                        $state.go('admin.userlist');
                    }
                }
                else if(data.RoleID === 3){
                    $window.sessionStorage.imsSessionInfo = JSON.stringify(angular.element.extend(JSON.parse($window.sessionStorage.getItem('imsSessionInfo')), data));
                    if(reqParentState === 'user'){
                        $state.go(requestedState);
                    }
                    else{
                        $state.go('user.analysis');
                    }
                }
            }
        }
        else{
            alert('Invalid User Role');
        }
    }


    /* setting path for image */
    function setImagePath(profileImg, logoImg){
    	var path = {};
   		path.profileImg = (profileImg !== '') ? (baseURL + profileImg) : (baseURL + 'avatars/avatar-2-256.png');
    	path.logoImg = (logoImg !== '') ? (baseURL + logoImg) : (path.logoImg = baseURL + 'avatars/demo_logo.jpg');
    	return path;
    }


    /* Compare Subscription dates for validity */
	function compareSubscriptionDate(Sdate, Edate){
		Sdate = new Date(Sdate).getTime();
		Edate = new Date(Edate).getTime();
		var Tdate = new Date().getTime();
		if((Sdate <= Edate) && (Tdate <= Edate)){
			return true;
		}
		return false;
	}


	/* Show Notification */
	function showNotification(options){
		notify({
            message: options.message,
            classes: options.class,
            templateUrl: null,
            position: 'center',
            duration: (options.duration !== null) ? options.duration : 2000
        });
        eval(options.routine);
	}


    return sessionMgntService_Obj;
}]);


'use strict';

/* Currrency filter */
IMSModule.filter("currencycutter", [function(){
	function localFunction(inputVal){
		var absValue = 0;
		var outputVal = 0;
		if(inputVal !== undefined){
			absValue = Math.abs(inputVal);
			if(absValue >= Math.pow(10,12)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,9)).toFixed(2) + ' t';
			}
			else if(absValue >= Math.pow(10,9)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,9)).toFixed(2) + ' b';
			}
			else if(absValue >= Math.pow(10,7)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,7)).toFixed(2) + ' Cr';
			}
			else if(absValue >= Math.pow(10,6)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,6)).toFixed(2) + ' m';
			}
			else if(absValue >= Math.pow(10,3)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,3)).toFixed(2) + ' k';
			}
			else{
				outputVal = 'Rs. ' + inputVal.toFixed(2);
			}			
			return outputVal;
		}
	}
	return localFunction;
}]);


/* Format a number into comma seperated string representing Indian number system format optionally dividing by provided divisor */
IMSModule.filter("NumFormat_IN", [function(){
	function localFunction(inputVal, divisor){
		if(inputVal !== undefined){
			if(divisor !== undefined){
				return (inputVal/divisor).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,');
			}
			else{
				return inputVal.toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,');
			}
		}
		else{
			return null;
		}
	}
	return localFunction;
}]);


/* Filter for Bootstrap multiselect dropdown */
IMSModule.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
        
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});


IMSModule.directive('bindFile', [function() {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function($scope, el, attrs, ngModel) {
            el.bind('change', function(event) {
                ngModel.$setViewValue(event.target.files[0]);
                angular.element(event.target).siblings('label.file-label').html(event.target.files[0].name);
                $scope.$apply();
            });

            $scope.$watch(function() {
                return ngModel.$viewValue;
            }, function(value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);


IMSModule.directive('bindFileContent', [function() {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function($scope, el, attrs, ngModel) {
            el.bind('change', function(event) {
            	if(event.target.files[0] !== undefined){
            		var file = event.target.files[0];
            		fileReader.readAsArrayBuffer(file);
	                angular.element(event.target).parent('label').find('span.name').html(file.name);
	                $scope.$apply();
            	}
            });
            
            var fileReader = new FileReader();
            fileReader.onload = function(event){
            	ngModel.$setViewValue(event.target.result);
            };

            $scope.$watch(function() {
                return ngModel.$viewValue;
            }, function(value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);


// Directive to pickup a dropped file
IMSModule.directive("fileDropZone", [function() {
    return {
    	require: "ngModel",
        restrict : "A",
        link: function (scope, elem, attrs, ngModel, $parse) {
        	elem.on('dragover', function(e) { elem.parent('label').addClass('drag-area'); e.preventDefault(); e.stopPropagation(); });
        	elem.on('dragleave', function(e) { elem.parent('label').removeClass('drag-area'); e.preventDefault(); e.stopPropagation(); });
        	elem.on('dragenter', function(e) { e.preventDefault(); e.stopPropagation(); });
            elem.on('drop', function(e) {
                e.stopPropagation();
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files[0];
                fileReader.readAsArrayBuffer(file);
                elem.parent('label').find('p.title').html(file.name);
            });
            
            var fileReader = new FileReader();
            fileReader.onload = function(event){
            	ngModel.$setViewValue(event.target.result);
            };
        }
    };
}]);


/* Directive to assign file object to ng-model */
IMSModule.directive('fileInput', function($parse){
	return{
		require: "ngModel",
		restrict : 'A',
		link: function(scope, elem, attrs, ngModel){
			elem.bind('change', function(event){
				if(event.target.files[0] !== undefined){
					scope.$apply(function(){
						ngModel.$setViewValue(event.target.files[0]);
					});
				}
			});
		}
	}
});


/* Directive to hide sidemenu */
IMSModule.directive('hideSidemenu', ['$rootScope', function($rootScope){
    var directive = {};
    directive.restrict = 'A';
    directive.template = '<a href="javascript:void(0)" class="sidemenu-trigger"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>';
    directive.replace = true;
    directive.link = function(attr, elem, scope){
        elem.on('click', function(event){
            var body = angular.element('body');
            if(body.hasClass('hide-slidemenu')){
                body.removeClass('hide-slidemenu');
                if($rootScope.refreshCharts_Analysis !== undefined){
                    $rootScope.refreshCharts_Analysis();
                }
                if($rootScope.resizeMap !== undefined){
                    $rootScope.resizeMap();
                }
                if($rootScope.refreshCharts_CompAn !== undefined){
                    $rootScope.refreshCharts_CompAn();
                }
            }
            else{
                body.addClass('hide-slidemenu');
                if($rootScope.refreshCharts_Analysis !== undefined){
                    $rootScope.refreshCharts_Analysis();
                }  
                if($rootScope.resizeMap !== undefined){
                    $rootScope.resizeMap();
                }
                if($rootScope.refreshCharts_CompAn !== undefined){
                    $rootScope.refreshCharts_CompAn();
                }
            }
        });
    };
    return directive;
}]);


