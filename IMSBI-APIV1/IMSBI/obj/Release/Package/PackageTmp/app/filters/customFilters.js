'use strict';

/* Currrency filter */
IMSModule.filter("currencycutter", [function(){
	function localFunction(inputVal){
		var absValue = 0;
		var outputVal = 0;
		if(inputVal !== undefined){
			absValue = Math.abs(inputVal);
			if(absValue >= Math.pow(10,12)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,9)).toFixed(2) + ' t';
			}
			else if(absValue >= Math.pow(10,9)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,9)).toFixed(2) + ' b';
			}
			else if(absValue >= Math.pow(10,7)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,7)).toFixed(2) + ' Cr';
			}
			else if(absValue >= Math.pow(10,6)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,6)).toFixed(2) + ' m';
			}
			else if(absValue >= Math.pow(10,3)){
				outputVal = 'Rs. ' + (inputVal / Math.pow(10,3)).toFixed(2) + ' k';
			}
			else{
				outputVal = 'Rs. ' + inputVal.toFixed(2);
			}			
			return outputVal;
		}
	}
	return localFunction;
}]);


/* Format a number into comma seperated string representing Indian number system format optionally dividing by provided divisor */
IMSModule.filter("NumFormat_IN", [function(){
	function localFunction(inputVal, divisor){
		if(inputVal !== undefined){
			if(divisor !== undefined){
				return (inputVal/divisor).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,');
			}
			else{
				return inputVal.toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,');
			}
		}
		else{
			return null;
		}
	}
	return localFunction;
}]);


/* Filter for Bootstrap multiselect dropdown */
IMSModule.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
        
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});
