'use strict';
IMSModule.factory('loaderServices', [function () {
    var serviceObj = {};
    var loaderCounter = 0;

    serviceObj.startLoader = function(){
        if(loaderCounter === 0){
            angular.element('.loader-backdrop').fadeIn();
        }
        loaderCounter++;
    };

    serviceObj.stopLoader = function(stopAll){
        stopAll = stopAll || false;
        if(stopAll){
            loaderCounter = 0;
            angular.element('.loader-backdrop').fadeOut();
        }
        else{
            if(loaderCounter === 1){
                angular.element('.loader-backdrop').fadeOut();
            }
            if(loaderCounter > 0){
                loaderCounter--;
            }
        }
    };

    return serviceObj;
}]);
