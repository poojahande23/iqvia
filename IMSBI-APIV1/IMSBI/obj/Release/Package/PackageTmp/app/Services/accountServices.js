'use strict';
IMSModule.factory('accountServices', ['$q', '$http', '$state', '$window', function ($q, $http, $state, $window) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var accountServiceFactory = {};

    // Add or Update Company User Role
    var _Login = function (username, pwd) {
        return $http.post(serviceBase + 'Login/Login?userName='+username+'&password='+ pwd)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Change Password
    var _ChangePassword = function (passwords, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + "Account/ChangePassword", passwords)
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Set Password
    var _SetPassword = function (passwords, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + "Account/SetPassword", passwords)
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    //Get User Session Info by SessionID
    var _GetUserSession = function (sessionId) {
        return $http.get(serviceBase + "Login/GetUserSession", {params: {SessionID:sessionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    //Terminate User session in server
    var _Logout = function (sessionId) {
        return $http.get(serviceBase + "Login/Logout", {params: {SessionID:sessionId}})
            .then(function (results) {
                if(results.data != null && results.status === 200){
                    $window.sessionStorage.removeItem('imsSessionInfo');
                    if($window.localStorage.BI_Tool_Info !== undefined){
                        $window.localStorage.removeItem('BI_Tool_Info');
                    }
                    $state.go('login');
                }
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    accountServiceFactory.Login = _Login;
    accountServiceFactory.ChangePassword = _ChangePassword;
    accountServiceFactory.SetPassword = _SetPassword;
    accountServiceFactory.GetUserSession = _GetUserSession;
    accountServiceFactory.Logout = _Logout;
    return accountServiceFactory;
}]);