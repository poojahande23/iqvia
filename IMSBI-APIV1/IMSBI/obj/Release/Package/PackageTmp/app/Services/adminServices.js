﻿'use strict';
IMSModule.factory('adminServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var adminServiceFactory = {};

    // Get User by Company
    var _GetCompanyUsers = function (companyid, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "User/GetCompanyUsers", {params: {CompanyID:companyid}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Roles
    var _GetUserRoles = function () {
        return $http.get(serviceBase + "User/GetUserRoles")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Add-Update User
    var _AddUpdateUser = function (userInfo) {
        return $http.post(serviceBase + "User/AddUpdateUser", userInfo)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Add or Update Company User Role
    var _AddUpdateCompanyUserRole = function (objRole) {
        return $http.post(serviceBase + 'User/AddUpdateCompanyUserRole', objRole)
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Get User Roles by Company & division ids
    var _GetUserRolesByCompany = function (companyId, divisionId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "User/GetUserRolesByCompany", {params: {CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Roles by Role id
    var _GetCompanyUserRoleByRoleID = function (roleId) {
        return $http.get(serviceBase + "User/GetCompanyUserRoleByRoleID", {params: {CompanyUserRoleID:roleId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Roles by Role id
    var _GetCompanyUserCount = function (companyId) {
        return $http.get(serviceBase + "User/GetCompanyUserCount", {params: {CompanyID:companyId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Get User Info by User Id
    var _GetUserInfo = function (userId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "User/GetUserInfo", {params: {UserID:userId}})
            .then(function (results) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
            	if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Change Password
    var _ChangePassword = function (bodyObj, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.post(serviceBase + 'User/ChangePassword', bodyObj)
                .then(function (results) {
                    if(LoaderHide !== undefined){
	                	eval(LoaderHide);
	                }
                    return $q.resolve(results);
                },
                function (rejection) {
                    if(LoaderHide !== undefined){
	                	eval(LoaderHide);
	                }
                    return $q.reject(rejection);
                }
            );
    };
    
    
    // Delete User by User ID
    var _DeleteUser = function (userId) {
        return $http.delete(serviceBase + "User/DeleteUser", {params: {UserID:userId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    // Delete Company User Role by CompanyRoleID
    var _DeleteCompanyUserRole = function (companyRoleId) {
        return $http.delete(serviceBase + "User/DeleteCompanyUserRole", {params: {CompanyRoleID:companyRoleId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };
    
    
    adminServiceFactory.GetCompanyUsers = _GetCompanyUsers;
    adminServiceFactory.GetUserRoles = _GetUserRoles;
    adminServiceFactory.AddUpdateUser = _AddUpdateUser;
    adminServiceFactory.AddUpdateCompanyUserRole = _AddUpdateCompanyUserRole;
    adminServiceFactory.GetUserRolesByCompany = _GetUserRolesByCompany;
    adminServiceFactory.GetCompanyUserRoleByRoleID = _GetCompanyUserRoleByRoleID;
    adminServiceFactory.GetCompanyUserCount = _GetCompanyUserCount;
    adminServiceFactory.GetUserInfo = _GetUserInfo;
    adminServiceFactory.ChangePassword = _ChangePassword;
    adminServiceFactory.DeleteUser = _DeleteUser;
    adminServiceFactory.DeleteCompanyUserRole = _DeleteCompanyUserRole;
    
    return adminServiceFactory;
}]);