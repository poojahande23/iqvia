﻿'use strict';
IMSModule.factory('userServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var userServiceFactory = {};

    //Get All Cities
    var _GetAllCities = function () {
        return $http.get(serviceBase + "Filter/GetAllCities")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get All States
    var _GetAllStates = function () {
        return $http.get(serviceBase + "Filter/GetAllState")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get All Zones
    var _GetAllZones = function () {
        return $http.get(serviceBase + "Filter/GetAllZone")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get All Geography
    var _GetGeographyValues = function (filterType, companyId, sessionId) {
        return $http.get(serviceBase + "Filter/GetGeographyValues", {params: {FilterType: filterType, CompanyID: companyId, SessionID: sessionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    //Get Period Types subscribed by Company
    var _GetSubscriptionPeriodType = function (sessionId) {
        return $http.get(serviceBase + "Filter/GetSubscriptionPeriodType", {params: {SessionID:sessionId}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    //Get by TestName
    var _GetTimePeriods = function (cityId, periodType) {
        return $http.get(serviceBase + "Filter/GetAllTimePeriods", {params: {CityID:cityId, PeriodType:periodType}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    //Get by Bricks By Geography
    var _GetBricksByGeography = function (filterType, geographyId, companyId, divisionId) {
        return $http.get(serviceBase + "Filter/GetBricksByGeography", {params: {FilterType:filterType, GeographyID:geographyId, CompanyID:companyId, DivisionID:divisionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


	// Category Values for Selected category type
    var _GetCategoryValues = function (catType, companyId, divisionId, sessionId) {
        return $http.get(serviceBase + "Filter/GetCategoryValues", {params: {CategoryType:catType, CompanyID:companyId, DivisionID:divisionId, SessionID:sessionId}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    // Category Values for Selected category type
    var _GetApplicationPermissionList = function () {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.get(serviceBase + "Filter/GetApplicationPermissionList")
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                angular.element('.loader-backdrop').fadeOut();
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                angular.element('.loader-backdrop').fadeOut();
                return $q.reject(rejection);
            }
        );
    };


    // Get patches by CityID
    var _GetPatchListBycityID = function (cityId, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetPatchListBycityID", {params: {CityID: cityId}})
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get PacthIDs by ClusterIDs
    var _GetPatchIDsByClusterIDs = function (clusterIds, LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetPatchIDsByClusterIDs", {params: {ClusterIDs: clusterIds}})
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get List of TC values for supergroup
    var _GetSupergroupList = function (LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetSupergroupList")
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get List of Subscription period types
    var _GetSubscriptionPeriodTypes = function (LoaderShow, LoaderHide) {
    	if(LoaderShow !== undefined){
    		eval(LoaderShow);
    	}
        return $http.get(serviceBase + "Filter/GetSubscriptionPeriodTypes")
            .then(function (results) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.resolve(results);
            },
            function (rejection) {
                if(LoaderHide !== undefined){
                	eval(LoaderHide);
                }
                return $q.reject(rejection);
            }
        );
    };


    // Get Therapy list by Therapy code
    var _GetTherapyListByTherapyCode = function (tcCode) {
        return $http.get(serviceBase + "Filter/GetTherapyListByTherapyCode", {params: {TCcode: tcCode}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    // Get Products by Therapy Code
    var _GetProductsByTherapyCode = function (therapyCode) {
        return $http.get(serviceBase + "Filter/GetProductsByTherapyCode", {params: {TherapyCode: therapyCode}})
            .then(function (results) {
                return $q.resolve(results);
            },
            function (rejection) {
                return $q.reject(rejection);
            }
        );
    };


    userServiceFactory.GetAllCities = _GetAllCities;
    userServiceFactory.GetAllStates = _GetAllStates;
    userServiceFactory.GetAllZones = _GetAllZones;
    userServiceFactory.GetGeographyValues = _GetGeographyValues;
    userServiceFactory.GetSubscriptionPeriodType = _GetSubscriptionPeriodType;
    userServiceFactory.GetTimePeriods = _GetTimePeriods;
    userServiceFactory.GetBricksByGeography = _GetBricksByGeography;
    userServiceFactory.GetCategoryValues = _GetCategoryValues;
    userServiceFactory.GetApplicationPermissionList = _GetApplicationPermissionList;
    userServiceFactory.GetPatchListBycityID = _GetPatchListBycityID;
    userServiceFactory.GetPatchIDsByClusterIDs = _GetPatchIDsByClusterIDs;
    userServiceFactory.GetSupergroupList = _GetSupergroupList;
    userServiceFactory.GetSubscriptionPeriodTypes = _GetSubscriptionPeriodTypes;
    userServiceFactory.GetTherapyListByTherapyCode = _GetTherapyListByTherapyCode;
    userServiceFactory.GetProductsByTherapyCode = _GetProductsByTherapyCode;

    return userServiceFactory;
}]);
