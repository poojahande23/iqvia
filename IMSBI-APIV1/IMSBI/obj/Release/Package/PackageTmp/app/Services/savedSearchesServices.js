﻿'use strict';
IMSModule.factory('savedSearhcesServices', ['$q', '$http', function ($q, $http) {
    var serviceBase = IMSModule.ServiceBaseUrl;
    var savedSearhcesServiceFactory = {};


	// Add-Update User
    var _AddUpdateSavedSearches = function (userInfo) {
        return $http.post(serviceBase + "SavedSearches/AddUpdateSavedSearches", userInfo)
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    // Get User by Company
    var _GetSavedSearchesbyUser = function (userId, tabType) {
        return $http.get(serviceBase + "SavedSearches/GetSavedSearchesbyUser", {params: {UserID:userId, TabType:tabType}})
            .then(function (results) {
                console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                return $q.resolve(results);
            },
            function (rejection) {
                console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                return $q.reject(rejection);
            }
        );
    };


    // Post to Delete Save Searches
    var _DeleteSavedSearches = function (searchKeyId) {
    	angular.element('.loader-backdrop').fadeIn();
        return $http.delete(serviceBase + "SavedSearches/DeleteSavedSearches", {params: {SearchKeyID:searchKeyId}})
                .then(function (results) {
                    console.log("Ser:Suc:Cod" + results.status + "Msg:" + results.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.resolve(results);
                },
                function (rejection) {
                    console.log("Ser:Err:Cod" + rejection.status + "Msg:" + rejection.statusText);
                    angular.element('.loader-backdrop').fadeOut();
                    return $q.reject(rejection);
                }
            );
    };



    savedSearhcesServiceFactory.AddUpdateSavedSearches = _AddUpdateSavedSearches;
    savedSearhcesServiceFactory.GetSavedSearchesbyUser = _GetSavedSearchesbyUser;
    savedSearhcesServiceFactory.DeleteSavedSearches = _DeleteSavedSearches;
    return savedSearhcesServiceFactory;
}]);
