﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IMSBI.Startup))]
namespace IMSBI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
